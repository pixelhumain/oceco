/* global Template Blaze $ */
/* global IonToast */
// eslint-disable-next-line no-global-assign
IonToast = {

  generateUniqueId() {
    return `toast-${Math.random().toString(36).substr(2, 9)}`;
  },

  show(options) {
    this.template = Template.ionToast;

    // Figure out if a template or just a html string was passed
    let innerTemplate = '';
    if (options.templateName) {
      innerTemplate = Template[options.templateName].renderFunction().value;
    } else if (options.template) {
      innerTemplate = `<span class="toast-message">${options.template}</span>`;
    }

    let title = null;
    if (options.title) {
      title = options.title;
    }

    let isSticky = false;
    if (options.isSticky) {
      isSticky = options.isSticky;
    }

    let timeOut = 3000;
    if (options.timeOut) {
      timeOut = options.timeOut;
    }


    let showClose = false;
    if (options.showClose) {
      showClose = options.showClose;
    }

    let position = 'top';
    if (options.position) {
      position = options.position;
    }

    let type;
    if (options.type) {
      type = options.type;
    }

    const toastId = this.generateUniqueId();

    const data = {
      template: innerTemplate,
      title,
      showClose,
      toastId,
    };

    this.view = Blaze.renderWithData(this.template, data, $('.ionic-body').get(0));
    // $('body').addClass('toast-open');

    const $backdrop = $(this.view.firstNode());
    $backdrop.addClass('visible active');
    const $toast = $backdrop.find('.toast-section');
    $toast.addClass('toast-showing active');
    if (position) {
      $toast.addClass(`toast-${position}`);
    }
    if (type) {
      $toast.addClass(`toast-${type}`);
    }

    if (isSticky) return;
    setTimeout(function () {
      IonToast.close(toastId);
    }, timeOut);
  },

  close(toastId) {
    if (toastId) {
      const $toast = $('#' + toastId);
      $toast.removeClass('active');
      $toast.addClass('toast-hidden');

      setTimeout(function () {
        $toast.remove();
        $('body').removeClass('toast-open');
        Blaze.remove(this.view);
      }.bind(this), 100);
    } else {
      // eslint-disable-next-line no-underscore-dangle
      const $toast = this._domrange ? $(this.view.firstNode()).find('.toast-section') : $('.toast-section');
      // $toast.addClass('toast-hidden').removeClass('active');
      $toast.removeClass('active');
      $toast.addClass('toast-hidden');

      setTimeout(function () {
        $('body').removeClass('toast-open');
        Blaze.remove(this.view);
      }.bind(this), 100);
    }
  },

};

Template.ionToast.onRendered(function () {
  $(window).on('keyup.ionToast', function (event) {
    if (event.which === 27) {
      IonToast.close();
    }
  });
});

Template.ionToast.onDestroyed(function () {
  $(window).off('keyup.ionToast');
});

Template.ionToast.events({
  // Handle clicking the backdrop
  click(event) {
    const $target = $(event.target);
    if ($target.hasClass('toast-section')) {
      IonToast.close($target.attr('id'));
    }
    if ($target.hasClass('toast-close-icon')) {
      IonToast.close($target.closest('.toast-section').attr('id'));
    }
  },

});

Template.ionToast.helpers({
  hasHead() {
    return this.title || this.subTitle;
  },
});
