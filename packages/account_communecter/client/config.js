/* global Meteor */
import { Accounts } from 'meteor/accounts-base';

Meteor.startup(function () {
  Meteor.loginAsPixel = function (email, password, callback) {
    const loginRequest = { email, pwd: password };
    Accounts.callLoginMethod({
      // methodName: 'loginAsPixel',
      methodArguments: [loginRequest],
      userCallback: callback,
    });
  };

  Meteor.loginWithPassword = function (email, password, callback) {
    const loginRequest = { email, pwd: password };
    Accounts.callLoginMethod({
      // methodName: 'loginAsPixel',
      methodArguments: [loginRequest],
      userCallback: callback,
    });
  };
});
