/* eslint-disable no-underscore-dangle */
/* eslint-disable no-else-return */
/* global Meteor */
import { Accounts } from 'meteor/accounts-base';
import { HTTP } from 'meteor/jkuester:http';
import CryptoJS from 'crypto-js';
import crypto from 'crypto';

const encryptWithAES = (text) => {
  const CryptoJSAesJson = {
    stringify(cipherParams) {
      const j = { ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64) };
      if (cipherParams.iv) j.iv = cipherParams.iv.toString();
      if (cipherParams.salt) j.s = cipherParams.salt.toString();
      return JSON.stringify(j);
    },
    parse(jsonStr) {
      const j = JSON.parse(jsonStr);
      const cipherParams = CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(j.ct) });
      if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
      if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
      return cipherParams;
    },
  };

  const { passphrase } = Meteor.settings;
  return CryptoJS.AES.encrypt(JSON.stringify(text), passphrase, { format: CryptoJSAesJson }).toString();
};

// const encryptWithAES = (text) => {
//   // Vos paramètres de chiffrement
//   const algorithm = 'aes-256-cbc'; // ou un autre algorithme AES selon vos besoins
//   const { passphrase } = Meteor.settings;
//   const iv = crypto.randomBytes(16); // IV aléatoire

//   const cipher = crypto.createCipheriv(algorithm, passphrase, iv);
//   let encrypted = cipher.update(JSON.stringify(text), 'utf8', 'base64');
//   encrypted += cipher.final('base64');

//   return JSON.stringify({
//     iv: iv.toString('hex'),
//     content: encrypted
//   });
// };

// eslint-disable-next-line import/prefer-default-export
export const login = function (loginRequest) {
  // console.log('loginRequest', loginRequest);

  if (!loginRequest.email || !loginRequest.pwd) {
    return undefined;
  }

  const response = HTTP.call('POST', `${Meteor.settings.endpoint}/${Meteor.settings.module}/person/authenticatetoken`, {
    params: {
      email: loginRequest.email,
      pwd: loginRequest.pwd,
      tokenName: 'comobi',
    },
  });

  if (response && response.data && response.data.result === true && response.data.id) {
    let userId = null;
    let retourId = null;

    if (response.data.id && response.data.id.$id) {
      retourId = response.data.id.$id;
    } else {
      retourId = response.data.id;
    }
    // console.log(response.data);

    // ok valide
    const userM = Meteor.users.findOne({ _id: retourId });

    const token = response.data.token ? response.data.token : false;

    // console.log(userM);
    if (userM) {
      // Meteor.user existe
      userId = userM._id;
      if (token) {
        const profile = {};
        profile.token = token;
        profile.nameToken = 'comobi';
        Meteor.users.update(userId, { $set: { profile } });
      }
      Meteor.users.update(userId, { $set: { emails: [loginRequest.email] } });
    } else {
      // Meteor.user n'existe pas
      // username ou emails
      userId = Meteor.users.insert({ _id: retourId, emails: [loginRequest.email] });
      if (token) {
        const profile = {};
        profile.token = token;
        profile.nameToken = 'comobi';
        Meteor.users.update(userId, { $set: { profile } });
      }
    }

    // eslint-disable-next-line no-underscore-dangle
    const stampedToken = Accounts._generateStampedLoginToken();

    // Meteor.users.update(userId,
    //   { $push: { 'services.resume.loginTokens': stampedToken } });

    /* const hashedToken = Accounts._hashLoginToken(stampedToken.token);
    Accounts._insertHashedLoginToken(userId, {
    hashedToken
    }); */

    if (typeof this.setUserId === 'function') {
      // console.log('setUserId');
      this.setUserId(userId);
    }
    // console.log(userId);
    return {
      userId,
      token: stampedToken.token,
      tokenExpires: Accounts._tokenExpiration(stampedToken.when)
    };
  }
  if (response && response.data && response.data.result === false) {
    throw new Meteor.Error(Accounts.LoginCancelledError.numericError, response.data.msg);
  } else if (response && response.data && response.data.result === true && response.data.msg) {
    throw new Meteor.Error(response.data.msg);
  }
};

Accounts.registerLoginHandler('loginAsPixel', login);

export const loginOauth = function (loginRequest) {
  // console.log('loginOauth', loginRequest);

  if (!loginRequest.oauth) {
    return undefined;
  }

  const credentials = OAuth.retrieveCredential(loginRequest.oauth.credentialToken, loginRequest.oauth.credentialSecret);
  const serviceData = OAuth.openSecrets(credentials.serviceData);
  // console.log('credentials', credentials);
  // console.log('serviceData', serviceData);

  if (credentials.serviceName === 'ademe' || credentials.serviceName === 'tierslieuxorg' || credentials.serviceName === 'fabmobio') {
    // const userM = Meteor.users.findOne({ emails: serviceData.email });
    const services = {};
    services[credentials.serviceName] = serviceData;
    if (credentials.options && credentials.options.profile && credentials.options.profile.name) {
      services[credentials.serviceName].name = credentials.options.profile.name;
    }

    // console.log('services', services);

    const encryptText = encryptWithAES(credentials);

    // console.log('encryptText', encryptText);

    const response = HTTP.call('POST', `${Meteor.settings.endpoint}/${Meteor.settings.module}/person/authenticateservicetoken`, {
      params: {
        service: encryptText,
        tokenName: 'comobi',
      },
    });

    // console.log('response', response);

    if (response && response.data && response.data.result === true && response.data.id) {
      let userId = null;
      let retourId = null;

      if (response.data.id && response.data.id.$id) {
        retourId = response.data.id.$id;
      } else {
        retourId = response.data.id;
      }
      // console.log(response.data);

      // ok valide
      const userM = Meteor.users.findOne({ _id: retourId });

      const token = response.data.token ? response.data.token : false;

      // console.log(userM);
      if (userM) {
        // Meteor.user existe
        userId = userM._id;
        if (token) {
          const profile = {};
          profile.token = token;
          profile.nameToken = 'comobi';
          Meteor.users.update(userId, { $set: { profile } });
        }
        Meteor.users.update(userId, { $set: { emails: [response.data.account.email], services } });
      } else {
        // Meteor.user n'existe pas
        // username ou emails
        userId = Meteor.users.insert({ _id: retourId, emails: [response.data.account.email], services });
        if (token) {
          const profile = {};
          profile.token = token;
          profile.nameToken = 'comobi';
          Meteor.users.update(userId, { $set: { profile } });
        }
      }

      // eslint-disable-next-line no-underscore-dangle
      const stampedToken = Accounts._generateStampedLoginToken();

      if (typeof this.setUserId === 'function') {
        // console.log('setUserId');
        this.setUserId(userId);
      }
      // console.log(userId);
      return {
        userId,
        token: stampedToken.token,
        tokenExpires: Accounts._tokenExpiration(stampedToken.when),
      };
    }

    if (response && response.data && response.data.result === false) {
      throw new Meteor.Error(Accounts.LoginCancelledError.numericError, response.data.msg);
    } else if (response && response.data && response.data.result === true && response.data.msg) {
      throw new Meteor.Error(response.data.msg);
    }
  }

  return undefined;
};

Accounts.registerLoginHandler('loginAsPixelOauth', loginOauth);