Package.describe({
  summary: 'OpenID Connect (OIDC) fabmobio for Meteor accounts',
  version: '1.1.0',
  name: 'communecter:accounts-oidcfabmobio',
});

Package.onUse(function (api) {
  api.use('underscore@1.0.0', ['server', 'client']);
  api.use('accounts-base@2.2.1', ['client', 'server']);
  // Export Accounts (etc) to packages using this one.
  api.imply('accounts-base', ['client', 'server']);
  api.use('accounts-oauth@1.4.0', ['client', 'server']);

  api.use('communecter:oidcfabmobio@1.1.0', ['client', 'server']);

  api.addFiles('oidcfabmobio.js');
});
