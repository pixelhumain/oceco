# oceco

oceco fonctionne avec communecter et permet de publier des actions

# activer oceco sur une organisations
il faut une config minimum dans une entités "organizations" dans communecter pour activer oceco sur l'application

```json

```

## dev - commande pour lancer localement

* installer meteor 
* cloner le projet
* puis à la racine du projet

```shell
meteor npm install
```

### env

MONGO_URL - url de la base de donnée  
MONGO_OPLOG_URL - url de la base de donnée oplog

```shell
MONGO_URL='xxx' MONGO_OPLOG_URL='xxx' UNIVERSE_I18N_LOCALES=all meteor run --settings settings.json --port 3000
```

## setting file

settings.json minimun

```json
{
  "module": "co2",
  "endpoint": "http://",
  "public": {
    "urlimage": "http://"
  }
}
```

settings.json complet

```json
{
  "pushapiKey": "",
  "firebaseAdminSdkJson": "",
  "module": "co2",
  "endpoint": "",
  "vapidPrivateKey": "",
  "vapidSubject": "mailto:",
  "passphrase": "",
  "packages": {
    "service-configuration": {
      "tierslieuxorg": {
        "loginStyle": "popup",
        "clientId": "communecter",
        "secret": "",
        "serverUrl": "",
        "authorizationEndpoint": "",
        "tokenEndpoint": "",
        "userinfoEndpoint": "",
        "idTokenWhitelistFields": [],
        "textButton": "",
        "logoImage": "",
        "enable": true,
        "cordovaIsEnable": true,
        "androidIsEnable": true,
        "iosIsEnable": false
      }
    }
  },
  "openai": {
    "apiKey": ""
  },
  "rocketchat": {
    "host": "",
    "userId": "",
    "token": ""
  },
  "mailSetting": {
    "dev": {
      "to": "",
      "from": "",
      "protocol": "smtp",
      "username": "",
      "password": "",
      "host": "",
      "port": "2525"
    },
    "prod": {
      "from": "",
      "protocol": "smtps",
      "username": "",
      "password": "",
      "host": "",
      "port": "465"
    }
  },
  "ocecoApiToken": "",
  "public": {
    "packages": {
      "quave:reloader": {
        "automaticInitialization": false,
        "debug": true
      }
    },
    "updatePlugin": {
      "IOS": {
        "type": "IMMEDIATE",
        "stallDays": 5
      },
      "ANDROID": {
        "type": "IMMEDIATE",
        "stallDays": 5
      }
    },
    "rocketchat": {
      "host": ""
    },
    "native": {
      "nativeAppEnabled": true,
      "appleItunesAppId": "",
      "appleTeamId": "",
      "appleBundleId": "",
      "googlePlayAppId": "",
    },
    "vapidPublicKey": "",
    "mapbox": "",
    "googlekey": "",
    "urlimage": ""
  }
}
```

endpoint - url du serveur communecter  
passphrase - mot de passe pour crypter l'envoie de certaine données à 

packages.service-configuration.* - configuration pour sso qui doit être aussi activer sur communecter

openai.apiKey - clé pour openai

rocketchat.host - url du serveur rocketchat  
rocketchat.userId - id de l'utilisateur  
rocketchat.token - token de l'utilisateur  
public.rocketchat.host - url du serveur rocketchat

configuration pour l'envoie de mail

mailSetting.dev - configuration pour l'envoie de mail en dev  
mailSetting.prod - configuration pour l'envoie de mail en prod  

vapidPrivateKey - clé privé pour les push notification  
vapidSubject - sujet pour les push notification  
public.vapidPublicKey - clé public pour les push notification  

public.mapbox - clé pour mapbox  
public.googlekey - clé pour google  

public.urlimage - url de base pour les images  

public.native - configuration meta pour les applications natives  
public.native.nativeAppEnabled - activer ou non les meta pour les applications natives  
public.native.appleItunesAppId - id de l'application sur itunes  
public.native.appleTeamId - id de l'équipe sur itunes  
public.native.appleBundleId - bundle id de l'application sur itunes  
public.native.googlePlayAppId - id de l'application sur google play  




