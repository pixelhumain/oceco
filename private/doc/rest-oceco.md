# Documentation pour `rest.js`

## Description générale
`rest.js` est un fichier JavaScript servant de serveur de backend pour une application web. Il utilise Meteor js et la bibliothèque Express.js pour gérer les requêtes HTTP et interagit avec une base de données MongoDB.

## Endpoints
Le fichier `rest.js` gère plusieurs points d'accès (endpoints) pour des requêtes HTTP GET et POST.

- GET `/api/me/profil`
- GET `/api/me/organizations`
- POST `/api/action/create`
- POST `/api/action/task/create`
- POST `/api/action/task/list`
- POST `/api/action/task/checked`
- POST `/api/action/comment/create`
- POST `/api/action/comment/list`
- POST `/api/action/assign`
- POST `/api/action/finishMe`
- POST `/api/action/listElement`
- GET `/api/action/listMe`
- POST `/api/action/listMe`
- POST `/api/action/dashboardElement`
- POST `/api/action/dashboardUserElement`
- POST `/api/action/dashboardOrgaUserElement`
- POST `/api/generatetokenchat`
- POST `/api/generatetokenrest(/:ical)?`
- POST `/api/generatetoken/:tokenName`
- POST `/api/batchjson/create`
- POST `/api/hooks/gitlab`
- POST `/api/hooks/github/:projectid`
- GET `/api/organizations/:id/events`
- GET `/api/organizations/:id/meWallet`
- GET `/ical/organizations/:id/events`
- GET `/ical/organizations/:id/events/archives`
- GET `/ical/organizations/:id/actions`
- GET `/ical/citoyens/:id/:token/actions`
- GET `/download/doc/:moduleId/:folder/:name/file/:suite`

# Documentation détaillée des endpoints

### Endpoint : GET `/api/me/profil`

Cet endpoint est utilisé pour récupérer les informations de profil de l'utilisateur actuellement authentifié.

### Endpoint : GET `/api/me/organizations`

Cet endpoint est utilisé pour récupérer la liste des organisations auxquelles l'utilisateur actuellement authentifié appartient.

### Endpoint : POST `/api/action/create`

Cet endpoint est utilisé pour créer une action. Les détails de l'action à créer doivent être inclus dans le corps de la requête POST en JSON. Ces détails sont validés à l'aide du schéma `SchemasActionsRest`. Voici les champs qui peuvent être inclus :

- `name` (String) : Nom de l'action. **Obligatoire**.
- `description` (String) : Description de l'action.
- `tags` (Array of Strings) : Tags associés à l'action. Exemple : `"tags": ["tag1", "tag2", "tag3"]`.
- `tagsText` (String) : Texte des tags. Optionnel.
- `assignText` (String) : Texte d'assignation. Optionnel.
- `assign` (Array of Strings) : Assignation. Optionnel.
- `credits` (Number) : Crédits associés à l'action. Optionnel, valeur par défaut : 1.
- `max` (Number) : Maximum. Optionnel.
- `min` (Number) : Minimum. Optionnel.
- `idParentRoom` (String) : ID de la salle parente. Optionnel.
- `startDate` (Date) : Date de début de l'action. Optionnel.
- `endDate` (Date) : Date de fin de l'action. Optionnel.
- `parentId` (String) : ID du parent. **Obligatoire**.
- `parentType` (String) : Type de parent. **Obligatoire**, les valeurs autorisées sont : 'projects', 'organizations', 'events', 'citoyens'.
- `urls` (Array of Strings) : URLs associées à l'action. Optionnel.
- `milestoneId` (String) : ID du jalon. Optionnel.
- `options` (Object) : Options associées à l'action. Optionnel.
  - `creditAddPorteur` (Boolean) : Option pour ajouter du crédit au porteur. Optionnel, valeur par défaut : false.
  - `creditSharePorteur` (Boolean) : Option pour partager du crédit avec le porteur. Obligatoire, valeur par défaut : false.
  - `possibleStartActionBeforeStartDate` (Boolean) : Option pour permettre de commencer l'action avant la date de début. Optionnel, valeur par défaut : false.

Veuillez noter que la définition exacte de ces champs peut varier en fonction du contexte de votre application. Il est recommandé de consulter le code source et la documentation de votre application pour obtenir des informations plus précises.

### Endpoint : POST `/api/action/task/create`

Cet endpoint est utilisé pour créer une tâche liée à une action. Les détails de la tâche à créer doivent être inclus dans le corps de la requête POST en JSON. Ces détails sont validés à l'aide d'un schéma `SimpleSchema`. Voici les champs qui peuvent être inclus :

- `id` (String) : L'ID de l'action à laquelle la tâche sera liée. **Obligatoire**.
- `task` (String) : Le nom ou la description de la tâche à créer. **Obligatoire**.

### Endpoint : POST `/api/action/task/list`

Cet endpoint est utilisé pour récupérer une liste de tâches liées à une action spécifique. Les détails de l'action sont passés dans le corps de la requête POST en JSON. Ces détails sont validés à l'aide d'un schéma `SimpleSchema`. Voici les champs qui peuvent être inclus :

- `id` (String) : L'ID de l'action pour laquelle récupérer les tâches. **Obligatoire**.

### Endpoint : POST `/api/action/task/checked`

Cet endpoint est utilisé pour marquer une tâche comme "vérifiée" ou "complétée". Les détails de la tâche à vérifier sont passés dans le corps de la requête POST en JSON. Ces détails sont validés à l'aide d'un schéma `SimpleSchema`. Voici les champs qui peuvent être inclus :

- `id` (String) : L'ID de l'action à laquelle la tâche est liée. **Obligatoire**.
- `taskId` (String) : L'ID de la tâche à marquer comme vérifiée. **Obligatoire**.

### Endpoint : POST `/api/action/comment/create`

Ce endpoint est utilisé pour créer un commentaire sur une action. Les détails du commentaire sont passés dans le corps de la requête POST.

- `id` (String) : L'ID de l'action à laquelle la tâche est liée. **Obligatoire**.
- `text` (String) : Le texte du commentaire. **Obligatoire**.

### Endpoint : POST `/api/action/comment/list`

Ce endpoint est utilisé pour récupérer une liste de commentaires sur une action spécifique. Les détails de l'action sont passés dans le corps de la requête POST.

- `id` (String) : L'ID de l'action à laquelle la tâche est liée. **Obligatoire**.

### Endpoint : POST `/api/action/assign`

Ce endpoint est utilisé pour assigner une action à un utilisateur spécifique. Les détails de l'assignation sont passés dans le corps de la requête POST.

- `id` (String) : L'ID de l'action à laquelle la tâche est liée. **Obligatoire**.
- `memberId` (String) : L'ID de l'utilisateur à qui assigner l'action. **Obligatoire**.
- `parentId` (String) : L'ID du parent de l'action. **Obligatoire**.
- `parentType` (String) : Le type de parent de l'action. **Obligatoire**. Les valeurs autorisées sont : 'organizations', 'projects', 'events'.

### Endpoint : POST `/api/action/finishMe`

Ce endpoint est utilisé pour marquer une action comme "terminée". Les détails de l'action sont passés dans le corps de la requête POST.

- `id` (String) : L'ID de l'action à laquelle la tâche est liée. **Obligatoire**.

### Endpoint : POST `/api/action/listElement`
Ce endpoint est utilisé pour récupérer une liste d'actions lié à un élément spécifique. Les détails de l'élément sont passés dans le corps de la requête POST.

- `parentId` (String) : L'ID du parent de l'action. **Obligatoire**.
- `parentType` (String) : Le type de parent de l'action. **Obligatoire**

### Endpoint : GET `/api/action/listMe`

Ce endpoint est utilisé pour récupérer une liste d'actions liées à l'utilisateur actuellement authentifié.

### Endpoint : POST `/api/action/listMe`

Ce endpoint est également utilisé pour récupérer une liste d'actions liées à l'utilisateur actuellement authentifié, mais avec des paramètres supplémentaires qui peuvent être passés dans le corps de la requête POST.

- `search` (String) : Le texte de recherche. Optionnel.
- `parentId` (String) : L'ID du parent de l'action. Optionnel.
- `parentType` (String) : Le type de parent de l'action. Optionnel. Les valeurs autorisées sont : 'organizations', 'projects', 'events', 'citoyens'.

### Endpoint : POST `/api/action/dashboardElement`

Ce endpoint est utilisé pour récupérer les données du tableau de bord pour un élément spécifique. Les détails de l'élément sont passés dans le corps de la requête POST.

### Endpoint : POST `/api/action/dashboardUserElement`

Ce endpoint est utilisé pour récupérer les données du tableau de bord pour un utilisateur spécifique. Les détails de l'utilisateur sont passés dans le corps de la requête POST.

### Endpoint : POST `/api/action/dashboardOrgaUserElement`

Ce endpoint est utilisé pour récupérer les données du tableau de bord pour une organisation spécifique. Les détails de l'organisation sont passés dans le corps de la requête POST.

### Endpoint : POST `/api/generatetokenchat`

Ce endpoint est utilisé pour générer un token pour le chat. Les détails nécessaires pour générer le token sont passés dans le corps de la requête POST.

### Endpoint : POST `/api/generatetokenrest(/:ical)?`

Ce endpoint est utilisé pour générer un token pour les requêtes REST. Il peut également prendre un paramètre optionnel `ical` dans l'URL. Les détails nécessaires pour générer le token sont passés dans le corps de la requête POST.

### Endpoint : GET `/api/generatetoken/:tokenName`

Cet endpoint est utilisé pour générer un token d'accès. Le nom du token est passé en tant que paramètre dans l'URL (`:tokenName`). Le token généré est associé à l'utilisateur actuellement authentifié, dont l'ID est obtenu à partir de l'en-tête HTTP `x-user-id`.

#### Paramètres URL

- `tokenName` (String) : Le nom du token à générer. **Obligatoire**.

#### En-têtes HTTP

- `x-access-token` : Token d'accès pour authentifier l'utilisateur. **Obligatoire**.
- `x-user-id` : ID de l'utilisateur à authentifier. **Obligatoire**.

#### Réponse

- Si la génération du token est réussie, la réponse contiendra les informations de l'utilisateur ainsi que le token généré.
- Si la génération du token échoue, la réponse contiendra un message d'erreur approprié.

#### Exemple de Réponse Succès

```json
{
  "_id": "user_id_here",
  "username": "username_here",
  "name": "name_here",
  "email": "email_here",
  "tokenName": "token_name_here",
  "token": "generated_token_here",
  "status": true,
  "msg": "generate token rest oceco"
}
```

#### Exemple de Réponse Échec

```json
{
  "status": false,
  "msg": "not token"
}
```
ou
```json
{
  "status": false,
  "msg": "not user"
}
```

### Endpoint : POST `/api/batchjson/create`

Ce endpoint est utilisé pour créer un lot de données JSON. Les données à créer sont probablement passées dans le corps de la requête POST.

### Endpoint : POST `/api/hooks/gitlab`

Ce endpoint est utilisé pour recevoir des webhooks de GitLab. Les détails du webhook sont passés dans le corps de la requête POST.

### Endpoint : POST `/api/hooks/github/:projectid`

Ce endpoint est utilisé pour recevoir des webhooks de GitHub pour un projet spécifique. L'ID du projet est passé en tant que paramètre dans l'URL, et les détails du webhook sont passés dans le corps de la requête POST.

### Endpoint : GET `/api/organizations/:id/events`

Ce endpoint est utilisé pour récupérer les événements d'une organisation spécifique. L'ID de l'organisation est passé en tant que paramètre dans l'URL.

### Endpoint : GET `/api/organizations/:id/meWallet`

Ce endpoint est utilisé pour récupérer le portefeuille de l'utilisateur actuellement authentifié pour une organisation spécifique. L'ID de l'organisation est passé en tant que paramètre dans l'URL.

### Endpoint : GET `/ical/organizations/:id/events`

Ce endpoint est utilisé pour récupérer les événements iCal d'une organisation spécifique. L'ID de l'organisation est passé en tant que paramètre dans l'URL.

### Endpoint : GET `/ical/organizations/:id/events/archives`

Ce endpoint est utilisé pour récupérer les événements archivés iCal d'une organisation spécifique. L'ID
 de l'organisation est passé en tant que paramètre dans l'URL.

### Endpoint : GET `/ical/organizations/:id/actions`

Ce endpoint est utilisé pour récupérer les actions iCal d'une organisation spécifique. L'ID de l'organisation est passé en tant que paramètre dans l'URL.

### Endpoint : GET `/ical/citoyens/:id/:token/actions`

Ce endpoint est utilisé pour récupérer les actions iCal d'un citoyen spécifique. L'ID du citoyen et le token sont passés en tant que paramètres dans l'URL.

### Endpoint : GET `/download/doc/:moduleId/:folder/:name/file/:suite`

Ce endpoint est utilisé pour télécharger un document spécifique. Les détails du document sont passés en tant que paramètres dans l'URL.

---

### Fonction : `verifyToken(req, res, next)`

Cette fonction est un middleware utilisé pour vérifier le token d'un utilisateur. Elle est généralement appelée avant d'exécuter un autre middleware ou une fonction de gestion d'endpoint. Elle prend en entrée la requête (`req`), la réponse (`res`), et la fonction `next` qui est généralement appelée après avoir terminé l'exécution de ce middleware.

La fonction `verifyToken` est utilisée dans le fichier `rest.js` sur les endpoints suivants :

- POST `/api/action/create`
- POST `/api/action/task/create`
- POST `/api/action/task/list`
- POST `/api/action/task/checked`
- POST `/api/action/comment/create`
- POST `/api/action/comment/list`
- POST `/api/action/assign`
- POST `/api/action/finishMe`
- POST `/api/action/listElement`
- GET `/api/action/listMe`
- POST `/api/action/listMe`
- POST `/api/action/dashboardElement`
- POST `/api/action/dashboardUserElement`
- POST `/api/action/dashboardOrgaUserElement`
- POST `/api/batchjson/create`
- GET `/api/organizations/:id/events`
- GET `/api/organizations/:id/meWallet`
- GET `/api/generatetoken/:tokenName`

Cela signifie que lorsque vous faites une requête à l'un de ces endpoints, vous devez inclure un token d'accès valide dans l'en-tête `x-access-token` et l'ID de l'utilisateur `x-user-id` de votre requête. La fonction `verifyToken` vérifie que ce token est valide. Si le token est valide, la requête est autorisée à se poursuivre. Si le token n'est pas valide, la requête est rejetée.

C'est une pratique courante pour sécuriser les endpoints d'une API. En exigeant un token d'accès pour ces endpoints, vous vous assurez que seuls les utilisateurs authentifiés peuvent effectuer certaines actions.

---

### En-tête : `x-access-token` (pas necessaire si Bearer)
L'en-tête x-access-token est généralement utilisé pour transmettre un token d'accès dans une requête HTTP. Ce token est utilisé pour authentifier l'utilisateur et vérifier ses autorisations pour l'action demandée. Le token est généralement généré lors de la connexion de l'utilisateur et doit être inclus dans chaque requête qui nécessite une authentification.

### En-tête : `x-user-id` (pas necessaire si Bearer)
L'en-tête x-user-id est utilisé pour transmettre l'ID de l'utilisateur dans une requête HTTP. Cet ID est généralement utilisé pour identifier l'utilisateur qui effectue la requête. Comme pour le token d'accès, l'ID utilisateur est généralement généré lors de la connexion de l'utilisateur.

### En-tête : `x-token-name` (necessaire suivant le token)
L'en-tête x-token-name est utilisé pour transmettre le nom du token dans une requête HTTP.


### En-tête : `Authorization` Bearer
L'en-tête Authorization: Bearer est généralement utilisé pour transmettre un token d'accès dans une requête HTTP. Ce token est utilisé pour authentifier l'utilisateur et vérifier ses autorisations pour l'action demandée. Le token est généralement généré lors de la connexion de l'utilisateur et doit être inclus dans chaque requête qui nécessite une authentification.

il y a 2 type de token possible testé dans `verifyToken`

---

### le token rest oceco  qui est verifier avce la fonction `verifyRestToken`

le token de l'user à comme nom `restOceco` par default autrement il faut `x-token-name` en header et son type est `restAccessToken`, qu'un seul token valide avec ce nom

### le token du sso qui est verifier avec la fonction `verifyPersonalToken`

le token de l'user à comme nom `sso` et comme type `personalAccessToken`
c'est le token qui est en retour de la connexion au sso

---

### Fonction : `runAsUser(userId, function())`

Cette fonction permet d'exécuter une certaine fonction en tant qu'utilisateur spécifique. Elle prend en entrée l'ID de l'utilisateur et la fonction à exécuter.

### Fonction : `verifyRestToken(id, 'restIcal', token)`

Cette fonction est utilisée pour vérifier le token d'un utilisateur. Elle prend en entrée l'ID de l'utilisateur, le type de token (dans ce cas, 'restIcal'), et le token à vérifier.

### Fonction : `verifyTokenMeteor(req, res, next)`

Cette fonction est un middleware utilisé pour vérifier le token d'un utilisateur. Elle est généralement appelée avant d'exécuter un autre middleware ou une fonction de gestion d'endpoint. Elle prend en entrée la requête (`req`), la réponse (`res`), et la fonction `next` qui est généralement appelée après avoir terminé l'exécution de ce middleware.


## Authentification via SSO

Pour les endpoints nécessitant une authentification, vous pouvez utiliser les tokens obtenus via le SSO (`/oauth/userinfo`). Ces tokens doivent être inclus dans les en-têtes HTTP de la requête comme suit :

- `user_id` du SSO est mappé à `x-user-id` dans l'en-tête HTTP.
- `apiToken` du SSO est mappé à `x-access-token` dans l'en-tête HTTP.


## Génération de Token Oceco pour Tibillet

Pour générer un token Oceco spécifique pour Tibillet, utilisez la commande `curl` suivante :

```bash
curl --location --request GET 'https://oce.co.tools/api/generatetoken/tibillet' \
--header 'x-access-token: apiToken' \
--header 'x-user-id: user_id' \
--header 'Content-Type: application/json'
```

### Réponse attendue

La réponse sera un JSON contenant le nouveau token et d'autres informations sur l'utilisateur.

```json
{
  "_id": "user_id_here",
  "username": "username_here",
  "name": "name_here",
  "email": "email_here",
  "tokenName": "tibillet",
  "token": "generated_token_here",
  "bearer": "authorization_bearer_token",
  "status": true,
  "msg": "generate token rest oceco"
}
```


## Utilisation de Token Oceco pour Tibillet

Une fois que vous avez généré un token Oceco pour Tibillet, vous pouvez l'utiliser pour accéder à différents endpoints. Par exemple :

### Récupérer la liste des actions pour l'utilisateur actuel

```bash
curl --location --request GET 'https://oce.co.tools/api/action/listMe' \
--header 'x-access-token: token' \
--header 'x-user-id: user_id' \
--header 'x-token-name: tibillet' \
--header 'Content-Type: application/json'
```

### Récupérer le solde du portefeuille pour une organisation spécifique

```bash
curl --location --request GET 'https://oce.co.tools/api/organizations/:organizationId/meWallet' \
--header 'x-access-token: token' \
--header 'x-user-id: user_id' \
--header 'x-token-name: tibillet' \
--header 'Content-Type: application/json'
```

### Réponse attendue

La réponse sera un JSON contenant le solde du portefeuille pour l'utilisateur au sein de l'organisation spécifiée.

```json
{
  "status": true,
  "userId": "user_id_here",
  "organizationId": "organization_id_here",
  "wallet": 1785,
  "msg": "organization wallet user"
}
```

### Bearer

On peut aussi utiliser aussi le header `Authorization: Bearer bearerToken` pour faire les appelles

```bash
curl --location --request POST 'http://localhost:3000/api/action/create' \
--header 'Authorization: Bearer bearerToken' \
--header 'Content-Type: application/json' \
-d '{"name":"test beaerer", "parentId":"55ed9107e41d75a41a558524", "parentType":"citoyens"}'
```


