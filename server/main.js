import '../imports/startup/server';

import '../imports/api/server/config.js';
import '../imports/api/server/publish.js';
import '../imports/api/server/method.js';
import '../imports/api/getAppUpdateData.js';

import '../imports/api/server/citoyens/index.js';
import '../imports/api/server/organizations/index.js';
import '../imports/api/server/events/index.js';
import '../imports/api/server/actions/index.js';
import '../imports/api/server/notes/index.js';
import '../imports/api/server/webpush/index.js';

