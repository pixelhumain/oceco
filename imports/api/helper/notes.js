/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

// collection
import { Notes } from '../collection/notes.js';
import { Citoyens } from '../collection/citoyens.js';
import { Documents } from '../collection/documents.js';

Notes.helpers({
  isAuthor() {
    return this.creator === Meteor.userId();
  },
  isPublic() {
    return this.visibility === 'public';
  },
  isPrivate() {
    return this.visibility === 'private';
  },
  isToBeValidated(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !((this.links?.contributors?.[bothUserId]?.toBeValidated));
  },
  isIsInviting(scope, scopeId) {
    return !((this.links?.[scope]?.[scopeId]?.isInviting));
  },
  isContributors(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !!(this.links?.contributors?.[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting('contributors', bothUserId));
  },
  isConnexion(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return this.links?.contributors?.[bothUserId]?.isPresence?.connexion
  },
  docList() {
    // Fusionner les tableaux this.media.images et this.mediaFile.files s'ils existent
    const combinedIds = [
      ...(this.media?.images || []),
      ...(this.mediaFile?.files || [])
    ];

    // Filtrer les ID qui sont des chaînes et les convertir en Mongo.ObjectID
    const arrayId = combinedIds
      .filter(_id => typeof _id === 'string')
      .map(_id => new Mongo.ObjectID(_id));

    // Retourner les documents trouvés ou undefined si aucun ID valide
    return arrayId.length > 0 ? Documents.find({ _id: { $in: arrayId } }) : undefined;
  }
});






