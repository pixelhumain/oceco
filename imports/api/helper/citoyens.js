/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';

import { Citoyens } from '../collection/citoyens.js';
// import { Lists } from '../collection/lists.js';
import { Events } from '../collection/events.js';
import { Projects } from '../collection/projects.js';
import { Organizations } from '../collection/organizations.js';
import { Documents } from '../collection/documents.js';
import { Rooms } from '../collection/rooms.js';
import { Actions } from '../collection/actions.js';
import { ActivityStream } from '../collection/activitystream.js';
import { Notes } from '../collection/notes.js';
import {
  searchQuery, searchQuerySort, searchQueryNotes, queryOrPrivateScopeLinksIds, queryOrPrivateScopeLinks, queryLink, queryOptions, queryLinkInter, nameToCollection, applyDiacritics, customLinksOrga, scopeElement, createHelpers, organizationsFieldsNoPerma
} from '../helpers.js';

import { commonHelpers } from './commonHelpers.js';

const customCitoyensHelpers = {
  isVisibleFields(field) {
    return (this.isMe() || (this.isPublicFields(field) || (this.isFollowersMe() && this.isPrivateFields(field)))) ?? false;
  },
  formatBirthDate() {
    return moment(this.birthDate).format('L');
  },
  funcRoles(scope, scopeId) {
    return this.rolesCitoyen(scope, scopeId);
  },
  isRoles(scope, scopeId, rolesMatch) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return this.links?.[scopeCible]?.[scopeId]?.roles?.some((role) => rolesMatch?.includes(role));
  },
  isFavorites(scope, scopeId) {
    return !!(this.collections?.favorites?.[scope]?.[scopeId]);
  },
  isScope(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !!(this.links?.[scopeCible]?.[scopeId]?.type && this.isIsInviting(scopeCible, scopeId));
  },
  isScopeAdminPending(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !!((this.links?.[scopeCible]?.[scopeId]?.type && this.isIsAdminPending(scopeCible, scopeId)));
  },
  isScopeAdmin(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !!(this.links?.[scopeCible]?.[scopeId]?.type && this.isIsAdminInviting(scopeCible, scopeId));
  },
  isIsInviting(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !((this.links?.[scopeCible]?.[scopeId]?.isInviting));
  },
  isIsAdminPending(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !((this.links?.[scopeCible]?.[scopeId]?.isAdminPending));
  },
  isIsAdminInviting(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !(this.links?.[scopeCible]?.[scopeId]?.isAdminInviting);
  },
  isInviting(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !!(this.links?.[scopeCible]?.[scopeId]?.isInviting);
  },
  isAdminInviting(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return !!(this.links?.[scopeCible]?.[scopeId]?.isAdminInviting);
  },
  InvitingUser(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return this.links?.[scopeCible]?.[scopeId];
  },
  isMe() {
    return this._id.valueOf() === Meteor.userId();
  },
  isAdmin() {
    return this._id.valueOf() === Meteor.userId();
  },
  isSuperAdmin() {
    return this.roles?.superAdmin === true;
  },
  listEvents(search) {
    if (this.links?.events) {
      const queryStart = queryLink(this.links.events, search);
      const query = queryOrPrivateScopeLinksIds(queryStart, 'attendees');
      return Events.find(query, queryOptions);
    }
    return false;
  },
  countEvents(search) {
    return this.listEvents(search) && this.listEvents(search).count();
  },
  listProjects(search) {
    if (this.links?.projects) {
      const queryStart = queryLink(this.links.projects, search);
      const query = queryOrPrivateScopeLinksIds(queryStart, 'contributors');
      return Projects.find(query, queryOptions);
    }
    return false;
  },
  countProjects(search) {
    return this.listProjects(search) && this.listProjects(search).count();
  },
  listProjectsCreator() {
    const query = queryOrPrivateScopeLinks('contributors', this._id.valueOf());
    return Projects.find(query);
  },
  countProjectsCreator() {
    return this.listProjectsCreator() && this.listProjectsCreator().count();
  },
  listEventsCreator() {
    queryOptions.fields.startDate = 1;
    queryOptions.fields.startDate = 1;
    queryOptions.fields.geo = 1;
    const query = queryOrPrivateScopeLinks('attendees', this._id.valueOf());
    return Events.find(query, queryOptions);
  },
  countEventsCreator() {
    return this.listEventsCreator() && this.listEventsCreator().count();
  },
  listOrganizationsCreator() {
    const query = queryOrPrivateScopeLinks('members', this._id.valueOf());
    const options = {};
    options.fields = { _id: 1, name: 1, slug: 1, profilThumbImageUrl: 1, [`links.members.${this._id.valueOf()}`]: 1, ['links.projects']: 1, preferences: 1, 'oceco.pole': 1 };
    return Organizations.find(query, options);
  },
  countOrganizationsCreator() {
    return this.listOrganizationsCreator() && this.listOrganizationsCreator().count();
  },
  listActionsCreator(type = 'all', status = 'todo', search, searchSort) {
    const bothUserId = Meteor.userId();
    const query = {};
    const inputDate = new Date();
    const linkUserID = `links.contributors.${bothUserId}`;

    let queryone = {};
    queryone.endDate = { $exists: true, $gte: inputDate };
    queryone[linkUserID] = { $exists: true };
    queryone.status = status;
    if (Meteor.isClient) {
      if (search) {
        queryone = searchQuery(queryone, search);
      }
    }

    let querytwo = {};
    querytwo.endDate = { $exists: false };
    querytwo[linkUserID] = { $exists: true };
    querytwo.status = status;
    if (Meteor.isClient) {
      if (search) {
        querytwo = searchQuery(querytwo, search);
      }
    }

    let querythree = {};
    querythree.endDate = { $exists: false };
    querythree.parentId = { $in: [this._id.valueOf()] };
    querythree.status = status;
    if (Meteor.isClient) {
      if (search) {
        querythree = searchQuery(querythree, search);
      }
    }

    let queryfour = {};
    queryfour.endDate = { $exists: true, $gte: inputDate };
    queryfour.parentId = { $in: [this._id.valueOf()] };
    queryfour.status = status;
    if (Meteor.isClient) {
      if (search) {
        queryfour = searchQuery(queryfour, search);
      }
    }

    if (type === 'aFaire') {
      queryone.credits = { $gt: 0 };
      querytwo.credits = { $gt: 0 };
      querythree.credits = { $gt: 0 };
      queryfour.credits = { $gt: 0 };
    } else if (type === 'depenses') {
      queryone.credits = { $lt: 0 };
      querytwo.credits = { $lt: 0 };
      querythree.credits = { $lt: 0 };
      queryfour.credits = { $lt: 0 };
    }

    query.$or = [];
    query.$or.push(queryone);
    query.$or.push(querytwo);
    query.$or.push(querythree);
    query.$or.push(queryfour);

    const options = {};
    if (Meteor.isClient) {
      if (searchSort) {
        const arraySort = searchQuerySort('actions', searchSort);
        if (arraySort) {
          options.sort = arraySort;
        }
      }
    } else {
      options.sort = {
        startDate: 1,
      };
    }

    // console.log(query);

    return Actions.find(query, options);
  },
  actionsUserAll(userId, etat, search) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();

    // faire un ou si date pas remplie
    const query = {};
    const inputDate = new Date();
    // query.endDate = { $gte: inputDate };

    const fields = {};
    if (search) {
      // regex qui marche coté serveur parcontre seulement sur un mot
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'regex');
      const pattern = new RegExp(`.*${searchApplyDiacritics.replace(/\\/g, '\\\\')}.*`, 'i');
      fields.name = { $regex: pattern };
    }

    const finishedObj = {};
    const finished = `finishedBy.${bothUserId}`;
    if (etat === 'aFaire') {
      finishedObj[finished] = { $exists: false };
    } else if (etat === 'enAttente') {
      finishedObj[finished] = 'toModerate';
    }

    query.$or = [];
    query.$or.push({
      endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj,
    });
    query.$or.push({
      endDate: { $exists: false }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj,
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    return Actions.find(query);
  },
  countActionsCreator(type = 'all', status = 'todo', search) {
    return this.listActionsCreator(type, status, search) && this.listActionsCreator(type, status, search).count();
  },
  listNotesCreator(search = null, searchSort = null, limit = null) {
    const query = {};

    const bothUserId = Meteor.userId();
    const linkUserID = `links.contributors.${bothUserId}`;

    let queryone = {};

    queryone[linkUserID] = { $exists: true };
    if (search && !search.hash) {
      queryone = searchQueryNotes(queryone, search);
    }

    let querytwo = {};

    querytwo.parentId = { $in: [this._id.valueOf()] };

    if (search && !search.hash) {
      querytwo = searchQueryNotes(querytwo, search);
    }

    const queryoneAnd = {};
    queryoneAnd.$and = [];
    Object.keys(queryone).forEach((key) => {
      queryoneAnd.$and.push({ [key]: queryone[key] });
    });

    const querytwoAnd = {};
    querytwoAnd.$and = [];
    Object.keys(querytwo).forEach((key) => {
      querytwoAnd.$and.push({ [key]: querytwo[key] });
    });

    query.$or = [];
    query.$or.push(queryoneAnd);
    query.$or.push(querytwoAnd);

    const options = {};

    if (searchSort && !searchSort.hash) {
      const arraySort = searchQuerySort('notes', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      } else {
        options.sort = {
          updatedAt: -1,
        };
      }
    } else {
      options.sort = {
        updatedAt: -1,
      };
    }

    if (limit && !limit.hash) {
      options.limit = limit;
    }

    options.fields = { _id: 1, name: 1, parentId: 1, parentType: 1, [linkUserID]: 1, tags: 1, tracking: 1, createdAt: 1, updatedAt: 1, visibility: 1, creator: 1, contributorsCount: 1, lastModifiedBy: 1 };

    return Notes.find(query, options);
  },
  countNotesCreator(search) {
    return this.listNotesCreator(search) && this.listNotesCreator(search).count();
  },
  actionsAllAgenda(inputDateStart, inputDateEnd) {
    // faire un ou si date pas remplie
    const query = {};
    query.$and = [];
    query.$and.push({ answerId: { $exists: false } });
    query.$and.push({ status: { $in: ['done', 'todo'] } });
    inputDateStart = inputDateStart || new Date();
    if (inputDateEnd) {
      query.$and.push({ endDate: { $exists: true, $gte: inputDateStart } });
      query.$and.push({ startDate: { $exists: true, $lte: inputDateEnd } });
    } else {
      query.$and.push({ endDate: { $exists: true, $gte: inputDateStart } });
    }

    const linkUserID = `links.contributors.${this._id.valueOf()}`;
    query.$or = [];
    query.$or.push({
      [linkUserID]: { $exists: true },
    });
    query.$or.push({
      parentId: this._id.valueOf(),
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    options.fields = { _id: 1, name: 1, startDate: 1, endDate: 1, parentId: 1, parentType: 1, idParentRoom: 1, status: 1, [linkUserID]: 1, tags: 1 };
    return Actions.find(query, options);
  },
  eventsAllAgenda(inputDateStart, inputDateEnd) {
    // faire un ou si date pas remplie
    const query = {};
    query.$and = [];
    inputDateStart = inputDateStart || new Date();

    query.$and.push({ endDate: { $exists: true, $gte: inputDateStart } });
    query.$and.push({ startDate: { $exists: true, $lte: inputDateEnd } });

    const linkUserID = `links.attendees.${this._id.valueOf()}`;

    query.$and.push({
      [linkUserID]: { $exists: true },
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    options.fields = { _id: 1, name: 1, startDate: 1, endDate: 1, organizerId: 1, organizerType: 1, [linkUserID]: 1, tags: 1 };
    return Events.find(query, options);
  },
  detailRooms(roomId) {
    // if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {
    const query = {};
    if (this.isAdmin()) {
      query._id = new Mongo.ObjectID(roomId);
      query.status = 'open';
    } else {
      query.$or = [];
      const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
      if (roles) {
        query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: true, $in: roles } });
      }
      query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: false } });
    }
    return Rooms.find(query);
    // }
  },
  room() {
    return Rooms.findOne({ _id: new Mongo.ObjectID(Router.current().params.roomId) });
  },
  listNotifications() {
    return ActivityStream.api.isUnread(this._id.valueOf());
  },
  listNotificationsAsk() {
    return ActivityStream.api.isUnreadAsk(this._id.valueOf());
  },
  scopeVar() {
    return scopeElement.citoyens;
  },
  scopeEdit() {
    return 'citoyensEdit';
  },
  listScope() {
    return 'listCitoyens';
  },
  creditsUserOrgaTotal(organizationId) {
    const creditsUserOrgaTotal = this.userWallet?.[organizationId]?.userCredits ?? false;
    return creditsUserOrgaTotal;
  },
  isTransfertCreditOrgaUser(organizationId, credits) {
    const creditsUserOrgaTotal = this.creditsUserOrgaTotal(organizationId);
    if (creditsUserOrgaTotal && creditsUserOrgaTotal >= credits) {
      return true;
    }
    return false;
  },
  isUserBlocked(userId) {
    return !!this.isBlocked?.citoyens?.includes(userId);
  },
};

const citoyensHelpers = createHelpers(
  commonHelpers.isPublicFields,
  commonHelpers.isPrivateFields,
  commonHelpers.rolesLinks,
  commonHelpers.rolesCitoyen,
  commonHelpers.isFollows,
  commonHelpers.isFollowsMe,
  commonHelpers.listFollows,
  commonHelpers.countFollows,
  commonHelpers.isFollowers,
  commonHelpers.isFollowersMe,
  commonHelpers.listFollowers,
  commonHelpers.countFollowers,
  commonHelpers.actionIndicatorCount,
);

Citoyens.helpers({ ...citoyensHelpers, ...customCitoyensHelpers });

