/* eslint-disable import/prefer-default-export */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { Router } from 'meteor/iron:router';

// collection
import { Rooms } from '../collection/rooms.js';
import { Citoyens } from '../collection/citoyens.js';
import { Actions } from '../collection/actions.js';

import { queryOptions, scopeElement, createHelpers } from '../helpers.js';

import { commonHelpers } from './commonHelpers.js';

const customRoomsHelpers = {
  scopeVar() {
    return scopeElement.rooms;
  },
  scopeEdit() {
    return 'roomsEdit';
  },
  listScope() {
    return 'listRooms';
  },
  isRoles(scope, scopeId) {
    if (this.roles) {
      return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isRoles(scope, scopeId, this.roles);
    }
    return true;
  },
  rolesJoin() {
    if (this.roles && Array.isArray(this.roles)) {
      return this.roles.join(',');
    }
    return this.roles;
  },
  listActions(search) {
    const query = {};
    query.idParentRoom = this._id.valueOf();
    if (Meteor.isClient) {
      if (search) {
        if (search.charAt(0) === '#' && search.length > 1) {
          query.tags = { $regex: search.substr(1), $options: 'i' };
        } else {
          query.name = { $regex: search, $options: 'i' };
        }
      }
    }
    // queryOptions
    return Actions.find(query);
  },
  listActionsStatus(status, search) {
    const query = {};
    query.idParentRoom = this._id.valueOf();
    if (Meteor.isClient) {
      if (search) {
        if (search.charAt(0) === '#' && search.length > 1) {
          query.tags = { $regex: search.substr(1), $options: 'i' };
        } else {
          query.name = { $regex: search, $options: 'i' };
        }
      }
      if (status) {
        query.status = status;
      }
    }
    // queryOptions
    return Actions.find(query);
  },
  countActions(search) {
    return this.listActions(search) && this.listActions(search).count();
  },
  countActionsStatus(status, search) {
    return this.listActionsStatus(status, search) && this.listActionsStatus(status, search).count();
  },
  action(actionId) {
    const paramActionId = actionId || Router.current().params.actionId;
    return Actions.findOne({ _id: new Mongo.ObjectID(paramActionId) });
  },
};

const roomsHelpers = createHelpers(
  commonHelpers.isVisibleFields,
  commonHelpers.isPublicFields,
  commonHelpers.isPrivateFields,
  commonHelpers.creatorProfile,
  commonHelpers.isCreator,
);

Rooms.helpers({ ...roomsHelpers, ...customRoomsHelpers });
