/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

// schemas
import { Actions } from '../../collection/actions.js';
import { Documents } from '../../collection/documents.js';
import { Citoyens } from '../../collection/citoyens.js';
import { Comments } from '../../collection/comments.js';
import { Answers } from '../../collection/answers.js';
import {
  queryLink, queryOptions, arrayLinkProper, arrayLinkProperNoObject, nameToCollection, scopeElement, createHelpers
} from '../../helpers.js';

import { commonHelpers } from '../commonHelpers.js';

import { commonHelpersClient } from './commonHelpersClient.js';

import { Chronos } from '../../client/chronos.js';
import { Notes } from '../../collection/notes.js';

Actions.helpers({
  isDepense() {
    if (Session.get('settingOceco') && Session.get('settingOceco').spendNegative === true && ((Session.get('settingOceco').spendNegativeMax - this.userCredit()) * -1) >= (this.credits * -1)) {
      return true;
    }
    return this.credits <= 0 && this.userCredit() && (this.userCredit() + this.credits) >= 0;
  },
  isCreditNeg() {
    return this.credits && this.credits < 0;
  },
  podomoroSpentStart() {
    if (this.podomoroData().startedAt) {
      return Chronos.moment(this.podomoroData().startedAt).fromNow();
    }
    return false;
  },
  statusPodomoroActual() {
    return this.podomoro?.[Meteor.userId()]?.statusPodomoro ?? null;
  },
  viewDescription() {
    return this.description || this.tags || this.urls;
    // eslint-disable-next-line comma-dangle
  }
});

const customActionsHelpers = {
  isReportAbuse() {
    return !!(this.reportAbuse?.[Meteor.userId()]);
  },
  photoActionsAlbums() {
    if (this.media?.images) {
      const arrayId = this.media.images.filter((_id) => typeof _id === 'string').map((_id) => new Mongo.ObjectID(_id));
      return Documents.find({ _id: { $in: arrayId } });
    }
    return undefined;
  },
  docActionsList() {
    if (this.mediaFile?.files) {
      const arrayId = this.mediaFile.files.map((_id) => new Mongo.ObjectID(_id));
      return Documents.find({ _id: { $in: arrayId } });
    }
    return undefined;
  },

  scopeVar() {
    return scopeElement.actions;
  },
  scopeEdit() {
    return 'actionsEdit';
  },
  listScope() {
    return 'listActions';
  },
  parentScope() {
    const collection = nameToCollection(this.parentType);
    const parentOne = collection.findOne({ _id: new Mongo.ObjectID(this.parentId) });
    return parentOne;
  },

  isPodomoroWorking(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return this.podomoro?.[bothUserId]?.statusPodomoro === 'working';
  },
  podomoroData(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return this.podomoro?.[bothUserId] ?? null;
  },
  isContributors(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !!(this.links?.contributors?.[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting('contributors', bothUserId));
  },
  userNotTerminated(userId) {
    const bothUserId = userId ?? Meteor.userId();
    // Vérifie si l'utilisateur est un contributeur et n'a pas de statut dans finishedBy.
    return this.isContributors(bothUserId) && !this.finishedBy?.some(({ userId, status }) => userId === bothUserId);
  },
  userIsValidated(userId) {
    // Vérifie si l'utilisateur a le statut 'validated' dans finishedBy.
    return this.finishedBy?.some(({ userId: id, status }) => id === userId && status === 'validated');
  },
  userIsNoValidated(userId) {
    // Vérifie si l'utilisateur a le statut 'novalidated' dans finishedBy.
    return this.finishedBy?.some(({ userId: id, status }) => id === userId && status === 'novalidated');
  },
  userTovalidate(userId) {
    // Vérifie si l'utilisateur a le statut 'toModerate' dans finishedBy.
    return this.finishedBy?.some(({ userId: id, status }) => id === userId && status === 'toModerate');
  },
  avatarOneUserAction() {
    // si une action à un participant min et max et qu'un user à pris la tache
    // avoir son avatar sur l'action
    if (this.max === 1 && this.links?.contributors) {
      const query = queryLink(this.links.contributors);
      const citoyenOne = Citoyens.findOne(query);
      if (citoyenOne?.profilThumbImageUrl) {
        return citoyenOne.profilThumbImageUrl;
      }
    }
    return false;
  },
  contributorsPlusOne() {
    if (this.links?.contributors) {
      const arrayContributors = arrayLinkProperNoObject(this.links.contributors);
      return arrayContributors?.length > 1;
    }
    return false;
  },
  contributorsOne() {
    if (this.links?.contributors) {
      const arrayContributors = arrayLinkProperNoObject(this.links.contributors);
      return arrayContributors?.length === 1;
    }
    return false;
  },
  listContributors(search) {
    if (this.links?.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  listContributorsUsername(search) {
    if (this.links?.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, { fields: { username: 1 } });
    }
    return false;
  },
  userCredit() {
    const citoyenOne = Citoyens.findOne({
      _id: new Mongo.ObjectID(Meteor.userId()),
    });
    return citoyenOne && citoyenOne.userCredit();
  },
  isActionDepense() {
    return Math.sign(this.credits) === -1;
  },
  isAFaire() {
    return this.credits > 0 || (this.options?.creditAddPorteur);
  },
  isCreditAddPorteur() {
    return !this.credits && this.options?.creditAddPorteur;
  },
  isPossibleStartActionBeforeStartDate() {
    return this.options?.possibleStartActionBeforeStartDate;
  },
  isNotMax() {
    return this.max ? (this.max > this.countContributors()) : true;
  },
  projectDayHour() {
    // return moment(this.startDate).format(' ddd Do MMM à HH:mm ');
    return moment(this.startDate).format(' ddd D MMM LT ');
  },
  projectDay() {
    return moment(this.startDate).format(' ddd D MMM YY');
  },
  projectDayHourEnd() {
    if (this.projectDay() === this.projectDayEnd()) {
      return moment(this.endDate).format('LT');
    }
    return moment(this.endDate).format('ddd D MMM YY LT');
  },
  projectDayEnd() {
    return moment(this.endDate).format(' ddd D MMM YY');
  },
  projectDuration() {
    const startDate = moment(this.startDate);
    const endDate = moment(this.endDate);
    return Math.round(endDate.diff(startDate, 'minutes') / 60);
  },
  actionStartFromEnd() {
    const startDate = moment(this.startDate);
    const endDate = moment(this.endDate);
    return startDate.from(endDate, true);
  },
  actionParticipantsNbr() {
    if (this.links) {
      const numberParticipant = arrayLinkProper(this.links.contributors).length;
      return numberParticipant;
    }
    return 'aucun';
  },
  creditPositive() {
    if (this.credits >= 0 || this.isCreditAddPorteur()) {
      return true;
    }
    return false;
  },
  creditNegative() {
    return -this.credits;
  },
  countContributors(search) {
    return this.listContributors(search) && this.listContributors(search).count() ? this.listContributors(search).count() : 0;
  },
  countContributorsArray() {
    const arrayContributors = this.links?.contributors ? arrayLinkProperNoObject(this.links.contributors) : [];
    return arrayContributors?.length ?? 0;
  },
  isNotMin() {
    return this.min ? (this.min > this.countContributorsArray()) : true;
  },
  creditPartage() {
    if (this.options?.creditSharePorteur) {
      if (this.countContributors() > 0) {
        return Math.round(this.credits / this.countContributors());
      }
    }
    return this.credits;
  },
  isToBeValidated(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !((this.links?.contributors?.[bothUserId]?.toBeValidated));
  },
  isIsInviting(scope, scopeId) {
    return !((this.links?.[scope]?.[scopeId]?.isInviting));
  },
  momentStartDate() {
    return moment(this.startDate).toDate();
  },
  momentEndDate() {
    return moment(this.endDate).toDate();
  },
  formatStartDate() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate).format('L LT');
  },
  formatEndDate() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate).format('L LT');
  },
  formatMilestoneStartDate() {
    return this.milestone?.startDate && moment(this.milestone.startDate).format('L');
  },
  formatMilestoneEndDate() {
    return this.milestone?.endDate && moment(this.milestone.endDate).format('L');
  },
  listComments() {
    // console.log('listComments');
    const query = {};
    query.contextId = this._id?.valueOf();
    const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) });
    if (citoyenOne?.isBlocked?.citoyens) {
      query.author = { $nin: citoyenOne.isBlocked.citoyens };
    }
    return Comments.find(query, { sort: { created: -1 } });
  },
  commentsCount() {
    if (this.commentCount) {
      return this.commentCount;
    }
    return 0;
  },
  pomodoroTotalTime() {
    if (this.podomoro && Object.keys(this.podomoro).length > 0) {
      const totalAllTime = Object.keys(this.podomoro).reduce((sum, key) => sum + parseFloat(this.podomoro[key].totalTime || 0), 0);
      // return totalAllTime;
      // return moment().duration(totalAllTime, 'minutes').format('h:mm');
      // return moment.duration({ minutes: totalAllTime }).humanize();
      return moment.utc(moment.duration(totalAllTime, 'seconds').asMilliseconds()).format('LT'); // HH:mm
    }
    return 0;
  },
  pomodoroUserTotalTime(userId) {
    const bothUserId = userId ?? Meteor.userId();
    if (this.podomoro && this.podomoro[bothUserId] && this.podomoro[bothUserId].totalTime) {
      // return this.podomoro[bothUserId].totalTime;
      // return moment().duration(this.podomoro[bothUserId].totalTime, 'minutes').format('h:mm');
      return moment.utc(moment.duration(this.podomoro[bothUserId].totalTime, 'seconds').asMilliseconds()).format('LTS'); // HH:mm:ss
    }
    return 0;
  },
  totalTask() {
    const totalTask = this.tasks?.length ?? 0;
    return totalTask;
  },
  totalChecked() {
    const totalChecked = this.tasks?.filter((k) => k.checked === true)?.length ?? 0;
    return totalChecked;
  },
  totalUnChecked() {
    const totalUnChecked = this.tasks?.filter((k) => k.checked === false)?.length ?? 0;
    return totalUnChecked;
  },
  pourTaskChecked() {
    const totalTask = this.tasks?.length ?? 0;
    const totalChecked = this.tasks?.filter((k) => k.checked === true)?.length ?? 0;
    if (totalTask === 0) {
      return 0;
    }
    const pourcentage = (100 * totalChecked) / totalTask;
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
  answer() {
    if (this.answerId) {
      return Answers.findOne({ _id: new Mongo.ObjectID(this.answerId) });
    }
  },
  note() {
    if (this.noteId) {
      return Notes.findOne({ _id: new Mongo.ObjectID(this.noteId) });
    }
  }
};

const actionsHelpers = createHelpers(
  commonHelpersClient.isStartDate,
  commonHelpersClient.isNotStartDate,
  commonHelpersClient.isEndDate,
  commonHelpersClient.isNotEndDate,
  commonHelpersClient.timeSpentStart,
  commonHelpersClient.timeSpentEnd,
  commonHelpers.isVisibleFields,
  commonHelpers.isPublicFields,
  commonHelpers.isPrivateFields,
  commonHelpers.creatorProfile,
  commonHelpers.isCreator,
  commonHelpers.listMembersToBeValidated,
);

Actions.helpers({ ...actionsHelpers, ...customActionsHelpers });

