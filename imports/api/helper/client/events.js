/* eslint-disable consistent-return */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';

// collection
import { Events } from '../../collection/events.js';
import { Lists } from '../../collection/lists.js';
import { Citoyens } from '../../collection/citoyens.js';
import { Organizations } from '../../collection/organizations.js';
import { Rooms } from '../../collection/rooms.js';
import { Actions } from '../../collection/actions.js';

import {
  searchQuery, searchQuerySort, queryLink, arrayLinkParent, arrayOrganizerParent, isAdminArray, queryLinkIsInviting, queryLinkAttendees, arrayLinkAttendees, queryOptions, scopeElement, createHelpers, statusNoVisible
} from '../../helpers.js';

import { commonHelpers } from '../commonHelpers.js';

import { commonHelpersClient } from './commonHelpersClient.js';

const customEventsHelpers = {
  organizerEvent() {
    if (this.organizer) {
      const childrenParent = arrayOrganizerParent(this.organizer, ['events', 'projects', 'organizations', 'citoyens']);
      if (childrenParent) {
        return childrenParent;
      }
    }
    return undefined;
  },
  isAdmin(userId) {
    const bothUserId = userId ?? Meteor.userId();

    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(bothUserId) });
    const organizerEvent = this.organizerEvent();

    if (bothUserId && this.parent) {
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!((this.links?.attendees?.[bothUserId]?.isAdmin && this.isIsInviting('attendees', bothUserId)))) {
        return true;
      }
      if (this.parent[bothUserId]?.type === 'citoyens') {
        return true;
      }
    }

    if (bothUserId && this.organizer && organizerEvent?.length > 0 && isAdminArray(organizerEvent, citoyen)) {
      return true;
    }

    return !!((this.links?.attendees?.[bothUserId]?.isAdmin && this.isIsInviting('attendees', bothUserId)));
  },
  isScopeMe() {
    return this.isAdmin();
  },
  listAttendeesIsInviting(search) {
    if (this.links?.attendees) {
      const query = queryLinkIsInviting(this.links.attendees, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countAttendeesIsInviting(search) {
    return this.listAttendeesIsInviting(search)?.count();
  },
  listAttendeesValidate(search) {
    if (this.links?.attendees) {
      const query = queryLinkAttendees(this.links.attendees, search, 'citoyens');
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countAttendeesValidate(search) {
    return this.listAttendeesValidate(search)?.count();
  },
  listAttendeesOrgaValidate(search) {
    if (this.links?.attendees) {
      const query = queryLinkAttendees(this.links.attendees, search, 'organizations');
      return Organizations.find(query, queryOptions);
    }
    return false;
  },
  countAttendeesOrgaValidate(search) {
    return this.listAttendeesOrgaValidate(search) && this.listAttendeesOrgaValidate(search).count();
  },

  scopeVar() {
    return scopeElement.events;
  },
  scopeEdit() {
    return 'eventsEdit';
  },
  listScope() {
    return 'listEvents';
  },
  isAttendees(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !!((this.links?.attendees?.[bothUserId] && this.isIsInviting('attendees', bothUserId)));
  },
  listAttendees(search) {
    if (this.links?.attendees) {
      const query = queryLink(this.links.attendees, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countAttendees(search) {
    return this.listAttendees(search) && this.listAttendees(search).count();
  },
  countAttendeesSimple() {
    return this.links?.attendees && arrayLinkAttendees(this.links.attendees, 'citoyens').length + arrayLinkAttendees(this.links.attendees, 'organizations').length;
  },

  countPopMap() {
    return this.links?.attendees && _.size(this.links.attendees);
  },
  isStart() {
    const start = moment(this.startDate).toDate();
    return moment(start).isBefore(); // True
  },
  formatStartDate() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate).format('L LT');
  },
  formatEndDate() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate).format('L LT');
  },
  typeValue() {
    const eventTypes = Lists.findOne({ name: 'eventTypes' });
    return this.type && eventTypes?.list?.[this.type];
  },
  listEventTypes() {
    return Lists.find({ name: 'eventTypes' });
  },
  listEventsCreator() {
    if (this.links?.subEvents) {
      const eventsIds = arrayLinkParent(this.links.subEvents, 'events');
      const query = {};
      query._id = {
        $in: eventsIds,
      };
      query.status = { $nin: statusNoVisible };
      queryOptions.fields.startDate = 1;
      queryOptions.fields.startDate = 1;
      queryOptions.fields.geo = 1;
      return Events.find(query, queryOptions);
    }
  },
  countEventsCreator() {
    return this.listEventsCreator() && this.listEventsCreator().count();
  },
  eventsParent() {
    if (this.parent) {
      const childrenParent = arrayOrganizerParent(this.parent, ['events']);
      if (childrenParent) {
        return childrenParent;
      }
    }
  },
  listRooms(search) {
    // if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {
    const query = {};

    if (this.isAdmin()) {
      if (Meteor.isClient && search) {
        query.parentId = this._id.valueOf();
        query.name = { $regex: search, $options: 'i' };
        query.status = 'open';
      } else {
        query.parentId = this._id.valueOf();
        query.status = 'open';
      }
    } else {
      query.$or = [];
      const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
      if (roles) {
        if (Meteor.isClient && search) {
          query.$or.push({
            parentId: this._id.valueOf(), name: { $regex: search, $options: 'i' }, status: 'open', roles: { $exists: true, $in: roles },
          });
        } else {
          query.$or.push({ parentId: this._id.valueOf(), status: 'open', roles: { $exists: true, $in: roles } });
        }
      }
      if (Meteor.isClient && search) {
        query.$or.push({
          parentId: this._id.valueOf(), name: { $regex: search, $options: 'i' }, status: 'open', roles: { $exists: false },
        });
      } else {
        query.$or.push({ parentId: this._id.valueOf(), status: 'open', roles: { $exists: false } });
      }
    }

    queryOptions.fields.parentId = 1;
    queryOptions.fields.parentType = 1;
    queryOptions.fields.status = 1;
    queryOptions.fields.roles = 1;
    return Rooms.find(query, queryOptions);
    // }
  },
  detailRooms(roomId) {
    // if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {
    const query = {};
    if (this.isAdmin()) {
      query._id = new Mongo.ObjectID(roomId);
      query.status = 'open';
    } else {
      query.$or = [];
      const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
      if (roles) {
        query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: true, $in: roles } });
      }
      query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: false } });
    }
    return Rooms.find(query);
    // }
  },
  countRooms(search) {
    return this.listRooms(search) && this.listRooms(search).count();
  },
  room(roomId) {
    const paramRoomId = roomId || Router.current().params.roomId;
    return Rooms.findOne({ _id: new Mongo.ObjectID(paramRoomId) });
  },
  listActionsCreator(type = 'all', status = 'todo', search, searchSort) {
    const query = {};
    const inputDate = new Date();

    let queryone = {};
    // si admin
    if (!this.isAdmin()) {
      queryone.endDate = { $exists: true, $gte: inputDate };
      queryone.status = status;
    }
    queryone.parentId = { $in: [this._id.valueOf()] };

    if (Meteor.isClient) {
      if (search) {
        queryone = searchQuery(queryone, search);
      }
    }

    let querytwo = {};
    if (!this.isAdmin()) {
      querytwo.status = status;
    }
    querytwo.endDate = { $exists: false };
    querytwo.parentId = { $in: [this._id.valueOf()] };

    if (Meteor.isClient) {
      if (search) {
        querytwo = searchQuery(querytwo, search);
      }
    }

    if (type === 'aFaire') {
      queryone.$or = [
        { credits: { $gt: 0 } }, { 'options.creditAddPorteur': true },
      ];
      querytwo.$or = [
        { credits: { $gt: 0 } }, { 'options.creditAddPorteur': true },
      ];

      // queryone.credits = { $gt: 0 };
      // querytwo.credits = { $gt: 0 };
    } else if (type === 'depenses') {
      queryone.credits = { $lte: 0 };
      querytwo.credits = { $lte: 0 };
    }


    const queryoneAnd = {};
    queryoneAnd.$and = [];
    Object.keys(queryone).forEach((key) => {
      queryoneAnd.$and.push({ [key]: queryone[key] });
    });

    const querytwoAnd = {};
    querytwoAnd.$and = [];
    Object.keys(querytwo).forEach((key) => {
      querytwoAnd.$and.push({ [key]: querytwo[key] });
    });

    query.$or = [];
    query.$or.push(queryoneAnd);
    query.$or.push(querytwoAnd);

    const options = {};
    if (Meteor.isClient) {
      if (searchSort) {
        const arraySort = searchQuerySort('actions', searchSort);
        if (arraySort) {
          options.sort = arraySort;
        }
      }
    } else {
      options.sort = {
        startDate: 1,
      };
    }
    return Actions.find(query, options);
  },
  countActionsCreator(type = 'all', status = 'todo', search) {
    return this.listActionsCreator(type, status, search) && this.listActionsCreator(type, status, search).count();
  },

};

const eventsHelpers = createHelpers(
  commonHelpersClient.isStartDate,
  commonHelpersClient.isNotStartDate,
  commonHelpersClient.isEndDate,
  commonHelpersClient.isNotEndDate,
  commonHelpersClient.timeSpentStart,
  commonHelpersClient.timeSpentEnd,
  commonHelpers.isVisibleFields,
  commonHelpers.isPublicFields,
  commonHelpers.isPrivateFields,
  commonHelpers.documents,
  commonHelpers.rolesLinks,
  commonHelpers.roles,
  commonHelpers.creatorProfile,
  commonHelpers.isCreator,
  commonHelpers.isFavorites,
  commonHelpers.isScope,
  commonHelpers.isIsInviting,
  commonHelpers.isInviting,
  commonHelpers.InvitingUser,
  commonHelpers.toBeisInviting,
  commonHelpers.listNotifications,
  commonHelpers.countListNotifications,
  commonHelpers.listNotificationsAsk,
  commonHelpers.actionIndicatorCount,
  commonHelpers.userActionIndicatorCount,
);

Events.helpers({ ...eventsHelpers, ...customEventsHelpers });

// }
