/* eslint-disable consistent-return */
/* eslint-disable import/prefer-default-export */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { Jobs } from 'meteor/wildhart:jobs';
import { moment } from 'meteor/momentjs:moment';

import { ActivityStream, ActivityStreamReference } from '../../collection/activitystream.js';
import { Organizations } from '../../collection/organizations.js';
import { Projects } from '../../collection/projects.js';

import { getBackgroundColor } from '../../helpers.js';

ActivityStream.api = {
  Unseen(userId) {
    const bothUserId = userId ?? Meteor.userId();
    if (Counter.get(`notifications.${bothUserId}.Unseen`)) {
      return Counter.get(`notifications.${bothUserId}.Unseen`);
    }
    return undefined;
  },
  UnseenAsk(userId) {
    const bothUserId = userId ?? Meteor.userId();
    if (Counter.get(`notifications.${bothUserId}.UnseenAsk`)) {
      return Counter.get(`notifications.${bothUserId}.UnseenAsk`);
    }
    return undefined;
  },
  Unread(userId) {
    const bothUserId = userId ?? Meteor.userId();
    if (Counter.get(`notifications.${bothUserId}.Unread`)) {
      return Counter.get(`notifications.${bothUserId}.Unread`);
    }
    return undefined;
  },
  queryUnseen(userId, scopeId, scope) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const queryUnseen = { type: 'oceco' };
    queryUnseen[`notify.id.${bothUserId}`] = { $exists: 1 };
    queryUnseen[`notify.id.${bothUserId}.isUnseen`] = true;
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        queryUnseen['target.id'] = bothScopeId;
      } else {
        queryUnseen[`target${scopeCap}.id`] = bothScopeId;
      }
    }
    return ActivityStream.find(queryUnseen, { fields: { _id: 1 } });
  },
  queryUnseenAsk(userId, scopeId, scope) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const queryUnseen = { type: 'oceco' };
    queryUnseen[`notify.id.${bothUserId}`] = { $exists: 1 };
    queryUnseen[`notify.id.${bothUserId}.isUnseen`] = true;
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        queryUnseen['target.id'] = bothScopeId;
      } else {
        queryUnseen[`target${scopeCap}.id`] = bothScopeId;
      }

      queryUnseen.verb = { $in: ['ask'] };
    }
    return ActivityStream.find(queryUnseen, { fields: { _id: 1 } });
  },
  queryUnread(userId, scopeId, scope) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const queryUnread = { type: 'oceco' };
    queryUnread[`notify.id.${bothUserId}`] = { $exists: 1 };
    queryUnread[`notify.id.${bothUserId}.isUnread`] = true;
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        queryUnread['target.id'] = bothScopeId;
      } else {
        queryUnread[`target${scopeCap}.id`] = bothScopeId;
      }
    }
    return ActivityStream.find(queryUnread, { fields: { _id: 1 } });
  },
  isUnread(userId, scopeId, scope, limit) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }
      query.verb = { $nin: ['ask'] };
      query[`notify.id.${bothUserId}.isUnread`] = true;
    } else {
      query[`notify.id.${bothUserId}.isUnread`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options.fields = {};
    options.fields[`notify.id.${bothUserId}.isUnread`] = 1;
    options.fields[`notify.id.${bothUserId}.isUnseen`] = 1;
    options.fields['notify.displayName'] = 1;
    options.fields['notify.labelArray'] = 1;
    options.fields['notify.icon'] = 1;
    options.fields['notify.url'] = 1;
    options.fields['notify.objectType'] = 1;
    options.fields.verb = 1;
    options.fields.target = 1;
    options.fields.targetEvent = 1;
    options.fields.targetRoom =console.log(query);.log(query); 1;
    options.fields.targetProject = 1;
    options.fields.object = 1;
    options.fields.created = 1;
    options.fields.author = 1;
    options.fields.type = 1; */
    if (limit) {
      options.limit = limit;
    }
    return ActivityStream.find(query, options);
  },
  isUnreadRef(userId, scopeId, scope, limit) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query.userId = bothUserId;

    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query.targetId = bothScopeId;
      } else {
        query[`target${scopeCap}`] = bothScopeId;
      }
      query.verb = { $nin: ['ask'] };
      query.isUnread = true;
    } else {
      query.isUnread = true;
    }

    const options = {};
    options.sort = { updated: -1 };

    if (limit) {
      options.limit = limit;
    }
    options.fields = { notificationId: 1 };
    return ActivityStream.find({ _id: { $in: ActivityStreamReference.find(query, options).map((k) => new Mongo.ObjectID(k.notificationId)) } }, { sort: options.sort, limit: options.limit });
  },
  isUnreadContext(userId, scopeId, limit) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const orga = {};

    const options = { sort: { updated: -1 } }
    if (limit) {
      options.limit = limit;
    }

    ActivityStreamReference.find({ targetId: bothScopeId, targetType: 'organizations', userId: bothUserId, isUnread: true, verb: { $nin: ['ask'] } }, options).forEach((ref) => {
      if (ref.targetType === 'organizations') {
        // par organisation
        if (!orga[ref.targetId]) {
          orga[ref.targetId] = [];
        }
        // par projet
        if (ref.targetProject) {
          if (orga[ref.targetId][ref.targetProject]) {
            orga[ref.targetId][ref.targetProject].push(ref.notificationId);
          } else {
            orga[ref.targetId][ref.targetProject] = [];
            orga[ref.targetId][ref.targetProject].push(ref.notificationId);
          }

        } else {
          if (orga[ref.targetId]['notifications']) {
            orga[ref.targetId]['notifications'].push(ref.notificationId);
          } else {
            orga[ref.targetId]['notifications'] = [];
            orga[ref.targetId]['notifications'].push(ref.notificationId);
          }
        }
      }
    });

    const retourNotifactions = Object.keys(orga).map((orgaId) => {
      const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(orgaId) });
      const orgaNotif = { _id: orgaId, name: orgaOne && orgaOne.name ? orgaOne.name : null };
      if (orga[orgaId]['notifications']) {
        orgaNotif.notifications = ActivityStream.find({ _id: { $in: orga[orgaId]['notifications'].map(id => new Mongo.ObjectID(id)) } }, { sort: { updated: -1 } }).fetch();
        orgaNotif.counts = orgaNotif.notifications.length > 0 ? orgaNotif.notifications.length : 0;
      }

      orgaNotif.projects = Object.keys(orga[orgaId]).filter(projId => projId !== 'notifications').map((projId) => {
        const projOne = Projects.findOne({ _id: new Mongo.ObjectID(projId) });
        if (projOne && projOne.name) {
          const projectArray = { _id: projId, name: projOne.name, color: getBackgroundColor(projOne.name) };
          const projectNotifications = ActivityStream.find({
            _id: { $in: orga[orgaId][projId].map(id => new Mongo.ObjectID(id)) }
          }, { sort: { updated: -1 } }).fetch();
          projectArray.notifications = projectNotifications;
          projectArray.counts = projectNotifications.length;
          return projectArray;
        }
      });
      return orgaNotif;
    });
    return retourNotifactions;
  },
  countUnreadContext(userId, scopeId) {
    const bothUserId = userId ?? Meteor.userId();
    return ActivityStreamReference.find({ targetId: scopeId, targetType: 'organizations', userId: bothUserId, isUnread: true, verb: { $nin: ['ask'] } }).count();
  },
  countUnseenContext(userId, scopeId, targetProject = null) {
    const bothUserId = userId ?? Meteor.userId();
    const query = { targetId: scopeId, targetType: 'organizations', userId: bothUserId, isUnseen: true, verb: { $nin: ['ask'] } };
    if (targetProject) {
      query.targetProject = targetProject;
    }
    return ActivityStreamReference.find(query).count();
  },
  isUnseen(userId, scopeId, scope) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }

      query[`notify.id.${bothUserId}.isUnseen`] = true;
    } else {
      query[`notify.id.${bothUserId}.isUnseen`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options['fields'] = {}
    options['fields'][`notify.id.${bothUserId}`] = 1;
    options['fields']['notify.displayName'] = 1;
    options['fields']['notify.icon'] = 1;
    options['fields']['notify.url'] = 1;
    options['fields']['notify.objectType'] = 1;
    options['fields']['verb'] = 1;
    options['fields']['target'] = 1;
    options['fields']['targetEvent'] = 1;
    options['fields']['targetRoom'] = 1;
    options['fields']['targetProject'] = 1;
    options['fields']['created'] = 1;
    options['fields']['author'] = 1;
    options['fields']['type'] = 1; */
    return ActivityStream.find(query, options);
  },
  isUnseenAsk(userId, scopeId, scope) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query.verb = { $in: ['ask'] };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }
    } else {
      query[`notify.id.${bothUserId}.isUnseen`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options['fields'] = {}
    options['fields'][`notify.id.${bothUserId}`] = 1;
    options['fields']['notify.displayName'] = 1;
    options['fields']['notify.icon'] = 1;
    options['fields']['notify.url'] = 1;
    options['fields']['notify.objectType'] = 1;
    options['fields']['verb'] = 1;
    options['fields']['target'] = 1;
    options['fields']['targetEvent'] = 1;
    options['fields']['targetRoom'] = 1;
    options['fields']['targetProject'] = 1;
    options['fields']['created'] = 1;
    options['fields']['author'] = 1;
    options['fields']['type'] = 1; */
    return ActivityStream.find(query, options);
  },
  isUnreadAsk(userId, scopeId, scope, limit) {
    const bothUserId = userId ?? Meteor.userId();
    const bothScopeId = (typeof scopeId !== 'undefined') ? scopeId : false;
    const bothScope = (typeof scope !== 'undefined') ? scope : false;
    const query = { type: 'oceco' };
    query.verb = { $in: ['ask'] };
    query[`notify.id.${bothUserId}`] = { $exists: 1 };
    if (bothScopeId && bothScope) {
      const scopeCap = bothScope.charAt(0).toUpperCase() + bothScope.slice(1, -1);
      if (bothScope === 'organizations') {
        query['target.id'] = bothScopeId;
      } else {
        query[`target${scopeCap}.id`] = bothScopeId;
      }
    } else {
      query[`notify.id.${bothUserId}.isUnread`] = true;
    }
    const options = {};
    options.sort = { created: -1 };
    /* options.fields = {};
    options.fields[`notify.id.${bothUserId}`] = 1;
    options.fields['notify.displayName'] = 1;
    options.fields['notify.labelArray'] = 1;
    options.fields['notify.icon'] = 1;
    options.fields['notify.url'] = 1;
    options.fields['notify.objectType'] = 1;
    options.fields.verb = 1;
    options.fields.target = 1;
    options.fields.targetEvent = 1;
    options.fields.targetRoom = 1;
    options.fields.targetProject = 1;
    options.fields.object = 1;
    options.fields.created = 1;
    options.fields.author = 1;
    options.fields.type = 1; */
    if (limit) {
      options.limit = limit;
    }
    return ActivityStream.find(query, options);
  },
  // eslint-disable-next-line no-unused-vars
  listUserOrga(array, type, idUser = null, author = null, notificationObj, noAdmin) {
    let arrayIdsUsers = [];
    if (type === 'isAdmin') {
      arrayIdsUsers = Object.keys(array)
        .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && array[k].isAdmin === true)
        .map((k) => k);
    } else if (type === 'isMember') {
      // users
      arrayIdsUsers = Object.keys(array)
        .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && array[k].isAdmin !== true)
        .map((k) => k);
    } else if (type === 'isUser' && idUser) {
      // users
      arrayIdsUsers = Object.keys(array)
        .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && k === idUser)
        .map((k) => k);
    } else if (type === 'isActionMembers') {
      if (noAdmin) {
        const arrayIdsAdmin = Object.keys(array)
          .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === 'citoyens' && array[k].isAdmin === true)
          .map((k) => k);

        arrayIdsUsers = Object.keys(array)
          .filter((k) => array[k].type === 'citoyens')
          .map((k) => k);

        arrayIdsUsers = arrayIdsUsers.filter((x) => !arrayIdsAdmin.includes(x));
      } else {
        arrayIdsUsers = Object.keys(array)
          .filter((k) => array[k].type === 'citoyens')
          .map((k) => k);
      }
    }

    if (arrayIdsUsers.length > 0) {
      const idUsersObj = {};
      arrayIdsUsers.forEach(function (id) {
        if (author && author.id && author.id === id) {
          // author not notif
        } else {
          idUsersObj[id] = {
            isUnread: true,
            isUnseen: true,
          };
        }
      });
      return idUsersObj;
    }
    return null;
  },
  authorNotif(notificationObj, { id, name, type }) {
    notificationObj.author = {};
    notificationObj.author[id] = {};
    notificationObj.author[id].id = id;
    notificationObj.author[id].name = name;
    notificationObj.author[id].type = type;
    return notificationObj;
  },
  targetNotif(notificationObj, { id, name, type }) {
    notificationObj.target = {};
    notificationObj.target.type = type;
    notificationObj.target.id = id;
    notificationObj.target.name = name;
    return notificationObj;
  },
  objectNotif(notificationObj, { id, name, type }) {
    notificationObj.object = {};
    notificationObj.object[id] = {};
    notificationObj.object[id].id = id;
    notificationObj.object[id].type = type;
    notificationObj.object[id].name = name;
    return notificationObj;
  },
  ocecoNotif(notificationObj, { projectOne, eventOne, roomOne }) {
    // project
    if (projectOne) {
      notificationObj.targetProject = {};
      notificationObj.targetProject.type = 'projects';
      notificationObj.targetProject.id = projectOne._id.valueOf();
      notificationObj.targetProject.name = projectOne.name.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, '');
    }
    // event
    if (eventOne) {
      notificationObj.targetEvent = {};
      notificationObj.targetEvent.type = 'events';
      notificationObj.targetEvent.id = eventOne._id.valueOf();
      notificationObj.targetEvent.name = eventOne.name;
    }

    // room
    if (roomOne) {
      notificationObj.targetRoom = {};
      notificationObj.targetRoom.type = 'rooms';
      notificationObj.targetRoom.id = roomOne._id.valueOf();
      notificationObj.targetRoom.name = roomOne.name;
    }
    return notificationObj;
  },
  isNotificationChat(organizationOne) {
    if (organizationOne?.oceco?.notificationChat === true) {
      // notification chat ok
      return true;
    }
    return false;
  },
};

ActivityStream.helpers({
  authorId() {
    const keyArray = _.map(this.author, (a, k) => k);
    return keyArray[0];
  },
});
