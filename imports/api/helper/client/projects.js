/* eslint-disable consistent-return */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';

// collection
import { Projects } from '../../collection/projects.js';
import { Citoyens } from '../../collection/citoyens.js';
import { Events } from '../../collection/events.js';
import { Rooms } from '../../collection/rooms.js';
import { Actions } from '../../collection/actions.js';
import { Forms } from '../../collection/forms.js';

import {
  searchQuery, searchQuerySort, queryLink, queryLinkType, arrayLinkParent, arrayOrganizerParent, isAdminArrayOrga, queryOptions, scopeElement, createHelpers, actionIndicatorCountQuery, statusNoVisible,
  searchQueryNotes
} from '../../helpers.js';

import { commonHelpers } from '../commonHelpers.js';
import { Notes } from '../../collection/notes.js';

const customProjectsHelpers = {
  organizerProject() {
    if (this.parent) {
      const childrenParent = arrayOrganizerParent(this.parent, ['citoyens', 'organizations', 'projects']);
      if (childrenParent) {
        return childrenParent;
      }
    }
    return undefined;
  },
  isScopeMe() {
    return this.isContributors();
  },
  isAdmin(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(bothUserId) });
    const organizerProject = this.organizerProject();

    if (bothUserId && this.parent) {
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!((this.links?.contributors?.[bothUserId]?.isAdmin && this.isToBeValidated(bothUserId) && this.isAdminPending(bothUserId) && this.isIsInviting('contributors', bothUserId)))) {
        return true;
      }
      if (this.parent[bothUserId]?.type === 'citoyens') {
        return true;
      }
      return isAdminArrayOrga(organizerProject, citoyen);
    }
    return !!((this.links?.contributors?.[bothUserId]?.isAdmin && this.isToBeValidated(bothUserId) && this.isAdminPending(bothUserId) && this.isIsInviting('contributors', bothUserId)));
  },
  isAdminContributor(userId) {
    const bothUserId = userId ?? Meteor.userId();

    if (bothUserId && this.parent) {
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!((this.links?.contributors?.[bothUserId]?.isAdmin && this.isToBeValidated(bothUserId) && this.isAdminPending(bothUserId) && this.isIsInviting('contributors', bothUserId)))) {
        return true;
      }
      if (this.parent[bothUserId]?.type === 'citoyens') {
        return true;
      }
    }
    return !!((this.links?.contributors?.[bothUserId]?.isAdmin && this.isToBeValidated(bothUserId) && this.isAdminPending(bothUserId) && this.isIsInviting('contributors', bothUserId)));
  },

  scopeVar() {
    return scopeElement.projects;
  },
  scopeEdit() {
    return 'projectsEdit';
  },
  listScope() {
    return 'listProjects';
  },

  isContributors(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !!((this.links?.contributors?.[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting('contributors', bothUserId)));
  },
  listContributors(search = null, limit = null) {
    if (this.links?.contributors) {
      const query = queryLink(this.links.contributors, search);

      const options = { ...queryOptions };
      options.fields.username = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  listContributorsActions(actionId, search = null, limit = null) {
    if (this.links?.contributors) {
      const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId) });
      const query = queryLinkType(this.links.contributors, search, 'citoyens');
      const options = {};
      options.transform = (item) => {
        item.assign = actionOne && actionOne.isContributors(item._id.valueOf()) ? 1 : 0;
        return item;
      };
      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.projects.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  listContributorsTaskActions(actionId, taskId, search = null, limit = null) {
    if (this.links?.contributors) {
      const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId) });
      const query = queryLinkType(this.links.contributors, search, 'citoyens');
      const options = {};

      options.transform = (item) => {
        const task = actionOne && actionOne.tasks ? actionOne.tasks.filter((k) => k.taskId === taskId) : null;
        item.assign = task && task[0] && task[0].contributors && task[0].contributors[item._id.valueOf()] ? 1 : 0;
        return item;
      };

      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.projects.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  listContributorsNotes(docId, search = null, limit = null) {
    if (this.links?.contributors) {
      const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
      const query = queryLinkType(this.links.contributors, search, 'citoyens');
      const options = {};
      options.transform = (item) => {
        item.assign = noteOne && noteOne.isContributors(item._id.valueOf()) ? 1 : 0;
        return item;
      };
      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.projects.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  listNotesCreator(search = null, searchSort = null, limit = null) {
    const query = {};

    let querytwo = {};

    querytwo.parentId = { $in: [this._id.valueOf()] };

    // if (search && !search.hash) {
    //   querytwo = searchQueryNotes(querytwo, search);
    // }

    const querytwoAnd = {};
    querytwoAnd.$and = [];
    Object.keys(querytwo).forEach((key) => {
      querytwoAnd.$and.push({ [key]: querytwo[key] });
    });


    const options = {};

    if (searchSort && !searchSort.hash) {
      const arraySort = searchQuerySort('notes', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      } else {
        options.sort = {
          updatedAt: -1,
        };
      }
    } else {
      options.sort = {
        updatedAt: -1,
      };
    }

    if (limit && !limit.hash) {
      options.limit = limit;
    }

    options.fields = { _id: 1, name: 1, parentId: 1, parentType: 1, 'links.contributors': 1, tags: 1, tracking: 1, createdAt: 1, updatedAt: 1, visibility: 1, creator: 1, contributorsCount: 1, lastModifiedBy: 1 };

    return Notes.find(querytwoAnd, options);
  },
  countNotesCreator(search) {
    return this.listNotesCreator(search) && this.listNotesCreator(search).count();
  },
  isStart() {
    const start = moment(this.startDate).toDate();
    const now = moment().toDate();
    return moment(start).isBefore(now); // True
  },
  countContributors(search) {
    return this.listContributors(search) && this.listContributors(search).count();
  },

  listEventsCreator(inputDateStart, inputDateEnd) {
    if (this.links?.events) {
      const eventIds = arrayLinkParent(this.links.events, 'events');
      const query = {};
      query._id = {
        $in: eventIds,
      };

      inputDateStart = inputDateStart || new Date();
      query.endDate = {
        $gte: inputDateStart,
      };
      if (inputDateEnd) {
        query.startDate = { $lte: inputDateEnd };
      }
      query.status = { $nin: statusNoVisible };
      const options = {};
      options.sort = {
        startDate: 1,
      };
      return Events.find(query, options);
    }
  },
  countEventsCreator() {
    return this.listEventsCreator() && this.listEventsCreator().count();
  },
  listProjectsEventsCreator(querySearch, inputDateStart, inputDateEnd) {
    if (this.links?.events) {
      const eventIds = arrayLinkParent(this.links.events, 'events');
      const query = {};
      query._id = {
        $in: eventIds,
      };

      // eslint-disable-next-line no-param-reassign
      inputDateStart = inputDateStart || new Date();
      // query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDateStart };
      if (inputDateEnd) {
        query.startDate = { $lte: inputDateEnd };
      }
      query.status = { $nin: statusNoVisible };
      const options = {};
      options.fields = { _id: 1, name: 1, startDate: 1, endDate: 1, organizer: 1, status: 1 };
      options.sort = {
        startDate: 1,
      };
      return Events.find(query, options);
    }
  },
  listForms() {
    const queryProjectId = `parent.${this._id.valueOf()}`;
    return Forms.find({ [queryProjectId]: { $exists: true } });
  },
  countForms() {
    return this.listForms() && this.listForms().count();
  },
  listProjectsCreator() {
    if (this.links?.projects) {
      const projectIds = arrayLinkParent(this.links.projects, 'projects');
      const query = {};
      query._id = {
        $in: projectIds,
      };
      return Projects.find(query, queryOptions);
    }
  },
  countProjectsCreator() {
    return this.listProjectsCreator() && this.listProjectsCreator().count();
  },
  projectsParent() {
    if (this.parent) {
      const childrenParent = arrayOrganizerParent(this.parent, ['projects']);
      if (childrenParent) {
        return childrenParent;
      }
    }
  },
  listRooms(search) {
    if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {
      const query = {};

      if (this.isAdmin()) {
        if (Meteor.isClient && search) {
          query.parentId = this._id.valueOf();
          query.name = { $regex: search, $options: 'i' };
          query.status = 'open';
        } else {
          query.parentId = this._id.valueOf();
          query.status = 'open';
        }
      } else {
        query.$or = [];
        const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
        if (roles) {
          if (Meteor.isClient && search) {
            query.$or.push({
              parentId: this._id.valueOf(), name: { $regex: search, $options: 'i' }, status: 'open', roles: { $exists: true, $in: roles },
            });
          } else {
            query.$or.push({ parentId: this._id.valueOf(), status: 'open', roles: { $exists: true, $in: roles } });
          }
        }
        if (Meteor.isClient && search) {
          query.$or.push({
            parentId: this._id.valueOf(), name: { $regex: search, $options: 'i' }, status: 'open', roles: { $exists: false },
          });
        } else {
          query.$or.push({ parentId: this._id.valueOf(), status: 'open', roles: { $exists: false } });
        }
      }

      queryOptions.fields.parentId = 1;
      queryOptions.fields.parentType = 1;
      queryOptions.fields.status = 1;
      queryOptions.fields.roles = 1;
      return Rooms.find(query, queryOptions);
    }
  },
  detailRooms(roomId) {
    // if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {
    const query = {};
    if (this.isAdmin()) {
      query._id = new Mongo.ObjectID(roomId);
      query.status = 'open';
    } else {
      query.$or = [];
      const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
      if (roles) {
        query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: true, $in: roles } });
      }
      query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: false } });
    }
    return Rooms.find(query);
    // }
  },
  countRooms(search) {
    return this.listRooms(search) && this.listRooms(search).count();
  },
  room(roomId) {
    const paramRoomId = roomId || Router.current().params.roomId;
    return Rooms.findOne({ _id: new Mongo.ObjectID(paramRoomId) });
  },
  listActionsCreator(type = 'all', status = 'todo', search = null, searchSort = null, milestoneId = null, limit = null) {
    const query = {};
    const inputDate = new Date();

    // all
    if (search && !search.hash && search.startsWith('all:')) {
      status = 'all';
    }

    /* {
      "$or": [
        {
          "$and": [
            { "endDate": { "$exists": true, "$gte": "2020-07-20T07:41:09.093Z" } },
            { "parentId": { "$in": ["5bd2bcfb40bb4e4509f7eabe"] } },
            { "status": "todo" },
            {
              "$or": [
                { "credits": { "$gt": 0 } }, { "options.creditAddPorteur": { "$exists": true } }
              ]
            }]
        },
        {
          "$and": [
            { "endDate": { "$exists": false } },
            { "parentId": { "$in": ["5bd2bcfb40bb4e4509f7eabe"] } },
            { "status": "todo" },
            {
              "$or": [
                { "credits": { "$gt": 0 } }, { "options.creditAddPorteur": { "$exists": true } }
              ]
            }]
        }
      ]
    } */

    let queryone = {};
    queryone.parentId = { $in: [this._id.valueOf()] };

    if (search && !search.hash) {
      queryone = searchQuery(queryone, search);
    }

    if (status !== 'all' && !queryone._id && !queryone.numberId) {
      queryone.status = status;
    }

    if (milestoneId && !milestoneId.hash) {
      if (milestoneId === 'noMilestone') {
        queryone.milestone = { $exists: false };
      } else {
        queryone['milestone.milestoneId'] = milestoneId;
      }
    } else {
      if (status === 'todo' && !queryone._id && !queryone.numberId) {
        queryone.endDate = { $exists: true, $gte: inputDate };
      } else {
        queryone.endDate = { $exists: true };
      }
    }



    let querytwo = {};
    querytwo.parentId = { $in: [this._id.valueOf()] };

    if (search && !search.hash) {
      querytwo = searchQuery(querytwo, search);
    }

    if (status !== 'all' && !querytwo._id && !querytwo.numberId) {
      querytwo.status = status;
    }
    if (milestoneId && !milestoneId.hash) {
      if (milestoneId === 'noMilestone') {
        querytwo.milestone = { $exists: false };
      } else {
        querytwo['milestone.milestoneId'] = milestoneId;
      }
    } else {
      querytwo.endDate = { $exists: false };
    }

    if (type === 'aFaire') {
      queryone.$or = [
        { credits: { $gt: 0 } }, { 'options.creditAddPorteur': true },
      ];
      querytwo.$or = [
        { credits: { $gt: 0 } }, { 'options.creditAddPorteur': true },
      ];
      // queryone.credits = { $gt: 0 };
      // querytwo.credits = { $gt: 0 };
    } else if (type === 'depenses') {
      queryone.credits = { $lte: 0 };
      querytwo.credits = { $lte: 0 };
    }

    const queryoneAnd = {};
    queryoneAnd.$and = [];
    Object.keys(queryone).forEach((key) => {
      queryoneAnd.$and.push({ [key]: queryone[key] });
    });

    const querytwoAnd = {};
    querytwoAnd.$and = [];
    Object.keys(querytwo).forEach((key) => {
      querytwoAnd.$and.push({ [key]: querytwo[key] });
    });

    query.$or = [];
    query.$or.push(queryoneAnd);
    query.$or.push(querytwoAnd);

    const options = {};

    if (searchSort && !searchSort.hash) {
      const arraySort = searchQuerySort('actions', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      }
    }

    if (limit && !limit.hash) {
      options.limit = limit;
    }

    return Actions.find(query, options);
  },
  countActionsCreator(type = 'all', status = 'todo', search, milestoneId) {
    return this.listActionsCreator(type, status, search, null, milestoneId) && this.listActionsCreator(type, status, search, null, milestoneId).count();
  },
  actionsAllAgenda(inputDateStart, inputDateEnd) {
    // faire un ou si date pas remplie
    const query = {};
    query.$and = [];
    query.$and.push({ answerId: { $exists: false } });
    query.$and.push({ parentId: this._id.valueOf() });
    query.$and.push({ status: { $in: ['done', 'todo'] } });
    inputDateStart = inputDateStart || new Date();
    query.$and.push({ endDate: { $exists: true, $gte: inputDateStart } });
    if (inputDateEnd) {
      query.$and.push({ startDate: { $exists: true, $lte: inputDateEnd } });
    }

    const options = {};
    options.sort = {
      startDate: 1,
    };

    options.fields = { _id: 1, name: 1, startDate: 1, endDate: 1, parentId: 1, parentType: 1, idParentRoom: 1, status: 1, tags: 1 };
    const linkUserID = `links.contributors.${Meteor.userId()}`;
    options.fields[linkUserID] = 1;
    return Actions.find(query, options);
  },
  milestoneDetail(milestoneId) {
    if (this.oceco?.milestones?.length > 0) {
      const arrayMilestoneOne = this.oceco.milestones.filter((m) => m.milestoneId === milestoneId);
      return arrayMilestoneOne?.length === 1 && arrayMilestoneOne[0];
    }
  },

  countPopMap() {
    return this.links?.contributors?.length ?? 0;
  },
  actionIndicatorCount(status, milestoneId) {
    const { query, options } = actionIndicatorCountQuery({ status, parentId: this._id.valueOf(), parentType: this.scopeVar() });

    if (milestoneId) {
      query['milestone.milestoneId'] = milestoneId;
    }

    return Actions.find(query, options);
  },
  listMilestones(status = 'all', search) {
    if (this.oceco?.milestones?.length > 0) {
      if (status !== 'all' && !search) {
        const arrayMilestones = this.oceco.milestones.filter((m) => m.status === status);
        return arrayMilestones;
      } if (status !== 'all' && search) {
        let arrayMilestones = this.oceco.milestones.filter((m) => m.status === status);
        const globalRegex = new RegExp(`\\b${search}\\b`, 'gi');
        arrayMilestones = arrayMilestones.filter((m) => globalRegex.test(m.name));
        return arrayMilestones;
      } if (status === 'all' && !search) {
        return this.oceco.milestones;
      } if (status === 'all' && search) {
        const globalRegex = new RegExp(`\\b${search}\\b`, 'gi');
        const arrayMilestones = this.oceco.milestones.filter((m) => globalRegex.test(m.name));
        return arrayMilestones;
      }
    }
    return [];
  },
  countMilestones(status = 'all', search) {
    return this.listMilestones(status, search).length;
  },
  isFavorisOceco(orgaId) {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isFavorisOceco('projects', this._id.valueOf(), orgaId);
  },
};

const projectsHelpers = createHelpers(
  commonHelpers.isVisibleFields,
  commonHelpers.isPublicFields,
  commonHelpers.isPrivateFields,
  commonHelpers.documents,
  commonHelpers.rolesLinks,
  commonHelpers.roles,
  commonHelpers.creatorProfile,
  commonHelpers.isCreator,
  commonHelpers.isFavorites,
  commonHelpers.isScope,
  commonHelpers.isScopeAdminPending,
  commonHelpers.isIsInviting,
  commonHelpers.isIsAdminPending,
  commonHelpers.isInviting,
  commonHelpers.InvitingUser,
  commonHelpers.isToBeValidated,
  commonHelpers.isAdminPending,
  commonHelpers.toBeValidated,
  commonHelpers.toBeisInviting,
  commonHelpers.listMembersToBeValidated,
  commonHelpers.listMembersToBeValidatedCount,
  commonHelpers.isFollows,
  commonHelpers.isFollowsMe,
  commonHelpers.listFollows,
  commonHelpers.countFollows,
  commonHelpers.isFollowers,
  commonHelpers.isFollowersMe,
  commonHelpers.listFollowers,
  commonHelpers.countFollowers,
  commonHelpers.listEvents,
  commonHelpers.countEvents,
  commonHelpers.listNotifications,
  commonHelpers.countListNotifications,
  commonHelpers.listNotificationsAsk,
  commonHelpers.isChat,
  commonHelpers.isOneChat,
  commonHelpers.isMultiChat,
  commonHelpers.chatUrl,
  commonHelpers.userActionIndicatorCount,

);

Projects.helpers({ ...projectsHelpers, ...customProjectsHelpers });

