import { moment } from 'meteor/momentjs:moment';

import { Chronos } from '../../client/chronos.js';

export const commonHelpersClient = {
  isStartDate() {
    if (this.startDate) {
      const start = moment(this.startDate).toDate();
      return Chronos.moment(start).isBefore(); // True
    }
    return false;
  },
  isNotStartDate() {
    if (this.startDate) {
      const start = moment(this.startDate).toDate();
      return Chronos.moment().isBefore(start); // True
    }
    return false;
  },
  isEndDate() {
    if (this.endDate) {
      const end = moment(this.endDate).toDate();
      return Chronos.moment(end).isBefore(); // True
    }
    return false;
  },
  isNotEndDate() {
    if (this.endDate) {
      const end = moment(this.endDate).toDate();
      return Chronos.moment().isBefore(end); // True
    }
    return false;
  },
  timeSpentStart() {
    if (this.startDate) {
      return Chronos.moment(this.startDate).fromNow();
    }
    return false;
  },
  timeSpentEnd() {
    if (this.endDate) {
      return Chronos.moment(this.endDate).fromNow();
    }
    return false;
  },
};