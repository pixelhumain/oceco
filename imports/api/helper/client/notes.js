/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

// collection
import { Notes } from '../../collection/notes.js';
import { Citoyens } from '../../collection/citoyens.js';
import { queryLink, queryOptions } from '../../helpers.js';
import { Documents } from '../../collection/documents.js';

Notes.helpers({
  creatorNote() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.creator) });
  },
  isAuthor() {
    return this.creator === Meteor.userId();
  },
  isNotAuthor() {
    return this.creator !== Meteor.userId();
  },
  isShared() {
    // compter le nombre de contributeurs si plus que 1 alors c'est un partage
    return this.contributorsCount > 1;
  },
  isPublic() {
    return this.visibility === 'public';
  },
  isPrivate() {
    return this.visibility === 'private';
  },
  isLock() {
    return this.lockEditing ?? false;
  },
  isToBeValidated(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !((this.links?.contributors?.[bothUserId]?.toBeValidated));
  },
  isIsInviting(scope, scopeId) {
    return !((this.links?.[scope]?.[scopeId]?.isInviting));
  },
  isContributors(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !!(this.links?.contributors?.[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting('contributors', bothUserId));
  },
  updatedNoteLocale() {
    return moment(this.updatedAt).format('L LT');
  },
  listContributors(search) {
    if (this.links?.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  listContributorsUsername(search) {
    if (this.links?.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, { fields: { username: 1 } });
    }
    return false;
  },
  listPresenceContributors() {
    if (this.links?.contributors) {
      // si isPresence existe alors on garde
      const isPresenceArray = Object.keys(this.links.contributors).filter((key) => {
        if (this.links.contributors[key].isPresence) {
          return key;
        }
      }
      ).map((key) => {
        return new Mongo.ObjectID(key);
      });
      return Citoyens.find({ _id: { $in: isPresenceArray } }, { fields: { name: 1 } });
    }
    return false;
  },
  presenceCount() {
    if (this.links?.contributors) {
      // si isPresence existe alors on garde
      const isPresenceArray = Object.keys(this.links.contributors).filter((key) => {
        if (this.links.contributors[key].isPresence) {
          return key;
        }
      });
      return isPresenceArray.length;
    }
    return 0;
  },
  docList() {
    // Fusionner les tableaux this.media.images et this.mediaFile.files s'ils existent
    const combinedIds = [
      ...(this.media?.images || []),
      ...(this.mediaFile?.files || [])
    ];

    // Filtrer les ID qui sont des chaînes et les convertir en Mongo.ObjectID
    const arrayId = combinedIds
      .filter(_id => typeof _id === 'string')
      .map(_id => new Mongo.ObjectID(_id));

    // Retourner les documents trouvés ou undefined si aucun ID valide
    return arrayId.length > 0 ? Documents.find({ _id: { $in: arrayId } }) : undefined;
  },
  lastModifiedByOther() {
    return this.lastModifiedBy && this.lastModifiedBy !== Meteor.userId() ? Citoyens.findOne({ _id: new Mongo.ObjectID(this.lastModifiedBy) }, { fields: { name: 1 } }) : false;
  }
});

