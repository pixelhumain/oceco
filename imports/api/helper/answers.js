/* eslint-disable meteor/no-session */
/* global */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

// schemas
import { Answers } from '../collection/answers.js';
import { Citoyens } from '../collection/citoyens.js';
import { Projects } from '../collection/projects.js';
import { Comments } from '../collection/comments.js';
import { Forms } from '../collection/forms.js';
import { Actions } from '../collection/actions.js';

import { isValidObjectId, scopeElement, createHelpers } from '../helpers.js';

import { commonHelpers } from './commonHelpers.js';

Answers.helpers({
  isEndDate() {
    if (this.endDate) {
      const end = moment(this.endDate).toDate();
      return moment(end).isBefore(); // True
    }
    return false;
  },
  isNotEndDate() {
    if (this.endDate) {
      const end = moment(this.endDate).toDate();
      return moment().isBefore(end); // True
    }
    return false;
  },
});

const customAnswersHelpers = {
  scopeVar() {
    return scopeElement.answers;
  },
  scopeEdit() {
    return 'answersEdit';
  },
  listScope() {
    return 'listAnswers';
  },
  isStart() {
    const start = moment(this.startDate).toDate();
    return moment(start).isBefore(); // True
  },
  formatStartDate() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate).format('L');
  },
  formatEndDate() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate).format('L');
  },
  formatStartDateIso() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate);
  },
  formatEndDateIso() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate);
  },
  duration() {
    const a = moment(this.startDate);
    const b = moment(this.endDate);
    const diffInMs = b.diff(a); // 86400000 milliseconds
    // const diffInDays = b.diff(a, 'days'); // 1 day
    const diffInDayText = moment.duration(diffInMs).humanize();
    return diffInDayText;
  },
  createdFormat() {
    const inputUnix = moment.unix(this.created);
    return moment(inputUnix).format('L LT');
  },
  viewDescription() {
    return this.answers?.aapStep1?.description || this.answers?.aapStep1?.tags;
  },
  projectOne() {
    return this.project?.id ? Projects.findOne({ _id: new Mongo.ObjectID(this.project.id) }) : undefined;
  },
  projectStartDate() {
    const dateTest = moment(this.project?.startDate);
    return dateTest.isValid() ? dateTest.format('L') : moment(this.project?.startDate, 'D/M/YYYY').format('L');
  },
  depenseArray(depenseIndex = null) {
    if (this.answers.aapStep1.depense) {
      const depenseKeys = Object.keys(this.answers.aapStep1.depense);
      const depense = Object.values(this.answers.aapStep1.depense);
      const arrayDepense = depense.filter((d, depenseKey) => depenseIndex === null || parseInt(depenseIndex) === parseInt(depenseKey)).map((d, depenseKey) => {
        d.key = parseInt(depenseKeys[depenseKey]);
        if (d.price) {
          d.price = parseFloat(d.price);
        }
        if (d.date) {
          const dateTest = moment(d.date);
          d.date = dateTest.isValid() ? dateTest.format('L') : moment(d.date, 'D/M/YYYY').format('L');
        }
        d.totalAmount = 0;
        d.nameArray = [];
        // financer
        if (d.financer) {
          const financerKeys = Object.keys(d.financer);
          d.financer = Object.values(d.financer);
          d.financer = d.financer.map((f, financeKey) => {
            f.financeKey = parseInt(financerKeys[financeKey]);
            if (f.amount) {
              f.amount = parseFloat(f.amount);
              d.totalAmount += f.amount;
              const pourcentage = (100 * f.amount) / d.price;
              if (Number.isInteger(pourcentage)) {
                f.pourcentageAmount = pourcentage;
              } else {
                f.pourcentageAmount = parseFloat(pourcentage).toFixed(2);
              }
            }
            if (f.date) {
              const dateTest = moment(f.date);
              f.date = dateTest.isValid() ? dateTest.format('L') : moment(f.date, 'D/M/YYYY').format('L');
            }
            if (f.name) {
              d.nameArray.push(f.name);
            }
            return f;
          });

          if (d.totalAmount === d.price) {
            d.isCompleteFinancer = true;
            d.totalRemaining = 0;
          } else if (d.totalAmount && d.price && d.totalAmount < d.price) {
            d.totalRemaining = d.price - d.totalAmount;
            d.isCompleteFinancer = false;
          } else {
            d.totalRemaining = d.price;
            d.isCompleteFinancer = false;
          }

          d.isFinancer = true;
        } else {
          d.totalRemaining = d.price;
          d.isFinancer = false;
        }
        if (d.price && d.totalAmount) {
          const pourcentage = (100 * d.totalAmount) / d.price;
          if (Number.isInteger(pourcentage)) {
            d.pourcentageAmount = pourcentage;
          } else {
            d.pourcentageAmount = parseFloat(pourcentage).toFixed(2);
          }
        }
        // estimates
        if (d.estimates) {
          d.estimatesKeys = Object.keys(d.estimates);
          d.estimatesKeys = d.estimatesKeys.map((f) => {
            const estimate = {};
            estimate.userId = f;
            if (d.estimates[f].price) {
              estimate.name = d.estimates[f].name;
            }
            if (d.estimates[f].days) {
              estimate.days = parseFloat(d.estimates[f].days);
            }
            if (d.estimates[f].selected === 'true' || d.estimates[f].selected === true) {
              estimate.selected = true;
            }
            if (d.estimates[f].price) {
              estimate.price = parseFloat(d.estimates[f].price);
            }
            if (d.estimates[f].date) {
              const dateTest = moment(d.estimates[f].date);
              estimate.date = dateTest.isValid() ? dateTest.format('L') : moment(d.estimates[f].date, 'D/M/YYYY').format('L');
            }
            return estimate;
          });
          d.isEstimates = true;
        } else {
          d.isEstimates = false;
        }
        // validFinal
        if (d.validFinal && d.validFinal.date) {
          const dateTest = moment(d.validFinal.date);
          d.validFinal.date = dateTest.isValid() ? dateTest.format('L') : moment(d.validFinal.date, 'D/M/YYYY').format('L');
        }
        // worker
        if (d.worker && d.worker.date) {
          const dateTest = moment(d.worker.date);
          d.worker.date = dateTest.isValid() ? dateTest.format('L') : moment(d.worker.date, 'D/M/YYYY').format('L');
        }
        // payement
        d.totalAmountPayement = 0;
        d.isPayement = false;
        if (d.payement) {
          const payementKeys = Object.keys(d.payement);
          d.payement = Object.values(d.payement);
          d.payement = d.payement.map((f, payementKey) => {
            f.payementKey = parseInt(payementKeys[payementKey]);
            if (f.amount) {
              f.amount = parseFloat(f.amount);
              d.totalAmountPayement += f.amount;
              const pourcentage = (100 * f.amount) / d.price;
              if (Number.isInteger(pourcentage)) {
                f.pourcentageAmountPayement = pourcentage;
              } else {
                f.pourcentageAmountPayement = parseFloat(pourcentage).toFixed(2);
              }
            }
            if (f.date) {
              const dateTest = moment(f.date);
              f.date = dateTest.isValid() ? dateTest.format('L') : moment(f.date, 'D/M/YYYY').format('L');
            }
            return f;
          });

          if (d.totalAmountPayement === d.price) {
            d.isCompletePayement = true;
            d.totalRemainingPayement = 0;
          } else if (d.totalAmountPayement && d.price && d.totalAmountPayement < d.price) {
            d.totalRemainingPayement = d.price - d.totalAmountPayement;
            d.isCompletePayement = false;
          } else {
            d.totalRemainingPayement = d.price;
            d.isCompletePayement = false;
          }

          d.isPayement = !!(d.payement && d.payement.length > 0);
        } else {
          d.totalRemainingPayement = d.price;
          d.isCompletePayement = false;
          d.isPayement = false;
        }
        if (d.price && d.totalAmountPayement) {
          const pourcentage = (100 * d.totalAmountPayement) / d.price;
          if (Number.isInteger(pourcentage)) {
            d.pourcentageAmountPayement = pourcentage;
          } else {
            d.pourcentageAmountPayement = parseFloat(pourcentage).toFixed(2);
          }
        }
        // tasks
        if (d.actionid && isValidObjectId(d.actionid)) {
          const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(d.actionid) });
          d.totalCreditTask = 0;
          d.tasks = null;
          if (actionOne && actionOne.tasks) {
            if (actionOne.tasks && actionOne.tasks.length > 0) {
              const tasks = actionOne.tasks.filter((task) => task.userId).map((task) => {
                if (task.userId) {
                  const citoyenAssign = Citoyens.findOne({
                    _id: new Mongo.ObjectID(task.userId),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                  task.citoyenAssign = citoyenAssign;
                }
                if (task.credits) {
                  task.credits = parseFloat(task.credits);
                  d.totalCreditTask += task.credits;
                  const pourcentage = (100 * task.credits) / d.price;
                  if (Number.isInteger(pourcentage)) {
                    task.pourcentageCreditTask = pourcentage;
                  } else {
                    task.pourcentageCreditTask = parseFloat(pourcentage).toFixed(2);
                  }
                }
                if (task.contributors) {
                  //
                }
                return task;
              });
              d.tasks = tasks;
            }
          }
          //
          if (d.totalCreditTask === d.price) {
            d.isCompleteTaskCredit = true;
            d.totalRemainingCredit = 0;
          } else if (d.totalCreditTask && d.price && d.totalCreditTask < d.price) {
            d.totalRemainingCreditTask = d.price - d.totalCreditTask;
            d.isCompleteTaskCredit = false;
          } else {
            d.totalRemainingCreditTask = d.price;
            d.isCompleteTaskCredit = false;
          }

          // d.isPayement = true;
        } else {
          d.totalRemainingCreditTask = d.price;
          d.isCompleteTaskCredit = false;
        }

        return d;
      });

      // console.log(arrayDepense);
      return arrayDepense;
    }
  },
  totalFinance() {
    if (this.answers?.aapStep1?.depense) {
      const depense = Object.values(this.answers.aapStep1.depense);
      const total = {};
      total.totalPrice = 0;
      total.totalAmount = 0;
      total.totalAmountPayement = 0;
      depense.forEach((d) => {
        if (d.price) {
          total.totalPrice += parseFloat(d.price);
        }
        d.nameArray = [];
        if (d.financer) {
          d.financer = Object.values(d.financer);
          d.financer.forEach((f) => {
            if (f.amount) {
              f.amount = parseFloat(f.amount);
              total.totalAmount += f.amount;
            }
          });
        }
        if (d.payement) {
          d.payement = Object.values(d.payement);
          d.payement.forEach((f) => {
            if (f.amount) {
              f.amount = parseFloat(f.amount);
              total.totalAmountPayement += f.amount;
            }
          });
        }
      });
      // console.log(total);
      return total;
    }
  },
  listComments() {
    // console.log('listComments');
    return Comments.find({
      contextId: this._id.valueOf(),
    }, { sort: { created: -1 } });
  },
  commentsCount() {
    if (this.commentCount) {
      return this.commentCount;
    }
    return 0;
  },
  isAdmin(userId) {
    const bothUserId = userId ?? Meteor.userId();
    if (bothUserId && this.form) {
      return Forms.findOne({ _id: new Mongo.ObjectID(this.form) }).isAdmin(bothUserId);
    }
  },
  isEvaluationMe(userId) {
    const bothUserId = userId ?? Meteor.userId();
    if (bothUserId) {
      return this.answers?.aapStep2?.evaluation?.[Meteor.userId()] ?? false;
    }
  },
  totalEvaluation(label = null) {
    if (this.answers?.aapStep2?.evaluation) {
      const evaluation = Object.values(this.answers.aapStep2.evaluation);

      const total = {};
      total.totalArray = [];
      total.nombreVote = evaluation && evaluation.length > 0 ? evaluation.length : 0;
      evaluation.forEach((d) => {
        Object.values(d).forEach((n) => {
          if (n.note) {
            total[n.label] = total[n.label] ? total[n.label] : [];
            total[n.label].push(parseFloat(n.coeff));
            total[n.label].push(5);
            total[n.label].push(parseFloat(n.note));

            total.totalArray.push(parseFloat(n.coeff));
            total.totalArray.push(5);
            total.totalArray.push(parseFloat(n.note));
          }
        });
      });
      const weightedAverage = (input) => {
        const weights = [];
        const values = [];
        let weightedTotal = 0;
        let totalWeight = 0;

        if (input.length % 3 !== 0) {
          return false;
        }

        for (var i = 0; i < input.length; i += 3) {
          weights.push(input[i] * input[i + 1]);
          values.push(input[i + 2]);
        }

        for (var i = 0; i < weights.length; i += 1) {
          weightedTotal += weights[i] * values[i];
          totalWeight += weights[i];
        }

        return weightedTotal / totalWeight;
      };
      if (total.totalArray && total.totalArray.length > 0) {
        const arrayTotal = Object.entries(total);
        total.evaluation = [];
        arrayTotal.forEach((d) => {
          if (d[0] !== 'nombreVote') {
            total[d[0]] = weightedAverage(d[1]).toFixed(2);
            if (d[0] !== 'nombreVote' && d[0] !== 'totalArray') {
              total.evaluation.push({ label: d[0], moyenne: total[d[0]] });
            }
          }
        });
        // console.log(total);
        return label ? total[label] : total;
      }
    }
  },
};

const answersHelpers = createHelpers(
  commonHelpers.creatorProfile,
  commonHelpers.isCreator,
);

Answers.helpers({ ...answersHelpers, ...customAnswersHelpers });

