import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

import { LogUserActions } from '../collection/loguseractions.js';
import { Actions } from '../collection/actions';
import { Citoyens } from '../collection/citoyens.js';
import { Organizations } from '../collection/organizations.js';

LogUserActions.helpers({
  action() {
    return Actions.findOne({ _id: new Mongo.ObjectID(this.actionId) });
  },
  citoyen() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) });
  },
  formatCreatedDate() {
    return moment(this.createdAt).format('L LT');
  },
  maitreId() {
    return this.subOrganizationId ?? this.organizationId;
  },
  subOrganization() {
    return this.subOrganizationId ? Organizations.findOne({ _id: new Mongo.ObjectID(this.subOrganizationId) }) : null;
  },
  organization() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(this.organizationId) });
  },
});
