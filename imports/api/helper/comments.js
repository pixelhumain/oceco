import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { Router } from 'meteor/iron:router';

// collection
import { Comments } from '../collection/comments.js';
import { Citoyens } from '../collection/citoyens.js';

Comments.helpers({
  authorComments() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(this.author) });
  },
  likesCount() {
    return this.voteUp?.voteUpCount || 0;
  },
  dislikesCount() {
    return this.voteDown?.voteDownCount || 0;
  },
  isAuthor() {
    return this.author === Meteor.userId();
  },
  isReportAbuse() {
    return !!(this.reportAbuse?.[Meteor.userId()]);
  },
});
