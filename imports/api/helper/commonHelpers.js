/* eslint-disable meteor/no-session */
/* eslint-disable consistent-return */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';

// collection
import { Citoyens } from '../collection/citoyens.js';
import { Documents } from '../collection/documents.js';
import { Events } from '../collection/events.js';
import { Actions } from '../collection/actions.js';
import { ActivityStream } from '../collection/activitystream.js';

import {
  queryLink, arrayLinkToBeValidated, queryLinkToBeValidated, queryOptions, customLinksOrga, scopeElementLink, actionIndicatorCountQuery
} from '../helpers.js';


export const commonHelpers = {
  isVisibleFields(field) {
    return true;
  },
  isPublicFields(field) {
    return !!this.preferences?.publicFields?.includes(field);
  },
  isPrivateFields(field) {
    return !!this.preferences?.privateFields?.includes(field);
  },
  documents() {
    return Documents.find({
      id: this._id.valueOf(),
      contentKey: 'profil',
    }, { sort: { created: -1 }, limit: 1 });
  },
  rolesLinks(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return this.links?.[scopeCible]?.[scopeId]?.roles?.join(',');
  },
  roles(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return this.links?.[scopeCible]?.[scopeId]?.roles?.join(',');
  },
  rolesCitoyen(scope, scopeId) {
    const scopeCible = customLinksOrga[scope] ?? scope;
    return this.links?.[scopeCible]?.[scopeId]?.roles?.join(',');
  },
  creatorProfile() {
    // collection anwser creator is user
    const creator = this.creator || this.user;
    return creator ? Citoyens.findOne({ _id: new Mongo.ObjectID(this.creator) }) : null;
  },
  isCreator() {
    // collection anwser creator is user
    const creator = this.creator || this.user;
    return creator === Meteor.userId();
  },
  isFavorites(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return !!Citoyens.findOne({ _id: new Mongo.ObjectID(bothUserId) })?.isFavorites?.(this.scopeVar(), this._id.valueOf());
  },
  isScope(scope, scopeId) {
    return !!((this.links?.[scope]?.[scopeId]?.type && this.isIsInviting(scope, scopeId)));
  },
  isScopeAdminPending(scope, scopeId) {
    return !!((this.links?.[scope]?.[scopeId]?.type && this.isIsAdminPending(scope, scopeId)));
  },
  isIsInviting(scope, scopeId) {
    return !((this.links?.[scope]?.[scopeId]?.isInviting));
  },
  isIsAdminPending(scope, scopeId) {
    return !((this.links?.[scope]?.[scopeId]?.isAdminPending));
  },
  isInviting(scope, scopeId) {
    return !!((this.links?.[scope]?.[scopeId]?.isInviting));
  },
  InvitingUser(scope, scopeId) {
    return this.links?.[scope]?.[scopeId];
  },
  isToBeValidated(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const linksScope = scopeElementLink[this.scopeVar()];
    return !(this.links?.[linksScope]?.[bothUserId]?.toBeValidated);
  },
  isAdminPending(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const linksScope = scopeElementLink[this.scopeVar()];
    return this.isIsAdminPending(linksScope, bothUserId);
  },
  toBeValidated(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const linksScope = scopeElementLink[this.scopeVar()];
    return !!((this.links?.[linksScope]?.[bothUserId]?.toBeValidated));
  },
  toBeisInviting(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const linksScope = scopeElementLink[this.scopeVar()];
    return this.isInviting(linksScope, bothUserId);
  },
  listMembersToBeValidated() {
    const linksScope = scopeElementLink[this.scopeVar()];
    const members = this.links?.[linksScope];
    if (members) {
      const query = queryLinkToBeValidated(members);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  listMembersToBeValidatedCount() {
    const linksScope = scopeElementLink[this.scopeVar()];
    const members = this.links?.[linksScope];
    if (members) {
      const array = arrayLinkToBeValidated(members);
      return array?.length || 0;
    }
    return false;
  },
  isFollows(followId) {
    return !!((this.links?.follows?.[followId]));
  },
  isFollowsMe() {
    return !!((this.links?.follows?.[Meteor.userId()]));
  },
  listFollows(search) {
    const follows = this.links?.follows;
    if (follows) {
      const query = queryLink(follows, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countFollows(search) {
    return this.listFollows(search)?.count();
  },
  isFollowers(followId) {
    return !!((this.links?.followers?.[followId]));
  },
  isFollowersMe() {
    return !!((this.links?.followers?.[Meteor.userId()]));
  },
  listFollowers(search) {
    const followers = this.links?.followers;
    if (followers) {
      const query = queryLink(followers, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  countFollowers(search) {
    return this.listFollowers(search)?.count();
  },
  listEvents(search) {
    const events = this.links?.events;
    if (events) {
      const query = queryLink(events, search);
      return Events.find(query, queryOptions);
    }
    return false;
  },
  countEvents(search) {
    return this.listEvents(search)?.count();
  },
  listNotifications(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return ActivityStream.api.isUnseen(bothUserId, this._id.valueOf(), this.scopeVar());
  },
  countListNotifications(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return this.listNotifications(bothUserId).count();
  },
  listNotificationsAsk(userId) {
    const bothUserId = userId ?? Meteor.userId();
    return ActivityStream.api.isUnseenAsk(bothUserId, this._id.valueOf(), this.scopeVar());
  },
  isChat() {
    return (this.tools?.chat?.int?.length ?? 0) > 0;
  },
  isOneChat() {
    return (this.tools?.chat?.int?.length ?? 0) === 1;
  },
  isMultiChat() {
    return (this.tools?.chat?.int?.length ?? 0) > 1;
  },
  chatUrl() {
    const hostChat = Meteor.settings.public.rocketchat.host ?? false;
    return this.tools?.chat?.int?.[0]?.url ? `${hostChat}${this.tools.chat.int[0].url}` : undefined;
  },
  actionIndicatorCount(status) {
    const { query, options } = actionIndicatorCountQuery({ status, parentId: this._id.valueOf(), parentType: this.scopeVar() });
    return Actions.find(query, options);
  },
  userActionIndicatorCount(status, userId) {
    const query = {
      parentId: this._id.valueOf(),
      parentType: this.scopeVar(),
      [`links.contributors.${userId}`]: { $exists: true }
    };

    if (status === 'inProgress') {
      query.status = 'todo';
      // query[`finishedBy.${userId}`] = { $exists: false };
      query[`finishedBy`] = {
        $not: {
          $elemMatch: {
            userId: userId
          }
        }
      };
    } else if (status === 'toModerate') {
      query.status = 'todo';
      // query[`finishedBy.${userId}`] = 'toModerate';
      query['finishedBy'] = {
        $elemMatch: {
          userId: userId,
          status: 'toModerate'
        }
      };
    } else if (status === 'validated') {
      // query[`finishedBy.${userId}`] = 'validated';
      query['finishedBy'] = {
        $elemMatch: {
          userId: userId,
          status: 'validated'
        }
      };
    } else if (status === 'novalidated') {
      // query[`finishedBy.${userId}`] = 'novalidated';
      query['finishedBy'] = {
        $elemMatch: {
          userId: userId,
          status: 'novalidated'
        }
      };
    } else if (status === 'all') {
      // query.$or = [
      //   { status: 'todo', [`finishedBy.${userId}`]: { $exists: false } },
      //   { status: 'todo', [`finishedBy.${userId}`]: 'toModerate' },
      //   { [`finishedBy.${userId}`]: 'validated' },
      //   { [`finishedBy.${userId}`]: 'novalidated' }
      // ];
      query.$or = [
        {
          status: 'todo', finishedBy: {
            $not: {
              $elemMatch: {
                userId: userId
              }
            }
          }
        },
        {
          status: 'todo', finishedBy: {
            $elemMatch: {
              userId: userId,
              status: 'toModerate'
            }
          }
        },
        {
          finishedBy: {
            $elemMatch: {
              userId: userId,
              status: { $in: ['validated', 'novalidated'] }
            }
          }
        }
      ];
    }

    return Actions.find(query);
  },
};