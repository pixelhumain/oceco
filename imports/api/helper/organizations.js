/* eslint-disable meteor/no-session */
/* eslint-disable consistent-return */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { _ } from 'meteor/underscore';
import { Router } from 'meteor/iron:router';
import { moment } from 'meteor/momentjs:moment';

import { remove as removeAccent } from 'remove-accents';

// collection
import { Organizations } from '../collection/organizations.js';
import { Lists } from '../collection/lists.js';
import { Citoyens } from '../collection/citoyens.js';
import { Events } from '../collection/events.js';
import { Projects } from '../collection/projects.js';
import { Rooms } from '../collection/rooms.js';
import { Actions } from '../collection/actions.js';
import { Forms } from '../collection/forms.js';
import { LogUserActions } from '../collection/loguseractions.js';

import {
  searchQuery, searchQuerySort, queryLink, queryLinkParent, queryLinkType, queryLinkIsAdmin, arrayLinkParent, arrayLinkParentNoObject, queryOptions, applyDiacritics, queryOrPrivatescopeIds, queryLinkTypeInvite, scopeElement, scopeElementLink, createHelpers, optionsNoFieldsScope, statusNoVisible,
  searchQueryNotes
} from '../helpers.js';


import { commonHelpers } from './commonHelpers.js';
import { Notes } from '../collection/notes.js';

const customOrganizationsHelpers = {
  isScopeMe() {
    return this.isMembers();
  },
  isAdmin(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const linksScope = scopeElementLink[this.scopeVar()];
    return !!((this.links?.[linksScope]?.[bothUserId]?.isAdmin && this.isToBeValidated(bothUserId) && this.isAdminPending(bothUserId) && this.isIsInviting(scopeElementLink[this.scopeVar()], bothUserId)));
  },
  scopeVar() {
    return scopeElement.organizations;
  },
  scopeEdit() {
    return 'organizationsEdit';
  },
  listScope() {
    return 'listOrganizations';
  },
  isMembers(userId) {
    const bothUserId = userId ?? Meteor.userId();
    const linksScope = scopeElementLink[this.scopeVar()];
    return !!(this.links?.[linksScope]?.[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting(scopeElementLink[this.scopeVar()], bothUserId));
  },

  listProjects(search) {
    const projects = this.links?.projects;
    if (projects) {
      const query = queryLink(projects, search);
      query[`parent.${this._id.valueOf()}`] = { $exists: true };
      queryOptions.fields.parent = 1;
      return Projects.find(query, queryOptions);
    }
    return false;
  },
  countProjects(search) {
    return this.listProjects(search)?.count() || 0;
  },
  listForms() {
    // type: "aap"
    // subType: "ocecoform"
    // active: true
    const queryProjectId = `parent.${this._id.valueOf()}`;
    return Forms.find({ [queryProjectId]: { $exists: true }, type: 'aap', subType: 'ocecoform' });
  },
  countForms() {
    return this.listForms()?.count() || 0;
  },
  listRooms(search) {
    if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {
      const query = {};

      if (this.isAdmin()) {
        if (Meteor.isClient && search) {
          query.parentId = this._id.valueOf();
          query.name = { $regex: search, $options: 'i' };
          query.status = 'open';
        } else {
          query.parentId = this._id.valueOf();
          query.status = 'open';
        }
      } else {
        query.$or = [];
        const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
        if (roles) {
          if (Meteor.isClient && search) {
            query.$or.push({
              parentId: this._id.valueOf(), name: { $regex: search, $options: 'i' }, status: 'open', roles: { $exists: true, $in: roles },
            });
          } else {
            query.$or.push({ parentId: this._id.valueOf(), status: 'open', roles: { $exists: true, $in: roles } });
          }
        }
        if (Meteor.isClient && search) {
          query.$or.push({
            parentId: this._id.valueOf(), name: { $regex: search, $options: 'i' }, status: 'open', roles: { $exists: false },
          });
        } else {
          query.$or.push({ parentId: this._id.valueOf(), status: 'open', roles: { $exists: false } });
        }
      }

      queryOptions.fields.parentId = 1;
      queryOptions.fields.parentType = 1;
      queryOptions.fields.status = 1;
      queryOptions.fields.roles = 1;
      return Rooms.find(query, queryOptions);
    }
  },
  detailRooms(roomId) {
    // eslint-disable-next-line no-empty
    if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).isScope(this.scopeVar(), this._id.valueOf())) {

    }
    const query = {};
    if (this.isAdmin()) {
      query._id = new Mongo.ObjectID(roomId);
      query.status = 'open';
    } else {
      query.$or = [];
      const roles = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).funcRoles(this.scopeVar(), this._id.valueOf()).split(',') : null;
      if (roles) {
        query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: true, $in: roles } });
      }
      query.$or.push({ _id: new Mongo.ObjectID(roomId), status: 'open', roles: { $exists: false } });
    }
    // console.log(query);
    return Rooms.find(query);
  },
  countRooms(search) {
    return this.listRooms(search) && this.listRooms(search).count();
  },
  room(roomId) {
    const paramRoomId = roomId || Router.current().params.roomId;
    return Rooms.findOne({ _id: new Mongo.ObjectID(paramRoomId) });
  },
  listActionsCreator(type = 'all', status = 'todo', search = null, searchSort = null, limit = null) {
    const query = {};
    const inputDate = new Date();

    // all
    if (search && !search.hash && search.startsWith('all:')) {
      status = 'all';
    }

    let queryone = {};

    if (search && !search.hash) {
      queryone = searchQuery(queryone, search);
    }

    if (status === 'todo' && !queryone._id && !queryone.numberId) {
      queryone.endDate = { $exists: true, $gte: inputDate };
    } else {
      queryone.endDate = { $exists: true };
    }
    queryone.parentId = { $in: [this._id.valueOf()] };

    if (status !== 'all' && !queryone._id && !queryone.numberId) {
      queryone.status = status;
    }

    let querytwo = {};
    querytwo.endDate = { $exists: false };
    querytwo.parentId = { $in: [this._id.valueOf()] };

    if (search && !search.hash) {
      querytwo = searchQuery(querytwo, search);
    }

    if (status !== 'all' && !querytwo._id && !querytwo.numberId) {
      querytwo.status = status;
    }

    if (type === 'aFaire') {
      queryone.$or = [
        { credits: { $gt: 0 } }, { 'options.creditAddPorteur': true },
      ];
      querytwo.$or = [
        { credits: { $gt: 0 } }, { 'options.creditAddPorteur': true },
      ];
      // queryone.credits = { $gt: 0 };
      // querytwo.credits = { $gt: 0 };
    } else if (type === 'depenses') {
      queryone.credits = { $lte: 0 };
      querytwo.credits = { $lte: 0 };
    }

    const queryoneAnd = {};
    queryoneAnd.$and = [];
    Object.keys(queryone).forEach((key) => {
      queryoneAnd.$and.push({ [key]: queryone[key] });
    });

    const querytwoAnd = {};
    querytwoAnd.$and = [];
    Object.keys(querytwo).forEach((key) => {
      querytwoAnd.$and.push({ [key]: querytwo[key] });
    });

    query.$or = [];
    query.$or.push(queryoneAnd);
    query.$or.push(querytwoAnd);

    const options = {};

    if (searchSort && !searchSort.hash) {
      const arraySort = searchQuerySort('actions', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      }
    }

    if (limit && !limit.hash) {
      options.limit = limit;
    }

    return Actions.find(query, options);
  },
  countActionsCreator(type = 'all', status = 'todo', search) {
    return this.listActionsCreator(type, status, search) && this.listActionsCreator(type, status, search).count();
  },
  listMembers(search = null, otherId = null, limit = null) {
    if (this.links?.members) {
      const orgId = this.maitre() ? this.maitre()._id.valueOf() : this._id.valueOf();
      const query = queryLinkType(this.links.members, search, 'citoyens');
      const options = {};
      options.sort = {};
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.memberOf.${this._id.valueOf()}`] = 1;
      if (otherId) {
        options.fields[`links.memberOf.${otherId}`] = 1;
        options.fields[`userWallet.${otherId}.userCredits`] = 1;
      }
      options.fields.name = 1;
      options.fields.username = 1;
      options.fields[`userWallet.${orgId}.userCredits`] = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }
      // console.log(Citoyens.find(query, options).count());
      return Citoyens.find(query, options);
    }
    return false;
  },
  listMembersInvite(search, otherId) {
    if (this.links?.members) {
      const orgId = this.maitre() ? this.maitre()._id.valueOf() : this._id.valueOf();
      const query = queryLinkTypeInvite(this.links.members, search, 'citoyens');
      const options = {};
      options.sort = {};
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.memberOf.${this._id.valueOf()}`] = 1;
      if (otherId) {
        options.fields[`links.memberOf.${otherId}`] = 1;
        options.fields[`userWallet.${otherId}.userCredits`] = 1;
      }
      options.fields.name = 1;
      options.fields[`userWallet.${orgId}.userCredits`] = 1;
      options.fields.profilThumbImageUrl = 1;
      return Citoyens.find(query, options);
    }
    return false;
  },
  listMembersDash(search) {
    if (this.links?.members) {
      const arrayDashUser = (this.oceco && this.oceco.dashboardUser && this.oceco.dashboardUser.length > 0) ? this.oceco.dashboardUser : null;
      const query = queryLinkType(this.links.members, search, 'citoyens');
      const options = {};
      options.transform = (item) => {
        item.assignDash = arrayDashUser && arrayDashUser.includes(item._id.valueOf()) ? 1 : 0;
        return item;
      };
      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.memberOf.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;
      return Citoyens.find(query, options);
    }
    return false;
  },
  listMembersActions(actionId, search = null, limit = null) {
    if (this.links?.members) {
      const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId) });
      const query = queryLinkType(this.links.members, search, 'citoyens');
      const options = {};
      options.transform = (item) => {
        item.assign = actionOne && actionOne.isContributors(item._id.valueOf()) ? 1 : 0;
        return item;
      };
      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.memberOf.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  listMembersTaskActions(actionId, taskId, search = null, limit = null) {
    if (this.links?.members) {
      const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId) });

      const query = queryLinkType(this.links.members, search, 'citoyens');
      const options = {};

      options.transform = (item) => {
        const task = actionOne && actionOne.tasks ? actionOne.tasks.filter((k) => k.taskId === taskId) : null;
        item.assign = task && task[0] && task[0].contributors && task[0].contributors[item._id.valueOf()] ? 1 : 0;
        return item;
      };


      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.memberOf.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  listMembersNotes(docId, search = null, limit = null) {
    if (this.links?.members) {
      const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
      const query = queryLinkType(this.links.members, search, 'citoyens');
      const options = {};
      options.transform = (item) => {
        item.assign = noteOne && noteOne.isContributors(item._id.valueOf()) ? 1 : 0;
        return item;
      };
      options.sort = {};
      options.sort.assign = -1;
      options.sort.name = 1;
      options.fields = {};
      options.fields[`links.memberOf.${this._id.valueOf()}`] = 1;
      options.fields.name = 1;
      options.fields.assign = 1;
      options.fields.profilThumbImageUrl = 1;

      if (limit && !limit.hash) {
        options.limit = limit;
      }

      return Citoyens.find(query, options);
    }
    return false;
  },
  countMembers(search) {
    return this.listMembers(search) && this.listMembers(search).count();
  },
  listMembersOrganizations(search, selectorga) {
    if (this.links?.members) {
      const query = queryLinkType(this.links.members, search, 'organizations', selectorga);
      return Organizations.find(query, queryOptions);
    }
    return false;
  },
  countMembersOrganizations(search, selectorga) {
    return this.listMembersOrganizations(search, selectorga) && this.listMembersOrganizations(search, selectorga).count();
  },
  listProjectsCreatorAdmin(search) {
    if (this.links?.projects) {
      const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
      const query = queryLinkIsAdmin(userC.links.projects, this.links.projects, search);
      query[`parent.${this._id.valueOf()}`] = { $exists: true };
      queryOptions.fields.parent = 1;
      queryOptions.fields.modified = 1;

      const arrayIds = Projects.find(query, queryOptions).fetch().filter((project) => {
        const arrayParent = Object.keys(project.parent).filter((k) => project.parent[k].type === 'organizations' && k === this._id.valueOf());
        return arrayParent['0'] === this._id.valueOf();
      }).map((project) => project._id);

      // console.log(arrayIds);
      return Projects.find({ _id: { $in: arrayIds } }, queryOptions);
    }
  },
  countProjectsCreatorAdmin(search) {
    return this.listProjectsCreatorAdmin(search) && this.listProjectsCreatorAdmin(search).count();
  },
  listProjectsCreator(search) {
    if (this.links?.projects) {
      /* const projectIds = arrayLinkParent(this.links.projects, 'projects');
      const query = {};
      query._id = {
        $in: projectIds,listProjectsCreatorAdmin
      }; */

      // const query = queryLink(this.links.projects, search);
      const query = queryLinkParent(this._id.valueOf(), search);
      queryOptions.fields.modified = 1;
      queryOptions.fields.parent = 1;
      // Todo il faut un param dans parent de projects pour identifier l'orga prioritaire
      // parent.${this._id.valueOf()}.ocecoMain = true
      const arrayProjectParent = Projects.find(query, queryOptions).fetch();
      const arrayIds = arrayProjectParent.filter((project) => {
        const arrayParent = Object.keys(project.parent).filter((k) => project.parent[k].type === 'organizations' && k === this._id.valueOf());
        return arrayParent['0'] === this._id.valueOf();
      }).map((project) => project._id);

      let queryArray = {};
      queryArray.$or = [];
      if (this.isAdmin()) {
        queryArray.$or.push({
          _id: { $in: arrayIds }
        });
      } else {
        queryArray.$or.push({
          _id: { $in: arrayIds },
          'preferences.private': false,
        });
        queryArray.$or.push({
          _id: { $in: arrayIds },
          'preferences.private': {
            $exists: false,
          },
        });

        queryArray = queryOrPrivatescopeIds(queryArray, 'contributors', arrayIds, Meteor.userId());
      }

      return Projects.find(queryArray, queryOptions);
    }
  },
  countProjectsCreator(search) {
    return this.listProjectsCreator(search) && this.listProjectsCreator(search).count();
  },
  listOrganizationsCreator(search) {
    const query = queryLinkParent(this._id.valueOf(), search);
    queryOptions.fields.modified = 1;
    queryOptions.fields.parent = 1;
    if (this.isAdmin) {
      query.$or = [];
      const queryAnd = {};
      queryAnd[`links.members.${Meteor.userId()}.isAdmin`] = true;
      queryAnd[`links.members.${Meteor.userId()}.isAdminPending`] = { $exists: false };
      queryAnd[`links.members.${Meteor.userId()}.toBeValidated`] = { $exists: false };
      queryAnd[`links.members.${Meteor.userId()}.isInviting`] = { $exists: false };
      query.$or.push(queryAnd);
      query.$or.push({
        oceco: { $exists: true },
      });
    } else {
      query.oceco = { $exists: true };
    }
    // Todo il faut un param dans parent de projects pour identifier l'orga prioritaire
    // parent.${this._id.valueOf()}.ocecoMain = true
    const arrayIds = Organizations.find(query, queryOptions).fetch().filter((organization) => {
      const arrayParent = Object.keys(organization.parent);
      return arrayParent['0'] === this._id.valueOf();
    }).map((organization) => organization._id);
    const options = {};
    const { fields } = optionsNoFieldsScope('organizations');
    options.fields = fields;
    return Organizations.find({ _id: { $in: arrayIds } }, options);
  },
  countOrganizationsCreator(search) {
    return this.listOrganizationsCreator(search) && this.listOrganizationsCreator(search).count();
  },
  listEventsCreator() {
    if (this.links?.events) {
      const eventsIds = arrayLinkParent(this.links.events, 'events');
      const query = {};
      query._id = {
        $in: eventsIds,
      };
      query.status = { $nin: statusNoVisible };
      queryOptions.fields.startDate = 1;
      queryOptions.fields.startDate = 1;
      queryOptions.fields.geo = 1;
      return Events.find(query, queryOptions);
    }
  },
  countEventsCreator() {
    return this.listEventsCreator() && this.listEventsCreator().count();
  },
  listProjectsEventsCreator(querySearch, inputDateStart, inputDateEnd) {
    if (this.links?.projects) {
      const projectIds = arrayLinkParent(this.links.projects, 'projects');
      // verifier que les projets sont parent
      // mais pas tester si le premier de l'objet car c'est pas un array
      const parentField = `parent.${this._id.valueOf()}`;
      const projectsParents = Projects.find({ _id: { $in: projectIds }, [parentField]: { $exists: true } }).fetch();
      const projectParentIds = projectsParents.filter((project) => {
        const arrayParent = Object.keys(project.parent).filter((k) => project.parent[k].type === 'organizations' && k === this._id.valueOf());
        return arrayParent['0'] === this._id.valueOf();
      }).map((project) => project._id.valueOf());

      const query = querySearch || {};
      query.$or = [];
      projectParentIds.forEach((id) => {
        const queryCo = {};
        queryCo[`organizer.${id}`] = { $exists: true };
        query.$or.push(queryCo);
      });
      // queryOptions.fields.parentId = 1;

      // eslint-disable-next-line no-param-reassign
      inputDateStart = inputDateStart || new Date();
      // query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDateStart };
      if (inputDateEnd) {
        query.startDate = { $lte: inputDateEnd };
      }
      query.status = { $nin: statusNoVisible };
      const options = {};
      options.fields = { _id: 1, name: 1, startDate: 1, endDate: 1, organizer: 1, status: 1 };
      options.sort = {
        startDate: 1,
      };
      return Events.find(query, options);
    }
  },
  listProjectsEventsCreatorOld(querySearch, inputDate) {
    if (this.links?.projects) {
      const projectIds = arrayLinkParentNoObject(this.links.projects, 'projects');
      const query = querySearch || {};
      query.$or = [];
      projectIds.forEach((id) => {
        const queryCo = {};
        queryCo[`organizer.${id}`] = { $exists: true };
        query.$or.push(queryCo);
      });
      // queryOptions.fields.parentId = 1;

      // eslint-disable-next-line no-param-reassign
      inputDate = inputDate || new Date();
      // query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDate };
      query.status = { $nin: statusNoVisible };
      const options = {};
      options.sort = {
        startDate: 1,
      };
      return Events.find(query, options);
    }
  },
  listProjectsEventsCreatorAdmin(querySearch) {
    if (this.links?.projects) {
      const projectIds = arrayLinkParentNoObject(this.links.projects, 'projects');
      const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
      const query = querySearch || {};
      query.$or = [];
      projectIds.forEach((id) => {
        const queryCo = {};
        if (userC && userC.links && userC.links.projects && userC.links.projects[id] && userC.links.projects[id].isAdmin && !userC.links.projects[id].toBeValidated && !userC.links.projects[id].isAdminPending && !userC.links.projects[id].isInviting) {
          queryCo[`organizer.${id}`] = { $exists: true };
          query.$or.push(queryCo);
        }
      });
      if (query.$or.length === 0) {
        delete query.$or;
      }
      // queryOptions.fields.parentId = 1;
      const inputDate = new Date();
      // query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDate };
      const options = {};
      options.sort = {
        startDate: 1,
      };
      return Events.find(query, options);
    }
  },
  countProjectsEventsCreatorAdmin() {
    return this.listProjectsEventsCreatorAdmin() && this.listProjectsEventsCreatorAdmin().count();
  },

  /*
  WARNING j'ai du créer listProjectsEventsCreator1M pour rajouter un delai de visibiliter 15 jours
  des actions lier au evenement pour pouvoir valider apres la fin
  */
  listProjectsEventsCreator1M(querySearch) {
    if (this.links?.projects) {
      const projectIds = arrayLinkParentNoObject(this.links.projects, 'projects');
      // const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
      const query = querySearch || {};
      query.$or = [];

      projectIds.forEach((id) => {
        const queryCo = {};
        // if (userC && userC.links && userC.links.projects && userC.links.projects[id] && userC.links.projects[id].isAdmin && !userC.links.projects[id].toBeValidated && !userC.links.projects[id].isAdminPending && !userC.links.projects[id].isInviting) {
        queryCo[`organizer.${id}`] = { $exists: true };
        query.$or.push(queryCo);
        // }
      });
      if (query.$or.length === 0) {
        delete query.$or;
      }

      // queryOptions.fields.parentId = 1;
      // const inputDate = new Date();
      const inputDate = moment(new Date()).subtract(15, 'day').toDate();
      // query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDate };

      const options = {};
      options.sort = {
        startDate: -1,
      };
      // console.log(query);
      return Events.find(query, options);
    }
  },
  countProjectsEventsCreator() {
    return this.listProjectsEventsCreator() && this.listProjectsEventsCreator().count();
  },

  listProjectsEventsCreatorAdmin1M(querySearch) {
    if (this.links?.projects) {
      // const projectIds = arrayLinkParentNoObject(this.links.projects, 'projects');

      const queryProject = queryLinkParent(this._id.valueOf());
      const projectList = Projects.find(queryProject, { fields: { _id: 1, parent: 1 } });
      const projectIds = projectList.map((project) => project._id.valueOf());

      const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
      const query = querySearch || {};
      query.$or = [];

      projectIds.forEach((id) => {
        const queryCo = {};
        if (userC && userC.links && userC.links.projects && userC.links.projects[id] && userC.links.projects[id].isAdmin && !userC.links.projects[id].toBeValidated && !userC.links.projects[id].isAdminPending && !userC.links.projects[id].isInviting) {
          queryCo[`organizer.${id}`] = { $exists: true };
          query.$or.push(queryCo);
        }
      });
      if (query.$or.length === 0) {
        delete query.$or;
      }

      // queryOptions.fields.parentId = 1;
      // const inputDate = new Date();
      const inputDate = moment(new Date()).subtract(15, 'day').toDate();
      // query.startDate = { $lte: inputDate };
      query.endDate = { $gte: inputDate };
      query.status = { $nin: statusNoVisible };

      query.parent = { $exists: false };
      const options = {};
      options.sort = {
        startDate: -1,
      };
      return Events.find(query, options);
    }
  },
  countProjectsEventsCreatorAdmin1M() {
    return this.listProjectsEventsCreatorAdmin1M() && this.listProjectsEventsCreatorAdmin1M().count();
  },
  listProjectsEventsActionsCreator(status = 'todo', limit) {
    const query = {};
    const listEvents = this.listProjectsEventsCreator1M();

    if (Meteor.isClient) {
      if (Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)) {
        const listProjects = this.listProjects();

        const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
        const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
        const mergeArray = [...eventIds, ...projectIds, this._id.valueOf()];

        query.parentId = {
          $in: mergeArray,
        };
      } else {
        const listProjects = this.listProjectsCreatorAdmin();
        const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
        const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
        const mergeArray = [...eventIds, ...projectIds];

        query.parentId = {
          $in: mergeArray,
        };
      }
    } else if (this.isAdmin()) {
      const listProjects = this.listProjects();
      const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
      const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
      const mergeArray = [...eventIds, ...projectIds, this._id.valueOf()];
      query.parentId = {
        $in: mergeArray,
      };
    } else {
      const listProjects = this.listProjectsCreatorAdmin();
      const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
      const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
      const mergeArray = [...eventIds, ...projectIds];
      query.parentId = {
        $in: mergeArray,
      };
    }

    if (status === 'todo') {
      query.status = 'todo';
    } else if (status === 'done') {
      query.status = 'done';
    }

    const options = {};
    options.sort = {
      created: -1,
    };

    if (limit) {
      options.limit = limit;
    }
    return Actions.find(query, options);
  },
  countProjectsEventsActionsCreator() {
    return this.listProjectsEventsActionsCreator() && this.listProjectsEventsActionsCreator().count();
  },
  listProjectsEventsActionsCreatorAdmin(status = 'todo', limit) {
    const query = {};
    const listEvents = this.listProjectsEventsCreatorAdmin1M();
    if (Meteor.isClient) {
      if (Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)) {
        const listProjects = this.listProjects();
        const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
        const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
        const mergeArray = [...eventIds, ...projectIds, this._id.valueOf()];

        query.parentId = {
          $in: mergeArray,
        };
      } else {
        const listProjects = this.listProjectsCreatorAdmin();
        const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
        const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
        const mergeArray = [...eventIds, ...projectIds];

        query.parentId = {
          $in: mergeArray,
        };
      }
    } else if (this.isAdmin()) {
      const listProjects = this.listProjects();
      const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
      const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
      const mergeArray = [...eventIds, ...projectIds, this._id.valueOf()];
      query.parentId = {
        $in: mergeArray,
      };
    } else {
      const listProjects = this.listProjectsCreatorAdmin();
      const eventIds = listEvents ? listEvents.map((event) => event._id.valueOf()) : [];
      const projectIds = listProjects ? listProjects.map((project) => project._id.valueOf()) : [];
      const mergeArray = [...eventIds, ...projectIds];
      query.parentId = {
        $in: mergeArray,
      };
    }

    if (status === 'todo') {
      query.status = 'todo';
    } else if (status === 'done') {
      query.status = 'done';
    }

    const options = {};
    options.sort = {
      created: -1,
    };

    if (limit) {
      options.limit = limit;
    }
    return Actions.find(query, options);
  },
  countProjectsEventsActionsCreatorAdmin() {
    return this.listProjectsEventsActionsCreatorAdmin() && this.listProjectsEventsActionsCreatorAdmin().count();
  },
  actionsInWaiting(search, searchSort) {
    const finished = `finishedBy.${Meteor.userId()}`;
    const UserId = `links.contributors.${Meteor.userId()}`;
    const raffEventsArray = this.listProjectsEventsCreator1M() ? this.listProjectsEventsCreator1M().map((event) => event._id.valueOf()) : [];

    let raffProjectsArray;
    if (search && search.charAt(0) === ':' && search.length > 1) {
      raffProjectsArray = this.listProjects(search) ? this.listProjects(search).map((project) => project._id.valueOf()) : [];
    } else {
      raffProjectsArray = this.listProjects() ? this.listProjects().map((project) => project._id.valueOf()) : [];
    }

    const mergeArray = [...raffEventsArray, ...raffProjectsArray, this._id.valueOf()];

    let query = {
      [UserId]: { $exists: 1 }, [finished]: { $exists: false }, parentId: { $in: mergeArray }, status: 'todo',
    };
    if (Meteor.isClient) {
      if (search && search.charAt(0) !== ':') {
        if (search) {
          query = searchQuery(query, search);
        }
      }
    }

    const options = {};
    if (Meteor.isClient) {
      if (searchSort) {
        const arraySort = searchQuerySort('actions', searchSort);
        if (arraySort) {
          options.sort = arraySort;
        }
      }
    } else {
      options.sort = {
        startDate: 1,
      };
    }

    return Actions.find(query, options);
  },
  actionsToValidate() {
    const finished = `finishedBy.${Meteor.userId()}`;
    const UserId = `links.contributors.${Meteor.userId()}`;
    const raffEventsArray = this.listProjectsEventsCreator1M() ? this.listProjectsEventsCreator1M().map((event) => event._id.valueOf()) : [];
    const raffProjectsArray = this.listProjects() ? this.listProjects().map((project) => project._id.valueOf()) : [];
    const mergeArray = [...raffEventsArray, ...raffProjectsArray, this._id.valueOf()];
    return Actions.find({
      [UserId]: { $exists: 1 }, [finished]: 'toModerate', parentId: { $in: mergeArray }, status: 'todo',
    });
  },
  actionsValidate() {
    const finished = `finishedBy.${Meteor.userId()}`;
    const UserId = `links.contributors.${Meteor.userId()}`;
    const raffEventsArray = this.listProjectsEventsCreator1M() ? this.listProjectsEventsCreator1M().map((event) => event._id.valueOf()) : [];
    const raffProjectsArray = this.listProjects() ? this.listProjects().map((project) => project._id.valueOf()) : [];
    const mergeArray = [...raffEventsArray, ...raffProjectsArray, this._id.valueOf()];
    return Actions.find({
      [UserId]: { $exists: 1 }, [finished]: 'validated', credits: { $gt: 0 }, parentId: { $in: mergeArray },
    });
  },
  actionsSpend() {
    const finished = `finishedBy.${Meteor.userId()}`;
    const UserId = `links.contributors.${Meteor.userId()}`;
    const raffEventsArray = this.listProjectsEventsCreator1M() ? this.listProjectsEventsCreator1M().map((event) => event._id.valueOf()) : [];
    const raffProjectsArray = this.listProjects() ? this.listProjects().map((project) => project._id.valueOf()) : [];
    const mergeArray = [...raffEventsArray, ...raffProjectsArray, this._id.valueOf()];
    return Actions.find({
      [UserId]: { $exists: 1 }, [finished]: 'validated', credits: { $lt: 0 }, parentId: { $in: mergeArray },
    });
  },
  actionsValidateSpend() {
    const finished = `finishedBy.${Meteor.userId()}`;
    const UserId = `links.contributors.${Meteor.userId()}`;
    // const raffProjectsArray = this.listProjectsEventsCreator1M().map(event => event._id.valueOf());
    // return Actions.find({ [UserId]: { $exists: 1 }, [finished]: 'validated', parentId: { $in: raffProjectsArray } }, { sort: { endDate: -1 } });
    return Actions.find({ [UserId]: { $exists: 1 }, [finished]: 'validated' }, { sort: { endDate: -1 } });
  },
  actionsAll() {
    const queryProjectId = `parent.${this._id.valueOf()}`;
    const poleProjects = Projects.find({ [queryProjectId]: { $exists: 1 } }).fetch();
    const poleProjectsId = [];
    if (poleProjects && poleProjects.length > 0) {
      poleProjects.forEach((element) => {
        poleProjectsId.push(element._id.valueOf());
      });
    }

    const queryEventsArray = {};
    const eventsArrayId = [];
    if (poleProjects && poleProjects.length > 0) {
      queryEventsArray.$or = [];
      poleProjects.forEach((element) => {
        const queryCo = {};
        queryCo[`organizer.${element._id.valueOf()}`] = { $exists: true };
        queryEventsArray.$or.push(queryCo);
      });

      Events.find(queryEventsArray).forEach(function (event) { eventsArrayId.push(event._id.valueOf()); });
    }
    // faire un ou si date pas remplie
    const query = {};
    const inputDate = new Date();
    // query.endDate = { $gte: inputDate };
    query.$or = [];
    query.$or.push({ answerId: { $exists: false }, endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo' });
    query.$or.push({ answerId: { $exists: false }, endDate: { $exists: false }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo' });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    return Actions.find(query);
  },
  actionsAllAgenda(inputDateStart, inputDateEnd) {
    inputDateStart = inputDateStart || new Date();
    const queryProjectId = `parent.${this._id.valueOf()}`;
    const poleProjects = Projects.find({ [queryProjectId]: { $exists: 1 } }).fetch();
    const poleProjectsId = [];
    if (poleProjects && poleProjects.length > 0) {
      poleProjects.forEach((element) => {
        poleProjectsId.push(element._id.valueOf());
      });
    }

    const queryEventsArray = {};
    const eventsArrayId = [];
    if (poleProjects && poleProjects.length > 0) {
      queryEventsArray.endDate = { $exists: true, $gte: inputDateStart };
      if (inputDateEnd) {
        queryEventsArray.startDate = { $exists: true, $lte: inputDateEnd };
      }

      queryEventsArray.$or = [];
      poleProjects.forEach((element) => {
        const queryCo = {};
        queryCo[`organizer.${element._id.valueOf()}`] = { $exists: true };
        queryEventsArray.$or.push(queryCo);
      });

      Events.find(queryEventsArray).forEach(function (event) { eventsArrayId.push(event._id.valueOf()); });
    }
    // faire un ou si date pas remplie
    const query = {};
    query.$and = [];
    query.$and.push({ answerId: { $exists: false } });
    query.$and.push({ parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] } });
    query.$and.push({ status: { $in: ['done', 'todo'] } });
    query.$and.push({ endDate: { $exists: true, $gte: inputDateStart } });
    if (inputDateEnd) {
      query.$and.push({ startDate: { $exists: true, $lte: inputDateEnd } });
    }

    const options = {};
    options.sort = {
      startDate: 1,
    };

    options.fields = { _id: 1, name: 1, startDate: 1, endDate: 1, parentId: 1, parentType: 1, idParentRoom: 1, status: 1, tags: 1 };
    const linkUserID = `links.contributors.${Meteor.userId()}`;
    options.fields[linkUserID] = 1;
    return Actions.find(query, options);
  },
  actionsUserAll(userId, etat, search) {
    const bothUserId = userId ?? Meteor.userId();

    const queryProjectId = `parent.${this._id.valueOf()}`;
    const poleProjects = Projects.find({ [queryProjectId]: { $exists: 1 } }).fetch();
    const poleProjectsId = [];
    poleProjects.forEach((element) => {
      poleProjectsId.push(element._id.valueOf());
    });

    const queryEventsArray = {};
    queryEventsArray.$or = [];
    poleProjects.forEach((element) => {
      const queryCo = {};
      queryCo[`organizer.${element._id.valueOf()}`] = { $exists: true };
      queryEventsArray.$or.push(queryCo);
    });

    const eventsArrayId = [];
    if (queryEventsArray && queryEventsArray.$or && queryEventsArray.$or.length > 0) {
      Events.find(queryEventsArray).forEach(function (event) { eventsArrayId.push(event._id.valueOf()); });
    } else {
      delete queryEventsArray.$or;
    }
    // faire un ou si date pas remplie
    const query = {};
    const inputDate = new Date();
    // query.endDate = { $gte: inputDate };
    const linkUserID = `links.contributors.${bothUserId}`;

    const fields = {};
    if (search) {
      // regex qui marche coté serveur parcontre seulement sur un mot
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'regex');
      const pattern = new RegExp(`.*${searchApplyDiacritics.replace(/\\/g, '\\\\')}.*`, 'i');
      fields.name = { $regex: pattern };
    }

    const finishedObj = {};
    const finished = `finishedBy.${bothUserId}`;
    if (etat === 'aFaire') {
      finishedObj[finished] = { $exists: false };
    } else if (etat === 'enAttente') {
      finishedObj[finished] = 'toModerate';
    }

    query.$or = [];
    query.$or.push({
      [linkUserID]: { $exists: true }, endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo', ...fields, ...finishedObj,
    });
    query.$or.push({
      [linkUserID]: { $exists: true }, endDate: { $exists: false }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo', ...fields, ...finishedObj,
    });
    // query.$or.push({ endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj });
    // query.$or.push({ endDate: { $exists: false }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    return Actions.find(query);
  },
  settingOceco() {
    /* {
      pole: true,
      organizations: true
      projectAction: true,
      eventAction: true,
      memberAuto: true,
    } */
    return this.oceco;
  },

  countPopMap() {
    return this.links?.members && Object.keys(this.links.members).length;
  },
  membersPopMap() {
    const members = this.links?.members;
    if (members) {
      const membersIds = arrayLinkParentNoObject(members, 'citoyens');
      return membersIds;
    }
  },


  creditOrgaAllCount() {
    const orgId = this.maitre() ? this.maitre()._id.valueOf() : this._id.valueOf();
    const userCredit = `userWallet.${orgId}.userCredits`;
    const arrayUserWallet = Citoyens.find({ [userCredit]: { $exists: true } }, { fields: { [userCredit]: 1 } }).fetch();
    const total = arrayUserWallet && arrayUserWallet.length > 0 ? arrayUserWallet.map((item) => item.userWallet[orgId].userCredits).reduce((a, b) => a + b) : 0;
    return total || 0;
  },
  typeValue() {
    const organisationTypes = Lists.findOne({ name: 'organisationTypes' });
    return this.type && organisationTypes?.list?.[this.type];
  },
  listOrganisationTypes() {
    return Lists.find({ name: 'organisationTypes' });
  },
  maitre() {
    if (this.oceco?.creditOrgaMaitre && this.parent) {
      return this.maitreName();
    }
  },
  maitreName() {
    if (this.parent) {
      const arrayIds = Object.keys(this.parent)
        .filter((k) => this.parent[k]?.type === 'organizations')
        .map((k) => new Mongo.ObjectID(k));
      if (arrayIds && arrayIds.length > 0) {
        return Organizations.findOne({ _id: { $in: [arrayIds['0']] } }, { fields: { _id: 1, name: 1 } });
      }
    }
  },
  maitreId() {
    return this.maitre() ? this.maitre()._id.valueOf() : this._id.valueOf();
  },
  arrayCountCredit(year, month = null) {
    // requete mongo sur la collection LogUserActions pour l'année en cours (createdAt) et par organizationId et faire le total de credits
    const currentDate = moment();
    const currentYear = currentDate.year();
    // const currentMonth = moment().month();
    const yearQuery = year || currentYear;
    const monthquery = typeof month === 'number' ? moment().month(month).format('MM') : currentDate.format('MM');
    const monthqueryOne = typeof month === 'number' ? moment().month(month).format('M') : currentDate.format('M');

    const query = { organizationId: this._id.valueOf() };
    if (typeof month === 'number') {
      query.createdAt = { $gte: new Date(`${yearQuery}-${monthquery}-01T00:00:00.000Z`), $lte: new Date(`${yearQuery}-${monthquery}-31T23:59:59.999Z`) };
    } else {
      query.createdAt = { $gte: new Date(`${yearQuery}-01-01T00:00:00.000Z`), $lte: new Date(`${yearQuery}-12-31T23:59:59.999Z`) };
    }

    const allActionsLogYear = LogUserActions.find(query).fetch();

    const total = allActionsLogYear.reduce((acc, action) => {
      if (action.credits) {
        acc += action.credits;
      }
      return acc;
    }, 0);

    // total de credits gagner (chiffre positif)
    const totalCreditsGagner = allActionsLogYear.reduce((acc, action) => {
      if (action.credits && action.credits > 0) {
        acc += action.credits;
      }
      return acc;
    }, 0);

    // total de credits depenser (chiffre negatif)
    const totalCreditsDepenser = allActionsLogYear.reduce((acc, action) => {
      if (action.credits && action.credits < 0) {
        acc += action.credits;
      }
      return acc;
    }, 0);

    const totalOb = { year: yearQuery, totalGagner: totalCreditsGagner, totalDepenser: totalCreditsDepenser, total };
    // month is number
    if (typeof month === 'number') {
      totalOb.month = parseInt(monthqueryOne);
    }
    return totalOb;
  },
  listRoleType(role, type = 'organizations') {
    const members = this.links?.members;
    if (role && members) {
      const arrayMongoId = Object.entries(members)
        .filter(([key, member]) =>
          member?.roles?.includes(role) &&
          member?.type === type &&
          Array.isArray(member?.roles)
        )
        .map(([key]) => new Mongo.ObjectID(key));
      return Organizations.find({ _id: { $in: arrayMongoId } }, { fields: { _id: 1, name: 1 } });
    }
  },
  listRoles(type = 'organizations', search = null) {
    const members = this.links?.members;
    if (members) {
      const ensembleRoles = Array.from(
        new Set(
          Object.values(members)
            .filter(member => member.roles && Array.isArray(member.roles) && member.type === type)
            .flatMap(member => member.roles)
            .map(role => role.trim())
            .filter(role => role !== "")
        )
      );

      // Convertir l'ensemble en un tableau contenant les rôles uniques et non vides
      const rolesUniques = Array.from(ensembleRoles);

      if (search) {
        const rolesUniquesSearch = rolesUniques.filter(element => removeAccent(element).toLowerCase().startsWith(removeAccent(search.toLowerCase())));

        // Trier les rôles par ordre alphabétique
        return rolesUniquesSearch.sort();
      } else {

        // Trier les rôles par ordre alphabétique
        return rolesUniques.sort();
      }

    }
  },
  listNotesCreator(search = null, searchSort = null, limit = null) {
    const query = {};

    let querytwo = {};

    querytwo.parentId = { $in: [this._id.valueOf()] };

    if (search && !search.hash) {
      querytwo = searchQueryNotes(querytwo, search);
    }

    const querytwoAnd = {};
    querytwoAnd.$and = [];
    Object.keys(querytwo).forEach((key) => {
      querytwoAnd.$and.push({ [key]: querytwo[key] });
    });


    const options = {};

    if (searchSort && !searchSort.hash) {
      const arraySort = searchQuerySort('notes', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      } else {
        options.sort = {
          updatedAt: -1,
        };
      }
    } else {
      options.sort = {
        updatedAt: -1,
      };
    }

    if (limit && !limit.hash) {
      options.limit = limit;
    }

    options.fields = { _id: 1, name: 1, parentId: 1, parentType: 1, 'links.contributors': 1, tags: 1, tracking: 1, createdAt: 1, updatedAt: 1, visibility: 1, creator: 1, contributorsCount: 1, lastModifiedBy: 1 };

    return Notes.find(querytwoAnd, options);
  },
  countNotesCreator(search) {
    return this.listNotesCreator(search) && this.listNotesCreator(search).count();
  },
};

const organizationsHelpers = createHelpers(
  commonHelpers.isVisibleFields,
  commonHelpers.isPublicFields,
  commonHelpers.isPrivateFields,
  commonHelpers.documents,
  commonHelpers.rolesLinks,
  commonHelpers.roles,
  commonHelpers.creatorProfile,
  commonHelpers.isCreator,
  commonHelpers.isFavorites,
  commonHelpers.isScope,
  commonHelpers.isScopeAdminPending,
  commonHelpers.isIsInviting,
  commonHelpers.isIsAdminPending,
  commonHelpers.isInviting,
  commonHelpers.InvitingUser,
  commonHelpers.isToBeValidated,
  commonHelpers.isAdminPending,
  commonHelpers.toBeValidated,
  commonHelpers.toBeisInviting,
  commonHelpers.listMembersToBeValidated,
  commonHelpers.listMembersToBeValidatedCount,
  commonHelpers.isFollows,
  commonHelpers.isFollowsMe,
  commonHelpers.listFollows,
  commonHelpers.countFollows,
  commonHelpers.isFollowers,
  commonHelpers.isFollowersMe,
  commonHelpers.listFollowers,
  commonHelpers.countFollowers,
  commonHelpers.listEvents,
  commonHelpers.countEvents,
  commonHelpers.listNotifications,
  commonHelpers.countListNotifications,
  commonHelpers.listNotificationsAsk,
  commonHelpers.isChat,
  commonHelpers.isOneChat,
  commonHelpers.isMultiChat,
  commonHelpers.chatUrl,
  commonHelpers.actionIndicatorCount,
  commonHelpers.userActionIndicatorCount,

);

Organizations.helpers({ ...organizationsHelpers, ...customOrganizationsHelpers });

// }
