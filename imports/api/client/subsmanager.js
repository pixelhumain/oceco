import { SubsManager } from 'meteor/meteorhacks:subs-manager';

export const listsSubs = new SubsManager({
  cacheLimit: 500,
  expireIn: 60,
});

export const notificationsSubs = new SubsManager({
  cacheLimit: 100,
  expireIn: 60,
});
