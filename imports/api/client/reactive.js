/* global PersistentSession */
import { ReactiveDict } from 'meteor/reactive-dict';

export const pageSession = new ReactiveDict('pageSession');

export const pageDirectory = new ReactiveDict('pageDirectory');

export const searchAction = new PersistentSession('searchAction');

export const searchNotes = new PersistentSession('searchNotes');

export const geoId = new ReactiveDict('geoId');

export const SessionGeo = new ReactiveDict('sessionGeo');

export const orgaCible = new ReactiveDict('orgaCible');
