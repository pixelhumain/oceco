import { moment } from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema } from './schema.js';

// eslint-disable-next-line import/prefer-default-export
export const SchemasActionsRest = new SimpleSchema(baseSchema.pick('name', 'description', 'tags', 'tags.$'), {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});
SchemasActionsRest.extend({
  tagsText: {
    type: String,
    optional: true,
  },
  assignText: {
    type: String,
    optional: true,
  },
  assign: {
    type: Array,
    optional: true,
  },
  'assign.$': {
    type: String,
  },
  // participants: {
  //   type: Array,
  //   optional: true,
  // },
  // 'participants.$': {
  //   type: String,
  // },
  // finishedBy: {
  //   type: Array,
  //   optional: true,
  // },
  // 'finishedBy.$': {
  //   type: String,
  // },
  // validated: {
  //   type: Array,
  //   optional: true,
  // },
  // 'validated.$': {
  //   type: String,
  // },
  credits: {
    type: Number,
    // defaultValue: 1,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      if (this.isModifier && !this.value) {
        return;
      }
      return 1;
    },
    optional: true,
  },
  max: {
    type: Number,
    optional: true,
  },
  min: {
    type: Number,
    optional: true,
  },
  idParentRoom: {
    type: String,
    optional: true,
  },
  startDate: {
    type: Date,
    optional: true,
    custom() {
      if (this.field('endDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      const startDate = moment(this.value).toDate();
      const endDate = moment(this.field('endDate').value).toDate();
      if (!this.field('options.possibleStartActionBeforeStartDate').value) {
        if (moment(endDate).isBefore(startDate)) {
          return 'maxDateStart';
        }
      }
    },
  },
  endDate: {
    type: Date,
    optional: true,
    custom() {
      // eslint-disable-next-line no-empty
      if (this.field('startDate').value && this.field('options.possibleStartActionBeforeStartDate').value) {

      } else if (this.field('startDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      if (this.value) {
        const startDate = moment(this.field('startDate').value).toDate();
        const endDate = moment(this.value).toDate();
        if (moment(endDate).isBefore(startDate)) {
          return 'minDateEnd';
        }
      }
    },
  },
  parentId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  parentType: {
    type: String,
    allowedValues: ['projects', 'organizations', 'events', 'citoyens'],
  },
  urls: {
    type: Array,
    optional: true,
  },
  'urls.$': {
    type: String,
  },
  milestoneId: {
    type: String,
    autoform: {
      type: 'select',
    },
    optional: true,
  },
  options: {
    type: Object,
    optional: true,
  },
  'options.creditAddPorteur': {
    type: Boolean,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return false;
    },
    optional: true,
  },
  'options.creditSharePorteur': {
    type: Boolean,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return false;
    },
    optional: false,
  },
  'options.possibleStartActionBeforeStartDate': {
    type: Boolean,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return false;
    },
    optional: true,
  },
});
