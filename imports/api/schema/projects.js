import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import { Random } from 'meteor/random';

// schemas
import {
  baseSchema, blockBaseSchema, geoSchema, preferences,
} from './schema.js';

// SimpleSchema.debug = true;
export const SchemasProjectsRest = new SimpleSchema(baseSchema, {
  tracker: Tracker,
});
SchemasProjectsRest.extend(geoSchema);
SchemasProjectsRest.extend({
  avancement: {
    type: String,
    optional: true,
  },
  startDate: {
    type: Date,
    optional: true,
  },
  endDate: {
    type: Date,
    optional: true,
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
    optional: true,
  },
  fixe: {
    type: String,
    optional: true,
  },
  mobile: {
    type: String,
    optional: true,
  },
  fax: {
    type: String,
    optional: true,
  },
  parentType: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  parentId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  public: {
    type: Boolean,
    defaultValue: true,
  },
});

export const BlockProjectsRest = {};

BlockProjectsRest.descriptions = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockProjectsRest.descriptions.extend(baseSchema.pick('shortDescription', 'description', 'tags', 'tags.$'));

BlockProjectsRest.info = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockProjectsRest.info.extend(baseSchema.pick('name', 'url'));
BlockProjectsRest.info.extend(SchemasProjectsRest.pick('avancement', 'email'));

BlockProjectsRest.network = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockProjectsRest.network.extend({
  github: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  instagram: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  skype: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  gpplus: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  twitter: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
  facebook: {
    type: String,
    regEx: SimpleSchema.RegEx.Url,
    optional: true,
  },
});

BlockProjectsRest.when = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockProjectsRest.when.extend(SchemasProjectsRest.pick('startDate', 'endDate'));

BlockProjectsRest.locality = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockProjectsRest.locality.extend(geoSchema);

BlockProjectsRest.preferences = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
BlockProjectsRest.preferences.extend({
  preferences: {
    type: preferences,
    optional: true,
  },
});

export const SchemasProjectsOcecoRest = new SimpleSchema({
  oceco: {
    type: Object,
  },
  'oceco.milestones': {
    type: Array,
  },
  'oceco.milestones.$': {
    type: Object,
  },
  'oceco.milestones.$.milestoneId': {
    type: String,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return Random.id();
    },
    optional: true,
    autoform: {
      type: 'hidden',
    },
  },
  'oceco.milestones.$.name': {
    type: String,
    label: 'Nom',
  },
  'oceco.milestones.$.description': {
    type: String,
    optional: true,
    label: 'Description',
  },
  'oceco.milestones.$.startDate': {
    type: Date,
    label: 'Date de début',
    optional: true,
  },
  'oceco.milestones.$.endDate': {
    type: Date,
    label: 'Date de fin',
    optional: true,
  },
  'oceco.milestones.$.status': {
    type: String,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return 'open';
    },
    optional: true,
    autoform: {
      type: 'hidden',
    },
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasProjectsGitOcecoRest = new SimpleSchema({
  oceco: {
    type: Object,
  },
  'oceco.git': {
    type: Object,
  },
  'oceco.git.type': {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  'oceco.git.server': {
    type: String,
  },
  'oceco.git.projectToken': {
    type: String,
  },
  'oceco.git.projectId': {
    type: String,
  },
  'oceco.git.active': {
    type: Boolean,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return false;
    },
    optional: true,
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});
