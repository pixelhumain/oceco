import { moment } from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const SchemasNotesRest = new SimpleSchema(baseSchema.pick('name', 'tags', 'tags.$'), {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

// TODO activeUsers

SchemasNotesRest.extend({
  tagsText: {
    type: String,
    optional: true,
  },
  parentId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  parentType: {
    type: String,
    allowedValues: ['projects', 'organizations', 'events', 'citoyens'],
  },
  creator: {
    type: String,
    autoValue() {
      if (this.isInsert) {
        return Meteor.userId();
      }
      return this.value;
    },
  },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset();
    },
  },
  updatedAt: {
    type: Date,
    autoValue() {
      return new Date();
    },
  },
  doc: {
    type: String,
  },
  visibility: {
    type: String,
    allowedValues: ['public', 'private'],
  },
  lockEditing: {
    type: Boolean,
    optional: true,
    autoValue() {
      if (this.isInsert) {
        return false;
      }
      return this.value;
    }
  },
  updates: {
    type: Array,
    optional: true,
  },
  'updates.$': {
    type: Object,
  },
  'updates.$.changes': {
    type: Array,
    blackbox: true
  },
  'updates.$.clientID': {
    type: String,
  },
  'updates.$.version': {
    type: SimpleSchema.Integer,
  },
  links: {
    type: Object,
    optional: true,
  },
});
