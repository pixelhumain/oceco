import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema } from './schema.js';

// eslint-disable-next-line import/prefer-default-export
export const SchemasAnswersRest = new SimpleSchema(baseSchema.pick('name', 'description', 'tags', 'tags.$'), {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

SchemasAnswersRest.extend({
  tagsText: {
    type: String,
    optional: true,
  },
  urgent: {
    type: Boolean,
    autoValue() {
      if (this.isSet) {
        return this.value;
      }
      return false;
    },
    optional: true,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
});

export const SchemasAnswersDepenseRest = new SimpleSchema({
  group: {
    type: Array,
    autoform: {
      type: 'select',
    },
    optional: true,
  },
  'group.$': {
    type: String,
  },
  nature: {
    type: Array,
    autoform: {
      type: 'select',
    },
    optional: true,
  },
  'nature.$': {
    type: String,
  },
  poste: {
    type: String,
  },
  price: {
    type: Number,
  },
  keyDepense: {
    type: String,
    optional: true,
  },
  answerId: {
    type: String,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasAnswersDepenseEstimateRest = new SimpleSchema({
  days: {
    type: Number,
  },
  price: {
    type: Number,
  },
  keyDepense: {
    type: String,
  },
  answerId: {
    type: String,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasAnswersDepenseFinanceRest = new SimpleSchema({
  line: {
    type: String,
  },
  amount: {
    type: Number,
  },
  communaute: {
    type: Boolean,
    defaultValue: true,
  },
  financeurId: {
    type: String,
    autoform: {
      type: 'select',
    },
    optional: true,
    custom() {
      const shouldBeRequired = this.field('communaute').value === true;
      if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
          if (!this.isSet || this.value === null || this.value === '') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
        } else if (this.isSet) {
          // eslint-disable-next-line no-mixed-operators
          if (this.operator === '$set' && this.value === null || this.value === '') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
          if (this.operator === '$unset') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
          if (this.operator === '$rename') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
        }
      }
    },
  },
  name: {
    type: String,
    optional: true,
    custom() {
      const shouldBeRequired = this.field('communaute').value === false;
      if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
          if (!this.isSet || this.value === null || this.value === '') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
        } else if (this.isSet) {
          // eslint-disable-next-line no-mixed-operators
          if (this.operator === '$set' && this.value === null || this.value === '') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
          if (this.operator === '$unset') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
          if (this.operator === '$rename') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
        }
      }
    },
  },
  email: {
    type: String,
    optional: true,
    custom() {
      const shouldBeRequired = this.field('communaute').value === false;
      if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
          if (!this.isSet || this.value === null || this.value === '') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
        } else if (this.isSet) {
          // eslint-disable-next-line no-mixed-operators
          if (this.operator === '$set' && this.value === null || this.value === '') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
          if (this.operator === '$unset') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
          if (this.operator === '$rename') {
            return SimpleSchema.ErrorTypes.REQUIRED;
          }
        }
      }
    },
  },
  keyDepense: {
    type: String,
  },
  keyFinance: {
    type: String,
    optional: true,
  },
  answerId: {
    type: String,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasAnswersDepenseWorkerRest = new SimpleSchema({
  workType: {
    type: String,
  },
  workerId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  keyDepense: {
    type: String,
  },
  answerId: {
    type: String,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasAnswersDepensePayementRest = new SimpleSchema({
  /* financeurId: {
    type: String,
    autoform: {
      type: 'select',
    },
    optional: true,
  }, */
  beneficiaryId: {
    type: String,
    autoform: {
      type: 'select',
    },
  },
  amount: {
    type: Number,
  },
  keyDepense: {
    type: String,
    optional: true,
  },
  keyPayement: {
    type: String,
    optional: true,
  },
  actionKey: {
    type: String,
    optional: true,
  },
  taskKey: {
    type: String,
    optional: true,
  },
  answerId: {
    type: String,
  },
  parentId: {
    type: String,
    optional: true,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasAnswersDepenseTaskRest = new SimpleSchema({
  task: {
    type: String,
  },
  amount: {
    type: Number,
  },
  endDate: {
    type: Date,
    optional: true,
  },
  assignText: {
    type: String,
    optional: true,
  },
  assign: {
    type: Array,
    optional: true,
  },
  'assign.$': {
    type: String,
  },
  keyAction: {
    type: String,
  },
  keyDepense: {
    type: String,
  },
  keyTask: {
    type: String,
    optional: true,
  },
  answerId: {
    type: String,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});
