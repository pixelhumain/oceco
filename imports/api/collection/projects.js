import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Projects = new Mongo.Collection('projects', { idGeneration: 'MONGO' });

Projects.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
