/* eslint-disable import/prefer-default-export */
import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

export const CountUserActions = new Mongo.Collection('countUserActions', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  CountUserActions.rawCollection().createIndex(
    { scopeId: 1, scope: 1 },
    { name: 'scopeId_1_scope_1' },
    (e) => {
      if (e) {
        console.log(e);
      }
    },
  );
}

CountUserActions.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

export const CountActions = new Mongo.Collection('countActions', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  CountActions.rawCollection().createIndex(
    { scopeId: 1, scope: 1 },
    { name: 'scopeId_1_scope_1' },
    (e) => {
      if (e) {
        console.log(e);
      }
    },
  );
}

CountActions.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});