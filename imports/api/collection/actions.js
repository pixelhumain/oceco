import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Actions = new Mongo.Collection('actions', { idGeneration: 'MONGO' });

Actions.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
