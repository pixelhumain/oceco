import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Organizations = new Mongo.Collection('organizations', { idGeneration: 'MONGO' });

Organizations.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
