/* eslint-disable import/prefer-default-export */
import { Mongo } from 'meteor/mongo';

export const LogOrgaMonthCredit = new Mongo.Collection('logorgamonthcredit', { idGeneration: 'MONGO' });

LogOrgaMonthCredit.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
