/* eslint-disable import/prefer-default-export */
import { Mongo } from 'meteor/mongo';

export const LogUserActions = new Mongo.Collection('loguseractions', { idGeneration: 'MONGO' });

LogUserActions.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
