import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Events = new Mongo.Collection('events', { idGeneration: 'MONGO' });

Events.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
