import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Notes = new Mongo.Collection('notes', { idGeneration: 'MONGO' });

export const NotesConnexion = new Mongo.Collection('notesConnexion', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  // Index

}

Notes.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
