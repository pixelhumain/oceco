/* eslint-disable import/prefer-default-export */
import { Mongo } from 'meteor/mongo';

export const Cities = new Mongo.Collection('cities', { idGeneration: 'MONGO' });

Cities.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
