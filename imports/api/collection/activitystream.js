import { Mongo } from 'meteor/mongo';

export const ActivityStream = new Mongo.Collection('activityStream', { idGeneration: 'MONGO' });

export const ActivityStreamReference = new Mongo.Collection('activityStreamReference', { idGeneration: 'MONGO' });

ActivityStream.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

ActivityStreamReference.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
