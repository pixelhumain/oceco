import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Forms = new Mongo.Collection('forms', { idGeneration: 'MONGO' });

Forms.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
