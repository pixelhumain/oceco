import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Rooms = new Mongo.Collection('rooms', { idGeneration: 'MONGO' });

Rooms.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
