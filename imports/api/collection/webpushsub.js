import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const Webpushsub = new Mongo.Collection('webpushsub', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  // Index

}

Webpushsub.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
