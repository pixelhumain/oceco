/* eslint-disable import/prefer-default-export */
import { Mongo } from 'meteor/mongo';

export const LogEmailOceco = new Mongo.Collection('logemailoceco', { idGeneration: 'MONGO' });

LogEmailOceco.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
