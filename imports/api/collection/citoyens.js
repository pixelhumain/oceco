/* eslint-disable meteor/no-session */
/* global */
import { Mongo } from 'meteor/mongo';

// Person
// eslint-disable-next-line import/prefer-default-export
export const Citoyens = new Mongo.Collection('citoyens', { idGeneration: 'MONGO' });

Citoyens.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
