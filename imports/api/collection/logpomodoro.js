import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const LogUserPomo = new Mongo.Collection('loguserpomodoro', { idGeneration: 'MONGO' });

LogUserPomo.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
