import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Answers = new Mongo.Collection('answers', { idGeneration: 'MONGO' });

Answers.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
