import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Comments = new Mongo.Collection('comments', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  // Index
  Comments.rawCollection().createIndex(
    { contextId: 1 },
    { name: 'contextId', partialFilterExpression: { contextId: { $exists: true } }, background: true },
    (e) => {
      if (e) {
        // console.log(e);
      }
    },
  );
}

Comments.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});
