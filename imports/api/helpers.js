/* eslint-disable no-useless-escape */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-shadow */
/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';

// collections
import { Organizations } from './collection/organizations.js';
import { Citoyens } from './collection/citoyens.js';
import { Events } from './collection/events.js';
import { Projects } from './collection/projects.js';
import { Rooms } from './collection/rooms.js';
import { Actions } from './collection/actions.js';
import { Forms } from './collection/forms.js';
import { Answers } from './collection/answers.js';
import { Comments } from './collection/comments.js';

const collections = {
  Citoyens: Citoyens,
  Organizations: Organizations,
  Projects: Projects,
  Events: Events,
  Rooms: Rooms,
  Actions: Actions,
  Forms: Forms,
  Answers: Answers,
  Comments: Comments,
};

export const scopeElement = {
  citoyens: 'citoyens',
  organizations: 'organizations',
  projects: 'projects',
  events: 'events',
  rooms: 'rooms',
  actions: 'actions',
  comments: 'comments',
  forms: 'forms',
  answers: 'answers',
}

export const scopeElementLink = {
  [scopeElement.organizations]: 'members',
  [scopeElement.projects]: 'contributors',
  [scopeElement.events]: 'attendees',
  [scopeElement.actions]: 'contributors',
}

export const customLinksOrga =
{
  organizations: 'memberOf',
};

export const statusNoVisible = ["uncomplete", "deleted", "deletePending"];

export const organizationsFieldsNoPerma = {
  costum: 0, video: 0, openingHours: 0, openingDate: 0, thematic: 0, badges: 0, serviceOffers: 0, linkFinancialDevice: 0, weaknesses: 0, strongPoints: 0, responsable: 0, publicCible: 0, objective: 0,
  mobile: 0,
  siren: 0,
  siret: 0,
  waldec: 0,
  category: 0,
  financialPartners: 0,
  jobFamily: 0,
  legalStatus: 0,
  link: 0,
  maximumAmount: 0,
  source: 0,
  properties: 0,
  reference: 0,
  onepageEdition: 0,
  modifiedByBatch: 0,
  contacts: 0,
};

export const organizationsFieldsNoSimple = { geo: 0, geoPosition: 0, address: 0, tags: 0, roles: 0, urls: 0, description: 0 }

// tags: 0, geo: 0, geoPosition: 0, address: 0, urls: 0, description: 0, telephone: 0, birthDate: 0, shortDescription: 0, socialNetwork: 0

export const optionsNoFieldsScope = (scope, isAdmin = false) => {
  const options = {};
  if (scope === 'citoyens') {
    options.fields = {
      pwd: 0, gamification: 0, '@context': 0, multitags: 0, collections: 0, badges: 0, modifiedByBatch: 0, numberOfInvit: 0, actions: 0
    };
  } else if (scope === 'projects') {
    options.fields = { costum: 0 };
    if (!isAdmin) {
      options.fields = { 'oceco.git': 0 };
    }
  } else if (scope === 'organizations') {
    if (!isAdmin) {
      options.fields = { 'oceco.tibillet.apiKey': 0, ...organizationsFieldsNoPerma };
    } else {
      options.fields = { ...organizationsFieldsNoPerma };
    }
  }
  return options;
};

export const StatusAnswers = [{
  status: 'progress',
  icon: 'fa fa-hourglass',
  color: 'balanced',
},
{
  status: 'vote',
  icon: 'fa fa-gavel',
  color: 'balanced',
},
{
  status: 'finance',
  icon: 'fa fa-euro',
  color: 'balanced',
},
{
  status: 'call',
  icon: 'fa fa-tag',
  color: 'balanced',
}, {
  status: 'newaction',
  icon: 'fa fa-tag',
  color: 'balanced',
},
{
  status: 'projectstate',
  icon: 'fa fa-tag',
  color: 'balanced',
},
{
  status: 'finish',
  icon: 'fa fa-tag',
  color: 'balanced',
},
{
  status: 'suspend',
  icon: 'fa fa-tag',
  color: 'balanced',
}];

export const AcceptationAnswers = [{
  status: 'retained',
  tradKey: 'acceptation.retained',
  icon: 'fa fa-check-circle',
  color: 'balanced',
},
{
  status: 'rejected',
  tradKey: 'acceptation.rejected',
  icon: 'fa fa-times-circle',
  color: 'assertive',
},
{
  status: 'pending',
  tradKey: 'acceptation.pending',
  icon: 'fa fa-pause-circle',
  color: 'energized',
}];

export const PriorityAnswers = [{
  status: 'urgency',
  tradKey: 'priority.urgency',
  icon: 'fa fa-exclamation-triangle',
  color: 'assertive',
}];

export const StatusTimes = {
  work: 1500,
  short_rest: 300,
  long_rest: 900,
};

const diacriticsApplyMap = {
  default: [
    { base: 'A', letters: /[{à}{á}{â}{ã}{ä}{å}{À}{Á}{Â}{Ã}{Ä}{Å}]/g },
    // { base: 'AA', letters: /[{Ꜳ}{ꜳ}]/g },
    { base: 'AE', letters: /[{Æ}{Ǽ}{Ǣ}{æ}{ǽ}{ǣ}]/g },
    // { base: 'AO', letters: /[{Ꜵ}{ꜵ}]/g },
    // { base: 'AU', letters: /[{Ꜷ}{ꜷ}]/g },
    // { base: 'AV', letters: /[{Ꜹ}{ꜹ}]/g },
    // { base: 'AY', letters: /[{Ꜽ}{ꜽ}]/g },
    { base: 'C', letters: /[{ç}{Ç}]/g },
    { base: 'E', letters: /[{è}{é}{ê}{ë}{É}{È}{Ê}{Ë}]/g },
    { base: 'I', letters: /[{ì}{í}{î}{ï}{Ì}{Í}{Î}{Ï}]/g },
    { base: 'O', letters: /[{ò}{ó}{ô}{õ}{ö}{ø}{Ò}{Ó}{Ô}{Ö}]/g },
    { base: 'U', letters: /[{ù}{ú}{û}{ü}{Ù}{Ú}{Û}{Ü}]/g },
    { base: 'a', letters: /[{à}{á}{â}{ã}{ä}{å}{À}{Á}{Â}{Ã}{Ä}{Å}]/g },
    // { base: 'aa', letters: /[{Ꜳ}{ꜳ}]/g },
    { base: 'ae', letters: /[{Æ}{Ǽ}{Ǣ}{æ}{ǽ}{ǣ}]/g },
    // { base: 'ao', letters: /[{Ꜵ}{ꜵ}]]/g },
    // { base: 'au', letters: /[{Ꜷ}{ꜷ}]/g },
    // { base: 'av', letters: /[{Ꜹ}{ꜹ}]/g },
    // { base: 'ay', letters: /[{Ꜽ}{ꜽ}]/g },
    { base: 'c', letters: /[{ç}{Ç}]/g },
    { base: 'e', letters: /[{è}{é}{ê}{ë}{É}{È}{Ê}{Ë}]/g },
    { base: 'i', letters: /[{ì}{í}{î}{ï}{Ì}{Í}{Î}{Ï}]/g },
    { base: 'o', letters: /[{ò}{ó}{ô}{õ}{ö}{ø}{Ò}{Ó}{Ô}{Ö}]/g },
    { base: 'u', letters: /[{ù}{ú}{û}{ü}{Ù}{Ú}{Û}{Ü}]/g },
  ],
};

diacriticsApplyMap.regex = [];
for (let i = 0; i < diacriticsApplyMap.default.length; i++) {
  const item = diacriticsApplyMap.default[i];
  const base = `(?:${item.base}|${item.letters.source.replace(/\\u([0-9a-zA-Z]{4})(|])/gi, '\\x{$1}$2')})`;
  diacriticsApplyMap.regex.push({
    base: `$1${base}`,
    letters: new RegExp(`([^\\[\\\\])${base}|^${base}`, 'g'),
  });
}


export const applyDiacritics = (str, which) => {
  const whichSelect = which || 'default';
  const changes = diacriticsApplyMap[whichSelect];
  const lettres = str.split('');
  let newStr = '';
  const pattern = /[0-9]/g;
  const patternCharSpe = /[^0-9a-zA-Z]/g;
  const patternLetterNo = /[^aeciouAECIOU]/g;
  // const patternSpe = /[.*+?^${}()\/|[\]\\]/g;
  for (let f = 0; f < lettres.length; f++) {
    if (lettres[f] === ' ') {
      newStr += lettres[f];
    } else if (lettres[f].match(pattern)) {
      newStr += lettres[f];
    } else if (lettres[f].match(patternCharSpe)) {
      newStr += lettres[f];
    } else if (lettres[f].match(patternLetterNo)) {
      newStr += lettres[f];
    } else {
      for (let i = 0; i < diacriticsApplyMap.default.length; i++) {
        const strReplace = lettres[f].replace(changes[i].letters, changes[i].base);
        if (strReplace !== lettres[f]) {
          newStr += strReplace;
        }
      }
    }
  }
  return newStr;
};

export const getBackgroundColor = (stringInput) => {
  //  verifier stringInput est bien une string
  if (typeof stringInput !== 'string') {
    return 'black';
  }

  const stringUniqueHash = [...stringInput].reduce((acc, char) => char.charCodeAt(0) + ((acc << 5) - acc), 0);
  return `hsl(${stringUniqueHash % 360}, 85%, 35%)`;
};

export const getContrastingTextColor = (hslColor) => {
  // Extraire la luminosité de la couleur HSL
  const luminosity = parseInt(hslColor.split(',')[2]);

  // Si la luminosité est inférieure à 50 %, utiliser du blanc, sinon du noir
  return luminosity < 50 ? 'white' : 'black';
};

export const capitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1);

// export const nameToCollection = (name) => {
//   if (Meteor.isClient) {
//     // Client
//     return window[capitalize(name)];
//   }
//   // Server
//   return global[capitalize(name)];
// };

export const nameToCollection = (name) => {
  if (Meteor.isClient) {
    // Client
    return collections[capitalize(name)];
  }
  // Server
  return collections[capitalize(name)];
};

export const encodeString = (str) => encodeURIComponent(str).replace(/\*/g, '%2A');

export const getDifference = (array1, array2) => array1.filter((object1) => !array2.some((object2) => JSON.stringify(object1) === JSON.stringify(object2)));

export const compareValues = (key, order = 'asc') => function innerSort(a, b) {
  if (!Object.prototype.hasOwnProperty.call(a, key) || !Object.prototype.hasOwnProperty.call(b, key)) {
    // property doesn't exist on either object
    return 0;
  }

  const varA = (typeof a[key] === 'string')
    ? a[key].toUpperCase() : a[key];
  const varB = (typeof b[key] === 'string')
    ? b[key].toUpperCase() : b[key];

  let comparison = 0;
  if (varA > varB) {
    comparison = 1;
  } else if (varA < varB) {
    comparison = -1;
  }
  return (
    (order === 'desc') ? (comparison * -1) : comparison
  );
};

export const arrayAllLink = (links) => {
  const arrayIdsRetour = _.union(_.flatten(_.map(links, (array) => _.map(array, (a, k) => k))));
  return arrayIdsRetour;
};

export const searchQuery = (query, search) => {
  if (search) {
    if (search && search.charAt(0) === '#' && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.tags = { $regex: searchApplyDiacritics, $options: 'i' };
    } else if (search && search.charAt(0) === '~' && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query['milestone.name'] = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
      // query['milestone.name'] = { $regex: `.*${search.substr(1)}.*`, $options: 'i' };
    } else if (search && search.charAt(0) === '?') {
      // filtrer avec ? sur les actions
      // pas de contributors : ?nc
      // avec contributors : ?c
      switch (search.substr(1)) {
        // avec contributors
        case 'c':
          query['links.contributors'] = { $exists: true };
          break;
        // pas de contributors
        case 'nc':
          query['links.contributors'] = { $exists: false };
          break;
        case 'me':
          query[`links.contributors.${Meteor.userId()}`] = { $exists: true };
          break;
        case 'cr':
          query[`creator`] = Meteor.userId();
          break;
        default:
        // console.log(`Sorry, we are out of ${search.substr(1)}.`);
      }
    } else if (search && search.startsWith('id:') && search.substr(3).length === 24) {
      if (isValidObjectId(search.substr(3))) {
        query._id = new Mongo.ObjectID(search.substr(3));
      }
    } else if (search && search.startsWith('nu:')) {
      const numberId = parseInt(search.substr(3), 10);
      if (numberId) {
        query.numberId = numberId;
      }
    } else if (search && search.charAt(0) === '@' && search.length > 1) {
      const username = search.substr(1);
      const userOne = Citoyens.findOne({ username });
      if (userOne) {
        query[`links.contributors.${userOne._id.valueOf()}`] = { $exists: true };
      }
    } else if (search && search.charAt(0) === ':' && search.length > 1) {
      // search projet donc pas de recherche dans actions
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.name = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
    } else if (search && search.charAt(0) === '*') {
      if (search.length > 1) {
        const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
        query.name = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
        query.tracking = true;
      } else {
        query.tracking = true;
      }
    } else if (search && search.length > 1) {
      let searchDia = search;
      if (search && search.startsWith('all:')) {
        if (search.substr(4).length > 2) {
          searchDia = search.substr(4);
        } else {
          return query;
        }
      }
      if (search && search.startsWith('bot:')) {
        query['autoCloserActionBot'] = { $exists: true };
        // query.status = 'disabled';
        if (search.substr(4).length > 2) {
          searchDia = search.substr(4);
        } else {
          return query;
        }
      }
      const searchApplyDiacritics = applyDiacritics(searchDia.replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.name = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
    }
  }
  return query;
};

export const searchQueryNotes = (query, search, fields = ['name', 'doc']) => {
  if (search) {
    if (search.charAt(0) === '#' && search.length > 1) {
      // Recherche par tags
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.tags = { $regex: searchApplyDiacritics, $options: 'i' };
    } else if (search.charAt(0) === '!') {
      // Gestion de la visibilité
      query.visibility = search.charAt(1) === 'p' ? 'private' : 'public';
    } else if (search.charAt(0) === '?') {
      // Recherche par conditions spécifiques
      if (search.substr(1) === 'cr') {
        query.creator = Meteor.userId();
      } else if (search.substr(1) === 'me') {
        query['links.contributors'] = { $exists: true };
        query[`links.contributors.${Meteor.userId()}`] = { $exists: true };
      } else if (search.substr(1) === 'perso') {
        query.parentType = 'citoyens';
        query.parentId = Meteor.userId();
      } else if (search.substr(1) === 'share') {
        query.parentId = { $ne: Meteor.userId() };
        query.creator = { $ne: Meteor.userId() };
      } else if (search.substr(1) === 'lm') {
        query.lastModifiedBy = { $exists: true, $ne: Meteor.userId() };
      }
    } else if (search.startsWith('parentId:')) {
      // Gestion du parentId
      const parentId = search.split(':')[1];
      if (parentId) {
        query.parentId = parentId;
      }
    } else if (search.charAt(0) === '*') {
      // Recherche par suivi (tracking)
      if (search.length > 1) {
        const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
        query.name = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
        query[`tracking.${Meteor.userId()}`] = true;
      } else {
        query[`tracking.${Meteor.userId()}`] = true;
      }
    } else if (search.length > 1) {
      // Recherche générique
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.$or = fields.map((field) => {
        const querySearch = {};
        querySearch[field] = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
        return querySearch;
      });
    }
  }
  return query;
};

export const searchQueryAnswers = (query, search) => {
  if (search) {
    if (search && search.charAt(0) === '?') {
      //
    } else if (search && search.charAt(0) === '#' && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query['answers.aapStep1.tags'] = { $regex: searchApplyDiacritics, $options: 'i' };
    } else if (search && search.charAt(0) === '!' && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.status = { $regex: searchApplyDiacritics, $options: 'i' };
    } else if (search && search.charAt(0) === '>' && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.substr(1).replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query.acceptation = { $regex: searchApplyDiacritics, $options: 'i' };
    } else if (search && search.charAt(0) === '~' && search.length > 1) {
      if (search.substr(1) === 'urgency') {
        query['answers.aapStep1.urgency'] = 'Urgent';
      }
    } else if (search && search.length > 1) {
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()\/|[\]\\]/g, '\\$&'), 'regex');
      query['answers.aapStep1.titre'] = { $regex: `.*${searchApplyDiacritics}.*`, $options: 'i' };
    }
  }
  return query;
};

export const searchQuerySort = (type, sort) => {
  if (sort && sort[type] && sort[type].length > 0) {
    const arrayChecked = [...sort[type]].filter((item) => item.checked === true).sort(compareValues('order'));
    if (arrayChecked && arrayChecked.length > 0) {
      // je suis dans le bonne ordre
      // il y a les champs
      const arraySort = arrayChecked.map((item) => {
        // const order = item.fieldDesc ? -1 : 1;
        const order = item.fieldDesc ? 'desc' : 'asc';
        // si field et le même que dans la requete
        if (item.existField) {
          return [item.field, order];
          // return { [item.field]: order };
        }
        if (item.field === 'withContributor') {
          //
        }
      });
      return arraySort;
    }
  }
  return false;
};

export const searchQuerySortActived = (sort) => {
  const arrayActived = Object.keys(sort)
    .filter((k) => [...sort[k]].filter((item) => item.checked === true).length > 0);
  return arrayActived.length > 0;
};

export const selectorgaQuery = (query, selectorga) => {
  if (selectorga) {
    query.type = selectorga;
  }
  return query;
};

export const arrayLinkProper = (array) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].type === 'projects' || (array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true)))
    .map((k) => new Mongo.ObjectID(k));
  /* const arrayIds = _.filter(_.map(array, (arrayLink, key) => {
    if (arrayLink.isInviting === true) {
      return undefined;
    }
    if (arrayLink.type === 'citoyens' && arrayLink.toBeValidated === true) {
      return undefined;
    }
    return new Mongo.ObjectID(key);
  }), arrayfilter => arrayfilter !== undefined); */
  return arrayIds;
};

export const arrayLinkProperNoObject = (array) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true))
    .map((k) => k);
  /* const arrayIds = _.filter(_.map(array, (arrayLink, key) => {
    if (arrayLink.isInviting === true) {
      return undefined;
    }
    if (arrayLink.type === 'citoyens' && arrayLink.toBeValidated === true) {
      return undefined;
    }
    return key;
  }), arrayfilter => arrayfilter !== undefined); */
  return arrayIds;
};

export const arrayLinkProperInter = (arraya, arrayb) => {
  const arrayaIds = arrayLinkProperNoObject(arraya);
  const arraybIds = arrayLinkProperNoObject(arrayb);
  const a = new Set(arrayaIds);
  const b = new Set(arraybIds);
  const arrayIdsSet = new Set(
    [...a].filter((x) => b.has(x)),
  );
  const arrayIds = [...arrayIdsSet].map((_id) => new Mongo.ObjectID(_id));
  return arrayIds;
};

export const queryLinkInter = (arraya, arrayb, search, selectorga) => {
  // const arrayIds = _.map(array, (arrayLink, key) => new Mongo.ObjectID(key));
  const arrayIds = arrayLinkProperInter(arraya, arrayb);
  let query = {};
  query._id = { $in: arrayIds };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
    if (selectorga) {
      query = selectorgaQuery(query, selectorga);
    }
  }
  return query;
};

export const queryLink = (array, search, selectorga) => {
  // const arrayIds = _.map(array, (arrayLink, key) => new Mongo.ObjectID(key));
  const arrayIds = arrayLinkProper(array);
  let query = {};
  query._id = { $in: arrayIds };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
    if (selectorga) {
      query = selectorgaQuery(query, selectorga);
    }
  }
  return query;
};

export const queryLinkParent = (id, search, selectorga) => {
  const parentSelect = `parent.${id}`;
  let query = {};
  query[parentSelect] = { $exists: true };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
    if (selectorga) {
      query = selectorgaQuery(query, selectorga);
    }
  }
  return query;
};

export const arrayLinkIsAdmin = (array, arrayOrga) => {
  const arrayIds = Object.keys(array)
    .filter((k) => arrayOrga && arrayOrga[k] && array[k].isAdmin && !array[k].toBeValidated && !array[k].isAdminPending && !array[k].isInviting)
    .map((k) => new Mongo.ObjectID(k));
  return arrayIds;
};

export const queryLinkIsAdmin = (array, arrayOrga, search) => {
  const arrayIds = arrayLinkIsAdmin(array, arrayOrga);
  let query = {};
  query._id = { $in: arrayIds };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
  }
  return query;
};

export const arrayLinkToBeValidated = (array) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].type === 'citoyens' && (array[k].toBeValidated === true || array[k].isAdminPending === true))
    .map((k) => new Mongo.ObjectID(k));
  /* const arrayIds = _.filter(_.map(array, (arrayLink, key) => {
    if (arrayLink.toBeValidated === true) {
      return new Mongo.ObjectID(key);
    }
    return undefined;
  }), arrayfilter => arrayfilter !== undefined); */
  return arrayIds;
};

export const queryLinkToBeValidated = (array) => {
  const arrayIds = arrayLinkToBeValidated(array);
  const query = {};
  query._id = { $in: arrayIds };
  return query;
};

export const arrayLinkToModerate = (array) => {
  // Filtrage du tableau pour trouver les objets avec un status 'toModerate'
  const arrayIds = array
    .filter(({ status }) => status === 'toModerate')
    .map(({ userId }) => new Mongo.ObjectID(userId));
  return arrayIds;
};

export const arrayLinkValidated = (array) => {
  // Filtrage du tableau pour trouver les objets avec un status 'validated'
  const arrayIds = array
    .filter(({ status }) => status === 'validated')
    .map(({ userId }) => new Mongo.ObjectID(userId));
  return arrayIds;
};

export const arrayLinkIsInviting = (array) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].isInviting === true)
    .map((k) => new Mongo.ObjectID(k));
  /* const arrayIds = _.filter(_.map(array, (arrayLink, key) => {
    if (arrayLink.isInviting === true) {
      return new Mongo.ObjectID(key);
    }
    return undefined;
  }), arrayfilter => arrayfilter !== undefined); */
  return arrayIds;
};

export const queryLinkIsInviting = (array, search) => {
  const arrayIds = arrayLinkIsInviting(array);
  let query = {};
  query._id = { $in: arrayIds };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
  }
  return query;
};

export const arrayLinkAttendees = (array, type) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].isInviting !== true && array[k].type === type)
    .map((k) => new Mongo.ObjectID(k));
  /* const arrayIds = _.filter(_.map(array, (arrayLink, key) => {
    if (arrayLink.isInviting === true) {
      return undefined;
    }
    if (arrayLink.type === type) {
      return new Mongo.ObjectID(key);
    }
    return undefined;
  }), arrayfilter => arrayfilter !== undefined); */
  return arrayIds;
};

export const arrayLinkParent = (array, type) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].type === type)
    .map((k) => new Mongo.ObjectID(k));
  return arrayIds;
};

export const arrayLinkParentNoObject = (array, type) => {
  const arrayIds = Object.keys(array)
    .filter((k) => array[k].type === type)
    .map((k) => k);
  return arrayIds;
};

export const arrayParent = (array, arrayType) => {
  const arrayIds = Object.keys(array)
    .filter((k) => _.contains(arrayType, array[k].type))
    .map((k) => new Mongo.ObjectID(k));
  return arrayIds;
};

export const queryOrPrivatescopeIds = (query, scope, scopeIds, userId) => {
  const queryOrPrivate = {};
  queryOrPrivate._id = { $in: scopeIds };
  queryOrPrivate['preferences.private'] = true;
  queryOrPrivate[`links.${scope}.${userId}`] = {
    $exists: true,
  };
  queryOrPrivate[`links.${scope}.${userId}.toBeValidated`] = {
    $exists: false,
  };
  query.$or.push(queryOrPrivate);

  const queryOrPrivateInvite = {};
  queryOrPrivateInvite._id = { $in: scopeIds };
  queryOrPrivateInvite['preferences.private'] = true;
  queryOrPrivateInvite[`links.${scope}.${userId}`] = {
    $exists: true,
  };
  queryOrPrivateInvite[`links.${scope}.${userId}.isInviting`] = {
    $exists: true,
  };
  query.$or.push(queryOrPrivateInvite);
  return query;
};

export const arrayChildrenParent = (scope, parentAuthorise, scopeParent = null, fields = {
  name: 1,
  links: 1,
  profilThumbImageUrl: 1,
  preferences: 1,
}) => {
  const childrenParent = [];

  // console.log(scope);

  if (scope === 'events') {
    // sous events
    const parentEventPush = {
      find(scopeD) {
        // console.log(scopeD);
        if (scopeD.parent) {
          const arrayIdsParent = arrayLinkParent(scopeD.parent, 'events');
          // console.log(arrayIdsParent);
          const collectionType = nameToCollection('events');

          let query = {};
          query.$or = [];
          query.$or.push({
            _id: {
              $in: arrayIdsParent,
            },
            'preferences.private': false,
          });
          query = queryOrPrivatescopeIds(query, 'attendees', arrayIdsParent, Meteor.userId());

          return collectionType.find(query, {
            fields,
          });
        }
      },
    };
    childrenParent.push(parentEventPush);
  }

  parentAuthorise.forEach((parent) => {
    if (scope === 'events') {
      const parentPush = {
        find(scopeD) {
          // console.log(scopeD);
          if (scopeD.organizer) {
            // console.log(parent);
            const arrayIdsParent = arrayLinkParent(scopeD.organizer, parent);
            // console.log(arrayIdsParent);
            const collectionType = nameToCollection(parent);
            let query = {};
            if (['events', 'projects', 'organizations'].includes(parent)) {
              query.$or = [];
              query.$or.push({
                _id: {
                  $in: arrayIdsParent,
                },
                'preferences.private': false,
              });

              if (parent === 'projects') {
                query = queryOrPrivatescopeIds(query, 'contributors', arrayIdsParent, Meteor.userId());
              } else if (parent === 'organizations') {
                query = queryOrPrivatescopeIds(query, 'members', arrayIdsParent, Meteor.userId());
              } else if (parent === 'events') {
                query = queryOrPrivatescopeIds(query, 'attendees', arrayIdsParent, Meteor.userId());
              }
            } else {
              query._id = {
                _id: {
                  $in: arrayIdsParent,
                },
              };
            }
            // console.log(query);
            return collectionType.find(query, {
              fields,
            });
          }
        },
      };

      childrenParent.push(parentPush);
    } else {
      const parentPush = {
        find(scopeD) {
          // console.log(scopeD);
          if (scopeParent) {
            if (_.contains(scopeParent, scope)) {
              if (scopeD.parent) {
                const arrayIdsParent = arrayLinkParent(scopeD.parent, parent);
                // console.log(arrayIdsParent);
                const collectionType = nameToCollection(parent);
                let query = {};
                if (['events', 'projects', 'organizations'].includes(parent)) {
                  query.$or = [];
                  query.$or.push({
                    _id: {
                      $in: arrayIdsParent,
                    },
                    'preferences.private': false,
                  });

                  if (parent === 'projects') {
                    query = queryOrPrivatescopeIds(query, 'contributors', arrayIdsParent, Meteor.userId());
                  } else if (parent === 'organizations') {
                    query = queryOrPrivatescopeIds(query, 'members', arrayIdsParent, Meteor.userId());
                  } else if (parent === 'events') {
                    query = queryOrPrivatescopeIds(query, 'attendees', arrayIdsParent, Meteor.userId());
                  }
                } else {
                  query._id = {
                    _id: {
                      $in: arrayIdsParent,
                    },
                  };
                }
                return collectionType.find(query, {
                  fields,
                });
              }
              if (scopeD.parentType && scopeD.parentId && _.contains(parentAuthorise, scopeD.parentType)) {
                const collectionType = nameToCollection(scopeD.parentType);
                return collectionType.find({
                  _id: new Mongo.ObjectID(scopeD.parentId),
                }, {
                  fields,
                });
              }
            }
          } else {
            if (scopeD.parent) {
              const arrayIdsParent = arrayLinkParent(scopeD.parent, parent);
              // console.log(arrayIdsParent);
              const collectionType = nameToCollection(parent);
              let query = {};
              if (['events', 'projects', 'organizations'].includes(parent)) {
                query.$or = [];
                query.$or.push({
                  _id: {
                    $in: arrayIdsParent,
                  },
                  'preferences.private': false,
                });

                if (parent === 'projects') {
                  query = queryOrPrivatescopeIds(query, 'contributors', arrayIdsParent, Meteor.userId());
                } else if (parent === 'organizations') {
                  query = queryOrPrivatescopeIds(query, 'members', arrayIdsParent, Meteor.userId());
                } else if (parent === 'events') {
                  query = queryOrPrivatescopeIds(query, 'attendees', arrayIdsParent, Meteor.userId());
                }
              } else {
                query._id = {
                  _id: {
                    $in: arrayIdsParent,
                  },
                };
              }
              return collectionType.find(query, {
                fields,
              });
            }
            if (scopeD.parentType && scopeD.parentId && _.contains(parentAuthorise, scopeD.parentType)) {
              const collectionType = nameToCollection(scopeD.parentType);
              return collectionType.find({
                _id: new Mongo.ObjectID(scopeD.parentId),
              }, {
                fields,
              });
            }
          }
        },
      };
      childrenParent.push(parentPush);
    }
  });
  return childrenParent;
};

export const isAdminArray = (organizerArray, citoyen) => {
  let isAdmin = false;
  if (organizerArray) {
    [organizerArray[0]].forEach((parent) => {
      // parent.type
      parent.values.forEach((value) => {
        // value._id
        if (parent.type === 'events' && citoyen.links && citoyen.links.events && citoyen.links.events[value._id.valueOf()] && citoyen.links.events[value._id.valueOf()].isAdmin) {
          isAdmin = true;
        } else if (parent.type === 'projects' && citoyen.links && citoyen.links.projects && citoyen.links.projects[value._id.valueOf()] && citoyen.links.projects[value._id.valueOf()].isAdmin) {
          isAdmin = true;
        } else if (parent.type === 'organizations' && citoyen.links && citoyen.links.memberOf && citoyen.links.memberOf[value._id.valueOf()] && citoyen.links.memberOf[value._id.valueOf()].isAdmin) {
          isAdmin = true;
        }
      });
    });
  }
  return isAdmin;
};

export const isAdminArrayOrga = (organizerArray, citoyen) => {
  let isAdmin = false;
  if (organizerArray) {
    [organizerArray[0]].forEach((parent) => {
      // parent.type
      parent.values.forEach((value) => {
        // value._id
        if (parent.type === 'organizations' && citoyen.links && citoyen.links.memberOf && citoyen.links.memberOf[value._id.valueOf()] && citoyen.links.memberOf[value._id.valueOf()].isAdmin) {
          isAdmin = true;
        }
      });
    });
  }
  return isAdmin;
}

export const arrayOrganizerParent = (arrayParent, parentAuthorise, fields = {
  name: 1,
  links: 1,
  preferences: 1,
}) => {
  const childrenParent = [];
  parentAuthorise.forEach((parent) => {
    const arrayIds = arrayLinkParent(arrayParent, parent);
    const collectionType = nameToCollection(parent);
    const arrayType = collectionType.find({
      _id: {
        $in: arrayIds,
      },
    }, {
      fields,
    }).fetch();
    if (arrayType && arrayType.length > 0) {
      childrenParent.push({
        type: parent,
        values: arrayType,
      });
    }
  });
  return childrenParent;
};

export const queryOrPrivateScopeLinks = (scope, scopeId) => {
  const query = {};
  query.$or = [];
  const queryOrDefault = {};
  queryOrDefault['preferences.private'] = false;
  queryOrDefault[`links.${scope}.${scopeId}`] = { $exists: true };
  // queryOrDefault[`links.contributors.${this._id.valueOf()}.toBeValidated`] = { $exists: false };
  // queryOrDefault[`links.contributors.${this._id.valueOf()}.isInviting`] = { $exists: false };
  query.$or.push(queryOrDefault);
  const queryOrDefaultVide = {};
  queryOrDefaultVide['preferences.private'] = { $exists: false };
  queryOrDefaultVide[`links.${scope}.${scopeId}`] = { $exists: true };
  // queryOrDefaultVide[`links.contributors.${this._id.valueOf()}.toBeValidated`] = { $exists: false };
  // queryOrDefaultVide[`links.contributors.${this._id.valueOf()}.isInviting`] = { $exists: false };
  query.$or.push(queryOrDefaultVide);
  // private userId validate
  const queryOrPrivate = {};
  queryOrPrivate['preferences.private'] = true;
  queryOrPrivate[`links.${scope}.${scopeId}`] = { $exists: true };
  queryOrPrivate[`links.${scope}.${scopeId}.toBeValidated`] = { $exists: false };
  queryOrPrivate[`links.${scope}.${scopeId}.isInviting`] = { $exists: false };
  queryOrPrivate[`links.${scope}.${Meteor.userId()}`] = { $exists: true };
  queryOrPrivate[`links.${scope}.${Meteor.userId()}.toBeValidated`] = { $exists: false };
  query.$or.push(queryOrPrivate);
  // private userId IsInviting
  const queryOrPrivateIsInviting = {};
  queryOrPrivateIsInviting['preferences.private'] = true;
  queryOrPrivateIsInviting[`links.${scope}.${scopeId}`] = { $exists: true };
  queryOrPrivateIsInviting[`links.${scope}.${scopeId}.toBeValidated`] = { $exists: false };
  queryOrPrivateIsInviting[`links.${scope}.${scopeId}.isInviting`] = { $exists: false };
  queryOrPrivateIsInviting[`links.${scope}.${Meteor.userId()}`] = { $exists: true };
  queryOrPrivateIsInviting[`links.${scope}.${Meteor.userId()}.isInviting`] = { $exists: true };
  query.$or.push(queryOrPrivateIsInviting);
  if (scope === 'members') {
    const queryAnd = {};
    queryAnd.$and = [];
    const queryOceco = {};
    queryOceco.oceco = { $exists: true };
    queryAnd.$and.push(queryOceco);
    queryAnd.$and.push(query);
    return queryAnd;
  }
  return query;
};

export const queryOrPrivateScopeLinksIds = (queryStart, scope) => {
  const query = {};
  query.$or = [];
  const queryOrDefault = { ...queryStart };
  queryOrDefault['preferences.private'] = false;
  query.$or.push(queryOrDefault);
  const queryOrDefaultVide = { ...queryStart };
  queryOrDefaultVide['preferences.private'] = { $exists: false };
  query.$or.push(queryOrDefaultVide);
  // private userId validate
  const queryOrPrivate = { ...queryStart };
  queryOrPrivate['preferences.private'] = true;
  queryOrPrivate[`links.${scope}.${Meteor.userId()}`] = { $exists: true };
  queryOrPrivate[`links.${scope}.${Meteor.userId()}.toBeValidated`] = { $exists: false };
  query.$or.push(queryOrPrivate);
  // private userId IsInviting
  const queryOrPrivateIsInviting = { ...queryStart };
  queryOrPrivateIsInviting['preferences.private'] = true;
  queryOrPrivateIsInviting[`links.${scope}.${Meteor.userId()}`] = { $exists: true };
  queryOrPrivateIsInviting[`links.${scope}.${Meteor.userId()}.isInviting`] = { $exists: true };
  query.$or.push(queryOrPrivateIsInviting);
  // console.log(JSON.stringify(query));
  return query;
};

export const queryOrPrivateScope = (query, scope, scopeId, userId) => {
  const queryOrPrivate = {};
  queryOrPrivate._id = new Mongo.ObjectID(scopeId);
  queryOrPrivate['preferences.private'] = true;
  queryOrPrivate[`links.${scope}.${userId}`] = {
    $exists: true,
  };
  queryOrPrivate[`links.${scope}.${userId}.toBeValidated`] = {
    $exists: false,
  };
  query.$or.push(queryOrPrivate);

  const queryOrPrivateInvite = {};
  queryOrPrivateInvite._id = new Mongo.ObjectID(scopeId);
  queryOrPrivateInvite['preferences.private'] = true;
  queryOrPrivateInvite[`links.${scope}.${userId}`] = {
    $exists: true,
  };
  queryOrPrivateInvite[`links.${scope}.${userId}.isInviting`] = {
    $exists: true,
  };
  query.$or.push(queryOrPrivateInvite);
  return query;
};

export const queryLinkAttendees = (array, search, type) => {
  const arrayIds = arrayLinkAttendees(array, type);
  let query = {};
  query._id = { $in: arrayIds };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
    /* if (selectorga) {
      query = selectorgaQuery(query, selectorga);
    } */
  }
  return query;
};

export const arrayLinkType = (array, type, role = null) => {
  const arrayIds = Object.keys(array)
    .filter((k) => {
      return array[k].isInviting !== true && !(array[k].type === 'citoyens' && array[k].toBeValidated === true) && array[k].type === type && (role === null || role && array[k].roles && array[k].roles.length > 0 && array[k].roles.includes(role))
    })
    .map((k) => new Mongo.ObjectID(k));
  //console.log('arrayIds', arrayIds);
  /* const arrayIds = _.filter(_.map(array, (arrayLink, key) => {
    if (arrayLink.isInviting === true) {
      return undefined;
    }
    if (arrayLink.type === 'citoyens' && arrayLink.toBeValidated === true) {
      return undefined;
    }
    if (arrayLink.type === type) {
      return new Mongo.ObjectID(key);
    }
    return undefined;
  }), arrayfilter => arrayfilter !== undefined); */
  return arrayIds;
};

export const queryLinkType = (array, search, type, selectorga) => {
  let role = null;
  if (search && search.startsWith('r:')) {
    if (search.substr(2).length > 0) {
      role = search.substr(2);
    }
  }
  const arrayIds = arrayLinkType(array, type, role);
  let query = {};
  query._id = { $in: arrayIds };
  // if (Meteor.isClient) {
  if (search && !search.hash && !search.startsWith('r:')) {
    query = searchQuery(query, search);
  }
  if (selectorga) {
    query = selectorgaQuery(query, selectorga);
  }
  // }
  return query;
};

export const arrayLinkTypeInvite = (array, type) => {
  const arrayIds = Object.keys(array)
    .filter((k) => (array[k].isAdminInviting === true || array[k].isAdminPending === true || array[k].isInviting === true || (array[k].type === 'citoyens' && array[k].toBeValidated === true)) && array[k].type === type)
    .map((k) => new Mongo.ObjectID(k));
  return arrayIds;
};

export const queryLinkTypeInvite = (array, search, type, selectorga) => {
  const arrayIds = arrayLinkTypeInvite(array, type);
  let query = {};
  query._id = { $in: arrayIds };
  if (Meteor.isClient) {
    if (search) {
      query = searchQuery(query, search);
    }
    if (selectorga) {
      query = selectorgaQuery(query, selectorga);
    }
  }
  return query;
};

export const queryOptions = {
  sort: { name: 1 },
  fields: {
    _id: 1,
    name: 1,
    links: 1,
    parent: 1,
    organizer: 1,
    preferences: 1,
    parentId: 1,
    parentType: 1,
    organizerId: 1,
    organizerType: 1,
    tags: 1,
    type: 1,
    profilThumbImageUrl: 1,
  },
};

export const userLanguage = () => {
  // If the user is logged in, retrieve their saved language
  if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.pixelhumain && Meteor.user().profile.pixelhumain.language) {
    return Meteor.user().profile.pixelhumain.language;
  }
  if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.language) {
    return Meteor.user().profile.language;
  }
  return undefined;
};

if (Meteor.isClient) {
  import position from './client/position.js';

  export const queryGeoFilter = (query) => {
    const radius = position.getRadius();
    const latlngObj = position.getLatlngObject();
    if (radius && latlngObj) {
      const nearObj = position.getNear();
      query.geoPosition = nearObj.geoPosition;
    } else {
      const city = position.getCity();
      if (city && city.geoShape && city.geoShape.coordinates) {
        query['address.codeInsee'] = city.insee;
      }
    }
    return query;
  };

  export const languageBrowser = () => {
    const localeFromBrowser = window.navigator.userLanguage || window.navigator.language;
    let locale = 'en';

    if (localeFromBrowser.match(/en/)) locale = 'en';
    if (localeFromBrowser.match(/fr/)) locale = 'fr';

    return locale;
  };

  export function scrollPagination() {
    const self = this;
    if (self.dataContext) {
      const showMoreVisible = () => {
        const $target = $('#showMoreResults');
        if (!$target.length) {
          return;
        }

        const threshold = $('.modal .content.has-header.overflow-scroll').scrollTop()
          + $('.modal .content.has-header.overflow-scroll').height() + 10;
        const heightLimit = $('.modal .content.has-header.overflow-scroll .list').height();
        // console.log('scopeHistoryActionsModal showMoreVisible', self.limit.get('limit'), self.limit.get('incremente'), threshold, heightLimit);
        if (heightLimit < threshold) {
          if (!$target.data('visible')) {
            $target.data('visible', true);
            self.limit.set('limit', self.limit.get('limit') + self.limit.get('incremente'));
          }
        } else if ($target.data('visible')) {
          $target.data('visible', false);
        }
      };

      $('.modal .content.has-header.overflow-scroll').scroll(showMoreVisible);
    } else {
      const showMoreVisible = () => {
        const $target = $('#showMoreResults');
        if (!$target.length) {
          return;
        }
        const threshold = $('.content.overflow-scroll').scrollTop()
          + $('.content.overflow-scroll').height() + 10;
        const heightLimit = $('.content.overflow-scroll .list').height();
        if (heightLimit < threshold) {
          if (!$target.data('visible')) {
            $target.data('visible', true);
            self.limit.set('limit', self.limit.get('limit') + self.limit.get('incremente'));
          }
        } else if ($target.data('visible')) {
          $target.data('visible', false);
        }
      };

      $('.content.overflow-scroll').scroll(showMoreVisible);
    }
  }
}

export const notifyDisplay = (notify, lang = null, html = false, chat = false) => {
  if (notify) {
    let label = notify.displayName;
    const arrayReplace = {};
    if (lang) {
      arrayReplace._locale = lang;
    }
    if (notify.displayName && notify.labelArray) {
      label = label.replace(new RegExp(/[\{\}]+/, 'g'), '');
      label = label.replace(new RegExp(' ', 'g'), '_');
      Object.keys(notify.labelArray).forEach((k) => {
        if (k === '{where}') {
          if (Array.isArray(notify.labelArray[k])) {
            const whereString = [];
            notify.labelArray[k].forEach((value) => {
              const labelWhere = value.replace(new RegExp(' ', 'g'), '_');
              const labelWhereIndex = `activitystream.notification.${labelWhere.replace(new RegExp('&amp;', 'g'), '&')}`;
              let labelWhereI18n = lang ? i18n.__(labelWhereIndex, { _locale: lang }) : i18n.__(labelWhereIndex);
              // correction
              if (labelWhereI18n !== labelWhereIndex) {
                whereString.push(labelWhereI18n);
              } else {
                whereString.push(value);
              }
            });
            arrayReplace.where = whereString.join(' ');
          } else {
            arrayReplace.where = '';
          }
        } else if (k === '{author}') {
          if (Array.isArray(notify.labelArray[k])) {
            arrayReplace.author = notify.labelArray[k].join(',');
          } else {
            arrayReplace.author = '';
          }
        } else if (k === '{mentions}') {
          if (Array.isArray(notify.labelArray[k])) {
            const mentionsString = [];
            notify.labelArray[k] = chat && notify.labelArray['{mentionsusername}'] ? notify.labelArray['{mentionsusername}'] : notify.labelArray[k];
            notify.labelArray[k].forEach((value) => {
              const labelMentions = value.replace(new RegExp(' ', 'g'), '_');
              const labelMentionsIndex = `activitystream.notification.${labelMentions.replace(new RegExp('&amp;', 'g'), '&')}`;
              let labelMentionsI18n = lang ? i18n.__(labelMentionsIndex, { _locale: lang }) : i18n.__(labelMentionsIndex);
              // correction
              if (labelMentionsI18n !== labelMentionsIndex) {
                mentionsString.push(labelMentionsI18n);
              } else {
                mentionsString.push(value);
              }
            });
            arrayReplace.mentions = mentionsString.join(' ');
            // arrayReplace.mentions = notify.labelArray[k].join(',');
          } else {
            arrayReplace.mentions = '';
          }
        } else if (k === '{what}') {
          if (Array.isArray(notify.labelArray[k])) {
            arrayReplace.what = notify.labelArray[k].join(',');
            // &quot;
            arrayReplace.what.replace(new RegExp('&quot;', 'g'), '');
          } else {
            arrayReplace.what = '';
          }
        } else if (k === '{task}') {
          if (Array.isArray(notify.labelArray[k])) {
            arrayReplace.task = notify.labelArray[k].join(',');
            // &quot;
            arrayReplace.task.replace(new RegExp('&quot;', 'g'), '');
          } else {
            arrayReplace.task = '';
          }
        } else if (k === '{who}') {
          if (Array.isArray(notify.labelArray[k])) {
            let whoNumber;
            const whoString = [];
            notify.labelArray[k] = chat && notify.labelArray['{whousername}'] ? notify.labelArray['{whousername}'] : notify.labelArray[k];
            notify.labelArray[k].forEach((value) => {
              if (Number.isInteger(value)) {
                whoNumber = lang ? i18n.__('activitystream.notification.whoNumber', { count: value, _locale: lang }) : i18n.__('activitystream.notification.whoNumber', { count: value });
              } else {
                whoString.push(value);
              }
            });
            if (whoString.length > 1 && whoNumber) {
              arrayReplace.who = `${whoString.join(',')}, ${whoNumber}`;
            } else if (whoString.length === 1 && !whoNumber) {
              arrayReplace.who = `${whoString.join(',')}`;
            } else if (whoString.length === 2 && !whoNumber) {
              arrayReplace.who = whoString.join(` ${i18n.__('activitystream.notification.and')} `);
            }
          } else {
            arrayReplace.who = '';
          }
        } else if (k === '{comment}') {
          if (Array.isArray(notify.labelArray[k])) {
            arrayReplace.comment = notify.labelArray[k].join(',');
          } else {
            arrayReplace.comment = '';
          }
        }
      });
      if (Meteor.isClient && html) {
        Object.keys(arrayReplace).forEach(function (item) {
          if (item === 'where') {
            arrayReplace[item] = `<strong style="color:${getBackgroundColor(arrayReplace[item])}">${arrayReplace[item]}</strong>`;
          } else {
            arrayReplace[item] = `<strong>${arrayReplace[item]}</strong>`;
          }
        });
      } else if (chat) {
        Object.keys(arrayReplace).forEach(function (item) {
          if (item === 'who' || item === 'author' || item === 'mentions') {
            // todo si array marche que pour 1
            arrayReplace[item] = `@${arrayReplace[item]}`;
          } else if (item === 'comment') {
            arrayReplace[item] = `\n _${arrayReplace[item]}_ \n`;
          } else if (item !== '_locale') {
            arrayReplace[item] = `*${arrayReplace[item].trim()}*`;
          }
        });

        if (notify.displayNameChat) {
          label = notify.displayNameChat;
          label = label.replace(new RegExp(/[\{\}]+/, 'g'), '');
          label = label.replace(new RegExp(' ', 'g'), '_');
        }
      }
      // {author} invited {who} to join {where}
      return lang ? i18n.__(`activitystream.notification.${label}`, arrayReplace) : i18n.__(`activitystream.notification.${label}`, arrayReplace);
    }
    return label;
  }
  return undefined;
};

export const matchTags = (doc, tags) => {
  // const regex = /(?:^|\s)(?:#)([a-zA-Z\d]+)/gm;
  // const regex = /(?:^|\s)(?:#)([^\s!@#$%^&*()=+./,\[{\]};:'"?><]+)/gm;
  // const regex = /(?:#)([^\s!@#$%^&*()=+./,\[{\]};:'"?><]+)/gm;
  const regex = /(?:#)([^\s!@#$%^*()=+./,\[{\]};:'"?><]+)/gm;

  const matches = [];
  let match;
  if (doc.shortDescription) {
    while ((match = regex.exec(doc.shortDescription))) {
      matches.push(match[1]);
    }
  }
  if (doc.description) {
    while ((match = regex.exec(doc.description))) {
      matches.push(match[1]);
    }
  }
  if (doc.tagsText) {
    while ((match = regex.exec(doc.tagsText))) {
      matches.push(match[1]);
    }
  }

  if (tags) {
    // const arrayTags = _.reject(tags, value => matches[value] === null, matches);
    const arrayTags = tags.filter((value) => matches[value] !== null);
    if (doc.tags) {
      const a = new Set([...doc.tags, ...arrayTags, ...matches]);
      doc.tags = [...a];
      // doc.tags = _.uniq(_.union(doc.tags, arrayTags, matches));
    } else {
      const a = new Set([...arrayTags, ...matches]);
      doc.tags = [...a];
      // doc.tags = _.uniq(_.union(arrayTags, matches));
    }
  } else if (matches.length > 0) {
    if (doc.tags) {
      const a = new Set([...doc.tags, ...matches]);
      doc.tags = [...a];
      // doc.tags = _.uniq(_.union(doc.tags, matches));
    } else {
      const a = new Set(matches);
      doc.tags = [...a];
      // doc.tags = _.uniq(matches);
    }
  }
  return doc;
};

export const removeObjectArray = (array, key, value) => {
  const index = array.findIndex((obj) => obj[key] === value);
  return index >= 0 ? [
    ...array.slice(0, index),
    ...array.slice(index + 1),
  ] : array;
};

export const isValidObjectId = (id) => {
  const isValid = (id) => {
    const checkForHexRegExp = new RegExp('^[0-9a-fA-F]{24}$');

    if (id == null) return false;
    if (typeof id === 'number') {
      return true;
    }
    if (typeof id === 'string') {
      return id.length === 12 || (id.length === 24 && checkForHexRegExp.test(id));
    }
    return false;
  };
  if (isValid(id)) {
    if ((String)(new Mongo.ObjectID(id).toHexString()) === id) {
      return true;
    }
    return false;
  }
  return false;
};

export const rolesUniquesArray = (list, typeLink) => {
  const rolesUniquesSet = list.reduce((ensembleRoles, objet) => {
    if (objet.links && objet.links[typeLink]) {
      const membres = objet.links[typeLink];
      Object.values(membres).forEach(membre => {
        if (membre.roles && Array.isArray(membre.roles)) {
          membre.roles.forEach(role => {
            if (role.trim() !== "") {
              ensembleRoles.add(role);
            }
          });
        }
      });
    }
    return ensembleRoles;
  }, new Set());

  // Convertir l'ensemble en un tableau contenant les rôles uniques et non vides
  const rolesUniques = Array.from(rolesUniquesSet);

  // Trier les rôles par ordre alphabétique
  return rolesUniques.sort();
};

export const actionIndicatorCountQuery = ({ status, parentId, parentType }) => {
  const query = {
    parentId,
    parentType,
  };

  const validStatus = ['all', 'contributors', 'finished', 'toValidated'];

  if (!validStatus.includes(status)) {
    query.status = status;
  } else if (status === 'contributors') {
    query['links.contributors'] = { $exists: true };
  } else if (status === 'finished') {
    query.finishedBy = { $exists: true };
  } else if (status === 'toValidated') {
    query.status = 'todo';
    query.finishedBy = { $exists: true };
  }

  const options = { sort: { created: 1 } };
  return { query, options };
};

export function createHelpers(...functions) {
  return functions.reduce((helpers, func) => {
    if (typeof func === 'function') {
      helpers[func.name] = func;
    }
    return helpers;
  }, {});
}
