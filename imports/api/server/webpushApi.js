import { Meteor } from 'meteor/meteor';
import webpush from 'web-push';
import { Webpushsub } from '../collection/webpushsub';

if (Meteor.settings?.vapidSubject && Meteor.settings?.public?.vapidPublicKey && Meteor.settings?.vapidPrivateKey) {
  webpush.setVapidDetails(
    Meteor.settings.vapidSubject,
    Meteor.settings.public.vapidPublicKey,
    Meteor.settings.vapidPrivateKey,
  );
}

export const sendWebNotification = async (subscription, dataToSend) => {
  try {
    await webpush.sendNotification(subscription, JSON.stringify(dataToSend));
    return true;
  } catch (error) {
    // console.error('Error sending notification, reason: ', error);
    // Ici, tu peux vérifier le type d'erreur et agir en conséquence
    if (error.statusCode === 410 || error.statusCode === 404) { // GCM/FCM renvoie souvent 410 pour les abonnements non valides
      // Appelle ta méthode pour supprimer l'abonnement invalide de ta base de données
      const endpoint = subscription.endpoint;
      await Webpushsub.removeAsync({
        'subscription.endpoint': endpoint
      });
    }
    return true;
  }
};