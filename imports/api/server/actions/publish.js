import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check, Match } from 'meteor/check';

import { Organizations } from '../../collection/organizations.js';
import { Citoyens } from '../../collection/citoyens.js';
import { arrayLinkParentNoObject, nameToCollection, optionsNoFieldsScope, queryOrPrivateScope } from '../../helpers.js';
import { Events } from '../../collection/events.js';
import { Actions } from '../../collection/actions.js';
import { moment } from 'meteor/momentjs:moment';
import { Lists } from '../../collection/lists.js';
import { Answers } from '../../collection/answers.js';
import { Notes } from '../../collection/notes.js';
import { orgaIdScope } from '../../helpersOrga.js';

Meteor.publish('all.actions', function (organizationId) {
  check(organizationId, String);

  if (!this.userId) {
    return null;
  }

  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(organizationId) }, { fields: { _id: 1 } });
  if (!orgaOne) {
    return null;
  }

  const allActions = orgaOne.actionsAllOpti();

  // Obtenir les IDs des citoyens à partir des actions
  const allIdsCitoyens = allActions.map((k) => {
    if (k.links && k.links.contributors) {
      return Object.keys(k.links.contributors);
    }
  }).filter(Boolean).flat();

  const uniqueIdsCitoyens = [...new Set(allIdsCitoyens)];
  const arrayAllMergeMongoId = uniqueIdsCitoyens.map((k) => new Mongo.ObjectID(k));

  return [allActions, Citoyens.find({ _id: { $in: arrayAllMergeMongoId } }, { fields: { name: 1, username: 1, profilThumbImageUrl: 1 } })];
});

Meteor.publishComposite('directoryProjectsListEventsActionsOpti', function (scope, scopeId, etat) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  check(etat, Match.Maybe(String));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['events', 'projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      // console.log(query);
      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
          if (scopeD.links && scopeD.links.projects) {
            const projectIds = arrayLinkParentNoObject(scopeD.links.projects, 'projects');
            const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
            const query = {};
            query.$or = [];

            projectIds.forEach((id) => {
              const queryCo = {};
              if (userC && userC.links && userC.links.projects && userC.links.projects[id] && userC.links.projects[id].isAdmin && !userC.links.projects[id].toBeValidated && !userC.links.projects[id].isAdminPending && !userC.links.projects[id].isInviting) {
                queryCo[`organizer.${id}`] = { $exists: true };
                query.$or.push(queryCo);
              }
            });
            if (query.$or.length === 0) {
              delete query.$or;
            }

            // queryOptions.fields.parentId = 1;
            // const inputDate = new Date();
            const inputDate = moment(new Date()).subtract(15, 'day').toDate();
            // query.startDate = { $lte: inputDate };
            query.endDate = { $gte: inputDate };

            const options = {};
            options.sort = {
              startDate: -1,
            };
            // console.log(query);
            return Events.find(query, options);
          }
          // return scopeD.listProjectsEventsCreator1M();
        }
      },
      children:
        [
          {
            find(scopeD) {
              const query = {};
              const inputDate = new Date();

              const queryone = {};
              queryone.endDate = { $exists: true, $lte: inputDate };
              if (etat) {
                queryone.status = etat;
              }
              queryone['links.contributors'] = { $exists: true };
              queryone.parentId = { $in: [scopeD._id.valueOf()] };

              const querytwo = {};
              if (etat) {
                querytwo.status = etat;
              }
              querytwo['links.contributors'] = { $exists: true };
              querytwo.endDate = { $exists: false };
              querytwo.parentId = { $in: [scopeD._id.valueOf()] };

              query.$or = [];
              query.$or.push(queryone);
              query.$or.push(querytwo);
              // * action d'events de projets de l'orga (sur des events des 15 dernier jours)
              return Actions.find(query);
            },
            children: [{
              find(scopeD) {
                return scopeD.listContributorsOpti();
              },
            }],
          },
        ],
    },
    {
      find(scopeD) {
        if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
          if (scopeD.isAdmin()) {
            return scopeD.listProjects();
          }
          return scopeD.listProjectsCreatorAdmin();
        }
      },
      children:
        [
          {
            find(scopeD) {
              const query = {};
              query.parentId = scopeD._id.valueOf();
              if (etat) {
                query.status = etat;
              }
              query.finishedBy = {
                $elemMatch: {
                  status: 'toModerate'
                }
              };

              query.answerId = { $exists: false };
              query['links.contributors'] = { $exists: true };
              return Actions.find(query);
            },
            children: [{
              find(scopeD) {
                return scopeD.listContributorsFinishedBy('toModerate');
              },
            }],
          },
        ],
    },
    {
      find(scopeD) {
        const query = {};
        query.parentId = scopeD._id.valueOf();
        if (etat) {
          query.status = etat;
        }
        query.finishedBy = {
          $elemMatch: {
            status: 'toModerate'
          }
        };
        query['links.contributors'] = { $exists: true };
        // * action de l'orga
        return Actions.find(query);
      },
      children: [{
        find(scopeD) {
          return scopeD.listContributorsFinishedBy('toModerate');
        },
      }],
    },
    ],
  };
});

Meteor.publishComposite('detailActionsOpti', function (scope, scopeId, roomId, actionId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(actionId, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'events', 'citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const orgId = orgaIdScope({ scope, scopeId });

  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(event) {
          if (scope === 'events') {
            const parent = event.organizerEvent();
            const collectionParent = nameToCollection(parent[0].type);
            const optionsParent = {};
            if (['citoyens', 'projects', 'organizations'].includes(parent[0].type)) {
              const { fields } = optionsNoFieldsScope(parent[0].type);
              optionsParent.fields = fields;
            }
            return collectionParent.find({
              _id: new Mongo.ObjectID(parent[0].values[0]._id.valueOf()),
            }, optionsParent);
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events' || scope === 'citoyens') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
      },
      {
        find() {
          if (orgId && orgId.length > 0) {
            const OrgaArrayIds = orgId.map((id) => new Mongo.ObjectID(id));
            return Organizations.find({ _id: { $in: [OrgaArrayIds['0']] } }, { fields: { _id: 1 } });
          }
        },
      },
      {
        find() {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events' || scope === 'citoyens') {
            return Actions.find({ _id: new Mongo.ObjectID(actionId) });
          }
        },
        children: [
          {
            find(action) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(action.creator),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
          {
            find(action) {
              return action.listContributors();
            },
          },
          {
            find(action) {
              return action.photoActionsAlbums();
            },
          },
          {
            find(action) {
              return action.docActionsList();
            },
          },
          {
            find(action) {
              if (action.answerId) {
                return Answers.find({ _id: new Mongo.ObjectID(action.answerId) }, { fields: { _id: 1, form: 1 } });
              }
            },
          },
          {
            find(action) {
              if (action.noteId) {
                return Notes.find({ _id: new Mongo.ObjectID(action.noteId) }, { fields: { _id: 1, doc: 1, parentType: 1, parentId: 1 } });
              }
            },
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('user.actionsOpti', function (scope, scopeId, etat) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations'].includes(name);
  }));
  check(etat, String);
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const UserId = `links.contributors.${this.userId}`;
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        if (scope === 'organizations') {
          return scopeD.listProjectsEventsCreator1M();
        }
      },
      children: [{
        find(scopeD) {
          const query = {};
          query.parentId = scopeD._id.valueOf();
          query[UserId] = {
            $exists: 1,
          };
          query.status = 'todo';
          const option = {};
          if (etat === 'aFaire') {
            query[`finishedBy`] = {
              $not: {
                $elemMatch: {
                  userId: this.userId
                }
              }
            };
            option.sort = { endDate: -1 };
          } else if (etat === 'enAttente') {
            query['finishedBy'] = {
              $elemMatch: {
                userId: this.userId,
                status: 'toModerate'
              }
            };
            option.sort = { endDate: -1 };
          } else if (etat === 'valides') {
            /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
            query['finishedBy'] = {
              $elemMatch: {
                userId: this.userId,
                status: 'validated'
              }
            };
            option.sort = { endDate: -1 };
            option.limit = 100;
          }
          // console.log(query);
          return Actions.find(query, option);
        },
      },
      ],
    },
    {
      find(scopeD) {
        if (scope === 'organizations') {
          return scopeD.listProjects();
        }
      },
      children: [{
        find(scopeD) {
          const query = {};
          query.parentId = scopeD._id.valueOf();
          query[UserId] = {
            $exists: 1,
          };
          query.status = 'todo';
          query.answerId = { $exists: false };
          const option = {};
          if (etat === 'aFaire') {
            query[`finishedBy`] = {
              $not: {
                $elemMatch: {
                  userId: this.userId
                }
              }
            };
            option.sort = { endDate: -1 };
          } else if (etat === 'enAttente') {
            query['finishedBy'] = {
              $elemMatch: {
                userId: this.userId,
                status: 'toModerate'
              }
            };
            option.sort = { endDate: -1 };
          } else if (etat === 'valides') {
            /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
            query['finishedBy'] = {
              $elemMatch: {
                userId: this.userId,
                status: 'validated'
              }
            };
            option.sort = { endDate: -1 };
            option.limit = 100;
          }
          // console.log(query);
          return Actions.find(query, option);
        },
      },
      ],
    },
    {
      find() {
        const query = {};
        query.parentId = scopeId;
        query[UserId] = {
          $exists: 1,
        };
        query.status = 'todo';
        const option = {};
        if (etat === 'aFaire') {
          query[`finishedBy`] = {
            $not: {
              $elemMatch: {
                userId: this.userId
              }
            }
          };
          option.sort = { endDate: -1 };
        } else if (etat === 'enAttente') {
          query['finishedBy'] = {
            $elemMatch: {
              userId: this.userId,
              status: 'toModerate'
            }
          };
          option.sort = { endDate: -1 };
        } else if (etat === 'valides') {
          /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
          query['finishedBy'] = {
            $elemMatch: {
              userId: this.userId,
              status: 'validated'
            }
          };
          option.sort = { endDate: -1 };
          option.limit = 100;
        }
        // console.log(query);
        return Actions.find(query, option);
      },
    },
    ],
  };
});

Meteor.publishComposite('user.actions.historiqueOpti', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const UserId = `links.contributors.${this.userId}`;
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return collection.find(query, options);
    },
    children: [{
      find(scopeD) {
        if (scope === 'organizations') {
          // return scopeD.listProjectsEventsCreator1M();
          if (scopeD.links && scopeD.links.projects) {
            const projectIds = arrayLinkParentNoObject(scopeD.links.projects, 'projects');

            let arrayEventsIds = [];
            // Construction de la condition pour récupérer les événements liés aux projets
            if (projectIds.length > 0) {
              // Calcul de la date 3 mois avant aujourd'hui
              const threeMonthsAgo = new Date();
              threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);
              const queryEventsArray = {
                $or: projectIds.map(id => ({ [`organizer.${id}`]: { $exists: true } })),
                endDate: { $gte: threeMonthsAgo } // Ajout de la condition sur endDate
              };
              arrayEventsIds = Events.find(queryEventsArray, { fields: { _id: 1 } })
                .map(event => event._id.valueOf());
            }

            const queryAction = {};
            queryAction.parentId = { $in: [...arrayEventsIds, ...projectIds, scopeId] };
            queryAction[UserId] = {
              $exists: 1,
            };
            const option = {};
            /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
            query['finishedBy'] = {
              $elemMatch: {
                userId: this.userId,
                status: 'validated'
              }
            };
            option.sort = { endDate: -1 };
            option.limit = 100;
            // console.log(queryAction);
            return Actions.find(queryAction, option);
          }
        }
      },
    },
    ],
  };
});

Meteor.publishComposite('all.user.actions', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const { fields } = optionsNoFieldsScope('citoyens');
      options.fields = fields;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.actionsUserAllOpti(scopeId);
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listOrganizationsCreator();
          }
        },
        children: [{
          find(orgaOne) {
            return orgaOne.actionsUserAllOpti(scopeId);
          },
          children: [{
            find(actionOne) {
              if (actionOne.parentType && actionOne.parentId) {
                const collection = nameToCollection(actionOne.parentType);
                return collection.find({ _id: new Mongo.ObjectID(actionOne.parentId) }, { fields: { _id: 1, name: 1, 'links.projects': 1, [`links.contributors.${this.userId}`]: 1, parent: 1, organizer: 1 } });
              }
            },
          }],
        }],
      },
    ],
  };
});