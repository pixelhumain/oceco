import { Meteor } from "meteor/meteor";
import { Actions } from "../../collection/actions.js";
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { arrayLinkToModerate, nameToCollection } from "../../helpers.js";
import { Organizations } from "../../collection/organizations.js";
import { Citoyens } from "../../collection/citoyens.js";
import { ActivityStream } from "../../collection/activitystream.js";
import { Mongo } from "meteor/mongo";
import SimpleSchema from 'simpl-schema';
import { Jobs } from 'meteor/wildhart:jobs';

import { LogUserActions } from "../../collection/loguseractions.js";
import { apiCommunecter } from "../api.js";
import { Rooms } from "../../collection/rooms.js";
import { Projects } from "../../collection/projects.js";
import { Events } from "../../collection/events.js";
import { orgaIdScope } from "../../helpersOrga.js";

// finishAction
// finishActionAdmin
// refundAdminAction
// ValidateAction
// noValidateAction
// assignmeActionRooms
// assignMemberActionRooms
// actionsType
// validateUserActions

export const updateFinishedByAction = (actionOne, userId, status) => {
  const userExists = actionOne.finishedBy && actionOne.finishedBy.some(item => item.userId === userId);
  if (userExists) {
    // Si l'utilisateur est présent, met à jour son statut
    return Actions.update(
      { _id: actionOne._id, "finishedBy.userId": userId },
      { $set: { "finishedBy.$.status": status } }
    );
  } else {
    // Si l'utilisateur n'est pas présent ou `finishedBy` est vide, ajoute l'utilisateur avec le statut 'toModerate'
    return Actions.update(
      { _id: actionOne._id },
      { $push: { finishedBy: { userId: userId, status: status } } }
    );
  }
};

export const orgIdWallet = (orgId) => {
  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(orgId) });
  if (orgaOne && orgaOne.maitre()) {
    return orgaOne.maitre()._id.valueOf();
  }
  return orgId;
};

export function typeOfNaN(x) {
  if (Number.isNaN(x)) {
    return true;
  }
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(x)) {
    return true;
  }
  return false;
}

export const countActionScope = (parentType, parentId, status) => {
  // ajouter
  /* actionsCount {
      todo : 1,
      done : 0,
      disabled: 0,
    } */
  const actionsCount = `actionsCount.${status}`;
  const collection = nameToCollection(parentType);
  const scopeOneExist = collection.findOne({ _id: new Mongo.ObjectID(parentId), [actionsCount]: { $exists: 1 } });
  if (!scopeOneExist) {
    // console.log('existe pas');
    if (status === 'all') {
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(parentId) });
      if (scopeOne) {
        const countAll = scopeOne.actionIndicatorCount('all').count();
        if (countAll > 0) {
          collection.update({ _id: new Mongo.ObjectID(parentId) }, { $set: { 'actionsCount.all': countAll } });
          let inc = 0;
          scopeOne.actionIndicatorCount('all').forEach((action) => {
            inc += 1;
            Actions.update({ _id: action._id }, { $set: { numberId: inc } });
          });
        }
      }
    } else {
      collection.update({ _id: new Mongo.ObjectID(parentId) }, { $set: { [actionsCount]: 1 } });
      if (status === 'done' || status === 'disabled') {
        collection.update({ _id: new Mongo.ObjectID(parentId) }, { $inc: { 'actionsCount.todo': -1 } });
      }
    }
  } else {
    collection.update({ _id: new Mongo.ObjectID(parentId) }, { $inc: { [actionsCount]: 1 } });
    // console.log('existe');
    if (status === 'done' || status === 'disabled') {
      collection.update({ _id: new Mongo.ObjectID(parentId) }, { $inc: { 'actionsCount.todo': -1 } });
    }
  }
  //
};

Meteor.methods({
  'finishAction'({ id }) {
    new SimpleSchema({
      id: { type: String },
    }).validate({ id });

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(id) });
    if (!actionOne) {
      throw new Meteor.Error('not-action');
    }

    if (actionOne.parentType === 'citoyens' && actionOne.parentId === this.userId) {
      const actionType = {};
      actionType.type = 'actions';
      actionType.id = actionOne._id.valueOf();
      actionType.parentType = actionOne.parentType;
      actionType.parentId = actionOne.parentId;
      actionType.name = 'status';
      actionType.value = 'done';
      Meteor.call('actionsType', actionType);
      return true;
    }

    if (!actionOne.isContributors()) {
      throw new Meteor.Error('is-not-contributor');
    }

    const actionId = new Mongo.ObjectID(id);

    updateFinishedByAction(actionOne, this.userId, 'toModerate');

    // notification

    if (actionOne && actionOne.max === 1 && actionOne.min === 1 && !actionOne.endDate) {
      Actions.update({ _id: actionId }, { $set: { endDate: new Date() } });
    }

    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
    };
    ActivityStream.api.add(notif, 'finish', 'isAdmin');

    return true;
  },
  'finishActionAdmin'({ actId, usrId, orgId }) {
    new SimpleSchema({
      actId: { type: String },
      usrId: { type: String },
      orgId: { type: String },
    }).validate({ actId, usrId, orgId });

    // je valide pas un user dans etat je le met
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if (!Organizations.findOne({
      _id: new Mongo.ObjectID(orgId),
    })) {
      throw new Meteor.Error('not-orga');
    }

    const action = Actions.findOne({ _id: new Mongo.ObjectID(actId) });

    if (!action) {
      throw new Meteor.Error('not-action');
    }
    const collection = nameToCollection(action.parentType);

    if (!collection.findOne({ _id: new Mongo.ObjectID(action.parentId) }).isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const actionId = new Mongo.ObjectID(actId);

    updateFinishedByAction(action, usrId, 'toModerate');

    // notification
    const actionOne = Actions.findOne({
      _id: actionId,
    });

    if (actionOne && actionOne.max === 1 && actionOne.min === 1 && !actionOne.endDate) {
      Actions.update({ _id: actionId }, { $set: { endDate: new Date() } });
    }

    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
    };
    ActivityStream.api.add(notif, 'finish', 'isAdmin');

    return true;
  },
  'refundAdminAction'({
    id, orgId, memberId,
  }) {
    new SimpleSchema({
      id: {
        type: String,
      },
      orgId: {
        type: String,
      },
      memberId: {
        type: String,
        optional: true,
      },
    }).validate({
      id, orgId, memberId,
    });

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const action = Actions.findOne({ _id: new Mongo.ObjectID(id) });

    if (!action) {
      throw new Meteor.Error('not-action');
    }

    if (!Organizations.findOne({
      _id: new Mongo.ObjectID(orgId),
    })) {
      throw new Meteor.Error('not-orga');
    }

    if (memberId) {
      // verifier admin
      const collection = nameToCollection(action.parentType);

      if (!collection.findOne({ _id: new Mongo.ObjectID(action.parentId) }).isAdmin()) {
        throw new Meteor.Error('not-admin');
      }
    }

    const logOne = LogUserActions.findOne({ actionId: id, userId: memberId });
    if (!logOne && !action.credits) {
      throw new Meteor.Error('not-log_credit');
    }

    // efface user de l'action
    const actionId = new Mongo.ObjectID(id);
    memberId = memberId || Meteor.userId();
    const parent = memberId ? `links.contributors.${memberId}` : `links.contributors.${Meteor.userId()}`;

    Actions.update({
      _id: actionId,
    }, {
      $unset: {
        [parent]: '',
      },
      $pull: {
        finishedBy: { userId: memberId }
      }
    });

    // remboursement user
    const orgIduserWallet = orgIdWallet(orgId);
    const creditsReverse = logOne && logOne.credits ? -(logOne.credits) : action.credits;
    const userCredit = `userWallet.${orgIduserWallet}.userCredits`;
    const userObjectId = new Mongo.ObjectID(memberId);
    Citoyens.update({ _id: userObjectId }, { $inc: { [userCredit]: creditsReverse } });

    // log remboursement
    const logInsert = {};
    logInsert.userId = memberId;
    logInsert.organizationId = orgIduserWallet;
    if (orgIduserWallet !== orgId) {
      logInsert.subOrganizationId = orgId;
    }
    logInsert.actionId = id;
    logInsert.commentaire = 'Remboursement';
    logInsert.credits = creditsReverse;
    logInsert.createdAt = new Date();
    LogUserActions.insert(logInsert);

    // notification
    const actionOne = Actions.findOne({
      _id: new Mongo.ObjectID(id),
    });

    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
    };

    if (memberId) {
      // mention
      const mentionOne = Citoyens.findOne({ _id: new Mongo.ObjectID(memberId) });
      notif.mention = {
        id: mentionOne._id.valueOf(), name: mentionOne.name, type: 'citoyens', username: mentionOne.username,
      };
      ActivityStream.api.add(notif, 'refundUser', 'isAdmin');
      ActivityStream.api.add(notif, 'refundUser', 'isUser', null, memberId);
    } else {
      ActivityStream.api.add(notif, 'refund', 'isAdmin');
    }

    return true;
  },
  'ValidateAction'({ actId, usrId, orgId }) {
    new SimpleSchema({
      actId: { type: String },
      usrId: { type: String },
      orgId: { type: String },
    }).validate({ actId, usrId, orgId });

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const action = Actions.findOne({ _id: new Mongo.ObjectID(actId) });

    if (!action) {
      throw new Meteor.Error('not-action');
    }
    const collection = nameToCollection(action.parentType);

    if (!collection.findOne({ _id: new Mongo.ObjectID(action.parentId) }).isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const userStatus = action.finishedBy.find(item => item.userId === usrId)?.status;
    if (userStatus === 'validated') {
      throw new Meteor.Error('user-validated');
    }

    const actionId = new Mongo.ObjectID(actId);
    const userNeed = new Mongo.ObjectID(usrId);

    const credit = Actions.findOne({ _id: actionId }) && Actions.findOne({ _id: actionId }).credits && !typeOfNaN(Actions.findOne({ _id: actionId }).credits) ? parseInt(Actions.findOne({ _id: actionId }).credits) : 0;

    const orgIduserWallet = orgIdWallet(orgId);
    const userCredits = `userWallet.${orgIduserWallet}.userCredits`;
    if (!Citoyens.findOne({ _id: userNeed, [userCredits]: { $exists: 1 } })) {
      Citoyens.update({ _id: userNeed }, { $set: { [userCredits]: 0 } });
    }

    updateFinishedByAction(action, usrId, 'validated');

    // log user action credit
    const logInsert = {};
    logInsert.userId = usrId;
    logInsert.organizationId = orgIduserWallet;
    if (orgIduserWallet !== orgId) {
      logInsert.subOrganizationId = orgId;
    }
    logInsert.actionId = actId;
    if (credit) {
      logInsert.credits = credit;
      logInsert.createdAt = new Date();
      LogUserActions.insert(logInsert);
    }

    Citoyens.update({ _id: userNeed }, { $inc: { [userCredits]: credit } });

    // verifier si tout les users sont valider
    const actionOne = Actions.findOne({ _id: actionId });
    if (actionOne.finishedBy && actionOne.countContributors() === actionOne.finishedBy.length && arrayLinkToModerate(actionOne.finishedBy).length === 0) {
      Meteor.call('actionsType', {
        parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: actId, name: 'status', value: 'done',
      });
    }
    //

    // notification
    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
    };

    ActivityStream.api.add(notif, 'validate', 'isUser', null, usrId);

    return true;
  },
  'noValidateAction'({ actId, usrId, orgId }) {
    new SimpleSchema({
      actId: { type: String },
      usrId: { type: String },
      orgId: { type: String },
    }).validate({ actId, usrId, orgId });

    // je valide pas un user dans etat je le met
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const action = Actions.findOne({ _id: new Mongo.ObjectID(actId) });

    if (!action) {
      throw new Meteor.Error('not-action');
    }
    const collection = nameToCollection(action.parentType);

    if (!collection.findOne({ _id: new Mongo.ObjectID(action.parentId) }).isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const userStatus = action.finishedBy.find(item => item.userId === usrId)?.status;
    if (userStatus === 'novalidated') {
      throw new Meteor.Error('user-novalidated');
    }

    const actionId = new Mongo.ObjectID(actId);

    updateFinishedByAction(action, usrId, 'novalidated');

    const actionOne = Actions.findOne({ _id: actionId });
    if (actionOne.finishedBy && actionOne.countContributors() === actionOne.finishedBy.length && arrayLinkToModerate(actionOne.finishedBy).length === 0) {
      Meteor.call('actionsType', {
        parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: actId, name: 'status', value: 'done',
      });
    }


    // notification
    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
    };

    ActivityStream.api.add(notif, 'noValidate', 'isUser', null, usrId);

    return true;
  },
});


export const assignmeActionRooms = new ValidatedMethod({
  name: 'assignmeActionRooms',
  validate: new SimpleSchema({
    id: { type: String },
    startDate: {
      type: String,
      optional: true,
    },
    endDate: {
      type: String,
      optional: true,
    },
    credits: {
      type: Number,
      optional: true,
    },
  }).validator(),
  run({
    id, startDate, endDate, credits,
  }) {
    const actionObjectId = new Mongo.ObjectID(id);
    const actionOne = Actions.findOne({ _id: actionObjectId });
    const parentObjectId = new Mongo.ObjectID(actionOne.parentId);
    const orgOne = Organizations.findOne({ _id: parentObjectId }, { fields: { _id: 1 } });
    let orgId;
    if (orgOne) {
      orgId = orgOne._id.valueOf();
    } else {
      if (actionOne && actionOne.parentType !== 'citoyens') {
        const orgIdArray = orgaIdScope({ scope: 'actions', scopeId: id });
        orgId = orgIdArray && orgIdArray.length > 0 ? orgIdArray[0] : false;
      } else {
        orgId = false;
      }
    }



    const orgaSetting = Organizations.findOne({ _id: new Mongo.ObjectID(orgId) });

    const orgIduserWallet = orgIdWallet(orgId);

    function userCredits() {
      const userObjId = new Mongo.ObjectID(Meteor.userId());
      const credits = Citoyens.findOne({ _id: userObjId });
      return credits && credits.userWallet && credits.userWallet[`${orgIduserWallet}`] && credits.userWallet[`${orgIduserWallet}`].userCredits ? credits.userWallet[`${orgIduserWallet}`].userCredits : null;
    }

    function walletIsOk(id) {
      const cost = Actions.findOne({ _id: new Mongo.ObjectID(id) }) && Actions.findOne({ _id: new Mongo.ObjectID(id) }).credits && !typeOfNaN(Actions.findOne({ _id: new Mongo.ObjectID(id) }).credits) ? parseInt(Actions.findOne({ _id: new Mongo.ObjectID(id) }).credits) : 0;
      if (cost >= 0) {
        return true;
      } if (userCredits() >= (cost * -1)) {
        const userCredit = `userWallet.${orgIduserWallet}.userCredits`;
        const userObjectId = new Mongo.ObjectID(Meteor.userId());
        if (!Citoyens.findOne({ _id: userObjectId, [userCredit]: { $exists: 1 } })) {
          Citoyens.update({ _id: userObjectId }, { $set: { [userCredit]: 0 } });
        }

        updateFinishedByAction(actionOne, Meteor.userId(), 'validated');
        Citoyens.update({ _id: userObjectId }, { $inc: { [userCredit]: cost } });
        // log user action credit
        const logInsert = {};
        logInsert.userId = Meteor.userId();
        logInsert.organizationId = orgIduserWallet;
        if (orgIduserWallet !== orgId) {
          logInsert.subOrganizationId = orgId;
        }
        logInsert.actionId = id;
        if (cost) {
          logInsert.credits = cost;
          logInsert.createdAt = new Date();
          LogUserActions.insert(logInsert);
        }
        return true;
      } if (orgaSetting && orgaSetting.oceco && orgaSetting.oceco.spendNegative === true && ((orgaSetting.oceco.spendNegativeMax - userCredits()) * -1) >= (cost * -1)) {
        const userCredit = `userWallet.${orgIduserWallet}.userCredits`;
        const userObjectId = new Mongo.ObjectID(Meteor.userId());
        if (!Citoyens.findOne({ _id: userObjectId, [userCredit]: { $exists: 1 } })) {
          Citoyens.update({ _id: userObjectId }, { $set: { [userCredit]: 0 } });
        }

        updateFinishedByAction(actionOne, Meteor.userId(), 'validated');
        Citoyens.update({ _id: userObjectId }, { $inc: { [userCredit]: cost } });
        // log user action credit
        const logInsert = {};
        logInsert.userId = Meteor.userId();
        logInsert.organizationId = orgIduserWallet;
        if (orgIduserWallet !== orgId) {
          logInsert.subOrganizationId = orgId;
        }
        logInsert.actionId = id;
        if (cost) {
          logInsert.credits = cost;
          logInsert.createdAt = new Date();
          LogUserActions.insert(logInsert);
        }
        return true;
      }

      return false;
    }

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(orgId) });
    // projects auto true
    const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
    if (userC && actionOne.parentType === 'projects') {
      if (!userC.isScope('projects', actionOne.parentId)) {
        if (orgaOne && orgaOne.oceco && orgaOne.oceco.contributorAuto) {
          if (userC.isInviting('projects', actionOne.parentId)) {
            if (orgaOne && orgaOne.oceco && orgaOne.oceco.contributorAuto) {
              Citoyens.update({
                _id: new Mongo.ObjectID(userC._id.valueOf()),
              }, {
                $unset: {
                  [`links.projects.${actionOne.parentId}.toBeValidated`]: '',
                  [`links.projects.${actionOne.parentId}.isInviting`]: '',
                },
              });

              Projects.update({
                _id: new Mongo.ObjectID(actionOne.parentId),
              }, {
                $unset: {
                  [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                  [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
                },
              });
            }
          } else {
            const retour = Meteor.call('connectEntity', actionOne.parentId, 'projects', userC._id.valueOf(), 'contributor', orgId);
            if (orgaOne && orgaOne.oceco && orgaOne.oceco.contributorAuto) {
              Citoyens.update({
                _id: new Mongo.ObjectID(userC._id.valueOf()),
              }, {
                $unset: {
                  [`links.projects.${actionOne.parentId}.toBeValidated`]: '',
                  [`links.projects.${actionOne.parentId}.isInviting`]: '',
                },
              });

              Projects.update({
                _id: new Mongo.ObjectID(actionOne.parentId),
              }, {
                $unset: {
                  [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                  [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
                },
              });
            }
          }
        }
      }
    } else if (userC && actionOne.parentType === 'events') {
      if (!userC.isScope('events', actionOne.parentId)) {
        if (orgaOne && orgaOne.oceco && orgaOne.oceco.attendeAuto) {
          if (userC.isInviting('events', actionOne.parentId)) {
            if (orgaOne && orgaOne.oceco && orgaOne.oceco.attendeAuto) {
              Citoyens.update({
                _id: new Mongo.ObjectID(userC._id.valueOf()),
              }, {
                $unset: {
                  [`links.events.${actionOne.parentId}.toBeValidated`]: '',
                  [`links.events.${actionOne.parentId}.isInviting`]: '',
                },
              });

              Events.update({
                _id: new Mongo.ObjectID(actionOne.parentId),
              }, {
                $unset: {
                  [`links.attendees.${userC._id.valueOf()}.toBeValidated`]: '',
                  [`links.attendees.${userC._id.valueOf()}.isInviting`]: '',
                },
              });
            }
          } else {
            const retour = Meteor.call('connectEntity', actionOne.parentId, 'events', userC._id.valueOf(), 'attendee', orgId);
            if (orgaOne && orgaOne.oceco && orgaOne.oceco.attendeAuto) {
              Citoyens.update({
                _id: new Mongo.ObjectID(userC._id.valueOf()),
              }, {
                $unset: {
                  [`links.events.${actionOne.parentId}.toBeValidated`]: '',
                  [`links.events.${actionOne.parentId}.isInviting`]: '',
                },
              });

              Events.update({
                _id: new Mongo.ObjectID(actionOne.parentId),
              }, {
                $unset: {
                  [`links.attendees.${userC._id.valueOf()}.toBeValidated`]: '',
                  [`links.attendees.${userC._id.valueOf()}.isInviting`]: '',
                },
              });
            }
          }

          const event = `links.events.${actionOne.parentId}`;
          const project = Projects.findOne({ [event]: { $exists: 1 } });
          if (project && project._id && project._id.valueOf()) {
            if (!userC.isScope('projects', project._id.valueOf())) {
              if (userC.isInviting('projects', project._id.valueOf())) {
                Citoyens.update({
                  _id: new Mongo.ObjectID(userC._id.valueOf()),
                }, {
                  $unset: {
                    [`links.projects.${project._id.valueOf()}.toBeValidated`]: '',
                    [`links.projects.${project._id.valueOf()}.isInviting`]: '',
                  },
                });

                Projects.update({
                  _id: project._id,
                }, {
                  $unset: {
                    [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                    [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
                  },
                });
              } else {
                const retour = Meteor.call('connectEntity', project._id.valueOf(), 'projects', userC._id.valueOf(), 'contributor', orgId);
                Citoyens.update({
                  _id: new Mongo.ObjectID(userC._id.valueOf()),
                }, {
                  $unset: {
                    [`links.projects.${project._id.valueOf()}.toBeValidated`]: '',
                    [`links.projects.${project._id.valueOf()}.isInviting`]: '',
                  },
                });

                Projects.update({
                  _id: project._id,
                }, {
                  $unset: {
                    [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                    [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
                  },
                });
              }
            }
          }
        }
      }
    }

    // TODO verifier si id est une room existante et les droit pour ce l'assigner
    // id action > recupérer idParentRoom,parentType,parentId > puis roles dans room
    const action = Actions.findOne({ _id: new Mongo.ObjectID(id), status: 'todo' });
    if (!action) {
      throw new Meteor.Error('not-authorized action');
    } else {
      const room = Rooms.findOne({ _id: new Mongo.ObjectID(action.idParentRoom) });
      if (!room) {
        throw new Meteor.Error('not-authorized room');
      }
    }
    if (orgId && !walletIsOk(id)) {
      throw new Meteor.Error('Pas assé de');
    }
    const docRetour = {};
    docRetour.id = id;
    const retour = apiCommunecter.postPixel('co2/rooms', 'assignme', docRetour);

    if (!action.startDate) {
      if (startDate) {
        Actions.update({ _id: actionObjectId }, { $set: { startDate: new Date(startDate), noStartDate: true } });
      } else {
        Actions.update({ _id: actionObjectId }, { $set: { startDate: new Date(), noStartDate: true } });
      }
    }

    if (action.startDate && action.isPossibleStartActionBeforeStartDate()) {
      if (startDate) {
        Actions.update({ _id: actionObjectId }, { $set: { startDate: new Date(startDate), noStartDate: true } });
      }
    }

    if (!action.endDate) {
      if (endDate) {
        Actions.update({ _id: actionObjectId }, { $set: { endDate: new Date(endDate), noEndDate: true } });
      }
    }

    if (!action.min) {
      Actions.update({ _id: actionObjectId }, { $set: { min: 1 } });
    }
    if (!action.max) {
      Actions.update({ _id: actionObjectId }, { $set: { max: 1 } });
    }
    if (!action.credits) {
      if (credits && action.options && action.options.creditAddPorteur) {
        Actions.update({ _id: actionObjectId }, { $set: { credits } });
      } else {
        Actions.update({ _id: actionObjectId }, { $set: { credits: 0 } });
      }
    }

    // notification
    if (action.parentType !== 'citoyens') {
      const notif = {};
      const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
        fields: {
          _id: 1, name: 1, email: 1, username: 1,
        },
      });
      // author
      notif.author = {
        id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
      };
      // object
      notif.object = {
        id: action._id.valueOf(), name: action.name, type: 'actions', parentType: action.parentType, parentId: action.parentId, idParentRoom: action.idParentRoom,
      };

      if (action.isActionDepense()) {
        ActivityStream.api.add(notif, 'joinSpent', 'isAdmin');
      } else {
        ActivityStream.api.add(notif, 'join', 'isAdmin');
      }
    }

    return retour;
  },
});

export const assignMemberActionRooms = new ValidatedMethod({
  name: 'assignMemberActionRooms',
  validate: new SimpleSchema({
    id: { type: String },
    memberId: { type: String },
    startDate: {
      type: String,
      optional: true,
    },
    endDate: {
      type: String,
      optional: true,
    },
  }).validator(),
  run({
    id, memberId, startDate, endDate,
  }) {
    const actionObjectId = new Mongo.ObjectID(id);
    const actionOne = Actions.findOne({ _id: actionObjectId });
    const parentObjectId = new Mongo.ObjectID(actionOne.parentId);
    const { parentType } = actionOne;
    const collection = nameToCollection(parentType);

    const orgOne = Organizations.findOne({ _id: parentObjectId });
    let orgId;
    if (orgOne) {
      orgId = orgOne._id.valueOf();
    } else {
      const orgIdArray = orgaIdScope({ scope: 'actions', scopeId: id });
      orgId = orgIdArray && orgIdArray.length > 0 ? orgIdArray[0] : false;
    }


    const orgIduserWallet = orgIdWallet(orgId);

    const orgaSetting = Organizations.findOne({ _id: new Mongo.ObjectID(orgId) }, { fields: { _id: 1, oceco: 1 } });

    function userCredits() {
      const userObjId = new Mongo.ObjectID(memberId);
      const credits = Citoyens.findOne({ _id: userObjId });
      return credits && credits.userWallet && credits.userWallet[`${orgIduserWallet}`] && credits.userWallet[`${orgIduserWallet}`].userCredits ? credits.userWallet[`${orgIduserWallet}`].userCredits : null;
    }

    function walletIsOk(id) {
      const cost = Actions.findOne({ _id: new Mongo.ObjectID(id) }) && Actions.findOne({ _id: new Mongo.ObjectID(id) }).credits && !typeOfNaN(Actions.findOne({ _id: new Mongo.ObjectID(id) }).credits) ? parseInt(Actions.findOne({ _id: new Mongo.ObjectID(id) }).credits) : 0;
      if (cost >= 0) {
        return true;
      } if (userCredits() >= (cost * -1)) {

        const userCredit = `userWallet.${orgIduserWallet}.userCredits`;
        const userObjectId = new Mongo.ObjectID(memberId);
        if (!Citoyens.findOne({ _id: userObjectId, [userCredit]: { $exists: 1 } })) {
          Citoyens.update({ _id: userObjectId }, { $set: { [userCredit]: 0 } });
        }

        updateFinishedByAction(actionOne, memberId, 'validated');

        Citoyens.update({ _id: userObjectId }, { $inc: { [userCredit]: cost } });
        // log user action credit
        const logInsert = {};
        logInsert.userId = memberId;
        logInsert.organizationId = orgIduserWallet;
        if (orgIduserWallet !== orgId) {
          logInsert.subOrganizationId = orgId;
        }
        logInsert.actionId = id;
        if (cost) {
          logInsert.credits = cost;
          logInsert.createdAt = new Date();
          LogUserActions.insert(logInsert);
        }
        return true;
      } if (orgaSetting && orgaSetting.oceco && orgaSetting.oceco.spendNegative === true && ((orgaSetting.oceco.spendNegativeMax - userCredits()) * -1) >= (cost * -1)) {
        const userCredit = `userWallet.${orgIduserWallet}.userCredits`;
        const userObjectId = new Mongo.ObjectID(memberId);
        if (!Citoyens.findOne({ _id: userObjectId, [userCredit]: { $exists: 1 } })) {
          Citoyens.update({ _id: userObjectId }, { $set: { [userCredit]: 0 } });
        }

        updateFinishedByAction(actionOne, memberId, 'validated');
        Citoyens.update({ _id: userObjectId }, { $inc: { [userCredit]: cost } });
        // log user action credit
        const logInsert = {};
        logInsert.userId = memberId;
        logInsert.organizationId = orgIduserWallet;
        if (orgIduserWallet !== orgId) {
          logInsert.subOrganizationId = orgId;
        }
        logInsert.actionId = id;
        if (cost) {
          logInsert.credits = cost;
          logInsert.createdAt = new Date();
          LogUserActions.insert(logInsert);
        }
        return true;
      }

      return false;
    }

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    // verifier si admin
    if (!collection.findOne({ _id: parentObjectId }).isAdmin()) {
      if (!(orgaSetting.oceco && orgaSetting.oceco.memberAddAction && Actions.findOne({ _id: actionObjectId }).isCreator() && memberId === this.userId)) {
        throw new Meteor.Error('not-authorized-admin');
      }
    }

    // verifier si member existe
    if (!Citoyens.findOne({ _id: new Mongo.ObjectID(memberId) })) {
      throw new Meteor.Error('not-exist-member-id');
    }
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(orgId) });
    // projects auto true
    const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(memberId) }, { fields: { pwd: 0 } });
    if (userC && parentType === 'projects') {
      // verifier si membre orga
      // testMembers
      if (!userC.isScope('organizations', orgId)) {
        const retour = Meteor.call('connectEntity', orgId, 'organizations', userC._id.valueOf(), 'member');
        Citoyens.update({
          _id: new Mongo.ObjectID(userC._id.valueOf()),
        }, {
          $unset: {
            [`links.memberOf.${orgId}.toBeValidated`]: '',
            [`links.memberOf.${orgId}.isInviting`]: '',
          },
        });

        Organizations.update({
          _id: new Mongo.ObjectID(orgId),
        }, {
          $unset: {
            [`links.members.${userC._id.valueOf()}.toBeValidated`]: '',
            [`links.members.${userC._id.valueOf()}.isInviting`]: '',
          },
        });
        // todo faire email
      }
      //
      if (!userC.isScope('projects', actionOne.parentId)) {
        if (orgaOne && orgaOne.oceco && orgaOne.oceco.contributorAuto) {
          if (userC.isInviting('projects', actionOne.parentId)) {
            Citoyens.update({
              _id: new Mongo.ObjectID(userC._id.valueOf()),
            }, {
              $unset: {
                [`links.projects.${actionOne.parentId}.toBeValidated`]: '',
                [`links.projects.${actionOne.parentId}.isInviting`]: '',
              },
            });

            Projects.update({
              _id: new Mongo.ObjectID(actionOne.parentId),
            }, {
              $unset: {
                [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
              },
            });
          } else {
            const retour = Meteor.call('connectEntity', actionOne.parentId, 'projects', userC._id.valueOf(), 'contributor', orgId);
            Citoyens.update({
              _id: new Mongo.ObjectID(userC._id.valueOf()),
            }, {
              $unset: {
                [`links.projects.${actionOne.parentId}.toBeValidated`]: '',
                [`links.projects.${actionOne.parentId}.isInviting`]: '',
              },
            });

            Projects.update({
              _id: new Mongo.ObjectID(actionOne.parentId),
            }, {
              $unset: {
                [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
              },
            });
          }
        }
      }
    } else if (userC && parentType === 'events') {
      if (!userC.isScope('events', actionOne.parentId)) {
        if (orgaOne && orgaOne.oceco && orgaOne.oceco.attendeAuto) {
          if (userC.isInviting('events', actionOne.parentId)) {
            Citoyens.update({
              _id: new Mongo.ObjectID(userC._id.valueOf()),
            }, {
              $unset: {
                [`links.events.${actionOne.parentId}.toBeValidated`]: '',
                [`links.events.${actionOne.parentId}.isInviting`]: '',
              },
            });

            Events.update({
              _id: new Mongo.ObjectID(actionOne.parentId),
            }, {
              $unset: {
                [`links.attendees.${userC._id.valueOf()}.toBeValidated`]: '',
                [`links.attendees.${userC._id.valueOf()}.isInviting`]: '',
              },
            });
          } else {
            const retour = Meteor.call('connectEntity', actionOne.parentId, 'events', userC._id.valueOf(), 'attendee', orgId);
            Citoyens.update({
              _id: new Mongo.ObjectID(userC._id.valueOf()),
            }, {
              $unset: {
                [`links.events.${actionOne.parentId}.toBeValidated`]: '',
                [`links.events.${actionOne.parentId}.isInviting`]: '',
              },
            });

            Events.update({
              _id: new Mongo.ObjectID(actionOne.parentId),
            }, {
              $unset: {
                [`links.attendees.${userC._id.valueOf()}.toBeValidated`]: '',
                [`links.attendees.${userC._id.valueOf()}.isInviting`]: '',
              },
            });
          }

          const event = `links.events.${actionOne.parentId}`;
          const project = Projects.findOne({ [event]: { $exists: 1 } });
          if (project && project._id && project._id.valueOf()) {
            if (!userC.isScope('projects', project._id.valueOf())) {
              if (userC.isInviting('projects', project._id.valueOf())) {
                Citoyens.update({
                  _id: new Mongo.ObjectID(userC._id.valueOf()),
                }, {
                  $unset: {
                    [`links.projects.${project._id.valueOf()}.toBeValidated`]: '',
                    [`links.projects.${project._id.valueOf()}.isInviting`]: '',
                  },
                });

                Projects.update({
                  _id: project._id,
                }, {
                  $unset: {
                    [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                    [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
                  },
                });
              } else {
                const retour = Meteor.call('connectEntity', project._id.valueOf(), 'projects', userC._id.valueOf(), 'contributor', orgId);
                Citoyens.update({
                  _id: new Mongo.ObjectID(userC._id.valueOf()),
                }, {
                  $unset: {
                    [`links.projects.${project._id.valueOf()}.toBeValidated`]: '',
                    [`links.projects.${project._id.valueOf()}.isInviting`]: '',
                  },
                });

                Projects.update({
                  _id: project._id,
                }, {
                  $unset: {
                    [`links.contributors.${userC._id.valueOf()}.toBeValidated`]: '',
                    [`links.contributors.${userC._id.valueOf()}.isInviting`]: '',
                  },
                });
              }
            }
          }
        }
      }
    }

    // TODO verifier si id est une room existante et les droit pour ce l'assigner
    // id action > recupérer idParentRoom,parentType,parentId > puis roles dans room
    const action = Actions.findOne({ _id: new Mongo.ObjectID(id), status: 'todo' });
    if (!action) {
      throw new Meteor.Error('not-authorized-action');
    } else {
      const room = Rooms.findOne({ _id: new Mongo.ObjectID(action.idParentRoom) });
      if (!room) {
        throw new Meteor.Error('not-authorized-room');
      }
    }
    if (!walletIsOk(id)) {
      throw new Meteor.Error('wallet-no-ok');
    }


    const contributor = `links.contributors.${memberId}`;
    const retour = Actions.update({ _id: new Mongo.ObjectID(id) }, { $set: { [contributor]: { type: 'citoyens' } } });

    if (!action.startDate) {
      if (startDate) {
        Actions.update({ _id: actionObjectId }, { $set: { startDate: new Date(startDate), noStartDate: true } });
      } else {
        Actions.update({ _id: actionObjectId }, { $set: { startDate: new Date(), noStartDate: true } });
      }
    }

    if (!action.endDate) {
      if (endDate) {
        Actions.update({ _id: actionObjectId }, { $set: { endDate: new Date(endDate), noEndDate: true } });
      }
    }

    if (!action.min) {
      Actions.update({ _id: actionObjectId }, { $set: { min: 1 } });
    }
    if (!action.max) {
      Actions.update({ _id: actionObjectId }, { $set: { max: 1 } });
    }
    if (!action.credits) {
      Actions.update({ _id: actionObjectId }, { $set: { credits: 0 } });
    }

    // si action max est plus petit que le nombre de contributor
    const countContrib = action.listContributors() ? action.listContributors().count() + 1 : 1;
    if (action.max < countContrib) {
      Actions.update({ _id: actionObjectId }, { $set: { max: countContrib } });
    }

    // notification
    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: action._id.valueOf(), name: action.name, type: 'actions', parentType: action.parentType, parentId: action.parentId, idParentRoom: action.idParentRoom,
    };
    // mention
    const mentionOne = Citoyens.findOne({ _id: new Mongo.ObjectID(memberId) });
    notif.mention = {
      id: mentionOne._id.valueOf(), name: mentionOne.name, type: 'citoyens', username: mentionOne.username,
    };

    if (action.isActionDepense()) {
      ActivityStream.api.add(notif, 'joinAssignSpent', 'isAdmin');
    } else {
      ActivityStream.api.add(notif, 'joinAssign', 'isAdmin');
      ActivityStream.api.add(notif, 'joinAssign', 'isUser', null, memberId);
    }

    return retour;
  },
});

export const actionsType = new ValidatedMethod({
  name: 'actionsType',
  validate: new SimpleSchema({
    parentType: { type: String, allowedValues: ['projects', 'organizations', 'events', 'citoyens'] },
    parentId: { type: String },
    type: { type: String, allowedValues: ['actions', 'proposals'] },
    id: { type: String },
    name: { type: String, allowedValues: ['status'] },
    value: { type: String, allowedValues: ['done', 'disabled', 'amendable', 'tovote', 'todo'] },
  }).validator(),
  run({
    parentType, parentId, type, id, name, value,
  }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const collection = nameToCollection(parentType);
    const collectionType = nameToCollection(type);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(parentId) });
    const scopeTypeOne = collectionType.findOne({ _id: new Mongo.ObjectID(id) });

    if (!scopeTypeOne) {
      throw new Meteor.Error('not-authorized');
    }

    if (parentType === 'citoyens') {
      if (!(scopeOne.isMe())) {
        throw new Meteor.Error('not-authorized');
      }
    } else if (!(scopeOne.isAdmin() || scopeTypeOne.isCreator())) {
      throw new Meteor.Error('not-authorized');
    }

    const docRetour = {};
    docRetour.parentType = parentType;
    docRetour.parentId = parentId;
    docRetour.type = type;
    docRetour.id = id;
    docRetour.name = name;
    docRetour.value = value;
    const retour = apiCommunecter.postPixel('co2/element', 'updatefield', docRetour);

    if (type === 'actions' && value === 'done') {
      if (parentType !== 'citoyens') {
        countActionScope(parentType, parentId, 'done');

        if (scopeTypeOne && scopeTypeOne.jobIdAlert && scopeTypeOne.jobIdStart && scopeTypeOne.jobIdEnd) {
          Jobs.remove(scopeTypeOne.jobIdAlert);
          Jobs.remove(scopeTypeOne.jobIdStart);
          Jobs.remove(scopeTypeOne.jobIdEnd);
        }

        // notification
        const actionOne = Actions.findOne({
          _id: new Mongo.ObjectID(id),
        });

        // && addEndDate === true
        if (actionOne && !actionOne.endDate) {
          Actions.update({ _id: new Mongo.ObjectID(id) }, { $set: { endDate: new Date(), noEndDate: true } });
        }

        const notif = {};
        const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
          fields: {
            _id: 1, name: 1, email: 1, username: 1,
          },
        });
        // author
        notif.author = {
          id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
        };
        // object
        notif.object = {
          id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', links: actionOne.links, parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
        };

        ActivityStream.api.add(notif, 'actionDone', 'isAdmin');
      } else {
        // si done en citoyens alors terminer/valider auto
        const actionId = new Mongo.ObjectID(id);

        updateFinishedByAction(scopeTypeOne, Meteor.userId(), 'validated');


        // notification
        const actionOne = Actions.findOne({
          _id: new Mongo.ObjectID(id),
        });

        if (actionOne && actionOne.max === 1 && actionOne.min === 1 && !actionOne.endDate) {
          Actions.update({ _id: actionId }, { $set: { endDate: new Date() } });
        }
      }
    } else if (type === 'actions' && value === 'disabled') {
      if (parentType !== 'citoyens') {
        countActionScope(parentType, parentId, 'done');

        if (scopeTypeOne && scopeTypeOne.jobIdAlert && scopeTypeOne.jobIdStart) {
          Jobs.remove(scopeTypeOne.jobIdAlert);
          Jobs.remove(scopeTypeOne.jobIdStart);
        }

        const notif = {};
        const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
          fields: {
            _id: 1, name: 1, email: 1, username: 1,
          },
        });
        // author
        notif.author = {
          id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
        };
        // object
        notif.object = {
          id: scopeTypeOne._id.valueOf(), name: scopeTypeOne.name, type: 'actions', links: scopeTypeOne.links, parentType: scopeTypeOne.parentType, parentId: scopeTypeOne.parentId, idParentRoom: scopeTypeOne.idParentRoom,
        };

        ActivityStream.api.add(notif, 'actionDisabled', 'isActionMembers');
        ActivityStream.api.add(notif, 'actionDisabled', 'isAdmin');
      }
    } else if (type === 'actions' && value === 'todo') {
      if (parentType !== 'citoyens') {
        countActionScope(parentType, parentId, 'todo');

        const actionOne = Actions.findOne({
          _id: new Mongo.ObjectID(id),
        });

        if (actionOne && actionOne.endDate && actionOne.noEndDate) {
          Actions.update({ _id: new Mongo.ObjectID(id) }, { $unset: { endDate: '', noEndDate: '' } });
        }

        const notif = {};
        const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
          fields: {
            _id: 1, name: 1, email: 1, username: 1,
          },
        });
        // author
        notif.author = {
          id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
        };
        // object
        notif.object = {
          id: scopeTypeOne._id.valueOf(), name: scopeTypeOne.name, type: 'actions', links: scopeTypeOne.links, parentType: scopeTypeOne.parentType, parentId: scopeTypeOne.parentId, idParentRoom: scopeTypeOne.idParentRoom,
        };


        ActivityStream.api.add(notif, 'actionReopen', 'isAdmin');
      }
    }

    return retour;
  },
});

export const validateUserActions = new ValidatedMethod({
  name: 'validateUserActions',
  validate: new SimpleSchema({
    userId: { type: String },
    actionId: { type: String },
    organizationId: { type: String },
    commentaire: { type: String },
    credits: { type: SimpleSchema.Integer, min: 0 },
  }).validator(),
  run(doc) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const action = Actions.findOne({ _id: new Mongo.ObjectID(doc.actionId) });

    if (!action) {
      throw new Meteor.Error('not-action');
    }
    const collection = nameToCollection(action.parentType);


    if (!collection.findOne({ _id: new Mongo.ObjectID(action.parentId) }).isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const userStatus = action.finishedBy.find(item => item.userId === doc.userId)?.status;
    if (userStatus === 'validated') {
      throw new Meteor.Error('user-validated');
    }

    const actionId = new Mongo.ObjectID(doc.actionId);
    const userNeed = new Mongo.ObjectID(doc.userId);

    let credit;
    if (doc.credits) {
      credit = doc.credits;
    } else {
      credit = Actions.findOne({ _id: actionId }) && Actions.findOne({ _id: actionId }).credits && !typeOfNaN(Actions.findOne({ _id: actionId }).credits) ? parseInt(Actions.findOne({ _id: actionId }).credits) : 0;
    }


    const orgIduserWallet = orgIdWallet(doc.organizationId);
    const userCredits = `userWallet.${orgIduserWallet}.userCredits`;
    if (!Citoyens.findOne({ _id: userNeed, [userCredits]: { $exists: 1 } })) {
      Citoyens.update({ _id: userNeed }, { $set: { [userCredits]: 0 } });
    }

    updateFinishedByAction(action, doc.userId, 'validated');


    // log user action credit
    const logInsert = {};
    logInsert.userId = doc.userId;
    logInsert.organizationId = orgIduserWallet;
    if (orgIduserWallet !== doc.organizationId) {
      logInsert.subOrganizationId = doc.organizationId;
    }
    logInsert.actionId = doc.actionId;
    if (doc.commentaire !== 'nocomment') {
      logInsert.commentaire = doc.commentaire;
    }
    if (credit || credit === 0) {
      logInsert.credits = credit;
      logInsert.createdAt = new Date();
      LogUserActions.insert(logInsert);
    }

    Citoyens.update({ _id: userNeed }, { $inc: { [userCredits]: credit } });

    // verifier si tout les users sont valider
    const actionOne = Actions.findOne({ _id: actionId });
    if (actionOne.finishedBy && actionOne.countContributors() === actionOne.finishedBy.length && arrayLinkToModerate(actionOne.finishedBy).length === 0) {
      // le status de l'action est passé en done
      Meteor.call('actionsType', {
        parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: doc.actionId, name: 'status', value: 'done',
      });
    }
    //

    // notification
    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: actionOne._id.valueOf(), name: actionOne.name, type: 'actions', parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
    };

    ActivityStream.api.add(notif, 'validate', 'isUser', null, doc.userId);

    return true;
  },
});