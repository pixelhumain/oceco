import { Actions } from '../../collection/actions.js';
import { Citoyens } from '../../collection/citoyens.js';
import { Mongo } from 'meteor/mongo';
import { queryLink } from '../../helpers.js';

Actions.helpers({
  listContributorsFinishedBy(statusUser) {
    const statusArray = ['toModerate', 'novalidated', 'validated'];
    if (this.finishedBy && statusArray.includes(statusUser)) {
      // Filtrage des éléments de `finishedBy` pour ne garder que ceux correspondant au `statusUser`
      const finishedByUserIds = this.finishedBy
        .filter(({ status }) => status === statusUser)
        .map(({ userId }) => new Mongo.ObjectID(userId));
      const query = { _id: { $in: finishedByUserIds } };

      return Citoyens.find(query, { fields: { _id: 1, name: 1 } });
    }
    return false;
  },
  listContributorsOpti(search) {
    if (this.links?.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, { fields: { _id: 1, name: 1 } });
    }
    return false;
  },
});