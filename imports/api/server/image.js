import sharp from 'sharp';

const dataURItoBuffer = (dataURI) => {
  const matches = dataURI.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
  if (matches.length !== 3) {
    throw new Error('Invalid input string');
  }
  return Buffer.from(matches[2], 'base64');
};

const bufferToDataURI = (buffer, mimeType) => {
  return `data:${mimeType};base64,${buffer.toString('base64')}`;
};

const extractMimeType = (dataURI) => {
  const regex = /^data:([^;]+);base64,/; // Cette regex capture le type MIME dans un Data URI.
  const match = dataURI.match(regex);
  return match ? match[1] : null; // Retourne le type MIME trouvé ou null si non trouvé.
};

export const processImage = async (dataURI, resize = 1000) => {
  try {
    const mimeType = extractMimeType(dataURI);
    const imageBuffer = dataURItoBuffer(dataURI);
    // Ajuste l'image basée sur les données EXIF pour l'orientation
    // et applique d'autres traitements si nécessaire
    let sharpInstance = sharp(imageBuffer).rotate();
    const metadata = await sharpInstance.metadata();
    if (metadata.width > resize) {
      sharpInstance = sharpInstance.resize({
        width: resize,
        fit: sharp.fit.inside,
        withoutEnlargement: true
      });
    }
    if (mimeType === 'image/jpeg') {
      sharpInstance = sharpInstance.jpeg({ quality: 80 });
    } else if (mimeType === 'image/png') {
      sharpInstance = sharpInstance.png({ quality: 80 });
    }
    const processedBuffer = await sharpInstance.toBuffer();
    const photoDataURI = bufferToDataURI(processedBuffer, mimeType);
    return photoDataURI;
  } catch (err) {
    console.error('Erreur lors du traitement de l\'image:', err);
    throw err;
  }
};