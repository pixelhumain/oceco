import { Meteor } from 'meteor/meteor';
import { Organizations } from '../../collection/organizations';
import { Projects } from '../../collection/projects';
import { Events } from '../../collection/events';
import { Actions } from '../../collection/actions';
import { moment } from 'meteor/momentjs:moment';
import { applyDiacritics } from '../../helpers';

Organizations.helpers({
  actionsAllOpti() {
    // recuperartion des projets liés à l'organisation
    const queryProjectId = `parent.${this._id.valueOf()}`;
    const poleProjectsId = Projects.find({ [queryProjectId]: { $exists: 1 } }, { fields: { _id: 1 } }).map(project => project._id.valueOf());

    let eventsArrayId = [];
    // Construction de la condition pour récupérer les événements liés aux projets
    if (poleProjectsId.length > 0) {
      // Calcul de la date 3 mois avant aujourd'hui
      const threeMonthsAgo = new Date();
      threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);
      const queryEventsArray = {
        $or: poleProjectsId.map(id => ({ [`organizer.${id}`]: { $exists: true } })),
        endDate: { $gte: threeMonthsAgo } // Ajout de la condition sur endDate
      };
      eventsArrayId = Events.find(queryEventsArray, { fields: { _id: 1 } })
        .map(event => event._id.valueOf());
    }

    // Construction de la requête finale pour Actions.find()
    const inputDate = new Date();
    const query = {
      $or: [
        { answerId: { $exists: false }, endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo' },
        { answerId: { $exists: false }, endDate: { $exists: false }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo' }
      ]
    };

    const options = { sort: { startDate: 1 }, fields: { description: 0, 'tasks.task': 0, 'tasks.createdAt': 0 } };

    return Actions.find(query, options);
  },
  actionsUserAllOpti(userId, etat, search) {
    const bothUserId = userId ?? Meteor.userId();

    const queryProjectId = `parent.${this._id.valueOf()}`;
    const poleProjects = Projects.find({ [queryProjectId]: { $exists: 1 } }).fetch();
    const poleProjectsId = [];
    poleProjects.forEach((element) => {
      poleProjectsId.push(element._id.valueOf());
    });

    const queryEventsArray = {};
    queryEventsArray.$or = [];
    poleProjects.forEach((element) => {
      const queryCo = {};
      queryCo[`organizer.${element._id.valueOf()}`] = { $exists: true };
      queryEventsArray.$or.push(queryCo);
    });

    const eventsArrayId = [];
    if (queryEventsArray && queryEventsArray.$or && queryEventsArray.$or.length > 0) {
      Events.find(queryEventsArray).forEach(function (event) { eventsArrayId.push(event._id.valueOf()); });
    } else {
      delete queryEventsArray.$or;
    }
    // faire un ou si date pas remplie
    const query = {};
    const inputDate = new Date();
    // query.endDate = { $gte: inputDate };
    const linkUserID = `links.contributors.${bothUserId}`;

    const fields = {};
    if (search) {
      // regex qui marche coté serveur parcontre seulement sur un mot
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'regex');
      const pattern = new RegExp(`.*${searchApplyDiacritics.replace(/\\/g, '\\\\')}.*`, 'i');
      fields.name = { $regex: pattern };
    }

    const finishedObj = {};
    if (etat === 'aFaire') {
      finishedObj.finishedBy = {
        $not: {
          $elemMatch: {
            userId: bothUserId
          }
        }
      };
    } else if (etat === 'enAttente') {
      finishedObj.finishedBy = {
        $elemMatch: {
          userId: bothUserId,
          status: 'toModerate'
        }
      }
    }

    query.$or = [];
    query.$or.push({
      [linkUserID]: { $exists: true }, endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo', ...fields, ...finishedObj,
    });
    query.$or.push({
      [linkUserID]: { $exists: true }, endDate: { $exists: false }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo', ...fields, ...finishedObj,
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    return Actions.find(query);
  },
  maintenanceChiffreStartEnd(monthStart, monthEnd, contributor = true) {

    const inputUnixStart = moment().subtract(monthStart, 'months').unix();
    const inputUnixEnd = monthEnd ? moment().subtract(monthEnd, 'months').unix() : new Date().getTime();

    // recuperartion des projets liés à l'organisation
    const queryProjectId = `parent.${this._id.valueOf()}`;
    const poleProjectsId = Projects.find({ [queryProjectId]: { $exists: 1 } }, { fields: { _id: 1 } }).map(project => project._id.valueOf());

    let eventsArrayId = [];
    // Construction de la condition pour récupérer les événements liés aux projets
    if (poleProjectsId.length > 0) {
      // Calcul de la date 3 mois avant aujourd'hui
      const threeMonthsAgo = new Date();
      threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);
      const queryEventsArray = {
        $or: poleProjectsId.map(id => ({ [`organizer.${id}`]: { $exists: true } })),
        endDate: { $gte: threeMonthsAgo } // Ajout de la condition sur endDate
      };
      eventsArrayId = Events.find(queryEventsArray, { fields: { _id: 1 } })
        .map(event => event._id.valueOf());
    }


    const query = { answerId: { $exists: false }, created: { $exists: true, $lt: inputUnixEnd, $gt: inputUnixStart }, startDate: { $exists: false }, endDate: { $exists: false }, parentId: { $in: [...eventsArrayId, ...poleProjectsId, this._id.valueOf()] }, status: 'todo' };

    // sans contributeur
    if (!contributor) {
      query['links.contributors'] = { $exists: false };
    }
    // console.log('count action plus de 18 mois', Actions.find(query).count());
    return Actions.find(query, {
      sort: { created: -1 },
    });

  }
});