import { Meteor } from 'meteor/meteor';
// import { URL } from 'meteor/url';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Jobs } from 'meteor/wildhart:jobs';
import { Mongo } from 'meteor/mongo';
import { SchemasOrganizationsOcecoRest } from '../../schema/organizations';
import { Organizations } from '../../collection/organizations';

export const updateOcecoOpti = new ValidatedMethod({
  name: 'updateOcecoOpti',
  validate: new SimpleSchema({
    modifier: {
      type: Object,
      blackbox: true,
    },
    _id: {
      type: String,
    },
  }).validator(),
  run({ modifier, _id }) {
    SchemasOrganizationsOcecoRest.clean(modifier);
    SchemasOrganizationsOcecoRest.validate(modifier, { modifier: true });

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // console.log(_id);
    // console.log(modifier);

    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(_id) });

    if (!orgaOne) {
      throw new Meteor.Error('not-authorized');
    }

    if (!orgaOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const retour = Organizations.update({ _id: new Mongo.ObjectID(_id) }, modifier);

    if (modifier.$set['oceco.autoCloserActionBot'] === true) {
      const jobsDoc = Jobs.run('autoCloserActionBot', _id, { singular: true, on: { hours: 1 } });
      if (jobsDoc && jobsDoc._id) {
        Organizations.update({ _id: new Mongo.ObjectID(_id) }, { $set: { 'oceco.autoCloserActionBotJob': jobsDoc._id } });
      }
    } else {
      if (orgaOne.oceco && orgaOne.oceco.autoCloserActionBotJob) {
        Jobs.remove(orgaOne.oceco.autoCloserActionBotJob);
        Organizations.update({ _id: new Mongo.ObjectID(_id) }, { $unset: { 'oceco.autoCloserActionBotJob': "" } });
      }
    }

    if (retour) {
      return _id;
    }
    throw new Meteor.Error('error');
  },
});
