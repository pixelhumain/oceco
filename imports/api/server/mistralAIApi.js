import { Meteor } from 'meteor/meteor';
import MistralClient from '@mistralai/mistralai';

export class MistralAiApi {
  constructor(apiKey) {
    this.apiKey = apiKey;
    this.defaultModel = 'mistral-large-latest';
    this.defaultOptions = {
      temperature: 0,
      maxTokens: 2000,
    };
  }

  async request(prompt, roleSystem, model, options) {

    try {
      const client = new MistralClient(this.apiKey);

      const response = await client.chat({
        model: model,
        messages: [
          {
            "role": "system",
            "content": roleSystem
          },
          {
            "role": "user",
            "content": prompt
          }
        ],
        ...options
      });


      if (!response || !response.choices || !response.choices[0] || !response.choices[0].message || !response.choices[0].message.content) {
        throw new Error('Impossible de récupérer une réponse valide de l’API');
      }

      const responseText = response.choices[0].message.content.trim();
      // console.log('Réponse de l’API OpenAI :', responseText);


      return responseText;
    } catch (error) {
      console.error('Erreur request :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'erreur de l’API MistralAi.');
    }
  }

  async completeSemanticAnalysis(text, model = this.defaultModel) {

    const prompt = `Analysez le texte suivant : "${text}".\n\n` +
      ` Il est crucial de respecter le format suivant pour l'analyse sémantique complète (et de se limiter strictement à 5 mots-clés principaux) et de répondre en français:\n` +
      `- Mots-clés: mot1, mot1, mot3, mot4, mot5\n` +
      `- Sentiment: [Négatif, Neutre, Positif]\n` +
      `- Domaine: [Identifiez un seul domaine le plus important]\n` +
      `- Résumé: [Résumez le texte en 1 ou 2 phrases]`;

    const roleSystem = "Tu es un assistant d'analyse sémantique";

    try {
      const responseText = await this.request(prompt, roleSystem, model, this.defaultOptions);

      // console.log('Réponse de l’API :', responseText);
      // Exemple de traitement de la réponse structurée
      const analysisResult = {
        keywords: [],
        sentiment: '',
        domaine: ''
      };

      const keywordsMatch = responseText.match(/Mots-clés: (.*?)(\n|$)/);
      if (keywordsMatch && keywordsMatch[1]) {
        analysisResult.keywords = keywordsMatch[1].split(', ').map(keyword => keyword.trim());
      }

      const sentimentMatch = responseText.match(/Sentiment: (.*?)(\n|$)/);
      if (sentimentMatch && sentimentMatch[1]) {
        analysisResult.sentiment = sentimentMatch[1].trim();
      }

      const domaineMatch = responseText.match(/Domaine: (.*?)(\n|$)/);
      if (domaineMatch && domaineMatch[1]) {
        analysisResult.domaine = domaineMatch[1].trim();
      }

      const resumeMatch = responseText.match(/Résumé: (.*?)(\n|$)/);
      if (resumeMatch && resumeMatch[1]) {
        analysisResult.resume = resumeMatch[1].trim();
      }

      // console.log('Résultat de l’analyse sémantique complète :', analysisResult);

      return analysisResult;
    } catch (error) {
      console.error('Erreur lors de l’analyse sémantique complète :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de réaliser une analyse sémantique complète.');
    }
  }


  async reportOpinion(text, model = this.defaultModel) {

    const prompt = `Vous êtes commentateur. Votre tâche consiste à rédiger un rapport sur un observation.\n
Lorsqu'on vous présente l'observation, proposez des questions intéressantes à poser et répondez à chaque question.\n
Ensuite, combinez toutes les informations et rédigez un rapport au format markdown.\n\n

# observation:\n
${text}\n\n

# Instructions:\n
## Résumer:\n
Dans un langage clair et concis, résumez les points et thèmes clés présentés dans l’observation.\n

## Questions intéressantes :\n
Générez trois questions distinctes et stimulantes qui peuvent être posées sur le contenu de l'observation. Pour chaque question :\n
- Après « Q : », décrivez le problème\n
- Après « R : », fournissez une explication détaillée du problème abordé dans la question.\n\n

## Ecrire un rapport\n
À l'aide du résumé de l'observation et des réponses aux questions intéressantes, créez un rapport complet au format Markdown.`;

    const roleSystem = "Tu es un assistant d'analyse d'observation";

    try {
      const responseText = await this.request(prompt, roleSystem, model, this.defaultOptions);
      return responseText;
    } catch (error) {
      console.error('Erreur lors de la génération de titre :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de générer un titre.');
    }
  }
  async correctText(text, model = this.defaultModel) {

    const prompt = `Corrigez le texte suivant pour corriger toute erreur grammaticale ou orthographique, sans ajouter de commentaires ou de contenu supplémentaire :\n\n

    [${text}]`;

    const roleSystem = "Tu es un assistant de correction de texte";

    try {
      const responseText = await this.request(prompt, roleSystem, model, this.defaultOptions);
      return responseText;
    } catch (error) {
      console.error('Erreur lors de la correction de texte :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de corriger le texte.');
    }
  }

  async reformulateText(text, model = this.defaultModel) {

    const prompt = `Reformulez le texte suivant pour qu'il soit plus clair et plus concis, sans ajouter de commentaires ou de contenu supplémentaire :\n\n
  
      [${text}]`;
    const roleSystem = "Tu es un assistant de reformulation de texte";

    try {
      const responseText = await this.request(prompt, roleSystem, model, this.defaultOptions);
      return responseText;
    } catch (error) {
      console.error('Erreur lors de la reformulation de texte :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de reformuler le texte.');
    }
  }

  async generateMermaidDiagram(text, model = this.defaultModel) {

    const prompt = `Générez du code Mermaid qui représente le mieux la description suivante. Choisissez le type de diagramme le plus adapté (flux, séquence, classe, état, entité-relation, etc.) en fonction du contexte, respecter la syntaxe mermaid précisement attention ne pas mettre de paranthése, la réponse doit être dans un code block mardown mermaid et en français  :\n\n
      [${text}]
      `;
    const roleSystem = "Tu es un assistant de génération de diagramme Mermaid";

    try {
      const responseText = await this.request(prompt, roleSystem, model, { ...this.defaultOptions, maxTokens: 4000 });
      return responseText;
    } catch (error) {
      console.error('Erreur lors de la génération de diagramme Mermaid :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de générer un diagramme Mermaid.');
    }
  }
  // Transformer automatiquement le texte sélectionné en éléments de syntaxe Markdown appropriés, comme des titres, listes, liens, images, et blocs de code, facilitant la mise en forme sans nécessiter une connaissance approfondie de Markdown
  async markdownText(text, model = this.defaultModel) {

    const prompt = `Transformez le texte suivant en éléments de syntaxe Markdown appropriés, comme des titres, listes, liens, images, et blocs de code, facilitant la mise en forme sans nécessiter une connaissance approfondie de Markdown :\n\n
      [${text}]`;

    const roleSystem = "Tu es un assistant de génération de Markdown";

    try {
      const responseText = await this.request(prompt, roleSystem, model, { ...this.defaultOptions, maxTokens: 4000 });
      return responseText;
    } catch (error) {
      console.error('Erreur lors de la génération de Markdown :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de générer du Markdown.');
    }
  }
  // repondre à la demande en fonction du texte, la reponse doit être une action lier au texte
  async respondToText(text, model = this.defaultModel) {

    const prompt = `Répondez à la demande suivante en formatant la réponse en mardown et en français en ne rajoutant aucun commentaire autre que la réponse :\n\n
      [${text}]`;

    const roleSystem = "Tu es un assistant de génération de Markdown";

    try {
      const responseText = await this.request(prompt, roleSystem, model, { ...this.defaultOptions, maxTokens: 4000 });
      return responseText;
    } catch (error) {
      console.error('Erreur lors de la réponse à la demande :', error);
      if (error.code === 'context_length_exceeded') {
        throw new Meteor.Error('api-error', 'Le texte est trop long pour être analysé.');
      }
      throw new Meteor.Error('api-error', 'Impossible de répondre à la demande.');
    }
  }
}
