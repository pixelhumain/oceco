/* eslint-disable no-dupe-keys */
/* eslint-disable consistent-return */
/* eslint-disable no-underscore-dangle */
/* global */
import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { _ } from 'meteor/underscore';
import { HTTP } from 'meteor/jkuester:http';
import { Random } from 'meteor/random';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
// collection
import { ActivityStream, ActivityStreamReference } from '../collection/activitystream.js';
import { Citoyens } from '../collection/citoyens.js';
import { Cities } from '../collection/cities.js';
import { Events } from '../collection/events.js';
import { Organizations } from '../collection/organizations.js';
import { Projects } from '../collection/projects.js';
import { Comments } from '../collection/comments.js';
import { Lists } from '../collection/lists.js';
// DDA
import { Actions } from '../collection/actions.js';
import { CountUserActions, CountActions } from '../collection/countUserActions.js';
import { LogUserActions } from '../collection/loguseractions.js';
import { Rooms } from '../collection/rooms.js';

import { Forms } from '../collection/forms.js';
import { Answers } from '../collection/answers.js';
import { LogOrgaMonthCredit } from '../collection/logorgamonthcredit.js';

import {
  nameToCollection, arrayLinkProperNoObject, arrayLinkParentNoObject, arrayChildrenParent, queryOrPrivateScope, isValidObjectId, optionsNoFieldsScope, statusNoVisible, organizationsFieldsNoSimple
} from '../helpers.js';

import { orgaIdScope } from '../helpersOrga.js';
import { Notes } from '../collection/notes.js';

// global.Events = Events;
// global.Organizations = Organizations;
// global.Projects = Projects;
// global.Citoyens = Citoyens;
// global.Actions = Actions;
// global.Rooms = Rooms;
// global.Forms = Forms;
// global.Answers = Answers;

Events.createIndex({
  geoPosition: '2dsphere',
});
Projects.createIndex({
  geoPosition: '2dsphere',
});
Organizations.createIndex({
  geoPosition: '2dsphere',
});
Citoyens.createIndex({
  geoPosition: '2dsphere',
});
Cities.createIndex({
  geoShape: '2dsphere',
});
/* collection.rawCollection().createIndex(
{ geoPosition: "2dsphere"},
{ background: true }
, (e) => {
if(e){
console.log(e)
}
}); */

// eslint-disable-next-line meteor/audit-argument-checks

Meteor.publish('lists', function (name) {
  if (!this.userId) {
    return null;
  }
  check(name, String);
  const lists = Lists.find({ name });
  return lists;
});

Meteor.publishComposite('notificationsUser', function (limit) {
  if (!this.userId) {
    return null;
  }
  check(limit, Match.Maybe(Number));
  return {
    find() {
      const query = { type: 'oceco' };
      query.userId = this.userId;
      query.isUnread = true;
      const optionsRef = {};
      optionsRef.sort = { updated: -1 };
      if (limit) {
        optionsRef.limit = limit;
      }
      return ActivityStreamReference.find(query, optionsRef);
    },
    children: [
      {
        find(item) {
          const options = {};
          options.fields = { _id: 1, type: 1, verb: 1, targetProject: 1, targetEvent: 1, targetRoom: 1, author: 1, target: 1, object: 1, created: 1, updated: 1, 'notify.objectType': 1, [`notify.id.${this.userId}`]: 1, 'notify.displayName': 1, 'notify.icon': 1, 'notify.url': 1, 'notify.labelAuthorObject': 1, 'notify.labelArray': 1 };
          return ActivityStream.find({ _id: new Mongo.ObjectID(item.notificationId), [`notify.id.${this.userId}.isUnread`]: true }, options);
        },
      },
    ],
  };
});

Meteor.publish('notificationsCountUser', function () {
  if (!this.userId) {
    return null;
  }
  const queryUnseen = { type: 'oceco' };
  queryUnseen.userId = this.userId;
  queryUnseen.isUnseen = true;

  const queryUnread = { type: 'oceco' };
  queryUnread.userId = this.userId;
  queryUnread.isUnread = true;

  const counterUnseen = new Counter(`notifications.${this.userId}.Unseen`, ActivityStreamReference.find(queryUnseen, { fields: { _id: 1 } }));
  const counterUnread = new Counter(`notifications.${this.userId}.Unread`, ActivityStreamReference.find(queryUnread, { fields: { _id: 1 } }));

  return [
    counterUnseen,
    counterUnread,
  ];
});

Meteor.publish('notificationsScopeCount', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  if (!collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isAdmin(this.userId)) {
    return null;
  }

  const scopeCap = scope.charAt(0).toUpperCase() + scope.slice(1, -1);

  const queryUnseen = { type: 'oceco' };
  queryUnseen.userId = this.userId;
  queryUnseen.isUnseen = true;
  if (scope === 'organizations') {
    queryUnseen.targetId = scopeId;
  } else {
    queryUnseen[`target${scopeCap}`] = scopeId;
  }

  const queryUnread = { type: 'oceco' };
  queryUnread.userId = this.userId;
  queryUnread.isUnread = true;
  if (scope === 'organizations') {
    queryUnread.targetId = scopeId;
  } else {
    queryUnread[`target${scopeCap}`] = scopeId;
  }

  const queryUnseenAsk = { ...queryUnseen };
  queryUnseenAsk.verb = { $in: ['ask'] };

  const counterUnseen = new Counter(`notifications.${scopeId}.Unseen`, ActivityStreamReference.find(queryUnseen, { fields: { _id: 1 } }));
  const counterUnread = new Counter(`notifications.${scopeId}.Unread`, ActivityStreamReference.find(queryUnread, { fields: { _id: 1 } }));
  const counterUnseenAsk = new Counter(`notifications.${scopeId}.UnseenAsk`, ActivityStreamReference.find(queryUnseenAsk, { fields: { _id: 1 } }));

  return [
    counterUnseen,
    counterUnread,
    counterUnseenAsk,
  ];
});

Meteor.publishComposite('notificationsScope', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  if (!collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).isAdmin(this.userId)) {
    return null;
  }

  return {
    find() {
      const query = { type: 'oceco' };
      query.userId = this.userId;
      query.isUnseen = true;
      const scopeCap = scope.charAt(0).toUpperCase() + scope.slice(1, -1);
      // Todo : pour organization ça prend pas en compte
      if (scope === 'organizations') {
        query.targetId = scopeId;
      } else {
        query[`target${scopeCap}`] = scopeId;
      }
      const optionsRef = {};
      optionsRef.sort = { updated: -1 };
      const limit = 100;
      if (limit) {
        optionsRef.limit = limit;
      }
      return ActivityStreamReference.find(query, optionsRef);
    },
    children: [
      {
        find(item) {
          return ActivityStream.find({ _id: new Mongo.ObjectID(item.notificationId) });
        },
      },
    ],
  };
});

Meteor.publish('cities', function (cp, country) {
  if (!this.userId) {
    return null;
  }
  check(cp, String);
  check(country, String);
  const lists = Cities.find({ 'postalCodes.postalCode': cp, country });
  return lists;
});

Meteor.publish('orga.switch', function () {
  if (!this.userId) {
    return null;
  }
  const options = {};
  options.fields = { _id: 1, name: 1, profilThumbImageUrl: 1, shortDescription: 1, 'oceco.pole': 1, collection: 1 };
  const lists = Organizations.find({ parent: { $exists: false }, oceco: { $exists: true } }, options);
  return lists;
});

Meteor.publish('orga.switchFavoris', function (favoris) {
  if (!this.userId) {
    return null;
  }
  let self = this;

  const citoyen = Citoyens.findOne(
    { _id: new Mongo.ObjectID(this.userId) },
    { fields: { 'oceco.favoris.organizations': 1, 'oceco.favoris.projects': 1, 'oceco.favoris.notes': 1 } },
  );

  let favorisOrgaIds = [];
  let favorisProjectIds = [];
  let favorisProjectsMap = {};
  let favorisNotesIds = [];
  let favorisNotesMap = {};

  // Récupérer les favoris passés dans l'argument
  if (favoris) {
    favorisOrgaIds = favoris.organizations || [];
    favorisProjectsMap = (favoris.projects || []).reduce((map, fav) => {
      map[fav.id] = fav.orgaId;
      return map;
    }, {});
    favorisProjectIds = Object.keys(favorisProjectsMap).map(id => id); // Extraire uniquement les IDs des projets

    favorisNotesMap = (favoris.notes || []).reduce((map, fav) => {
      map[fav.id] = fav.organizationId || null;
      return map;
    }, {});
    favorisNotesIds = (favoris.notes || []).map(note => new Mongo.ObjectID(note.id)); // Extraire uniquement les IDs des projets
  }

  // Construire la requête pour les organizations
  const favorisOrgaArrayIds = favorisOrgaIds.map(id => new Mongo.ObjectID(id));
  const findOrgaRequest = {
    oceco: { $exists: true },
    parent: { $exists: false },
    _id: { $in: favorisOrgaArrayIds },
  };

  // Construire la requête pour les projects
  const favorisProjectArrayIds = favorisProjectIds.map(id => new Mongo.ObjectID(id));
  const findProjectRequest = {
    _id: { $in: favorisProjectArrayIds },
  };

  // Options communes
  const options = {
    fields: {
      _id: 1,
      name: 1,
      profilThumbImageUrl: 1,
      shortDescription: 1,
      collection: 1,
      'oceco.pole': 1,
    },
  };

  // Options notes
  const optionsNotes = {
    fields: {
      _id: 1,
      name: 1,
      parentId: 1,
      parentType: 1
    },
  };

  // Construire la requête pour les projects
  const findNotesRequest = {
    _id: { $in: favorisNotesIds },
  };

  // Observer les changements dans les organizations
  const orgaHandle = Organizations.find(findOrgaRequest, options).observe({
    added: function (doc) {
      self.added('switchfavoris', doc._id, doc);
    },
    changed: function (newDoc, oldDoc) {
      self.changed('switchfavoris', newDoc._id, newDoc);
    },
    removed: function (doc) {
      self.removed('switchfavoris', doc._id);
    },
  });

  // Observer les changements dans les projects
  const projectHandle = Projects.find(findProjectRequest, options).observe({
    added: function (doc) {
      const orgaId = favorisProjectsMap[doc._id.valueOf()] || null; // Associer orgaId au document
      self.added('switchfavoris', doc._id, { ...doc, orgaId });
    },
    changed: function (newDoc, oldDoc) {
      const orgaId = favorisProjectsMap[newDoc._id.valueOf()] || null; // Associer orgaId au document modifié
      self.changed('switchfavoris', newDoc._id, { ...newDoc, orgaId });
    },
    removed: function (doc) {
      self.removed('switchfavoris', doc._id);
    },
  });


  // Observer les changements dans les notes
  const notesHandle = Notes.find(findNotesRequest, optionsNotes).observe({
    added: function (doc) {
      let organizationId = favorisNotesMap[doc._id.valueOf()] || null;
      if (doc.parentType === 'organizations') {
        organizationId = doc.parentId;
      }
      const collection = 'notes';
      self.added('switchfavoris', doc._id, { ...doc, organizationId, collection });
    },
    changed: function (newDoc, oldDoc) {
      const organizationId = favorisNotesMap[newDoc._id.valueOf()] || null; // Associer orgaId au document modifié
      const collection = 'notes';
      self.changed('switchfavoris', newDoc._id, { ...newDoc, organizationId, collection });
    },
    removed: function (doc) {
      self.removed('switchfavoris', doc._id);
    },
  });

  // Marquer la publication comme prête
  self.ready();

  // Arrêter les handles quand le client se déconnecte
  self.onStop(function () {
    orgaHandle.stop();
    projectHandle.stop();
    notesHandle.stop();
  });
});



Meteor.publish('citoyen', function () {
  if (!this.userId) {
    return null;
  }
  const objectId = new Mongo.ObjectID(this.userId);
  const options = {};
  const { fields } = optionsNoFieldsScope('citoyens');
  options.fields = fields;
  const citoyen = Citoyens.find({ _id: objectId }, options);
  return citoyen;
});

Meteor.publish('citoyenLight', function (userId) {
  check(userId, String);
  if (!this.userId) {
    return null;
  }
  const objectId = new Mongo.ObjectID(userId);
  const citoyen = Citoyens.find({ _id: objectId }, { fields: { _id: 1, name: 1, username: 1 } });
  return citoyen;
});

Meteor.publishComposite('scopeDetail', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['events', 'projects', 'organizations', 'citoyens', 'actions', 'rooms', 'forms'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  // organizations
  // projects > organizations
  // events > projects > organizations
  // actions > rooms > events > projects > organizations

  const orgId = orgaIdScope({ scope, scopeId });

  return {
    find() {
      // options['_disableOplog'] = true;
      const options = {};
      let query = {};

      if (['events', 'projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId), 'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId), 'preferences.private': { $exists: false },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      query.status = { $nin: statusNoVisible };

      // console.log(query);

      if (scope === 'citoyens') {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      } else if (scope === 'organizations') {
        const orgOne = collection.findOne(query);
        if (orgOne) {
          const { fields } = optionsNoFieldsScope(scope, orgOne.isAdmin());
          options.fields = fields;
        }
      } else if (scope === 'projects') {
        const projectOne = collection.findOne(query, options);
        if (projectOne) {
          const { fields } = optionsNoFieldsScope(scope, projectOne.isAdmin());
          options.fields = fields;
        }
      }

      return collection.find(query, options);
    },
    children: [
      {
        find() {
          if (orgId && orgId.length > 0) {
            // console.log(orgId);
            const OrgaArrayIds = orgId.map((id) => new Mongo.ObjectID(id));
            // console.log(OrgaArrayIds);
            return Organizations.find({ _id: { $in: [OrgaArrayIds['0']] } }, { fields: { _id: 1 } });
          }
        },
      },
      {
        find(scopeD) {
          if (scopeD && scopeD.parent) {
            const arrayIds = Object.keys(scopeD.parent)
              .filter((k) => scopeD.parent[k].type === 'organizations')
              .map((k) => new Mongo.ObjectID(k));
            if (scope === 'organizations' && scopeD.parent && arrayIds && arrayIds.length > 0) {
              return Organizations.find({ _id: { $in: [arrayIds['0']] } }, { fields: { _id: 1, name: 1 } });
            }
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'events') {
            return scopeD.listEventTypes();
          } if (scope === 'organizations') {
            return scopeD.listOrganisationTypes();
          }
        },
      },
      {
        find(scopeD) {
          return Citoyens.find({
            _id: new Mongo.ObjectID(scopeD.creator),
          }, {
            fields: {
              name: 1,
              profilThumbImageUrl: 1,
            },
          });
        },
      },
      {
        find() {
          if (scope !== 'citoyens') {
            return Citoyens.find({
              _id: new Mongo.ObjectID(this.userId),
            }, {
              fields: {
                name: 1,
                links: 1,
                collections: 1,
                profilThumbImageUrl: 1,
              },
            });
          }
        },
      },
      // ...arrayChildrenParent(scope, ['events', 'projects', 'organizations', 'citoyens']),
      {
        find(scopeD) {
          if (scopeD && scopeD.address && scopeD.address.postalCode) {
            return Cities.find({
              'postalCodes.postalCode': scopeD.address.postalCode,
            });
          }
        },
      },
    ],
  };
});


Meteor.publishComposite('directoryListEvents', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;

      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      let query = {};
      if (['events', 'projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }
      return collection.find(query, options);
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['eventTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
            return scopeD.listEventsCreator();
          }
        },
        children: arrayChildrenParent(scope, ['citoyens', 'organizations', 'projects', 'events']),
        /* [
          {
            find(scopeD) {
              if (scopeD.organizerType && scopeD.organizerId && _.contains(['citoyens', 'organizations', 'projects', 'events'], scopeD.organizerType)) {
                const collectionType = nameToCollection(scopeD.organizerType);
                return collectionType.find({
                  _id: new Mongo.ObjectID(scopeD.organizerId),
                }, {
                  fields: {
                    name: 1,
                    links: 1,
                    profilThumbImageUrl: 1,
                  },
                });
              }
            },
          },
        ], */
      },
    ],
  };
});

Meteor.publishComposite('directoryListForms', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations', 'projects'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      let query = {};
      query.$or = [];
      query.$or.push({
        _id: new Mongo.ObjectID(scopeId),
        'preferences.private': false,
      });
      query.$or.push({
        _id: new Mongo.ObjectID(scopeId),
        'preferences.private': {
          $exists: false,
        },
      });

      if (scope === 'projects') {
        query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
      } else if (scope === 'organizations') {
        query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
      }

      if (['projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find(query, options);
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['eventTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'projects' || scope === 'organizations') {
            return scopeD.listForms();
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryProjectsListEvents', function (scope, scopeId, startDateCal, endDateCal) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations', 'projects'].includes(name);
  }));
  check(startDateCal, Match.Maybe(Date));
  check(endDateCal, Match.Maybe(Date));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;

      let query = {};
      if (['organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }


      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        return scopeD.listProjectsEventsCreator(null, startDateCal, endDateCal);
      },
      children: arrayChildrenParent('events', ['citoyens', 'organizations', 'projects', 'events']),
      /* [
          {
            find(scopeD) {
              if (scopeD.organizerType && scopeD.organizerId && _.contains(['citoyens', 'organizations', 'projects', 'events'], scopeD.organizerType)) {
                const collectionType = nameToCollection(scopeD.organizerType);
                return collectionType.find({
                  _id: new Mongo.ObjectID(scopeD.organizerId),
                }, {
                  fields: {
                    name: 1,
                    links: 1,
                    profilThumbImageUrl: 1,
                  },
                });
              }
            },
          },
        ], */
    },
    ],
  };
});

Meteor.publishComposite('directoryProjectsListEventsAdmin', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;

      }

      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        if (scope === 'organizations') {
          return scopeD.listProjectsEventsCreatorAdmin1M();
        }
      },
      children: arrayChildrenParent('events', ['citoyens', 'organizations', 'projects', 'events']),
    },
    ],
  };
});

Meteor.publish('directoryActionsAllCounter', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
  }));
  if (!this.userId) {
    return null;
  }
  const collection = nameToCollection(scope);
  return new Counter(`countActionsAll.${scopeId}`, collection.findOne({ _id: new Mongo.ObjectID(scopeId) }).listProjectsEventsActionsCreatorAdmin('all'));
});

Meteor.publishComposite('directoryActionsAll', function (scope, scopeId, etat, limit) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  check(etat, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['events', 'projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
            return scopeD.listProjectsEventsActionsCreatorAdmin('all', limit);
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
            return scopeD.listProjectsEventsCreatorAdmin1M();
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryProjectsListEventsActions', function (scope, scopeId, etat) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  check(etat, Match.Maybe(String));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['events', 'projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      // console.log(query);
      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
          if (scopeD.links && scopeD.links.projects) {
            const projectIds = arrayLinkParentNoObject(scopeD.links.projects, 'projects');
            const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
            const query = {};
            query.$or = [];

            projectIds.forEach((id) => {
              const queryCo = {};
              if (userC && userC.links && userC.links.projects && userC.links.projects[id] && userC.links.projects[id].isAdmin && !userC.links.projects[id].toBeValidated && !userC.links.projects[id].isAdminPending && !userC.links.projects[id].isInviting) {
                queryCo[`organizer.${id}`] = { $exists: true };
                query.$or.push(queryCo);
              }
            });
            if (query.$or.length === 0) {
              delete query.$or;
            }

            // queryOptions.fields.parentId = 1;
            // const inputDate = new Date();
            const inputDate = moment(new Date()).subtract(15, 'day').toDate();
            // query.startDate = { $lte: inputDate };
            query.endDate = { $gte: inputDate };

            const options = {};
            options.sort = {
              startDate: -1,
            };
            // console.log(query);
            return Events.find(query, options);
          }
          // return scopeD.listProjectsEventsCreator1M();
        }
      },
      children:
        [
          {
            find(scopeD) {
              const query = {};
              const inputDate = new Date();

              const queryone = {};
              queryone.endDate = { $exists: true, $lte: inputDate };
              if (etat) {
                queryone.status = etat;
              }
              queryone['links.contributors'] = { $exists: true };
              queryone.parentId = { $in: [scopeD._id.valueOf()] };

              const querytwo = {};
              if (etat) {
                querytwo.status = etat;
              }
              querytwo['links.contributors'] = { $exists: true };
              querytwo.endDate = { $exists: false };
              querytwo.parentId = { $in: [scopeD._id.valueOf()] };

              query.$or = [];
              query.$or.push(queryone);
              query.$or.push(querytwo);
              // * action d'events de projets de l'orga (sur des events des 15 dernier jours)
              return Actions.find(query);
            },
            children: [{
              find(scopeD) {
                return scopeD.listContributors();
              },
            }],
          },
        ],
    },
    {
      find(scopeD) {
        if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
          if (scopeD.isAdmin()) {
            return scopeD.listProjects();
          }
          return scopeD.listProjectsCreatorAdmin();
        }
      },
      children:
        [
          {
            find(scopeD) {
              const query = {};
              query.parentId = scopeD._id.valueOf();
              if (etat) {
                query.status = etat;
              }
              // * action de projets de l'orga
              query.finishedBy = { $exists: true };
              query.answerId = { $exists: false };
              query['links.contributors'] = { $exists: true };
              return Actions.find(query);
            },
            children: [{
              find(scopeD) {
                return scopeD.listContributors();
              },
            }],
          },
        ],
    },
    {
      find(scopeD) {
        const query = {};
        query.parentId = scopeD._id.valueOf();
        if (etat) {
          query.status = etat;
        }
        query.finishedBy = { $exists: true };
        query['links.contributors'] = { $exists: true };
        // * action de l'orga
        return Actions.find(query);
      },
      children: [{
        find(scopeD) {
          return scopeD.listContributors();
        },
      }],
    },
    ],
  };
});

Meteor.publishComposite('directoryListActions', function ({ scope, scopeId, etat, search, searchSort, milestoneId, limit }) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  check(etat, Match.Maybe(String));
  check(search, Match.Maybe(String));
  check(searchSort, Match.Maybe(Object));
  check(milestoneId, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['events', 'projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        } else if (scope === 'events') {
          query = queryOrPrivateScope(query, 'attendees', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects' || scope === 'events') {
          return scopeD.listActionsCreator('all', etat, search, searchSort, milestoneId, limit);
        }
      },
      children: [{
        find(action) {
          // if (action.avatarOneUserAction()) {
          if (action && action.links && action.links.contributors) {
            const arrayContributors = arrayLinkProperNoObject(action.links.contributors);
            if (arrayContributors && arrayContributors[0]) {
              const arrayAllMergeMongoId = arrayContributors.map((k) => new Mongo.ObjectID(k));
              return Citoyens.find({ _id: { $in: arrayAllMergeMongoId } }, { fields: { name: 1, username: 1, profilThumbImageUrl: 1 } });
            }
          }
          // }
        },
      }],
    },
    ],
  };
});


Meteor.publishComposite('directoryListAnswers', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['forms'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'forms') {
            return scopeD.listAnswers();
          }
        },
        children: [{
          find(answer) {
            if (answer && answer.user) {
              return Citoyens.find({ _id: new Mongo.ObjectID(answer.user) }, { fields: { name: 1, username: 1, profilThumbImageUrl: 1 } });
            }
          },
        },
        ],
      },
    ],
  };
});

Meteor.publish('historiqueUserActionsAllCounter', function (scopeId, citoyenId) {
  check(scopeId, String);
  check(citoyenId, String);

  if (!this.userId) {
    return null;
  }

  return new Counter(`historiqueUsercountActionsAll.${scopeId}.${citoyenId}`, LogUserActions.find({ organizationId: scopeId, userId: citoyenId }));
});

Meteor.publishComposite('listMembersDetailHistorique', function (organizationId, citoyenId, limit) {
  check(organizationId, String);
  check(citoyenId, String);
  check(limit, Number);

  if (!this.userId) {
    return null;
  }

  const collectionOne = Organizations.findOne({ _id: new Mongo.ObjectID(organizationId) });
  if (!collectionOne) {
    return null;
  }

  let isAdminProject = false;
  const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });

  if (collectionOne.links && collectionOne.links.projects && userC && userC.links && userC.links.projects) {
    // eslint-disable-next-line no-unused-vars
    const arrayIds = Object.keys(collectionOne.links.projects)
      .filter((k) => userC.links.projects[k] && userC.links.projects[k].isAdmin && !userC.links.projects[k].toBeValidated && !userC.links.projects[k].isAdminPending && !userC.links.projects[k].isInviting)
      // eslint-disable-next-line array-callback-return
      .map((k) => k);
    // console.log(arrayIds);
    isAdminProject = !!(arrayIds && arrayIds.length > 0);
  }

  if (citoyenId === this.userId) {
    //
  } else if (!collectionOne.isAdmin()) {
    if (collectionOne && collectionOne.oceco && collectionOne.oceco.membersAdminProjectAdmin) {
      if (!isAdminProject) {
        return null;
      }
    } else {
      return null;
    }
  }

  return {
    find() {
      const options = {};
      options.fields = {
        name: 1,
        _id: 1,
      };

      const query = {};
      query._id = new Mongo.ObjectID(citoyenId);
      return Citoyens.find(query, options);
    },
    children: [{
      find() {
        const options = {};
        options.limit = limit || 10;
        options.sort = { createdAt: -1 };
        return LogUserActions.find({ organizationId, userId: citoyenId }, options);
      },
      children: [{
        find(log) {
          return Actions.find({ _id: new Mongo.ObjectID(log.actionId) }, { sort: { createdAt: -1 } });
        },
      },
      ],
    },
    ],
  };
});


Meteor.publishComposite('listOrgaDetailHistorique', function (organizationId, limit) {
  check(organizationId, String);
  check(limit, Number);

  if (!this.userId) {
    return null;
  }

  const collectionOne = Organizations.findOne({ _id: new Mongo.ObjectID(organizationId) });
  if (!collectionOne) {
    return null;
  }

  if (!collectionOne.isAdmin()) {
    return null;
  }

  return {
    find() {
      const options = {};
      options.limit = limit || 10;
      options.sort = { createdAt: -1 };
      return LogUserActions.find({ organizationId }, options);
    },
    children: [{
      find(log) {
        return Actions.find({ _id: new Mongo.ObjectID(log.actionId) }, { sort: { createdAt: -1 } });
      },
    },
    {
      find(log) {
        const options = {};
        options.fields = {
          name: 1,
          _id: 1,
        };

        const query = {};
        query._id = new Mongo.ObjectID(log.userId);
        return Citoyens.find(query, options);
      },
    },
    {
      find(log) {
        const options = {};
        options.fields = {
          name: 1,
          _id: 1,
        };

        const query = {};
        if (log.organizationId && log.subOrganizationId) {
          query._id = { $in: [new Mongo.ObjectID(log.organizationId), new Mongo.ObjectID(log.subOrganizationId)] };
        } else if (log.organizationId) {
          query._id = new Mongo.ObjectID(log.organizationId);
        }
        return Organizations.find(query, options);
      },
    },
    ],
  };
});

Meteor.publishComposite('directoryListProjects', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations', 'citoyens', 'projects'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }

  const orgId = orgaIdScope({ scope, scopeId });

  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
          // const projectOne = collection.findOne(query, options);
          // if (projectOne) {
          const { fields } = optionsNoFieldsScope(scope);
          options.fields = { ...fields, ...organizationsFieldsNoSimple };
          // }
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
          // const orgOne = collection.findOne(query, options);
          // if (orgOne) {
          const { fields } = optionsNoFieldsScope(scope);
          options.fields = { ...fields, ...organizationsFieldsNoSimple };
          // }
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['citoyens'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find(query, options);
    },
    children: [
      {
        find() {
          if (orgId && orgId.length > 0) {
            // console.log(orgId);
            const OrgaArrayIds = orgId.map((id) => new Mongo.ObjectID(id));
            // console.log(OrgaArrayIds);
            return Organizations.find({ _id: { $in: [OrgaArrayIds['0']] } }, { fields: { _id: 1 } });
          }
        },
      },
      {
        find(scopeD) {
          if (scopeD && scopeD.parent) {
            const arrayIds = Object.keys(scopeD.parent)
              .filter((k) => scopeD.parent[k].type === 'organizations')
              .map((k) => new Mongo.ObjectID(k));
            if (scope === 'organizations' && scopeD.parent && arrayIds && arrayIds.length > 0) {
              return Organizations.find({ _id: { $in: [arrayIds['0']] } }, { fields: { _id: 1, name: 1 } });
            }
          }
        },
      },
      {
        find() {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects') {
            // console.log('directoryListProjects');
            return scopeD.listProjectsCreator();
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryListRooms', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'events'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find() {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            const options = {};
            options.fields = {
              _id: 1,
              name: 1,
              profilThumbImageUrl: 1,
            };
            let scopeCible = scope;
            if (scope === 'organizations') {
              scopeCible = 'memberOf';
            }
            options.fields[`links.${scopeCible}`] = 1;
            return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, options);
          }
        },
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events') {
            return scopeD.listRooms();
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryListOrganizations', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['citoyens', 'organizations'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (['citoyens', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listOrganizationsCreator();
          } if (scope === 'organizations') {
            return scopeD.listOrganizationsCreator();
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('directoryListInvitations', function ({ scope, scopeId, search, limit }) {
  check(scopeId, String);
  check(scope, String);
  check(search, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  /* check(scope, Match.Where(function(name) {
    return ['citoyens', 'organizations', 'projects'].includes(name);
  })); */
  const collection = nameToCollection(scope);

  if (!this.userId) {
    return null;
  }
  return [{
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const { fields } = optionsNoFieldsScope('citoyens');
      options.fields = fields;
      return Citoyens.find({ _id: new Mongo.ObjectID(this.userId) }, options);
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(citoyen) {
          if (scope === 'organizations' && !collection.findOne({ _id: new Mongo.ObjectID(scopeId), parent: { $exists: true } }, { fields: { _id: 1, parent: 1 } })) {
            return citoyen.listFollows();
          } else if (scope === 'projects') {
            return citoyen.listFollows();
          }
        },
      },
    ],
  },
  {
    find() {
      // verifier parent
      return collection.find({ _id: new Mongo.ObjectID(scopeId), parent: { $exists: true } }, { fields: { _id: 1, parent: 1 } });
    },
    children: [
      {
        find(sousOrga) {
          if (sousOrga) {
            const arrayParent = Object.keys(sousOrga.parent);

            // liste des membres et admin de l'orga
            if (scope === 'organizations') {
              const options = {
                fields: {
                  'links.members': 1, _id: 1, oceco: 1, name: 1,
                },
              };
              return collection.find({ _id: new Mongo.ObjectID(arrayParent['0']) }, options);
            }
          }
        },
        children: [
          {
            find(orgaParent) {
              return orgaParent.listMembers(search, scopeId, limit);
            },
          },
        ],
      },
    ],
  },
  ];
});

Meteor.publish('actionPodomoro', function () {
  if (!this.userId) {
    return null;
  }
  const selectUserid = `podomoro.${this.userId}.statusPodomoro`;
  const eventActions = Actions.find({ [selectUserid]: { $in: ['working', 'paused', 'resting'] }, status: 'todo' });
  return eventActions;
});

Meteor.publishComposite('detailActions', function (scope, scopeId, roomId, actionId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(actionId, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'events', 'citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const orgId = orgaIdScope({ scope, scopeId });

  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(event) {
          if (scope === 'events') {
            const parent = event.organizerEvent();
            const collectionParent = nameToCollection(parent[0].type);
            const optionsParent = {};
            if (['citoyens', 'projects', 'organizations'].includes(parent[0].type)) {
              const { fields } = optionsNoFieldsScope(parent[0].type);
              optionsParent.fields = fields;
            }
            return collectionParent.find({
              _id: new Mongo.ObjectID(parent[0].values[0]._id.valueOf()),
            }, optionsParent);
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events' || scope === 'citoyens') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
      },
      {
        find() {
          if (orgId && orgId.length > 0) {
            const OrgaArrayIds = orgId.map((id) => new Mongo.ObjectID(id));
            return Organizations.find({ _id: { $in: [OrgaArrayIds['0']] } }, { fields: { _id: 1 } });
          }
        },
      },
      {
        find() {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events' || scope === 'citoyens') {
            return Actions.find({ _id: new Mongo.ObjectID(actionId) });
          }
        },
        children: [
          {
            find(action) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(action.creator),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
          {
            find(action) {
              return action.listContributors();
            },
          },
          {
            find(action) {
              return action.photoActionsAlbums();
            },
          },
          {
            find(action) {
              return action.docActionsList();
            },
          },
          {
            find(action) {
              if (action.answerId) {
                return Answers.find({ _id: new Mongo.ObjectID(action.answerId) }, { fields: { _id: 1, form: 1 } });
              }
            },
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('detailAnswers', function (orgaCibleId, scope, scopeId, answerId) {
  check(orgaCibleId, String);
  check(scopeId, Match.Maybe(String));
  check(scope, String);
  check(answerId, String);
  check(scope, Match.Where(function (name) {
    return ['forms'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }

  return {
    find() {
      const options = {};
      if (!scopeId) {
        const answerOne = Answers.findOne({ _id: new Mongo.ObjectID(answerId) }, { fields: { form: 1 } });
        return collection.find({ _id: new Mongo.ObjectID(answerOne.form) }, options);
      } else {
        return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
      }
    },
    children: [
      {
        find() {
          return Organizations.find({ _id: new Mongo.ObjectID(orgaCibleId) }, { fields: { _id: 1, 'links.members': 1 } });
        },
        children: [
          {
            find(orga) {
              return orga.listRoleType('Financeur');
            },
          },
          {
            find(orga) {
              return orga.listRoleType('maitreOuvrage');
            },
          },
        ],
      },
      {
        find() {
          return Answers.find({ _id: new Mongo.ObjectID(answerId) });
        },
        children: [
          {
            find(answer) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(answer.user),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
          {
            find(answer) {
              if (answer.project && answer.project.id) {
                return Projects.find({
                  _id: new Mongo.ObjectID(answer.project.id),
                }, {
                  fields: {
                    name: 1,
                    profilThumbImageUrl: 1,
                    'links.contributors': 1,
                  },
                });
              }
            },
            children: [{
              find(project) {
                return project.listContributors();
              },
            },
            ],
          },
          {
            find(answer) {
              if (answer && answer.answers && answer.answers.aapStep1 && answer.answers.aapStep1.depense) {
                const arrayValues = Object.values(answer.answers.aapStep1.depense);
                if (arrayValues && arrayValues.length > 0) {
                  const arrayFilterActionId = arrayValues.filter((depense) => depense.actionid && isValidObjectId(depense.actionid));
                  if (arrayFilterActionId && arrayFilterActionId.length > 0) {
                    const arrayActionId = arrayFilterActionId.map((depense) => new Mongo.ObjectID(depense.actionid));
                    if (arrayActionId) {
                      return Actions.find({
                        _id: { $in: arrayActionId },
                      }, {
                        fields: {
                          name: 1,
                          parentId: 1,
                          parentType: 1,
                          idParentRoom: 1,
                          tasks: 1,
                        },
                      });
                    }
                  }
                }
              }
            },
            children: [{
              find(action) {
                if (action.tasks && action.tasks.length > 0) {
                  const arrayUserId = action.tasks.filter((task) => task.userId).map((task) => new Mongo.ObjectID(task.userId));
                  return Citoyens.find({
                    _id: { $in: arrayUserId },
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                }
              },
            },
            ],
          },
          /* {
            find(answer) {
              return answer.listContributors();
            },
          },
          {
            find(action) {
              return answer.photoActionsAlbums();
            },
          }, */
        ],
      },
    ],
  };
});

Meteor.publishComposite('answersDetailComments', function (orgaCibleId, scope, scopeId, answerId) {
  check(orgaCibleId, String);
  check(scopeId, String);
  check(scope, String);
  check(answerId, String);
  check(scope, Match.Where(function (name) {
    return ['forms'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }

  const orgId = orgaIdScope({ scope, scopeId });

  return {
    find() {
      const options = {};
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find() {
          if (orgId && orgId.length > 0) {
            const OrgaArrayIds = orgId.map((id) => new Mongo.ObjectID(id));
            return Organizations.find({ _id: { $in: [OrgaArrayIds['0']] } }, { fields: { _id: 1 } });
          }
        },
      },
      {
        find() {
          return Answers.find({ _id: new Mongo.ObjectID(answerId) });
        },
        children: [
          {
            find(answer) {
              return Citoyens.find({
                _id: new Mongo.ObjectID(answer.user),
              }, {
                fields: {
                  name: 1,
                  profilThumbImageUrl: 1,
                },
              });
            },
          },
          {
            find(answer) {
              return answer.listComments();
            },
            children: [
              {
                find(comment) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(comment.author),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('actionsDetailComments', function (scope, scopeId, roomId, actionId) {
  check(scopeId, String);
  check(scope, String);
  check(roomId, String);
  check(actionId, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'events', 'citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }

  const orgId = orgaIdScope({ scope, scopeId });

  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find() {
          if (orgId && orgId.length > 0) {
            const OrgaArrayIds = orgId.map((id) => new Mongo.ObjectID(id));
            return Organizations.find({ _id: { $in: [OrgaArrayIds['0']] } }, { fields: { _id: 1 } });
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'organizations' || scope === 'projects' || scope === 'events' || scope === 'citoyens') {
            // return Rooms.find({ _id: new Mongo.ObjectID(roomId) });
            return scopeD.detailRooms(roomId);
          }
        },
        children: [
          {
            find() {
              if (scope === 'organizations' || scope === 'projects' || scope === 'events' || scope === 'citoyens') {
                return Actions.find({ _id: new Mongo.ObjectID(actionId) });
              }
            },
            children: [
              {
                find(action) {
                  return Citoyens.find({
                    _id: new Mongo.ObjectID(action.creator),
                  }, {
                    fields: {
                      name: 1,
                      profilThumbImageUrl: 1,
                    },
                  });
                },
              },
              {
                find(action) {
                  return action.listComments();
                },
                children: [
                  {
                    find(comment) {
                      return Citoyens.find({
                        _id: new Mongo.ObjectID(comment.author),
                      }, {
                        fields: {
                          name: 1,
                          profilThumbImageUrl: 1,
                        },
                      });
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  };
});

Meteor.publishComposite('listAttendees', function (scopeId) {
  check(scopeId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Events.find({ _id: new Mongo.ObjectID(scopeId) });
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(event) {
          return event.listAttendeesValidate();
        },
        children: [
          {
            find(citoyen) {
              return Meteor.users.find({
                _id: citoyen._id.valueOf(),
              }, {
                fields: {
                  'status.online': 1,
                },
              });
            },
          },
        ],
      },
      {
        find(event) {
          return event.listAttendeesIsInviting();
        },
        children: [
          {
            find(citoyen) {
              return Meteor.users.find({
                _id: citoyen._id.valueOf(),
              }, {
                fields: {
                  'status.online': 1,
                },
              });
            },
          },
        ],
      },
      {
        find(event) {
          return event.listAttendeesOrgaValidate();
        },
      },
    ],
  };
});

Meteor.publishComposite('listMembers', function ({ scopeId, invite, search, limit }) {
  check(scopeId, String);
  check(invite, Match.Maybe(Boolean));
  check(search, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return Organizations.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(organisation) {
          return organisation.listMembers(search, null, limit);
        },
      },
      {
        find(organisation) {
          if (invite ?? false) {
            return organisation.listMembersInvite();
          } else {
            return null;
          }
        },
      },
    ],
  };
});

Meteor.publishComposite('listAssignActions', function ({ scopeId, actionId, search, limit }) {
  check(scopeId, String);
  check(actionId, String);
  check(search, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  if (!this.userId) {
    return null;
  }
  const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId) });

  return [{
    find() {
      const options = {};
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return Organizations.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(organisation) {
          return organisation.listMembersActions(actionId, search, limit);
        },
      },
    ],
  },
  {
    find() {
      if (actionOne.parentType === 'projects') {
        return Projects.find({ _id: new Mongo.ObjectID(actionOne.parentId) }, { fields: { costum: 0 } });
      }
    },
    children: [
      {
        find(project) {
          return project.listContributorsActions(actionId, search, limit);
        },
      },
    ],
  }];
});

Meteor.publishComposite('listAssignTaskActions', function ({ scopeId, actionId, taskId, search, limit }) {
  check(scopeId, String);
  check(actionId, String);
  check(taskId, String);
  check(search, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  if (!this.userId) {
    return null;
  }
  const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId) });

  return [{
    find() {
      const options = {};
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return Organizations.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(organisation) {
          return organisation.listMembersTaskActions(actionId, taskId, search, limit);
        },
      },
    ],
  },
  {
    find() {
      if (actionOne.parentType === 'projects') {
        return Projects.find({ _id: new Mongo.ObjectID(actionOne.parentId) }, { fields: { costum: 0 } });
      }
    },
    children: [
      {
        find(project) {
          return project.listContributorsTaskActions(actionId, taskId, search, limit);
        },
      },
    ],
  }];
});


Meteor.publishComposite('listMembersToBeValidated', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations', 'projects'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      if (['projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(organisation) {
          return organisation.listMembersToBeValidated();
        },
        children: [
          {
            find(citoyen) {
              return Meteor.users.find({
                _id: citoyen._id.valueOf(),
              }, {
                fields: {
                  'status.online': 1,
                },
              });
            },
          }, /* ,
          {
            find(citoyen) {
              return citoyen.documents();
            },
          }, */
        ],
      },
    ],
  };
});

Meteor.publishComposite('listContributors', function ({ scopeId, search, limit }) {
  check(scopeId, String);
  check(search, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Projects.find({ _id: new Mongo.ObjectID(scopeId) }, { fields: { costum: 0 } });
    },
    children: [
      {
        find(project) {
          return project.listContributors(search, limit);
        },
      },
    ],
  };
});

Meteor.publishComposite('listFollows', function (scopeId) {
  check(scopeId, String);

  if (!this.userId) {
    return null;
  }
  return {
    find() {
      return Citoyens.find({ _id: new Mongo.ObjectID(scopeId) }, {
        fields: {
          _id: 1,
          name: 1,
          'links.follows': 1,
          profilThumbImageUrl: 1,
        },
      });
    },
    children: [
      {
        find(citoyen) {
          return citoyen.listFollows();
        },
        children: [
          {
            find(citoyen) {
              return Meteor.users.find({
                _id: citoyen._id.valueOf(),
              }, {
                fields: {
                  'status.online': 1,
                },
              });
            },
          }, /* ,
          {
            find(citoyen) {
              return citoyen.documents();
            },
          }, */
        ],
      },
    ],
  };
});


Meteor.publish('poles.actions2', function (raffId, poleName) {
  check(raffId, String);
  check(poleName, String);
  if (!this.userId) {
    return null;
  }
  const queryProjectId = `parent.${raffId}`;
  const poleProjects = Projects.find({ $and: [{ tags: poleName }, { [queryProjectId]: { $exists: 1 } }] }).fetch();
  const poleProjectsId = [];
  poleProjects.forEach((element) => {
    poleProjectsId.push(element._id.valueOf());
  });
  const eventsArrayId = [];
  Events.find({ organizerId: { $in: poleProjectsId } }).forEach(function (event) { eventsArrayId.push(event._id.valueOf()); });

  const inputDate = new Date();
  const query = {};
  // query.endDate = { $gte: inputDate };
  // query.parentId = { $in: [...eventsArrayId, ...poleProjectsId] };
  // query.status = 'todo';

  query.$or = [];
  query.$or.push({ answerId: { $exists: false }, endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [...eventsArrayId, ...poleProjectsId] }, status: 'todo' });
  query.$or.push({ answerId: { $exists: false }, endDate: { $exists: false }, parentId: { $in: [...poleProjectsId] }, status: 'todo' });

  const options = {};
  options.sort = {
    startDate: 1,
  };

  const eventActions = Actions.find(query);
  return eventActions;
});

Meteor.publish('all.avatarOne', function (raffId) {
  check(raffId, String);
  if (!this.userId) {
    return null;
  }

  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(raffId) }, { fields: { _id: 1 } });
  if (!orgaOne) {
    return null;
  }
  const allActions = orgaOne.actionsAll();
  const allIdsCitoyens = allActions.map((k) => {
    if (k.links && k.links.contributors) {
      const arrayContributors = arrayLinkProperNoObject(k.links.contributors);
      return arrayContributors;
    }
    return false;
  }).filter(Boolean);
  const mergeDedupe = (arr) => [...new Set([].concat(...arr))];
  const arrayAllMerge = mergeDedupe(allIdsCitoyens);
  const arrayAllMergeMongoId = arrayAllMerge.map((k) => new Mongo.ObjectID(k));
  return Citoyens.find({ _id: { $in: arrayAllMergeMongoId } }, { fields: { name: 1, username: 1, profilThumbImageUrl: 1 } });
});

Meteor.publish('all.actions2', function (raffId) {
  check(raffId, String);

  if (!this.userId) {
    return null;
  }

  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(raffId) }, { fields: { _id: 1 } });
  if (!orgaOne) {
    return null;
  }
  return orgaOne.actionsAll();
});

Meteor.publishComposite('all.actionsSCope', function (scope, scopeId, startDateCal, endDateCal) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['citoyens', 'organizations', 'projects'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  check(startDateCal, Match.Maybe(Date));
  check(endDateCal, Match.Maybe(Date));

  if (!this.userId) {
    return null;
  }

  return {
    find() {
      const options = {};
      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'organizations') {
            return scopeD.listProjectsEventsCreator(null, startDateCal, endDateCal);
          }
        },
      },
      {
        find(scopeD) {
          return scopeD.actionsAllAgenda(startDateCal, endDateCal);
        },
        children: [{
          find(action) {
            if (scope === 'citoyens') {
              const collectionScope = nameToCollection(action.parentType);
              return collectionScope.find({ _id: new Mongo.ObjectID(action.parentId) }, { fields: { _id: 1, name: 1 } });
            }
          },
        }],
      },
    ],
  };
});


Meteor.publishComposite('all.user.actions2', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['citoyens'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const { fields } = optionsNoFieldsScope('citoyens');
      options.fields = fields;
      return collection.find({ _id: new Mongo.ObjectID(scopeId) }, options);
    },
    children: [
      {
        find() {
          return Lists.find({ name: { $in: ['organisationTypes'] } });
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.actionsUserAll(scopeId);
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listOrganizationsCreator();
          }
        },
        children: [{
          find(orgaOne) {
            return orgaOne.actionsUserAll(scopeId);
          },
          children: [{
            find(actionOne) {
              if (actionOne.parentType && actionOne.parentId) {
                const collection = nameToCollection(actionOne.parentType);
                return collection.find({ _id: new Mongo.ObjectID(actionOne.parentId) }, { fields: { _id: 1, name: 1, 'links.projects': 1, [`links.contributors.${this.userId}`]: 1, parent: 1, organizer: 1 } });
              }
            },
          }],
        }],
      },
    ],
  };
});


Meteor.publishComposite('user.actions', function (scope, scopeId, etat) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations'].includes(name);
  }));
  check(etat, String);
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const UserId = `links.contributors.${this.userId}`;
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return collection.find(query, options);
    },
    children: [{
      find() {
        return Lists.find({
          name: {
            $in: ['eventTypes'],
          },
        });
      },
    },
    {
      find(scopeD) {
        if (scope === 'organizations') {
          return scopeD.listProjectsEventsCreator1M();
        }
      },
      children: [{
        find(scopeD) {
          const finished = `finishedBy.${this.userId}`;
          const query = {};
          query.parentId = scopeD._id.valueOf();
          query[UserId] = {
            $exists: 1,
          };
          query.status = 'todo';
          const option = {};
          if (etat === 'aFaire') {
            query[finished] = { $exists: false };
            option.sort = { endDate: -1 };
          } else if (etat === 'enAttente') {
            query[finished] = 'toModerate';
            option.sort = { endDate: -1 };
          } else if (etat === 'valides') {
            /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
            query[finished] = 'validated';
            option.sort = { endDate: -1 };
            option.limit = 100;
          }
          // console.log(query);
          return Actions.find(query, option);
        },
      },
      ],
    },
    {
      find(scopeD) {
        if (scope === 'organizations') {
          return scopeD.listProjects();
        }
      },
      children: [{
        find(scopeD) {
          const finished = `finishedBy.${this.userId}`;
          const query = {};
          query.parentId = scopeD._id.valueOf();
          query[UserId] = {
            $exists: 1,
          };
          query.status = 'todo';
          query.answerId = { $exists: false };
          const option = {};
          if (etat === 'aFaire') {
            query[finished] = { $exists: false };
            option.sort = { endDate: -1 };
          } else if (etat === 'enAttente') {
            query[finished] = 'toModerate';
            option.sort = { endDate: -1 };
          } else if (etat === 'valides') {
            /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
            query[finished] = 'validated';
            option.sort = { endDate: -1 };
            option.limit = 100;
          }
          // console.log(query);
          return Actions.find(query, option);
        },
      },
      ],
    },
    {
      find() {
        const finished = `finishedBy.${this.userId}`;
        const query = {};
        query.parentId = scopeId;
        query[UserId] = {
          $exists: 1,
        };
        query.status = 'todo';
        const option = {};
        if (etat === 'aFaire') {
          query[finished] = { $exists: false };
          option.sort = { endDate: -1 };
        } else if (etat === 'enAttente') {
          query[finished] = 'toModerate';
          option.sort = { endDate: -1 };
        } else if (etat === 'valides') {
          /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
          query[finished] = 'validated';
          option.sort = { endDate: -1 };
          option.limit = 100;
        }
        // console.log(query);
        return Actions.find(query, option);
      },
    },
    ],
  };
});

Meteor.publishComposite('user.actions.historique', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['organizations'].includes(name);
  }));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const UserId = `links.contributors.${this.userId}`;
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      const { fields } = optionsNoFieldsScope('organizations');
      options.fields = fields;
      return collection.find(query, options);
    },
    children: [{
      find(scopeD) {
        if (scope === 'organizations') {
          // return scopeD.listProjectsEventsCreator1M();
          if (scopeD.links && scopeD.links.projects) {
            const projectIds = arrayLinkParentNoObject(scopeD.links.projects, 'projects');
            const query = {};
            query.$or = [];
            projectIds.forEach((id) => {
              const queryCo = {};
              queryCo[`organizer.${id}`] = { $exists: true };
              query.$or.push(queryCo);
            });
            /* const options = {};
            options.sort = {
              startDate: 1
            }; */
            const arrayEventsIds = Events.find(query).fetch().map((event) => event._id.valueOf());
            const finished = `finishedBy.${this.userId}`;
            const queryAction = {};
            queryAction.parentId = { $in: [...arrayEventsIds, ...projectIds, scopeId] };
            queryAction[UserId] = {
              $exists: 1,
            };
            const option = {};
            /* WARNING pour l'historique listProjectsEventsCreator1M ne va pas aller car il prend les evenements qui ne sont pas terminer ou 15 jous apres la fin */
            queryAction[finished] = 'validated';
            option.sort = { endDate: -1 };
            option.limit = 100;
            // console.log(queryAction);
            return Actions.find(queryAction, option);
          }
        }
      },
    },
    ],
  };
});


Meteor.publish('poles.events', function (raffId, poleName) {
  check(raffId, String);
  check(poleName, Match.Maybe(String));
  if (!this.userId) {
    return null;
  }

  const queryProjectId = `parent.${raffId}`;
  const projectId = Projects.find({ [queryProjectId]: { $exists: 1 } }).fetch();
  const projectsId = [];
  projectId.forEach((element) => {
    projectsId.push(element._id);
  });
  let poleProjects;
  if (poleName) {
    poleProjects = Projects.find({ $and: [{ tags: poleName }, { _id: { $in: projectsId } }] }).fetch();
  } else {
    poleProjects = Projects.find({ _id: { $in: projectsId } }).fetch();
  }

  if (poleProjects && poleProjects.length > 0) {
    // / const poleProjectsId = [];
    /* poleProjects.forEach((element) => {
      poleProjectsId.push(element._id.valueOf());
    }); */

    const query = {};
    const inputDate = new Date();
    query.endDate = { $gte: inputDate };
    query.status = { $nin: statusNoVisible };
    query.$or = [];
    poleProjects.forEach((element) => {
      const queryCo = {};
      queryCo[`organizer.${element._id.valueOf()}`] = { $exists: true };
      query.$or.push(queryCo);
    });

    // query.organizerId = { $in: poleProjectsId };

    const options = {};
    options.sort = {
      startDate: 1,
    };
    return Events.find(query, options);
  }
});

// Meteor.publish('raffinerie.members',function(raffhId){
//   let id = new Mongo.ObjectID(raffId)
//   let orgaCursor = Organizations.findOne({_id: id }).links.members
//   console.log(arrayLinkProper(orgaCursor))
// })

// Meteor.publish('poles.list', function())

Meteor.publish('scopeActionIndicatorCount', function (scope, scopeId, status, milestoneId) {
  check(scopeId, String);
  check(scope, String);
  check(status, String);
  check(status, Match.Where(function (name) {
    return ['todo', 'done', 'disabled', 'contributors', 'finished', 'toValidated', 'all'].includes(name);
  }));
  check(scope, Match.Where(function (name) {
    return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
  }));
  check(milestoneId, Match.Maybe(String));

  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
  if (!scopeOne) {
    return null;
  }
  const countName = milestoneId ? `countScopeAction.${scopeId}.${scope}.${status}.${milestoneId}` : `countScopeAction.${scopeId}.${scope}.${status}`;
  return new Counter(countName, scopeOne.actionIndicatorCount(status, milestoneId));
});

Meteor.publish('scopeUserActionIndicatorCount', function (scope, scopeId, status, userId) {
  check(scopeId, String);
  check(scope, String);
  check(status, String);
  check(status, Match.Where(function (name) {
    return ['inProgress', 'toModerate', 'validated', 'novalidated', 'all'].includes(name);
  }));
  check(scope, Match.Where(function (name) {
    return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
  }));
  check(userId, String);

  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
  if (!scopeOne) {
    return null;
  }
  if (!Citoyens.findOne({ _id: new Mongo.ObjectID(userId) })) {
    return null;
  }

  return new Counter(`countScopeUserAction.${userId}.${scopeId}.${scope}.${status}`, scopeOne.userActionIndicatorCount(status, userId));
});


Meteor.publishComposite('scopeUsersActionIndicatorCount', function (scope, scopeId) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations'].includes(name);
  }));

  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
  if (!scopeOne) {
    return null;
  }

  return [{
    find() {
      const options = {};
      const query = {};
      query.scopeId = scopeId;
      query.scope = scope;
      return CountActions.find(query, options);
    },
  },
  {
    find() {
      const options = {};
      const query = {};
      query.scopeId = scopeId;
      query.scope = scope;
      return CountUserActions.find(query, options);
    },
    /* children: [{
      find(scopeD) {
        return Citoyens.find({ _id: new Mongo.ObjectID(scopeD.userId) }, { fields: { _id: 1, name: 1, username: 1, profilThumbImageUrl: 1 } });
      },
    },
    ], */
  }];
});

Meteor.publish('creditHistoryStats', function (scopeId) {
  check(scopeId, String);
  if (!this.userId) {
    return null;
  }

  const scopeOne = Organizations.findOne({ _id: new Mongo.ObjectID(scopeId) });

  if (!scopeOne) {
    return null;
  }
  if (!scopeOne.isAdmin()) {
    return null;
  }

  return LogOrgaMonthCredit.find({ organizationId: scopeId }, { sort: { year: 1, month: 1 } });
});

