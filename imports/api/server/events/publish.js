import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

import { Projects } from '../../collection/projects.js';
import { Events } from '../../collection/events.js';
import { statusNoVisible } from '../../helpers';

Meteor.publish('polesOrAll.events', function (organizationId, poleName) {
  check(organizationId, String);
  check(poleName, Match.Maybe(String));
  if (!this.userId) {
    return null;
  }

  // Initialisation de la requête de base pour les projets
  const query = {
    [`parent.${organizationId}`]: { $exists: 1 }
  };

  // Si poleName est fourni, ajoute le critère à la requête
  if (poleName) {
    query.tags = poleName;
  }

  // Exécution d'une seule requête pour récupérer les projets selon les critères
  const projects = Projects.find(query, { fields: { _id: 1 } }).fetch();

  // Extraction des IDs des projets
  const projectIds = projects.map(project => project._id);

  if (projectIds && projectIds.length > 0) {

    const query = {};
    const inputDate = new Date();
    query.endDate = { $gte: inputDate };
    query.status = { $nin: statusNoVisible };
    query.$or = [];
    projectIds.forEach((_id) => {
      const queryCo = {};
      queryCo[`organizer.${_id.valueOf()}`] = { $exists: true };
      query.$or.push(queryCo);
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };
    return Events.find(query, options);
  }
});