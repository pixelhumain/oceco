/* eslint-disable no-else-return */
/* eslint-disable consistent-return */
/* eslint-disable no-underscore-dangle */
import express from 'express';
import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';
import { Random } from 'meteor/random';
import { DDPCommon } from 'meteor/ddp-common';
import { DDP } from 'meteor/ddp';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';

// import bodyParser from 'body-parser';
import crypto from 'crypto';
import SimpleSchema from 'simpl-schema';
import FileType from 'file-type';
import { fetch, Headers, Request, Response } from 'meteor/fetch';

import ical from 'ical-generator';

import { login } from 'meteor/communecter:account';

import { Organizations } from '../collection/organizations.js';
import { Projects } from '../collection/projects.js';
import { nameToCollection, arrayLinkParent, statusNoVisible } from '../helpers.js';
import { Actions } from '../collection/actions.js';
import { SchemasActionsRest } from '../schema/actions.js';
import { Citoyens } from '../collection/citoyens.js';
import { Events } from '../collection/events.js';

// schemas
import {
  baseSchema, geoSchema, preferences,
} from '../schema/schema.js';

import { orgaIdScope } from '../helpersOrga.js';

import { optionsNoFieldsScope, queryOrPrivateScopeLinks } from '../helpers.js';

import { bin2hex, generateRestToken, verifyRestToken, verifyPersonalToken, decryptToken, generateEncryptedToken } from './helpersToken.js';

// function bin2hex(bytesLength) {
//   const bin = crypto.randomBytes(bytesLength);
//   return Buffer.from(bin).toString('hex');
// }

// comment recréé l'inscription d'un user sur rocketchat
// user présent sur co mais pas sur chat
// depuis chat créé l'user

const generateToken = function (userId, tokenName) {
  const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'personalAccessToken' });
  // console.log({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'personalAccessToken' });
  if (!citoyenOne) {
    const token = bin2hex(16);
    const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
    const buf = Buffer.from(tokenHash);
    const tokenHashBase64 = buf.toString('base64');
    const loginToken = {};
    loginToken.createdAt = Math.floor(new Date().getTime() / 1000);
    loginToken.hashedToken = tokenHashBase64;
    loginToken.type = 'personalAccessToken';
    loginToken.name = tokenName;
    loginToken.lastTokenPart = tokenHashBase64.substr(-6);
    const modifier = {};
    modifier.$addToSet = { loginTokens: loginToken };
    Citoyens.update({ _id: new Mongo.ObjectID(userId) }, modifier);
    return token;
  }
  return false;
};

// const generateRestToken = function (userId, tokenName) {
//   const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'restAccessToken' });
//   // console.log({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'personalAccessToken' });
//   if (!citoyenOne) {
//     const token = bin2hex(16);
//     const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
//     const buf = Buffer.from(tokenHash);
//     const tokenHashBase64 = buf.toString('base64');
//     const loginToken = {};
//     loginToken.createdAt = Math.floor(new Date().getTime() / 1000);
//     loginToken.hashedToken = tokenHashBase64;
//     loginToken.type = 'restAccessToken';
//     loginToken.name = tokenName;
//     loginToken.lastTokenPart = tokenHashBase64.substr(-6);
//     const modifier = {};
//     modifier.$addToSet = { loginTokens: loginToken };
//     Citoyens.update({ _id: new Mongo.ObjectID(userId) }, modifier);
//     return token;
//   } else {
//     const token = bin2hex(16);
//     const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
//     const buf = Buffer.from(tokenHash);
//     const tokenHashBase64 = buf.toString('base64');
//     const modifier = {};
//     modifier.$set = {};
//     modifier.$set['loginTokens.$.createdAt'] = Math.floor(new Date().getTime() / 1000);
//     modifier.$set['loginTokens.$.hashedToken'] = tokenHashBase64;
//     modifier.$set['loginTokens.$.lastTokenPart'] = tokenHashBase64.substr(-6);
//     Citoyens.update({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName }, modifier);
//     return token;
//   }
// };

// const verifyRestToken = function (userId, tokenName, token) {
//   const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'restAccessToken' });
//   if (citoyenOne) {
//     const loginToken = citoyenOne.loginTokens.filter((l) => l.name === tokenName && l.type === 'restAccessToken');

//     const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
//     const buf = Buffer.from(tokenHash);
//     const tokenHashBase64 = buf.toString('base64');

//     if (tokenHashBase64 === loginToken[0].hashedToken) {
//       return true;
//     }
//   }
//   return false;
// };

const userNotConnected = function (userId, tokenName) {
  const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId) });
  const userM = Meteor.users.findOne({ _id: userId });

  if (citoyenOne) {
    if (userM) {
      const token = generateToken(userId, tokenName);
      if (token) {
        const profile = {};
        profile.token = token;
        Meteor.users.update(userId, { $set: { profile } });
      }
      // Meteor.users.update(userId, { $set: { emails: [citoyenOne.email] } });
    } else {
      Meteor.users.insert({ _id: userId, emails: [citoyenOne.email] });
      const token = generateToken(userId, tokenName);
      if (token) {
        const profile = {};
        profile.token = token;
        Meteor.users.update(userId, { $set: { profile } });
      }
    }

    /* const stampedToken = Accounts._generateStampedLoginToken();
    Meteor.users.update(userId,
      { $push: { 'services.resume.loginTokens': stampedToken } },
    ); */
    return true;
  }
  return false;
};

function verifyToken(req, res, next) {
  const authHeader = req.headers.authorization;
  // console.log(authHeader);
  let token = req.headers['x-access-token'];
  let userId = req.headers['x-user-id'];
  let tokenName = req.headers['x-token-name'] ?? 'restOceco';

  if (authHeader && !req.headers['x-access-token'] && !req.headers['x-user-id'] && !req.headers['x-token-name']) {
    if (authHeader && authHeader.startsWith('Bearer ')) {
      const tokenAuth = authHeader.substring(7, authHeader.length); // Extrait le token après 'Bearer '
      req.token = tokenAuth; // Stocke le token dans l'objet de requête pour y accéder plus tard
      const tokenData = decryptToken(tokenAuth);
      token = tokenData.token;
      req.headers['x-access-token'] = tokenData.token;
      userId = tokenData.userId;
      req.headers['x-user-id'] = tokenData.userId;
      tokenName = tokenData.tokenName ?? 'restOceco';
      req.headers['x-token-name'] = tokenData.tokenName ?? 'restOceco';
    } else {
      // Gérer le cas où il n'y a pas de token ou le format est incorrect
      return res.status(403).json({ message: "Token non fourni ou format incorrect" });
    }
  }

  if (!token) {
    return res.status(403).json({ auth: false, message: 'No token provided.' });
  }

  if (verifyRestToken(userId, tokenName, token)) {
    //
  } else if (verifyPersonalToken(userId, 'sso', token)) {
    //
  } else if (token !== Meteor.settings.ocecoApiToken) {
    return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
  }
  if (!userId) {
    return res.status(403).json({ auth: false, message: 'No userId provided.' });
  }
  // const userOne = Meteor.users.findOne({ _id: userId });
  const userOne = userNotConnected(userId, 'comobi');
  if (!userOne) {
    return res.status(403).json({ auth: false, message: 'user not exist' });
  }
  next();
}

function verifyTokenGitlab(req, res, next) {
  const token = req.headers['x-gitlab-token'];
  const event = req.headers['x-gitlab-event'];
  if (!token) {
    return res.status(400).json({ auth: false, message: 'No token provided.' });
  }
  const projectOne = Projects.findOne({ 'oceco.git.webhookSecret': token }, { fields: { _id: 1, 'oceco.git.webhookSecret': 1 } });
  if (!projectOne) {
    return res.status(400).json({ auth: false, message: 'Failed to authenticate token.' });
  }
  if (!event) {
    return res.status(400).json({ auth: false, message: 'No X-Gitlab-Event found on request' });
  }
  next();
}

function verifyTokenGithub(req, res, next) {
  const signature = req.headers['x-hub-signature'];
  const event = req.headers['x-github-event'];

  /* if (!req.rawBody) {
    return res.status(400).json({ auth: false, message: 'No rawBody.' });
  } */

  if (req.params.projectId ?? false) {
    return res.status(400).json({ auth: false, message: 'No projectId.' });
  }

  if (!event) {
    return res.status(400).json({ auth: false, message: 'No X-GitHub-Event found on request' });
  }

  if (!signature) {
    return res.status(400).json({ auth: false, message: 'No token provided.' });
  }

  const projectOne = Projects.findOne({ _id: new Mongo.ObjectID(req.params.projectid), 'oceco.git.webhookSecret': { $exists: true } }, { fields: { _id: 1, 'oceco.git.webhookSecret': 1 } });

  if (!projectOne) {
    return res.status(400).json({ auth: false, message: 'project not configured github.' });
  }

  const WEBHOOK_SECRET = projectOne.oceco.git.webhookSecret;
  const webhook_payload = req.body;
  const hmac = crypto.createHmac('sha1', WEBHOOK_SECRET);
  const webhookDigest = hmac.update(webhook_payload).digest('hex');

  const computedSignature = `sha1=${webhookDigest}`;
  const requestSignature = req.header('X-Hub-Signature');

  if (!crypto.timingSafeEqual(Buffer.from(computedSignature, 'utf8'), Buffer.from(requestSignature, 'utf8'))) {
    // console.log(`[Optimizely] Signatures did not match! Do not trust webhook request")`)
    return res.status(400).json({ auth: false, message: 'Invalid signature.' });
  }

  next();
}

function verifyTokenMeteor(req, res, next) {
  // console.log(req.headers);
  const token = req.headers['x-access-token'];
  const userId = req.headers['x-user-id'];
  const hash = crypto.createHash('sha256');
  hash.update(token);
  if (!token) {
    return res.status(403).json({ auth: false, message: 'No token provided.' });
  }

  const user = Meteor.users.findOne({ _id: userId, 'services.resume.loginTokens.hashedToken': hash.digest('base64') });

  if (!user) {
    return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
  }

  next();
}

/* function nocache(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
} */

const runAsUser = function (userId, func) {
  // const DDPCommon = Package['ddp-common'].DDPCommon;
  // put whatever you want in these properties
  const invocation = new DDPCommon.MethodInvocation({
    isSimulation: false,
    userId,
    setUserId: () => { },
    unblock: () => { },
    connection: {},
    randomSeed: Random.id(),
  });

  return DDP._CurrentInvocation.withValue(invocation, () => func());
};

const handleErrorAsJson = function (err, req, res) {
  if (err.sanitizedError && err.sanitizedError.errorType === 'Meteor.Error') {
    if (!err.sanitizedError.statusCode) {
      err.sanitizedError.statusCode = err.statusCode || 400;
    }

    // eslint-disable-next-line no-param-reassign
    err = err.sanitizedError;
  } else if (err.errorType === 'Meteor.Error') {
    if (!err.statusCode) err.statusCode = 400;
  } else {
    const { statusCode } = err;
    // eslint-disable-next-line no-param-reassign
    err = new Error();
    err.statusCode = statusCode;
  }

  const body = {
    error: err.error || 'internal-server-error',
    reason: err.reason || 'Internal server error',
    details: err.details,
    data: err.data,
  };

  return res.status(err.statusCode || 500).json(body);
};

/* const synchroAdmin = function ({ parentId, parentType }) {
  const collectionScope = nameToCollection(parentType);
  const scopeOne = collectionScope.findOne({
    _id: new Mongo.ObjectID(parentId),
  });

  // Todo : modifier avec parent en prenant le premier pour eviter les conflit si plusieurs
  if (scopeOne) {
    if (parentType === 'organizations') {
      const testConnectAdminRetour = Meteor.call('testConnectAdmin', { id: scopeOne._id.valueOf() });
      return testConnectAdminRetour;
    } else if (parentType === 'projects' || parentType === 'events') {
      const orgId = orgaIdScope({ scope: parentType, scopeId: scopeOne._id.valueOf() });
      if (orgId && orgId.length > 0) {
        const testConnectAdminRetour = Meteor.call('testConnectAdmin', { id: orgId['0'] });
        return testConnectAdminRetour;
      }
    }
  }
}; */

const synchroAdmin = function ({ parentId, parentType }) {
  const collectionScope = nameToCollection(parentType);
  const scopeOne = collectionScope.findOne({
    _id: new Mongo.ObjectID(parentId),
  });

  if (scopeOne) {
    if (parentType === 'organizations') {
      const testConnectAdminRetour = Meteor.call('testConnectAdmin', { id: scopeOne._id.valueOf() });
      return testConnectAdminRetour;
    } if (parentType === 'projects') {
      const project = `links.projects.${scopeOne._id.valueOf()}`;
      const organizationOne = Organizations.findOne({ [project]: { $exists: 1 }, oceco: { $exists: 1 } });
      const testConnectAdminRetour = Meteor.call('testConnectAdmin', { id: organizationOne._id.valueOf() });
      return testConnectAdminRetour;
    } if (parentType === 'events') {
      const event = `links.events.${scopeOne._id.valueOf()}`;
      const projectOne = Projects.findOne({ [event]: { $exists: 1 } });
      if (projectOne) {
        const project = `links.projects.${projectOne._id.valueOf()}`;
        const organizationOne = Organizations.findOne({ [project]: { $exists: 1 }, oceco: { $exists: 1 } });
        const testConnectAdminRetour = Meteor.call('testConnectAdmin', { id: organizationOne._id.valueOf() });
        return testConnectAdminRetour;
      }
    }
  }
};

const app = express();
/* WebApp.rawConnectHandlers.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Authorization,Content-Type,x-access-token,x-user-id');
  return next();
}); */

// WebApp.connectHandlers.use(express.json());

WebApp.connectHandlers.use((req, res, next) => {
  if (req.originalUrl.startsWith('/api/hooks/github')) {
    // Stripe requires the raw body to construct the event

    express.raw({ type: 'application/json' })(req, res, next);
  } else {
    express.json()(req, res, next);
  }
});

WebApp.connectHandlers.use(express.urlencoded({ extended: false }));
WebApp.connectHandlers.use(Meteor.bindEnvironment(app));


app.get('/api/me/profil', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  // console.log(req.body);
  runAsUser(userId, function () {
    try {

      const options = {};
      // const { fields } = optionsNoFieldsScope('citoyens');
      // options.fields = fields;
      // options.fields.links = 0;
      // options.fields.loginTokens = 0;
      // options.fields.services = 0;
      // options.fields.oceco = 0;
      // options.fields.preferences = 0;
      // options.fields.roles = 0;
      // options.fields.lastLoginDate = 0;
      // options.fields.profilRealBannerUrl = 0;
      // options.fields.profilMarkerImageUrl = 0;
      // options.fields.profilMediumImageUrl = 0;
      // options.fields.profilBannerUrl = 0;
      // options.fields.profilImageUrl = 0;
      // options.fields.socialNetwork = 0;
      // options.fields.geo = 0;
      // options.fields.geoPosition = 0;
      // options.fields.userWallet = 0;
      // options.fields.address = 0;
      // options.fields.telephone = 0;
      // options.fields.birthDate = 0;
      // options.fields.description = 0;
      // options.fields.shortDescription = 0;
      // options.fields.tags = 0;
      // options.fields.email = 0;
      // options.fields.profilThumbImageUrl = 0;
      options.fields = { _id: 1, username: 1, name: 1, slug: 1, created: 1, updated: 1, modified: 1, language: 1, collection: 1 };
      const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId) }, options);
      // console.log(retourCall);
      const valid = {
        status: true,
        msg: 'profil user (citoyens)',
        ...citoyenOne
      };

      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});


app.get('/api/me/organizations', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  // console.log(req.body);
  runAsUser(userId, function () {
    try {

      const options = {};
      options.fields = { _id: 1, username: 1, name: 1, slug: 1, created: 1, updated: 1, modified: 1, language: 1, collection: 1, preferences: 1, 'links.members': 1 };
      const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId) });
      const organizationsList = citoyenOne.listOrganizationsCreator();

      const valid = {
        status: true,
        msg: 'organizations user',
        organizations: organizationsList.fetch().map((orga) => {
          const orgaJson = orga;
          orgaJson.isAdmin = orga.links.members[userId].isAdmin ?? false;
          delete orgaJson.links;
          delete orgaJson.preferences;
          delete orgaJson.oceco;
          delete orgaJson.profilThumbImageUrl;
          return orgaJson;
        }),
      };

      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/create', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  // console.log(req.body);
  /* Todo
  verifier si user deja connecter ou pas à l'application
  */
  runAsUser(userId, function () {
    try {
      if (!req.body.parentId && !req.body.parentType) {
        req.body.parentId = userId;
        req.body.parentType = 'citoyens';
      }

      const actionPost = SchemasActionsRest.clean(req.body);

      // eslint-disable-next-line no-unused-vars
      const synchroRetour = synchroAdmin({ parentId: req.body.parentId, parentType: req.body.parentType });

      const retourCall = Meteor.call('insertAction', actionPost);
      // console.log(retourCall);
      const valid = {
        status: true,
        msg: 'action created',
        id: retourCall.data.id,
      };
      /* #swagger.responses[200] = {
            description: "action created",
            content: {
                "application/json": {
                    schema: {
                        status: true,
                        msg: 'action created',
                        id: 'eedcaecazec'
                    }
                }
            }
        }
    */
      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/task/create', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
        task: { type: String },
      }).validate(req.body);

      const actionOne = Actions.findOne({
        _id: new Mongo.ObjectID(req.body.id),
      });

      if (!actionOne) {
        const error = {
          status: false,
          msg: 'erreur : l\'action n\'existe pas',
        };
        res.status(200).json(error);
      } else {
        // eslint-disable-next-line no-unused-vars
        const synchroRetour = synchroAdmin({ parentId: actionOne.parentId, parentType: actionOne.parentType });

        const taskPost = {};
        taskPost.id = req.body.id;
        taskPost.task = req.body.task;

        const retourCall = Meteor.call('insertActionTask', taskPost);
        // console.log(retourCall);
        const valid = {
          status: true,
          msg: 'task created',
          id: retourCall,
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/task/list', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
      }).validate(req.body);

      const actionOne = Actions.findOne({
        _id: new Mongo.ObjectID(req.body.id),
      });

      if (!actionOne) {
        const error = {
          status: false,
          msg: 'erreur : l\'action n\'existe pas',
        };
        res.status(200).json(error);
      } else {
        // eslint-disable-next-line no-unused-vars
        const synchroRetour = synchroAdmin({ parentId: actionOne.parentId, parentType: actionOne.parentType });

        const { tasks } = actionOne;
        // authorComments
        // const arrayComments = tasks.map(comment => ({ ...comment, username: comment.authorComments().username }));
        // console.log(arrayComments);
        const valid = {
          status: true,
          tasks,
          msg: 'action task list',
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/task/checked', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
        taskId: { type: String },
      }).validate(req.body);

      // eslint-disable-next-line no-unused-vars
      const retourCall = Meteor.call('checkedActionTask', { id: req.body.id, taskId: req.body.taskId, checked: true });
      // console.log(retourCall);
      const valid = {
        status: true,
        msg: 'task checked',
      };
      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/comment/create', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
        text: { type: String },
      }).validate(req.body);

      const actionOne = Actions.findOne({
        _id: new Mongo.ObjectID(req.body.id),
      });

      if (!actionOne) {
        const error = {
          status: false,
          msg: 'erreur : l\'action n\'existe pas',
        };
        res.status(200).json(error);
      } else {
        // eslint-disable-next-line no-unused-vars
        const synchroRetour = synchroAdmin({ parentId: actionOne.parentId, parentType: actionOne.parentType });

        const commentPost = {};
        commentPost.contextId = req.body.id;
        commentPost.contextType = 'actions';
        commentPost.text = req.body.text;
        // console.log(commentPost);

        const retourCall = Meteor.call('insertComment', commentPost);
        // console.log(retourCall);
        const valid = {
          status: true,
          msg: 'comment created',
          id: retourCall._str,
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/comment/list', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
      }).validate(req.body);

      const actionOne = Actions.findOne({
        _id: new Mongo.ObjectID(req.body.id),
      });

      if (!actionOne) {
        const error = {
          status: false,
          msg: 'erreur : l\'action n\'existe pas',
        };
        res.status(200).json(error);
      } else {
        // eslint-disable-next-line no-unused-vars
        const synchroRetour = synchroAdmin({ parentId: actionOne.parentId, parentType: actionOne.parentType });

        const comments = actionOne.listComments();
        // authorComments
        const arrayComments = comments.map((comment) => ({ ...comment, username: comment.authorComments().username }));
        // console.log(arrayComments);
        const valid = {
          status: true,
          comments: arrayComments,
          msg: 'action comment list',
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/assign', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
        memberId: { type: String },
        parentId: { type: String },
        parentType: { type: String, allowedValues: ['organizations', 'projects', 'events'] },
      }).validate(req.body);

      // eslint-disable-next-line no-unused-vars
      const synchroRetour = synchroAdmin({ parentId: req.body.parentId, parentType: req.body.parentType });

      // eslint-disable-next-line no-unused-vars
      const retourCall = Meteor.call('assignMemberActionRooms', { id: req.body.id, memberId: req.body.memberId });
      // console.log(retourCall);
      const valid = {
        status: true,
        msg: 'member assign',
      };
      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/finishMe', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
      }).validate(req.body);

      // eslint-disable-next-line no-unused-vars
      const retourCall = Meteor.call('finishAction', { id: req.body.id });
      // console.log(retourCall);
      const valid = {
        status: true,
        msg: 'action finish',
      };
      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/listElement', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        parentId: { type: String },
        parentType: { type: String },
      }).validate(req.body);

      // eslint-disable-next-line no-unused-vars
      const synchroRetour = synchroAdmin({ parentId: req.body.parentId, parentType: req.body.parentType });

      const collection = nameToCollection(req.body.parentType);
      const query = {};
      query._id = new Mongo.ObjectID(req.body.parentId);
      const scope = collection.findOne(query);
      if (scope) {
        const actions = scope.listActionsCreator('all', 'todo');
        // ajouter les assigner
        const actionsJson = actions.map((action) => {
          const arrayUsername = action.listContributorsUsername();
          // console.log(arrayUsername.fetch())
          if (arrayUsername) {
            action.arrayUsername = arrayUsername.fetch().map((citoyen) => citoyen.username);
          }
          return action;
        });
        // console.log(actionsJson);
        const valid = {
          status: true,
          actions: actionsJson,
          msg: 'action element list',
        };
        res.status(200).json(valid);
      } else {
        const valid = {
          status: false,
          msg: 'no element',
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.get('/api/action/listMe', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      // const synchroRetour = synchroAdmin({ parentId: req.body.parentId, parentType: req.body.parentType });
      const query = {};
      query._id = new Mongo.ObjectID(userId);
      const scope = Citoyens.findOne(query);
      if (scope) {
        const actions = [];
        const listOrga = scope.listOrganizationsCreator();
        listOrga.forEach((orgaOne) => {
          const actionOrga = orgaOne.actionsUserAll(userId, 'aFaire').fetch();
          actions.push(...actionOrga);
        });

        const actionCitoyen = scope.actionsUserAll(userId, 'aFaire').fetch();
        actions.push(...actionCitoyen);

        // console.log(actions);
        const valid = {
          status: true,
          actions,
          msg: 'action user list',
        };
        res.status(200).json(valid);
      } else {
        const valid = {
          status: false,
          msg: 'no user',
        };
        res.status(200).json(valid);
        return;
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/listMe', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        search: { type: String, optional: true },
        parentId: { type: String, optional: true },
        parentType: { type: String, optional: true },
      }).validate(req.body);

      const query = {};
      query._id = new Mongo.ObjectID(userId);
      const scope = Citoyens.findOne(query);
      if (scope) {
        const actions = [];
        const listOrga = scope.listOrganizationsCreator();
        const parentId = req.body.parentId || false;
        listOrga.forEach((orgaOne) => {
          if (req.body.search) {
            const actionOrga = orgaOne.actionsUserAll(userId, 'aFaire', req.body.search).fetch();
            actions.push(...actionOrga);
          } else {
            const actionOrga = orgaOne.actionsUserAll(userId, 'aFaire').fetch();
            actions.push(...actionOrga);
          }
        });

        if (req.body.search) {
          const actionCitoyen = scope.actionsUserAll(userId, 'aFaire', req.body.search).fetch();
          actions.push(...actionCitoyen);
        } else {
          const actionCitoyen = scope.actionsUserAll(userId, 'aFaire').fetch();
          actions.push(...actionCitoyen);
        }

        let actionArray;
        if (parentId) {
          actionArray = actions.filter((action) => action.parentId === parentId);
        } else {
          const arrayProjectName = [];
          actionArray = actions.map((action) => {
            if (action.parentType === 'projects') {
              if (action.parentId && !arrayProjectName[action.parentId]) {
                arrayProjectName[action.parentId] = Projects.findOne({ _id: new Mongo.ObjectID(action.parentId) }, { fields: { _id: 1, name: 1 } }).name;
              }
              action.projectName = arrayProjectName[action.parentId];
            }
            return action;
          });
        }

        // console.log(actions);
        const valid = {
          status: true,
          actions: actionArray,
          msg: 'action user list',
        };
        res.status(200).json(valid);
      } else {
        const valid = {
          status: false,
          msg: 'no user',
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/dashboardElement', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        parentId: { type: String },
        parentType: { type: String },
      }).validate(req.body);

      const retour = Meteor.call('dashboardType', { parentId: req.body.parentId, parentType: req.body.parentType });

      if (retour) {
        const valid = {
          status: true,
          dashboard: retour,
          msg: 'action element dashboard',
        };
        res.status(200).json(valid);
      } else {
        const valid = {
          status: false,
          msg: 'no dashboard',
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/dashboardUserElement', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        parentId: { type: String },
        parentType: { type: String },
        userId: { type: String },
      }).validate(req.body);

      const retour = Meteor.call('dashboardUserType', { parentId: req.body.parentId, parentType: req.body.parentType, userId: req.body.userId });

      if (retour) {
        const valid = {
          status: true,
          dashboard: retour,
          msg: 'action element user dashboard',
        };
        res.status(200).json(valid);
      } else {
        const valid = {
          status: false,
          msg: 'no dashboard',
        };
        res.status(200).json(valid);
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/action/dashboardOrgaUserElement', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        parentId: { type: String },
        parentType: { type: String },
        userId: { type: String, optional: true },
      }).validate(req.body);

      /*
      si un seul project ?
      si un seul user ?
      */

      /* const organizationActionsCount = (parentId, user) => {
        const query = {};
        query.parentId = parentId;
        query.status = 'todo';
        query[`links.contributors.${user._id.valueOf()}`] = { $exists: true };
        return Actions.find(query).count() > 0;
      }; */

      // list des projets de l'orga
      const poleProjects2 = (parentId) => {
        const queryProjectId = `parent.${parentId}`;
        const query = {};
        const options = {
          fields: {
            name: 1, slug: 1, [queryProjectId]: 1, _id: 1,
          },
        };
        query[queryProjectId] = { $exists: 1 };
        return Projects.find(query, options);
      };

      const projectActions = (parentId, user) => {
        const query = {};
        query.parentId = parentId;
        query.status = 'todo';
        query[`links.contributors.${user._id.valueOf()}`] = { $exists: true };
        const options = {
          fields: {
            name: 1, _id: 1, status: 1, parentId: 1, [`links.contributors.${user._id.valueOf()}`]: 1, tasks: 1,
          },
        };
        return Actions.find(query, options);
      };

      if (req.body.userId) {
        if (req.body.parentType === 'organizations') {
          const citoyensDash = Citoyens.find({ _id: { $in: [new Mongo.ObjectID(req.body.userId)] } }, { fields: { username: 1, name: 1, _id: 1 } });
          if (citoyensDash) {
            const retourUsers = [];
            // allUsers
            citoyensDash.forEach((citoyen) => {
              // citoyen.name citoyen.username
              const projects = [];
              poleProjects2(req.body.parentId).forEach((project) => {
                const projectActionsArray = projectActions(project._id.valueOf(), citoyen);
                const countActionsProject = projectActionsArray.count();
                if (countActionsProject > 0) {
                  projects.push({
                    _id: project._id.str, name: project.name, slug: project.slug, countActions: countActionsProject, actions: projectActionsArray.fetch(),
                  });
                }
              });

              if (projects.length > 0) {
                const citoyenRetour = {};
                citoyenRetour._id = citoyen._id.valueOf();
                citoyenRetour.name = citoyen.name;
                citoyenRetour.username = citoyen.username;
                citoyenRetour.projects = projects;
                retourUsers.push(citoyenRetour);
              }
            });

            const valid = {
              status: true,
              dashboard: retourUsers,
              msg: 'action orga user dashboardUser',
            };
            res.status(200).json(valid);
          }
        } else if (req.body.parentType === 'projects') {
          const query = {};
          query._id = new Mongo.ObjectID(req.body.parentId);
          const project = Projects.findOne(query);

          const orgId = orgaIdScope({ scope: req.body.parentType, scopeId: req.body.parentId });

          if (orgId && orgId.length > 0) {
            const citoyensDash = Citoyens.find({ _id: { $in: [new Mongo.ObjectID(req.body.userId)] } }, { fields: { username: 1, name: 1, _id: 1 } });
            if (citoyensDash) {
              const retourUsers = [];
              // allUsers
              citoyensDash.forEach((citoyen) => {
                // citoyen.name citoyen.username
                const projects = [];

                // poleProjects2(req.body.parentId).forEach((project) => {
                const projectActionsArray = projectActions(project._id.valueOf(), citoyen);
                const countActionsProject = projectActionsArray.count();
                if (countActionsProject > 0) {
                  projects.push({
                    _id: project._id.str, name: project.name, slug: project.slug, countActions: countActionsProject, actions: projectActionsArray.fetch(),
                  });
                }
                // });

                if (projects.length > 0) {
                  const citoyenRetour = {};
                  citoyenRetour._id = citoyen._id.valueOf();
                  citoyenRetour.name = citoyen.name;
                  citoyenRetour.username = citoyen.username;
                  citoyenRetour.projects = projects;
                  retourUsers.push(citoyenRetour);
                }
              });

              const valid = {
                status: true,
                dashboard: retourUsers,
                msg: 'action project user dashboardUser',
              };
              res.status(200).json(valid);
            }
          }
        }
      } else {
        // eslint-disable-next-line no-lonely-if
        if (req.body.parentType === 'organizations') {
          const query = {};
          query._id = new Mongo.ObjectID(req.body.parentId);
          const scope = Organizations.findOne(query);

          if (scope && scope.oceco && scope.oceco.dashboardUser && scope.oceco.dashboardUser.length > 0) {
            const arrayIds = scope.oceco.dashboardUser.map((k) => new Mongo.ObjectID(k));
            const citoyensDash = Citoyens.find({ _id: { $in: arrayIds } }, { fields: { username: 1, name: 1, _id: 1 } });
            if (citoyensDash) {
              const retourUsers = [];
              // allUsers
              citoyensDash.forEach((citoyen) => {
                // citoyen.name citoyen.username
                const projects = [];
                poleProjects2(req.body.parentId).forEach((project) => {
                  const projectActionsArray = projectActions(project._id.valueOf(), citoyen);
                  const countActionsProject = projectActionsArray.count();
                  if (countActionsProject > 0) {
                    projects.push({
                      _id: project._id.str, name: project.name, slug: project.slug, countActions: countActionsProject, actions: projectActionsArray.fetch(),
                    });
                  }
                });

                if (projects.length > 0) {
                  const citoyenRetour = {};
                  citoyenRetour._id = citoyen._id.valueOf();
                  citoyenRetour.name = citoyen.name;
                  citoyenRetour.username = citoyen.username;
                  citoyenRetour.projects = projects;
                  retourUsers.push(citoyenRetour);
                }
              });

              const valid = {
                status: true,
                dashboard: retourUsers,
                msg: 'action orga dashboardUser',
              };
              res.status(200).json(valid);
            }
          } else {
            const valid = {
              status: false,
              msg: 'no dashboardUser config',
            };
            res.status(200).json(valid);
          }
        } else if (req.body.parentType === 'projects') {
          const query = {};
          query._id = new Mongo.ObjectID(req.body.parentId);
          const project = Projects.findOne(query);

          const orgId = orgaIdScope({ scope: req.body.parentType, scopeId: req.body.parentId });

          if (orgId && orgId.length > 0) {
            const scope = Organizations.findOne({ _id: new Mongo.ObjectID(orgId[0]) });
            if (scope && scope.oceco && scope.oceco.dashboardUser && scope.oceco.dashboardUser.length > 0) {
              const arrayIds = scope.oceco.dashboardUser.map((k) => new Mongo.ObjectID(k));
              const citoyensDash = Citoyens.find({ _id: { $in: arrayIds } }, { fields: { username: 1, name: 1, _id: 1 } });
              if (citoyensDash) {
                const retourUsers = [];
                // allUsers
                citoyensDash.forEach((citoyen) => {
                  // citoyen.name citoyen.username
                  const projects = [];

                  // poleProjects2(req.body.parentId).forEach((project) => {
                  const projectActionsArray = projectActions(project._id.valueOf(), citoyen);
                  const countActionsProject = projectActionsArray.count();
                  if (countActionsProject > 0) {
                    projects.push({
                      _id: project._id.str, name: project.name, slug: project.slug, countActions: countActionsProject, actions: projectActionsArray.fetch(),
                    });
                  }
                  // });

                  if (projects.length > 0) {
                    const citoyenRetour = {};
                    citoyenRetour._id = citoyen._id.valueOf();
                    citoyenRetour.name = citoyen.name;
                    citoyenRetour.username = citoyen.username;
                    citoyenRetour.projects = projects;
                    retourUsers.push(citoyenRetour);
                  }
                });

                const valid = {
                  status: true,
                  dashboard: retourUsers,
                  msg: 'action project dashboardUser',
                };
                res.status(200).json(valid);
              }
            } else {
              const valid = {
                status: false,
                msg: 'no dashboardUser config',
              };
              res.status(200).json(valid);
            }
          }
        }
      }
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/generatetokenchat', verifyToken, function (req, res) {
  const userId = req.headers['x-user-id'];
  // console.log(userId);
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        username: { type: String },
        tokenName: { type: String },
      }).validate(req.body);

      const citoyenOne = Citoyens.findOne({ username: req.body.username });
      let valid;
      // console.log(citoyenOne._id.valueOf(), req.body.tokenName);
      if (citoyenOne) {
        const token = generateToken(citoyenOne._id.valueOf(), req.body.tokenName);
        // console.log(token);
        if (token) {
          valid = {
            _id: citoyenOne._id.valueOf(),
            username: citoyenOne.username,
            name: citoyenOne.name,
            email: citoyenOne.email,
            token,
            status: true,
            msg: `generate token chat: ${req.body.username}`,
          };
        } else {
          valid = {
            status: false,
            msg: `not token: ${req.body.username}`,
          };
        }
      } else {
        valid = {
          status: false,
          msg: `not user : ${req.body.username}`,
        };
      }
      // console.log(valid);
      res.status(200).json(valid);
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

app.post('/api/generatetokenrest(/:ical)?', function (req, res) {
  // const userId = req.headers['x-user-id'];
  // console.log(userId);

  /* #swagger.path = '/api/generatetokenrest'
  #swagger.produces = ['application/json']
  #swagger.consumes = ['application/json']
  #swagger.parameters['pwd'] = {
    in: 'body',
    required: true,
    schema: {
      email: 'jhon.doe@gmail.com',
      pwd: '123456'
    }
  } */

  try {
    new SimpleSchema({
      email: { type: String },
      pwd: { type: String },
    }).validate(req.body);
    const loggin = login(req.body);
    // console.log(valid);
    if (loggin && loggin.userId) {
      runAsUser(login.userId, function () {
        const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(loggin.userId) });
        let valid;

        if (citoyenOne) {
          const tokenName = req.params && req.params.ical ? 'restIcal' : 'restOceco';
          const token = generateRestToken(citoyenOne._id.valueOf(), tokenName);
          // console.log(token);
          if (token) {
            valid = {
              _id: citoyenOne._id.valueOf(),
              username: citoyenOne.username,
              name: citoyenOne.name,
              email: citoyenOne.email,
              token,
              status: true,
              msg: `generate token rest oceco: ${req.body.email}`,
            };
            // #swagger.responses[200] = { description: 'generate token rest oceco' }
            if (req.params && req.params.ical) {
              valid.ical = Meteor.absoluteUrl(`ical/citoyens/${citoyenOne._id.valueOf()}/${token}/actions`);
              valid.msg = `generate token rest ical: ${req.body.email}`;
            }
          } else {
            valid = {
              status: false,
              msg: 'not token',
            };
            // #swagger.responses[200] = { description: 'not token' }
          }
        } else {
          valid = {
            status: false,
            msg: 'not user',
          };
          // #swagger.responses[200] = { description: 'not user' }
        }
        // console.log(valid);
        res.status(200).json(valid);
      });
    }
  } catch (e) {
    // console.log(e);
    handleErrorAsJson(e, req, res);
    // #swagger.responses[400] = { description: 'error' }
    // #swagger.responses[500] = { description: 'Internal Server Error' }
  }
});

app.get('/api/generatetoken/:tokenName', verifyToken, function (req, res) {

  const userId = req.headers['x-user-id'];

  try {
    new SimpleSchema({
      tokenName: { type: String },
    }).validate(req.params);

    runAsUser(userId, function () {
      const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId) });
      let valid;

      if (citoyenOne) {
        const tokenName = req.params.tokenName;
        const token = generateRestToken(citoyenOne._id.valueOf(), tokenName);
        // console.log(token);
        const creationTimestamp = new Date().getTime();
        const tokenData = {
          userId: citoyenOne._id.valueOf(),
          tokenName: tokenName,
          tokentype: 'restAccessToken',
          token: token,
          createdAt: creationTimestamp
        };
        const encryptedToken = generateEncryptedToken(tokenData);
        // console.log(encryptedToken);
        // const tokenDataDecrypt = decryptToken(encryptedToken);
        // console.log(tokenDataDecrypt);
        if (token) {

          valid = {
            _id: citoyenOne._id.valueOf(),
            username: citoyenOne.username,
            name: citoyenOne.name,
            email: citoyenOne.email,
            tokenName: tokenName,
            token,
            bearer: encryptedToken,
            status: true,
            msg: `generate token rest oceco`,
          };
        } else {
          valid = {
            status: false,
            msg: 'not token',
          };
        }
      } else {
        valid = {
          status: false,
          msg: 'not user',
        };
      }
      // console.log(valid);
      res.status(200).json(valid);
    });

  } catch (e) {
    // console.log(e);
    handleErrorAsJson(e, req, res);
  }
});

app.post('/api/batchjson/create', verifyToken, async (req, res) => {
  const userId = req.headers['x-user-id'];

  const imageProcessData = async (url, scope, id, actionid = null) => {
    const imageUrlData = await fetch(url);
    const buffer = await imageUrlData.buffer();
    const contentType = await imageUrlData.headers.get('content-type');
    const imageBas64 = `data:image/${contentType};base64,${buffer.toString('base64')}`;
    const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
    if (actionid) {
      return Meteor.call('photoActions', imageBas64, str, scope, id, actionid);
    } else {
      return Meteor.call('photoScope', scope, imageBas64, str, id);
    }
  };

  const SchemaActionsB = new SimpleSchema({
    name: { type: String },
    description: { type: String },
    startDate: {
      type: String,
      optional: true,
      custom() {
        /* if (this.field('endDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
          return 'required';
        } */
        if (this.value) {
          const isValid = moment(this.value, 'YYYY-MM-DDTHH:mm:ssZ').isValid();
          // const isValid = aDate.isValid();
          if (!isValid) {
            return 'invalidDate';
          }
        }
        /* const startDate = moment(this.value).toDate();
         console.log('maxDateStart', this.field('endDate').value);
         const endDate = moment(this.field('endDate').value).toDate();
         if (!this.field('options.possibleStartActionBeforeStartDate').value) {
           if (moment(endDate).isBefore(startDate)) {
             return 'maxDateStart';
           }
         } */
      },
    },
    endDate: {
      type: String,
      optional: true,
      custom() {
        // eslint-disable-next-line no-empty
        /* if (this.field('startDate').value && this.field('options.possibleStartActionBeforeStartDate').value) {

        } else if (this.field('startDate').value && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
          return 'required';
        } */
        if (this.value) {
          const isValid = moment(this.value, 'YYYY-MM-DDTHH:mm:ssZ').isValid();
          // const isValid = aDate.isValid();
          if (!isValid) {
            return 'invalidDate';
          }
          /* const startDate = moment(this.field('startDate').value).toDate();
          const endDate = moment(this.value).toDate();
          if (moment(endDate).isBefore(startDate)) {
            return 'minDateEnd';
          } */
        }
      },
    },
    credits: {
      type: Number,
      // defaultValue: 1,
      autoValue() {
        if (this.isSet) {
          return this.value;
        }
        if (this.isModifier && !this.value) {
          return;
        }
        return 1;
      },
      optional: true,
    },
    max: {
      type: Number,
      optional: true,
    },
    min: {
      type: Number,
      optional: true,
    },
    imageUrl: {
      type: String,
      optional: true,
    },
  }, {
    clean: {
      filter: true,
      autoConvert: true,
      removeEmptyStrings: true,
      trimStrings: true,
      getAutoValues: true,
      removeNullsFromArrays: true,
    },
  });

  const SchemaEventsB = new SimpleSchema(baseSchema, {
    clean: {
      filter: true,
      autoConvert: true,
      removeEmptyStrings: true,
      trimStrings: true,
      getAutoValues: true,
      removeNullsFromArrays: true,
    },
  });
  SchemaEventsB.extend(geoSchema);
  SchemaEventsB.extend({
    type: {
      type: String,
    },
    allDay: {
      type: Boolean,
      defaultValue: false,
      optional: true,
    },
    startDate: {
      type: String,
      custom() {
        if (!this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
          return 'required';
        }
        if (this.value) {
          const isValid = moment(this.value, 'YYYY-MM-DDTHH:mm:ssZ').isValid();
          // const isValid = aDate.isValid();
          if (!isValid) {
            return 'invalidDate';
          }
        }
      },
    },
    endDate: {
      type: String,
      custom() {
        if (!this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
          return 'required';
        }
        if (this.value) {
          const isValid = moment(this.value, 'YYYY-MM-DDTHH:mm:ssZ').isValid();
          // const isValid = aDate.isValid();
          if (!isValid) {
            return 'invalidDate';
          }
        }
      },
    },
    email: {
      type: String,
      optional: true,
    },
    fixe: {
      type: String,
      optional: true,
    },
    mobile: {
      type: String,
      optional: true,
    },
    fax: {
      type: String,
      optional: true,
    },
    imageUrl: {
      type: String,
      optional: true,
    },
    actions: { type: Array, optional: true, minCount: 1 },
    'actions.$': { type: SchemaActionsB },
    /* organizerId: {
      type: String,
    },
    organizerType: {
      type: String,
    },
    parentId: {
      type: String,
      optional: true,
    }, */
  });

  const SchemasBatchRest = new SimpleSchema({
    organizations: { type: Object },
    'organizations._id': { type: String },
    'organizations.actions': { type: Array, optional: true },
    'organizations.actions.$': { type: SchemaActionsB },
    'organizations.projects': { type: Array, optional: true },
    'organizations.projects.$': { type: Object },
    'organizations.projects.$._id': { type: String },
    'organizations.projects.$.actions': { type: Array, optional: true },
    'organizations.projects.$.actions.$': { type: SchemaActionsB },
    'organizations.projects.$.events': { type: Array, optional: true },
    'organizations.projects.$.events.$': { type: SchemaEventsB },
  }, {
    clean: {
      filter: true,
      autoConvert: true,
      removeEmptyStrings: true,
      trimStrings: true,
      getAutoValues: true,
      removeNullsFromArrays: true,
    },
  });

  try {
    SchemasBatchRest.validate(req.body);

    runAsUser(userId, function () {
      const batchPost = SchemasBatchRest.clean(req.body);

      // organizations
      const { organizations } = batchPost;
      const { _id: organizationId } = organizations;
      const synchroRetour = synchroAdmin({ parentId: organizationId, parentType: 'organizations' });
      // actions
      if (organizations.actions && organizations.actions.length > 0) {
        const { actions: actionsOrgaArray } = organizations;
        actionsOrgaArray.forEach((action) => {
          action.parentId = organizationId;
          action.parentType = 'organizations';
          if (action.startDate) {
            const aDate = moment(action.startDate, 'YYYY-MM-DDTHH:mm:ssZ');
            action.startDate = new Date(aDate.toDate());
          }
          if (action.endDate) {
            const aDate = moment(action.endDate, 'YYYY-MM-DDTHH:mm:ssZ');
            action.endDate = new Date(aDate.toDate());
          }
          const actionImageUrl = action.imageUrl;
          delete action.imageUrl;
          // methode insert action
          const retourAction = Meteor.call('insertAction', action);
          if (retourAction && retourAction.data && retourAction.data.id) {
            if (actionImageUrl) {
              // url, scope, id, actionid
              const imageBas64 = imageProcessData(actionImageUrl, 'organizations', organizationId, retourAction.data.id);
            }
          }
        });
      }
      // projects
      if (organizations.projects && organizations.projects.length > 0) {
        const { projects: projectsOrgaArray } = organizations;
        projectsOrgaArray.forEach((project) => {
          const { _id: projectId } = project;
          const synchroRetour = synchroAdmin({ parentId: projectId, parentType: 'projects' });
          // events
          if (project.events && project.events.length > 0) {
            const { events: eventsProjectArray } = project;
            eventsProjectArray.forEach((event) => {
              event.organizerId = projectId;
              event.organizerType = 'projects';
              if (event.startDate) {
                const aDate = moment(event.startDate, 'YYYY-MM-DDTHH:mm:ssZ');
                event.startDate = new Date(aDate.toDate());
              }
              if (event.endDate) {
                const aDate = moment(event.endDate, 'YYYY-MM-DDTHH:mm:ssZ');
                event.endDate = new Date(aDate.toDate());
              }

              // methode insert event
              const retourEvent = Meteor.call('insertEvent', event);
              if (retourEvent && retourEvent.data && retourEvent.data.id) {
                if (event.imageUrl) {
                  const imageBas64 = imageProcessData(event.imageUrl, 'events', retourEvent.data.id);
                }
              }
              // actions
              if (event.actions && event.actions.length > 0) {
                const { actions: actionsEventArray } = event;
                actionsEventArray.forEach((action) => {
                  action.parentId = retourEvent.data.id;
                  action.parentType = 'events';
                  if (action.startDate) {
                    const aDate = moment(action.startDate, 'YYYY-MM-DDTHH:mm:ssZ');
                    action.startDate = new Date(aDate.toDate());
                  }
                  if (action.endDate) {
                    const aDate = moment(action.endDate, 'YYYY-MM-DDTHH:mm:ssZ');
                    action.endDate = new Date(aDate.toDate());
                  }
                  const actionImageUrl = action.imageUrl;
                  delete action.imageUrl;
                  // methode insert action
                  const retourAction = Meteor.call('insertAction', action);
                  if (retourAction && retourAction.data && retourAction.data.id) {
                    if (actionImageUrl) {
                      // url, scope, id, actionid
                      const imageBas64 = imageProcessData(actionImageUrl, 'events', retourEvent.data.id, retourAction.data.id);
                    }
                  }
                });
              }
            });
          }
          // actions
          if (project.actions && project.actions.length > 0) {
            const { actions: actionsProjectArray } = project;
            actionsProjectArray.forEach((action) => {
              action.parentId = projectId;
              action.parentType = 'projects';
              if (action.startDate) {
                const aDate = moment(action.startDate, 'YYYY-MM-DDTHH:mm:ssZ');
                action.startDate = new Date(aDate.toDate());
              }
              if (action.endDate) {
                const aDate = moment(action.endDate, 'YYYY-MM-DDTHH:mm:ssZ');
                action.endDate = new Date(aDate.toDate());
              }
              const actionImageUrl = action.imageUrl;
              delete action.imageUrl;
              // methode insert action
              const retourAction = Meteor.call('insertAction', action);
              if (retourAction && retourAction.data && retourAction.data.id) {
                if (actionImageUrl) {
                  const imageBas64 = imageProcessData(actionImageUrl, 'projects', projectId, retourAction.data.id);
                }
              }
            });
          }
        });
      }

      const valid = {
        status: true,
        msg: 'batch created',
        json: batchPost,
      };

      res.status(200).json(valid);
    });
  } catch (e) {
    // console.log(e);
    handleErrorAsJson(e, req, res);
  }
});

app.post('/api/hooks/gitlab', verifyTokenGitlab, function (req, res) {
  const token = req.headers['x-gitlab-token'];
  const projectOne = Projects.findOne({ 'oceco.git.webhookSecret': token, 'oceco.git.active': true }, { fields: { _id: 1, 'oceco.git.webhookSecret': 1, 'oceco.git.user.userId': 1 } });
  if (projectOne) {
    // console.log('projectOne', projectOne.oceco.git.webhookSecret);
    const event = req.headers['x-gitlab-event'];
    // console.log('event', event);
    if (event === 'Issue Hook' || event === 'Confidential Issue Hook') {
      if (req.body && req.body.object_attributes) {
        // console.log('object_attributes', req.body.object_attributes);
        const { iid, state, action, updated_at, project_id, title, labels } = req.body.object_attributes;

        const actionOne = Actions.findOne({ parentId: projectOne._id.valueOf(), parentType: 'projects', 'git.iid': iid });

        // comment savoir si pas nouveau alors que la mise à jour de l'update de l'api n'est encore faite
        if (!actionOne && action === 'open') {
          if (labels.length > 0) {
            const tagOceco = labels.filter((tag) => tag.title === 'oceco');
            if (tagOceco.length > 0) {
              const current = moment(updated_at);
              const actionOneTest = Actions.findOne({ parentId: projectOne._id.valueOf(), parentType: 'projects', name: title, status: 'todo' }, { sort: { created: -1 } });
              if (actionOneTest) {
                const inputUnix = moment.unix(actionOneTest.created);
                const durationTest = moment.duration(inputUnix.diff(current)).seconds();
                if (durationTest < 4) {
                  const valid = {
                    ok: true,
                  };
                  res.status(200).json(valid);
                  return;
                }
              }
            }
          }
        }

        if (actionOne && actionOne.creator) {
          // l'action existe

          // l'action existe est à updatedAt et on compare avec updated_at
          if (actionOne.git && actionOne.git.updatedAt) {
            const one = moment(actionOne.git.updatedAt);
            const current = moment(updated_at);
            // console.log('update diff', moment(one).toDate(), moment(current).toDate());
            if (moment(one).format() === moment(current).format()) {
              const valid = {
                ok: true,
              };
              res.status(200).json(valid);
              return;
            }
          }

          // console.log('creator', actionOne.creator);
          // update
          if (action === 'close' && actionOne.status !== 'done') {
            runAsUser(actionOne.creator, function () {
              Actions.update({ _id: actionOne._id }, { $set: { 'git.state': state, 'git.updatedAt': updated_at } });
              Meteor.call('actionsType', {
                parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: actionOne._id.valueOf(), name: 'status', value: 'done',
              });
            });
          } else if (action === 'reopen' && actionOne.status !== 'todo') {
            runAsUser(actionOne.creator, function () {
              Actions.update({ _id: actionOne._id }, { $set: { 'git.state': state, 'git.updatedAt': updated_at } });
              Meteor.call('actionsType', {
                parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: actionOne._id.valueOf(), name: 'status', value: 'todo',
              });
            });
          } else if (action === 'update') {
            const { title, description, labels } = req.body.object_attributes;
            const updateAction = { name: title, parentId: projectOne._id.valueOf(), parentType: 'projects' };
            if (description) {
              updateAction.description = description;
            }
            updateAction.tagsText = labels && labels.length > 0 ? `#gitlab ${labels.map((tag) => `#${tag.title}`).join(' ')}` : '#gitlab';
            // console.log('updateAction', updateAction);
            runAsUser(actionOne.creator, function () {
              try {
                const actionPost = SchemasActionsRest.clean(updateAction);
                const modifier = { $set: actionPost };
                const retourCall = Meteor.call('updateAction', { modifier, _id: actionOne._id.valueOf(), noNotif: true });
                // console.log(retourCall);
                if (retourCall) {
                  Actions.update({ _id: new Mongo.ObjectID(actionOne._id.valueOf()) }, { $set: { 'git.state': state, 'git.updatedAt': updated_at } });
                }
              } catch (e) {
                // console.log(e);
                // handleErrorAsJson(e, req, res);
              }
            });
          }
        } else {
          // create action
          if (action === 'open') {
            // probléme pour créer une action creator
            const { title, description, labels, web_url } = req.body.object_attributes;
            const insertAction = { name: title, parentId: projectOne._id.valueOf(), parentType: 'projects' };
            if (description) {
              insertAction.description = description;
            }
            insertAction.tagsText = labels && labels.length > 0 ? `#gitlab ${labels.map((tag) => `#${tag.title}`).join(' ')}` : '#gitlab';
            // console.log('insertAction', insertAction);
            // peut être l'admin qui la configuré
            // test avce mon user deja
            runAsUser(projectOne.oceco.git.user.userId, function () {
              try {
                const actionPost = SchemasActionsRest.clean(insertAction);
                const retourCall = Meteor.call('insertAction', actionPost);
                // console.log(retourCall);
                if (retourCall) {
                  Actions.update({ _id: new Mongo.ObjectID(retourCall.data.id) }, { $set: { 'git.iid': iid, 'git.url': web_url, 'git.state': state, 'git.updatedAt': updated_at } });
                }
              } catch (e) {
                // console.log(e);
                // handleErrorAsJson(e, req, res);
              }
            });
          }
        }
      }
    }
  }
  const valid = {
    ok: true,
  };
  res.status(200).json(valid);
  return;
});

app.post('/api/hooks/github/:projectid', verifyTokenGithub, function (req, res) {
  new SimpleSchema({
    projectid: { type: String },
  }).validate(req.params);

  req.body = JSON.parse(req.body.toString());
  const projectOne = Projects.findOne({ _id: new Mongo.ObjectID(req.params.projectid), 'oceco.git.active': true }, { fields: { _id: 1, 'oceco.git.webhookSecret': 1, 'oceco.git.user.userId': 1 } });
  if (projectOne) {
    // console.log('projectOne', projectOne.oceco.git.webhookSecret);
    const event = req.headers['x-github-event'];
    // console.log('event', event);

    if (event === 'issues') {
      if (req.body && req.body.issue) {
        // console.log('object_attributes', req.body.issue);
        const { issue, action } = req.body;
        const { number, state, updated_at, title, labels } = issue;
        /*
        opened, edited, deleted, closed, reopened,
        pinned, unpinned, assigned, unassigned, labeled, unlabeled, locked, unlocked, transferred, milestoned, or demilestoned
        */
        const actionOne = Actions.findOne({ parentId: projectOne._id.valueOf(), parentType: 'projects', 'git.number': number });

        // comment savoir si pas nouveau alors que la mise à jour de l'update de l'api n'est encore faite
        if (!actionOne && action === 'opened') {
          if (labels.length > 0) {
            const tagOceco = labels.filter((tag) => tag.title === 'oceco');
            if (tagOceco.length > 0) {
              const current = moment(updated_at);
              const actionOneTest = Actions.findOne({ parentId: projectOne._id.valueOf(), parentType: 'projects', name: title, status: 'todo' }, { sort: { created: -1 } });
              if (actionOneTest) {
                const inputUnix = moment.unix(actionOneTest.created);
                const durationTest = moment.duration(inputUnix.diff(current)).seconds();
                if (durationTest < 4) {
                  const valid = {
                    ok: true,
                  };
                  res.status(200).json(valid);
                  return;
                }
              }
            }
          }
        }

        if (actionOne && actionOne.creator) {
          // l'action existe

          // l'action existe est à updatedAt et on compare avec updated_at
          if (actionOne.git && actionOne.git.updatedAt) {
            const one = moment(actionOne.git.updatedAt);
            const current = moment(updated_at);
            // console.log('update diff', moment(one).toDate(), moment(current).toDate());
            if (moment(one).format() === moment(current).format()) {
              const valid = {
                ok: true,
              };
              res.status(200).json(valid);
              return;
            }
          }

          // console.log('creator', actionOne.creator);
          // update
          if (action === 'closed' && actionOne.status !== 'done') {
            runAsUser(actionOne.creator, function () {
              Actions.update({ _id: actionOne._id }, { $set: { 'git.state': state, 'git.updatedAt': updated_at } });
              Meteor.call('actionsType', {
                parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: actionOne._id.valueOf(), name: 'status', value: 'done',
              });
            });
          } else if (action === 'reopened' && actionOne.status !== 'todo') {
            runAsUser(actionOne.creator, function () {
              Actions.update({ _id: actionOne._id }, { $set: { 'git.state': state, 'git.updatedAt': updated_at } });
              Meteor.call('actionsType', {
                parentType: actionOne.parentType, parentId: actionOne.parentId, type: 'actions', id: actionOne._id.valueOf(), name: 'status', value: 'todo',
              });
            });
          } else if (action === 'edited') {
            const { title, body, labels } = issue;
            const updateAction = { name: title, parentId: projectOne._id.valueOf(), parentType: 'projects' };
            if (body) {
              updateAction.description = body;
            }
            updateAction.tagsText = labels && labels.length > 0 ? `#gitlab ${labels.map((tag) => `#${tag.title}`).join(' ')}` : '#gitlab';
            // console.log('updateAction', updateAction);
            runAsUser(actionOne.creator, function () {
              try {
                const actionPost = SchemasActionsRest.clean(updateAction);
                const modifier = { $set: actionPost };
                const retourCall = Meteor.call('updateAction', { modifier, _id: actionOne._id.valueOf(), noNotif: true });
                // console.log(retourCall);
                if (retourCall) {
                  Actions.update({ _id: new Mongo.ObjectID(actionOne._id.valueOf()) }, { $set: { 'git.state': state, 'git.updatedAt': updated_at } });
                }
              } catch (e) {
                // console.log(e);
                // handleErrorAsJson(e, req, res);
              }
            });
          }
        } else {
          // create action
          if (action === 'opened') {
            // probléme pour créer une action creator
            const { title, body, labels, html_url } = issue;
            const insertAction = { name: title, parentId: projectOne._id.valueOf(), parentType: 'projects' };
            if (body) {
              insertAction.description = body;
            }
            insertAction.tagsText = labels && labels.length > 0 ? `#gitlab ${labels.map((tag) => `#${tag.title}`).join(' ')}` : '#gitlab';
            // console.log('insertAction', insertAction);
            // peut être l'admin qui la configuré
            // test avce mon user deja
            runAsUser(projectOne.oceco.git.user.userId, function () {
              try {
                const actionPost = SchemasActionsRest.clean(insertAction);
                const retourCall = Meteor.call('insertAction', actionPost);
                // console.log(retourCall);
                if (retourCall) {
                  Actions.update({ _id: new Mongo.ObjectID(retourCall.data.id) }, { $set: { 'git.number': number, 'git.url': html_url, 'git.state': state, 'git.updatedAt': updated_at } });
                }
              } catch (e) {
                // console.log(e);
                // handleErrorAsJson(e, req, res);
              }
            });
          }
        }
      }
    }
  }
  const valid = {
    ok: true,
  };
  res.status(200).json(valid);
  return;
});

app.get('/api/organizations/:id/events', verifyToken, async (req, res) => {
  new SimpleSchema({
    id: { type: String },
  }).validate(req.params);

  if (!Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id), oceco: { $exists: true } })) {
    const valid = {
      status: false,
      msg: 'not organizations',
    };
    res.status(200).json(valid);
    return;
  }

  const eventParse = [];
  // events
  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id) });

  // verifier si private
  if (orgaOne.links && orgaOne.links.projects && (preferences.private === false || preferences.private === 'false' || !preferences.private)) {
    const projectLinkIds = arrayLinkParent(orgaOne.links.projects, 'projects');
    const projectsList = Projects.find({ _id: { $in: projectLinkIds }, $or: [{ 'preferences.private': false }, { 'preferences.private': { $exists: false } }] });
    const projectIds = projectsList.map((project) => project._id.valueOf());

    const query = {};
    query.$or = [];
    projectIds.forEach((id) => {
      const queryCo = {};
      queryCo[`organizer.${id}`] = { $exists: true };
      query.$or.push(queryCo);
    });
    // const inputDate = new Date();
    // query.startDate = { $lte: inputDate };
    const inputDate = moment(new Date()).subtract(60, 'day').toDate();
    query.endDate = { $gte: new Date(inputDate) };
    const options = {};
    options.sort = {
      startDate: 1,
    };

    // console.log(query);
    const events = Events.find(query, options);

    if (events) {
      events.forEach((event) => {
        const eventOne = {};
        // console.log(event.name);
        eventOne.id = event._id.valueOf();

        if (event.name) {
          eventOne.summary = event.name;
        }
        if (event.description) {
          eventOne.description = event.description;
        }
        if (event.startDate) {
          eventOne.start = event.startDate;
        }
        if (event.endDate) {
          eventOne.end = event.endDate;
        }
        if (event.timeZone) {
          eventOne.timezone = event.timeZone;
        }
        eventOne.url = `${Meteor.absoluteUrl()}organizations/${req.params.id}/events/actions/${event._id.valueOf()}`;
        if (event.startDate) {
          eventParse.push(eventOne);
        }
      });
    }
  }

  res.status(200).json(eventParse);
  return;
});

// renvois le wallet de l'organisation pour l'utilisateur qui fait la requete
app.get('/api/organizations/:id/meWallet', verifyToken, function (req, res) {

  const userId = req.headers['x-user-id'];

  try {
    new SimpleSchema({
      id: { type: String },
    }).validate(req.params);

    runAsUser(userId, function () {
      let valid = null;
      if (!Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id), oceco: { $exists: true } })) {
        valid = {
          status: false,
          msg: 'not organizations',
        };
        res.status(200).json(valid);
        return;
      }

      const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId) });

      if (citoyenOne) {

        valid = {
          status: true,
          userId: userId,
          oragnizationId: req.params.id,
          wallet: citoyenOne.userWallet && citoyenOne.userWallet[`${req.params.id}`] && citoyenOne.userWallet[`${req.params.id}`].userCredits ? citoyenOne.userWallet[`${req.params.id}`].userCredits : 0,
          msg: 'organization wallet user',
        };

      } else {
        valid = {
          status: false,
          msg: 'not user',
        };
      }
      // console.log(valid);
      res.status(200).json(valid);
    });

  } catch (e) {
    // console.log(e);
    handleErrorAsJson(e, req, res);
  }
});

app.get('/ical/organizations/:id/events', function (req, res) {
  new SimpleSchema({
    id: { type: String },
  }).validate(req.params);

  if (!Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id), oceco: { $exists: true } })) {
    const valid = {
      status: false,
      msg: 'not organizations',
    };
    res.status(200).json(valid);
    return;
  }

  const eventParse = [];
  // events
  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id) });

  // verifier si private
  if (orgaOne.links && orgaOne.links.projects && (preferences.private === false || preferences.private === 'false' || !preferences.private)) {
    const projectLinkIds = arrayLinkParent(orgaOne.links.projects, 'projects');
    const projectsList = Projects.find({ _id: { $in: projectLinkIds }, $or: [{ 'preferences.private': false }, { 'preferences.private': { $exists: false } }] });
    const projectIds = projectsList.map((project) => project._id.valueOf());

    const query = {};
    query.$or = [];
    projectIds.forEach((id) => {
      const queryCo = {};
      queryCo[`organizer.${id}`] = { $exists: true };
      query.$or.push(queryCo);
    });
    // const inputDate = new Date();
    // query.startDate = { $lte: inputDate };
    const inputDate = moment(new Date()).subtract(60, 'day').toDate();
    query.endDate = { $gte: new Date(inputDate) };

    // status different de deletePending
    query.status = { $nin: statusNoVisible };

    const options = {};
    options.sort = {
      startDate: 1,
    };

    // console.log(query);
    const events = Events.find(query, options);

    if (events) {
      events.forEach((event) => {
        const eventOne = {};
        // console.log(event.name);
        eventOne.id = event._id.valueOf();

        if (event.name) {
          eventOne.summary = event.name;
        }
        if (event.description) {
          eventOne.description = event.description;
        }
        if (event.startDate) {
          eventOne.start = event.startDate;
        }
        if (event.endDate) {
          eventOne.end = event.endDate;
        }
        if (event.timeZone) {
          eventOne.timezone = event.timeZone;
        }
        // eventOne.url = `${Meteor.absoluteUrl()}organizations/${req.params.id}/events/actions/${event._id.valueOf()}`;
        if (event.startDate) {
          eventParse.push(eventOne);
        }
      });
    }
  }

  const cal = ical({
    domain: 'oce.co.tools',
    prodId: '//oce.co.tools//ical-generator//FR',
    events: [...eventParse],
  });
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  cal.serve(res);
});

app.get('/ical/organizations/:id/events/archives', verifyTokenMeteor, function (req, res) {
  const userId = req.headers['x-user-id'];
  // console.log(req.body);
  /* Todo
  verifier si user deja connecter ou pas à l'application
  */
  runAsUser(userId, function () {
    try {
      new SimpleSchema({
        id: { type: String },
      }).validate(req.params);

      if (!Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id), oceco: { $exists: true } })) {
        const valid = {
          status: false,
          msg: 'not organizations',
        };
        res.status(200).json(valid);
        return;
      }

      if (!Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id), oceco: { $exists: true } }).isAdmin()) {
        const valid = {
          status: false,
          msg: 'not admin',
        };
        res.status(200).json(valid);
        return;
      }

      const eventParse = [];
      // events
      const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id) });

      // verifier si private
      if (orgaOne.links && orgaOne.links.projects && (preferences.private === false || preferences.private === 'false' || !preferences.private)) {
        const projectLinkIds = arrayLinkParent(orgaOne.links.projects, 'projects');
        const projectsList = Projects.find({ _id: { $in: projectLinkIds }, $or: [{ 'preferences.private': false }, { 'preferences.private': { $exists: false } }] });
        const projectIds = projectsList.map((project) => project._id.valueOf());

        const query = {};
        query.$or = [];
        projectIds.forEach((id) => {
          const queryCo = {};
          queryCo[`organizer.${id}`] = { $exists: true };
          query.$or.push(queryCo);
        });
        // const inputDate = new Date();
        // query.startDate = { $lte: inputDate };
        const inputDate = moment(new Date()).subtract(12, 'month').toDate();
        query.endDate = { $gte: inputDate };
        query.status = { $nin: statusNoVisible };
        const options = {};
        options.sort = {
          startDate: 1,
        };
        const events = Events.find(query, options);
        if (events) {
          events.forEach((event) => {
            const eventOne = {};
            // console.log(event);
            eventOne.id = event._id.valueOf();

            if (event.name) {
              eventOne.summary = event.name;
            }
            if (event.description) {
              eventOne.description = event.description;
            }
            if (event.startDate) {
              eventOne.start = event.startDate;
            }
            if (event.endDate) {
              eventOne.end = event.endDate;
            }
            if (event.timeZone) {
              eventOne.timezone = event.timeZone;
            }
            // eventOne.url = `${Meteor.absoluteUrl()}organizations/${req.params.id}/events/actions/${event._id.valueOf()}`;
            if (event.startDate) {
              eventParse.push(eventOne);
            }
          });
        }
      }

      const cal = ical({
        domain: 'oce.co.tools',
        prodId: '//oce.co.tools//ical-generator//FR',
        events: [...eventParse],
      });
      res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
      res.header('Expires', '-1');
      res.header('Pragma', 'no-cache');
      res.send(cal.toString());
    } catch (e) {
      // console.log(e);
      handleErrorAsJson(e, req, res);
    }
  });
  //
});

// export ical des actions de organizations
/* app.get('/ical/organizations/:id/actions', function (req, res) {
  new SimpleSchema({
    id: { type: String },
  }).validate(req.params);

  if (!Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id), oceco: { $exists: true } })) {
    const valid = {
      status: false,
      msg: 'not organizations',
    };
    res.status(200).json(valid);
    return;
  }

  const actionParse = [];
  // actions
  const inputDateStart = moment(new Date()).subtract(60, 'day').toDate();
  const inputDateEnd = moment(new Date()).add(180, 'day').toDate();
  const actions = Organizations.findOne({ _id: new Mongo.ObjectID(req.params.id) }).actionsAllAgenda(inputDateStart, inputDateEnd);

  if (actions) {
    actions.forEach((action) => {
      const actionOne = {};
      // console.log(action.name);
      actionOne.id = action._id.valueOf();

      if (action.name) {
        actionOne.summary = action.name;
      }
      if (action.description) {
        actionOne.description = action.description;
      }
      if (action.startDate) {
        actionOne.start = action.startDate;
      }
      if (action.endDate) {
        actionOne.end = action.endDate;
      }
      // eventOne.url = `${Meteor.absoluteUrl()}organizations/${req.params.id}/events/actions/${action._id.valueOf()}`;
      if (action.startDate) {
        actionParse.push(actionOne);
      }
    });
  }

  const cal = ical({
    domain: 'oce.co.tools',
    prodId: '//oce.co.tools//ical-generator//FR',
    events: [...actionParse],
  });
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  cal.serve(res);
}); */

// export ical des actions citoyens et rajout des event ou on est en participant
app.get('/ical/citoyens/:id/:token/actions', function (req, res) {
  new SimpleSchema({
    id: { type: String },
    token: { type: String },
  }).validate(req.params);

  if (!verifyRestToken(req.params.id, 'restIcal', req.params.token)) {
    const valid = {
      status: false,
      msg: 'not valid token',
    };
    res.status(200).json(valid);
    return;
  }

  if (!Citoyens.findOne({ _id: new Mongo.ObjectID(req.params.id) })) {
    const valid = {
      status: false,
      msg: 'not user',
    };
    res.status(200).json(valid);
    return;
  }

  const actionParse = [];
  // actions
  const inputDateStart = moment(new Date()).subtract(60, 'day').toDate();
  const inputDateEnd = moment(new Date()).add(360, 'day').toDate();
  const actions = Citoyens.findOne({ _id: new Mongo.ObjectID(req.params.id) }).actionsAllAgenda(inputDateStart, inputDateEnd);

  if (actions) {
    actions.forEach((action) => {
      const actionOne = {};
      // console.log(action.name);
      actionOne.id = action._id.valueOf();

      if (action.name) {
        actionOne.summary = action.name;
      }
      if (action.description) {
        actionOne.description = action.description;
      }
      if (action.startDate) {
        actionOne.start = action.startDate;
      }
      if (action.endDate) {
        actionOne.end = action.endDate;
      }

      if (action.startDate) {
        actionParse.push(actionOne);
      }
    });
  }

  // events
  const events = Citoyens.findOne({ _id: new Mongo.ObjectID(req.params.id) }).eventsAllAgenda(inputDateStart, inputDateEnd);

  if (events) {
    events.forEach((event) => {
      const eventOne = {};
      // console.log(action.name);
      eventOne.id = event._id.valueOf();

      if (event.name) {
        eventOne.summary = event.name;
      }
      if (event.description) {
        eventOne.description = event.description;
      }
      if (event.startDate) {
        eventOne.start = event.startDate;
      }
      if (event.endDate) {
        eventOne.end = event.endDate;
      }

      if (event.startDate && event.endDate) {
        actionParse.push(eventOne);
      }
    });
  }

  const cal = ical({
    domain: 'oce.co.tools',
    prodId: '//oce.co.tools//ical-generator//FR',
    events: [...actionParse],
  });
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  cal.serve(res);
});

// export ical des events citoyens
app.get('/ical/citoyens/:id/:token/events', function (req, res) {
  new SimpleSchema({
    id: { type: String },
    token: { type: String },
  }).validate(req.params);

  if (!verifyRestToken(req.params.id, 'restIcal', req.params.token)) {
    const valid = {
      status: false,
      msg: 'not valid token',
    };
    res.status(200).json(valid);
    return;
  }

  if (!Citoyens.findOne({ _id: new Mongo.ObjectID(req.params.id) })) {
    const valid = {
      status: false,
      msg: 'not user',
    };
    res.status(200).json(valid);
    return;
  }

  const eventParse = [];
  // events
  const inputDateStart = moment(new Date()).subtract(60, 'day').toDate();
  const inputDateEnd = moment(new Date()).add(360, 'day').toDate();
  const events = Citoyens.findOne({ _id: new Mongo.ObjectID(req.params.id) }).eventsAllAgenda(inputDateStart, inputDateEnd);

  if (events) {
    events.forEach((event) => {
      const eventOne = {};
      // console.log(action.name);
      eventOne.id = event._id.valueOf();

      if (event.name) {
        eventOne.summary = event.name;
      }
      if (event.description) {
        eventOne.description = event.description;
      }
      if (event.startDate) {
        eventOne.start = event.startDate;
      }
      if (event.endDate) {
        eventOne.end = event.endDate;
      }

      if (event.startDate) {
        eventParse.push(eventOne);
      }
    });
  }

  const cal = ical({
    domain: 'oce.co.tools',
    prodId: '//oce.co.tools//ical-generator//FR',
    events: [...eventParse],
  });
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  cal.serve(res);
});

// {{urlImageCommunecter}}/upload/{{moduleId}}/{{folder}}/{{name}}
app.get('/download/doc/:moduleId/:folder/:name/file/:suite', verifyTokenMeteor, async (req, res) => {
  const userId = req.headers['x-user-id'];
  // console.log(req);
  new SimpleSchema({
    moduleId: { type: String },
    folder: { type: String },
    name: { type: String },
    suite: { type: String },
  }).validate(req.params);

  const ooIprocessData = async () => {
    const doc = await fetch(`${Meteor.settings.endpoint}/upload/${req.params.moduleId}/${req.params.folder}/${req.params.name}/file/${req.params.suite}`);
    const ooiResponseData = await doc.buffer();
    const fileType = await FileType.fromBuffer(ooiResponseData);

    runAsUser(userId, function () {
      try {
        const collectionScope = nameToCollection(req.params.folder);
        const scopeOne = collectionScope.findOne({
          _id: new Mongo.ObjectID(req.params.name),
        });

        if (!scopeOne) {
          const valid = {
            status: false,
            msg: 'not element',
          };
          res.status(404).json(valid);
          return;
        }

        if (req.params.folder === 'citoyens') {
          if (userId !== req.params.name) {
            const valid = {
              status: false,
              msg: 'not admin',
            };
            res.status(404).json(valid);
            return;
          }
        } else if (req.params.folder === 'projects') {
          if (!scopeOne.isAdmin()) {
            if (!scopeOne.isContributors(userId)) {
              const valid = {
                status: false,
                msg: 'not admin',
              };
              res.status(404).json(valid);
              return;
            }
          }
        } else if (req.params.folder === 'organizations') {
          if (!scopeOne.isAdmin()) {
            if (!scopeOne.isMembers(userId)) {
              const valid = {
                status: false,
                msg: 'not admin',
              };
              res.status(404).json(valid);
              return;
            }
          }
        } else if (req.params.folder === 'events') {
          if (!scopeOne.isAdmin()) {
            if (!scopeOne.isAttendees(userId)) {
              const valid = {
                status: false,
                msg: 'not admin',
              };
              res.status(404).json(valid);
              return;
            }
          }
        } else if (!scopeOne.isAdmin()) {
          const valid = {
            status: false,
            msg: 'not admin',
          };
          res.status(404).json(valid);
          return;
        }
        if (fileType) {
          res.setHeader('x-doc-type', fileType.mime);
        }
        res.send(ooiResponseData);
      } catch (e) {
        // console.log(e);
        handleErrorAsJson(e, req, res);
      }
    });
  };

  ooIprocessData();

  //
});
