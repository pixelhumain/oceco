import { Mongo } from 'meteor/mongo';
import crypto from 'crypto';

import { Citoyens } from '../collection/citoyens.js';

export function bin2hex(bytesLength) {
  const bin = crypto.randomBytes(bytesLength);
  // crypto.randomBytes(16).toString('hex');
  // buffer deprecated
  // return new Buffer(bin).toString('hex');
  return Buffer.from(bin).toString('hex');
}

export const generateRestToken = function (userId, tokenName) {
  const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'restAccessToken' });
  // console.log({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'personalAccessToken' });
  if (!citoyenOne) {
    const token = bin2hex(16);
    const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
    const buf = Buffer.from(tokenHash);
    const tokenHashBase64 = buf.toString('base64');
    const loginToken = {};
    loginToken.createdAt = Math.floor(new Date().getTime() / 1000);
    loginToken.hashedToken = tokenHashBase64;
    loginToken.type = 'restAccessToken';
    loginToken.name = tokenName;
    loginToken.lastTokenPart = tokenHashBase64.substr(-6);
    const modifier = {};
    modifier.$addToSet = { loginTokens: loginToken };
    Citoyens.update({ _id: new Mongo.ObjectID(userId) }, modifier);
    return token;
  } else {
    const token = bin2hex(16);
    const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
    const buf = Buffer.from(tokenHash);
    const tokenHashBase64 = buf.toString('base64');
    const modifier = {};
    modifier.$set = {};
    modifier.$set['loginTokens.$.createdAt'] = Math.floor(new Date().getTime() / 1000);
    modifier.$set['loginTokens.$.hashedToken'] = tokenHashBase64;
    modifier.$set['loginTokens.$.lastTokenPart'] = tokenHashBase64.substr(-6);
    Citoyens.update({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName }, modifier);
    return token;
  }
};

export const verifyRestToken = function (userId, tokenName, token) {
  const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'restAccessToken' });
  if (citoyenOne) {
    const loginToken = citoyenOne.loginTokens.filter((l) => l.name === tokenName && l.type === 'restAccessToken');

    const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
    const buf = Buffer.from(tokenHash);
    const tokenHashBase64 = buf.toString('base64');

    if (tokenHashBase64 === loginToken[0].hashedToken) {
      return true;
    }
  }
  return false;
};

// sso et autres
export const verifyPersonalToken = function (userId, tokenName, token) {
  const citoyenOne = Citoyens.findOne({ _id: new Mongo.ObjectID(userId), 'loginTokens.name': tokenName, 'loginTokens.type': 'personalAccessToken' });
  if (citoyenOne) {
    const loginToken = citoyenOne.loginTokens.filter((l) => l.name === tokenName && l.type === 'personalAccessToken');

    const tokenHash = crypto.createHmac('sha256', userId).update(token).digest('bin');
    const buf = Buffer.from(tokenHash);
    const tokenHashBase64 = buf.toString('base64');

    if (tokenHashBase64 === loginToken[0].hashedToken) {
      return true;
    }
  }
  return false;
};

export const generateEncryptedToken = (tokenData) => {
  try {
    const { passphrase } = Meteor.settings;
    const algorithm = 'aes-256-cbc';
    const iv = crypto.randomBytes(16);
    const key = crypto.pbkdf2Sync(passphrase, 'salt', 100000, 32, 'sha512'); // Génère une clé de 32 octets

    const cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
    let encrypted = cipher.update(JSON.stringify(tokenData));
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return `${iv.toString('hex')}:${encrypted.toString('hex')}`;
  } catch (error) {
    console.log(error);
    throw new Meteor.Error('500', 'Erreur lors de la génération du token');
  }
};

export const decryptToken = (encryptedToken) => {
  try {
    const { passphrase } = Meteor.settings;
    const algorithm = 'aes-256-cbc';
    const key = crypto.pbkdf2Sync(passphrase, 'salt', 100000, 32, 'sha512'); // Génère une clé de 32 octets

    const tokenParts = encryptedToken.split(':');
    const iv = Buffer.from(tokenParts.shift(), 'hex');
    const encryptedText = Buffer.from(tokenParts.join(':'), 'hex');

    const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return JSON.parse(decrypted.toString());
  } catch (error) {
    console.log(error);
    throw new Meteor.Error('500', 'Erreur lors de la génération du token');
  }
};

// const creationTimestamp = new Date().getTime();

// const tokenData = {
//   userId: '',
//   tokenName: 'restOceco',
//   tokentype: 'restAccessToken',
//   token: '',
//   createdAt: creationTimestamp
// };

// const encryptedToken = generateEncryptedToken(tokenData); // Expiration en 60 minutes
// console.log(encryptedToken);
// const tokenData = decryptToken(encryptedToken);
// console.log(tokenData);