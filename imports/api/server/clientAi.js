import { Meteor } from 'meteor/meteor';
import { OpenAiApi } from './OpenAiApi.js';
import { MistralAiApi } from './mistralAIApi.js';

const apiKey = Meteor.settings?.openai?.apiKey || null;
// const organization = Meteor.settings.openai.organization;

export const clientAi = new OpenAiApi(apiKey);

// const apiKey = Meteor.settings.mistralai.apiKey;

// export const clientAi = new MistralAiApi(apiKey);