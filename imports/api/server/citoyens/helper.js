import { Meteor } from 'meteor/meteor';
import { Citoyens } from '../../collection/citoyens.js';
import { applyDiacritics } from '../../helpers';
import { Actions } from '../../collection/actions';

Citoyens.helpers({
  actionsUserAllOpti(userId, etat, search) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();

    // faire un ou si date pas remplie
    const query = {};
    const inputDate = new Date();
    // query.endDate = { $gte: inputDate };

    const fields = {};
    if (search) {
      // regex qui marche coté serveur parcontre seulement sur un mot
      const searchApplyDiacritics = applyDiacritics(search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), 'regex');
      const pattern = new RegExp(`.*${searchApplyDiacritics.replace(/\\/g, '\\\\')}.*`, 'i');
      fields.name = { $regex: pattern };
    }

    const finishedObj = {};
    if (etat === 'aFaire') {
      finishedObj[`finishedBy`] = {
        $not: {
          $elemMatch: {
            userId: bothUserId
          }
        }
      };
    } else if (etat === 'enAttente') {
      finishedObj['finishedBy'] = {
        $elemMatch: {
          userId: bothUserId,
          status: 'toModerate'
        }
      };
    }

    query.$or = [];
    query.$or.push({
      endDate: { $exists: true, $gte: inputDate }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj,
    });
    query.$or.push({
      endDate: { $exists: false }, parentId: { $in: [bothUserId] }, status: 'todo', ...fields, ...finishedObj,
    });

    const options = {};
    options.sort = {
      startDate: 1,
    };

    return Actions.find(query);
  },
});