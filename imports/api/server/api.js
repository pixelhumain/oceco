/* eslint-disable no-else-return */
/* eslint-disable no-prototype-builtins */
/* eslint-disable consistent-return */
/* eslint-disable import/prefer-default-export */
import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/jkuester:http';
// import { request } from 'meteor/froatsnook:request';
import FormData from 'form-data';
import { fetch } from 'meteor/fetch';

export const apiCommunecter = {};

const callPixelRest = (token, name, method, controller, action, post) => {
  // post['json'] = 1;
  // console.log(post);
  // console.log(`${Meteor.settings.endpoint}/${controller}/${action}`);
  const responsePost = HTTP.call(method, `${Meteor.settings.endpoint}/${controller}/${action}`, {
    headers: {
      'X-Auth-Token': token,
      'X-User-Id': Meteor.userId(),
      'X-Auth-Name': name,
      // Origin: 'https://co-mobile.communecter.org',
    },
    params: post,
    npmRequestOptions: {
      jar: true,
    },
  });
  // console.log(responsePost);
  if (responsePost && responsePost.data && responsePost.data.result) {
    return responsePost;
  }
  if (responsePost && responsePost.data && responsePost.data.msg) {
    // console.log(responsePost);
    throw new Meteor.Error('error_call', responsePost.data.msg);
  } else if (responsePost && responsePost.data && responsePost.data.msgError && responsePost.data.msgError.msg) {
    // console.log(responsePost);
    throw new Meteor.Error('error_call', responsePost.data.resultErrors.msg);
  } else if (responsePost && responsePost.data && responsePost.data.resultErrors && responsePost.data.resultErrors.msg) {
    // console.log(responsePost);
    throw new Meteor.Error('error_call', responsePost.data.resultErrors.msg);
  } else {
    throw new Meteor.Error('error_server', 'error server');
  }
};

const callPixelMethodRest = (token, name, method, controller, action, post) => {
  post.json = 1;
  // console.log(post);
  const responsePost = HTTP.call(method, `${Meteor.settings.endpoint}/${controller}/${action}`, {
    headers: {
      'X-Auth-Token': token,
      'X-User-Id': Meteor.userId(),
      'X-Auth-Name': name,
      // Origin: 'https://co-mobile.communecter.org',
    },
    params: post,
    npmRequestOptions: {
      jar: true,
    },
  });
  // console.log(responsePost);
  if (responsePost && responsePost.data) {
    return responsePost;
  }
  throw new Meteor.Error('error_server', 'error server');
};

apiCommunecter.postPixel = function (controller, action, params) {
  const userC = Meteor.users.findOne({ _id: Meteor.userId() });
  // console.log(userC.services.resume.loginTokens[0]);
  if (userC && userC.profile && userC.profile.token) {
    const name = userC.profile.nameToken ? userC.profile.nameToken : 'comobi';
    const retour = callPixelRest(userC.profile.token, name, 'POST', controller, action, params);
    return retour;
  }
  throw new Meteor.Error('Error identification');
};

apiCommunecter.postPixelMethod = function (controller, action, params) {
  const userC = Meteor.users.findOne({ _id: Meteor.userId() });
  if (userC && userC.profile && userC.profile.token) {
    const name = userC.profile.nameToken ? userC.profile.nameToken : 'comobi';
    const retour = callPixelMethodRest(userC.profile.token, name, 'POST', controller, action, params);
    return retour;
  }
  throw new Meteor.Error('Error identification');
};

apiCommunecter.callRCRestUserToken = (userId, method, action, post) => {
  // console.log(post);
  const params = {};
  params.userId = userId;
  const auth = apiCommunecter.callRCrest('POST', 'users.createToken', params);
  if (auth && auth.data && auth.data.authToken) {
    try {
      const responsePost = HTTP.call(method, `${Meteor.settings.rocketchat.host}/api/v1/${action}`, {
        headers: {
          'X-Auth-Token': auth.data.authToken,
          'X-User-Id': auth.data.userId,
          // 'Content-type': 'application/json'
        },
        params: post,
      });
      // console.log(responsePost);
      if (responsePost && responsePost.data && responsePost.data.success) {
        return responsePost.data;
      }
    } catch (error) {
      // console.log(error.response.data.error);
      if (error && error.response && error.response.data && error.response.data.error) {
        throw new Meteor.Error(error.response.data.errorType, error.response.data.error);
      } else {
        throw new Meteor.Error('error_server', 'error server RC');
      }
    }
  }
};

apiCommunecter.callRCrest = (method, action, post) => {
  // console.log(post);
  let sendObject = {};
  try {
    if (method === 'POST') {
      sendObject = {
        headers: {
          'X-Auth-Token': Meteor.settings.rocketchat.token,
          'X-User-Id': Meteor.settings.rocketchat.userId,
          'Content-type': 'application/json',
        },
        data: post,
      };
    } else {
      sendObject = {
        headers: {
          'X-Auth-Token': Meteor.settings.rocketchat.token,
          'X-User-Id': Meteor.settings.rocketchat.userId,
        },
        params: post,
      };
    }
    const responsePost = HTTP.call(method, `${Meteor.settings.rocketchat.host}/api/v1/${action}`, sendObject);
    // console.log(responsePost);
    if (responsePost && responsePost.data && responsePost.data.success) {
      return responsePost.data;
    }
  } catch (error) {
    // console.log(error.response.data.error);
    if (error && error.response && error.response.data && error.response.data.error) {
      throw new Meteor.Error(error.response.data.errorType, error.response.data.error);
    } else {
      throw new Meteor.Error('error_server', 'error server RC');
    }
  }
};

apiCommunecter.callRCInfo = (name) => {
  const params = {};
  params.roomName = name;
  const info = apiCommunecter.callRCrest('GET', 'rooms.info', params);
  if (info) {
    const room = {};
    room._id = info.room._id;
    room.fname = info.room.fname;
    room.type = info.room.t === 'c' ? 'channel' : 'group';
    return room;
  }
  return false;
};

apiCommunecter.callRCPostMessage = (name, params) => {
  const info = apiCommunecter.callRCInfo(name);
  if (info) {
    params.roomId = info._id;
    params.alias = 'oceco';
    // params.emoji = '';
    params.avatar = Meteor.isDevelopment ? Meteor.absoluteUrl('/avatar.png', { rootUrl: 'http://192.168.1.24:3000' }) : Meteor.absoluteUrl('/avatar.png');
    // const userIdTarget = userId || Meteor.userId();
    // const retour = apiCommunecter.callRCRestUserToken(userIdTarget, 'POST', 'chat.postMessage', params);
    const retour = apiCommunecter.callRCrest('POST', 'chat.postMessage', params);
    // console.log(retour);
    return retour;
  }
};

apiCommunecter.callRCCreate = ({ name, type, username, owner = false, customFields = [] }) => {
  let info = false;
  const errorLog = {};
  // console.log({ name, type, username, owner, customFields });
  try {
    info = apiCommunecter.callRCInfo(name);
  } catch (error) {
    // console.log('info error', error);
    errorLog.infoRoom = error;
  }
  // console.log('info', info);
  const method = type === 'channel' ? 'channels' : 'groups';
  let path = type === 'channel' ? `/channel/${name}` : `/group/${name}`;
  // verifier si username existe sur le chat
  let user = false;
  try {
    user = apiCommunecter.callRCrest('GET', 'users.info', { username });
  } catch (error) {
    // console.log('user error', error);
    errorLog.users = error;
  }
  const retour = { path: path };
  if (user && user.user && user.user._id) {
    if (!info) {
      // create
      let retourCreate = false;
      try {
        const paramsCreate = { name };
        retourCreate = apiCommunecter.callRCrest('POST', `${method}.create`, paramsCreate);
      } catch (error) {
        // console.log('create error', error);
        errorLog.create = error;
      }

      if (retourCreate) {
        // invite
        try {
          const paramsInvite = { roomName: name, username };
          apiCommunecter.callRCrest('POST', `${method}.invite`, paramsInvite);
        } catch (error) {
          // console.log('invite error', error);
          errorLog.invite = error;
        }

        // ajouter owner
        if (owner) {
          try {
            const paramsOwner = { roomName: name, username };
            apiCommunecter.callRCrest('POST', `${method}.addOwner`, paramsOwner);
          } catch (error) {
            errorLog.addOwner = error;
            // console.log('addOwner error', error);
          }
        }

        // set custom fields
        if (customFields.length > 0) {
          try {
            const paramsCostum = { roomName: name, customFields };
            apiCommunecter.callRCrest('POST', `${method}.setCustomFields`, paramsCostum);
          } catch (error) {
            // console.log('setCustomFields error', error);
            if (error.error === 'invalid-roomCustomFields-type') {
              errorLog.setCustomFields = error;
            }
            /* error : 'invalid-roomCustomFields-type',
            reason : 'Invalid roomCustomFields type [invalid-roomCustomFields-type]', */
          }
        }
      }
      return retourCreate ? retour : false;
    } else {
      // le channel/group existe deja
      // verifier le type du channel/group est correct
      const infoType = info.type === 'channel' ? 'channels' : 'groups';
      // console.log('infoType', infoType, 'method', method);
      if (infoType !== method) {
        // le type est different
        // changer le type
        let retourSetType = false;
        try {
          const switchType = method === 'channels' ? 'c' : 'p';
          const paramsSetType = { roomName: name, type: switchType };
          retourSetType = apiCommunecter.callRCrest('POST', `${infoType}.setType`, paramsSetType);
        } catch (error) {
          // console.log('setType error', error);
          errorLog.setType = error;
        }
        retour.path = method === 'channels' ? `/channel/${name}` : `/group/${name}`;
        retour.setType = retourSetType;
      }

      // verifier si l'utilisateur est deja dans le channel/group
      // invite
      try {
        const paramsInvite = { roomName: name, username };
        apiCommunecter.callRCrest('POST', `${method}.invite`, paramsInvite);
      } catch (error) {
        // console.log('invite error', error);
        errorLog.invite = error;
      }

      // ajouter moderator car deja créer
      if (owner) {
        try {
          const paramsOwner = { roomName: name, username };
          apiCommunecter.callRCrest('POST', `${method}.addModerator`, paramsOwner);
        } catch (error) {
          // console.log('addOwner error', error);
          if (error.error !== 'error-user-already-moderator') {
            errorLog.addOwner = error;
          }
          /* error: 'error-user-already-owner',
          reason : 'User is already an owner [error-user-already-owner]', */
        }
      }
      return retour;
    }
  } else {
    // l'utilisateur n'existe pas sur rocketchat
    if (errorLog.users) {
      throw errorLog.users;
    }
  }
  return false;
};

apiCommunecter.callRCInvites = ({ name, userIds = [], adminsUserIds = [] }) => {
  let info = false;
  const errorLog = {};
  // console.log({ name, userIds, adminsUserIds });
  if (userIds.length === 0 && adminsUserIds.length === 0) {
    throw new Meteor.Error('error', 'userIds is empty');
  }
  // console.log({ name, type, username, owner, customFields });
  try {
    info = apiCommunecter.callRCInfo(name);
  } catch (error) {
    // console.log('info error', error);
    errorLog.infoRoom = error;
  }
  // console.log('info', info);
  const infoType = info.type === 'channel' ? 'channels' : 'groups';
  if (info) {
    if (userIds.length > 0) {
      try {
        const paramsInvite = { roomName: name, userIds };
        apiCommunecter.callRCrest('POST', `${infoType}.invite`, paramsInvite);
      } catch (error) {
        // console.log('invite error', error);
        errorLog.invite = error;
      }
      // if (errorLog.invite) {
      //   throw errorLog.invite;
      // }
    }
    if (adminsUserIds.length > 0) {
      adminsUserIds.forEach((username) => {
        try {
          const paramsOwner = { roomName: name, username };
          apiCommunecter.callRCrest('POST', `${infoType}.addModerator`, paramsOwner);
        } catch (error) {
          if (error.error !== 'error-user-already-moderator') {
            errorLog.addOwner = error;
          }
          /* error: 'error-user-already-owner',
          reason : 'User is already an owner [error-user-already-owner]', */
        }
      });
    }
  } else {
    if (errorLog.infoRoom) {
      throw errorLog.infoRoom;
    }
  }
  return false;
};

apiCommunecter.callTibilletrest = (server, apiKey, method, action, post) => {
  // console.log(post);
  let sendObject = {};
  try {
    sendObject = {
      headers: {
        Authorization: `Api-Key ${apiKey}`,
      },
      params: post,
    };
    const responsePost = HTTP.call(method, `${server}/api/${action}`, sendObject);
    // console.log(responsePost);
    if (responsePost && responsePost.data && responsePost.statusCode === 200) {
      return responsePost.data;
    }
  } catch (error) {
    // console.log(error);
    if (error && error.response && error.response.data) {
      if (error.response.data.number_printed && error.response.data.number_printed[0]) {
        throw new Meteor.Error(error.response.data.number_printed[0], error.response.data.number_printed[0]);
      }
      if (error.response.data.qty_oceco && error.response.data.qty_oceco[0]) {
        throw new Meteor.Error(error.response.data.qty_oceco[0], error.response.data.qty_oceco[0]);
      }
      throw new Meteor.Error(error.response.data, error.response.data);
    } else {
      throw new Meteor.Error('error_server', 'error server Tibillet');
    }
  }
};

apiCommunecter.callGitlabrest = (server, token, method, action, post = null) => {
  // console.log(post);
  let sendObject = {};
  try {
    sendObject = {
      headers: {
        'PRIVATE-TOKEN': token,
      },
      params: post,
    };
    if (post) {
      sendObject.params = post;
    }
    const responsePost = HTTP.call(method, `${server}/${action}`, sendObject);
    // console.log(responsePost.statusCode);
    if (responsePost && responsePost.data && (responsePost.statusCode === 201 || responsePost.statusCode === 200)) {
      return responsePost.data;
    }
  } catch (error) {
    // console.log(error);
    if (error) {
      if (error && error.response && error.response.data && error.response.data.error) {
        throw new Meteor.Error('error_git', error.response.data.error);
      } else {
        throw new Meteor.Error('error_git', error);
      }
    }
  }
};

apiCommunecter.callGithubrest = (server, token, method, action, post = null) => {
  // console.log(post);
  let sendObject = {};
  try {
    sendObject = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `token ${token}`,
      },
      data: post,
    };
    if (post) {
      sendObject.data = post;
    }
    const responsePost = HTTP.call(method, `${server}/${action}`, sendObject);
    // console.log(responsePost.statusCode);
    if (responsePost && responsePost.data && (responsePost.statusCode === 201 || responsePost.statusCode === 200)) {
      return responsePost.data;
    }
  } catch (error) {
    // console.log(error);
    if (error) {
      if (error && error.response && error.response.data && error.response.data.message) {
        throw new Meteor.Error('error_git', error.response.data.message);
      } else {
        throw new Meteor.Error('error_git', error);
      }
    }
  }
};

const dataUriToBuffer = (uri) => {
  if (!/^data:/i.test(uri)) {
    throw new TypeError('`uri` does not appear to be a Data URI (must begin with "data:")');
  }

  // strip newlines
  // eslint-disable-next-line no-param-reassign
  uri = uri.replace(/\r?\n/g, '');

  // split the URI up into the "metadata" and the "data" portions
  const firstComma = uri.indexOf(',');
  if (firstComma === -1 || firstComma <= 4) throw new TypeError('malformed data: URI');

  // remove the "data:" scheme and parse the metadata
  const meta = uri.substring(5, firstComma).split(';');

  let base64 = false;
  for (let i = 0; i < meta.length; i += 1) {
    if (meta[i] === 'base64') {
      base64 = true;
    } else if (meta[i].indexOf('charset=') === 0) {
      /* charset = meta[i].substring(8); */
    }
  }

  // get the encoded data portion and decode URI-encoded chars
  const data = unescape(uri.substring(firstComma + 1));

  const encoding = base64 ? 'base64' : 'ascii';
  // const buffer = new Buffer(data, encoding);
  // eslint-disable-next-line new-cap
  const buffer = new Buffer.from(data, encoding);

  // set `.type` property to MIME type
  buffer.type = meta[0] || 'text/plain';

  // set the `.charset` property
  // buffer.charset = charset;

  return buffer;
};

const callPixelUploadRest = async (token, nameToken, folder, ownerId, input, dataURI, name) => {
  const fileBuf = dataUriToBuffer(dataURI);
  const formData = {};

  const form = new FormData();

  formData[input] = {
    value: fileBuf,
    options: {
      filename: name,
      contentType: 'image/jpeg',
    },
  };

  const appendFormValue = function (key, value) {
    if (value && value.hasOwnProperty('value') && value.hasOwnProperty('options')) {
      form.append(key, value.value, value.options);
    } else {
      form.append(key, value);
    }
  };
  // eslint-disable-next-line no-restricted-syntax
  for (const formKey in formData) {
    if (formData.hasOwnProperty(formKey)) {
      const formValue = formData[formKey];
      if (formValue instanceof Array) {
        for (let j = 0; j < formValue.length; j++) {
          appendFormValue(formKey, formValue[j]);
        }
      } else {
        appendFormValue(formKey, formValue);
      }
    }
  }

  try {
    const response = await fetch(`${Meteor.settings.endpoint}/${Meteor.settings.module}/document/upload/dir/communecter/folder/${folder}/ownerId/${ownerId}/input/${input}`, {
      method: 'POST',
      headers: {
        'X-Auth-Token': token,
        'X-User-Id': Meteor.userId(),
        'X-Auth-Name': nameToken,
        ...form.getHeaders(),
        // 'Content-type': 'application/json'
      },
      body: form,
    });

    const data = await response.json();

    if (data && data.success === true) {
      // console.log(data);
      return data;
    }
    if (data && data.msg) {
      throw new Meteor.Error('error_call', data.msg);
    }
  } catch (error) {
    // your catch block code goes here
    throw new Meteor.Error('error_server', 'error server');
  }
};

const callPixelUploadSaveRest = async (token, nameToken, folder, ownerId, input, dataURI, name, doctype, contentKey, params = null) => {
  const fileBuf = dataUriToBuffer(dataURI);
  const formData = {};

  const form = new FormData();

  formData[input] = {
    value: fileBuf,
    options: {
      filename: name,
      // contentType: 'image/jpeg',
    },
  };

  if (params) {
    if (params.parentId) {
      formData.parentId = params.parentId;
    }
    if (params.parentType) {
      formData.parentType = params.parentType;
    }
    if (input) {
      formData.formOrigin = input;
    }
  }

  const appendFormValue = function (key, value) {
    if (value && value.hasOwnProperty('value') && value.hasOwnProperty('options')) {
      form.append(key, value.value, value.options);
    } else {
      form.append(key, value);
    }
  };
  // eslint-disable-next-line no-restricted-syntax
  for (const formKey in formData) {
    if (formData.hasOwnProperty(formKey)) {
      const formValue = formData[formKey];
      if (formValue instanceof Array) {
        for (let j = 0; j < formValue.length; j++) {
          appendFormValue(formKey, formValue[j]);
        }
      } else {
        appendFormValue(formKey, formValue);
      }
    }
  }

  const contentKeyUrl = contentKey ? `/contentKey/${contentKey}` : '';

  // console.log(`${Meteor.settings.endpoint}/${Meteor.settings.module}/document/uploadSave/dir/communecter/folder/${folder}/ownerId/${ownerId}/input/${input}/docType/${doctype}${contentKeyUrl}`);

  // http://localhost:5080/co2/document/uploadSave/dir/communecter/folder/projects/ownerId/5eaf1e396908641b7a8b46c9/input/newsFile/docType/file

  // http://localhost:5080/co2/document/uploadSave/dir/communecter/folder/citoyens/ownerId/55ed9107e41d75a41a558524/input/newsFile/docType/file

  // http://localhost:5080/co2/document/uploadSave/dir/communecter/folder/projects/ownerId/5eaf1e396908641b7a8b46c9/input/newsImage/docType/image/contentKey/slider

  try {
    const response = await fetch(`${Meteor.settings.endpoint}/${Meteor.settings.module}/document/uploadSave/dir/communecter/folder/${folder}/ownerId/${ownerId}/input/${input}/docType/${doctype}${contentKeyUrl}`, {
      method: 'POST',
      headers: {
        'X-Auth-Token': token,
        'X-User-Id': Meteor.userId(),
        'X-Auth-Name': nameToken,
        ...form.getHeaders(),
        // 'Content-type': 'application/json'
      },
      body: form,
    });

    const data = await response.json();
    // console.log(data);
    if (data && data.success === true) {
      // console.log(data);
      return data;
    }
    if (data && data.msg) {
      throw new Meteor.Error('error_call', data.msg);
    }
  } catch (error) {
    // your catch block code goes here
    // console.log(error);
    throw new Meteor.Error('error_server', 'error server');
  }
};

apiCommunecter.postUploadPixel = async (folder, ownerId, input, dataBlob, name) => {
  const userC = Meteor.users.findOne({ _id: Meteor.userId() });
  if (userC && userC.profile && userC.profile.token) {
    const nameToken = userC.profile.nameToken ? userC.profile.nameToken : 'comobi';
    const retour = await callPixelUploadRest(userC.profile.token, nameToken, folder, ownerId, input, dataBlob, name);
    if (retour && retour.name) {
      return retour;
    }
    throw new Meteor.Error('Error upload');
  } else {
    throw new Meteor.Error('Error identification');
  }
};

apiCommunecter.postUploadSavePixel = async (folder, ownerId, input, dataBlob, name, doctype, contentKey, params = null) => {
  const userC = Meteor.users.findOne({
    _id: Meteor.userId(),
  });
  if (userC && userC.profile && userC.profile.token) {
    const nameToken = userC.profile.nameToken ? userC.profile.nameToken : 'comobi';
    const retour = await callPixelUploadSaveRest(userC.profile.token, nameToken, folder, ownerId, input, dataBlob, name, doctype, contentKey, params);
    if (retour && retour.name) {
      return retour;
    }
    throw new Meteor.Error('Error upload');
  } else {
    throw new Meteor.Error('Error identification');
  }
};

apiCommunecter.authPixelRest = (email, pwd) => {
  const response = HTTP.call('POST', `${Meteor.settings.endpoint}/${Meteor.settings.module}/person/authenticatetoken`, {
    params: {
      pwd,
      email,
    },
    npmRequestOptions: {
      jar: true,
    },
  });
  return response;
};
