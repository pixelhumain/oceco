import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';


import { Webpushsub } from '../../collection/webpushsub.js';
import { sendWebNotification } from '../webpushApi.js';

Meteor.methods({
  async webPushsaveSubscription(subscription) {
    check(subscription, Object);
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // Vérifie si la souscription existe déjà pour éviter les doublons
    const exists = await Webpushsub.findOneAsync({ userId: this.userId, subscription: subscription });
    if (!exists) {
      Webpushsub.insert({
        userId: this.userId,
        subscription: subscription,
        createdAt: new Date(), // Optionnel
      });
    }
  },
  async webPushUnsubscribe(subscription) {
    check(subscription, Object);
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    // console.log('Unsubscribe', subscription);
    const endpoint = subscription.endpoint;

    const result = await Webpushsub.removeAsync({
      userId: this.userId,
      'subscription.endpoint': endpoint
    });
    return result;
  },
});