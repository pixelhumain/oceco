import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { URL } from 'meteor/url';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Mongo } from 'meteor/mongo';
import { ChangeSet, Text } from '@codemirror/state';

import { Notes } from '../../collection/notes.js';
import { SchemasActionsRest } from '../../schema/actions.js';
import { Actions } from '../../collection/actions.js';
import { Documents } from '../../collection/documents.js';

import {
  nameToCollection,
  searchQueryNotes
} from '../../helpers.js';
import { clientAi } from '../clientAi.js';
import { apiCommunecter } from '../api.js';

import { processImage } from '../image.js';
import { name } from 'dayjs/locale/fr.js';
import { ActivityStream } from '../../collection/activitystream.js';
import { Citoyens } from '../../collection/citoyens.js';

const maintenanceNote = (docId) => {
  const documentMaintenance = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
  if (!documentMaintenance) {
    throw new Meteor.Error('not-found', 'Document non trouvé');
  }
  // on verifie si aucun links.contributors.{userId}.isPresence est présent
  if (documentMaintenance?.links?.contributors) {
    const isPresenceAbsent = Object.keys(documentMaintenance.links.contributors).every(key => {
      return !documentMaintenance.links.contributors[key].isPresence;
    });
    console.log('isPresenceAbsent', isPresenceAbsent);
    if (isPresenceAbsent) {
      // on peut remettre effacer le contenu de l'array updates pour qu'il y ai moins de données à stocker
      Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { updates: [] } });
    }
  }
};

Meteor.methods({
  getOrCreateDocument(parentType, parentId, docId) {
    check(parentType, Match.Where(function (name) {
      return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
    }));
    check(parentId, String);
    check(docId, Match.Maybe(String));

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(parentType);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(parentId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    if (docId) {
      const document = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
      // verifier si admin

      if (!document) {
        throw new Meteor.Error('not-found', 'Document non trouvé');
      }

      if (document.isPrivate() && !scopeOne.isAdmin() && !document.isContributors()) {
        throw new Meteor.Error('not-authorized');
      }

      // const updateChange = {};
      // updateChange.$set = {};
      // if (document?.links?.contributors[this.userId]) {
      //   const addPresence = `links.contributors.${this.userId}.isPresence`;
      //   updateChange.$set[addPresence] = { date: new Date() };
      // } else {
      //   const links = {};
      //   links.contributors = {};

      //   links.contributors[this.userId] = { type: 'citoyens', isPresence: { date: new Date() } };
      //   if (scopeOne.isAdmin()) {
      //     links.contributors[this.userId].isAdmin = true;
      //   }
      //   updateChange.$set.links = links;
      // }
      // Notes.update({ _id: new Mongo.ObjectID(docId) }, updateChange);

      return { doc: document.doc, version: document.updates.length, newDocId: document._id.valueOf() };
    } else {

      // verifier si admin
      if (!scopeOne.isAdmin()) {
        throw new Meteor.Error('not-authorized');
      }

      // Créer un nouveau document avec un tableau updates vide
      moment.locale('fr');
      const now = new Date();
      const links = {};
      links.contributors = {};
      links.contributors[this.userId] = { type: 'citoyens', isAdmin: true, isPresence: { date: now } };

      // ajouter le premier contributeur contributorsCount
      const newDocId = Notes.insert({ name: `Note de texte du ${moment().utc().format('LLL')} UTC`, doc: 'start', updates: [], createdAt: now, updatedAt: now, creator: this.userId, parentType, parentId, visibility: 'private', links: links, contributorsCount: 1, lastModifiedBy: this.userId, 'lastModified': [{ userId: this.userId, lastModifiedAt: now }] });
      const doc = Notes.findOne({ _id: newDocId });
      return { doc: doc.doc, version: doc.updates.length, newDocId: doc._id.valueOf() };
    }
  },
  async createNoteAuto({ parentType, parentId, name }) {
    check(parentType, Match.Where(function (name) {
      return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
    }));
    check(parentId, String);
    check(name, String);

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(parentType);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(parentId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    if (!scopeOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    // Créer un nouveau document avec un tableau updates vide
    moment.locale('fr');
    const now = new Date();
    const links = {};
    links.contributors = {};
    links.contributors[this.userId] = { type: 'citoyens', isAdmin: true };

    // ajouter le premier contributeur contributorsCount
    const newDocId = await Notes.insertAsync({ name: name, doc: 'start', updates: [], createdAt: now, updatedAt: now, creator: this.userId, parentType, parentId, visibility: 'private', links: links, contributorsCount: 1, lastModifiedBy: this.userId, 'lastModified': [{ userId: this.userId, lastModifiedAt: now }] });
    return { newDocId: newDocId.valueOf() };
  },
  updateNoteTitle(parentType, parentId, docId, title) {
    check(parentType, Match.Where(function (name) {
      return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
    }));
    check(parentId, String);
    check(docId, String);
    check(title, String);

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(parentType);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(parentId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    const document = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
    if (!document) {
      throw new Meteor.Error('not-found', 'Document non trouvé');
    }

    // verifier si admin
    if (!scopeOne.isAdmin() && !document.isContributors()) {
      throw new Meteor.Error('not-authorized');
    }

    const now = new Date();

    const query = { _id: new Mongo.ObjectID(docId) };
    const updateChange = { $set: { name: title, updatedAt: now } };

    // add lastModified
    const lastModifiedArray = document.lastModified || [];
    const existingContributorIndex = lastModifiedArray.findIndex(c => c.userId === this.userId);
    if (existingContributorIndex > -1) {
      query["lastModified.userId"] = this.userId
      updateChange.$set["lastModified.$.lastModifiedAt"] = now;
    } else {
      updateChange.$push = { 'lastModified': { userId: this.userId, lastModifiedAt: now } };
    }
    updateChange.$set.lastModifiedBy = this.userId;

    Notes.update(query, updateChange);
    return true;
  },
  pushUpdates(parentType, parentId, docId, requestedVersion, updates) {
    check(parentType, Match.Where(function (name) {
      return ['events', 'projects', 'organizations', 'citoyens'].includes(name);
    }));
    check(parentId, String);
    check(docId, String);
    check(requestedVersion, Number);
    check(updates, Array);
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(parentType);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(parentId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    const document = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
    if (!document) {
      throw new Meteor.Error('not-found', 'Document non trouvé');
    }

    // verifier si admin
    if (!scopeOne.isAdmin() && !document.isContributors()) {
      throw new Meteor.Error('not-authorized');
    }

    try {
      if (requestedVersion != document.updates.length) {
        // console.log('requestedVersion != document.updates.length');
        return false;
      } else {
        let doc = Text.of([document.doc])

        const lastModifiedArray = document.lastModified || [];
        const existingContributorIndex = lastModifiedArray.findIndex(c => c.userId === this.userId);

        const query = { _id: new Mongo.ObjectID(docId) };

        for (let update of updates) {
          const changes = ChangeSet.fromJSON(update.changes);
          doc = changes.apply(doc);

          const now = new Date();

          const updateChange = {
            $push: {
              updates: {
                changes: update.changes,
                clientID: update.clientID,
                effects: update.effects,
                version: document.updates.length,
                ChangeUpdatedAt: now
              }
            },
            $set: {
              doc: doc.toString(),
              updatedAt: now,
            }
          };

          if (doc.toString() !== document.doc) {
            // add lastModified
            if (existingContributorIndex > -1) {
              query["lastModified.userId"] = this.userId
              updateChange.$set["lastModified.$.lastModifiedAt"] = now;
            } else {
              updateChange.$push.lastModified = { userId: this.userId, lastModifiedAt: now };
            }
            updateChange.$set.lastModifiedBy = this.userId;
          }

          Notes.update(query, updateChange);
        }
        return true;
      }
    } catch (error) {
      // console.error(error)
    }


  },
  removePresence(docId) {
    check(docId, String);
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const document = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
    if (!document) {
      throw new Meteor.Error('not-found', 'Document non trouvé');
    }

    const addPresence = `links.contributors.${this.userId}.isPresence`;
    Notes.update({ _id: new Mongo.ObjectID(docId) }, { $unset: { [addPresence]: '' } });

    maintenanceNote(docId);

    return true;
  },
  exitNote({ id, memberId, }) {
    new SimpleSchema({
      id: {
        type: String,
      },
      memberId: {
        type: String,
        optional: true,
      },
    }).validate({
      id, memberId,
    });

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    const note = Notes.findOne({ _id: new Mongo.ObjectID(id) });

    if (!note) {
      throw new Meteor.Error('not-note');
    }

    if (memberId) {
      // verifier admin
      const collection = nameToCollection(note.parentType);

      if (!collection.findOne({ _id: new Mongo.ObjectID(note.parentId) }).isAdmin()) {
        throw new Meteor.Error('not-admin');
      }
    }

    const parent = memberId ? `links.contributors.${memberId}` : `links.contributors.${Meteor.userId()}`;
    // decrementer le nombre de contributeur
    Notes.update({
      _id: new Mongo.ObjectID(id),
    }, {
      $unset: {
        [parent]: '',
      },
      $inc: {
        contributorsCount: -1,
      },
    });

    // // notification
    // const noteOne = Notes.findOne({
    //   _id: new Mongo.ObjectID(id),
    // });

    // const notif = {};
    // const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
    //   fields: {
    //     _id: 1, name: 1, email: 1, username: 1,
    //   },
    // });
    // // author
    // notif.author = {
    //   id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    // };
    // // object
    // notif.object = {
    //   id: noteOne._id.valueOf(), name: noteOne.name, type: 'actions', parentType: noteOne.parentType, parentId: noteOne.parentId
    // };

    // if (memberId) {
    //   // mention
    //   const mentionOne = Citoyens.findOne({ _id: new Mongo.ObjectID(memberId) });
    //   notif.mention = {
    //     id: mentionOne._id.valueOf(), name: mentionOne.name, type: 'citoyens', username: mentionOne.username,
    //   };
    //   ActivityStream.api.add(notif, 'leaveAssignNote', 'isAdmin');
    //   ActivityStream.api.add(notif, 'leaveAssignNote', 'isUser', null, memberId);
    // } else {
    //   ActivityStream.api.add(notif, 'leaveNote', 'isAdmin');
    // }

    return true;
  },
  async photoNotes(photo, str, type, idType, docId) {
    check(str, String);
    check(type, String);
    check(idType, String);
    check(docId, String);
    check(type, Match.Where(function (name) {
      return ['projects', 'organizations', 'citoyens'].includes(name);
    }));

    const collectionScope = nameToCollection(type);
    const scopeOne = collectionScope.findOne({
      _id: new Mongo.ObjectID(idType),
    });

    if (!scopeOne) {
      throw new Meteor.Error('scope-not-exist');
    }

    const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
    if (!noteOne) {
      throw new Meteor.Error('note-no-exit');
    }

    // verifier si admin
    if (!scopeOne.isAdmin() && !noteOne.isContributors()) {
      throw new Meteor.Error('not-authorized');
    }

    let photoDataURI;
    try {
      photoDataURI = await processImage(photo);
    } catch (error) {
      throw new Meteor.Error('error-process-image', error.message);
    }

    const doc = await apiCommunecter.postUploadSavePixel(type, idType, 'newsImage', photoDataURI, str, 'image', 'slider');

    if (doc) {

      doc.id = doc.id && doc.id.$id ? doc.id.$id : doc.id && doc.id.$oid ? doc.id.$oid : null;
      if (noteOne && noteOne.media && noteOne.media.images && noteOne.media.images.length > 0) {
        // console.log(noteOne.media.images.length);
        const countImages = noteOne.media.images.length + 1;
        Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { 'media.countImages': countImages.toString() }, $push: { 'media.images': doc.id } });
      } else {
        const media = {};
        media.type = 'gallery_images';
        media.countImages = '1';
        media.images = [doc.id];
        Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { media } });
      }

      // au participant
      // au admin

      // const notif = {};
      // const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      //   fields: {
      //     _id: 1, name: 1, email: 1, username: 1,
      //   },
      // });
      // // author
      // notif.author = {
      //   id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
      // };
      // // object
      // notif.object = {
      //   id: noteOne._id.valueOf(), name: noteOne.name, type: 'actions', links: noteOne.links, parentType: noteOne.parentType, parentId: noteOne.parentId, docPath: doc.docPath,
      // };

      // ActivityStream.api.add(notif, 'addActionimage', 'isActionMembers');
      // ActivityStream.api.add(notif, 'addActionimage', 'isAdmin');

      const imageOne = Documents.findOne({ _id: new Mongo.ObjectID(doc.id) });

      return { photoret: doc.id, docId: docId, url: `${Meteor.settings.public.urlimage}/upload/${imageOne.moduleId}/${imageOne.folder}/${imageOne.name}` };
    }
    throw new Meteor.Error('postUploadPixel-error');
  },
  async docNotes(photo, str, type, idType, docId) {
    check(str, String);
    check(type, String);
    check(idType, String);
    check(docId, String);
    check(type, Match.Where(function (name) {
      return ['projects', 'organizations', 'citoyens'].includes(name);
    }));
    this.unblock();
    const collectionScope = nameToCollection(type);
    const scopeOne = collectionScope.findOne({
      _id: new Mongo.ObjectID(idType),
    });

    if (!scopeOne) {
      throw new Meteor.Error('scope-not-exist');
    }

    const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
    if (!noteOne) {
      throw new Meteor.Error('note-no-exit');
    }

    // verifier droit
    if (!scopeOne.isAdmin() && !noteOne.isContributors()) {
      throw new Meteor.Error('not-authorized');
    }

    const doc = await apiCommunecter.postUploadSavePixel(type, idType, 'newsFile', photo, str, 'file');

    // console.log(doc);

    if (doc) {

      doc.id = doc.id && doc.id.$id ? doc.id.$id : doc.id && doc.id.$oid ? doc.id.$oid : null;

      if (noteOne && noteOne.mediaFile && noteOne.mediaFile.files && noteOne.mediaFile.files.length > 0) {
        // console.log(noteOne.media.images.length);
        const countImages = noteOne.mediaFile.files.length + 1;
        Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { 'mediaFile.countImages': countImages.toString() }, $push: { 'mediaFile.files': doc.id } });
      } else {
        const mediaFile = {};
        mediaFile.countImages = '1';
        mediaFile.files = [doc.id];
        Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { mediaFile } });
      }

      // au participant
      // au admin

      // const notif = {};
      // const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      //   fields: {
      //     _id: 1, name: 1, email: 1, username: 1,
      //   },
      // });
      // // author
      // notif.author = {
      //   id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
      // };
      // // object
      // notif.object = {
      //   id: noteOne._id.valueOf(), name: noteOne.name, type: 'actions', links: noteOne.links, parentType: noteOne.parentType, parentId: noteOne.parentId, docPath: doc.docPath,
      // };

      // ActivityStream.api.add(notif, 'addActionDoc', 'isActionMembers');
      // ActivityStream.api.add(notif, 'addActionDoc', 'isAdmin');

      const imageOne = Documents.findOne({ _id: new Mongo.ObjectID(doc.id) });

      return { photoret: doc.id, docId: docId, url: `${Meteor.settings.public.urlimage}/upload/${imageOne.moduleId}/${imageOne.folder}/${imageOne.name}` };
    }
    throw new Meteor.Error('postUploadPixel error');
  },
  async completeSemanticAnalysis(text, model) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      const analysisResult = await clientAi.completeSemanticAnalysis(text);
      return analysisResult;
    } catch (error) {
      throw new Meteor.Error('error-complete-semantic-analysis', error.message);
    }
  },
  async reportOpinion(text, model) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      const analysisResult = await clientAi.reportOpinion(text);
      return analysisResult;
    } catch (error) {
      throw new Meteor.Error('error-report-opinion', error.message);
    }
  },
  async correctText({ text, model }) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      const correctTextResult = await clientAi.correctText(text);
      return correctTextResult;
    } catch (error) {
      throw new Meteor.Error('error-correct-text', error.message);
    }
  },
  async reformulateText({ text, model }) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      const reformulateTextResult = await clientAi.reformulateText(text);
      return reformulateTextResult;
    } catch (error) {
      throw new Meteor.Error('error-reformulate-text', error.message);
    }
  },
  async generateMermaidDiagram({ text, model }) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      //'gpt-4-0125-preview'
      const mermaidDiagramResult = await clientAi.generateMermaidDiagram(text, 'gpt-4-0125-preview');
      return mermaidDiagramResult;
    } catch (error) {
      throw new Meteor.Error('error-generate-mermaid-diagram', error.message);
    }
  },
  async markdownText({ text, model }) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      //'gpt-4-0125-preview'
      const markdownTextResult = await clientAi.markdownText(text, 'gpt-4-0125-preview');
      return markdownTextResult;
    } catch (error) {
      throw new Meteor.Error('error-markdown-text', error.message);
    }
  },
  async respondToText({ text, model }) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      // 'gpt-4-0125-preview'
      const respondToTextResult = await clientAi.respondToText(text, 'gpt-4-0125-preview');
      return respondToTextResult;
    } catch (error) {
      throw new Meteor.Error('error-respond-to-text', error.message);
    }
  },
  async createImageFromText({ text, model }) {
    check(text, String);
    check(model, Match.Maybe(String));
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    try {
      const respondToTextResult = await clientAi.createImageFromText(text, 'dall-e-3', { n: 1, size: "1792x1024" });


      return respondToTextResult;
    } catch (error) {
      throw new Meteor.Error('error-create-image-from-text', error.message);
    }
  },
  async searchNotes({ search, scope, scopeId }) {
    check(search, Match.Maybe(String));
    check(scope, String);
    check(scopeId, String);

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    let query = {};
    try {
      if (search.length > 0) {
        query = searchQueryNotes(query, search, ['name']);
      }

      // si citoyens et que c'est nous
      if (scope === 'citoyens') {
        // if (scopeId === this.userId) {
        // on est sur nos notes : on voit toutes nos notes et celles ou on est contributeurs
        query.$or = [{ parentType: scope, parentId: this.userId }, { [`links.contributors.${this.userId}`]: { $exists: true } }];
        // } else {
        //   // on est sur la note d'un autre citoyen qui à partager avec nous ou en public :
        //   // on liste mes notes perso public et mes notes perso ou il est contributeur
        //   query.$or = [
        //     { parentType: scope, parentId: this.userId, visibility: 'public' },
        //     { parentType: scope, parentId: this.userId, [`links.contributors.${scopeId}`]: { $exists: true } }
        //   ];
        // }
      } else {
        // c'est sur un projet ou une organisation
        // voir la note si elle est public ou si on est contributeurs
        // query.$or = [{ parentType: scope, parentId: scopeId, visibility: 'public' }, { parentType: scope, parentId: scopeId, [`links.contributors.${this.userId}`]: { $exists: true } }];
        query.parentType = scope;
        query.parentId = scopeId;
      }

      const options = {
        fields: { _id: 1, name: 1, parentId: 1, parentType: 1, 'links.contributors': 1 },
        sort: { updatedAt: -1 },
        limit: 15,
      };

      const noteslist = await Notes.find(query, options).fetchAsync();
      return noteslist;
    } catch (error) {
      throw new Meteor.Error('error-search-notes', error.message);
    }
  },

});



export const assignMemberNote = new ValidatedMethod({
  name: 'assignMemberNote',
  validate: new SimpleSchema({
    id: { type: String },
    memberId: { type: String },
  }).validator(),
  run({
    id, memberId
  }) {

    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const noteObjectId = new Mongo.ObjectID(id);
    const noteOne = Notes.findOne({ _id: noteObjectId }, { fields: { _id: 1, name: 1, parentId: 1, parentType: 1, 'links.contributors': 1 } });
    const parentObjectId = new Mongo.ObjectID(noteOne.parentId);
    const { parentType } = noteOne;
    const collection = nameToCollection(parentType);

    const scopeOne = collection.findOne({ _id: parentObjectId });

    if (!scopeOne) {
      throw new Meteor.Error('not-exist-parent');
    }

    if (!scopeOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const contributor = {};
    contributor[`links.contributors.${memberId}`] = { type: 'citoyens' };
    const retour = Notes.update({ _id: noteObjectId, }, { $set: contributor, $inc: { 'contributorsCount': 1 } });

    // notification
    const notif = {};
    const authorOne = Citoyens.findOne({ _id: new Mongo.ObjectID(this.userId) }, {
      fields: {
        _id: 1, name: 1, email: 1, username: 1,
      },
    });
    // author
    notif.author = {
      id: authorOne._id.valueOf(), name: authorOne.name, type: 'citoyens', username: authorOne.username,
    };
    // object
    notif.object = {
      id: noteOne._id.valueOf(), name: noteOne.name, type: 'notes', parentType: noteOne.parentType, parentId: noteOne.parentId
    };

    // mention
    const mentionOne = Citoyens.findOne({ _id: new Mongo.ObjectID(memberId) });
    notif.mention = {
      id: mentionOne._id.valueOf(), name: mentionOne.name, type: 'citoyens', username: mentionOne.username,
    };

    if (this.userId === memberId) {
      //   ActivityStream.api.add(notif, 'joinNote', 'isAdmin');
    } else {
      // ActivityStream.api.add(notif, 'joinAssignNote', 'isAdmin');
      ActivityStream.api.add(notif, 'joinAssignNote', 'isUser', null, memberId);
    }
    return retour;
  },
});

export const assignArrayMembersNote = new ValidatedMethod({
  name: 'assignArrayMembersNote',
  validate: new SimpleSchema({
    id: { type: String },
    membersId: { type: Array },
    'membersId.$': { type: String },
  }).validator(),
  run({
    id, membersId,
  }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }
    // assign
    if (membersId && membersId.length > 0) {
      membersId.forEach((memberId) => {
        Meteor.call('assignMemberNote', { id: id, memberId });
      });
    }
  }
});


export const leaveShareNote = new ValidatedMethod({
  name: 'leaveShareNote',
  validate: new SimpleSchema({
    docId: { type: String },
  }).validator(),
  run({ docId }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });

    if (!noteOne) {
      throw new Meteor.Error('not-note');
    }

    const contributor = {};
    contributor[`links.contributors.${this.userId}`] = '';
    const retour = Notes.update({ _id: new Mongo.ObjectID(docId), }, { $unset: contributor, $inc: { 'contributorsCount': -1 } });

    return retour;
  },
});

export const trackingNote = new ValidatedMethod({
  name: 'trackingNote',
  validate: new SimpleSchema({
    docId: { type: String },
    tracking: { type: Boolean },
    organizationId: { type: String, optional: true },
  }).validator(),
  run({ tracking, docId, organizationId }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const scopeNoteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });

    if (!scopeNoteOne) {
      throw new Meteor.Error('not-note');
    }

    const options = {};
    if (scopeNoteOne.parentType === 'citoyens') {
      options.parentType = 'citoyens';
      options.parentId = this.userId;
    } else if (scopeNoteOne.parentType === 'organizations') {
      options.parentType = 'organizations';
      options.parentId = scopeNoteOne.parentId;
    } else if (scopeNoteOne.parentType === 'projects') {
      options.parentType = 'projects';
      options.parentId = scopeNoteOne.parentId;
      options.organizationId = organizationId;
    }

    Meteor.call('favorisScope', { scope: 'notes', scopeId: scopeNoteOne._id.valueOf(), options });

    // peut être bloqué au user qui sont associé à la note ?
    // const docUpdate = {};
    // docUpdate.$set = {};
    // docUpdate.$set.tracking = tracking;

    // Mise à jour spécifique à l'utilisateur
    const updateField = `tracking.${this.userId}`;
    const update = tracking ? { $set: { [updateField]: true } } : { $unset: { [updateField]: '' } };

    const retour = Notes.update({ _id: new Mongo.ObjectID(docId) }, update);
    return retour;
  },
});

export const deleteNote = new ValidatedMethod({
  name: 'deleteNote',
  validate: new SimpleSchema({
    scope: { type: String, allowedValues: ['organizations', 'projects', 'events', 'citoyens'] },
    scopeId: { type: String },
    docId: { type: String },
  }).validator(),
  run({ scope, scopeId, docId }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(scope);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    // verifier si admin
    if (!scopeOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const scopeNoteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });

    if (!scopeNoteOne) {
      throw new Meteor.Error('not-note');
    }

    const docList = scopeNoteOne.docList();
    if (docList) {
      docList.forEach((file) => {
        Meteor.call('noteDocDelete', { name: file.name, parentId: scopeId, parentType: scope, path: file.moduleId, id: file._id.valueOf(), docId: docId });
      });
    }

    const retour = Notes.remove({ _id: new Mongo.ObjectID(docId) });

    // ok on met à jour les actions liées et on met à jour avec le dernier doc la description
    Actions.update({ noteId: docId }, { $unset: { noteId: '' }, $set: { description: scopeNoteOne.doc } }, { multi: true });
    return retour;
  },
});

export const updateNoteVisibility = new ValidatedMethod({
  name: 'updateNoteVisibility',
  validate: new SimpleSchema({
    scope: { type: String, allowedValues: ['organizations', 'projects', 'events', 'citoyens'] },
    scopeId: { type: String },
    docId: { type: String },
    visibility: { type: String, allowedValues: ['public', 'private'] },
  }).validator(),
  run({ scope, scopeId, docId, visibility }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(scope);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    // verifier si admin
    if (!scopeOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const scopeNoteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });

    if (!scopeNoteOne) {
      throw new Meteor.Error('not-note');
    }

    const retour = Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { visibility } });
    return retour;
  },
});

export const updateNoteLockEditing = new ValidatedMethod({
  name: 'updateNoteLockEditing',
  validate: new SimpleSchema({
    scope: { type: String, allowedValues: ['organizations', 'projects', 'events', 'citoyens'] },
    scopeId: { type: String },
    docId: { type: String },
    lock: { type: Boolean },
  }).validator(),
  run({ scope, scopeId, docId, lock }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(scope);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    // verifier si admin
    if (!scopeOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const scopeNoteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });

    if (!scopeNoteOne) {
      throw new Meteor.Error('not-note');
    }

    const retour = Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { lockEditing: lock } });
    return retour;
  },
});

export const convertNoteToAction = new ValidatedMethod({
  name: 'convertNoteToAction',
  validate: new SimpleSchema({
    scope: { type: String, allowedValues: ['organizations', 'projects', 'events', 'citoyens'] },
    scopeId: { type: String },
    docId: { type: String },
  }).validator(),
  run({ scope, scopeId, docId }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    const collection = nameToCollection(scope);
    const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });

    if (!scopeOne) {
      throw new Meteor.Error('not-parent');
    }

    // verifier si admin
    if (!scopeOne.isAdmin()) {
      throw new Meteor.Error('not-authorized');
    }

    const scopeNoteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });

    if (!scopeNoteOne) {
      throw new Meteor.Error('not-note');
    }

    // transformer la note en action
    const noteToAction = {};
    noteToAction.name = scopeNoteOne.name;
    noteToAction.description = scopeNoteOne.doc;
    noteToAction.parentType = scope;
    noteToAction.parentId = scopeId;

    const actionPost = SchemasActionsRest.clean(noteToAction);
    const retourCall = Meteor.call('insertAction', noteToAction);
    // console.log(retourCall);
    if (retourCall) {

      const updateChange = { $set: { 'noteId': docId } };
      // récupére les liste de tâches de la note qu'elle soit check [X|x] ou pas []
      const listTasks = scopeNoteOne.doc.match(/- \[(X|x| )\] (.*)/g);

      if (listTasks) {
        const tasks = listTasks.map((task) => {
          const taskInsert = {};
          taskInsert.taskId = Random.id();
          // Utilisez une regex pour extraire le statut de la tâche et le texte de la tâche
          const match = task.match(/- \[(X|x| )\] (.*)/);
          taskInsert.task = match[2]; // Le texte de la tâche
          taskInsert.userId = Meteor.userId();
          // Définissez checked à true si le match contient 'X' ou 'x', sinon false
          taskInsert.checked = match[1].toLowerCase() === 'x';
          taskInsert.createdAt = new Date();
          return taskInsert;
        });
        updateChange.$push = { tasks: { $each: tasks } };
      }

      Actions.update({ _id: new Mongo.ObjectID(retourCall.data.id) }, updateChange);
      // supprimer la note
      // Notes.remove({ _id: new Mongo.ObjectID(docId) });


      return true;
    }
    return false;
  },
});

export const noteDocDelete = new ValidatedMethod({
  name: 'noteDocDelete',
  validate: new SimpleSchema({
    name: {
      type: String,
    },
    parentId: {
      type: String,
    },
    parentType: {
      type: String,
    },
    path: {
      type: String,
    },
    id: {
      type: String,
    },
    docId: {
      type: String,
    }
  }).validator(),
  run({
    name, parentId, parentType, path, id, docId,
  }) {
    if (!this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    check(parentType, Match.Where(function (name) {
      return ['projects', 'organizations', 'citoyens'].includes(name);
    }));

    const collectionScope = nameToCollection(parentType);
    const scopeOne = collectionScope.findOne({
      _id: new Mongo.ObjectID(parentId),
    });

    if (!scopeOne) {
      throw new Meteor.Error('scope-not-exist');
    }

    // verifier droit
    const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
    if (!noteOne) {
      throw new Meteor.Error('note-no-exit');
    }

    // verifier si admin
    if (!scopeOne.isAdmin() && !noteOne.isContributors()) {
      throw new Meteor.Error('not-authorized');
    }

    const docRetour = {};
    docRetour.name = name;
    docRetour.parentId = parentId;
    docRetour.parentType = parentType;
    docRetour.path = path;
    docRetour.ids = [];
    docRetour.ids.push(id);

    // const retour = apiCommunecter.postPixel('co2/document', `delete/dir/co2/type/${parentType}/id/${parentId}`, docRetour);
    const retour = apiCommunecter.postPixel('co2/document', `delete/contextType/${parentType}/contextId/${parentId}`, docRetour);
    // console.log(retour);

    if (retour) {
      // si name finit par .jpg ou .png ou .jpeg
      if (name.match(/\.(jpg|jpeg|png)$/)) {
        if (noteOne && noteOne.media && noteOne.media.images && noteOne.media.images.length > 1) {
          const countImages = noteOne.media.images.length - 1;
          Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { 'media.countImages': countImages.toString() }, $pull: { 'media.images': id } });
          return { docId };
        }
      } else {
        if (noteOne && noteOne.mediaFile && noteOne.mediaFile.files && noteOne.mediaFile.files.length > 1) {
          const countImages = noteOne.mediaFile.files.length - 1;
          Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { 'mediaFile.countImages': countImages.toString() }, $pull: { 'mediaFile.files': id } });
          return { docId };
        }
      }

      Notes.update({ _id: new Mongo.ObjectID(docId) }, { $unset: { media: '' } });
      return { docId };
    }
    throw new Meteor.Error('postUploadPixel error');
  },
});