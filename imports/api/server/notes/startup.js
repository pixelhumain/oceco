
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Notes, NotesConnexion } from '/imports/api/collection/notes.js';

Meteor.startup(function () {

  // nettoyer les notesConnexion et les notes si serveur redémarré
  NotesConnexion.find({}).forEach(function (connexion) {
    Notes.update({ _id: new Mongo.ObjectID(connexion.docId) }, { $set: { [`links.contributors.${connexion.userId}.isPresence`]: '' } });
    NotesConnexion.remove({ _id: connexion._id });
  });

});