import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import { Mongo } from 'meteor/mongo';

import { Citoyens } from '../../collection/citoyens.js';
import { Notes, NotesConnexion } from '../../collection/notes.js';
import { Organizations } from '../../collection/organizations.js';
import { Projects } from '../../collection/projects.js';

import {
  nameToCollection, arrayLinkProperNoObject, optionsNoFieldsScope,
  queryOrPrivateScope,
  queryOrPrivateScopeLinks
} from '../../helpers.js';


Meteor.publishComposite('documentUpdates', function (scope, scopeId, docId, noCo) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  check(docId, String);
  check(noCo, Match.Maybe(Boolean));

  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
  if (!scopeOne) {
    return null;
  }
  const userId = this.userId;
  const connexionId = this._session.connectionHandle.id;

  const notelog = NotesConnexion.findOne({ docId: docId, userId: userId });
  if (!notelog) {
    NotesConnexion.insert({ docId: docId, userId: userId, connexionId: connexionId, date: new Date() });
  }

  // if (notelog && notelog.connexionId !== connexionId) {

  // } else if (notelog && notelog.connexionId === connexionId) {

  // }

  const deconexionNote = (userId, connexionId, docId) => {
    // console.log('deconexionNote');
    const connexionPresence = `links.contributors.${userId}.isPresence.connexion`;
    // console.log(connexionPresence, connexionId);
    const noteConnexion = Notes.findOne({ _id: new Mongo.ObjectID(docId), [connexionPresence]: { $exists: true } });
    if (noteConnexion?.links?.contributors?.[userId]?.isPresence?.connexion?.[connexionId]) {
      const addPresence = `links.contributors.${userId}.isPresence`;
      // console.log(`present donc remove ${addPresence}`);
      Notes.update({ _id: new Mongo.ObjectID(docId) }, { $unset: { [addPresence]: '' } });

      const documentMaintenance = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
      if (!documentMaintenance) {
        return null;
      }
      // on verifie si aucun links.contributors.{userId}.isPresence est présent
      if (documentMaintenance?.links?.contributors) {
        const isPresenceAbsent = Object.keys(documentMaintenance.links.contributors).every(key => {
          return !documentMaintenance.links.contributors[key].isPresence;
        });
        if (isPresenceAbsent) {
          // on peut remettre effacer le contenu de l'array updates pour qu'il y ai moins de données à stocker
          Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { updates: [] } });
        }
      }
      NotesConnexion.remove({ docId: docId, userId: userId, connexionId: connexionId });
    }
  };

  const noteConnexion = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
  if (noteConnexion && noteConnexion.isContributors()) {
    if (noteConnexion.isConnexion()) {
      // console.log('deja connexion');
      // deja connecté sur la note avce cette user
      if (!noteConnexion.links.contributors[userId].isPresence.connexion[connexionId]) {
        // c'est le même user mais pas la même connexion donc ne pas laisser possible la modification
        return null;
      }
    } else {
      // console.log('ajout de la connexion');
      // le connexion est vide pour cette user donc on peut ajouter la connexion
      const IsPresence = { type: 'citoyens', isPresence: { date: new Date(), connexion: { [connexionId]: true } } };
      if (scopeOne.isAdmin()) {
        IsPresence.isAdmin = true;
      }
      Notes.update({ _id: new Mongo.ObjectID(docId) }, { $set: { [`links.contributors.${userId}`]: IsPresence } });
      Meteor._sleepForMs(200);
    }
  }


  if (!noCo) {
    // gestion de la deconnexion de la note
    this.onStop(() => {
      // console.log('fermeture de la onStop de la note');
      deconexionNote(userId, connexionId, docId);
    });
  }

  // Meteor._sleepForMs(10000);

  return [{
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      const { fields } = optionsNoFieldsScope(scope);
      options.fields = fields;
      return collection.find(query, options);
    },
  },
  {
    find() {
      return Notes.find({ _id: new Mongo.ObjectID(docId) })
    },
    children: [
      {
        find(doc) {
          let arrayContributorsMongoId = [];
          if (doc.links.contributors) {
            const arrayContributors = arrayLinkProperNoObject(doc.links.contributors);
            arrayContributorsMongoId = arrayContributors.map((k) => new Mongo.ObjectID(k));
          }
          return Citoyens.find({ _id: { $in: [...arrayContributorsMongoId, new Mongo.ObjectID(this.creator)] } }, { fields: { _id: 1, name: 1, username: 1, profilThumbImageUrl: 1 } });
        },
      },
      {
        find(doc) {
          return doc.docList()
        },
      },
    ],
  }];
});

Meteor.publishComposite('noteOne', function (scope, scopeId, docId, isPublic) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens', 'events'].includes(name);
  }));
  check(docId, String);
  // isPublic est présent il est egale à true
  check(isPublic, Match.Maybe(Boolean));

  const collection = nameToCollection(scope);
  if (!this.userId && !isPublic) {
    return null;
  }
  const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
  if (!scopeOne) {
    return null;
  }

  const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
  if (!noteOne) {
    return null;
  }

  return [{
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      const query = {};
      query._id = new Mongo.ObjectID(scopeId);
      const { fields } = optionsNoFieldsScope(scope);
      options.fields = fields;
      return collection.find(query, options);
    },
  },
  {
    find() {
      const query = { _id: new Mongo.ObjectID(docId) };
      if (isPublic) {
        query.visibility = 'public';
      }
      return Notes.find(query);
    },
    children: [
      {
        find(doc) {
          let arrayContributorsMongoId = [];
          if (doc.links.contributors) {
            const arrayContributors = arrayLinkProperNoObject(doc.links.contributors);
            arrayContributorsMongoId = arrayContributors.map((k) => new Mongo.ObjectID(k));
          }
          return Citoyens.find({ _id: { $in: [...arrayContributorsMongoId, new Mongo.ObjectID(this.creator)] } }, { fields: { _id: 1, name: 1, username: 1, profilThumbImageUrl: 1 } });
        },
      },
    ],
  }];
});

Meteor.publishComposite('listAssignNotes', function ({ scopeId, docId, search, limit }) {
  check(scopeId, Match.Maybe(String));
  check(docId, String);
  check(search, Match.Maybe(String));
  check(limit, Match.Maybe(Number));
  if (!this.userId) {
    return null;
  }
  // TODO : verifier si l'utilisateur est bien admin de l'organisation

  const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(docId) });



  return [
    {
      find() {
        const query = queryOrPrivateScopeLinks('members', Meteor.userId());
        const options = {};
        options.fields = { _id: 1, name: 1, slug: 1, profilThumbImageUrl: 1, [`links.members.${Meteor.userId()}`]: 1, ['links.projects']: 1, preferences: 1, 'oceco.pole': 1 };
        return Organizations.find(query, options);
      },
    },
    {
      find() {
        const options = {};
        const { fields } = optionsNoFieldsScope('organizations');
        options.fields = fields;
        return Organizations.find({ _id: new Mongo.ObjectID(scopeId) }, options);
      },
      children: [
        {
          find(organisation) {
            return organisation.listMembersNotes(docId, search, limit);
          },
        },
      ],
    },
    {
      find() {
        if (noteOne.parentType === 'projects') {
          return Projects.find({ _id: new Mongo.ObjectID(noteOne.parentId) }, { fields: { costum: 0 } });
        }
      },
      children: [
        {
          find(project) {
            return project.listContributorsNotes(docId, search, limit);
          },
        },
      ],
    }];
});

Meteor.publishComposite('directoryListNotes', function ({ scope, scopeId, search, searchSort, limit }) {
  check(scopeId, String);
  check(scope, String);
  check(scope, Match.Where(function (name) {
    return ['projects', 'organizations', 'citoyens'].includes(name);
  }));
  check(search, Match.Maybe(String));
  check(searchSort, Match.Maybe(Object));
  check(limit, Match.Maybe(Number));
  const collection = nameToCollection(scope);
  if (!this.userId) {
    return null;
  }
  return {
    find() {
      const options = {};
      // options['_disableOplog'] = true;
      let query = {};
      if (['projects', 'organizations'].includes(scope)) {
        query.$or = [];
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': false,
        });
        query.$or.push({
          _id: new Mongo.ObjectID(scopeId),
          'preferences.private': {
            $exists: false,
          },
        });

        if (scope === 'projects') {
          query = queryOrPrivateScope(query, 'contributors', scopeId, this.userId);
        } else if (scope === 'organizations') {
          query = queryOrPrivateScope(query, 'members', scopeId, this.userId);
        }
      } else {
        query._id = new Mongo.ObjectID(scopeId);
      }

      if (['citoyens', 'projects', 'organizations'].includes(scope)) {
        const { fields } = optionsNoFieldsScope(scope);
        options.fields = fields;
      }

      return collection.find(query, options);
    },
    children: [
      {
        find(scopeD) {
          if (scope === 'citoyens') {
            return scopeD.listOrganizationsCreator();
          }
        },
      },
      {
        find(scopeD) {
          if (scope === 'citoyens' || scope === 'organizations' || scope === 'projects') {
            return scopeD.listNotesCreator(search, searchSort, limit);
          }
        },
        children: [
          {
            find(noteOne) {
              if (noteOne.parentType && noteOne.parentId) {
                const collection = nameToCollection(noteOne.parentType);
                return collection.find({ _id: new Mongo.ObjectID(noteOne.parentId) }, { fields: { _id: 1, name: 1, 'links.projects': 1, [`links.contributors.${this.userId}`]: 1, parent: 1 } });
              }
            },
          },
          {
            find(note) {
              if (note && note.links && note.links.contributors) {
                const arrayContributors = arrayLinkProperNoObject(note.links.contributors);
                if (arrayContributors && arrayContributors[0]) {
                  const arrayAllMergeMongoId = arrayContributors.map((k) => new Mongo.ObjectID(k));
                  arrayAllMergeMongoId.push(new Mongo.ObjectID(note.creator))
                  return Citoyens.find({ _id: { $in: arrayAllMergeMongoId } }, { fields: { name: 1, username: 1, profilThumbImageUrl: 1 } });
                }
              }
            },
          },
          {
            find(note) {
              if (note && note.lastModifiedBy && note.lastModifiedBy !== this.userId) {
                return Citoyens.find({ _id: new Mongo.ObjectID(note.lastModifiedBy) }, { fields: { name: 1, username: 1, profilThumbImageUrl: 1 } });
              }
            },
          }],
      },
    ],
  };
});