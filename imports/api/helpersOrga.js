/* eslint-disable meteor/no-session */
/* global Session */
import { Mongo } from 'meteor/mongo';
import { Organizations } from './collection/organizations.js';
import { Projects } from './collection/projects.js';
import { Actions } from './collection/actions.js';
import { Events } from './collection/events.js';

const organizationCible = ({ orgaNoName, orgaCibleId }) => {
  // eslint-disable-next-line no-unused-vars
  const parentDataContext = { orgaNoName, orgaCibleId };
  // IonModal.open('organizationCible', parentDataContext);
};

export const projectsCountOrga = ({ orgaNoName, countOrgaNow, orgaCibleId }) => {
  if (countOrgaNow) {
    // on sur une orga deja bonne avec le projet
    // console.log('news news.js 123 ok bonne', orgaCibleId);
  } else {
    // console.log('news news.js 126 orgaNoName count', orgaNoName.count(), orgaCibleId);
    // eslint-disable-next-line no-lonely-if
    if (orgaNoName.count() > 1) {
      // il y a au moin 2 orga en plus
      // ? faire demande
      // console.log('news news.js 129 multi', orgaCibleId);
      organizationCible({ orgaNoName, orgaCibleId });
    } else if (orgaNoName.count() === 1) {
      // il y a qu'une seul orga
      const orgaNoNameOne = orgaNoName.fetch()[0];
      if (orgaNoNameOne && Session.get('orgaCibleId') !== orgaNoNameOne._id.valueOf()) {
        Session.setPersistent('orgaCibleId', orgaNoNameOne._id.valueOf());

        // console.log('news news.js 111', orgaCibleId, orgaNoNameOne._id.valueOf());
      }
    }
  }
};

export const eventsCountOrga = ({
  _id, orgaNoName, orgaCibleId, projectOne,
}) => {
  // probléme un event peut être lier à plusieurs projets
  const event = `links.events.${_id}`;
  if (projectOne) {
    if (projectOne.count() > 1) {
      // si plusieurs projets lier à l'event
      const query = {};
      query.$or = [];
      Projects.find({ [event]: { $exists: 1 } }).forEach((project) => {
        const projectKey = `links.projects.${project._id.valueOf()}`;
        query.$or.push({ _id: new Mongo.ObjectID(orgaCibleId), [projectKey]: { $exists: true } });
      });

      const orgaSelector = Organizations.find(query);
      /* orgId = orgaSelector.map((orga) => {
              return orga._id.valueOf();
            }); */

      const countOrgaNow = orgaSelector.count();
      projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId });
    } else if (projectOne.count() === 1) {
      // un projet lier à l'event
      const projectOneArray = projectOne.fetch();
      const project = `links.projects.${projectOneArray[0]._id.valueOf()}`;
      const orgaSelector = Organizations.find({ _id: new Mongo.ObjectID(orgaCibleId), [project]: { $exists: 1 } });
      /* orgId = orgaSelector.map((orga) => {
              return orga._id.valueOf();
            }); */
      const countOrgaNow = orgaSelector.count();
      projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId });
    }
  } else {
    // il est pas dans la liste des projets en local
    // eslint-disable-next-line no-lonely-if
    if (orgaNoName.count() > 1) {
      // il y a au moin 2 orga en plus
      // ? faire demande
      // console.log('news news.js 129 multi', orgaCibleId);
      organizationCible({ orgaNoName, orgaCibleId });
    } else if (orgaNoName.count() === 1) {
      // il y a qu'une seul orga
      const orgaNoNameOne = orgaNoName.fetch()[0];
      if (orgaNoNameOne && orgaCibleId !== orgaNoNameOne._id.valueOf()) {
        Session.setPersistent('orgaCibleId', orgaNoNameOne._id.valueOf());

        // console.log('news news.js 111', orgaCibleId, orgaNoNameOne._id.valueOf());
      }
    }
  }
};

// hack pour bypass le parent avec plusieurs element
// on prend le premier ajouter comme main
export const orgaIdScope = ({ scope, scopeId }) => {
  let orgId = null;
  if (scope === 'projects') {
    const projectOne = Projects.findOne({ _id: new Mongo.ObjectID(scopeId) }, { fields: { _id: 1, parent: 1 } });
    const arrayIds = Object.keys(projectOne.parent)
      .filter((k) => projectOne.parent[k].type === 'organizations')
      .map((k) => new Mongo.ObjectID(k));
    const orgaSelector = Organizations.find({ _id: { $in: [arrayIds[0]] }, oceco: { $exists: true } });
    orgId = orgaSelector.map((orga) => orga._id.valueOf());
  } else if (scope === 'events') {
    const eventOne = Events.findOne({ _id: new Mongo.ObjectID(scopeId) }, { fields: { _id: 1, organizer: 1 } });
    const arrayIdsEvent = Object.keys(eventOne.organizer)
      .filter((k) => eventOne.organizer[k].type === 'projects')
      .map((k) => new Mongo.ObjectID(k));

    const projectOne = Projects.findOne({ _id: { $in: [arrayIdsEvent[0]] } });
    const arrayIds = Object.keys(projectOne.parent)
      .filter((k) => projectOne.parent[k].type === 'organizations')
      .map((k) => new Mongo.ObjectID(k));

    const orgaSelector = Organizations.find({ _id: { $in: [arrayIds[0]] }, oceco: { $exists: true } });
    orgId = orgaSelector.map((orga) => orga._id.valueOf());
  } else if (scope === 'actions') {
    const actionObjectId = new Mongo.ObjectID(scopeId);
    const actionOne = Actions.findOne({ _id: actionObjectId });

    const parentObjectId = new Mongo.ObjectID(actionOne.parentId);

    const eventOne = Events.findOne({ _id: parentObjectId });
    if (eventOne) {
      const arrayIdsEvent = Object.keys(eventOne.organizer)
        .filter((k) => eventOne.organizer[k].type === 'projects')
        .map((k) => new Mongo.ObjectID(k));

      const projectOne = Projects.findOne({ _id: { $in: [arrayIdsEvent[0]] } });
      const arrayIds = Object.keys(projectOne.parent)
        .filter((k) => projectOne.parent[k].type === 'organizations')
        .map((k) => new Mongo.ObjectID(k));

      const orgaSelector = Organizations.find({ _id: { $in: [arrayIds[0]] }, oceco: { $exists: true } });
      orgId = orgaSelector.map((orga) => orga._id.valueOf());
    } else {
      const projectOne = Projects.findOne({ _id: parentObjectId });
      if (projectOne) {
        const arrayIds = Object.keys(projectOne.parent)
          .filter((k) => projectOne.parent[k].type === 'organizations')
          .map((k) => new Mongo.ObjectID(k));
        const orgaSelector = Organizations.find({ _id: { $in: [arrayIds[0]] }, oceco: { $exists: true } });
        orgId = orgaSelector.map((orga) => orga._id.valueOf());
      } else {
        const orgaSelector = Organizations.find({ _id: parentObjectId, oceco: { $exists: true } });
        orgId = orgaSelector.map((orga) => orga._id.valueOf());
      }
    }
  }
  return orgId;
};
