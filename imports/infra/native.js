import { Meteor } from 'meteor/meteor';

const nativeSettings = Meteor.settings.public.native || {};

// Récupère les identifiants natifs en utilisant les valeurs par défaut si les paramètres sont manquants
export const APPLE_ITUNES_APP_ID = nativeSettings.appleItunesAppId || null;
const APPLE_TEAM_ID = nativeSettings.appleTeamId || null;
const APPLE_BUNDLE_ID = nativeSettings.appleBundleId || null;
const GOOGLE_PLAY_APP_ID = nativeSettings.googlePlayAppId || null;

export const getGoolePlayAppUrl = ({ googlePlayAppId = GOOGLE_PLAY_APP_ID }) => {
  if (!googlePlayAppId) {
    return null;
  }
  return `https://play.google.com/store/apps/details?id=${googlePlayAppId}`;
};

export const getAppleItunesAppUrl = ({ appleItunesAppId = APPLE_ITUNES_APP_ID }) => {
  if (!appleItunesAppId) {
    return null;
  }
  return `https://itunes.apple.com/app/id${appleItunesAppId}`;
};

export const getNativeStoresInfo = () => ({
  appleItunesAppId: APPLE_ITUNES_APP_ID,
  googlePlayAppId: GOOGLE_PLAY_APP_ID,
  appleTeamId: APPLE_TEAM_ID,
  appleBundleId: APPLE_BUNDLE_ID,
  nativeAppEnabled: nativeSettings.nativeAppEnabled || false,
});
