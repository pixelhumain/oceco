import { Meteor } from 'meteor/meteor';

export const Colors = { BACKGROUND: '#FFFFFF', PRIMARY: '#324553' };
export const LANGUAGE = 'fr-FR';
export const NAME = 'Oceco';
export const SHORT_NAME = NAME;
export const DESCRIPTION = 'OCECO : Outils COllaboratifs d’Économie COopérative';
export const KEYWORDS = ['communecter', 'collaboratif'];
export const LOGO_URL_WITHOUT_EXT = `${Meteor.absoluteUrl()}oceco`;
