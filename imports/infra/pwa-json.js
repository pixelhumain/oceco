import { Meteor } from 'meteor/meteor';

import {
  getAppleItunesAppUrl,
  getGoolePlayAppUrl,
  getNativeStoresInfo,
} from './native';

import {
  Colors, DESCRIPTION,
  LANGUAGE,
  LOGO_URL_WITHOUT_EXT,
  NAME,
  SHORT_NAME,
} from './constants';

export const getPwaSettings = () => {
  const logo = LOGO_URL_WITHOUT_EXT;
  const nativeStoresInfo = getNativeStoresInfo();
  const {
    appleItunesAppId,
    googlePlayAppId,
    nativeAppEnabled,
  } = nativeStoresInfo;

  return {
    background_color: Colors.BACKGROUND,
    theme_color: Colors.PRIMARY,
    start_url: '/',
    display: 'standalone',
    display_override: ["standalone"],
    orientation: 'portrait',
    lang: LANGUAGE,
    name: NAME,
    short_name: SHORT_NAME,
    description: DESCRIPTION,
    icons: [
      {
        "src": `${Meteor.absoluteUrl()}ic_stat_co_24.png`,
        "sizes": "48x48",
        "type": "image/png",
        "purpose": "monochrome"
      },
      {
        "src": `${Meteor.absoluteUrl()}oceco_red_48.png`,
        "sizes": "48x48",
        "type": "image/png"
      },
      // {
      //   src: `${logo}_72.png`,
      //   type: 'image/png',
      //   sizes: '72x72',
      // },
      {
        "src": `${Meteor.absoluteUrl()}oceco_gris_white_96.png`,
        "sizes": "96x96",
        "type": "image/png"
      },
      {
        src: `${logo}_128.png`,
        type: 'image/png',
        sizes: '128x128',
      },
      {
        src: `${logo}_152.png`,
        type: 'image/png',
        sizes: '152x152',
      },
      {
        src: `${logo}_144.png`,
        type: 'image/png',
        sizes: '144x144',
      },
      {
        src: `${logo}_192.png`,
        type: 'image/png',
        sizes: '192x192',
      },
      {
        src: `${logo}_512.png`,
        type: 'image/png',
        sizes: '512x512',
      },
    ].filter((icon) => !!icon.src),
    screenshots: [
      {
        src: `${Meteor.absoluteUrl()}screenshots/1280x800-screenshot.png`,
        sizes: "1280x800",
        type: "image/png"
      },
      {
        src: `${Meteor.absoluteUrl()}screenshots/750x1334-screenshot.png`,
        sizes: "750x1334",
        form_factor: "wide",
        type: "image/png"
      }
    ],
    shortcuts: [
      {
        name: "Profile",
        url: "/me/profil",
        description: "Consultez votre profil et vos informations personnelles.",
        icons: [
          {
            src: "/shortcuts/home-solid.png",
            sizes: "96x96"
          }
        ]
      },
      {
        name: "Mes actions",
        url: "/me/actions",
        description: "Consultez toutes vos actions",
        icons: [
          {
            src: "/shortcuts/inbox-solid.png",
            sizes: "96x96"
          }
        ]
      },
      {
        name: "Mes notes",
        url: "/me/notes",
        description: "Consultez toutes vos notes",
        icons: [
          {
            src: "/shortcuts/sticky-note-solid.png",
            sizes: "96x96"
          }
        ]
      },
      {
        name: "Notifications",
        url: "/notifications",
        description: "Consultez toutes vos notifications et mises à jour des autres utilisateurs.",
        icons: [
          {
            src: "/shortcuts/bell-solid.png",
            sizes: "96x96"
          }
        ]
      }
    ],
    // share_target: {
    //   action: "/me/notes/share",
    //   method: "POST",
    //   enctype: "multipart/form-data",
    //   params: {
    //     files: [
    //       {
    //         name: "fichier",
    //         accept: [
    //           "text/plain",
    //         ]
    //       }
    //     ]
    //   }
    // },
    protocol_handlers: [
      {
        protocol: "web+oceco",
        url: "/redirect?url=%s",
        title: "Ouvrir dans oceco"
      },
      {
        protocol: "web+note",
        url: "/redirect?url=%s",
      }
    ],
    file_handlers: [
      {
        action: "/?file=txt",
        accept: {
          "text/plain": [".txt", ".text", ".md"],
        }
      },
      {
        action: "/?file=img",
        accept: {
          "image/jpeg": [".jpg", ".jpeg"],
          "image/jpg": [".jpg", ".jpeg"],
          "image/png": [".png"],
        }
      }
    ],
    prefer_related_applications: nativeAppEnabled,
    related_applications: [
      nativeAppEnabled
      && googlePlayAppId && {
        platform: 'play',
        url: getGoolePlayAppUrl(nativeStoresInfo),
        id: googlePlayAppId,
      },
      nativeAppEnabled
      && appleItunesAppId && {
        platform: 'itunes',
        url: getAppleItunesAppUrl(nativeStoresInfo),
        id: appleItunesAppId,
      },
    ].filter(Boolean),
  };
};

export const pwaJson = (req, res) => {
  res.setHeader('Content-Type', 'javascript/json');
  res.writeHead(200);

  // if you have multiple apps using the same backend you can customize here
  // the color, name, description, etc using the req.headers
  const pwa = getPwaSettings();

  res.end(JSON.stringify(pwa));
};
