/* eslint-disable meteor/no-session */
/* eslint-disable consistent-return */
/* global AutoForm Session IonToast */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';
import { $ } from 'meteor/jquery';
import { _ } from 'meteor/underscore';
import i18n from 'meteor/universe:i18n';

import { Citoyens } from '../../../api/collection/citoyens.js';
import { Organizations } from '../../../api/collection/organizations.js';
import { LogUserActions } from '../../../api/collection/loguseractions.js';
import { LogOrgaMonthCredit } from '../../../api/collection/logorgamonthcredit.js';

import { scrollPagination } from '../../../api/helpers.js';

import './members.html';

// window.Organizations = Organizations;
// window.Citoyens = Citoyens;

const pageSession = new ReactiveDict('pageMembres');

Template.listMembers.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));


  pageSession.setDefault('creditOrgaAllCount', 0);

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params.orgaCibleId);
    pageSession.set('scope', Router.current().params.scope);
  });

  self.autorun(function () {
    Meteor.call('creditOrgaAllCount', { scopeId: Router.current().params.orgaCibleId }, function (error, total) {
      if (!error) {
        pageSession.set('creditOrgaAllCount', total);
      }
    });
  });

  self.autorun(function () {
    const paramPub = { scopeId: Router.current().params.orgaCibleId, invite: !!(Session.get('orgaCibleId') && Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)), limit: self.limit.get('limit') };
    if (pageSession.get('search')) {
      paramPub.search = pageSession.get('search');
    }
    const handle = Meteor.subscribe('listMembers', paramPub);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });
});

Template.listMembers.onRendered(scrollPagination);

Template.listMembers.events({
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('search', null);
    }
  }, 500),
  'click .organization-members-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('member')}: ${self.name}`,
      buttons: [{ text: i18n.__('Pass admin') }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this member'),
          onOk() {
            Meteor.call('deleteMember', { scopeId: pageSession.get('scopeId'), memberId: self._id.valueOf() }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('changeRoleMember', { scopeId: pageSession.get('scopeId'), memberId: self._id.valueOf(), role: 'admin' }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
  'click .organization-members-admin-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('member')}: ${self.name}`,
      buttons: [{ text: i18n.__('Pass member') }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this member'),
          onOk() {
            Meteor.call('deleteMember', { scopeId: pageSession.get('scopeId'), memberId: self._id.valueOf() }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('changeRoleMember', { scopeId: pageSession.get('scopeId'), memberId: self._id.valueOf(), role: 'contributor' }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
  'click .organization-members-validate-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('member')}: ${self.name}`,
      buttons: [{ text: i18n.__('Validate member') }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this member'),
          onOk() {
            Meteor.call('deleteMember', { scopeId: pageSession.get('scopeId'), memberId: self._id.valueOf() }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('validateMember', { scopeId: pageSession.get('scopeId'), memberId: self._id.valueOf() }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
  'click .historic-js'(event) {


  },
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});

Template.listMembers.helpers({
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Router.current().params.orgaCibleId) });
  },
  search() {
    return pageSession.get('search');
  },
  creditOrgaAllCount() {
    return pageSession.get('creditOrgaAllCount');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  badgeColorTest(userCredit) {
    return userCredit > 0 ? 'balanced' : 'assertive';
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

Template.listMembersDetailCitoyens.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();

  pageSession.setDefault('limit', 10);
  pageSession.setDefault('incremente', 10);

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
  });

  self.autorun(function () {
    if (pageSession.get('limit')) {
      const subs = [
        Meteor.subscribe('historiqueUserActionsAllCounter', Session.get('orgaCibleId'), Router.current().params.citoyenId),
        Meteor.subscribe('listMembersDetailHistorique', Session.get('orgaCibleId'), Router.current().params.citoyenId, pageSession.get('limit'))
      ];

      Tracker.autorun(() => {
        // Vérifier si tous les abonnements sont prêts et mettre à jour isReady en conséquence
        const areAllSubsReady = subs.every(handle => handle.ready());
        self.ready.set(areAllSubsReady);
      });

    }
  });
});

Template.listMembersDetailCitoyens.onRendered(function () {
  const showMoreVisible = () => {
    const $target = $('#showMoreResults');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 10;
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visible')) {
        $target.data('visible', true);
        pageSession.set('limit', pageSession.get('limit') + pageSession.get('incremente'));
      }
    } else if ($target.data('visible')) {
      $target.data('visible', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.listMembersDetailCitoyens.events({
  'click .give-me-more'(event) {
    event.preventDefault();
    const newLimit = pageSession.get('limit') + pageSession.get('incremente');
    pageSession.set('limit', newLimit);
  },
});

Template.listMembersDetailCitoyens.helpers({
  logUserActions() {
    const options = {};
    options.limit = pageSession.get('limit');
    options.sort = { createdAt: -1 };
    return LogUserActions.find({ organizationId: Session.get('orgaCibleId'), userId: Router.current().params.citoyenId }, options);
  },
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
  },
  citoyen() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Router.current().params.citoyenId) });
  },
  isLimit(countActions) {
    return countActions > pageSession.get('limit');
  },
  countActions() {
    return Counter.get(`historiqueUsercountActionsAll.${Session.get('orgaCibleId')}.${Router.current().params.citoyenId}`);
  },
  badgeColorTest() {
    return this.credits > 0 ? 'balanced' : 'dark';
  },
  limit() {
    return pageSession.get('limit');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.listHistoricCreditDetailOrga.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();

  pageSession.setDefault('limit', 10);
  pageSession.setDefault('incremente', 10);

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
  });

  self.autorun(function () {
    if (pageSession.get('limit')) {
      // const handleCounter = Meteor.subscribe('historiqueUserActionsAllCounter', Session.get('orgaCibleId'), Router.current().params.citoyenId);
      const handle = Meteor.subscribe('listOrgaDetailHistorique', Session.get('orgaCibleId'), pageSession.get('limit'));
      Tracker.autorun(() => {
        self.ready.set(handle.ready());
      });
    }
  });
});

Template.listHistoricCreditDetailOrga.onRendered(function () {
  const showMoreVisible = () => {
    const $target = $('#showMoreResults');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 10;
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visible')) {
        $target.data('visible', true);
        pageSession.set('limit', pageSession.get('limit') + pageSession.get('incremente'));
      }
    } else if ($target.data('visible')) {
      $target.data('visible', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.listHistoricCreditDetailOrga.events({
  'click .give-me-more'(event) {
    event.preventDefault();
    const newLimit = pageSession.get('limit') + pageSession.get('incremente');
    pageSession.set('limit', newLimit);
  },
});

Template.listHistoricCreditDetailOrga.helpers({
  logUserActions() {
    const options = {};
    options.limit = pageSession.get('limit');
    options.sort = { createdAt: -1 };
    return LogUserActions.find({ organizationId: Session.get('orgaCibleId') }, options);
  },
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
  },
  isLimit(countActions) {
    return countActions > pageSession.get('limit') || (countActions === pageSession.get('limit'));
  },
  countActions() {
    const options = {};
    options.limit = pageSession.get('limit');
    options.sort = { createdAt: -1 };
    return LogUserActions.find({ organizationId: Session.get('orgaCibleId') }, options).count();
  },
  badgeColorTest() {
    return this.credits > 0 ? 'balanced' : 'dark';
  },
  limit() {
    return pageSession.get('limit');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.creditHistoryStats.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Session.get('orgaCibleId'));
    pageSession.set('scope', 'organizations');
  });

  self.autorun(function () {
    Meteor.call('historicCountCreditOrga', { scopeId: Session.get('orgaCibleId') }, (err, res) => {
      if (err) {
      } else {
      }
    });
    const handle = Meteor.subscribe('creditHistoryStats', Session.get('orgaCibleId'));
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });
});

Template.creditHistoryStats.helpers({
  LogOrgaMonthCredit() {
    return LogOrgaMonthCredit.find({ organizationId: Session.get('orgaCibleId') }, { sort: { year: 1, month: 1 } });
  },
  totalAll() {
    const log = LogOrgaMonthCredit.find({ organizationId: Session.get('orgaCibleId') }, { sort: { year: 1, month: 1 } }).fetch();
    // calcul total avce reduce
    const total = log.reduce((sum, value) => sum + value.total, 0);
    const totalGagner = log.reduce((sum, value) => sum + value.totalGagner, 0);
    const totalDepenser = log.reduce((sum, value) => sum + value.totalDepenser, 0);
    return { total, totalGagner, totalDepenser };
  },
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.logUserActionsAdd.onCreated(function () {
  const template = Template.instance();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);

  this.autorun(function () {
    pageSession.set('citoyenId', Router.current().params.citoyenId);
  });
});

AutoForm.addHooks(['addLogUserActions'], {
  after: {
    method(error) {
      if (!error) {
        Router.go('listMembersDetailCitoyens', { _id: Session.get('orgaCibleId'), citoyenId: pageSession.get('citoyenId') }, { replaceState: true });
      }
    },
  },
  before: {
    method(doc) {
      doc.organizationId = Session.get('orgaCibleId');
      doc.userId = pageSession.get('citoyenId');
      return doc;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(': ', ''));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      }
    }
  },
});

Template.transfertOcecoTibillet.onCreated(function () {
  pageSession.set('error', false);
  this.autorun(function () {
    Session.setPersistent('orgaCibleId', Router.current().params._id);
  });
});

Template.transfertOcecoTibillet.helpers({
  citoyen() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) });
  },
  error() {
    return pageSession.get('error');
  },
});

AutoForm.addHooks(['transfertOcecoTibillet'], {
  after: {
    method(error) {
      if (!error) {
        Router.go('listMembersDetailCitoyens', { _id: Session.get('orgaCibleId'), citoyenId: Meteor.userId() }, { replaceState: true });
      }
    },
  },
  before: {
    method(doc) {
      doc.organizationId = Session.get('orgaCibleId');
      return doc;
    },
  },
  onError(formType, error) {
    if (error && error.reason) {
      pageSession.set('error', error.reason);
      IonToast.show({
        title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason}`,
      });
    }
  },
});
