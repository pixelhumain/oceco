import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { lazy } from '../iolazyload.js';

import './item.html';

Template.scopeItemList.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});

Template.scopeItemList.events({
  'click .favoris-js'(event, instance) {
    event.preventDefault();
    event.stopPropagation();
    // console.log(this);
    // console.log(instance);
    // Session.get('orgaCibleId')
    if (instance.data?.scope === 'projects') {
      Meteor.call('favorisScope', { scope: instance.data.scope, scopeId: this._id.valueOf(), options: { orgaId: Session.get('orgaCibleId') } }, function (err, res) {
        if (err) {
          // console.log(err);
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
          });
        } else {
          IonToast.show({
            template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('favorites')}`,
          });
        }
      });
    }
  }
});

Template.scopeItemListEvent.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});

Template.scopeItemListForm.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});
