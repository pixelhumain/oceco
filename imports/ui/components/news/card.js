/* eslint-disable meteor/no-session */
/* global Session IonPopup IonActionSheet IonToast IonModal */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';

import './card.html';
import { lazy } from '../iolazyload.js';
import { Citoyens } from '../../../api/collection/citoyens.js';
import { Organizations } from '../../../api/collection/organizations.js';
import { CountUserActions, CountActions } from '../../../api/collection/countUserActions.js';

import { pageSession, searchAction } from '../../../api/client/reactive.js';

import { nameToCollection, scrollPagination } from '../../../api/helpers.js';

Template.scopeCard.onRendered(function () {
  lazy.lazyLoad();
  this.autorun(function () {
    if (Template.parentData().profilThumbImageUrl) {
      lazy.lazyLoad();
    }
  });
});

Template.scopeCard.helpers({
  preferenceTrue(value) {
    return !!((value === true || value === 'true'));
  },
  listMembers() {
    // eslint-disable-next-line meteor/no-session
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) }).membersPopMap();
  },
});

Template.scopeCard.events({
  'click .board-users-actions-modal-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      _id: this._id.valueOf(), scope: this.scopeVar(),
    };
    IonModal.open('scopeBoardActionsModal', parentDataContext);
  },
  'click .board-history-actions-modal-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      _id: this._id.valueOf(), scope: this.scopeVar(),
    };
    if (event.currentTarget.dataset && event.currentTarget.dataset.etat) {
      parentDataContext.etat = event.currentTarget.dataset.etat;
    }
    IonModal.open('scopeHistoryActionsModal', parentDataContext);
  },
});

Template.scopeHistoryActionsModal.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  const template = Template.instance();

  this.autorun(function () {
    if (Template.currentData().search) {
      searchAction.setPersistent('search', Template.currentData().search);
    } else {
      searchAction.setPersistent('search', null);
      searchAction.setPersistent('searchHelp', null);
      searchAction.setPersistent('searchTag', null);
      searchAction.setPersistent('searchMilestone', null);
      searchAction.setPersistent('searchHelp', null);
      searchAction.setPersistent('searchMaintenance', null);
      searchAction.setPersistent('searchMaintenanceMe', null);
      searchAction.setPersistent('actionName', null);
      searchAction.setPersistent('searchUser', null);
    }
  }.bind(this));

  this.autorun(function () {
    if (searchAction.get('search') || searchAction.get('searchSort') || Template.currentData().etat || Template.currentData().search) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  this.autorun(function () {
    const dataContext = Template.currentData();
    if (dataContext) {
      template.dataContext = true;
    }
    const paramPub = { scope: dataContext.scope, scopeId: dataContext._id, etat: dataContext.etat ?? 'done', limit: this.limit.get('limit') };
    if (searchAction.get('search')) {
      paramPub.search = searchAction.get('search');
    }
    if (searchAction.get('searchSort')) {
      paramPub.searchSort = searchAction.get('searchSort');
    }
    const handleScope = Meteor.subscribe('directoryListActions', paramPub);
    Tracker.autorun(() => {
      this.ready.set(handleScope.ready());
    });
  }.bind(this));
});

Template.scopeHistoryActionsModal.onRendered(scrollPagination);

Template.scopeHistoryActionsModal.helpers({
  scope() {
    if (Template.currentData().scope) {
      const collection = nameToCollection(Template.currentData().scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Template.currentData()._id) });
      return scopeOne;
    }
    // return undefined;
  },
  isLimit(countActions) {
    return countActions === Template.instance().limit.get('limit');
  },
  search() {
    return searchAction.get('search');
  },
  etat() {
    return Template.currentData().etat ?? 'done';
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  searchMaintenance() {
    return searchAction.get('searchMaintenance') || searchAction.get('searchMaintenanceMe');
  },
  searchMaintenanceMe() {
    return searchAction.get('searchMaintenanceMe');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeHistoryActionsModal.events({
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});


Template.scopeBoardActionsModal.onCreated(function () {
  this.ready = new ReactiveVar();
  this.state = new ReactiveDict();
  this.state.setDefault({
    call: false,
  });

  const self = this;

  this.autorun(function () {
    const dataContext = Template.currentData();
    const handleScope = Meteor.subscribe('scopeUsersActionIndicatorCount', dataContext.scope, dataContext._id);
    Tracker.autorun(() => {
      this.ready.set(handleScope.ready());
    });
  }.bind(this));
});

Template.scopeBoardActionsModal.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
  listMembersCount() {
    // eslint-disable-next-line meteor/no-session
    return CountUserActions.find({ scopeId: Template.currentData()._id, scope: Template.currentData().scope });
  },
  actionsCount() {
    // eslint-disable-next-line meteor/no-session
    return CountActions.findOne({ scopeId: Template.currentData()._id, scope: Template.currentData().scope });
  },
  isCall() {
    return Template.instance().state.get('call');
  },
});

Template.scopeBoardActionsModal.events({
  'click .board-users-actions-update-js'(event, instance) {
    event.preventDefault();
    instance.state.set('call', true);
    Meteor.call('scopeBoardActionsUpdate', {
      scopeId: Template.currentData()._id, scope: Template.currentData().scope,
    }, (error) => {
      if (error) {
        instance.state.set('call', false);
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      } else {
        instance.state.set('call', false);
      }
    });
  },
  'click .history-actions-modal-js'(event) {
    event.preventDefault();
    IonModal.close();
    const parentDataContext = {
      _id: Template.currentData()._id, scope: Template.currentData().scope,
    };
    if (event.currentTarget.dataset && event.currentTarget.dataset.etat) {
      parentDataContext.etat = event.currentTarget.dataset.etat;
    }
    if (event.currentTarget.dataset && event.currentTarget.dataset.search) {
      parentDataContext.search = event.currentTarget.dataset.search;
    } else {
      parentDataContext.search = '';
    }
    IonModal.open('scopeHistoryActionsModal', parentDataContext);
  },
});


Template.scopeBoardActions.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('scopeActionIndicatorCount', dataContext.scope, dataContext._id, 'all');
  });
});

Template.scopeBoardActions.helpers({
  countActionsStatus(status) {
    const dataContext = Template.currentData();
    return Counter.get(`countScopeAction.${dataContext._id}.${dataContext.scope}.${status}`);
  },
});

Template.scopeBoardActionsItem.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('scopeActionIndicatorCount', dataContext.scope, dataContext._id, dataContext.status, dataContext.milestoneId);
  });
});

Template.scopeBoardActionsItem.helpers({
  countActionsStatus(status) {
    const dataContext = Template.currentData();
    const countName = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}.${dataContext.milestoneId}` : `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}`;
    return Counter.get(countName);
  },
  pourActionsStatus(totalStatus, status) {
    const dataContext = Template.currentData();
    const countName = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}.${dataContext.milestoneId}` : `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}`;
    const countTodo = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.todo.${dataContext.milestoneId}` : null;
    const countNameTotalStatus = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.${totalStatus}.${dataContext.milestoneId}` : `countScopeAction.${dataContext._id}.${dataContext.scope}.${totalStatus}`;
    const total = Counter.get(countNameTotalStatus);
    const count = Counter.get(countName);
    if (total === 0) {
      return 0;
    }
    let pourcentage = (100 * count) / total;
    if (status === 'done' && Counter.get(countTodo) === 0) {
      pourcentage = 100;
    }
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.scopeBoardUserActions.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('citoyenLight', dataContext.userId);
    Meteor.subscribe('scopeUserActionIndicatorCount', dataContext.scope, dataContext._id, 'all', dataContext.userId);
  });
});

Template.scopeBoardUserActions.helpers({
  userItem() {
    const dataContext = Template.currentData();
    return Citoyens.findOne({ _id: new Mongo.ObjectID(dataContext.userId) });
  },
  countUserActionsStatus(status, userId) {
    const dataContext = Template.currentData();
    return Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${status}`);
  },
});

Template.scopeBoardUserActions.events({
  'click .searchuser-js'(event) {
    event.preventDefault();
    if (this) {
      const userItem = Template.scopeBoardUserActions.__helpers.get('userItem').call();
      searchAction.set('search', `@${userItem.username}`);
      Router.go('actionsListOrga', { orgaCibleId: Session.get('orgaCibleId'), _id: this._id, scope: this.scope });
    } else {
      searchAction.set('search', null);
    }
  },
});

Template.scopeBoardUserActionsItem.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('scopeUserActionIndicatorCount', dataContext.scope, dataContext._id, dataContext.status, dataContext.userId);
  });
});

Template.scopeBoardUserActionsItem.helpers({
  countUserActionsStatus(status, userId) {
    const dataContext = Template.currentData();
    return Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${status}`);
  },
  pourUserActionsStatus(totalStatus, status, userId) {
    const dataContext = Template.currentData();
    const total = Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${totalStatus}`);
    const count = Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${status}`);
    if (total === 0) {
      return 0;
    }
    const pourcentage = (100 * count) / total;
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.scopeBoardUserActionsItemModal.helpers({
  pourUserActionsStatus(total, count) {
    if (total === 0) {
      return 0;
    }
    const pourcentage = (100 * count) / total;
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.scopeBoardActionsItemModal.helpers({
  pourActionsStatus(total, count, countTodo, status) {
    if (total === 0) {
      return 0;
    }
    let pourcentage = (100 * count) / total;
    if (status === 'done' && countTodo === 0) {
      pourcentage = 100;
    }
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.actionSheet.events({
  'click .action-card-citoyen'(event) {
    event.preventDefault();
    event.stopPropagation();
    // info,description,contact
    IonActionSheet.show({
      titleText: i18n.__('Actions Citoyens'),
      buttons: [
        { text: `${i18n.__('edit info')}` },
        { text: `${i18n.__('edit network')}` },
        { text: `${i18n.__('edit description')}` },
        { text: `${i18n.__('edit address')}` },
        { text: `${i18n.__('edit privacy settings')}` },
        { text: `${i18n.__('edit notification oceco')}` },
        { text: `${i18n.__('change password')}` },
        { text: `${i18n.__('generate token user api')}` },
      ],
      destructiveText: `${i18n.__('Delete my account')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        // console.log('Edit!');
        IonModal.open('_deletemyaccount');
        /* IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete my account'),
          onOk() {
            Meteor.call('deleteMyAccount', { scope: 'events', scopeId: Router.current().params._id }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                Router.go('/');
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        }); */

        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        if (index === 1) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'network' });
        }
        if (index === 2) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        if (index === 3) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        }
        if (index === 4) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        }
        if (index === 5) {
          // console.log('Edit!');
          Router.go('ocecoCitoyenEdit', { _id: Router.current().params._id });
        }
        if (index === 6) {
          // console.log('Edit!');
          Router.go('ocecoCitoyenChangePassword', { _id: Router.current().params._id });
        }
        if (index === 7) {
          // console.log('Edit!');
          IonModal.open('generateTokenCitoyen');
        }
        return true;
      },
    });
  },
  'click .action-card-events'(event) {
    event.preventDefault();
    event.stopPropagation();
    // info,description,contact
    IonActionSheet.show({
      titleText: i18n.__('Actions Events'),
      buttons: [
        { text: `${i18n.__('edit info')}` },
        { text: `${i18n.__('edit network')}` },
        { text: `${i18n.__('edit description')}` },
        { text: `${i18n.__('edit address')}` },
        { text: `${i18n.__('edit dates')}` },
      ],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        // console.log('Edit!');
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this event'),
          onOk() {
            Meteor.call('deleteElementAdmin', { scope: 'events', scopeId: Router.current().params._id }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                Router.go('/');
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        if (index === 1) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'network' });
        }
        if (index === 2) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        if (index === 3) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        }
        if (index === 4) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'when' });
        }
        /* if (index === 5) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        } */
        return true;
      },
    });
  },
  'click .action-card-organizations'(event) {
    event.preventDefault();
    event.stopPropagation();
    // info,description,contact
    const self = this;
    const arrayButtons = [
      { name: 'edit-info', text: `${i18n.__('edit info')}` },
      { name: 'edit-network', text: `${i18n.__('edit network')}` },
      { name: 'edit-description', text: `${i18n.__('edit description')}` },
      { name: 'edit-address', text: `${i18n.__('edit address')}` },
    ];
    if (self.isAdmin() && self.isAdmin() === true && self.isChat() && self.isOneChat()) {
      arrayButtons.push({ name: 'synchronize-user-chat-admin', text: `${i18n.__('synchronize members on the chat channel')}` });
    }
    IonActionSheet.show({
      titleText: i18n.__('Actions Organizations'),
      buttons: arrayButtons,
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 'edit-info') {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        if (index === 'edit-network') {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'network' });
        }
        if (index === 'edit-description') {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        if (index === 'edit-address') {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        }
        // if (index === 'edit-preferences') {
        //   // console.log('Edit!');
        //   Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        // }
        if (index === 'synchronize-user-chat-admin') {
          if (self.isAdmin() && self.isAdmin() === true && self.isChat() && self.isOneChat()) {
            IonPopup.confirm({
              title: i18n.__('Chat'),
              template: i18n.__('Synchronize the members of the organization and switch the administrators to moderator if they are present on the chat'),
              onOk() {
                Meteor.call('synchroMembersChat', { scope: 'organizations', id: Router.current().params._id }, (error) => {
                  if (error) {
                    IonToast.show({
                      title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                    });
                  } else {
                    IonToast.show({
                      template: i18n.__('Synchronize members chat'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Organization')}`,
                    });
                  }
                });
              },
              onCancel() {
              },
              cancelText: i18n.__('no'),
              okText: i18n.__('yes'),
            });
          }
        }
        return true;
      },
    });
  },
  'click .action-card-projects'(event) {
    event.preventDefault();
    event.stopPropagation();
    // info,description,contact
    const self = this;
    const arrayButtons = [
      { name: 'edit-info', text: `${i18n.__('edit info')}` },
      // { text: `${i18n.__('edit network')}` },
      { name: 'edit-description', text: `${i18n.__('edit description')}` },
      // { text: `${i18n.__('edit address')}` },
      { name: 'edit-dates', text: `${i18n.__('edit dates')}` },
    ];
    if (Session.get('settingOceco') && Session.get('settingOceco').milestonesProject && Session.get('orgaCibleId') && Session.get(`isAdmin${Session.get('orgaCibleId')}`)) {
      arrayButtons.push({ name: 'edit-config-oceco', text: `${i18n.__('edit config oceco')}` });
    }
    if (Session.get('settingOceco') && Session.get('settingOceco').gitProject && Session.get('orgaCibleId') && Session.get(`isAdmin${Session.get('orgaCibleId')}`)) {
      arrayButtons.push({ name: 'edit-config-git-oceco', text: `${i18n.__('edit config git oceco')}` });
    }
    if (self.isAdmin() && self.isAdmin() === true && self.isChat() && self.isOneChat()) {
      arrayButtons.push({ name: 'synchronize-user-chat-admin', text: `${i18n.__('synchronize contributors on the chat channel')}` });
    }
    IonActionSheet.show({
      titleText: i18n.__('Actions Projects'),
      buttons: arrayButtons,
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        // console.log('Edit!');
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this project'),
          onOk() {
            Meteor.call('deleteElementAdmin', { scope: 'projects', scopeId: Router.current().params._id }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Project')}`,
                });
                Router.go('/');
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 'edit-info') {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        /* if (index === 1) {
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'network' });
        } */
        if (index === 'edit-description') {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        /* if (index === 3) {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        } */
        if (index === 'edit-dates') {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'when' });
        }
        /* if (index === 3) {
          // console.log('Edit!');
          // Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        } */
        if (index === 'edit-config-oceco') {
          // console.log('Edit!');
          Router.go('ocecoProjectEdit', { _id: Router.current().params._id });
        }
        if (index === 'edit-config-git-oceco') {
          // console.log('Edit!');
          Router.go('ocecoProjectGitEdit', { _id: Router.current().params._id });
        }
        if (index === 'synchronize-user-chat-admin') {
          if (self.isAdmin() && self.isAdmin() === true && self.isChat() && self.isOneChat()) {
            IonPopup.confirm({
              title: i18n.__('Chat'),
              template: i18n.__('Synchronize project contributors and switch administrators to moderators if they are present on the chat'),
              onOk() {
                Meteor.call('synchroMembersChat', { scope: 'projects', id: Router.current().params._id }, (error) => {
                  if (error) {
                    IonToast.show({
                      title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                    });
                  } else {
                    IonToast.show({
                      template: i18n.__('Synchronize contributors chat'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Project')}`,
                    });
                  }
                });
              },
              onCancel() {
              },
              cancelText: i18n.__('no'),
              okText: i18n.__('yes'),
            });
          }
        }
        return true;
      },
    });
  },
});

Template.scopeFileUpload.onRendered(function () {
  const self = this;
  const template = Template.instance();

  pageSession.set('drop', false);
  pageSession.set('fileCount', 0);
  pageSession.set('fileArray', []);

  const dropZone = document.querySelector('.cardDrop');
  const hiddenPasteArea = document.getElementById('hiddenPasteArea');;

  // Focus automatiquement sur l'élément caché quand .cardDrop est cliqué
  dropZone.addEventListener('click', () => {
    hiddenPasteArea.focus();
  });

  hiddenPasteArea.addEventListener('paste', (e) => {
    console.log('fileInput', e);
    if (e.clipboardData.files) {
      if (e.clipboardData.files.length === 1) {
        if (e.clipboardData.files[0].type === 'image/png' || e.clipboardData.files[0].type === 'image/jpg' || e.clipboardData.files[0].type === 'image/jpge' || e.clipboardData.files[0].type === 'image/jpeg') {
          // const fileInput = template.find('#file-upload');
          const fileInput = document.getElementById('file-upload');



          if (fileInput) {
            fileInput.files = e.clipboardData.files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload').trigger('change');
          }
        }
      }
    }
  });


  if (dropZone) {
    const hoverClassName = 'drop';

    dropZone.addEventListener('dragenter', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragover', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragleave', function (e) {
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
    });

    dropZone.addEventListener('drop', (e) => {
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
      const { files } = e.dataTransfer;
      if (files) {
        if (files.length === 1) {
          if (files[0].type === 'image/png' || files[0].type === 'image/jpg' || files[0].type === 'image/jpge' || files[0].type === 'image/jpeg') {
            const fileInput = template.find('#file-upload');
            fileInput.files = files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload').trigger('change');
          }
        }
      }
    });
  }
});
