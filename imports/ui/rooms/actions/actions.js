/* eslint-disable no-shadow */
/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
/* eslint-disable meteor/no-session */
/* eslint-disable consistent-return */
/* global AutoForm Session IonActionSheet IonToast _ PhotoViewer Platform */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';
import i18n from 'meteor/universe:i18n';
import { IonPopup } from 'meteor/meteoric:ionic';
import { MeteorCameraUI } from 'meteor/aboire:camera-ui';
import { $ } from 'meteor/jquery';
import { moment } from 'meteor/momentjs:moment';
import { Accounts } from 'meteor/accounts-base';
import { HTTP } from 'meteor/jkuester:http';
import { saveAs } from 'file-saver';
import Sortable from 'sortablejs';

import { Actions } from '../../../api/collection/actions.js';
import { Events } from '../../../api/collection/events.js';
import { Organizations } from '../../../api/collection/organizations.js';
import { Projects } from '../../../api/collection/projects.js';

import { nameToCollection } from '../../../api/helpers.js';

import { projectsCountOrga, eventsCountOrga } from '../../../api/helpersOrga.js';

import { pageSession, searchAction } from '../../../api/client/reactive.js';

import { Pomodoro } from '../../../api/client/podomoro.js';
import { Citoyens } from '../../../api/collection/citoyens.js';

import './actions.html';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;

Template.detailActions.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.ready = new ReactiveVar();
  this.autorun(function () {

    // verifier si membre orga ?

    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
        // verfier si membre orga en data context ?
        const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
        if (!isMember) {
          // fermer modal avant redirection
          IonModal.close();
          Router.go('home');
        }
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
        // verfier si membre orga en data context ?
        const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
        if (!isMember) {
          // fermer modal avant redirection
          IonModal.close();
          Router.go('home');
        }
      }
      template.dataContext = true;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
      if (Router.current().params.orgaCibleId) {
        template.orgaCibleId = Router.current().params.orgaCibleId;
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
        // verfier si membre orga ?
        const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
        if (!isMember) {
          Router.go('home');
        }
      } else if (Router.current().params.scope === 'organizations') {
        template.orgaCibleId = Router.current().params._id;
        Session.setPersistent('orgaCibleId', Router.current().params._id);
        // verfier si membre orga ?
        const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
        if (!isMember) {
          Router.go('home');
        }
      }
      template.dataContext = false;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
    pageSession.set('dataContext', template.dataContext);

    if (template.scope && template._id && template.roomId && template.actionId) {

      Tracker.autorun(() => {
        const handle = Meteor.subscribe('detailActionsOpti', template.scope, template._id, template.roomId, template.actionId);

        if (pageSession.get('reloadSub')) {
          handle.stop();
          pageSession.set('reloadSub', false);
        }

        if (handle.ready()) {
          if (!template.orgaCibleId) {
            const countOrga = Organizations.find({}).count();
            const orgaNoName = Organizations.find({
              name: { $exists: false },
            });

            if (countOrga > 1) {
              // console.log('news news.js 117 countOrga', countOrga, Session.get('orgaCibleId'));
              // 1 de plus quand faut changer

              if (template.scope === 'projects') {
                const project = `links.projects.${template._id}`;
                const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
                // verfier si membre orga ?
                const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
                if (!isMember) {
                  Router.go('home');
                }
              } else if (template.scope === 'events') {
                const event = `links.events.${template._id}`;
                const projectOne = Projects.find({ [event]: { $exists: 1 } });
                eventsCountOrga({
                  _id: template._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                });
                // verfier si membre orga ?
                const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
                if (!isMember) {
                  Router.go('home');
                }
              } else if (template.scope === 'actions') {
                const actionObjectId = new Mongo.ObjectID(template._id);
                const actionOne = Actions.findOne({ _id: actionObjectId });
                const parentObjectId = new Mongo.ObjectID(actionOne.parentId);

                if (actionOne.parentType === 'events') {
                  // events
                  const projectOne = Projects.find({ _id: parentObjectId });
                  eventsCountOrga({
                    _id: template._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                  });
                } else if (actionOne.parentType === 'projects') {
                  // projects
                  const project = `links.projects.${actionOne.parentId}`;
                  const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                  projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
                } else if (actionOne.parentType === 'organizations') {
                  Session.setPersistent('orgaCibleId', actionOne.parentId);
                } else if (actionOne.parentType === 'citoyens') {
                  // citoyens
                }
              }
            } else if (countOrga === 1 && !Session.get('orgaCibleId')) {
              // console.log('news news.js 144 countOrga === 1', Session.get('orgaCibleId'));
              template.orgaCibleId = Organizations.findOne({})._id.valueOf();
              Session.setPersistent('orgaCibleId', template.orgaCibleId);
              // verfier si membre orga ?
              const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
              if (!isMember) {
                Router.go('home');
              }
            }
          }

          this.ready.set(handle.ready());

        }

      });
    }
  }.bind(this));
});


Template.detailActions.helpers({
  scope() {
    if (Template.instance().scope && pageSession.get('actionId')) {
      const collection = nameToCollection(Template.instance().scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      if (scopeOne && pageSession.get('actionId')) {
        scopeOne.roomId = Template.instance().roomId;
        scopeOne.actionId = Template.instance().actionId;
        scopeOne.dataContext = Template.instance().dataContext;
        scopeOne.orgaCibleId = Template.instance().orgaCibleId;
      }
      return scopeOne;
    }
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  statusPodomoroActual() {
    return this.podomoro && this.podomoro[Meteor.userId()] && this.podomoro[Meteor.userId()].statusPodomoro ? this.podomoro[Meteor.userId()].statusPodomoro : null;
  },
});

Template.detailActionsModal.inheritsHelpersFrom('detailActions');
Template.detailActionsModal.inheritsEventsFrom('detailActions');
Template.detailActionsModal.inheritsHooksFrom('detailActions');

Template.taskList.onRendered(function () {
  const instance = this;

  // Fonction d'initialisation de SortableJS
  const container = instance.find('.tasks-container');
  instance.sortableInstance = Sortable.create(container, {
    animation: 150,
    forceFallback: true,
    handle: '.handle-icon',
    onChoose(e) {
      e.item.placeholder = document.createComment('sort-placeholder')
      e.item.after(e.item.placeholder)
    },
    onSort(e) {
      const newOrder = Array.from(e.to.children).map(item => item.getAttribute('data-task-id'));
      // put back in original location
      if (e.item.placeholder) {
        e.item.placeholder.replaceWith(e.item)
        delete e.item.placeholder
      }

      Meteor.call('updateTasksOrder', { id: instance.data._id.valueOf(), newOrder }, (error) => {
        if (error) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
          });
        }
      });
    }
  });
});

Template.taskList.onDestroyed(function () {
  this.sortableInstance.destroy();
});

Template.taskList.helpers({
  filteredTasks() {
    const filter = pageSession.get('task') ? pageSession.get('task').toLowerCase() : null;
    // Supposons que "this.tasks" soit le tableau des tâches disponibles
    if (filter && this.tasks) {
      return this.tasks.filter(task => task.task.toLowerCase().indexOf(filter) !== -1);
    }
    return this.tasks ? this.tasks : [];
  },
  totalTask() {
    const totalTask = this.tasks && this.tasks.length > 0 ? this.tasks.length : 0;
    return totalTask;
  },
  totalChecked() {
    const totalChecked = this.tasks && this.tasks.length > 0 && this.tasks.filter((k) => k.checked === true).length ? this.tasks.filter((k) => k.checked === true).length : 0;
    return totalChecked;
  },
  totalUnChecked() {
    const totalUnChecked = this.tasks && this.tasks.length > 0 && this.tasks.filter((k) => k.checked === false).length ? this.tasks.filter((k) => k.checked === false).length : 0;
    return totalUnChecked;
  },
  pourTaskChecked() {
    const totalTask = this.tasks && this.tasks.length > 0 ? this.tasks.length : 0;
    const totalChecked = this.tasks && this.tasks.length > 0 && this.tasks.filter((k) => k.checked === true).length ? this.tasks.filter((k) => k.checked === true).length : 0;
    if (totalTask === 0) {
      return 0;
    }
    const pourcentage = (100 * totalChecked) / totalTask;
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.taskAdd.onCreated(function () {
  this.state = new ReactiveDict();
  this.state.setDefault({
    call: false,
  });
  pageSession.set('task', null);
  pageSession.set('taskId', null);
});

Template.taskAdd.onRendered(function () {
  const currentData = Template.currentData();
  const target = document.querySelector('input[name="task"]');
  if (target) {
    target.addEventListener('paste', (event) => {
      const paste = (event.clipboardData || window.clipboardData).getData('text');
      const tasks = paste.split(/\r\n|\r|\n/);
      if (tasks && tasks.length > 1) {
        event.preventDefault();
        tasks.forEach((taskValue) => {
          if (!pageSession.get('taskId') && taskValue.trim()) {
            const task = { id: currentData._id.valueOf() };
            task.task = taskValue.trim();
            Meteor.call('insertActionTask', task, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          }
        });
      }
    });
  }
});

Template.taskAdd.helpers({
  isCall() {
    return Template.instance().state.get('call');
  },
  task() {
    return pageSession.get('task');
  },
  taskId() {
    return pageSession.get('taskId');
  },
});

Template.taskAdd.events({
  'submit .form-task-js'(event, instance) {
    event.preventDefault();
    instance.state.set('call', true);
    if (pageSession.get('taskId')) {
      const task = { id: this._id.valueOf(), taskId: pageSession.get('taskId') };
      const taskValue = event.target && event.target.task && event.target.task.value && event.target.task.value.trim() ? event.target.task.value.trim() : null;
      if (taskValue) {
        task.task = taskValue;
        Meteor.call('updateActionTask', task, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
            });
          }
          instance.state.set('call', false);
          pageSession.set('task', null);
          pageSession.set('taskId', null);
        });
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Unspecified task')}`,
        });
        instance.state.set('call', false);
        pageSession.set('task', null);
        pageSession.set('taskId', null);
      }
    } else {
      const task = { id: this._id.valueOf() };
      const taskValue = event.target && event.target.task && event.target.task.value && event.target.task.value.trim() ? event.target.task.value.trim() : null;
      if (taskValue) {
        task.task = taskValue;
        Meteor.call('insertActionTask', task, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
            });
          }
          instance.state.set('call', false);
          pageSession.set('task', null);
        });
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Unspecified task')}`,
        });
        instance.state.set('call', false);
      }
    }
  },
  // Pressing Ctrl+Enter should submit the form
  'keydown .form-task-js'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/change input[name="task"]': _.debounce((event) => {
    if (event.currentTarget.value) {
      pageSession.set('task', event.currentTarget.value);
    } else {
      pageSession.set('task', null);
    }
  }),
});

Template.taskItem.onCreated(function () {
  this.editing = new ReactiveVar(false);
});

Template.taskItem.helpers({
  isEditing() {
    return Template.instance().editing.get();
  },
  taskCheckedUser(userId) {
    if (userId) {
      return Citoyens.findOne({ _id: new Mongo.ObjectID(userId) }, { fields: { name: 1 } }).name;
    }
  },
  taskContributor(objectContributor) {
    const arrayContributor = objectContributor && Object.keys(objectContributor).length > 0 ? Object.keys(objectContributor) : null;
    if (arrayContributor) {
      const arrayIds = arrayContributor.map((contributor) => new Mongo.ObjectID(contributor));
      return Citoyens.find({ _id: { $in: arrayIds } }, { fields: { name: 1 } });
    }
  },
  taskIsContributor() {
    return this.task && this.task.contributors && this.task.contributors[Meteor.userId()];
  },
});

Template.taskItem.events({
  'blur .edit-task-input-js'(event, instance) {
    const newValue = event.currentTarget.value.trim();
    if (newValue && newValue !== instance.data.task.task) {
      // Appeler votre méthode de mise à jour
      Meteor.call('updateActionTask', { id: instance.data.action._id.valueOf(), taskId: instance.data.task.taskId, task: newValue }, (error) => {
        if (error) {
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
          });
        }
      });
    }
    instance.editing.set(false);
  },
  // Vous pouvez aussi gérer la validation avec "Enter" :
  'keydown .edit-task-input-js'(event, instance) {
    if (event.keyCode === 13) {
      event.preventDefault();
      event.currentTarget.blur();
    } else if (event.keyCode === 27) { // Touche "Échap"
      event.preventDefault();
      instance.editing.set(false); // Annuler l'édition
    }
  },
  // si doubleclick sur .item-text alors on passe en mode édition
  'dblclick .item-text'(event, instance) {
    event.preventDefault();
    event.stopPropagation();
    instance.editing.set(true);
    Meteor.setTimeout(() => {
      instance.$('.edit-task-input-js').focus();
      // Optionnel : vous pouvez scroller jusqu'à l'élément si nécessaire
      instance.$('.edit-task-input-js')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
    }, 100);
  },
  'contextmenu .item-handle'(event, instance) {
    event.preventDefault();
    instance.$('.task-action-js')[0].click();
  },
  'click .task-checked-js'(event) {
    if (this.action.isContributors()) {
      if (event.currentTarget.id) {
        if (this.task.contributors) {
          if (!this.task.contributors[Meteor.userId()]) {
            event.currentTarget.checked = false;
            IonToast.show({
              title: i18n.__('Warning'), position: 'top', type: 'warning', timeOut: 5000, showClose: true, template: `<i class="icon fa fa-warning"></i> ${i18n.__('You are not assigned to this task')}`,
            });
          } else {
            const task = { id: this.action._id.valueOf(), taskId: event.currentTarget.id, checked: event.currentTarget.checked };
            Meteor.call('checkedActionTask', task, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          }
        } else {
          if (this.action.answerId) {
            event.currentTarget.checked = false;
          }
          const task = { id: this.action._id.valueOf(), taskId: event.currentTarget.id, checked: event.currentTarget.checked };
          Meteor.call('checkedActionTask', task, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            }
          });
        }
      }
    } else {
      event.currentTarget.checked = false;
      IonToast.show({
        title: i18n.__('Warning'), position: 'top', type: 'warning', timeOut: 5000, showClose: true, template: `<i class="icon fa fa-warning"></i> ${i18n.__('You must participate in the action to be able to perform tasks')}`,
      });
    }
  },
  'click .task-action-js'(event, instance) {
    event.preventDefault();
    const { task } = this;
    const { action } = this;
    const sheetObjet = {
      titleText: i18n.__('Task actions'),
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this task'),
          onOk() {
            Meteor.call('deleteActionTask', { id: action._id.valueOf(), taskId: task.taskId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
    };
    sheetObjet.buttons = [];
    if (!action.answerId) {
      sheetObjet.buttons.push({ name: 'edit', text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` });
      if (action.parentType !== 'citoyens') {
        sheetObjet.buttons.push({ name: 'assign', text: `${i18n.__('Assign')} <i class="icon ion-ios-people"></i>` });
        if (task.contributors && task.contributors[Meteor.userId()]) {
          sheetObjet.buttons.push({ name: 'unassignMe', text: `${i18n.__('Unassign me the task')} <i class="icon ion-ios-person"></i>` });
        } else {
          sheetObjet.buttons.push({ name: 'assignMe', text: `${i18n.__('Assign me the task')} <i class="icon ion-ios-person"></i>` });
        }
      }
      // si tasks.length > 1
      if (action.tasks && action.tasks.length > 1) {
        // si task est pas le premier
        if (action.tasks[0].taskId !== task.taskId) {
          sheetObjet.buttons.push({ name: 'upTask', text: `${i18n.__('Move task up')} <i class="icon ion-arrow-up-a"></i>` });
        }
        // si task est pas le dernier
        if (action.tasks[action.tasks.length - 1].taskId !== task.taskId) {
          sheetObjet.buttons.push({ name: 'downTask', text: `${i18n.__('Move task down')} <i class="icon ion-arrow-down-a"></i>` });
        }
      }
    }
    if (typeof task.credits !== 'undefined' && task.checked && task.checkedUserId && !task.payed) {
      sheetObjet.buttons.push({ name: 'pay', text: `${i18n.__('answers.pay')} <i class="icon fa fa-euro"></i>` });
    }
    sheetObjet.buttonClicked = (index) => {
      if (index === 'upTask') {
        Meteor.call('orderTask', { id: this.action._id.valueOf(), taskId: task.taskId, action: 'up' }, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
            });
          }
        });
      }
      if (index === 'downTask') {
        Meteor.call('orderTask', { id: this.action._id.valueOf(), taskId: task.taskId, action: 'down' }, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
            });
          }
        });
      }
      if (index === 'edit') {
        // edit pas pareil si estimationId
        instance.editing.set(true);
        Meteor.setTimeout(() => {
          instance.$('.edit-task-input-js').focus();
          // Optionnel : vous pouvez scroller jusqu'à l'élément si nécessaire
          instance.$('.edit-task-input-js')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
        }, 100);
      }
      if (index === 'assign') {
        // if (this.isAdmin) {
        const parentDataContext = {
          orgaCibleId: Session.get('orgaCibleId'), _id: this.action.parentId, scope: this.action.parentType, roomId: this.action.idParentRoom, actionId: this.action._id.valueOf(), taskId: task.taskId,
        };
        IonModal.open('assignMembersTaskModal', parentDataContext);
        // }
      }
      if (index === 'assignMe') {
        // if (this.isAdmin) {
        // const parentDataContext = {
        //   orgaCibleId: Session.get('orgaCibleId'), _id: this.action.parentId, scope: this.action.parentType, roomId: this.action.idParentRoom, actionId: this.action._id.valueOf(), taskId: task.taskId,
        // };
        Meteor.call('assignMemberTaskActionRooms', { id: this.action._id.valueOf(), taskId: task.taskId, memberId: Meteor.userId() }, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
            });
          }
        });
        // }
      }
      if (index === 'unassignMe') {
        // if (this.isAdmin) {
        const orgId = Session.get('orgaCibleId');
        Meteor.call('exitTaskAction', {
          id: action._id.valueOf(), taskId: task.taskId, orgId,
        }, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
            });
          }
        });
        // }
      }
      if (index === 'pay') {
        if (task.checked && task.checkedUserId && !task.payed) {
          const insert = {};
          insert._id = action.answerId;
          insert.modifier = {};
          insert.modifier.$set = {};
          insert.modifier.$set.beneficiaryId = task.checkedUserId;
          insert.modifier.$set.amount = parseInt(task.credits);
          insert.modifier.$set.parentType = 'forms';
          // insert.modifier.$set.parentId = pageSession.get('scopeId');
          insert.modifier.$set.answerId = action.answerId;
          // insert.modifier.$set.keyDepense = depense.key.toString();
          insert.modifier.$set.actionKey = action._id.valueOf();
          insert.modifier.$set.taskKey = task.taskId;
          IonPopup.confirm({
            title: i18n.__('answers.pay'),
            template: i18n.__('answers.pay ?'),
            onOk() {
              if (task.contributors) {
                const keysContributors = Object.keys(task.contributors);
                const CountContributors = keysContributors && keysContributors.length > 0 ? keysContributors.length : 0;
                Object.keys(task.contributors).forEach((key) => {
                  insert.modifier.$set.beneficiaryId = key;

                  const credits = Math.round((parseInt(task.credits) / CountContributors) * 100) / 100;
                  if (task.checkedUserId === key) {
                    const reste = parseInt(task.credits) - (credits * CountContributors);
                    insert.modifier.$set.amount = credits + reste;
                  } else {
                    insert.modifier.$set.amount = credits;
                  }

                  Meteor.call('insertDepenseAnswerPayement', { ...insert }, (error) => {
                    if (error) {
                      IonToast.show({
                        title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                      });
                    }
                  });
                });
              } else {
                Meteor.call('insertDepenseAnswerPayement', { ...insert }, (error) => {
                  if (error) {
                    IonToast.show({
                      title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                    });
                  }
                });
              }
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
        }
      }
      return true;
    };
    IonActionSheet.show(sheetObjet);
  },
  'click .task-action-assignme-js'(event) {
    event.preventDefault();
    const { task } = this;
    const { action } = this;

    Meteor.call('assignMemberTaskActionRooms', { id: action._id.valueOf(), taskId: task.taskId, memberId: Meteor.userId() }, (error) => {
      if (error) {
        // instance.state.set('call', false);
        // IonPopup.alert({ template: i18n.__(error.reason) });
      }
    });
  },
  'click .task-action-unassignme-js'(event) {
    event.preventDefault();
    const { task } = this;
    const { action } = this;
    const orgId = Session.get('orgaCibleId');
    Meteor.call('exitTaskAction', {
      id: action._id.valueOf(), taskId: task.taskId, orgId,
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
});

Template.buttonsActions.events({
  'click .action-edit-modal-js'(event) {
    event.preventDefault();
    if (this.isAdmin || this.isCreator) {
      const parentDataContext = {
        orgaCibleId: Session.get('orgaCibleId'), _id: this._id, scope: this.scope, roomId: this.roomId, actionId: this.actionId,
      };
      IonModal.open('actionsEditModal', parentDataContext);
    }
  },
  'click .action-assign-modal-js'(event) {
    event.preventDefault();
    if (this.isAdmin) {
      const parentDataContext = {
        orgaCibleId: Session.get('orgaCibleId'), _id: this._id, scope: this.scope, roomId: this.roomId, actionId: this.actionId,
      };
      IonModal.open('assignMembersModal', parentDataContext);
    }
  },
});

Template.buttonActionItem.onCreated(function () {
  this.state = new ReactiveDict();
  this.state.setDefault({
    call: false,
  });
});

Template.buttonActionItem.helpers({
  isCall() {
    return Template.instance().state.get('call');
  },
});

Template.buttonActionItem.events({
  'click .action-action-js'(event, instance) {
    event.preventDefault();
    let scope;
    let scopeId;
    let actionId;
    if (this.scope && this.scopeId && this.actionId) {
      scope = this.scope;
      scopeId = this.scopeId;
      actionId = this.actionId;
    } else {
      scope = pageSession.get('scope');
      scopeId = pageSession.get('scopeId');
      actionId = pageSession.get('actionId');
    }
    instance.state.set('call', true);
    const action = $(event.currentTarget).data('action');
    Meteor.call('actionsType', {
      parentType: scope, parentId: scopeId, type: 'actions', id: actionId, name: 'status', value: action,
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      } else {
        if (pageSession.get('dataContext') && action === 'disabled' && Router.current().route.options && Router.current().route.options.template === 'listActionsAgenda') {
          pageSession.set('removeCalendarActionId', actionId);
        }
        if (action === 'todo') {
          pageSession.set('reloadSub', true);
        }
      }
      instance.state.set('call', false);
    });
  },
});

Template.fileInfoUpload.helpers({
  fileCount() {
    return pageSession.get('fileCount');
  },
  fileSize() {
    return pageSession.get('fileSize');
  },
  fileName() {
    return pageSession.get('fileName');
  },
  fileArray() {
    // console.log(pageSession.get('fileArray'));
    return pageSession.get('fileArray');
  },
});

Template.scopeFileUploadActions.onRendered(function () {
  const self = this;
  const template = Template.instance();

  pageSession.set('drop', false);
  pageSession.set('fileCount', 0);
  pageSession.set('fileArray', []);

  /* this.autorun(function () {
    if (pageSession.get('fileName')) {
      const arrayFiles = pageSession.get('fileArray');
      const newArr = arrayFiles.filter(name => name !== pageSession.get('fileName'));
      pageSession.set('fileArray', newArr);
    }
  }); */

  const dropZone = document.querySelector('.detailActionsDrop');
  const hiddenPasteArea = document.getElementById('hiddenPasteAreaAction');;

  // Focus automatiquement sur l'élément caché quand .cardDrop est cliqué
  dropZone.addEventListener('click', () => {
    hiddenPasteArea.focus();
  });

  hiddenPasteArea.addEventListener('paste', (e) => {
    // file-upload-action
    // console.log(e.clipboardData.files);
    // console.log('paste', e.clipboardData);
    console.log('paste', e);
    if (e.clipboardData.files) {
      if (e.clipboardData.files.length === 1) {
        if (e.clipboardData.files[0].type === 'image/png' || e.clipboardData.files[0].type === 'image/jpg' || e.clipboardData.files[0].type === 'image/jpge' || e.clipboardData.files[0].type === 'image/jpeg') {
          const fileInput = template.find('#file-upload-action');
          if (fileInput) {
            fileInput.files = e.clipboardData.files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload-action').trigger('change');
          }
        } else {
          const fileInput = template.find('#file-doc-upload-action');
          fileInput.files = e.clipboardData.files;
          // console.log(fileInput);
          pageSession.set('drop', true);
          self.$('#file-doc-upload-action').trigger('change');
        }
      } else if (e.clipboardData.files.length > 1) {
        const fileInput = template.find('#file-doc-upload-action');
        fileInput.files = e.clipboardData.files;
        // console.log(fileInput);
        pageSession.set('drop', true);
        self.$('#file-doc-upload-action').trigger('change');
      }
    }
  });

  if (dropZone) {
    const hoverClassName = 'drop';

    dropZone.addEventListener('dragenter', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragover', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragleave', function (e) {
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
    });

    dropZone.addEventListener('drop', (e) => {
      // file-upload-action
      // console.log(e.clipboardData.files);
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
      // console.log('drop', e.clipboardData);
      // console.log('drop', e.dataTransfer);
      const { files } = e.dataTransfer;
      if (files) {
        /* pageSession.set('fileCount', files.length);
        const arrayFile = Object.keys(files).map((k) => {
          return files.item(k).name;
        });
        pageSession.set('fileArray', arrayFile); */
        if (files.length === 1) {
          if (files[0].type === 'image/png' || files[0].type === 'image/jpg' || files[0].type === 'image/jpge' || files[0].type === 'image/jpeg') {
            const fileInput = template.find('#file-upload-action');
            fileInput.files = files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload-action').trigger('change');
          } else {
            const fileInput = template.find('#file-doc-upload-action');
            fileInput.files = files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-doc-upload-action').trigger('change');
          }
        } else if (files.length > 1) {
          const fileInput = template.find('#file-doc-upload-action');
          fileInput.files = files;
          // console.log(fileInput);
          pageSession.set('drop', true);
          self.$('#file-doc-upload-action').trigger('change');
        }
      }
    });
  }
});

Template.detailViewActions.events({
  'click .action-modal-comment-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      _id: this.parentId, scope: this.parentType, roomId: this.idParentRoom, actionId: this._id.valueOf(),
    };
    IonModal.open('actionsDetailCommentsModal', parentDataContext);
  },
  'click .action-modal-message-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      _id: this.parentId, scope: this.parentType, roomId: this.idParentRoom, actionId: this._id.valueOf(),
    };
    IonModal.open('pageMessagesModal', parentDataContext);
  },
  'click .tracking-js'(event) {
    event.preventDefault();
    const tracking = this.tracking !== true;
    Meteor.call('trackingAction', { actionId: this._id.valueOf(), tracking }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Tracking action'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__(tracking ? 'tracking this action' : 'untracking this action')}`,
        });
      }
    });
  },
  'click .action-doc-admin-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('Document')}: ${self.name}`,
      buttons: [],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this document'),
          onOk() {
            // name, parentId, parentType, path, id, actionId
            // console.log({ parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: self.name, path: self.moduleId, id: self._id.valueOf(), actionId: pageSession.get('actionId') })
            Meteor.call('actionDocumentDelete', {
              parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: self.name, path: self.moduleId, id: self._id.valueOf(), actionId: pageSession.get('actionId'),
            }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          /* Meteor.call('changeRoleContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id.valueOf(), role: 'contributor' }, (error) => {
            if (error) {
              IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
            }
          }); */
        }
        return true;
      },
    });
  },
  'click .action-image-admin-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('Image')}: ${self.name}`,
      buttons: [],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: 'Delete this image ?',
          onOk() {
            // name, parentId, parentType, path, id, actionId
            // console.log({ parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: self.name, path: self.moduleId, id: self._id.valueOf(), actionId: pageSession.get('actionId') })
            Meteor.call('actionImageDelete', {
              parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: self.name, path: self.moduleId, id: self._id.valueOf(), actionId: pageSession.get('actionId'),
            }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          /* Meteor.call('changeRoleContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id.valueOf(), role: 'contributor' }, (error) => {
            if (error) {
              IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
            }
          }); */
        }
        return true;
      },
    });
  },
  'click .search-tags-list-js'(event) {
    event.preventDefault();
    searchAction.set('search', `#${this}`);
    Router.go('home');
  },
  'click .play-js'() {
    const self = this;
    const secondes = Pomodoro.getSecondes();
    const lastPlayedTask = Pomodoro.podomoroSessionGet('lastPlayedTask');
    const totalSecondes = Pomodoro.getEndTime();
    const secondesEcoule = totalSecondes - secondes;
    // const secondesEcouleStr = moment.utc(moment.duration(secondesEcoule, 'seconds').asMilliseconds()).format('HH:mm:ss');
    if (lastPlayedTask !== this._id.valueOf()) {
      if (secondes) {
        Meteor.call('podomoroActionStatus', { actionId: this._id.valueOf(), statusPodomoro: 'pausedInt', secondes }, (err) => {
          if (err) {
            IonPopup.alert({ template: i18n.__(err.reason) });
          } else {
            Pomodoro.pause();
            IonPopup.confirm({
              title: `Ajouter le temps passé sur l'action ? (${secondesEcoule})`,
              template: '',
              onOk() {
                Meteor.call('podomoroActionStatus', {
                  actionId: lastPlayedTask, statusPodomoro: 'pending', secondes, totalSecondes,
                }, (err) => {
                  if (err) {
                    IonPopup.alert({ template: i18n.__(err.reason) });
                  } else {
                    Pomodoro.play();
                    Pomodoro.podomoroSessionSet('lastPlayedTask', self._id.valueOf());
                    Meteor.call('podomoroActionStatus', { actionId: self._id.valueOf(), statusPodomoro: 'working' });
                  }
                });
              },
              onCancel() {
                Meteor.call('podomoroActionStatus', { actionId: lastPlayedTask, statusPodomoro: 'pending' }, (err) => {
                  if (err) {
                    IonPopup.alert({ template: i18n.__(err.reason) });
                  } else {
                    Pomodoro.play();
                    Pomodoro.podomoroSessionSet('lastPlayedTask', self._id.valueOf());
                    Meteor.call('podomoroActionStatus', { actionId: self._id.valueOf(), statusPodomoro: 'working' });
                  }
                });
              },
              cancelText: 'Non',
              okText: 'Oui',
            });
          }
        });
      } else {
        Pomodoro.play();
        Pomodoro.podomoroSessionSet('lastPlayedTask', self._id.valueOf());
        Meteor.call('podomoroActionStatus', { actionId: self._id.valueOf(), statusPodomoro: 'working' });
      }
    } else {
      Pomodoro.play();
      Meteor.call('podomoroActionStatus', { actionId: Pomodoro.podomoroSessionGet('lastPlayedTask'), statusPodomoro: 'pending' }, (err) => {
        if (err) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        } else {
          Pomodoro.podomoroSessionSet('lastPlayedTask', self._id.valueOf());
          Meteor.call('podomoroActionStatus', { actionId: self._id.valueOf(), statusPodomoro: 'working' });
        }
      });
    }
  },
  'click .pause-js'() {
    Meteor.call('podomoroActionStatus', { actionId: this._id.valueOf(), statusPodomoro: 'pausedInt', secondes: Pomodoro.getSecondes() }, (err) => {
      if (err) {
        IonPopup.alert({ template: i18n.__(err.reason) });
      } else {
        Pomodoro.pause();
      }
    });
  },

  'click .reset-js'() {
    const self = this;
    const secondes = Pomodoro.getSecondes();
    const totalSecondes = Pomodoro.getEndTime();

    const secondesEcoule = totalSecondes - secondes;
    const secondesEcouleStr = moment.utc(moment.duration(secondesEcoule, 'seconds').asMilliseconds()).format('LTS'); // HH:mm:ss
    Meteor.call('podomoroActionStatus', { actionId: this._id.valueOf(), statusPodomoro: 'pausedInt', secondes: Pomodoro.getSecondes() }, (err) => {
      if (err) {
        IonPopup.alert({ template: i18n.__(err.reason) });
      } else {
        Pomodoro.pause();
        IonPopup.confirm({
          title: `Ajouter le temps passé sur l'action ? ${secondesEcouleStr}`,
          template: '',
          onOk() {
            Meteor.call('podomoroActionStatus', {
              actionId: self._id.valueOf(), statusPodomoro: 'pendingInt', secondes, totalSecondes,
            }, (err) => {
              if (err) {
                IonPopup.alert({ template: i18n.__(err.reason) });
              } else {
                Pomodoro.podomoroSessionSet('pomodoroInProgress', false);
                Pomodoro.initialize();
                Pomodoro.podomoroSessionSet('playerStatus', 'play');
              }
            });
          },
          onCancel() {
            Meteor.call('podomoroActionStatus', { actionId: self._id.valueOf(), statusPodomoro: 'pendingInt' }, (err) => {
              if (err) {
                IonPopup.alert({ template: i18n.__(err.reason) });
              } else {
                Pomodoro.podomoroSessionSet('pomodoroInProgress', false);
                Pomodoro.initialize();
                Pomodoro.podomoroSessionSet('playerStatus', 'play');
              }
            });
          },
          cancelText: 'Non',
          okText: 'Oui',
        });
      }
    });
  },
  'click .photo-link-action'(event, instance) {
    // const scope = pageSession.get('scope');
    // const scopeId = pageSession.get('scopeId');
    // const actionId = pageSession.get('actionId');

    // if (Meteor.isCordova) {
    //   const options = {
    //     width: 640,
    //     height: 480,
    //     quality: 75,
    //   };

    //   const successCallback = () => {
    //     IonPopup.confirm({
    //       title: i18n.__('Photo'),
    //       template: i18n.__('Voulez vous ajouter une autre photo à cette action ?'),
    //       onOk() {
    //         MeteorCameraUI.getPicture(options, function (error, data) {
    //           if (!error) {
    //             const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
    //             Meteor.call('photoActions', data, str, scope, scopeId, actionId, function (errorCall) {
    //               if (!errorCall) {
    //                 successCallback();
    //               } else {
    //                 // console.log('error',error);
    //               }
    //             });
    //           }
    //         });
    //       },
    //       onCancel() {
    //         if (!pageSession.get('dataContext')) {
    //           Router.go('actionsDetailOrga', {
    //             orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), roomId: pageSession.get('roomId'), actionId: pageSession.get('actionId'),
    //           });
    //         }
    //       },
    //       cancelText: i18n.__('finish'),
    //       okText: i18n.__('other picture'),
    //     });
    //   };

    //   MeteorCameraUI.getPicture(options, function (error, data) {
    //     if (!error) {
    //       const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
    //       Meteor.call('photoActions', data, str, scope, scopeId, actionId, function (errorCall) {
    //         if (!errorCall) {
    //           successCallback();
    //         } else {
    //           // console.log('error',error);
    //         }
    //       });
    //     }
    //   });
    // } else {
    instance.$('#file-upload-action').trigger('click');
    // }
  },
  'change #file-upload-action'(event, instance) {
    event.preventDefault();
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    const actionId = pageSession.get('actionId');
    const drop = pageSession.get('drop');

    function successCallback() {
      IonPopup.confirm({
        title: i18n.__('Photo'),
        template: i18n.__('Voulez vous ajouter une autre photo à cette action ?'),
        onOk() {
          instance.$('#file-upload-action').trigger('click');
        },
        onCancel() {
          if (!pageSession.get('dataContext')) {
            Router.go('actionsDetailOrga', {
              orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), roomId: pageSession.get('roomId'), actionId: pageSession.get('actionId'),
            });
          }
        },
        cancelText: i18n.__('finish'),
        okText: i18n.__('other picture'),
      });
    }

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      _.each(instance.find('#file-upload-action').files, function (file) {
        if (file.size > 1) {
          const reader = new FileReader();
          reader.onload = function () {
            const str = file.name;
            const dataURI = reader.result;
            Meteor.call('photoActions', dataURI, str, scope, scopeId, actionId, function (error) {
              if (!error) {
                pageSession.set('fileSize', file.size);
                pageSession.set('fileName', file.name);

                if (!drop) {
                  successCallback();
                }
              } else {
                // console.log('error',error);
              }
            });
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
  'click .doc-link-action'(event, instance) {
    instance.$('#file-doc-upload-action').trigger('click');
  },
  'change #file-doc-upload-action'(event, instance) {
    event.preventDefault();
    // console.log('change');
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    const actionId = pageSession.get('actionId');
    const drop = pageSession.get('drop');

    function successCallback() {
      IonPopup.confirm({
        title: i18n.__('Document'),
        template: i18n.__('Voulez-vous ajouter un autre document ?'),
        onOk() {
          instance.$('#file-doc-upload-action').trigger('click');
        },
        onCancel() {
          if (!pageSession.get('dataContext')) {
            Router.go('actionsDetailOrga', {
              orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), roomId: pageSession.get('roomId'), actionId: pageSession.get('actionId'),
            });
          }
        },
        cancelText: i18n.__('finish'),
        okText: i18n.__('autre document'),
      });
    }

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      // console.log('change2');
      // console.log($('#file-doc-upload-action'));
      const countFiles = instance.find('#file-doc-upload-action').files && instance.find('#file-doc-upload-action').files.length ? instance.find('#file-doc-upload-action').files.length : 0;
      _.each(instance.find('#file-doc-upload-action').files, function (file) {
        // console.log('change3');
        if (file.size > 1) {
          // console.log('change4', file);
          const reader = new FileReader();
          reader.onload = function () {
            const str = file.name;
            const dataURI = reader.result;
            // console.log('change5', dataURI);
            Meteor.call('docActions', dataURI, str, scope, scopeId, actionId, function (error) {
              if (!error) {
                // pageSession.set('fileSize', file.size);
                // pageSession.set('fileName', file.name);
                if (!drop && countFiles === 1) {
                  successCallback();
                }
              } else {
                // eslint-disable-next-line no-console
                console.log('error', error);
              }
            });
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
  'click .photo-viewer'(event) {
    event.preventDefault();
    if (this.moduleId) {
      const url = `${Meteor.settings.public.urlimage}/upload/${this.moduleId}/${this.folder}/${this.name}`;
      if (Meteor.isCordova) {
        // PhotoViewer.show(url);
        window.open(url, '_blank');
      } else {
        window.open(url, '_blank');
      }
    }
  },
  'click .download-doc-js'(event) {
    event.preventDefault();
    const self = this;
    // http://localhost:3000/upload/communecter/projects/5eaf1e396908641b7a8b46c9/file/1613464497_facture-dYobroussailleuse.pdf
    // {{urlImageCommunecter}}/upload/{{moduleId}}/{{folder}}/{{name}}
    HTTP.get(Meteor.absoluteUrl(`download/doc/${self.moduleId}/${self.folder}/${self.name}`), {
      headers: {
        'x-access-token': Accounts._storedLoginToken(),
        'x-user-id': Meteor.userId(),
      },
    }, function (error, result) {
      if (result && result.content) {
        // let icalFile = null;
        const mime = result.headers && result.headers['x-doc-type'] ? result.headers['x-doc-type'] : 'application/octet-stream';
        const data = new Blob([result.content], {
          type: `${mime}`,
        });

        if (Meteor.isCordova) {
          const makeTextFile = function (dataB) {
            if (icalFile !== null) {
              window.URL.revokeObjectURL(icalFile);
            }
            icalFile = window.URL.createObjectURL(dataB);
            return icalFile;
          };
          const downloadLink = makeTextFile(result);
          cordova.InAppBrowser.open(downloadLink, '_system');
        } else {
          // window.open(downloadLink, '_blank');
          saveAs(data, self.name);
        }
      } else if (error) {
        // console.log(error);
      }
    });
  },
  'click .download-doc-direct-js'(event) {
    event.preventDefault();
    if (Meteor.isCordova) {
      cordova.InAppBrowser.open(event.currentTarget.href, '_system');
    } else {
      window.open(event.currentTarget.href, '_blank');
    }
  },
  'click .table-of-contents a'(event, instance) {
    event.preventDefault();
    var hash = event.target.getAttribute('href').substring(1); // Supprime le '#' du début
    var element = document.getElementById(hash);
    if (element) {
      element.scrollIntoView();
    }
  },
});

Template.shareId.events({
  'focus input[name="shareid"]'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareid"]');
    element.select();
    if (!Platform.isAndroid()) {
      navigator.permissions.query({ name: 'clipboard-write' }).then((result) => {
        if (result.state === 'granted' || result.state === 'prompt') {
          navigator.clipboard.writeText(element.value).then(function () {
            /* clipboard successfully set */
          }, function () {
            /* clipboard write failed */
          });
        }
      });
    }
  },
});

Template.actionsAdd.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('options.creditAddPorteur', null);
  pageSession.set('options.creditSharePorteur', null);
  pageSession.set('min', null);
  pageSession.set('max', null);
  pageSession.set('isPossiblecreditSharePorteur', null);
  pageSession.set('isCredits', null);
  pageSession.set('credits', null);
  pageSession.set('isStartDate', null);
  pageSession.set('isDepense', null);
  pageSession.set('noUpdateCredits', null);
  pageSession.set('dataContext', null);

  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      if (dataContext.startDate) {
        template.startDate = dataContext.startDate;
      }
      if (dataContext.endDate) {
        template.endDate = dataContext.endDate;
      }
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('scopeDetail', template.scope, template._id);
    Tracker.autorun(() => {
      template.ready.set(handle.ready());
    });
  });
});

Template.actionsFields.onDestroyed(function () {
  const self = this;
  self.$("input[name='tagsText']").atwho('destroy');
  self.$("input[name='assignText']").atwho('destroy');
  pageSession.set('milestoneId', null);
});

Template.actionsFields.helpers({
  isCordova() {
    return Meteor.isCordova;
  },
  isScopeCitoyens() {
    return pageSession.get('scope') === 'citoyens';
  },
  isMilestonesProject() {
    if (Session.get('settingOceco') && Session.get('settingOceco').milestonesProject && pageSession.get('scope') === 'projects') {
      return true;
    }
  },
  optionsMilestonesProject() {
    if (Session.get('settingOceco') && Session.get('settingOceco').milestonesProject && pageSession.get('scope') === 'projects') {
      const projectOne = Projects.findOne({ _id: new Mongo.ObjectID(pageSession.get('scopeId')) }, { fields: { _id: 1, 'oceco.milestones': 1 } });
      const options = projectOne && projectOne.oceco && projectOne.oceco.milestones && projectOne.oceco.milestones.length > 0 && projectOne.oceco.milestones && projectOne.oceco.milestones.filter((c) => c.status === 'open').map(function (c) {
        return { label: c.name, value: c.milestoneId };
      });
      if (options && options.length > 0) {
        return options;
      }
      return false;
    }
  },
  milestoneId() {
    return pageSession.get('milestoneId');
  },
  isMinMax() {
    if (Session.get('settingOceco')) {
      const settingOceco = Session.get('settingOceco');
      return settingOceco && settingOceco.costum && settingOceco.costum.actions && settingOceco.costum.actions.form && (settingOceco.costum.actions.form.max || settingOceco.costum.actions.form.min);
    }
  },
  isCreditAddPorteur() {
    return pageSession.get('options.creditAddPorteur');
  },
  isPossiblecreditSharePorteur() {
    return pageSession.get('isPossiblecreditSharePorteur');
  },
  isCreditSharePorteur() {
    return pageSession.get('options.creditSharePorteur');
  },
  isMin() {
    return pageSession.get('min');
  },
  isMax() {
    return pageSession.get('max');
  },
  isCredits() {
    return pageSession.get('isCredits');
  },
  noUpdateCredits() {
    return pageSession.get('noUpdateCredits');
  },
  isStartDate() {
    return pageSession.get('isStartDate');
  },
  isDepense() {
    return pageSession.get('isDepense');
  },
  isNote() {
    return pageSession.get('isNote');
  },
  parentType() {
    return pageSession.get('parentType');
  },
  parentId() {
    return pageSession.get('parentId');
  },
  optionsParentId(parentType) {
    let optionsParent = false;
    if (Meteor.userId() && Citoyens && Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }) && parentType) {
      // console.log(organizerType);
      if (parentType === 'organizations' || parentType === 'projects') {
        const optionsParentCursorOrga = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
        const optionsParentCursor = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).listProjectsCreator();
        optionsParent = optionsParentCursor.map(function (c) {
          return { label: c.name, value: c._id.valueOf() };
        });
        optionsParent = [{ label: `${optionsParentCursorOrga.name} (${i18n.__('organizations')})`, value: optionsParentCursorOrga._id.valueOf() }, ...optionsParent]
      }
      if (optionsParent) {
        return optionsParent;
      }
    }
    return false;
  },
  dataReadyParent() {
    return Template.instance().readyParent.get();
  },
});



Template.actionsFields.events({
  // Pressing Enter should submit the form
  'keydown input[name*="urls"]'(event, instance) {
    if (event.keyCode === 13) {
      instance.find('.autoform-add-item').click();
    }
  },
  'click input[name="options.creditAddPorteur"]'(event) {
    pageSession.set('options.creditAddPorteur', event.currentTarget.checked);
    pageSession.set('isPossiblecreditSharePorteur', false);
    pageSession.set('min', 1);
    pageSession.set('max', 1);
    pageSession.set('credits', 1);
  },
  'click input[name="options.creditSharePorteur"]'(event, instance) {
    pageSession.set('options.creditSharePorteur', event.currentTarget.checked);
    const credits = pageSession.get('credits');
    const max = pageSession.get('max');
    if (credits && max && credits < max) {
      instance.$('input[name="credits"]').val(parseInt(max));
      pageSession.set('credits', parseInt(max));
    } else if (!credits && max) {
      instance.$('input[name="credits"]').val(parseInt(max));
      pageSession.set('credits', parseInt(max));
    }
  },
  'keyup input[name="min"]'(event) {
    if (!pageSession.get('isDepense')) {
      if ((event.currentTarget.value && event.currentTarget.value > 1) || (pageSession.get('max') && pageSession.get('max') > 1)) {
        pageSession.set('isPossiblecreditSharePorteur', true);
      } else {
        pageSession.set('isPossiblecreditSharePorteur', false);
      }
    } else {
      pageSession.set('isPossiblecreditSharePorteur', false);
    }

    pageSession.set('min', parseInt(event.currentTarget.value));
  },
  'keyup input[name="max"]'(event) {
    if (!pageSession.get('isDepense')) {
      if ((event.currentTarget.value && event.currentTarget.value > 1) || (pageSession.get('min') && pageSession.get('min') > 1)) {
        pageSession.set('isPossiblecreditSharePorteur', true);
      } else {
        pageSession.set('isPossiblecreditSharePorteur', false);
      }
    } else {
      pageSession.set('isPossiblecreditSharePorteur', false);
    }

    pageSession.set('max', parseInt(event.currentTarget.value));
  },
  'keyup input[name="credits"]'(event, instance) {
    if (event.currentTarget && event.currentTarget.value && event.currentTarget.value > 0) {
      pageSession.set('isCredits', true);
      pageSession.set('credits', parseInt(event.currentTarget.value));
      pageSession.set('isDepense', false);
    } else if (event.currentTarget && event.currentTarget.value && event.currentTarget.value < 0) {
      // console.log('depense');
      pageSession.set('isCredits', true);
      pageSession.set('credits', parseInt(event.currentTarget.value));
      pageSession.set('isDepense', true);
      const creditSharePorteur = instance.find('input[name="options.creditSharePorteur"]');
      if (creditSharePorteur) {
        creditSharePorteur.checked = false;
      }
    } else {
      pageSession.set('isCredits', false);
      pageSession.set('credits', parseInt(event.currentTarget.value));
      pageSession.set('isDepense', false);
    }
    const max = pageSession.get('max');
    if (event.currentTarget && event.currentTarget.value && event.currentTarget.value < max) {
      const creditSharePorteur = instance.find('input[name="options.creditSharePorteur"]');
      if (creditSharePorteur) {
        creditSharePorteur.checked = false;
      }
    }
  },
  'keyup/change input[name="startDate"]'(event) {
    if (event.currentTarget.value) {
      pageSession.set('isStartDate', true);
    } else {
      pageSession.set('isStartDate', false);
    }
  },
  'change select[name="parentType"]'(event, instance) {
    event.preventDefault();
    // console.log(tmpl.$(e.currentTarget).val());
    pageSession.set('parentType', instance.$(event.currentTarget).val());
    pageSession.set('parentId', false);
  },
  'change select[name="parentId"]'(event, instance) {
    event.preventDefault();
    // console.log(instance.$(event.currentTarget).val());
    pageSession.set('parentId', instance.$(event.currentTarget).val());
  },
});


Template.actionsFields.onCreated(function () {
  const self = this;
  const template = Template.instance();
  template.readyParent = new ReactiveVar();

  self.autorun(function (c) {
    if (pageSession.get('scopeId') && pageSession.get('scope')) {
      pageSession.set('parentType', pageSession.get('scope'));
      pageSession.set('parentId', pageSession.get('scopeId'));
      c.stop();
    }
  });
});

Template.actionsFields.onRendered(function () {
  const self = this;

  self.autorun(function () {
    const parentType = pageSession.get('parentType');
    // console.log(`autorun ${parentType}`);
    if (parentType && Meteor.userId()) {
      if (parentType === 'organizations') {
        // const handleParent = self.subscribe('directoryListOrganizations', 'citoyens', Meteor.userId());
        // self.readyParent.set(handleParent.ready());
        const handleParent = self.subscribe('directoryListProjects', 'organizations', Session.get('orgaCibleId'));
        Tracker.autorun(() => {
          self.readyParent.set(handleParent.ready());
        });
      } else if (parentType === 'projects') {
        const handleParent = self.subscribe('directoryListProjects', 'organizations', Session.get('orgaCibleId'));
        Tracker.autorun(() => {
          self.readyParent.set(handleParent.ready());
        });
      } else if (parentType === 'citoyens') {
        // self.readyParent.set(true);
      }
    }
  });

  const template = Template.instance();
  template.find('input[name=name]').focus();

  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);

  if (Platform.isAndroid()) {
    // change le fields credits en type tel
    const credits = template.find("input[name='credits']");
    if (credits) {
      credits.type = 'tel';
    }
  }

  if (Meteor.isCordova) {
    // mobile predictive desactived
    const tagText = template.find("input[name='tagsText']");
    if (tagText && tagText.value !== '') {
      tagText.type = 'text';
    }
    self.$("input[name='tagsText']").on('focus', function () {
      this.type = 'text';
      if (!this.value) {
        this.value = '#';
      }
    });
  } else {
    self.$("input[name='tagsText']").on('focus', function () {
      if (!this.value) {
        this.value = '#';
      }
    });
  }

  if (Meteor.isCordova) {
    // mobile predictive desactived
    const tagText = template.find("input[name='assignText']");
    if (tagText && tagText.value !== '') {
      tagText.type = 'text';
    }
    self.$("input[name='assignText']").on('focus', function () {
      this.type = 'text';
      if (!this.value) {
        // this.value = '@';
      }
    });
  } else {
    self.$("input[name='assignText']").on('focus', function () {
      if (!this.value) {
        // this.value = '@';
      }
    });
  }

  // #tags
  if (template.scope !== 'citoyens') {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    pageSession.set('queryTag', false);
    pageSession.set('tags', false);
    self.$("input[name='tagsText']").atwho({
      at: '#',
      data: orgaOne && orgaOne.oceco && orgaOne.oceco.tags ? orgaOne.oceco.tags : [],
      limit: orgaOne && orgaOne.oceco && orgaOne.oceco.tags && orgaOne.oceco.tags.length > 0 ? orgaOne.oceco.tags.length : 0,
    }).on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));
      if ($li.data('item-data')['atwho-at'] === '#') {
        const tag = $li.data('item-data').name;
        if (pageSession.get('tags')) {
          const arrayTags = pageSession.get('tags');
          arrayTags.push(tag);
          pageSession.set('tags', arrayTags);
        } else {
          pageSession.set('tags', [tag]);
        }
      }
    });

    self.$("input[name='assignText']").atwho({
      at: '',
      limit: 100,
      delay: 600,
      displayTimeout: 300,
      // startWithSpace: true,
      displayTpl(item) {
        return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
      },
      // eslint-disable-next-line no-template-curly-in-string
      insertTpl: '${atwho-at}${slug}',
      searchKey: 'name',
    }).on('matched.atwho', function (event, flag, query) {
      // console.log(event, "matched " + flag + " and the result is " + query);
      if (flag === '' && query) {
        // console.log(pageSession.get('queryMention'));
        if (pageSession.get('queryMention') !== query) {
          pageSession.set('queryMention', query);
          const querySearch = {};
          querySearch.search = query;
          querySearch.orgaCibleId = Session.get('orgaCibleId');
          Meteor.call('searchListAssignActionsautocomplete', querySearch, function (error, result) {
            if (!error) {
              // console.log(result);
              const citoyensArray = _.map(result, (array) => (array.profilThumbImageUrl ? {
                id: array._id.valueOf(), name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
              } : {
                id: array._id.valueOf(), name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
              }));
              if (citoyensArray && citoyensArray.length > 0) {
                // const uniqueArray = pageSession.get('mentions') && pageSession.get('mentions').length > 1 ? citoyensArray.filter((obj) => !pageSession.get('mentions').some((obj2) => obj.id === obj2.id)) : citoyensArray;
                self.$("input[name='assignText']").atwho('load', '', citoyensArray).atwho('run');
              }
            }
          });
        }
      }
    })
      .on('inserted.atwho', function (event, $li) {
        if ($li.data('item-data')['atwho-at'] === '') {
          const mentions = {};
          // const arrayMentions = [];
          mentions.name = $li.data('item-data').name;
          mentions.id = $li.data('item-data').id;
          mentions.avatar = $li.data('item-data').avatar;
          mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
          mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
          if (pageSession.get('mentions')) {
            const arrayMentions = pageSession.get('mentions');
            arrayMentions.push(mentions);
            pageSession.set('mentions', arrayMentions);
          } else {
            pageSession.set('mentions', [mentions]);
          }
        }
      });

    /* const querySearch = {};
    querySearch.orgaCibleId = Session.get('orgaCibleId');
    Meteor.call('searchListAssignActionsautocomplete', querySearch, function (error, result) {
      if (!error) {
        const citoyensArray = _.map(result, (array, key) => (array.profilThumbImageUrl ? { id: array._id.valueOf(), name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}` } : { id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens' }));
        if (citoyensArray && citoyensArray.length > 0) {
          self.$("input[name='assignText']").atwho('load', '@', citoyensArray).atwho('run');
        }
      }
    }); */
  }
});

Template.actionsEdit.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('options.creditAddPorteur', null);
  pageSession.set('options.creditSharePorteur', null);
  pageSession.set('min', null);
  pageSession.set('max', null);
  pageSession.set('isPossiblecreditSharePorteur', null);
  pageSession.set('isCredits', null);
  pageSession.set('credits', null);
  pageSession.set('isStartDate', null);
  pageSession.set('isDepense', null);
  pageSession.set('noUpdateCredits', null);
  pageSession.set('dataContext', null);
  pageSession.set('isNote', false);

  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
  });

  this.autorun(function () {
    // console.log('detailActions 2');
    const handle = Meteor.subscribe('detailActionsOpti', template.scope, template._id, template.roomId, template.actionId);
    Tracker.autorun(() => {
      template.ready.set(handle.ready());
    });
  });
});

Template.actionsAdd.helpers({
  action() {
    const actionEdit = {};
    const collection = nameToCollection(Template.instance().scope);
    const event = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    if (event && event.isAdmin()) {
      actionEdit.isAdmin = event.isAdmin();
    }
    if (Template.instance().scope === 'events') {
      if (event) {
        if (event.startDate) {
          actionEdit.startDate = moment(event.startDate).toDate();
        }
        if (event.endDate) {
          actionEdit.endDate = moment(event.endDate).toDate();
        }
      }
      // console.log(actionEdit);
    }
    if (Template.instance().startDate) {
      actionEdit.startDate = moment(Template.instance().startDate).toDate();
    }
    if (Template.instance().endDate) {
      actionEdit.endDate = moment(Template.instance().endDate).toDate();
    }
    if (searchAction.get('actionName')) {
      actionEdit.name = searchAction.get('actionName');
    }
    return actionEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.actionsAdd.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'change input[name=startDate]'(event, instance) {
    if (instance.$('input[name=endDate]').val() === '') {
      const startOne = moment(event.target.value).add(1, 'hours');
      instance.$('input[name=endDate]').val(moment(startOne).format('YYYY-MM-DDTHH:MM'));
      instance.$('.autoform-datetimepicker-input').trigger('change');
    }
  }
});

Template.actionsAddModal.inheritsHelpersFrom('actionsAdd');
Template.actionsAddModal.inheritsEventsFrom('actionsAdd');
Template.actionsAddModal.inheritsHooksFrom('actionsAdd');

Template.actionsEdit.helpers({
  action() {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    const actionEdit = {};
    actionEdit._id = action._id.valueOf();
    actionEdit.name = action.name;
    actionEdit.startDate = action.startDate;
    actionEdit.endDate = action.endDate;
    if (action.startDate) {
      actionEdit.startDate = action.momentStartDate();
      pageSession.set('isStartDate', true);
    }
    if (action.endDate) {
      actionEdit.endDate = action.momentEndDate();
    }
    actionEdit.description = action.description;
    if (action.tags && action.tags.length > 0) {
      actionEdit.tagsText = action.tags.map((tag) => `#${tag}`).join(' ');
    }
    actionEdit.urls = action.urls;
    actionEdit.min = action.min;
    actionEdit.max = action.max;
    actionEdit.credits = action.credits;
    actionEdit.options = action.options;

    if (action.options) {
      if (action.options.creditAddPorteur) {
        pageSession.set('options.creditAddPorteur', true);
        pageSession.set('isPossiblecreditSharePorteur', false);
      }
      if (action.options.creditSharePorteur) {
        pageSession.set('options.creditSharePorteur', true);
        pageSession.set('isPossiblecreditSharePorteur', true);
      }
    }

    if (action.min) {
      pageSession.set('min', action.min);
    }
    if (action.max) {
      pageSession.set('max', action.max);
    }

    if (action.startDate) {
      actionEdit.startDate = action.momentStartDate();
    }

    if (action.credits && action.credits > 0) {
      pageSession.set('isCredits', true);
      pageSession.set('credits', action.credits);
    } else {
      pageSession.set('isCredits', false);
      pageSession.set('credits', action.credits);
    }

    // si deja des users on peut pas modifier les crédits
    if (action.countContributorsArray() > 0) {
      pageSession.set('noUpdateCredits', true);
    }
    //
    if (action.milestone && action.milestone.milestoneId) {
      actionEdit.milestoneId = action.milestone.milestoneId;
      pageSession.set('milestoneId', action.milestone.milestoneId);
    }

    if (action.noteId) {
      pageSession.set('isNote', true);
    }

    return actionEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.actionsEditModal.inheritsHelpersFrom('actionsEdit');
Template.actionsEditModal.inheritsEventsFrom('actionsEdit');
Template.actionsEditModal.inheritsHooksFrom('actionsEdit');

AutoForm.addHooks(['addAction', 'editAction'], {
  after: {
    method(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('dda.actions.action')}`,
        });
        searchAction.set('search', null);
        searchAction.set('actionName', null);
        if (pageSession.get('dataContext')) {
          IonModal.close();
        } else {
          Router.go('actionsList', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope') }, { replaceState: true });
        }
      }
    },
    'method-update'(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('dda.actions.action')}`,
        });
        if (pageSession.get('dataContext')) {
          IonModal.close();
        } else {
          Router.go('actionsDetailOrga', {
            orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), roomId: pageSession.get('roomId'), actionId: pageSession.get('actionId'),
          }, { replaceState: true });
        }

      }
    },
  },
  before: {
    method(doc) {
      doc.parentType = pageSession.get('parentType') ? pageSession.get('parentType') : pageSession.get('scope');
      doc.parentId = pageSession.get('parentId') ? pageSession.get('parentId') : pageSession.get('scopeId');

      if (pageSession.get('scope') === 'organizations' || pageSession.get('scope') === 'projects') {
        if (Session.get('orgaCibleId') === doc.parentId) {
          doc.parentType = 'organizations';
        } else {
          doc.parentType = 'projects';
        }
      }


      if (pageSession.get('mentions') && doc.assignText) {
        // console.log(pageSession.get('mentions'));
        // console.log(doc.assignText);
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => doc.assignText.match(`${array.value}`) !== null);
        // console.log('arrayMentions', arrayMentions);
        if (arrayMentions && arrayMentions.length > 0) {
          doc.assign = arrayMentions.map((member) => member.id);
        }
        delete doc.assignText;
      } else {
        delete doc.assignText;
      }
      // console.log(doc);
      return doc;
    },
    'method-update'(modifier) {
      modifier.$set.parentType = pageSession.get('parentType') ? pageSession.get('parentType') : pageSession.get('scope');
      modifier.$set.parentId = pageSession.get('parentId') ? pageSession.get('parentId') : pageSession.get('scopeId');
      // modifier.$set.idParentRoom = pageSession.get('roomId');

      if (pageSession.get('scope') === 'organizations' || pageSession.get('scope') === 'projects') {
        if (Session.get('orgaCibleId') === modifier.$set.parentId) {
          modifier.$set.parentType = 'organizations';
        } else {
          modifier.$set.parentType = 'projects';
        }
      }

      if (pageSession.get('credits') > 0 && !modifier.$set.credits) {
        modifier.$set.credits = pageSession.get('credits');
      }
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(': ', ''));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      }
    }
  },
});
