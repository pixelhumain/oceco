/* eslint-disable meteor/no-session */
/* global IonToast Session */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';
import i18n from 'meteor/universe:i18n';
import { _ } from 'meteor/underscore';

import { Organizations } from '../../../../api/collection/organizations.js';
import { Actions } from '../../../../api/collection/actions.js';

import { pageSession } from '../../../../api/client/reactive.js';
import { scrollPagination } from '../../../../api/helpers.js';
import './assign.html';
import { Projects } from '../../../../api/collection/projects.js';
import { Notes } from '/imports/api/collection/notes.js';
import { Citoyens } from '/imports/api/collection/citoyens.js';

// window.Organizations = Organizations;

Template.assignMembers.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  const dataContext = Template.currentData();
  const template = Template.instance();

  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  self.autorun(function () {

    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
      if (Router.current().params.orgaCibleId) {
        template.orgaCibleId = Router.current().params.orgaCibleId;
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
        template.orgaCibleId = Router.current().params._id;
      }
      template.dataContext = false;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('detailActionsOpti', template.scope, template._id, template.roomId, template.actionId);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });

  self.autorun(function () {

    const paramPub = { scopeId: template.orgaCibleId, actionId: template.actionId, limit: self.limit.get('limit') };
    if (pageSession.get('search')) {
      paramPub.search = pageSession.get('search');
    }
    const handle = Meteor.subscribe('listAssignActions', paramPub);
  });
});

Template.assignMembers.onRendered(scrollPagination);

Template.assignMembers.helpers({
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
  },
  actions() {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    return action;
  },
  listAssignActions(search) {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    let listMembers = [];
    const listMembersActions = orgaOne.listMembersActions(Template.instance().actionId, search);
    if (action.parentType === 'projects') {
      const listContributorsActions = Projects.findOne({ _id: new Mongo.ObjectID(action.parentId) }).listContributorsActions(Template.instance().actionId, search);
      listMembers = [...listContributorsActions.fetch(), ...listMembersActions.fetch()];
    } else {
      listMembers = [...listMembersActions.fetch()];
    }

    const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);

    Template.instance().listMembersUnique = listMembersUnique;
    return listMembersUnique.sort((a, b) => {
      if (a.assign > b.assign) {
        return -1;
      }
      if (a.assign < b.assign) {
        return 1;
      }
      if (a.assign === b.assign && a.assign === 1) {
        return -1;
      }
      if (a.assign === b.assign && a.assign === 0) {
        return 1;
      }
      return 0;
    });
  },
  countMembers(search) {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    let listMembers = [];
    const listMembersActions = orgaOne.listMembersActions(Template.instance().actionId, search);

    if (action.parentType === 'projects') {
      const listContributorsActions = Projects.findOne({ _id: new Mongo.ObjectID(action.parentId) }).listContributorsActions(Template.instance().actionId, search);
      listMembers = [...listContributorsActions.fetch(), ...listMembersActions.fetch()];
    } else {
      listMembers = [...listMembersActions.fetch()];
    }
    const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);

    const countMembers = listMembersUnique && listMembersUnique.length ? listMembersUnique.length : 0;
    return countMembers;
  },
  search() {
    return pageSession.get('search');
  },
  listRolesSearch() {
    const search = pageSession.get('search');
    if (search && search.startsWith('r:')) {
      if (search.substr(2).length > 0) {
        const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
        return orgaOne.listRoles('citoyens', search.substr(2));
      } else {
        const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
        return orgaOne.listRoles('citoyens');
      }
    }
  },
  isSearchRole() {
    const search = pageSession.get('search');
    if (search && search.startsWith('r:')) {
      return true;
    }
    return false;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

/*
admin : assigner un user à une action
admin : de-assigner un user à une action
*/

Template.assignMembers.events({
  'click .assignmember-js'(event) {
    event.preventDefault();
    Meteor.call('assignMemberActionRooms', { id: pageSession.get('actionId'), memberId: this._id.valueOf() }, (error) => {
      if (error) {
        // instance.state.set('call', false);
        // IonPopup.alert({ template: i18n.__(error.reason) });
      }
    });
  },
  'click .unassignmember-js'(event) {
    event.preventDefault();
    const actionId = pageSession.get('actionId');
    const orgId = Session.get('orgaCibleId');
    Meteor.call('exitAction', {
      id: actionId, orgId, memberId: this._id.valueOf(),
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
  'click .assignmemberbysearch-js'(event, instance) {
    event.preventDefault();
    if (instance.listMembersUnique && instance.listMembersUnique.length > 0) {
      const membersId = instance.listMembersUnique.filter((member) => { return member.assign === 0; }).map((member) => member._id.valueOf());
      if (membersId && membersId.length > 0) {
        IonPopup.confirm({
          title: i18n.__('Assign'),
          template: i18n.__('Do you want to assign all the members present in the list'),
          onOk() {
            Meteor.call('assignArrayMembersActionRooms', { id: pageSession.get('actionId'), membersId: membersId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
      } else {
        IonToast.show({
          title: i18n.__('Information'), position: 'top', type: 'progress', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('all members in the list are already assigned')}`,
        });
      }
    }

  },
  'click .searchroles-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('search', `r:${this}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('search', null);
    }
  }, 500),
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});

Template.assignMembersModal.inheritsHelpersFrom('assignMembers');
Template.assignMembersModal.inheritsEventsFrom('assignMembers');
Template.assignMembersModal.inheritsHooksFrom('assignMembers');

Template.assignMembersTask.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  const dataContext = Template.currentData();
  const template = Template.instance();

  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  self.autorun(function () {

    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
      template.taskId = dataContext.taskId;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
      template.taskId = Router.current().params.taskId;
      if (Router.current().params.orgaCibleId) {
        template.orgaCibleId = Router.current().params.orgaCibleId;
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
        template.orgaCibleId = Router.current().params._id;
      }
      template.dataContext = false;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
    pageSession.set('taskIdIn', template.taskId);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('detailActionsOpti', template.scope, template._id, template.roomId, template.actionId);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });

  self.autorun(function () {
    const paramPub = { scopeId: template.orgaCibleId, actionId: template.actionId, taskId: template.taskId, limit: self.limit.get('limit') };
    if (pageSession.get('search')) {
      paramPub.search = pageSession.get('search');
    }
    const handle = Meteor.subscribe('listAssignTaskActions', paramPub);
  });
});

Template.assignMembersTask.onRendered(scrollPagination);

Template.assignMembersTask.helpers({
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
  },
  actions() {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    return action;
  },
  listAssignActions(search) {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    let listMembers = [];
    const listMembersActions = orgaOne.listMembersTaskActions(Template.instance().actionId, Template.instance().taskId, search);
    if (action.parentType === 'projects') {
      const listContributorsActions = Projects.findOne({ _id: new Mongo.ObjectID(action.parentId) }).listContributorsTaskActions(Template.instance().actionId, Template.instance().taskId, search);
      listMembers = [...listContributorsActions.fetch(), ...listMembersActions.fetch()];
    } else {
      listMembers = [...listMembersActions.fetch()];
    }

    const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);

    return listMembersUnique.sort((a, b) => {
      if (a.assign > b.assign) {
        return -1;
      }
      if (a.assign < b.assign) {
        return 1;
      }
      if (a.assign === b.assign && a.assign === 1) {
        return -1;
      }
      if (a.assign === b.assign && a.assign === 0) {
        return 1;
      }
      return 0;
    });
  },
  countMembers(search) {
    const action = Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
    let listMembers = [];
    const listMembersActions = orgaOne.listMembersTaskActions(Template.instance().actionId, Template.instance().taskId, search);
    if (action.parentType === 'projects') {
      const listContributorsActions = Projects.findOne({ _id: new Mongo.ObjectID(action.parentId) }).listContributorsTaskActions(Template.instance().actionId, Template.instance().taskId, search);
      listMembers = [...listContributorsActions.fetch(), ...listMembersActions.fetch()];
    } else {
      listMembers = [...listMembersActions.fetch()];
    }
    const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);
    const countMembers = listMembersUnique && listMembersUnique.length ? listMembersUnique.length : 0;
    return countMembers;
  },
  search() {
    return pageSession.get('search');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

/*
admin : assigner un user à une action
admin : de-assigner un user à une action
*/

Template.assignMembersTask.events({
  'click .assignmember-js'(event) {
    event.preventDefault();
    Meteor.call('assignMemberTaskActionRooms', { id: pageSession.get('actionId'), taskId: pageSession.get('taskIdIn'), memberId: this._id.valueOf() }, (error) => {
      if (error) {
        // instance.state.set('call', false);
        // IonPopup.alert({ template: i18n.__(error.reason) });
      }
    });
  },
  'click .unassignmember-js'(event) {
    event.preventDefault();
    const actionId = pageSession.get('actionId');
    const taskId = pageSession.get('taskIdIn');
    const orgId = Session.get('orgaCibleId');
    Meteor.call('exitTaskAction', {
      id: actionId, taskId, orgId, memberId: this._id.valueOf(),
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('search', null);
    }
  }, 500),
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});

Template.assignMembersTaskModal.inheritsHelpersFrom('assignMembersTask');
Template.assignMembersTaskModal.inheritsEventsFrom('assignMembersTask');
Template.assignMembersTaskModal.inheritsHooksFrom('assignMembersTask');


Template.assignMembersNote.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  const dataContext = Template.currentData();
  const template = Template.instance();

  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  self.autorun(function () {

    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.docId = dataContext.docId;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.docId = Router.current().params.docId;
      if (Router.current().params.orgaCibleId) {
        template.orgaCibleId = Router.current().params.orgaCibleId;
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
        template.orgaCibleId = Router.current().params._id;
      }
      template.dataContext = false;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('docId', template.docId);

  });


  const handle = Meteor.subscribe('documentUpdates', template.scope, template._id, template.docId, true);

  Tracker.autorun(() => {
    const areAllSubsReady = handle.ready();
    if (areAllSubsReady) {
      Tracker.autorun(function () {
        if (Session.get('orgaCibleId')) {
          dataContext.orgaCibleId = Session.get('orgaCibleId');
          template.orgaCibleId = Session.get('orgaCibleId');
        } else {
          self.ready.set(areAllSubsReady);
        }

        const paramPub = { docId: template.docId, limit: self.limit.get('limit') };
        if (template.orgaCibleId) {
          paramPub.scopeId = template.orgaCibleId;
        }
        if (pageSession.get('search')) {
          paramPub.search = pageSession.get('search');
        }
        const handle = Meteor.subscribe('listAssignNotes', paramPub);
        Tracker.autorun(function () {
          self.ready.set(handle.ready());
        }
        );

      });
    }
  });


});

Template.assignMembersNote.onRendered(scrollPagination);

Template.assignMembersNote.helpers({
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
  },
  listOrganizations() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).listOrganizationsCreator();
  },
  notes() {
    const note = Notes.findOne({ _id: new Mongo.ObjectID(Template.instance().docId) });
    return note;
  },
  listAssignNotes(search) {
    const action = Notes.findOne({ _id: new Mongo.ObjectID(Template.instance().docId) });
    if (Session.get('orgaCibleId')) {
      const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
      let listMembers = [];
      const listMembersNotes = orgaOne.listMembersNotes(Template.instance().docId, search);

      if (action.parentType === 'projects') {
        const listContributorsNotes = Projects.findOne({ _id: new Mongo.ObjectID(action.parentId) }).listContributorsNotes(Template.instance().docId, search);
        listMembers = [...listContributorsNotes.fetch(), ...listMembersNotes.fetch()];
      } else {
        listMembers = [...listMembersNotes.fetch()];
      }

      const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);

      Template.instance().listMembersUnique = listMembersUnique;
      return listMembersUnique.sort((a, b) => {
        if (a.assign > b.assign) {
          return -1;
        }
        if (a.assign < b.assign) {
          return 1;
        }
        if (a.assign === b.assign && a.assign === 1) {
          return -1;
        }
        if (a.assign === b.assign && a.assign === 0) {
          return 1;
        }
        return 0;
      });
    }

  },
  countMembers(search) {
    const note = Notes.findOne({ _id: new Mongo.ObjectID(Template.instance().docId) });
    if (Session.get('orgaCibleId')) {
      const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
      let listMembers = [];
      const listMembersNotes = orgaOne.listMembersNotes(Template.instance().docId, search);
      if (note.parentType === 'projects') {
        const listContributorsNotes = Projects.findOne({ _id: new Mongo.ObjectID(note.parentId) }).listContributorsTaskActions(Template.instance().docId, search);
        listMembers = [...listContributorsNotes.fetch(), ...listMembersNotes.fetch()];
      } else {
        listMembers = [...listMembersNotes.fetch()];
      }
      const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);
      const countMembers = listMembersUnique && listMembersUnique.length ? listMembersUnique.length : 0;
      return countMembers;
    }

  },
  listRolesSearch() {
    const search = pageSession.get('search');
    if (search && search.startsWith('r:')) {
      if (search.substr(2).length > 0) {
        const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
        return orgaOne.listRoles('citoyens', search.substr(2));
      } else {
        const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) });
        return orgaOne.listRoles('citoyens');
      }
    }
  },
  isSearchRole() {
    const search = pageSession.get('search');
    if (search && search.startsWith('r:')) {
      return true;
    }
    return false;
  },
  search() {
    return pageSession.get('search');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
  isCitoyen() {
    return Template.instance().scope === 'citoyens';
  }
});

/*
admin : assigner un user à une action
admin : de-assigner un user à une action
*/

Template.assignMembersNote.events({
  'click .assignmember-js'(event) {
    event.preventDefault();
    Meteor.call('assignMemberNote', { id: pageSession.get('docId'), memberId: this._id.valueOf() }, (error) => {
      if (error) {
        // instance.state.set('call', false);
        // IonPopup.alert({ template: i18n.__(error.reason) });
      }
    });
  },
  'click .unassignmember-js'(event) {
    event.preventDefault();
    const docId = pageSession.get('docId');
    Meteor.call('exitNote', {
      id: docId, memberId: this._id.valueOf(),
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
  'click .assignmemberbysearch-js'(event, instance) {
    event.preventDefault();
    if (instance.listMembersUnique && instance.listMembersUnique.length > 0) {
      const membersId = instance.listMembersUnique.filter((member) => { return member.assign === 0; }).map((member) => member._id.valueOf());
      if (membersId && membersId.length > 0) {
        IonPopup.confirm({
          title: i18n.__('Assign'),
          template: i18n.__('Do you want to assign all the members present in the list'),
          onOk() {
            Meteor.call('assignArrayMembersNote', { id: pageSession.get('docId'), membersId: membersId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
      } else {
        IonToast.show({
          title: i18n.__('Information'), position: 'top', type: 'progress', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('all members in the list are already assigned')}`,
        });
      }
    }

  },
  'click .searchroles-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('search', `r:${this}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('search', null);
    }
  }, 500),
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
  'change #organization'(event, instance) {
    event.preventDefault();
    if (event.currentTarget.value !== '') {
      Session.setPersistent('orgaCibleId', event.currentTarget.value);
    }
  }
});

Template.assignMembersNoteModal.inheritsHelpersFrom('assignMembersNote');
Template.assignMembersNoteModal.inheritsEventsFrom('assignMembersNote');
Template.assignMembersNoteModal.inheritsHooksFrom('assignMembersNote');