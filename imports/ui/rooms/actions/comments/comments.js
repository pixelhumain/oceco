/* eslint-disable meteor/no-session */
/* global Session IonModal IonToast IonPopup */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';
import { AutoForm } from 'meteor/aldeed:autoform';
import { IonActionSheet } from 'meteor/meteoric:ionic';
import { _ } from 'meteor/underscore';

// collection
import { Events } from '../../../../api/collection/events.js';
import { Organizations } from '../../../../api/collection/organizations.js';
import { Projects } from '../../../../api/collection/projects.js';
import { Citoyens } from '../../../../api/collection/citoyens.js';
import { Comments } from '../../../../api/collection/comments.js';

import { nameToCollection } from '../../../../api/helpers.js';

import './comments.html';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;

const pageSession = new ReactiveDict('pageActionsComments');

Template.actionsDetailComments.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
    if (template.scope && template._id && template.roomId && template.actionId) {
      const handle = Meteor.subscribe('actionsDetailComments', template.scope, template._id, template.roomId, template.actionId);
      Tracker.autorun(() => {
        if (handle.ready()) {
          if (Organizations.find({}).count() === 2) {
            const orgaOne = Organizations.findOne({
              name: { $exists: false },
            });
            if (orgaOne && Session.get('orgaCibleId') !== orgaOne._id.valueOf()) {
              Session.setPersistent('orgaCibleId', orgaOne._id.valueOf());
            }
          }
          template.ready.set(handle.ready());
        }
      });
    }
  });
});

Template.actionsDetailComments.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id && Template.instance().roomId && Template.instance().actionId && Template.instance().ready.get()) {
      const collection = nameToCollection(Template.instance().scope);
      const scope = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      return scope;
    }
    return undefined;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.actionsDetailComments.events({
  'click .action-comment'(event, instance) {
    const self = this;
    event.preventDefault();
    IonActionSheet.show({
      titleText: i18n.__('Actions Comment'),
      buttons: [
        { text: `${i18n.__('edit')} <i class="icon ion-edit"></i>` },
      ],
      destructiveText: i18n.__('delete'),
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          IonModal.close();
          Router.go('commentsActionsEdit', {
            _id: instance._id, roomId: instance.roomId, actionId: instance.actionId, scope: instance.scope, commentId: self._id.valueOf(),
          });
        }
        return true;
      },
      destructiveButtonClicked() {
        Meteor.call('deleteComment', self._id.valueOf(), function () {
          // Router.go('actionsDetail', { _id: instance._id, roomId: instance.roomId, actionId: instance.actionId, scope: instance.scope }, { replaceState: true });
        });
        return true;
      },
    });
  },
  'click .like-comment'(event) {
    Meteor.call('likeScope', this._id.valueOf(), 'comments');
    event.preventDefault();
  },
  'click .dislike-comment'(event) {
    Meteor.call('dislikeScope', this._id.valueOf(), 'comments');
    event.preventDefault();
  },
});

Template.actionsDetailCommentsData.events({
  'click .action-news-flags'(event) {
    const self = this;
    event.preventDefault();
    if (self.isAuthor()) {
      IonPopup.alert({
        title: i18n.__('Reporting not possible'),
        template: i18n.__('This news was written by you, it is not possible to use the report on your own content'),
      });
      return;
    }
    // _id=target.id newsId=_id.valueOf() scope=target.type
    IonActionSheet.show({
      titleText: i18n.__('Actions report / flag'),
      buttons: [
        { text: `${i18n.__('report users')} <i class="icon ion-flag"></i>` },
        { text: `${i18n.__('report violating content')} <i class="icon ion-flag"></i>` },
        { text: `${i18n.__('block user')} <i class="icon ion-flag"></i>` },
      ],
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // report user
          const authorComments = self.authorComments() ?? null;
          const cibleId = authorComments._id.valueOf();
          const cibleScope = 'citoyens';
          IonModal.open('_reportflag', { cibleId, cibleScope });
        }
        if (index === 1) {
          // report violating content
          IonModal.open('_reportflag', { cibleScope: 'comments', cibleId: self._id.valueOf() });
        }
        if (index === 2) {
          // block user
          const authorComments = self.authorComments() ?? null;
          const cibleId = authorComments._id.valueOf();
          const cibleScope = 'citoyens';

          IonPopup.confirm({
            title: i18n.__('block user/element'),
            template: i18n.__('Are you sure you want to block this user/element ?'),
            onOk() {
              Meteor.call('blockUser', cibleId, cibleScope, (error) => {
                if (error) {
                  IonPopup.alert({ template: i18n.__(error.reason) });
                } else {
                  // window.history.back();
                  // Router.go('newsDetail', { _id: Router.current().params._id, newsId: Router.current().params.newsId, scope: Router.current().params.scope }, { replaceState: true });
                }
              });
            },
            onCancel() {

            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
        }
        return true;
      },
    });
  },
});

Template.actionsDetailCommentsModal.inheritsHelpersFrom('actionsDetailComments');
Template.actionsDetailCommentsModal.inheritsEventsFrom('actionsDetailComments');
Template.actionsDetailCommentsModal.inheritsHooksFrom('actionsDetailComments');

Template.commentsActionsAdd.onRendered(function () {
  const self = this;
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('mentions', false);
  self.$('textarea').atwho({
    at: '@',
    limit: 20,
    delay: 600,
    displayTimeout: 300,
    startWithSpace: true,
    displayTpl(item) {
      return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
    },
    // eslint-disable-next-line no-template-curly-in-string
    insertTpl: '${atwho-at}${slug}',
    searchKey: 'name',
  }).on('matched.atwho', function (event, flag, query) {
    // console.log(event, "matched " + flag + " and the result is " + query);
    if (flag === '@' && query) {
      // console.log(pageSession.get('queryMention'));
      if (pageSession.get('queryMention') !== query) {
        pageSession.set('queryMention', query);
        const querySearch = {};
        querySearch.search = query;
        Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
          if (!error) {
            const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
            }));
            const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations',
            }));
            const arrayUnions = _.union(citoyensArray, organizationsArray);
            // console.log(citoyensArray);
            self.$('textarea').atwho('load', '@', arrayUnions).atwho('run');
          }
        });
      }
    }
  })
    .on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));

      if ($li.data('item-data')['atwho-at'] === '@') {
        const mentions = {};
        // const arrayMentions = [];
        mentions.name = $li.data('item-data').name;
        mentions.id = $li.data('item-data').id;
        mentions.type = $li.data('item-data').type;
        mentions.avatar = $li.data('item-data').avatar;
        mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
        mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
        if (pageSession.get('mentions')) {
          const arrayMentions = pageSession.get('mentions');
          arrayMentions.push(mentions);
          pageSession.set('mentions', arrayMentions);
        } else {
          pageSession.set('mentions', [mentions]);
        }
      }
    });
});

Template.commentsActionsAdd.onDestroyed(function () {
  this.$('textarea').atwho('destroy');
});

Template.commentsActionsAdd.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
    }
    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
  });

  pageSession.set('error', false);
});

Template.commentsActionsAdd.onRendered(function () {
  pageSession.set('error', false);
});

Template.commentsActionsAdd.helpers({
  error() {
    return pageSession.get('error');
  },
});

Template.commentsActionsAdd.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/focus textarea[name="text"]'(event, instance) {
    const element = instance.find('textarea[name="text"]');
    // element.style.height = '5px';
    element.style.height = `${element.scrollHeight}px`;
  },
});

Template.commentsActionsEdit.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  pageSession.set('error', false);
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.roomId = dataContext.roomId;
      template.actionId = dataContext.actionId;
      template.commentId = dataContext.commentId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.roomId = Router.current().params.roomId;
      template.actionId = Router.current().params.actionId;
      template.commentId = Router.current().params.commentId;
    }
    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('roomId', template.roomId);
    pageSession.set('actionId', template.actionId);
    pageSession.set('commentId', template.commentId);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('actionsDetailComments', template.scope, template._id, template.roomId, template.actionId);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });
});

Template.commentsActionsEdit.onRendered(function () {
  const self = this;
  self.$('textarea').atwho({
    at: '@',
    limit: 20,
    delay: 600,
    displayTimeout: 300,
    startWithSpace: true,
    displayTpl(item) {
      return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
    },
    // eslint-disable-next-line no-template-curly-in-string
    insertTpl: '${atwho-at}${slug}',
    searchKey: 'name',
  }).on('matched.atwho', function (event, flag, query) {
    // console.log(event, "matched " + flag + " and the result is " + query);
    if (flag === '@' && query) {
      // console.log(pageSession.get('queryMention'));
      if (pageSession.get('queryMention') !== query) {
        pageSession.set('queryMention', query);
        const querySearch = {};
        querySearch.search = query;
        Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
          if (!error) {
            const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
            }));
            const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations',
            }));
            const arrayUnions = _.union(citoyensArray, organizationsArray);
            // console.log(citoyensArray);
            self.$('textarea').atwho('load', '@', arrayUnions).atwho('run');
          }
        });
      }
    }
  })
    .on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));

      if ($li.data('item-data')['atwho-at'] === '@') {
        const mentions = {};
        // const arrayMentions = [];
        mentions.name = $li.data('item-data').name;
        mentions.id = $li.data('item-data').id;
        mentions.type = $li.data('item-data').type;
        mentions.avatar = $li.data('item-data').avatar;
        mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
        mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
        if (pageSession.get('mentions')) {
          const arrayMentions = pageSession.get('mentions');
          arrayMentions.push(mentions);
          pageSession.set('mentions', arrayMentions);
        } else {
          pageSession.set('mentions', [mentions]);
        }
      }
    });
});

Template.commentsActionsEdit.onDestroyed(function () {
  this.$('textarea').atwho('destroy');
});

Template.commentsActionsEdit.helpers({
  comment() {
    const comment = Comments.findOne({ _id: new Mongo.ObjectID(Template.instance().commentId) });
    comment._id = comment._id.valueOf();
    return comment;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.commentsActionsEdit.events({
  // Pressing Ctrl+Enter should submit the form
  'keydown form'(event, instance) {
    if (event.keyCode === 13 && (event.metaKey || event.ctrlKey)) {
      instance.find('button[type=submit]').click();
    }
  },
  'keyup/focus textarea[name="text"]'(event, instance) {
    const element = instance.find('textarea[name="text"]');
    // element.style.height = '5px';
    element.style.height = `${element.scrollHeight}px`;
  },
});

AutoForm.addHooks(['addActionsComment', 'editActionsComment'], {
  before: {
    method(doc) {
      const actionId = pageSession.get('actionId');
      doc.contextType = 'actions';
      doc.contextId = actionId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => doc.text.match(`@${array.value}`) !== null);
        doc.mentions = arrayMentions;
      }
      return doc;
    },
    'method-update'(modifier) {
      const actionId = pageSession.get('actionId');
      modifier.$set.contextType = 'actions';
      modifier.$set.contextId = actionId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => modifier.$set.text.match(`@${array.value}`) !== null);
        modifier.$set.mentions = arrayMentions;
      }
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(':', ' '));
      }
    }
  },
});

AutoForm.addHooks(['addActionsComment'], {
  after: {
    method(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('comment')}`,
        });
      }
    },
  },
});

AutoForm.addHooks(['editActionsComment'], {
  after: {
    'method-update'(error) {
      if (!error) {
        Router.go('actionsDetailComments', {
          _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), roomId: pageSession.get('roomId'), actionId: pageSession.get('actionId'),
        }, { replaceState: true });
      }
    },
  },
});
