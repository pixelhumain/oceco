/* global IonToast */
/* eslint-disable consistent-return */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { _ } from 'meteor/underscore';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';
import { AutoForm } from 'meteor/aldeed:autoform';

import './invitations.html';

import '../components/directory/list.js';
import '../components/news/button-card.js';

import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';

import { nameToCollection, scrollPagination } from '../../api/helpers.js';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;

const pageSession = new ReactiveDict('pageSearchInvitations');

Template.pageInvitations.onCreated(function () {
  const self = this;
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  pageSession.set('searchGlobal', null);
  pageSession.set('filter', null);

  this.autorun(function () {
    if (pageSession.get('filter')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template.scopeId = dataContext.scopeId;
      pageSession.set('scopeId', template.scopeId);
      pageSession.set('scope', template.scope);
      template.dataContext = true;
    } else {
      template.scope = Router.current().params.scope;
      template.scopeId = Router.current().params._id;
      pageSession.set('scopeId', template.scopeId);
      pageSession.set('scope', template.scope);
      template.dataContext = false;
    }
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('scopeDetail', template.scope, template.scopeId);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));

  self.autorun(function () {
    const paramPub = { scope: template.scope, scopeId: template.scopeId, limit: self.limit.get('limit') };
    if (pageSession.get('filter')) {
      paramPub.search = pageSession.get('filter');
    }
    const handle = Meteor.subscribe('directoryListInvitations', paramPub);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });
});

Template.pageInvitations.onRendered(scrollPagination);

Template.pageInvitations.helpers({
  scope() {
    if (Template.instance().scope) {
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance().scopeId) });
    }
    // return undefined;
  },
  citoyen() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) });
  },
  scopeSousOrga() {
    if (Template.instance().scope) {
      const collection = nameToCollection(Template.instance().scope);
      const orgaOneParent = collection.findOne({ _id: new Mongo.ObjectID(Template.instance().scopeId), parent: { $exists: true } }, { fields: { _id: 1, parent: 1 } });
      if (orgaOneParent) {
        const arrayParent = Object.keys(orgaOneParent.parent);

        // liste des membres et admin de l'orga
        if (Template.instance().scope === 'organizations') {
          const options = {};
          const orgaOne = collection.findOne({ _id: new Mongo.ObjectID(arrayParent['0']) }, options);
          return orgaOne;
        }
      }
    }
  },
  filter() {
    return pageSession.get('filter');
  },
  scopeId() {
    return Template.instance().scopeId;
  },
  scopeVar() {
    return Template.instance().scope;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

Template.pageInvitations.events({
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 2) {
      pageSession.set('filter', event.currentTarget.value);
    } else if (event.currentTarget.value.length === 0) {
      pageSession.set('filter', null);
      pageSession.set('searchGlobal', null);
    }
  }, 500),
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});


Template.searchGlobalInvitations.onRendered(function () {
  this.autorun(function () {
    if (pageSession.get('filter')) {
      const query = pageSession.get('filter');
      const querySearch = {};
      querySearch.search = query;
      // querySearch.searchMode = 'personOnly';
      querySearch.elementId = pageSession.get('scopeId');
      Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
        let arrayCitoyens = [];
        let arrayOrganizations = [];
        if (result && result.citoyens) {
          arrayCitoyens = _.map(result.citoyens, (arraySearch, key) => ({
            _id: key,
            name: arraySearch.name,
            profilThumbImageUrl: arraySearch.profilThumbImageUrl,
            type: arraySearch.type,
            typeSig: 'citoyens',
            address: arraySearch.address,
          }));
          if (result && result.organizations) {
            arrayOrganizations = _.map(result.organizations, (arraySearch, key) => ({
              _id: key,
              name: arraySearch.name,
              profilThumbImageUrl: arraySearch.profilThumbImageUrl,
              type: arraySearch.type,
              typeSig: 'organizations',
              address: arraySearch.address,
            }));
          }
          // console.log(array);
          if (result) {
            pageSession.set('searchGlobal', [...arrayCitoyens, ...arrayOrganizations]);
          }
        }
      });
    }
  });
});

Template.searchGlobalInvitations.helpers({
  searchGlobal() {
    return pageSession.get('searchGlobal');
  },
  countSearchGlobal() {
    return pageSession.get('searchGlobal') && pageSession.get('searchGlobal').length;
  },
  icone(icone) {
    if (icone === 'citoyens') {
      return { class: 'icon fa fa-user yellow' };
    } if (icone === 'projects') {
      return { class: 'icon fa fa-lightbulb-o purple' };
    } if (icone === 'organizations') {
      return { class: 'icon fa fa-users green' };
    } if (icone === 'city') {
      return { class: 'icon fa fa-university red' };
    }
    return undefined;
  },
  urlType() {
    if (this.typeSig === 'citoyens') {
      return { class: 'icon fa fa-user yellow' };
    } if (this.typeSig === 'projects') {
      return { class: 'icon fa fa-lightbulb-o purple' };
    } if (this.typeSig === 'organizations') {
      return { class: 'icon fa fa-users green' };
    } if (this.typeSig === 'city') {
      return { class: 'icon fa fa-university red' };
    }
    return undefined;
  },
});

Template.formInvitations.onCreated(function () {
  pageSession.set('error', false);
  pageSession.set('invitedUserEmail', null);
});

Template.formInvitations.onRendered(function () {
  pageSession.set('error', false);
  pageSession.set('invitedUserEmail', null);
});

Template.formInvitations.helpers({
  error() {
    return pageSession.get('error');
  },
  isAlt() {
    if (pageSession.get('filter')) {
      const filter = pageSession.get('filter');
      return filter.includes('@') && filter;
    }
  },
  isName() {
    if (pageSession.get('filter')) {
      const filter = pageSession.get('filter');
      return !filter.includes('@') && filter;
    }
  },
});

AutoForm.addHooks(['formInvitations'], {
  before: {
    method(doc) {
      pageSession.set('error', null);
      pageSession.set('invitedUserEmail', doc.childEmail.trim());
      const scopeId = pageSession.get('scopeId');
      const scope = pageSession.get('scope');
      doc.parentId = scopeId;
      doc.parentType = scope;
      return doc;
    },
  },
  after: {
    method(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('invitations.invitation'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Invitation sent')}`,
        });
        pageSession.set('error', null);
      }
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        if (error.reason === "Problème à l'insertion du nouvel utilisateur : une personne avec cet mail existe déjà sur la plateforme") {
          pageSession.set('error', error.reason.replace(':', ' '));
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Problem when inserting the new user: a person with this email already exists on the platform')}`,
          });
          pageSession.set('filter', pageSession.get('invitedUserEmail'));
        } else {
          pageSession.set('error', error.reason.replace(':', ' '));
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__(error.reason.replace(':', ' '))}`,
          });
        }
      } else {
        pageSession.set('error', error.reason.replace(':', ' '));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(':', ' ')}`,
        });
      }
    }
  },
});
