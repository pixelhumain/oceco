/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { moment } from 'meteor/momentjs:moment';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';

import { Events } from '../../../api/collection/events.js';
import { Organizations } from '../../../api/collection/organizations.js';
import { Projects } from '../../../api/collection/projects.js';
import { Citoyens } from '../../../api/collection/citoyens.js';
import { Rooms } from '../../../api/collection/rooms.js';
import { Actions } from '../../../api/collection/actions.js';

import { nameToCollection } from '../../../api/helpers.js';

import { projectsCountOrga, eventsCountOrga } from '../../../api/helpersOrga.js';

import { searchAction } from '../../../api/client/reactive.js';

import './milestones.html';

import '../../components/directory/list.js';
import '../../components/news/button-card.js';
import '../../components/news/card.js';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;
// window.Rooms = Rooms;

const pageSession = new ReactiveDict('pageMilestones');

Template.projectsMilestonesListActions.onCreated(function () {
  pageSession.set('selectview', 'scopeActionsTemplate');
  pageSession.set('selectsubview', 'todo');
  this.readyScopeDetail = new ReactiveVar();

  this.autorun(function () {
    if (Router.current().params.scope && Router.current().params._id) {
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
        // console.log('news news.js 102');
      }

      const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
      Tracker.autorun(() => {
        if (handle.ready()) {

          if (!Router.current().params.orgaCibleId && Router.current().params.scope !== 'organizations') {
            const countOrga = Organizations.find({}).count();
            const orgaNoName = Organizations.find({
              name: { $exists: false },
            });

            if (countOrga > 1) {
              // console.log('news news.js 117 countOrga', countOrga, Session.get('orgaCibleId'));
              // 1 de plus quand faut changer

              if (Router.current().params.scope === 'projects') {
                const project = `links.projects.${Router.current().params._id}`;
                const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
              } else if (Router.current().params.scope === 'events') {
                const event = `links.events.${Router.current().params._id}`;
                const projectOne = Projects.find({ [event]: { $exists: 1 } });
                eventsCountOrga({
                  _id: Router.current().params._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                });
              } else if (Router.current().params.scope === 'actions') {
                const actionObjectId = new Mongo.ObjectID(Router.current().params._id);
                const actionOne = Actions.findOne({ _id: actionObjectId });
                const parentObjectId = new Mongo.ObjectID(actionOne.parentId);

                if (actionOne.parentType === 'events') {
                  // events
                  const projectOne = Projects.find({ _id: parentObjectId });
                  eventsCountOrga({
                    _id: Router.current().params._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                  });
                } else if (actionOne.parentType === 'projects') {
                  // projects
                  const project = `links.projects.${actionOne.parentId}`;
                  const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                  projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
                } else if (actionOne.parentType === 'organizations') {
                  Session.setPersistent('orgaCibleId', actionOne.parentId);
                } else if (actionOne.parentType === 'citoyens') {
                  // citoyens
                }
              }
            } else if (countOrga === 1) {
              // console.log('news news.js 144 countOrga === 1', Session.get('orgaCibleId'));
            }
          }
        }

        this.readyScopeDetail.set(handle.ready());
      });


    }
  }.bind(this));
});

Template.projectsMilestonesListActions.helpers({
  scope() {
    if (Router.current().params.scope) {
      const collection = nameToCollection(Router.current().params.scope);
      const scope = collection.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
      return collection.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    }
    // return undefined;
  },
  dataReadyScopeDetail() {
    return Template.instance().readyScopeDetail.get();
  },
  scopeMe() {
    return pageSession.get('scope') === 'citoyens' && Router.current().params._id === Meteor.userId();
  },
});

Template.projectsMilestonesListActionsItems.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    pageSession.set('milestoneId', Router.current().params.milestoneId);
    // const selectsubview = pageSession.get('selectsubview') ? pageSession.get('selectsubview') : 'todo';
    const paramPub = { scope: Router.current().params.scope, scopeId: Router.current().params._id, etat: 'all', milestoneId: Router.current().params.milestoneId };
    if (searchAction.get('search')) {
      paramPub.search = searchAction.get('search');
    }
    if (searchAction.get('searchSort')) {
      paramPub.searchSort = searchAction.get('searchSort');
    }
    const handle = this.subscribe('directoryListActions', { scope: Router.current().params.scope, scopeId: Router.current().params._id, etat: 'all', milestoneId: Router.current().params.milestoneId });

    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.projectsMilestonesListActionsItems.helpers({
  search() {
    return searchAction.get('search');
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  milestoneId() {
    return pageSession.get('milestoneId');
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.projectsMilestonesListActionsItems.events({
  'click .change-selectsubview-js'(event) {
    event.preventDefault();
    pageSession.set('selectsubview', event.currentTarget.id);
  },
});

Template.milestoneDetailCard.helpers({
  formatMilestoneDate(date) {
    return date && moment(date).format('L');
  },
  isStartDate() {
    if (this.milestone.startDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment(start).isBefore(); // True
    }
    return false;
  },
  isNotStartDate() {
    if (this.milestone.startDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment().isBefore(start); // True
    }
    return false;
  },
  isEndDate() {
    if (this.milestone.endDate) {
      const end = moment(this.milestone.endDate).toDate();
      return moment(end).isBefore(); // True
    }
    return false;
  },
  isNotEndDate() {
    if (this.milestone.endDate) {
      const end = moment(this.milestone.endDate).toDate();
      return moment().isBefore(end); // True
    }
    return false;
  },
  isInProgress() {
    if (this.milestone.startDate && this.milestone.endDate) {
      const start = moment(this.milestone.startDate).toDate();
      const end = moment(this.milestone.endDate).toDate();
      return moment().isBefore(end) && moment(start).isBefore(); // True
    } if (this.milestone.startDate && !this.milestone.endDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment(start).isBefore(); // True
    }
    return false;
  },
  badgeStatus(status) {
    return status && status === 'open' ? 'balanced' : 'assertive';
  },
  boutonsAction() {
    return { event: 'button-positive action-project-milestone-js', icon: 'fa fa-cog' };
  },
});
