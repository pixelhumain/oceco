import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';

import { Citoyens } from '../../../api/collection/citoyens.js';
import { Projects } from '../../../api/collection/projects.js';

import { scrollPagination } from '../../../api/helpers.js';

import './contributors.html';

const pageSession = new ReactiveDict('pageContributors');

Template.listContributors.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });

  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  self.autorun(function () {
    const paramPub = { scopeId: Router.current().params._id, limit: self.limit.get('limit') };
    if (pageSession.get('search')) {
      paramPub.search = pageSession.get('search');
    }

    const handle = Meteor.subscribe('listContributors', paramPub);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });
});

Template.listContributors.onRendered(scrollPagination);

Template.listContributors.helpers({
  projects() {
    return Projects.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
  },
  onlineContributors() {
    const user = Meteor.users.findOne({ _id: this._id.valueOf() });
    return user && user.profile && user.status.online;
  },
  isFollowsContributors(followId) {
    if (Meteor.userId() === followId) {
      return true;
    }
    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { links: 1 } });
    return citoyen.links && citoyen.links.follows && citoyen.links.follows[followId];
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

Template.listContributors.events({
  'click .followperson-link'(event) {
    event.preventDefault();
    Meteor.call('followEntity', this._id.valueOf(), 'citoyens');
  },
  'click .unfollowperson-link'(event) {
    event.preventDefault();
    Meteor.call('disconnectEntity', this._id.valueOf(), 'citoyens');
  },
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});
