/* eslint-disable meteor/no-session */
/* global Session IonPopup IonModal IonToast */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Router } from 'meteor/iron:router';
import i18n from 'meteor/universe:i18n';
import { moment } from 'meteor/momentjs:moment';

// collection
import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Actions } from '../../api/collection/actions.js';

import { searchAction } from '../../api/client/reactive.js';
import { searchQuery, searchQuerySort } from '../../api/helpers.js';

import './polesView.html';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;

Template.polesView.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.autorun(function () {
    const poleName = Router.current().params.pole;
    const subs = [
      this.subscribe('poles.actions2', Session.get('orgaCibleId'), poleName),
      this.subscribe('polesOrAll.events', Session.get('orgaCibleId'), poleName)
    ];

    Tracker.autorun(() => {
      // Vérifier si tous les abonnements sont prêts et mettre à jour isReady en conséquence
      const areAllSubsReady = subs.every(handle => handle.ready());
      this.ready.set(areAllSubsReady);
    });
  }.bind(this));
});

Template.polesView.helpers({
  poleName() {
    const poleName = Router.current().params.pole;
    return poleName;
  },

  poleProjects2() {
    const poleName = Router.current().params.pole;
    const queryProjectId = `parent.${Session.get('orgaCibleId')}`;
    const projectId = Projects.find({ [queryProjectId]: { $exists: 1 } }).fetch();
    const projectsId = [];
    projectId.forEach((element) => {
      projectsId.push(element._id);
    });
    const poleProjectsCursor = Projects.find({ $and: [{ tags: poleName }, { _id: { $in: projectsId } }] });
    return poleProjectsCursor;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.projectList2.onCreated(function () {
  this.scroll = new ReactiveVar(false);
});

Template.eventsList2.onCreated(function () {
  this.scrollAction = new ReactiveVar(false);
});

Template.projectList2.helpers({
  projectEvents(projectObjectId) {
    const query = {};
    const projectId = projectObjectId.valueOf();
    query[`organizer.${projectId}`] = { $exists: true };

    const options = {};
    const searchSort = searchAction.get('searchSort');
    if (searchSort) {
      const arraySort = searchQuerySort('events', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      }
    }

    return Events.find(query, options);
  },
  projectGlobalCount(projectObjectId) {
    const dataContext = Template.currentData();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    const projectId = projectObjectId.valueOf();
    if (search && search.charAt(0) === ':' && search.length > 1) {
      return true;
    }
    let query = {};
    const organizerExist = `organizer.${projectId}`;
    query.parentId = projectId;
    query.status = 'todo';
    if (search) {
      query = searchQuery(query, search);
    }

    return Events.find({ [organizerExist]: { $exists: true } }).count() > 0 || Actions.find(query).count() > 0;
  },
  projectActionsEventsCount(projectObjectId) {
    const query = {};
    const projectId = projectObjectId.valueOf();
    query[`organizer.${projectId}`] = { $exists: true };
    // comment ajouter aussi les actions du projets
    const queryActions = {};
    queryActions.parentId = projectId;
    queryActions.status = 'todo';
    const totalCOunt = Actions.find(queryActions).count() + Events.find(query).count();
    return totalCOunt;
  },
  projectEventsCount(projectObjectId) {
    const query = {};
    const projectId = projectObjectId.valueOf();
    query[`organizer.${projectId}`] = { $exists: true };
    return Events.find(query).count();
  },
  projectActionsCount(projectObjectId) {
    const dataContext = Template.currentData();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    const projectId = projectObjectId.valueOf();

    let query = {};
    query.parentId = projectId;
    query.status = 'todo';
    if (search) {
      query = searchQuery(query, search);
    }

    return Actions.find(query).count();
  },
  projectActions(projectObjectId) {
    const dataContext = Template.currentData();
    const projectId = projectObjectId.valueOf();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    let query = {};
    query.parentId = projectId;
    query.status = 'todo';
    if (search) {
      query = searchQuery(query, search);
    }

    const options = {};
    const searchSort = searchAction.get('searchSort');
    if (searchSort) {
      const arraySort = searchQuerySort('actions', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      }
    }

    return Actions.find(query, options);
  },
  scroll() {
    const search = searchAction.get('search');
    if (search && !search.startsWith('?')) {
      return true;
    }
    return Template.instance().scroll.get();
  },
  classesProject(scroll) {
    const classes = [];
    classes.push('item');
    if (scroll) {
      classes.push('item-positive');
    } else {
      classes.push('item-stable');
    }
    classes.push('item-avatar item-content item-button-right');
    return classes.join(' ');
  },
  classesProjectTitle(scroll) {
    const classes = [];
    if (scroll) {
      classes.push('light');
    } else {
      classes.push('dark');
    }
    return classes.join(' ');
  },
});

Template.organizationList.onCreated(function () {
  this.scrollOrga = new ReactiveVar(false);
});

Template.organizationList.helpers({
  organizationActionsCount() {
    const dataContext = Template.currentData();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    let query = {};
    query.parentId = Session.get('orgaCibleId');
    query.status = 'todo';
    if (search) {
      query = searchQuery(query, search);
    }

    return Actions.find(query).count() > 0;
  },
  organizationActions() {
    const dataContext = Template.currentData();
    const search = dataContext && dataContext.user ? `@${dataContext.user.username}` : searchAction.get('search');
    // const search = searchAction.get('search');
    let query = {};
    query.parentId = Session.get('orgaCibleId');
    query.status = 'todo';
    if (search) {
      query = searchQuery(query, search);
    }

    const options = {};
    const searchSort = searchAction.get('searchSort');
    if (searchSort) {
      const arraySort = searchQuerySort('actions', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      }
    }

    return Actions.find(query, options);
  },
  scrollOrga() {
    const search = searchAction.get('search');
    if (search) {
      return true;
    }
    return Template.instance().scrollOrga.get();
  },
});

Template.organizationList.events({
  'click .button-see-orga-js'(event) {
    event.preventDefault();
    if (Template.instance().scrollOrga.get()) {
      Template.instance().scrollOrga.set(false);
    } else Template.instance().scrollOrga.set(true);
  },
});

Template.eventsList2.helpers({
  scrollAction() {
    return Template.instance().scrollAction.get();
  },
});

Template.projectList2.events({
  'click .button-see-event-js'(event) {
    event.preventDefault();
    if (Template.instance().scroll.get()) {
      Template.instance().scroll.set(false);
    } else Template.instance().scroll.set(true);
  },
});
Template.eventsList2.events({
  'click .button-see-actions-js'(event) {
    event.preventDefault();
    if (Template.instance().scrollAction.get()) {
      Template.instance().scrollAction.set(false);
    } else Template.instance().scrollAction.set(true);
  },
});
Template.eventsList2.helpers({
  eventAction(eventId) {
    const userAddedAction = `links.contributors.${Meteor.userId()}`;
    const search = searchAction.get('search');
    let query = {};
    query.parentId = eventId;
    query.status = 'todo';
    query[userAddedAction] = { $exists: false };
    if (search) {
      query = searchQuery(query, search);
    }

    const options = {};
    const searchSort = searchAction.get('searchSort');
    if (searchSort) {
      const arraySort = searchQuerySort('actions', searchSort);
      if (arraySort) {
        options.sort = arraySort;
      }
    }

    return Actions.find(query, options);
  },
  eventActionCount(eventId) {
    const userAddedAction = `links.contributors.${Meteor.userId()}`;
    const search = searchAction.get('search');
    let query = {};
    query.parentId = eventId;
    query.status = 'todo';
    query[userAddedAction] = { $exists: false };
    if (search) {
      query = searchQuery(query, search);
    }
    return Actions.find(query).count();
  },
});

Template.itemInputAction.onCreated(function () {
  const dataContext = Template.currentData();
  this.displayDesc = new ReactiveVar(false);
});

Template.itemInputAction.events({
  'click .display-desc-js'(event) {
    event.preventDefault();
    if (!Template.instance().displayDesc.get()) {
      Template.instance().displayDesc.set(true);
    } else {
      Template.instance().displayDesc.set(false);
    }
  },
  'click .action-redirect-js'(event) {
    event.preventDefault();
    Router.go('actionsDetailOrga', {
      orgaCibleId: Session.get('orgaCibleId'), _id: this.action.parentId, scope: this.action.parentType, roomId: this.action.idParentRoom, actionId: this.action._id.valueOf(),
    });
  },
  'click .action-modal-comment-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      _id: this.action.parentId, scope: this.action.parentType, roomId: this.action.idParentRoom, actionId: this.action._id.valueOf(),
    };
    IonModal.open('actionsDetailCommentsModal', parentDataContext);
  },
  'contextmenu .action-modal-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      _id: this.action.parentId, scope: this.action.parentType, roomId: this.action.idParentRoom, actionId: this.action._id.valueOf(),
    };
    if (this.viewScope !== 'citoyens') {
      parentDataContext.orgaCibleId = Session.get('orgaCibleId');
      IonModal.open('detailActionsModal', parentDataContext);
    }
  },
  'click .action-edit-modal-js'(event) {
    event.preventDefault();
    const parentDataContext = {
      orgaCibleId: Session.get('orgaCibleId'), _id: this.action.parentId, scope: this.action.parentType, roomId: this.action.idParentRoom, actionId: this.action._id.valueOf(),
    };
    IonModal.open('actionsEditModal', parentDataContext);
  },
  'click .assign-jalon-action-js'(event) {
    event.preventDefault();
    Meteor.call('milestoneAssignAction', { scopeId: this.action.parentId, actionId: this.action._id.valueOf(), milestoneId: this.milestoneIdCible }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Milestone assignment'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('this milestone has been assigned to this action')}`,
        });
      }
    });
  },
  'click .tracking-js'(event) {
    event.preventDefault();
    const tracking = this.action.tracking !== true;
    Meteor.call('trackingAction', { actionId: this.action._id.valueOf(), tracking }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Tracking'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__(tracking ? 'tracking this action' : 'untracking this action')}`,
        });
      }
    });
  },
});

Template.itemInputActionTags.events({
  'click .searchtag-js'(event) {
    event.preventDefault();
    if (this) {
      searchAction.setPersistent('search', `#${this}`);
    } else {
      searchAction.setPersistent('search', null);
    }
  },
});

Template.itemInputActionMilestone.events({
  'click .searchmilestone-js'(event) {
    event.preventDefault();
    if (this && this.milestone && this.milestone.name) {
      searchAction.setPersistent('search', `~${this.milestone.name}`);
    } else {
      searchAction.setPersistent('search', null);
    }
  },
});

Template.itemInputAction.helpers({
  displayDesc() {
    return Template.instance().displayDesc.get();
  },
});

Template.itemInputActionDetail.inheritsHelpersFrom('itemInputAction');

Template.buttonSubscribeAction.onCreated(function () {
  this.state = new ReactiveDict();
  this.state.setDefault({
    call: false,
  });
});

Template.buttonSubscribeAction.helpers({
  isCall() {
    return Template.instance().state.get('call');
  },
  isCallChange() {
    Template.instance().state.set('call', false);
  },
  startDateDefault() {
    return moment().format('YYYY-MM-DDTHH:mm');
  },
});

Template.buttonSubscribeAction.events({
  'submit .form-assignme-js'(event, instance) {
    event.preventDefault();
    instance.state.set('call', true);
    const action = { id: this._id.valueOf() };

    if (!this.startDate && event.target && event.target.startDate && event.target.startDate.value) {
      action.startDate = moment(event.target.startDate.value).format('YYYY-MM-DDTHH:mm:ssZ');
      // console.log(action.startDate);
    }
    if (!this.endDate && event.target && event.target.endDate && event.target.endDate.value) {
      action.endDate = moment(event.target.endDate.value).format('YYYY-MM-DDTHH:mm:ssZ');
      // console.log(action.endDate);
    }
    if (this.isCreditAddPorteur() && event.target && event.target.credits && event.target.credits.value) {
      action.credits = parseInt(event.target.credits.value);
      // console.log(action.credits);
    }
    if (this.isPossibleStartActionBeforeStartDate() && event.target && event.target.startAction && event.target.startAction.checked) {
      action.startDate = moment().format('YYYY-MM-DDTHH:mm:ssZ');
      // console.log(action.credits);
    }

    Meteor.call('assignmeActionRooms', action, (error) => {
      if (error) {
        instance.state.set('call', false);
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
  'click .action-assignme-js'(event, instance) {
    event.preventDefault();
    instance.state.set('call', true);
    Meteor.call('assignmeActionRooms', { id: this._id.valueOf() }, (error) => {
      if (error) {
        instance.state.set('call', false);
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
  'click .action-depenseme-js'(event, instance) {
    event.preventDefault();
    const self = this;
    instance.state.set('call', true);
    IonPopup.confirm({
      title: 'Depenser',
      template: 'Voulez vous depenser vos credits ?',
      onOk() {
        Meteor.call('assignmeActionRooms', {
          id: self._id.valueOf(),
        }, (error) => {
          if (error) {
            instance.state.set('call', false);
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
            });
          }
        });
      },
      onCancel() {
        instance.state.set('call', false);
      },
      cancelText: i18n.__('no'),
      okText: i18n.__('yes'),
    });
  },
});
