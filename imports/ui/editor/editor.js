import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import {
  receiveUpdates,
  sendableUpdates,
  collab,
  getSyncedVersion,
  getClientID
} from '@codemirror/collab';
import { basicSetup } from 'codemirror';
import { ChangeSet, EditorState, Text, StateEffect, StateField, EditorSelection, Prec } from '@codemirror/state';
import { EditorView, ViewPlugin, showTooltip, keymap, showPanel } from '@codemirror/view';
import { defaultKeymap, indentMore, indentLess, history, historyKeymap, undo, redo } from '@codemirror/commands';
import { searchKeymap } from '@codemirror/search';
import { markdown, markdownLanguage } from '@codemirror/lang-markdown';
import { htmlCompletionSource } from "@codemirror/lang-html"
import { languages } from '@codemirror/language-data';
import { autocompletion, completeFromList, insertCompletionText } from "@codemirror/autocomplete";

import Reveal from 'reveal.js';

import NotesPlugin from 'reveal.js/plugin/notes/notes.esm.js';
// import HighlightPlugin from 'reveal.js/plugin/highlight/highlight.esm.js';
// import ZoomPlugin from 'reveal.js/plugin/zoom/zoom.esm.js';
// import 'reveal.js/plugin/highlight/monokai.css';

import { Router } from 'meteor/iron:router';

import { Notes } from '/imports/api/collection/notes.js';

import './editor.html';
import { getBackgroundColor, getContrastingTextColor, nameToCollection } from '/imports/api/helpers';

import { orgaCible, pageSession } from '../../api/client/reactive.js';
import { Citoyens } from '/imports/api/collection/citoyens';
import { methodCall } from '/imports/infra/methodCall';
import { extraireEtParserYAML } from '/imports/startup/client/parser-yaml';

const toggleEditorAndView = () => {
  // console.log('Edit!');
  pageSession.set('isEditor', !pageSession.get('isEditor'));
  // Sélectionne l'élément '.ionic-body' pour vérifier s'il possède la classe 'fullscreen'
  if (document.querySelector('.ionic-body').classList.contains('fullscreen')) {
    // Simule un clic sur l'élément '#tool-fullscreen'
    document.getElementById('tool-fullscreen').click();
    // Bascule la classe 'hidden' sur l'élément '#mardown'
    document.getElementById('mardown').classList.toggle('hidden');
  } else {
    // Bascule la classe 'hidden' sur l'élément '#mardown' si '.ionic-body' n'a pas la classe 'fullscreen'
    document.getElementById('mardown').classList.toggle('hidden');
  }
  // Bascule la classe 'hidden' sur l'élément '#editors'
  document.getElementById('editors').classList.toggle('hidden');
};

const toggleFullscreen = () => {
  // Basculer la classe 'fullscreen' sur '.ionic-body'
  document.querySelector('.ionic-body').classList.toggle('fullscreen');
  const element = document.getElementById('tool-fullscreen');
  // Basculer la classe 'overflow-scroll' sur '.content'
  document.querySelectorAll(".content").forEach((content) => {
    content.classList.toggle("overflow-scroll");
  });

  if (document.querySelector('.ionic-body').classList.contains('fullscreen')) {
    // Si en mode plein écran
    if (pageSession.get('isPreview') === true) {
      // Simuler un clic sur '#tool-preview' si 'isPreview' est vrai
      document.getElementById("tool-preview").click();
    }
    pageSession.set('fullscreen', true);
    // Modifier l'icône pour indiquer l'état compressé
    element.innerHTML = '<i class="icon fa fa-compress-arrows-alt"></i>';
  } else {
    // Si pas en mode plein écran
    if (pageSession.get('isPreview') === true) {
      // Simuler un clic sur '#tool-preview' si 'isPreview' est vrai
      document.getElementById("tool-preview").click();
    }
    pageSession.set('fullscreen', false);
    // Modifier l'icône pour indiquer l'état étendu
    element.innerHTML = '<i class="icon fa fa-expand-arrows-alt"></i>';
  }

  // Basculer les classes supplémentaires sur '.content'
  const contents = document.querySelectorAll('.content');
  if (contents.length >= 3) {
    const thirdContent = contents[2];
    thirdContent.classList.toggle('has-header');
    thirdContent.classList.toggle('overflow-scroll');
  }

};

const togglePreview = () => {
  pageSession.set('isPreview', !pageSession.get('isPreview'));
  if (pageSession.get('fullscreen') === true) {
    // Basculement des classes pour l'aperçu en plein écran
    document.getElementById("mardown").classList.toggle("hidden");
    document.getElementById("mardown").classList.toggle("child");
    document.getElementById("editors").classList.toggle("child");
    document.getElementById("editor-container").classList.toggle("cm-container");
  } else {
    // Logique pour l'aperçu sans plein écran, si nécessaire
  }
};


Template.editor.onCreated(function () {
  this.ready = new ReactiveVar(false);
  pageSession.set('fullscreen', false);
  pageSession.set('isDelete', false);
  pageSession.set('isPreview', false);
  pageSession.set('scopeId', null);
  pageSession.set('scope', null);
  pageSession.set('docId', null);
  pageSession.set('version', 0);
  pageSession.set('doc', null);
  pageSession.set('isNew', false);
  pageSession.set('isEditor', false);
  pageSession.set('dataReadyMethod', false);
  pageSession.set('oldStatus', Meteor.status().connected);


  // on récupère les paramètres de l'url pour les stocker dans la session
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    pageSession.set('docId', Router.current().params.docId ? Router.current().params.docId : undefined);
    pageSession.set('version', 0);
    if (Router.current().params.query && Router.current().params.query.isEditor) {
      pageSession.set('isNew', Router.current().params.query.isEditor === 'true' ? false : true);
      pageSession.set('isEditor', Router.current().params.query.isEditor === 'true' ? true : false);
    } else {
      pageSession.set('isNew', Router.current().params.docId ? true : false);
      pageSession.set('isEditor', Router.current().params.docId ? false : true);
    }
  }.bind(this));

  // le subscribe ce fait que si le docId est présent
  this.autorun(function () {
    const docId = pageSession.get('docId');
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (docId && scope && scopeId) {
      // console.log('subscribe documentUpdates');
      const handleScope = this.subscribe('documentUpdates', scope, scopeId, docId);
      Tracker.autorun(() => {
        const isReady = handleScope.ready();
        // console.log('Subscription is ready:', isReady);
        this.ready.set(isReady);
      });
    }
  }.bind(this));

  this.autorun(function () {
    const currentStatus = Meteor.status().connected;
    // Actions spécifiques en fonction du changement de statut
    if (pageSession.get('oldStatus') === false && currentStatus === true) {
      console.log('Rechargement nécessaire.');
      pageSession.set('dataReadyMethod', false);
      location.reload();
    }

    if (currentStatus !== pageSession.get('oldStatus')) {
      console.log(`Statut changé de ${pageSession.get('oldStatus')} à ${currentStatus}`);
      // Mise à jour de oldStatus avec le nouveau statut
      pageSession.set('oldStatus', currentStatus);
    }

  });
});

Template.wakeLock.onCreated(function () {
  this.wakeLock = null;
  if ("wakeLock" in navigator) {
    const self = this;
    (async function () {
      try {
        self.wakeLock = await navigator.wakeLock.request("screen");
        // console.log("Wake Lock is active");
      } catch (err) {
        // the wake lock request fails - usually system related, such being low on battery
        console.log(`${err.name}, ${err.message}`);
      }
    })();
  }
});


Template.wakeLock.onDestroyed(function () {
  const self = this;
  if (self.wakeLock) {
    self.wakeLock.release().then(() => {
      self.wakeLock = null;
      // console.log("Wake Lock is released");
    });
  }
});

function getDocument(scope, scopeId, docId) {
  return new Promise((resolve, reject) => {
    Meteor.call('getOrCreateDocument', scope, scopeId, docId, (error, result) => {
      if (error) {
        reject(error);
      } else {
        resolve({
          version: result.version,
          doc: result.doc,
          newDocId: result.newDocId
        });
      }
    });
  });
}

Template.editor.onRendered(async function () {
  this.autorun(async function () {
    pageSession.set('dataReadyMethod', false);
    // Ici, vous récupérez les données nécessaires avant d'initialiser CodeMirror.
    try {
      let { version, doc, newDocId } = await getDocument(
        pageSession.get('scope'),
        pageSession.get('scopeId'),
        pageSession.get('docId') ? pageSession.get('docId') : undefined
      );

      if (newDocId) {
        pageSession.set('docId', newDocId);
      }
      if (version) {
        pageSession.set('version', version);
      }

      pageSession.set('doc', doc ?? "");


      // Mise à jour de l'URL, si nécessaire.
      if (pageSession.get('isNew') === false) {
        Router.go('notesEditor',
          { _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), docId: pageSession.get('docId') },
          { query: 'isEditor=true', replaceState: true }
        );
      }

      // console.log('dataReadyMethod', true);
      pageSession.set('dataReadyMethod', true);
    } catch (error) {
      pageSession.set('dataReadyMethod', true);
    }
  }.bind(this));
});

Template.editor.helpers({
  scope() {
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (scope && scopeId) {
      const collection = nameToCollection(scope);
      return collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
    }
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  dataReadyMethod() {
    return pageSession.get('dataReadyMethod');
  },
  doc() {
    if (pageSession.get('docId')) {
      const document = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) });
      return document;
    }
  },
  isEditor() {
    return pageSession.get('isEditor');
  },
  isNew() {
    return pageSession.get('isNew');
  },
  isFullscreen() {
    return pageSession.get('fullscreen');
  },
  isDelete() {
    return pageSession.get('isDelete');
  },
  isConnected() {
    return Meteor.status().connected;
  }
});

Template.editor.events({
  'contextmenu .content'(event, instance) {
    if (event.target.closest('.exclude-dblclick')) {
      return;
    }

    event.preventDefault();
    document.getElementsByClassName('action-card-notes-js')[0].click();
  },
  'dblclick .content'(event, instance) {
    if (event.target.closest('.exclude-dblclick')) {
      return;
    }
    event.preventDefault();
    toggleEditorAndView();
  },
});

Template.editorCms.helpers({
  isAdminOrContributors(scope, doc) {
    if (scope && !doc) {
      return scope.isAdmin();
    } else if (!scope && doc) {
      return doc.isContributors() && !doc.isLock();
    } else if (scope && doc) {
      return scope.isAdmin() || (doc.isContributors() && !doc.isLock());
    }
    return false;
  }
});

Template.editorCms.events({
  'click .table-of-contents a'(event, instance) {
    event.preventDefault();
    var hash = event.target.getAttribute('href').substring(1); // Supprime le '#' du début
    var element = document.getElementById(hash);
    if (element) {
      element.scrollIntoView();
    }
  },
});

Template.editorMardownView.onRendered(function () {

  const markdownContent = this.find("#mardown-content");
  const menuContainer = this.find("#menu-container-view");
  const instance = this;

  function createHtmlMenu(filter = "") {
    // Supprimer uniquement les anciens items du menu, mais garder la recherche
    const menu = menuContainer.querySelector(".table-of-contents-html");
    if (menu) menu.remove();

    const newMenu = document.createElement("ul");
    newMenu.className = "table-of-contents-html";

    // Trouver tous les titres (h1, h2, h3, etc.) dans le contenu
    const headings = Array.from(markdownContent.querySelectorAll("h1, h2, h3, h4, h5, h6"));

    if (headings.length === 0) {
      // Si aucun titre n'est trouvé, afficher un message
      const noHeadingsMessage = document.createElement("p");
      noHeadingsMessage.textContent = "Aucun titre disponible.";
      noHeadingsMessage.style.textAlign = "center";
      noHeadingsMessage.style.color = "#666";
      noHeadingsMessage.style.fontStyle = "italic";

      // Nettoyer et ajouter le message
      menuContainer.appendChild(noHeadingsMessage);

      // Fermer automatiquement après une courte durée
      setTimeout(() => toggleMenu(false), 2000);
      return;
    }

    // Filtrer les titres si un filtre est défini
    const filteredHeadings = headings.filter((heading) =>
      heading.textContent.toLowerCase().includes(filter.toLowerCase())
    );

    if (filteredHeadings.length === 0) {
      let noResultsMessage = menuContainer.querySelector(".no-results-message");

      // Si le message n'existe pas encore, le créer
      if (!noResultsMessage) {
        noResultsMessage = document.createElement("p");
        noResultsMessage.className = "no-results-message";
        noResultsMessage.textContent = "Aucun titre correspondant.";
        noResultsMessage.style.textAlign = "center";
        noResultsMessage.style.color = "#666";
        noResultsMessage.style.fontStyle = "italic";

        // Ajouter le message d'absence de résultats
        menuContainer.appendChild(noResultsMessage);
      }
      return;
    } else {
      // Supprimer le message d'absence de résultats s'il existe
      const existingNoResultsMessage = menuContainer.querySelector(".no-results-message");
      if (existingNoResultsMessage) {
        existingNoResultsMessage.remove();
      }
    }

    filteredHeadings.forEach((heading) => {
      const item = document.createElement("li");
      const level = parseInt(heading.tagName.substring(1), 10); // h1 -> 1, h2 -> 2, etc.
      item.style.marginLeft = `${(level - 1) * 10}px`; // Indentation
      item.textContent = heading.textContent;

      // Vérifier si l'élément est visible dans la fenêtre
      if (isElementInView(heading)) {
        item.classList.add("active"); // Indiquer que l'utilisateur est sur cette section
        item.setAttribute("title", "Vous êtes déjà ici");
      }

      // Ajouter un événement de clic pour scroller vers la section
      item.addEventListener("click", (event) => {
        if (item.classList.contains("active")) {
          event.preventDefault(); // Empêcher le clic si on est déjà sur la section
          return;
        }

        heading.scrollIntoView({ behavior: "smooth", block: "start" });

        // Masquer le menu après clic
        toggleMenu(false);
      });

      newMenu.appendChild(item);
    });

    // Ajouter le menu au conteneur
    menuContainer.appendChild(newMenu);
  }

  // Fonction pour vérifier si un élément est visible dans la fenêtre
  function isElementInView(element) {
    const rect = element.getBoundingClientRect();
    return rect.top >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight);
  }

  // Fonction pour afficher ou masquer le menu
  function toggleMenu(show) {
    if (show) {
      menuContainer.style.display = "block";
      menuContainer.style.position = "fixed";
      menuContainer.style.top = "50%";
      menuContainer.style.left = "50%";
      menuContainer.style.transform = "translate(-50%, -50%)"; // Centre le menu horizontalement et verticalement
      menuContainer.style.zIndex = "1000"; // Place le menu au-dessus des autres éléments
      menuContainer.style.backgroundColor = "#fff"; // Ajout d'une couleur de fond pour la lisibilité
      menuContainer.style.boxShadow = "0 4px 6px rgba(0, 0, 0, 0.1)"; // Ombre pour esthétique
      menuContainer.style.padding = "20px"; // Ajoute un peu d'espace autour des éléments
      menuContainer.style.borderRadius = "8px"; // Coins arrondis pour un rendu moderne
      menuContainer.style.maxWidth = "80%"; // Limite la largeur du menu
      menuContainer.style.maxHeight = "80%"; // Limite la hauteur du menu
      menuContainer.style.overflowY = "auto"; // Permet le défilement si le contenu dépasse la hauteur

      // Ajouter un champ de recherche si ce n'est pas encore fait
      if (!menuContainer.querySelector(".menu-search")) {
        const searchInput = document.createElement("input");
        searchInput.type = "text";
        searchInput.placeholder = "Rechercher...";
        searchInput.className = "menu-search";
        searchInput.style.marginBottom = "10px";
        searchInput.style.width = "100%";
        searchInput.style.padding = "10px";
        searchInput.style.border = "1px solid #ccc";
        searchInput.style.borderRadius = "5px";

        // Ajouter un événement d'entrée pour la recherche
        searchInput.addEventListener("input", (event) => {
          createHtmlMenu(event.target.value); // Mettre à jour la liste filtrée
        });

        menuContainer.appendChild(searchInput);
      }

      // Vider le champ de recherche au moment d'ouvrir le menu
      const searchInput = menuContainer.querySelector(".menu-search");
      if (searchInput) {
        searchInput.value = "";
        searchInput.focus();
      }

      // Générer le menu initial
      createHtmlMenu();
    } else {
      // Vider le champ de recherche au moment de fermer le menu
      const searchInput = menuContainer.querySelector(".menu-search");
      if (searchInput) {
        searchInput.value = "";
      }
      menuContainer.style.display = "none";
    }
  }

  // Gestion des raccourcis clavier (Ctrl+K pour afficher, Esc pour cacher)
  instance.keydownHandler = (event) => {
    if (event.ctrlKey && event.key === "k") {
      event.preventDefault();
      event.stopPropagation();
      toggleMenu(true);
    }

    if (event.key === "Escape") {
      event.preventDefault();
      event.stopPropagation();
      toggleMenu(false);
    }

    if (event.altKey && event.key === "Enter") {
      event.preventDefault();
      event.stopPropagation();
      toggleEditorAndView();
      return true;
    }

  };

  document.addEventListener("keydown", instance.keydownHandler);

  // Initialisation : Masquer le menu par défaut
  toggleMenu(false);
});

Template.editorMardownView.onDestroyed(function () {
  // Supprimer keydownHandler à partir de l'instance
  if (this.keydownHandler) {
    document.removeEventListener("keydown", this.keydownHandler);
  }
});


Template.editorCodemiror.onRendered(function () {
  const instance = this;

  // Fonction pour extraire les titres du document Markdown
  function extractHeadings(markdown) {
    const lines = markdown.split("\n");
    return lines
      .map((line, index) => {
        const match = line.match(/^(#{1,6})\s+(.*)/);
        if (match) {
          return {
            level: match[1].length,
            text: match[2].trim(),
            line: index,
          };
        }
        return null;
      })
      .filter(Boolean);
  }

  function createMenu(headings, editorView) {
    const menuContainer = document.getElementById("menu-container");
    menuContainer.innerHTML = ""; // Nettoyer les anciens contenus

    // Vérifiez s'il y a des titres
    if (headings.length === 0) {
      const noHeadingsMessage = document.createElement("p");
      noHeadingsMessage.textContent = "Aucun titre disponible.";
      noHeadingsMessage.style.textAlign = "center";
      noHeadingsMessage.style.color = "#666";
      noHeadingsMessage.style.fontStyle = "italic";
      menuContainer.appendChild(noHeadingsMessage);

      // Fermer automatiquement le menu après 2 secondes
      setTimeout(() => toggleMenu(editorView, false), 2000);
      return;
    }

    // Créer le champ de recherche
    const searchInput = document.createElement("input");
    searchInput.type = "text";
    searchInput.placeholder = "Rechercher...";
    searchInput.className = "menu-search";
    searchInput.style.marginBottom = "10px";
    searchInput.style.width = "100%";
    searchInput.style.padding = "10px";
    searchInput.style.border = "1px solid #ccc";
    searchInput.style.borderRadius = "5px";

    menuContainer.appendChild(searchInput);

    const menu = document.createElement("ul");
    menu.className = "table-of-contents-markdown";
    menuContainer.appendChild(menu);

    // Fonction pour afficher les résultats filtrés
    function updateMenu(filteredHeadings) {
      menu.innerHTML = ""; // Nettoyer le contenu précédent

      if (filteredHeadings.length === 0) {
        let noResultsMessage = menu.querySelector(".no-results-message");

        // Si le message n'existe pas encore, le créer
        if (!noResultsMessage) {
          noResultsMessage = document.createElement("p");
          noResultsMessage.className = "no-results-message";
          noResultsMessage.textContent = "Aucun titre correspondant.";
          noResultsMessage.style.textAlign = "center";
          noResultsMessage.style.color = "#666";
          noResultsMessage.style.fontStyle = "italic";
          menu.appendChild(noResultsMessage);
        }
        return;
      }

      // Supprimer le message d'absence de résultats s'il existe
      const existingNoResultsMessage = menu.querySelector(".no-results-message");
      if (existingNoResultsMessage) {
        existingNoResultsMessage.remove();
      }

      // Afficher les titres filtrés
      filteredHeadings.forEach((heading) => {
        const item = document.createElement("li");
        item.style.marginLeft = `${heading.level * 10}px`; // Indentation en fonction du niveau
        item.textContent = heading.text;

        // Vérifiez si l'utilisateur est déjà sur la ligne
        const currentLine = editorView.state.doc.lineAt(editorView.state.selection.main.head).number;
        if (currentLine === heading.line + 1) {
          item.classList.add("active");
          item.setAttribute("title", "Vous êtes déjà ici");
        }

        item.addEventListener("click", () => {
          const pos = editorView.state.doc.line(heading.line + 1).from;

          if (currentLine === heading.line + 1) {
            event.preventDefault();
            return;
          }

          editorView.dispatch({
            effects: EditorView.scrollIntoView(pos, { y: "start" }),
            selection: { anchor: pos },
          });

          toggleMenu(editorView, false);
          editorView.focus();
        });

        menu.appendChild(item);
      });
    }

    // Initialiser le menu avec tous les titres
    updateMenu(headings);

    instance.searchInputHandler = (event) => {
      const query = searchInput.value.toLowerCase().trim();
      const filteredHeadings = headings.filter((heading) =>
        heading.text.toLowerCase().includes(query)
      );
      updateMenu(filteredHeadings);
    };

    searchInput.addEventListener("input", instance.searchInputHandler);
    // focus sur le champ de recherche
    searchInput.focus();
  }

  instance.escKeyListener = (event) => {
    if (event.key === "Escape") {
      const menuContainer = document.getElementById("menu-container");
      if (menuContainer && menuContainer.style.display === "block") {
        toggleMenu(null, false);
      }
    }
  }

  function toggleMenu(editorView, show) {
    const menuContainer = document.getElementById("menu-container");

    if (show) {
      const markdown = editorView.state.doc.toString();
      const headings = extractHeadings(markdown);
      createMenu(headings, editorView);

      menuContainer.style.display = "block";
      menuContainer.style.top = `calc(50% - ${menuContainer.offsetHeight / 2}px)`;
      menuContainer.style.left = `calc(50% - ${menuContainer.offsetWidth / 2}px)`;

      // Ajouter gestion d'ESC même pour le champ de recherche
      document.addEventListener("keydown", instance.escKeyListener);
    } else {
      menuContainer.style.display = "none";

      // Réinitialiser le champ de recherche quand le menu est fermé
      const searchInput = menuContainer.querySelector(".menu-search");
      if (searchInput) searchInput.value = "";

      // Supprimer le gestionnaire d'ESC
      document.removeEventListener("keydown", instance.escKeyListener);
    }
  }

  //!help
  const createHelpPanel = (view) => {
    let dom = document.createElement("div")
    dom.textContent = "F1: Toggle the help panel"
    dom.className = "cm-help-panel"
    return { top: true, dom }
  }

  const toggleHelp = StateEffect.define();

  const helpPanelState = StateField.define({
    create: () => false,
    update(value, tr) {
      for (let e of tr.effects) if (e.is(toggleHelp)) value = e.value
      return value
    },
    provide: f => showPanel.from(f, on => on ? createHelpPanel : null)
  })

  const helpKeymap = [{
    key: "F1",
    run(view) {
      view.dispatch({
        effects: toggleHelp.of(!view.state.field(helpPanelState))
      })
      return true
    }
  }];

  const helpTheme = EditorView.baseTheme({
    ".cm-help-panel": {
      padding: "5px 10px",
      backgroundColor: "#fffa8f",
      fontFamily: "monospace"
    }
  });

  const helpPanel = () => {
    return [helpPanelState, keymap.of(helpKeymap), helpTheme]
  }

  //!collab
  function pushUpdates(scope, scopeId, docId, version, fullUpdates) {
    let updates = fullUpdates.map(u => ({
      clientID: u.clientID,
      changes: u.changes.toJSON(),
      effects: u.effects.map(e => e.value)
    }));

    return new Promise((resolve, reject) => {
      Meteor.call('pushUpdates', scope, scopeId, docId, version, updates, (error, result) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      });
    });
  }

  const setPeerSelection = StateEffect.define({
    map: function ({ clientID, ranges }, changes) {
      return { clientID, ranges }
    }
  });

  let peerSelectionsField = StateField.define({
    create: () => new Map(),
    update(peerSelections, tr) {
      let newPeerSelections = new Map();
      for (let [k, v] of peerSelections) {
        const noteOne = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')), [`links.contributors.${k}.isPresence`]: { $exists: true } });
        if (!noteOne) {
          newPeerSelections.delete(k);
        } else {
          newPeerSelections.set(k, v);
        }
      }
      tr.effects.filter(e => e.is(setPeerSelection)).forEach(e => {
        newPeerSelections.set(e.value.clientID, {
          pos: e.value.ranges[0].to,
          above: true,
          strictSide: true,
          arrow: true,
          create: () => {
            let dom = document.createElement("div");
            dom.className = "cm-tooltip-cursor";
            const nameUser = Citoyens.findOne({ _id: new Mongo.ObjectID(e.value.clientID) }).name;
            const colorName = getBackgroundColor(nameUser);
            const colorText = getContrastingTextColor(colorName);
            dom.textContent = nameUser;
            dom.style.backgroundColor = colorName;
            dom.style.color = colorText;
            const arrow = document.createElement("div");
            arrow.className = "cm-tooltip-arrow";
            arrow.style.borderTopColor = colorName;
            dom.appendChild(arrow);
            return { dom };
          },
        });
      });
      return newPeerSelections;
    },
    provide: f => showTooltip.computeN([f], (state) => {
      let tooltips = [];
      for (let tooltip of state.field(f).values()) {
        tooltips.push(tooltip);
      }
      return tooltips;
    }),
  });

  //!peerExtension

  function peerExtension(startVersion) {

    let plugin = ViewPlugin.fromClass(
      class {
        pushing = false
        done = false

        constructor(view) {
          this.view = view
          instance.observer = null;
          this.pull();
        }

        async update(update) {
          if (update.docChanged) await this.push();
          if (update.selectionSet) await this.push();
        }



        async push() {
          let updates = sendableUpdates(this.view.state)
          if (this.pushing || !updates.length) return
          this.pushing = true
          let version = getSyncedVersion(this.view.state);
          pageSession.set('version', version);
          await pushUpdates(pageSession.get('scope'), pageSession.get('scopeId'), pageSession.get('docId'), version, updates);
          this.pushing = false
          if (sendableUpdates(this.view.state).length) {
            setTimeout(() => this.push(), 100)
          }
        }

        async pull() {
          instance.observer = Notes.find().observeChanges({
            added: (id, fields) => {
              if (fields.updates) {
                let version = getSyncedVersion(this.view.state)
                pageSession.set('version', version);
                if (fields.updates.length <= version) return;
                const newUpdates = fields.updates.slice(version);
                this.view.dispatch(receiveUpdates(this.view.state, newUpdates.map(u => ({
                  changes: ChangeSet.fromJSON(u.changes),
                  clientID: u.clientID,
                  effects: u.effects.map(e => setPeerSelection.of(e)),
                  ChangeUpdatedAt: u.ChangeUpdatedAt
                }))));
              }
            },
            changed: (id, fields) => {
              if (fields.updates) {
                let version = getSyncedVersion(this.view.state)
                pageSession.set('version', version);
                // if (fields.updates.length <= version) return;
                const newUpdates = fields.updates.slice(version);
                this.view.dispatch(receiveUpdates(this.view.state, newUpdates.map(u => ({
                  changes: ChangeSet.fromJSON(u.changes),
                  clientID: u.clientID,
                  effects: u.effects.map(e => setPeerSelection.of(e)),
                  ChangeUpdatedAt: u.ChangeUpdatedAt
                }))));
              }
            }
          });
        }

        destroy() {
          this.done = true;
          if (instance.observer) {
            instance.observer.stop();
            instance.observer = null;
          }
        }
      }
    )
    return [collab({
      startVersion, clientID: Meteor.userId(), sharedEffects: (transaction) => {

        if (transaction.selection) {
          return [setPeerSelection.of({
            clientID: getClientID(transaction.startState),
            ranges: transaction.selection.ranges,
          })];
        } else {
          return [];
        }
      }
    }), plugin]
  }

  //!rest
  function addPeer() {
    const docBrute = pageSession.get('doc');

    const doc = Text.of(docBrute.split("\n"));
    const version = pageSession.get('version');

    const frenchPhrases = {
      // @codemirror/view
      "Control character": "Caractère de contrôle",
      // @codemirror/commands
      "Selection deleted": "Sélection supprimée",
      // @codemirror/language
      "Folded lines": "Lignes pliées",
      "Unfolded lines": "Lignes dépliées",
      "to": "à",
      "folded code": "code plié",
      "unfold": "déplier",
      "Fold line": "Plier la ligne",
      "Unfold line": "Déplier la ligne",
      // @codemirror/search
      "Go to line": "Aller à la ligne",
      "go": "aller",
      "Find": "Trouver",
      "Replace": "Remplacer",
      "next": "suivant",
      "previous": "précédent",
      "all": "tous",
      "match case": "respecter la casse",
      "by word": "par mot",
      "replace": "remplacer",
      "replace all": "tout remplacer",
      "close": "fermer",
      "current match": "correspondance actuelle",
      "replaced $ matches": "$ correspondances remplacées",
      "replaced match on line $": "correspondance remplacée à la ligne $",
      "on line": "à la ligne",
      // @codemirror/autocomplete
      "Completions": "Complétions",
      // @codemirror/lint
      "Diagnostics": "Diagnostics",
      "No diagnostics": "Pas de diagnostics",
    }


    const changeBySelectedLine = (state, f) => {
      let atLine = -1;
      return state.changeByRange(range => {
        let changes = [];
        for (let pos = range.from; pos <= range.to;) {
          let line = state.doc.lineAt(pos);
          if (line.number > atLine && (range.empty || range.to > line.from)) {
            f(line, changes, range);
            atLine = line.number;
          }
          pos = line.to + 1;
        }
        let changeSet = state.changes(changes);
        return {
          changes,
          range: EditorSelection.range(changeSet.mapPos(range.anchor, 1), changeSet.mapPos(range.head, 1))
        };
      });
    };

    const toggleCheckbox = ({ state, dispatch }) => {
      // Create or toggle markdown style check boxes: "- [ ]" and "- [x]", respecting indentation,
      // for all selected lines:
      let changes = changeBySelectedLine(state, (line, changes, range) => {
        let indent = line.text.search(/\S|$/);
        // Detect markdown bullet
        if ((line.text.substring(indent, indent + 2) == "- ") ||
          (line.text.substring(indent, indent + 2) == "* ")) {
          // Toggle an existing checkbox
          if (line.text.substring(indent + 2, indent + 5) == "[ ]") {
            changes.push({ from: line.from + indent + 3, to: line.from + indent + 4, insert: "x" });
          }
          else if (line.text.substring(indent + 2, indent + 5) == "[x]") {
            changes.push({ from: line.from + indent + 3, to: line.from + indent + 4, insert: " " });
          }
          else {
            // Add new checkbox
            changes.push({ from: line.from + indent + 2, to: line.from + indent + 2, insert: "[ ] " });
          }
        }
        else {
          // No bullet, add one with checkbox
          changes.push({ from: line.from + indent, to: line.from + indent, insert: "- [ ] " });
        }
      });
      if (!changes.changes.empty) {
        dispatch(state.update(changes));
      }
      return true;
    }


    const themeBase = EditorView.theme({
      "&": {
        color: "#324553",
        backgroundColor: "#f8f8f8"
      },
      ".cm-content": {
        caretColor: "#f8f8f8"
      },
      ".cm-cursor, .cm-dropCursor": {
        borderLeftColor: '#f8f8f8'
      },
      "&.cm-focused .cm-cursor": {
        borderLeftColor: "#b6bdc2",
      },

      "&.cm-focused > .cm-scroller > .cm-selectionLayer .cm-selectionBackground, .cm-selectionBackground, .cm-content ::selection": {
        backgroundColor: "#324553",
        color: "white"
      },
      ".cm-panels": { backgroundColor: '#f8f8f8', color: '#324553' },
      ".cm-panels.cm-panels-top": { borderBottom: "2px solid black" },
      ".cm-panels.cm-panels-bottom": { borderTop: "2px solid black" },

      ".cm-searchMatch": {
        backgroundColor: "#72a1ff59",
        outline: "1px solid #457dff"
      },
      ".cm-searchMatch.cm-searchMatch-selected": {
        backgroundColor: "#6199ff2f"
      },
      ".cm-activeLine": { backgroundColor: "#6699ff0b" },
      ".cm-selectionMatch": { backgroundColor: "#aafe661a" },
      "&.cm-focused .cm-matchingBracket, &.cm-focused .cm-nonmatchingBracket": {
        backgroundColor: "#bad0f847"
      },
      ".cm-gutters": {
        backgroundColor: "#f8f8f8",
        color: "#324553",
        borderRight: "1px dotted #f8f8f8"
      },
      "&.cm-focused .cm-gutters": {
        borderRight: "1px dotted #324553"
      },
      ".cm-activeLineGutter": {
        backgroundColor: '#324553',
        color: "white"
      },
    }, { dark: false });

    const cursorTooltipBaseTheme = EditorView.baseTheme({
      ".cm-tooltip.cm-tooltip-cursor": {
        backgroundColor: "#E33551",
        color: "white",
        border: "none",
        padding: "2px 7px",
        borderRadius: "4px",
        "& .cm-tooltip-arrow:before": {
          borderTopColor: "inherit"
        },
        "& .cm-tooltip-arrow:after": {
          borderTopColor: "transparent"
        }
      }
    })

    const underlineTheme = EditorView.baseTheme({
      ".cm-underline": { textDecoration: "underline 3px red" }
    })


    const footerPanel = (view) => {
      let dom = document.createElement("div");
      const docChange = updateDocChange(view);
      dom.textContent = `${updatePositionChange(view)} ${docChange.words} - ${docChange.char}`;
      return {
        dom,
        update(update) {
          if (update.selectionSet || update.docChanged) {
            const docChange = updateDocChange(update);
            dom.textContent = `${updatePositionChange(update)} ${docChange.words} - ${docChange.char}`;
          }
        }
      }
    };

    const updateMenu = (update) => {
      const doc = update?.state?.doc;
      const markdown = doc ? doc.toString() : "";
      const headings = extractHeadings(markdown);
      createMenu(headings, update);
    };

    const updateDocChange = (update) => {
      const doc = update?.state?.doc;
      const textContent = doc ? doc.toString() : "";
      const wordCount = textContent.split(/\s+/).filter(Boolean).length;
      const charCount = textContent.length;

      return { words: `Mots: ${wordCount}`, char: `Caractères: ${charCount}` };
    };

    const updateFooterDocChange = (update) => {
      const docChange = updateDocChange(update);

      document.getElementById("editor-wordCount").textContent = docChange.words;
      document.getElementById("editor-charCount").textContent = docChange.char;

    };

    const updatePositionChange = (update) => {
      const cursorPos = update?.state?.selection?.main?.head ?? 0;

      // Trouver la ligne correspondante et calculer le numéro de colonne
      const line = update?.state?.doc?.lineAt(cursorPos) ?? { number: 0, from: 0 };
      const lineNumber = line.number;
      const columnNumber = cursorPos - line.from + 1; // +1 pour un index basé sur 1
      const totalLines = update?.state?.doc?.lines ?? 0;

      return `Lignes: ${lineNumber}, Colonnes: ${columnNumber} - ${totalLines}`;
    };

    const updateFooterPositionChange = (update) => {
      const cursorPosition = updatePositionChange(update);

      document.getElementById("editor-cursorPosition").textContent = cursorPosition;

    };

    const regexCompletion = (regexToMatch, options) => (context) => {
      const match = context.matchBefore(regexToMatch);
      if (!match || (match.from === match.to && !context.explicit)) {
        return null
      }
      return {
        from: match.from,
        options: typeof options === 'function' ? options(context) : options,
      }
    }

    const basicCompletion = (regexToMatch, replace, description) => {
      return regexCompletion(regexToMatch, [{ label: replace, detail: description }])(context)
    }

    // autocomplete
    const customAutocomplete = (context) => {
      // Obtenir le texte avant le curseur
      const line = context.state.doc.lineAt(context.pos);
      const lineTextUpToCursor = line.text.slice(0, context.pos - line.from);

      // console.log("Autocomplétion appelée, position:", lineTextUpToCursor);

      // Vérifier si l'utilisateur a tapé "#"
      if (lineTextUpToCursor.endsWith("#")) {
        // Retourner les suggestions pour les niveaux de titre Markdown
        return completeFromList([
          { label: "# Titre 1", apply: "# Titre 1" },
          { label: "## Titre 2", apply: "## Titre 2" },
          { label: "### Titre 3", apply: "### Titre 3" },
          { label: "#### Titre 4", apply: "#### Titre 4" },
        ])(context);
      }

      return null;
    }

    //autocomplete contributors
    const contributorsAutocomplete = (context) => {
      const contributors = (context) => {
        const contributors = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) }).listContributorsUsername().map((contributor) => {
          return { label: `@${contributor.username}`, apply: `[@${contributor.username}](${Router.url('detailList', { scope: 'citoyens', _id: contributor._id.valueOf() })})` };
        });
        return contributors;
      }

      // console.log('regexCompletion')
      return regexCompletion(/@(?:[\w-+]+)?/, contributors)(context);
    }

    //autocomplete file
    const fileAutocomplete = (context) => {
      const files = (context) => {
        const files = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) }).docList()?.map((file) => {
          const fileUrl = `${Meteor.settings.public.urlimage}/upload/${file.moduleId}/${file.folder}/${file.name}`;
          const fileAplly = file.doctype === 'file' ? `[${file.name}](${fileUrl})` : `![${file.name}](${fileUrl})`;
          return { label: `! ${file.name}`, apply: fileAplly };
        });
        return files ? files : [];
      }

      // console.log('regexCompletion')
      return regexCompletion(/!(?:[\w-+]+)?/, files)(context);
    }

    // `<!-- .slide: data-background="${fileUrl}" -->`
    const imgAutocompleteSlideBackground = (context) => {
      const files = (context) => {
        const files = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) }).docList()?.fetch().filter((file) => file.doctype === 'image').map((file) => {
          const fileUrl = `${Meteor.settings.public.urlimage}/upload/${file.moduleId}/${file.folder}/${file.name}`;
          const fileAplly = `<!-- .slide: data-background="${fileUrl}" -->`;
          return { label: `!bg ${file.name}`, apply: fileAplly, detail: 'slide background' };
        });
        return files ? files : [];
      }

      // console.log('regexCompletion')
      return regexCompletion(/!(?:[\w-+]+)?/, files)(context);
    }


    const noteLinkAutocomplete = async (context) => {
      const word = context.matchBefore(/~(?:[\w-\s\u00C0-\u017F]+)?/);
      if (!word || word.from == word.to && !context.explicit) {
        return null;
      }

      const text = word.text.slice(1);

      // Récupération de la liste des notes
      const noteslist = await Meteor.callAsync('searchNotes', { search: text, scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId') });

      let suggestions = noteslist?.map((note) => {
        if (note.parentType === 'citoyens') {
          return {
            label: `~${note.name}`,
            apply: `[${note.name}](${Router.url('notesEditor', { _id: note.parentId, scope: note.parentType, docId: note._id.valueOf() })})`
          };
        } else {
          return {
            label: `~${note.name}`,
            apply: `[${note.name}](${Router.url('notesEditorOrga', { orgaCibleId: Session.get('orgaCibleId'), _id: note.parentId, scope: note.parentType, docId: note._id.valueOf() })})`
          };
        }
      }) ?? [];

      // Ajoute une option pour créer une note si aucun résultat n'est trouvé
      // if (suggestions.length === 0) {
      if (text.length > 4) {
        suggestions.push({
          label: `~${text}`,
          apply: async (view, completion, from, to) => {
            const note = await Meteor.callAsync('createNoteAuto', { parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: text });
            if (note) {
              const linkNote = pageSession.get('scope') === 'citoyens' ? `[${text}](${Router.url('notesEditor', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), docId: note.newDocId })})` : `[${text}](${Router.url('notesEditorOrga', { orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), docId: note.newDocId })})`;
              view.dispatch(insertCompletionText(view.state, linkNote, from, to));
            }
          },
          detail: 'Créer une note'
        });
      }
      // }

      // console.log(suggestions);

      return regexCompletion(/~(?:[\w-\s\u00C0-\u017F]+)?/, suggestions)(context);
    };

    const correctText = async (view, from, to) => {
      // Logique pour corriger le texte sélectionné
      IonLoading.show();
      try {
        const correctedText = await Meteor.callAsync('correctText', { text: view.state.doc.sliceString(from, to) });
        const transaction = view.state.update({
          changes: { from, to, insert: correctedText }
        });
        view.dispatch(transaction);
        IonLoading.hide();
      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }

    const reformulateText = async (view, from, to) => {
      // Logique pour reformuler le texte sélectionné
      IonLoading.show();
      try {
        const reformulatedText = await Meteor.callAsync('reformulateText', { text: view.state.doc.sliceString(from, to) });
        const transaction = view.state.update({
          changes: { from, to, insert: reformulatedText }
        });
        view.dispatch(transaction);
        IonLoading.hide();
      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }

    const generateMermaid = async (view, from, to) => {
      // Logique pour générer du code Mermaid à partir du texte sélectionné
      IonLoading.show();
      try {
        const generateMermaidDiagram = await Meteor.callAsync('generateMermaidDiagram', { text: view.state.doc.sliceString(from, to) });
        const transaction = view.state.update({
          changes: { from: to, to, insert: `\n\n${generateMermaidDiagram}` }
        });
        view.dispatch(transaction);
        IonLoading.hide();
      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }

    const markdownText = async (view, from, to) => {
      IonLoading.show();
      try {
        const markdownText = await Meteor.callAsync('markdownText', { text: view.state.doc.sliceString(from, to) });
        const transaction = view.state.update({
          changes: { from: to, to, insert: `\n\n${markdownText}` }
        });
        view.dispatch(transaction);
        IonLoading.hide();
      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }

    const respondToText = async (view, from, to) => {
      IonLoading.show();
      try {
        const respondToText = await Meteor.callAsync('respondToText', { text: view.state.doc.sliceString(from, to) });
        const transaction = view.state.update({
          changes: { from: to, to, insert: `\n\n${respondToText}` }
        });
        view.dispatch(transaction);
        IonLoading.hide();
      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }

    const createImageFromText = async (view, from, to) => {
      IonLoading.show();
      // view.state.doc.sliceString(from, to) doit être limiter à 1000 caractére

      if (view.state.doc.sliceString(from, to).length > 1000) {
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('The text is too long, it must be less than 1000 characters')}`,
        });
        return;
      }

      try {
        const createImageFromText = await Meteor.callAsync('createImageFromText', { text: view.state.doc.sliceString(from, to) });
        const scope = pageSession.get('scope');
        const scopeId = pageSession.get('scopeId');
        const docId = pageSession.get('docId');
        const str = `${Math.random().toString(36).substring(7)}.jpg`;
        IonLoading.hide();
        IonPopup.confirm({
          title: i18n.__('Image generated from text'),
          template: `<div class="item item-text-wrap item-image">
          <img src="${createImageFromText}" />
          <p>
          ${i18n.__('Do you want to insert the image into the note')}
          </p>
          </div>`,
          async onOk() {
            try {
              IonLoading.show();
              const result = await Meteor.callAsync('photoNotes', createImageFromText, str, scope, scopeId, docId);
              if (result && result.url) {
                IonToast.show({
                  template: str, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                });
                const transaction = view.state.update({
                  changes: { from: to, to, insert: `\n\n![${str}](${result.url})\n\n` }
                });
                view.dispatch(transaction);
                IonLoading.hide();

              }
            } catch (error) {
              // console.log('error',error);
              IonLoading.hide();
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
              });
            }
          },
          onCancel() {
            IonLoading.hide();
          },
          cancelText: i18n.__('cancel'),
          okText: i18n.__('ok'),
        });


      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }

    const createListActions = async (view, from, to) => {
      IonLoading.show();
      try {

        const selectedText = view.state.doc.sliceString(from, to);
        // Vérifier si la sélection est une liste et extraire chaque élément
        const listItems = selectedText.match(/^(\s*(\-|\*|\d+\.)\s+.+)$/gm);

        if (listItems) {
          // Utiliser `let` pour la variable item car elle sera réaffectée
          for (let item of listItems) {
            // Nettoyer le texte de chaque élément
            const cleanedText = item.replace(/^(\s*(\-|\*|\d+\.)\s+)/, '').trim();

            // Préparer les données pour l'appel de méthode avec template literals
            const actionPost = {
              name: cleanedText,
              parentType: pageSession.get('scope'),
              parentId: pageSession.get('scopeId'),
            };

            // Appeler la méthode pour chaque élément nettoyé avec une fonction fléchée
            await methodCall('insertAction', actionPost).then(respondToText => {
              // console.log(`Réponse pour ${cleanedText}: `, respondToText);

              IonToast.show({
                template: cleanedText, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-inbox"></i> ${i18n.__('Action created')}`,
              });

            }).catch(error => {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
              });
            });
          }
        }

        IonLoading.hide();
      } catch (error) {
        console.log('error', error);
        IonLoading.hide();
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    }


    let isSelecting = false;

    const selectionMenuIa = (view) => {
      let { from, to } = view.state.selection.main;
      if (from === to) return null; // Aucune sélection, ne rien afficher

      let menu = document.querySelector(".ia-menu-codemirror");

      if (menu) {
        menu.remove();
      }
      menu = document.createElement("div");
      menu.className = "ia-menu-codemirror";
      document.body.appendChild(menu);
      // Ajoutez ici le contenu du menu
      // Options du menu
      const options = [
        { text: "Corriger", icon: "fa fa-spell-check", action: () => correctText(view, from, to) },
        { text: "Reformuler", icon: "fa fa-sync-alt", action: () => reformulateText(view, from, to) },
        { text: "Markdown", icon: "fa fa-magic", action: () => markdownText(view, from, to) },
        { text: "Générer Mermaid", icon: "fa fa-code", action: () => generateMermaid(view, from, to) },
        { text: "Question IA", icon: "fa fa-reply", action: () => respondToText(view, from, to) },
        { text: "Image IA", icon: "fa fa-image", action: () => createImageFromText(view, from, to) }
      ];

      options.forEach(opt => {
        const optionElement = document.createElement("div");
        optionElement.className = "menu-option"; // Classe pour les options, à styliser dans CSS

        const iconElement = document.createElement("i");
        iconElement.className = opt.icon; // Classe pour l'icône
        optionElement.appendChild(iconElement);

        const textNode = document.createTextNode(` ${opt.text}`);
        optionElement.appendChild(textNode);

        optionElement.onclick = (event) => {
          event.stopPropagation(); // Empêche la propagation de l'événement
          opt.action();
          document.body.removeChild(menu);
        };
        menu.appendChild(optionElement);
      });

      // Positionnez le menu près de la sélection
      const { bottom, left } = view.coordsAtPos(from);
      const menuHeight = menu.offsetHeight;

      // Vérifiez si le menu dépasse le bas de la fenêtre
      if (bottom + menuHeight > window.innerHeight) {
        // Si c'est le cas, positionnez le menu au-dessus de la sélection
        const { top } = view.coordsAtPos(to);
        menu.style.top = `${top - menuHeight}px`;
      } else {
        // Sinon, positionnez le menu en dessous de la sélection
        menu.style.top = `${bottom}px`;
      }

      menu.style.left = `${left}px`;

      menu.onclick = (event) => {
        event.stopPropagation();
      };

      menu.onmouseover = () => {
        isMouseOverMenu = true;
      };

      menu.onmouseout = () => {
        isMouseOverMenu = false;
      };

      document.body.appendChild(menu);


      let isMouseOverMenu = false;


      const closeContextMenu = (menu) => {
        if (isSelecting) {
          setTimeout(() => {
            if (!isMouseOverMenu && menu.parentNode) {
              document.body.removeChild(menu);
            }
            isSelecting = false;
          }, 2000);
        } else if (!isMouseOverMenu && menu.parentNode) {
          document.body.removeChild(menu);
        }
      }

      // Ajoutez une fonctionnalité pour fermer le menu, par exemple en cliquant en dehors
      const removeMenu = (event) => {
        if (menu.parentNode) {
          document.body.removeChild(menu);
        }
        document.removeEventListener("click", removeMenu);
      };

      setTimeout(() => document.addEventListener("click", () => closeContextMenu(menu)), 0);


      return true; // Indique que le menu a été traité
    }


    const selectionMenuToolBar = (view, triggerElement) => {
      let { from, to } = view.state.selection.main;
      if (from === to) return null; // Aucune sélection, ne rien afficher

      let menu = document.querySelector(".ia-menu-codemirror");

      if (menu) {
        menu.remove();
      }
      menu = document.createElement("div");
      menu.className = "ia-menu-codemirror";
      document.body.appendChild(menu);
      // Ajoutez ici le contenu du menu
      // Options du menu
      const options = [
        { text: "Corriger", icon: "fa fa-spell-check", action: () => correctText(view, from, to) },
        { text: "Reformuler", icon: "fa fa-sync-alt", action: () => reformulateText(view, from, to) },
        { text: "Markdown", icon: "fa fa-magic", action: () => markdownText(view, from, to) },
        { text: "Générer Mermaid", icon: "fa fa-code", action: () => generateMermaid(view, from, to) },
        { text: "Question IA", icon: "fa fa-reply", action: () => respondToText(view, from, to) },
        { text: "Image IA", icon: "fa fa-image", action: () => createImageFromText(view, from, to) },
      ];

      if (isMarkdownList(view.state.doc.sliceString(from, to))) {
        options.push({ text: "Créer actions", icon: "fa fa-inbox", action: () => createListActions(view, from, to) });
      }

      options.forEach(opt => {
        const optionElement = document.createElement("div");
        optionElement.className = "menu-option"; // Classe pour les options, à styliser dans CSS

        const iconElement = document.createElement("i");
        iconElement.className = opt.icon; // Classe pour l'icône
        optionElement.appendChild(iconElement);

        const textNode = document.createTextNode(` ${opt.text}`);
        optionElement.appendChild(textNode);

        optionElement.onclick = (event) => {
          event.stopPropagation(); // Empêche la propagation de l'événement
          opt.action();
          document.body.removeChild(menu);
        };
        menu.appendChild(optionElement);
      });

      // Utilisez les coordonnées du bouton pour positionner le menu
      const rect = triggerElement.getBoundingClientRect();
      const menuWidth = menu.offsetWidth; // Largeur du menu

      // Calculer la position horizontale
      let leftPos = rect.left + window.scrollX; // Ajout du scroll pour ajustement
      if (rect.left + menuWidth > window.innerWidth) {
        // Ajuster pour que le menu ne sorte pas de la vue sur la droite
        leftPos = window.innerWidth - menuWidth + window.scrollX;
      }

      // Calculer la position verticale en fonction du bouton, sans ajuster pour le clavier
      let topPos = rect.bottom + window.scrollY; // Utilisation de scrollY pour inclure le défilement de la page

      // Appliquer les positions calculées avec 'position: absolute' pour le positionnement global
      menu.style.position = 'absolute'; // Assurez-vous que votre menu peut être positionné absolument dans votre CSS
      menu.style.left = `${leftPos}px`;
      menu.style.top = `${topPos}px`;

      menu.onclick = (event) => {
        event.stopPropagation();
      };

      menu.onmouseover = () => {
        isMouseOverMenu = true;
      };

      menu.onmouseout = () => {
        isMouseOverMenu = false;
      };

      document.body.appendChild(menu);


      let isMouseOverMenu = false;


      const closeContextMenu = (menu) => {
        if (isSelecting) {
          setTimeout(() => {
            if (!isMouseOverMenu && menu.parentNode) {
              document.body.removeChild(menu);
            }
            isSelecting = false;
          }, 2000);
        } else if (!isMouseOverMenu && menu.parentNode) {
          document.body.removeChild(menu);
        }
      }

      // Ajoutez une fonctionnalité pour fermer le menu, par exemple en cliquant en dehors
      const removeMenu = (event) => {
        if (menu.parentNode) {
          document.body.removeChild(menu);
        }
        document.removeEventListener("click", removeMenu);
      };

      setTimeout(() => document.addEventListener("click", () => closeContextMenu(menu)), 0);


      return true; // Indique que le menu a été traité
    }

    const isMarkdownList = (selectedText) => {
      // Expression régulière pour détecter les listes en Markdown
      const markdownListPattern = /^(\s*(\-|\*|\d+\.)\s+.+(\n|$))+/;
      return markdownListPattern.test(selectedText);
    };

    const selectionMenuList = (view) => {
      let { from, to } = view.state.selection.main;
      if (from === to) return null; // Aucune sélection, ne rien afficher

      if (!isMarkdownList(view.state.doc.sliceString(from, to))) {
        console.log('not a list');
        return null;
      }

      let menu = document.querySelector(".ia-menu-codemirror");

      if (menu) {
        menu.remove();
      }
      menu = document.createElement("div");
      menu.className = "ia-menu-codemirror";
      document.body.appendChild(menu);
      // Ajoutez ici le contenu du menu
      // Options du menu
      const options = [
        { text: "Créer actions", icon: "fa fa-inbox", action: () => createListActions(view, from, to) }
      ];

      options.forEach(opt => {
        const optionElement = document.createElement("div");
        optionElement.className = "menu-option"; // Classe pour les options, à styliser dans CSS

        const iconElement = document.createElement("i");
        iconElement.className = opt.icon; // Classe pour l'icône
        optionElement.appendChild(iconElement);

        const textNode = document.createTextNode(` ${opt.text}`);
        optionElement.appendChild(textNode);

        optionElement.onclick = (event) => {
          event.stopPropagation(); // Empêche la propagation de l'événement
          opt.action();
          document.body.removeChild(menu);
        };
        menu.appendChild(optionElement);
      });

      // Positionnez le menu près de la sélection
      const { bottom, left } = view.coordsAtPos(from);
      const menuHeight = menu.offsetHeight;

      // Vérifiez si le menu dépasse le bas de la fenêtre
      if (bottom + menuHeight > window.innerHeight) {
        // Si c'est le cas, positionnez le menu au-dessus de la sélection
        const { top } = view.coordsAtPos(to);
        menu.style.top = `${top - menuHeight}px`;
      } else {
        // Sinon, positionnez le menu en dessous de la sélection
        menu.style.top = `${bottom}px`;
      }

      menu.style.left = `${left}px`;

      menu.onclick = (event) => {
        event.stopPropagation();
      };

      menu.onmouseover = () => {
        isMouseOverMenu = true;
      };

      menu.onmouseout = () => {
        isMouseOverMenu = false;
      };

      document.body.appendChild(menu);


      let isMouseOverMenu = false;


      const closeContextMenu = (menu) => {
        if (isSelecting) {
          setTimeout(() => {
            if (!isMouseOverMenu && menu.parentNode) {
              document.body.removeChild(menu);
            }
            isSelecting = false;
          }, 2000);
        } else if (!isMouseOverMenu && menu.parentNode) {
          document.body.removeChild(menu);
        }
      }

      // Ajoutez une fonctionnalité pour fermer le menu, par exemple en cliquant en dehors
      const removeMenu = (event) => {
        if (menu.parentNode) {
          document.body.removeChild(menu);
        }
        document.removeEventListener("click", removeMenu);
      };

      setTimeout(() => document.addEventListener("click", () => closeContextMenu(menu)), 0);


      return true; // Indique que le menu a été traité
    }

    // autocomplete blockquote
    // const blockquoteAutocomplete = (context) => {
    //   const blockquoteTagRegex = /(?:^|\s)\[(?:\w+)?/
    //   return [
    //     regexCompletion(blockquoteTagRegex, [{ label: '[name=]', detail: 'name' }])(context),
    //     regexCompletion(blockquoteTagRegex, [{ label: '[time=]', detail: 'time' }])(context),
    //     regexCompletion(blockquoteTagRegex, [{ label: '[color=]', detail: 'color' }])(context)
    //   ];
    // }

    let state = EditorState.create({
      doc,
      extensions: [basicSetup, peerExtension(version),
        markdown({
          base: markdownLanguage,
          codeLanguages: languages,
          completeHTMLTags: true,
        }),
        EditorView.lineWrapping,
        themeBase,
        EditorState.phrases.of(frenchPhrases),
        peerSelectionsField,
        cursorTooltipBaseTheme,
        underlineTheme,
        keymap.of([
          ...defaultKeymap,
          ...searchKeymap,
          ...historyKeymap,
          { key: "Tab", run: indentMore, shift: indentLess },
          { key: "c-d", run: toggleCheckbox },
          {
            key: "c-y",
            run: selectionMenuIa,
            preventDefault: true
          },
          {
            key: "c-l",
            run: selectionMenuList,
            preventDefault: true
          }
        ]),
        // helpPanel(),
        // showPanel.of(footerPanel),
        history(),
        EditorView.domEventHandlers({
          keydown: (event, view) => {
            if (event.ctrlKey && event.key === "k") {
              event.preventDefault();
              event.stopPropagation();
              toggleMenu(view, true);
              return true;
            }
            if (event.key === "Escape") {
              toggleMenu(view, false);
              return true;
            }
            if (event.altKey && event.key === "Enter") {
              event.preventDefault();
              event.stopPropagation();
              toggleEditorAndView();
              return true;
            }
          },
          scroll(event, view) {
            if (event?.target?.scrollTop) {
              const editorScrollTop = event.target.scrollTop;
              const editorScrollHeight = event.target.scrollHeight;
              const editorClientHeight = event.target.clientHeight;

              // Calculez le pourcentage de défilement
              const scrollPercentage = editorScrollTop / (editorScrollHeight - editorClientHeight);

              // Appliquez ce pourcentage à la prévisualisation
              const preview = document.getElementById('mardown'); // Remplacez par l'ID de votre div de prévisualisation
              const previewScrollHeight = preview.scrollHeight;
              preview.scrollTop = scrollPercentage * (previewScrollHeight - preview.clientHeight);
            }

          },
          paste(e, view) {
            const scope = pageSession.get('scope');
            const scopeId = pageSession.get('scopeId');
            const docId = pageSession.get('docId');

            if (window.File && window.FileReader && window.FileList && window.Blob) {
              if (e.clipboardData && e.clipboardData.items) {
                const items = e.clipboardData.items;
                for (let i = 0; i < items.length; i++) {
                  if (items[i].kind === "file") {
                    const file = items[i].getAsFile();
                    if (file.size > 1) {
                      const reader = new FileReader();
                      reader.onload = (event) => {
                        const dataURI = reader.result;
                        const str = file.name;
                        // Vérifier si le fichier est une image acceptée
                        if (file.type === "image/jpeg" || file.type === "image/jpg" || file.type === "image/png") {
                          IonLoading.show();
                          Meteor.call('photoNotes', dataURI, str, scope, scopeId, docId, function (error, result) {
                            if (!error) {
                              pageSession.set('fileSize', file.size);
                              pageSession.set('fileName', file.name);

                              if (result && result.url) {
                                IonToast.show({
                                  template: file.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                                });
                                instance.insertMarkdown(`![${file.name}](${result.url})\n\n`);
                                IonLoading.hide();
                              }
                            } else {
                              // Gestion de l'erreur
                              IonLoading.hide();
                              IonToast.show({
                                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
                              });
                            }
                          });
                        } else if ([".pdf", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx", ".odt", ".ods", ".odp", ".csv"].some(ext => file.name.endsWith(ext))) {
                          IonLoading.show();
                          // Si le fichier correspond à un des types de documents acceptés
                          Meteor.call('docNotes', dataURI, str, scope, scopeId, docId, function (error, result) {
                            if (!error) {
                              pageSession.set('fileSize', file.size);
                              pageSession.set('fileName', file.name);

                              if (result && result.url) {
                                IonToast.show({
                                  template: file.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                                });
                                instance.insertMarkdown(`[${file.name}](${result.url})\n\n`);
                                IonLoading.hide();
                              }
                            } else {
                              // Gestion de l'erreur
                              IonLoading.hide();
                              IonToast.show({
                                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
                              });
                            }
                          });
                        }
                      };
                      reader.readAsDataURL(file);
                    }
                  }
                }
              }
            }
          },
          drop: (e) => {
            e.preventDefault();
            const scope = pageSession.get('scope');
            const scopeId = pageSession.get('scopeId');
            const docId = pageSession.get('docId');

            if (window.File && window.FileReader && window.FileList && window.Blob) {
              const { items } = e.dataTransfer;
              if (items) {
                for (let i = 0; i < items.length; i++) {
                  if (items[i].kind === "file") {
                    const file = items[i].getAsFile();
                    if (file.size > 1) {
                      const reader = new FileReader();
                      reader.onload = (event) => {
                        const dataURI = reader.result;
                        const str = file.name;
                        // Vérifier si le fichier est une image acceptée

                        if (file.type === "image/jpeg" || file.type === "image/jpg" || file.type === "image/png") {
                          IonLoading.show();
                          Meteor.call('photoNotes', dataURI, str, scope, scopeId, docId, function (error, result) {
                            if (!error) {
                              pageSession.set('fileSize', file.size);
                              pageSession.set('fileName', file.name);

                              if (result && result.url) {
                                IonToast.show({
                                  template: file.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                                });
                                instance.insertMarkdown(`![${file.name}](${result.url})\n\n`);
                                IonLoading.hide();
                              }
                            } else {
                              // Gestion de l'erreur
                              IonLoading.hide();
                              IonToast.show({
                                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
                              });
                            }
                          });
                        } else if ([".pdf", ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx", ".odt", ".ods", ".odp", ".csv"].some(ext => file.name.endsWith(ext))) {
                          IonLoading.show();
                          // Si le fichier correspond à un des types de documents acceptés
                          Meteor.call('docNotes', dataURI, str, scope, scopeId, docId, function (error, result) {
                            if (!error) {
                              pageSession.set('fileSize', file.size);
                              pageSession.set('fileName', file.name);

                              if (result && result.url) {
                                IonToast.show({
                                  template: file.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                                });
                                instance.insertMarkdown(`[${file.name}](${result.url})\n\n`);
                                IonLoading.hide();
                              }
                            } else {
                              // Gestion de l'erreur
                              IonLoading.hide();
                              IonToast.show({
                                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
                              });
                            }
                          });
                        }
                      };
                      reader.readAsDataURL(file);
                    }
                  }
                }
              }
            }
          },
        }),
        EditorView.updateListener.of(update => {
          if (update.docChanged) {
            // console.log('docChanged');
            updateFooterDocChange(update);
            updateMenu(update);
          }
          if (update.selectionSet) {
            // console.log('selectionSet');
            updateFooterPositionChange(update);
            const { from, to } = update.view.state.selection.main;
            if (from !== to) {
              // je veux que le button avce l'id tool-select devient clickable
              // console.log('from', from, 'to', to);
              const toolIa = document.getElementById('tool-select');
              if (toolIa) {
                toolIa.classList.remove('disabled');
              }
              // isSelecting = true; // Commence la sélection
              // selectionMenuIa(update.view);
            } else {
              const toolIa = document.getElementById('tool-select');
              if (toolIa) {
                toolIa.classList.add('disabled');
              }
            }
          }

        }),
        autocompletion({ override: [htmlCompletionSource, customAutocomplete, contributorsAutocomplete, fileAutocomplete, imgAutocompleteSlideBackground, noteLinkAutocomplete] })
      ],
    });
    let editors = instance.find("#editors");
    if (editors) {
      let wrap = editors.appendChild(document.createElement("div"));
      wrap.className = "editor";

      instance.editorView = new EditorView({ state, parent: wrap });

      instance.insertMarkdown = (before, after) => {
        const { state, dispatch } = instance.editorView;
        const { from, to } = state.selection.main;

        const endOfInsertion = from + before.length;

        const changes = [
          { from, insert: before },
          { from: to, insert: after }
        ];
        dispatch(state.update({ changes, selection: { anchor: endOfInsertion } }));

        // Focaliser l'éditeur après l'insertion
        instance.editorView.focus();
      };

      instance.selectionMenuToolBar = selectionMenuToolBar;

      updateFooterDocChange(instance.editorView);
      updateFooterPositionChange(instance.editorView);

      const menuContainer = document.getElementById("menu-container");
      menuContainer.style.display = "none";

      updateMenu(instance.editorView);

      // file handler pwa
      if (pageSession.get('docOpen')) {
        const { state, dispatch } = instance.editorView;
        dispatch({
          changes: {
            from: 0,
            to: state.doc.length,
            insert: pageSession.get('docOpen')
          }
        });
        pageSession.set('docOpen', null);
        pageSession.set('isFileOpen', false);
      }

      if (docBrute === 'start' && !pageSession.get('docOpen')) {
        const { state, dispatch } = instance.editorView;

        // Supposons que "start" se trouve au début du document.
        // La position de début est 0 et "start" a une longueur de 5 caractères.
        const startPos = 0;
        const endPos = "start".length;

        // Créer des changements pour sélectionner le texte "start".
        // Dans cet exemple, aucun changement de texte n'est effectué, seulement une sélection.
        const changes = {
          // Aucun changement de texte ici, donc cette partie peut être omise si vous ne faites que sélectionner
          // changes: [{ from: startPos, to: endPos }],

          // Définir la nouvelle sélection pour englober "start"
          selection: { anchor: startPos, head: endPos }
        };

        // Appliquer la sélection et mettre le focus
        dispatch(state.update(changes));
        instance.editorView.focus();
      }

    }
  }

  addPeer();

});

Template.editorCodemiror.onDestroyed(function () {
  if (this.observer) {
    this.observer.stop();
    this.observer = null;
  }
  if (pageSession.get('fullscreen') === true) {
    toggleFullscreen();
  }
  const menuContainer = document.getElementById("menu-container");

  if (this.searchInputHandler && menuContainer) {
    const searchInput = menuContainer.querySelector(".menu-search");
    if (searchInput) {
      searchInput.removeEventListener("input", this.searchInputHandler);
    }
  }

  if (this.escKeyListener) {
    document.removeEventListener("keydown", this.escKeyListener);
  }
});

Template.editorCodemiror.helpers({
  toolbarLines(isMobile, notCordova, isFullscreen) {
    const baseClass = "button button-dark button-clear";
    const mobileClassAddon = " button-small";

    const toolbarLines = [
      [ // Première ligne
        { id: "tool-bold", iconClass: "icon fa fa-bold", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Bold" },
        { id: "tool-italic", iconClass: "icon fa fa-italic", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Italic" },
        { id: "tool-link", iconClass: "icon fa fa-link", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Insert Link" },
        { id: "tool-image", iconClass: "icon fa fa-image", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Insert Image" },
        { id: "tool-upload", iconClass: "icon fa fa-upload fa-fw", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Upload" },
        { id: "tool-code", iconClass: "icon fa fa-code", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Insert Code" },
        { id: "tool-list", iconClass: "icon fa fa-list-ul", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Bulleted List" },
        { id: "tool-numbered-list", iconClass: "icon fa fa-list-ol", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Numbered List" }
      ],
      [ // Deuxième ligne
        { id: "tool-checkbox", iconClass: "icon fa fa-check-square-o", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Checkbox" },
        { id: "tool-quote", iconClass: "icon fa fa-quote-left", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Blockquote" },
        { id: "tool-hr", iconClass: "icon fa fa-minus", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Horizontal Rule" },
        { id: "tool-heading", iconClass: "icon fa fa-header fa-heading header-1", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Heading" },
        { id: "tool-undo", iconClass: "icon fa fa-undo", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Undo" },
        { id: "tool-redo", iconClass: "icon fa fa-repeat", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Redo" },
        { id: "tool-select", iconClass: "icon fas fa-magic", buttonClass: "button button-dark button-clear disabled", tooltip: "Select" },
        { id: "tool-fullscreen", iconClass: "icon fas fa-expand-arrows-alt", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Fullscreen" }
      ]
    ];

    // Condition pour isFullscreen et notCordova
    if (notCordova && !isMobile && isFullscreen) {
      toolbarLines[1].push(
        { id: "tool-preview", iconClass: "icon fas fa-columns", buttonClass: baseClass + (isMobile ? mobileClassAddon : ""), tooltip: "Preview" }
      );
    }

    return toolbarLines;
  }
});

Template.editorCodemiror.events({
  'click #tool-bold'(event, instance) {
    instance.insertMarkdown('**', '**');
  },
  'click #tool-italic'(event, instance) {
    instance.insertMarkdown('*', '*');
  },
  'click #tool-code'(event, instance) {
    instance.insertMarkdown('```', '```');
  },
  'click #tool-quote'(event, instance) {
    instance.insertMarkdown('> ', '');
  },
  'click #tool-list'(event, instance) {
    instance.insertMarkdown('- ', '');
  },
  'click #tool-numbered-list'(event, instance) {
    instance.insertMarkdown('1. ', '');
  },
  'click #tool-checkbox'(event, instance) {
    instance.insertMarkdown('- [ ] ', '');
  },
  'click #tool-link'(event, instance) {
    instance.insertMarkdown('[', '](https://)');
  },
  'click #tool-image'(event, instance) {
    instance.insertMarkdown('![', '](https://)');
  },
  'click #tool-upload'(event, instance) {
    // const scope = pageSession.get('scope');
    // const scopeId = pageSession.get('scopeId');
    // const docId = pageSession.get('docId');

    // if (Meteor.isCordova) {
    //   const options = {
    //     width: 640,
    //     height: 480,
    //     quality: 75,
    //   };

    //   MeteorCameraUI.getPicture(options, function (error, data) {
    //     if (!error) {
    //       const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
    //       Meteor.call('photoNotes', data, str, scope, scopeId, docId, function (errorCall, result) {
    //         if (!errorCall) {
    //           if (result && result.url) {
    //             instance.insertMarkdown(`![${str}](${result.url})`);
    //           }
    //         } else {
    //           // console.log('error',error);
    //         }
    //       });
    //     }
    //   });
    // } else {
    instance.$('#file-upload-note').trigger('click');
    // }
  },
  'change #file-upload-note'(event, instance) {
    event.preventDefault();
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    const docId = pageSession.get('docId');
    const drop = pageSession.get('drop');

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      _.each(instance.find('#file-upload-note').files, function (file) {
        if (file.size > 1) {
          const reader = new FileReader();
          reader.onload = function () {
            const str = file.name;
            const dataURI = reader.result;
            IonLoading.show();
            if (file.type.startsWith('image/')) {
              Meteor.call('photoNotes', dataURI, str, scope, scopeId, docId, function (error, result) {
                if (!error) {
                  pageSession.set('fileSize', file.size);
                  pageSession.set('fileName', file.name);

                  if (result && result.url) {
                    IonToast.show({
                      template: file.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                    });
                    instance.insertMarkdown(`![${file.name}](${result.url})\n\n`);
                    IonLoading.hide();
                  }

                } else {
                  // console.log('error',error);
                  IonLoading.hide();
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
                  });
                }
              });
            } else {
              Meteor.call('docNotes', dataURI, str, scope, scopeId, docId, function (error, result) {
                if (!error) {
                  pageSession.set('fileSize', file.size);
                  pageSession.set('fileName', file.name);

                  if (result && result.url) {
                    IonToast.show({
                      template: file.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('uploading')}`,
                    });
                    instance.insertMarkdown(`[${file.name}](${result.url})\n\n`);
                    IonLoading.hide();
                  }
                } else {
                  // console.log('error',error);
                  IonLoading.hide();
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
                  });
                }
              });
            }
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
  'click #tool-table'(event, instance) {
    instance.insertMarkdown('| Header 1 | Header 2 |\n| -------- | -------- |\n| Cell 1   | Cell 2   |');
  },
  'click #tool-hr'(event, instance) {
    instance.insertMarkdown('---\n');
  },
  'click #tool-heading'(event, instance) {
    const { state, dispatch, focus } = instance.editorView;
    const { from, to } = state.selection.main;
    const line = state.doc.lineAt(from); // Récupère la ligne courante

    let numberOfHashes = (line.text.match(/^#+/) || [''])[0].length; // Compte les '#'
    let insertText = numberOfHashes >= 6 ? '' : numberOfHashes === 0 ? '# ' : '#'; // Limite à 6 niveaux de titre

    const changes = [
      { from: line.from, to: line.from, insert: insertText }
    ];

    const endOfLine = line.from + line.length + insertText.length;

    dispatch(state.update({ changes, selection: { anchor: endOfLine } }));
    instance.editorView.focus();
  },
  'click #tool-undo'(event, instance) {
    instance.editorView.dispatch(undo(instance.editorView));
  },
  'click #tool-redo'(event, instance) {
    instance.editorView.dispatch(redo(instance.editorView));
  },
  'click #tool-fullscreen'(event, instance) {
    toggleFullscreen();
    instance.editorView.focus();
  },
  'click #tool-preview'(event, instance) {
    togglePreview();
  },
  'click #tool-select'(event, instance) {
    const { state, dispatch } = instance.editorView;
    const { from, to } = state.selection.main;
    if (from !== to) {
      instance.selectionMenuToolBar(instance.editorView, event.currentTarget);
    }
  },
});

Template.editorNoAuthorized.onCreated(function () {
  if (Template.instance().data.dataReady === true && Template.instance().data.isDelete === false) {
    IonPopup.alert({
      title: i18n.__('Not authorized'),
      template: i18n.__('You are not authorized to view this note, or you are already viewing it with this user in another window or on another device'),
      okText: i18n.__('Close'),
      onOk() {
        window.history.back();
      }
    });
  }
});


Template.editorButtonRight.events({
  'click .action-card-notes-js'(event, instance) {
    event.preventDefault();
    const self = this;

    const arrayButtons = [];
    if (pageSession.get('isEditor') === true) {
      arrayButtons.push({ name: 'edit-preview', text: `${i18n.__('preview')} <i class="icon fa fa-eye"></i>` });
    } else {
      if (self.scope.isAdmin() || (self.doc.isContributors() && !self.doc.isLock())) {
        arrayButtons.push({ name: 'edit-note', text: `${i18n.__('edit')} <i class="icon fa fa-edit"></i>` });
      }
    }

    // arrayButtons.push({ name: 'share-url', text: `${i18n.__('share Url')} <i class="icon fa fa-share-alt"></i>` });

    arrayButtons.push({ name: 'slideshow', text: `${i18n.__('slideshow')} <i class="icon fa fa-desktop"></i>` });

    if (!Meteor.isCordova) {
      if (!Platform.isAndroid() && !Platform.isIOS()) {
        arrayButtons.push({ name: 'copy', text: `${i18n.__('copy')} <i class="icon fa fa-copy"></i>` });


        arrayButtons.push({ name: 'print', text: `${i18n.__('print')} <i class="icon fa fa-print"></i>` });

        // download .md
        arrayButtons.push({ name: 'download', text: `${i18n.__('download')} <i class="icon fa fa-download"></i>` });
      }
    }

    const destructive = {};

    // gestion file present dans le document (delete, insert)
    if (self.scope.isAdmin() || self.doc.isContributors()) {
      arrayButtons.push({ name: 'file', text: `${i18n.__('file')} <i class="icon fa fa-file"></i>` });
    }

    if (self.doc.isNotAuthor() && self.doc.isContributors()) {
      arrayButtons.push({ name: 'leave-share', text: `${i18n.__('leave the share')} <i class="icon fa fa-user-minus"></i>` });
    }

    // public/private
    if (self.scope.isAdmin()) {
      if (self.doc.isPublic()) {
        arrayButtons.push({ name: 'private', text: `${i18n.__('switch to private')} <i class="icon fa fa-lock"></i>` });
      } else {
        arrayButtons.push({ name: 'public', text: `${i18n.__('switch to public')} <i class="icon fa fa-unlock"></i>` });
      }
      // partager avec user
      arrayButtons.push({ name: 'share-user', text: `${i18n.__('share with users')} <i class="icon fa fa-user-plus"></i>` });

      // convert to action
      arrayButtons.push({ name: 'convert', text: `${i18n.__('convert to action')} <i class="icon fa fa-exchange-alt"></i>` });

      if (self.doc.isLock()) {
        arrayButtons.push({ name: 'unlock-editing', text: `${i18n.__('to unlock editing')} <i class="icon fa fa-unlock"></i>` });
      } else {
        arrayButtons.push({ name: 'lock-editing', text: `${i18n.__('to lock editing')} <i class="icon fa fa-lock"></i>` });
      }
      // // analyse semantic
      // arrayButtons.push({ name: 'semantic', text: `${i18n.__('semantic analysis')} <i class="icon fa fa-chart-bar"></i>` });

      // // report opinion
      // arrayButtons.push({ name: 'report', text: `${i18n.__('report opinion')} <i class="icon fa fa-exclamation-triangle"></i>` });

      // delete
      destructive.destructiveText = `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`;
      destructive.destructiveButtonClicked = function () {
        // console.log('Edit!');
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this note'),
          onOk() {
            pageSession.set('isDelete', true);
            Meteor.call('deleteNote', { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), docId: pageSession.get('docId') }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
                });
                window.history.back();
                // if (self.scope.scopeVar() === 'citoyens') {
                //   Router.go('notesList', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope') }, { replaceState: true });
                // } else {
                //   Router.go('notesListOrga', { orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope') }, { replaceState: true });
                // }
              }
            });
          },
          onCancel() {
            pageSession.set('isDelete', false);
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      };
    }
    IonActionSheet.show({
      titleText: i18n.__('Actions Notes'),
      buttons: arrayButtons,
      ...destructive,
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 'edit-preview' || index === 'edit-note') {
          toggleEditorAndView();
        }
        // if (index === 'share') {
        //   // console.log('Share!');
        //   IonPopup.prompt({
        //     title: i18n.__('Share with'),
        //     template: i18n.__('Enter the email of the person with whom you want to share this note'),
        //     okText: i18n.__('Share'),
        //     cancelText: i18n.__('Cancel'),
        //     inputType: 'email',
        //     inputPlaceholder: i18n.__('Email'),
        //     onOk(event, email) {
        //       // Meteor.call('shareNote', { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), docId: pageSession.get('docId'), email }, (error) => {
        //       //   if (error) {
        //       //     IonToast.show({
        //       //       title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        //       //     });
        //       //   } else {
        //       //     IonToast.show({
        //       //       template: i18n.__('shared'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
        //       //     });
        //       //   }
        //       // });
        //     },
        //     onCancel() {
        //     },
        //   });
        // }
        if (index === 'share-url') {
          // console.log('Share Url!');
          IonModal.open('shareUrl');
        }
        if (index === 'copy') {
          // console.log('Copy!');
          const text = document.getElementById("mardown").innerText;

          navigator.permissions.query({ name: "clipboard-write" }).then(result => {
            if (result.state === "granted" || result.state === "prompt") {
              navigator.clipboard.writeText(text).then(() => {
                IonToast.show({
                  template: i18n.__('copied'),
                  position: 'bottom',
                  type: 'success',
                  showClose: false,
                  title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
                });
              }).catch(err => {
                console.error('Erreur lors de la copie :', err);
              });
            } else {
              console.error('Accès au presse-papiers refusé');
            }
          });

        }
        if (index === 'print') {
          // console.log('Print!');
          var contenu = document.getElementById("mardown").innerHTML;
          var fenetreImpression = window.open('', '_blank');
          fenetreImpression.document.write(contenu);
          fenetreImpression.document.close();
          fenetreImpression.focus();
          fenetreImpression.print();
          fenetreImpression.close();
        }
        if (index === 'private') {
          // console.log('Private!');
          Meteor.call('updateNoteVisibility', { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), docId: pageSession.get('docId'), visibility: 'private' }, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonToast.show({
                template: i18n.__('private'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
              });
            }
          });
        }
        if (index === 'public') {
          // console.log('Public!');
          Meteor.call('updateNoteVisibility', { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), docId: pageSession.get('docId'), visibility: 'public' }, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonToast.show({
                template: i18n.__('public'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
              });
            }
          });
        }
        if (index === 'share-user') {
          // console.log('Share with user!');
          if (self.scope.isAdmin()) {
            const parentDataContext = {
              orgaCibleId: Session.get('orgaCibleId'), _id: self.scope._id.valueOf(), scope: self.scope.scopeVar(), docId: self.doc._id.valueOf(),
            };
            IonModal.open('assignMembersNoteModal', parentDataContext);
          }
        }
        if (index === 'download') {
          // console.log('Download!');
          const text = self.doc.doc;
          const blob = new Blob([text], { type: "text/plain;charset=utf-8" });
          const url = URL.createObjectURL(blob);
          const a = document.createElement("a");
          a.href = url;
          // slugify self.doc.name pour le nom du fichier
          const slugify = self.doc.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
          a.download = `${slugify}.md`;
          a.click();
          URL.revokeObjectURL(url);
        }
        if (index === 'convert') {
          // console.log('Convert to action!');
          Meteor.call('convertNoteToAction', { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), docId: pageSession.get('docId') }, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonToast.show({
                template: i18n.__('converted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
              });
            }
          });
        }
        if (index === 'semantic') {
          // console.log('Semantic analysis!');
          Meteor.call('completeSemanticAnalysis', self.doc.doc, (error, result) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonModal.open('semanticAnalysisModal', { result });
            }
          });
        }
        if (index === 'report') {
          // console.log('Semantic analysis!');
          Meteor.call('reportOpinion', self.doc.doc, (error, result) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonModal.open('reportOpinionModal', { doc: result });
            }
          });
        }
        if (index === 'file') {
          // console.log('File!');
          IonModal.open('fileModal', { doc: self.doc, scope: self.scope });
        }
        if (index === 'slideshow') {
          // console.log('Slideshow!');
          if (self.scope.scopeVar() === 'citoyens') {
            Router.go('notesSlide', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), docId: pageSession.get('docId') });
          } else {
            Router.go('notesSlideOrga', { orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), docId: pageSession.get('docId') });

          }
        }
        if (index === 'leave-share') {
          Meteor.call('leaveShareNote', { docId: pageSession.get('docId') }, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonToast.show({
                template: i18n.__('leave the share'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
              });
            }
          });
        }

        if (index === 'unlock-editing' || index === 'lock-editing') {
          Meteor.call('updateNoteLockEditing', { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), docId: pageSession.get('docId'), lock: !self.doc.isLock() }, (error) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
              });
            } else {
              IonToast.show({
                template: self.doc.isLock() ? i18n.__('unlock editing') : i18n.__('lock editing'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
              });
            }
          });
        }

        return true;
      },
    });
  },
});

Template.EditorNameNote.events({
  'input #nameNote': _.debounce((event, instance) => {
    event.preventDefault();
    const title = event.target.value;
    if (title === '') {
      return;
    }
    // Mettre à jour après un collage, une saisie, etc.
    Meteor.call('updateNoteTitle', pageSession.get('scope'), pageSession.get('scopeId'), pageSession.get('docId'), title);
  }, 500),
});

Template.semanticAnalysisModal.helpers({
  sentimentClass() {
    switch (this.result.sentiment) {
      case 'Positif':
        return 'badge-balanced';
      case 'Neutre':
        return 'badge-stable';
      case 'Négatif':
        return 'badge-assertive';
      default:
        return '';
    }
  },
  sentimentIcon() {
    switch (this.result.sentiment) {
      case 'Positif':
        return 'fa-smile';
      case 'Neutre':
        return 'fa-meh';
      case 'Négatif':
        return 'fa-frown';
      default:
        return '';
    }
  }
});

Template.notesSlide.onCreated(function () {
  this.ready = new ReactiveVar(false);
  pageSession.set('scopeId', null);
  pageSession.set('scope', null);
  pageSession.set('docId', null);

  // on récupère les paramètres de l'url pour les stocker dans la session
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    pageSession.set('docId', Router.current().params.docId ? Router.current().params.docId : undefined);
  }.bind(this));

  // le subscribe ce fait que si le docId est présent
  this.autorun(function () {
    const docId = pageSession.get('docId');
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (docId && scope && scopeId) {
      // console.log('subscribe documentUpdates');
      const handleScope = this.subscribe('noteOne', scope, scopeId, docId);
      Tracker.autorun(() => {
        const isReady = handleScope.ready();
        // console.log('Subscription is ready:', isReady);
        if (isReady) {
          this.ready.set(isReady);
        }
      });
    }
  }.bind(this));
})

Template.notesSlide.onRendered(function () {

  const slideThemes = ['white', 'black', 'moon', 'beige', 'blood', 'dracula', 'league', 'night', 'serif', 'simple', 'sky', 'solarized', 'white-contrast', 'black-contrast', 'white_contrast_compact_verbatim_headers'];
  const instance = this;
  let deck = new Reveal();
  pageSession.setDefault('currentSlideState', {
    indexHorizontal: 0,
    indexVertical: 0
  });

  pageSession.set('slideTheme', 'white');

  this.autorun(function () {
    if (instance.ready.get()) {
      console.log('ready');

      Tracker.autorun(function () {

        if (instance._cssLink) {
          // Retirer le fichier CSS du document
          document.head.removeChild(instance._cssLink);
        }

        if (pageSession.get('slideTheme') && slideThemes.includes(pageSession.get('slideTheme')) && !Platform.isAndroid() && !Platform.isIOS()) {
          // Créer un élément link pour le fichier CSS
          const link = document.createElement('link');
          link.href = `/theme/${pageSession.get('slideTheme')}.css`; // Assurez-vous que le chemin est correct
          link.type = 'text/css';
          link.rel = 'stylesheet';

          // Conserver une référence à l'élément link pour le retirer plus tard
          instance._cssLink = link;

          // Ajouter le fichier CSS au document
          document.head.appendChild(link);

        } else {
          // Créer un élément link pour le fichier CSS
          const link = document.createElement('link');
          link.href = `/theme/white.css`; // Assurez-vous que le chemin est correct
          link.type = 'text/css';
          link.rel = 'stylesheet';

          // Conserver une référence à l'élément link pour le retirer plus tard
          instance._cssLink = link;

          // Ajouter le fichier CSS au document
          document.head.appendChild(link);
        }
      });

      deck.initialize({
        embedded: true,
        width: 1280,
        height: 1280,
        margin: 0.1,
        minScale: 0.2,
        maxScale: 1.5,
        // slideNumber: true
        // plugins: [NotesPlugin]
      }).then(() => {
        // reveal.js is ready
        deck.layout();
        deck.slide(0, 0, 0);
        deck.addEventListener('slidechanged', (event) => {
          pageSession.set('currentSlideState', {
            indexHorizontal: event.indexh,
            indexVertical: event.indexv ?? 0
          });

        })
      });
    }
  });

  Notes.find().observeChanges({
    added(id, fields) {
      if (fields.doc) {
        const { metadata, content } = extraireEtParserYAML(fields.doc);
        if (metadata && metadata.slideOptions) {
          if (metadata.slideOptions.theme && slideThemes.includes(metadata.slideOptions.theme)) {
            pageSession.set('slideTheme', metadata.slideOptions.theme);
          }
          deck.configure(metadata.slideOptions);
          // console.log('slide observeChanges');
        }
      }
    },
    changed(id, fields) {
      if (fields.doc) {
        console.log('slide observeChanges');
        deck.destroy();
        instance.ready.set(false);
        instance.ready.set(true);
        // deck.sync();
        // const current = pageSession.get('currentSlideState');
        // deck.slide(current.indexHorizontal, current.indexVertical);
        // console.log('slide observeChanges');
      }
    }
  });

});

Template.notesSlide.onDestroyed(function () {
  // Retirer le fichier CSS du document
  if (this._cssLink) {
    document.head.removeChild(this._cssLink);
  }
});

Template.notesSlide.helpers({
  scope() {
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (scope && scopeId) {
      const collection = nameToCollection(scope);
      return collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
    }
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  doc() {
    if (pageSession.get('docId')) {
      const document = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) });
      return document;
    }
  },
  isAdminOrContributors(scope, doc) {
    if (scope && !doc) {
      return scope.isAdmin();
    } else if (!scope && doc) {
      return doc.isContributors();
    } else if (scope && doc) {
      return scope.isAdmin() || doc.isContributors();
    } else if (doc.isPublic()) {
      return true;
    }
    return false;
  }
});

Template.doNoteDocItem.events({
  'click .note-doc-admin-js'(event) {
    event.preventDefault();
    const self = this;
    IonPopup.confirm({
      title: i18n.__('delete'),
      template: i18n.__('Delete this document'),
      onOk() {
        // name, parentId, parentType, path, id, actionId
        // console.log({ parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: self.name, path: self.moduleId, id: self._id.valueOf(), actionId: pageSession.get('actionId') })
        Meteor.call('noteDocDelete', {
          parentType: pageSession.get('scope'), parentId: pageSession.get('scopeId'), name: self.file.name, path: self.file.moduleId, id: self.file._id.valueOf(), docId: pageSession.get('docId'),
        }, (error) => {
          if (error) {
            IonToast.show({
              title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
            });
          }
        });
      },
      onCancel() {
      },
      cancelText: i18n.__('no'),
      okText: i18n.__('yes'),
    });
  },
  'click .download-doc-direct-js'(event) {
    event.preventDefault();
    if (Meteor.isCordova) {
      cordova.InAppBrowser.open(event.currentTarget.href, '_system');
    } else {
      window.open(event.currentTarget.href, '_blank');
    }
  },
});

Template.notePublic.onCreated(function () {
  this.ready = new ReactiveVar(false);
  pageSession.set('scopeId', null);
  pageSession.set('scope', null);
  pageSession.set('docId', null);

  // on récupère les paramètres de l'url pour les stocker dans la session
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    pageSession.set('docId', Router.current().params.docId ? Router.current().params.docId : undefined);
  }.bind(this));

  // le subscribe ce fait que si le docId est présent
  this.autorun(function () {
    const docId = pageSession.get('docId');
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (docId && scope && scopeId) {
      // console.log('subscribe documentUpdates');
      const handleScope = this.subscribe('noteOne', scope, scopeId, docId, true);
      Tracker.autorun(() => {
        const isReady = handleScope.ready();
        // console.log('Subscription is ready:', isReady);
        if (isReady) {
          this.ready.set(isReady);
          //changer le titre de la page
          const document = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
          if (document) {
            window.document.title = document.name;
          }
        }
      });
    }
  }.bind(this));
}
);

Template.notePublic.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
  doc() {
    if (pageSession.get('docId')) {
      const document = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) });
      return document;
    }
  },
  scope() {
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (scope && scopeId) {
      const collection = nameToCollection(scope);
      return collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
    }
  }
});

Template.notePublic.events({
  'click .action-card-notes-js'(event, instance) {
    event.preventDefault();
    const self = this;

    const arrayButtons = [];

    arrayButtons.push({ name: 'slideshow', text: `${i18n.__('slideshow')} <i class="icon fa fa-desktop"></i>` });

    if (!Platform.isAndroid() && !Platform.isIOS()) {
      arrayButtons.push({ name: 'copy', text: `${i18n.__('copy')} <i class="icon fa fa-copy"></i>` });


      arrayButtons.push({ name: 'print', text: `${i18n.__('print')} <i class="icon fa fa-print"></i>` });

      // download .md
      arrayButtons.push({ name: 'download', text: `${i18n.__('download')} <i class="icon fa fa-download"></i>` });
    }

    IonActionSheet.show({
      titleText: i18n.__('Actions Notes'),
      buttons: arrayButtons,
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {

        if (index === 'copy') {
          // console.log('Copy!');
          const text = document.getElementById("mardown").innerText;

          navigator.permissions.query({ name: "clipboard-write" }).then(result => {
            if (result.state === "granted" || result.state === "prompt") {
              navigator.clipboard.writeText(text).then(() => {
                IonToast.show({
                  template: i18n.__('copied'),
                  position: 'bottom',
                  type: 'success',
                  showClose: false,
                  title: `<i class="icon ion-checkmark"></i> ${i18n.__('Note')}`,
                });
              }).catch(err => {
                console.error('Erreur lors de la copie :', err);
              });
            } else {
              console.error('Accès au presse-papiers refusé');
            }
          });

        }
        if (index === 'print') {
          // console.log('Print!');
          var contenu = document.getElementById("mardown").innerHTML;
          var fenetreImpression = window.open('', '_blank');
          fenetreImpression.document.write(contenu);
          fenetreImpression.document.close();
          fenetreImpression.focus();
          fenetreImpression.print();
          fenetreImpression.close();
        }

        if (index === 'download') {
          // console.log('Download!');
          const text = self.doc.doc;
          const blob = new Blob([text], { type: "text/plain;charset=utf-8" });
          const url = URL.createObjectURL(blob);
          const a = document.createElement("a");
          a.href = url;
          // slugify self.doc.name pour le nom du fichier
          const slugify = self.doc.name.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
          a.download = `${slugify}.md`;
          a.click();
          URL.revokeObjectURL(url);
        }

        if (index === 'slideshow') {
          // console.log('Slideshow!');
          Router.go('noteSlidePublic', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), docId: pageSession.get('docId') });
        }

        return true;
      },
    });
  },
  'click .table-of-contents a'(event, instance) {
    event.preventDefault();
    var hash = event.target.getAttribute('href').substring(1); // Supprime le '#' du début
    var element = document.getElementById(hash);
    if (element) {
      element.scrollIntoView();
    }
  },
});

Template.noteSlidePublic.onCreated(function () {
  this.ready = new ReactiveVar(false);
  pageSession.set('scopeId', null);
  pageSession.set('scope', null);
  pageSession.set('docId', null);

  // on récupère les paramètres de l'url pour les stocker dans la session
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    pageSession.set('docId', Router.current().params.docId ? Router.current().params.docId : undefined);
  }.bind(this));

  // le subscribe ce fait que si le docId est présent
  this.autorun(function () {
    const docId = pageSession.get('docId');
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (docId && scope && scopeId) {
      // console.log('subscribe documentUpdates');
      const handleScope = this.subscribe('noteOne', scope, scopeId, docId, true);
      Tracker.autorun(() => {
        const isReady = handleScope.ready();
        // console.log('Subscription is ready:', isReady);
        if (isReady) {
          this.ready.set(isReady);
          //changer le titre de la page
          const document = Notes.findOne({ _id: new Mongo.ObjectID(docId) });
          if (document) {
            window.document.title = document.name;
          }
        }
      });
    }
  }.bind(this));
})

Template.noteSlidePublic.onRendered(function () {
  const slideThemes = ['white', 'black', 'moon', 'beige', 'blood', 'dracula', 'league', 'night', 'serif', 'simple', 'sky', 'solarized', 'white-contrast', 'black-contrast', 'white_contrast_compact_verbatim_headers'];
  const instance = this;
  instance.deck = new Reveal();
  pageSession.setDefault('currentSlideState', {
    indexHorizontal: 0,
    indexVertical: 0
  });

  pageSession.set('slideTheme', 'white');

  this.autorun(function () {
    if (instance.ready.get()) {
      console.log('ready');

      Tracker.autorun(function () {

        if (instance._cssLink) {
          // Retirer le fichier CSS du document
          document.head.removeChild(instance._cssLink);
        }

        if (pageSession.get('slideTheme') && slideThemes.includes(pageSession.get('slideTheme')) && !Platform.isAndroid() && !Platform.isIOS()) {
          // Créer un élément link pour le fichier CSS
          const link = document.createElement('link');
          link.href = `/theme/${pageSession.get('slideTheme')}.css`; // Assurez-vous que le chemin est correct
          link.type = 'text/css';
          link.rel = 'stylesheet';

          // Conserver une référence à l'élément link pour le retirer plus tard
          instance._cssLink = link;

          // Ajouter le fichier CSS au document
          document.head.appendChild(link);

        } else {
          // Créer un élément link pour le fichier CSS
          const link = document.createElement('link');
          link.href = `/theme/white.css`; // Assurez-vous que le chemin est correct
          link.type = 'text/css';
          link.rel = 'stylesheet';

          // Conserver une référence à l'élément link pour le retirer plus tard
          instance._cssLink = link;

          // Ajouter le fichier CSS au document
          document.head.appendChild(link);
        }
      });

      instance.deck.initialize({
        embedded: true,
        width: 1280,
        height: 1280,
        margin: 0.1,
        minScale: 0.2,
        maxScale: 1.5,
        // slideNumber: true
        plugins: [NotesPlugin]
      }).then(() => {
        // reveal.js is ready
        instance.deck.layout();
        instance.deck.slide(0, 0, 0);
        instance.deck.addEventListener('slidechanged', (event) => {
          pageSession.set('currentSlideState', {
            indexHorizontal: event.indexh,
            indexVertical: event.indexv ?? 0
          });

        })
      });
    }
  });

  Notes.find().observeChanges({
    added(id, fields) {
      if (fields.doc) {
        const { metadata, content } = extraireEtParserYAML(fields.doc);
        if (metadata && metadata.slideOptions) {
          if (metadata.slideOptions.theme && slideThemes.includes(metadata.slideOptions.theme)) {
            pageSession.set('slideTheme', metadata.slideOptions.theme);
          }
          instance.deck.configure(metadata.slideOptions);
          // console.log('slide observeChanges');
        }
      }
    },
    changed(id, fields) {
      if (fields.doc) {
        console.log('slide observeChanges');
        instance.deck.destroy();
        instance.ready.set(false);
        instance.ready.set(true);
        // deck.sync();
        // const current = pageSession.get('currentSlideState');
        // deck.slide(current.indexHorizontal, current.indexVertical);
        // console.log('slide observeChanges');
      }
    }
  });

});

Template.noteSlidePublic.onDestroyed(function () {
  // Retirer le fichier CSS du document
  const instance = Template.instance();
  if (this._cssLink) {
    document.head.removeChild(this._cssLink);
  }
  instance.deck.destroy();
});

Template.noteSlidePublic.helpers({
  scope() {
    const scope = pageSession.get('scope');
    const scopeId = pageSession.get('scopeId');
    if (scope && scopeId) {
      const collection = nameToCollection(scope);
      return collection.findOne({ _id: new Mongo.ObjectID(scopeId) });
    }
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  doc() {
    if (pageSession.get('docId')) {
      const document = Notes.findOne({ _id: new Mongo.ObjectID(pageSession.get('docId')) });
      return document;
    }
  },
});