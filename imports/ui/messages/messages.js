/* eslint-disable consistent-return */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';
import { AutoForm } from 'meteor/aldeed:autoform';
import { IonToast } from 'meteor/meteoric:ionic';

import './messages.html';

import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Actions } from '../../api/collection/actions.js';

import { nameToCollection } from '../../api/helpers.js';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;

const pageSession = new ReactiveDict('pageSearchMessages');

Template.pageMessages.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('dataContext', null);
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      if (dataContext.actionId) {
        template.actionId = dataContext.actionId;
        template.roomId = dataContext.roomId;
      }
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
      pageSession.set('dataContext', template.dataContext);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      if (Router.current().params.actionId) {
        template.actionId = Router.current().params.actionId;
        template.roomId = Router.current().params.roomId;
      }
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
      template.dataContext = false;
      pageSession.set('dataContext', template.dataContext);
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    if (template.actionId) {
      pageSession.set('actionId', template.actionId);
    }

    if (template.actionId) {
      const handle = Meteor.subscribe('detailActionsOpti', template.scope, template._id, template.roomId, template.actionId);
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    } else {
      const handle = Meteor.subscribe('scopeDetail', template.scope, template._id);
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    }
  }.bind(this));
});

Template.pageMessages.helpers({
  scope() {
    if (Template.instance().scope) {
      if (Template.instance().actionId) {
        return Actions.findOne({ _id: new Mongo.ObjectID(Template.instance().actionId) });
      }
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    }
    // return undefined;
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  actionId() {
    return pageSession.get('actionId');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  error() {
    return pageSession.get('error');
  },
});

Template.pageMessagesModal.inheritsHelpersFrom('pageMessages');
Template.pageMessagesModal.inheritsEventsFrom('pageMessages');
Template.pageMessagesModal.inheritsHooksFrom('pageMessages');

AutoForm.addHooks(['formMessages'], {
  before: {
    method(doc) {
      pageSession.set('error', null);
      const scopeId = pageSession.get('scopeId');
      const scope = pageSession.get('scope');
      if (pageSession.get('actionId')) {
        doc.actionId = pageSession.get('actionId');
      }
      doc.parentId = scopeId;
      doc.parentType = scope;
      return doc;
    },
  },
  after: {
    method(error) {
      if (!error) {
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Send message')}`,
        });
        if (pageSession.get('dataContext')) {
          IonModal.close();
        }
        pageSession.set('error', null);
      }
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(':', ' '));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      } else {
        pageSession.set('error', error.reason.replace(':', ' '));
      }
    }
  },
});
