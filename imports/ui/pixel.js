/* eslint-disable consistent-return */
/* eslint-disable meteor/no-session */
/* global Session device cordova */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { Mongo } from 'meteor/mongo';
import { ReactiveVar } from 'meteor/reactive-var';
import { $ } from 'meteor/jquery';
import { ActivityStream, ActivityStreamReference } from '../api/collection/activitystream.js';
import QRCode from 'qrcode';
import { StatusTimes } from '../api/helpers.js';

import './notifications/notifications.js';

import './pixel.html';

import { Organizations } from '../api/collection/organizations.js';
import { Citoyens } from '../api/collection/citoyens.js';
import { Projects } from '../api/collection/projects.js';

import './components/scope/item.js';

import { Pomodoro } from '../api/client/podomoro.js';

import './rooms/actions/podomoro/podomoro.html';
import './rooms/actions/podomoro/podomoro.js';
import { pageSession } from '../api/client/reactive.js';

Template.layout.onCreated(function () {
  Meteor.subscribe('notificationsCountUser');

  // this.ready = new ReactiveVar(false);
  // this.autorun(function () {
  //   // si citoyen direct sans orgaCibleId
  //   if (Session.get('orgaCibleId')) {
  //     const handleScopeDetail = Meteor.subscribe('scopeDetail', 'organizations', Session.get('orgaCibleId'));
  //     const handleDirectoryListProjects = Meteor.subscribe('directoryListProjects', 'organizations', Session.get('orgaCibleId'));
  //     const handleCitoyen = Meteor.subscribe('citoyen');
  //     console.log('tracker autorun layout');
  //     if (handleCitoyen.ready()) {
  //       Pomodoro.podomoroSessionSetDefault('pomodoroTimeWork', StatusTimes.work);
  //       Pomodoro.podomoroSessionSetDefault('pomodoroTimeShortRest', StatusTimes.short_rest);
  //       Pomodoro.podomoroSessionSetDefault('pomodoroTimeLongRest', StatusTimes.long_rest);
  //     }

  //     if (handleScopeDetail.ready() && handleDirectoryListProjects.ready() && handleCitoyen.ready()) {
  //       const orgaOne = Organizations.findOne({
  //         _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
  //       });
  //       if (orgaOne && orgaOne.oceco) {
  //         Session.setPersistent('settingOceco', orgaOne.oceco);
  //         this.ready.set(handleScopeDetail.ready());
  //       }
  //     }
  //   }
  // }.bind(this));

  this.autorun(function () {
    if (Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { 'oceco.pomodoroTimeWork': 1, 'oceco.pomodoroTimeShortRest': 1, 'oceco.pomodoroTimeLongRest': 1 } })) {
      const citoyenOne = () => (Meteor.userId() && Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { oceco: 1 } }) ? Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { oceco: 1 } }) : null);
      if (citoyenOne && citoyenOne().oceco) {
        Pomodoro.podomoroSessionSet('pomodoroTimeWork', (citoyenOne().oceco.pomodoroTimeWork * 60) || StatusTimes.work);
        Pomodoro.podomoroSessionSet('pomodoroTimeShortRest', (citoyenOne().oceco.pomodoroTimeShortRest * 60) || StatusTimes.short_rest);
        Pomodoro.podomoroSessionSet('pomodoroTimeLongRest', (citoyenOne().oceco.pomodoroTimeLongRest * 60) || StatusTimes.long_rest);
      }
    }
  });
});

Template.layout.events({
  'click [target=_blank]'(event) {
    event.preventDefault();
    const url = $(event.currentTarget).attr('href');
    if (Meteor.isCordova) {
      cordova.InAppBrowser.open(url, '_system');
    } else {
      window.open(url, '_blank');
    }
  },
  'click [target=_system]'(event) {
    event.preventDefault();
    const url = $(event.currentTarget).attr('href');
    if (Meteor.isCordova) {
      cordova.InAppBrowser.open(url, '_system');
    } else {
      window.open(url, '_system');
    }
  },
  'change .all-read input'() {
    Meteor.call('allSeen');
    Meteor.call('allRead');
  },
  'click .all-seen'() {
    Meteor.call('allSeen');
  },
  'click .js-filter-context'(event) {
    if (Session.get('filterContextChecked') !== true) {
      Session.setPersistent('filterContextChecked', true);
    } else {
      Session.setPersistent('filterContextChecked', null);
    }
  },
  'click .close-modal-js'(event) {
    IonModal.close();
  },
});

Template.layout.helpers({
  scope() {
    const orgaOne = Organizations.findOne({
      _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
    });
    return orgaOne;
  },
  allReadChecked(notificationsCount) {
    if (notificationsCount === 0) {
      return 'checked';
    }
    return undefined;
  },
  notifications() {
    if (Session.get('filterContextChecked')) {
      const orgaNotif = ActivityStream.api.isUnreadContext(undefined, Session.get('orgaCibleId'))
      return orgaNotif;
      // return ActivityStream.api.isUnread(undefined, Session.get('orgaCibleId'), "organizations");
    } else {
      return ActivityStream.api.isUnread();
    }
  },
  routeSwitch() {
    return Router.current().route.getName() !== 'switch';
  },
  routeNotCitoyens() {
    return !(Router.current().params && Router.current().params.scope && Router.current().params.scope === 'citoyens') && Router.current().route.getName() !== 'notesEditor' && Router.current().route.getName() !== 'notesEditorOrga' && Router.current().route.getName() !== 'notesSlideOrga' && Router.current().route.getName() !== 'notesSlide';
  },
  lastPlayedTask() {
    return Pomodoro.podomoroSessionGet('lastPlayedTask');
  }
});

Template.forceUpdateAvailable.events({
  'click .positive-url'(event) {
    event.preventDefault();
    const url = event.currentTarget.getAttribute('href');
    window.open(url, '_system');
  },
});

Template.shareUrl.events({
  'focus input[name="shareurl"]'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareurl"]');
    element.select();
  },
  'click #copyurl'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareurl"]');
    element.select();
    navigator.permissions.query({ name: 'clipboard-write' }).then((result) => {
      if (result.state === 'granted' || result.state === 'prompt') {
        navigator.clipboard.writeText(element.value).then(function () {
          /* clipboard successfully set */
        }, function () {
          /* clipboard write failed */
        });
      }
    });
  },
  'click #shareurl'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="shareurl"]');
    element.select();
    if (navigator.share) {
      navigator.share({
        url: element.value,
      })
        .then(() => console.log('Successful share'))
        .catch((error) => console.log('Error sharing', error));
    }
  },
});

Template.shareUrl.helpers({
  shareUrl() {
    return Meteor.absoluteUrl(window.location.pathname.replace(/\//, ''));
  },
  canShare() {
    return navigator.share;
  },
});

Template.generateQRCode.helpers({
  getQRCode() {
    const instance = Template.instance();
    return instance.Qrcode.get();
  }
});

Template.generateQRCode.onCreated(function () {
  const instance = this;
  instance.Qrcode = new ReactiveVar();
  QRCode.toDataURL(this.data.url)
    .then((url) => {
      // console.log(url);
      instance.Qrcode.set(url);
    })
    .catch((err) => {
      console.error(err);
    });
});

Template.filehandler.onCreated(function () {
  // verifier si on ouvert l'app avec un fichier
  // si il y a un fichier ouvert on le garde en memoire, pour pouvoir l'utliser quelque part ou c'est possible
  // dans les notes ou dans une action

  pageSession.set('docOpen', null);
  pageSession.set('imageOpen', null);
  pageSession.set('isFileOpen', false);

  if (Router.current().params.query.file) {
    if ("launchQueue" in window) {
      window.launchQueue.setConsumer(async (launchParams) => {
        try {
          if (launchParams.files && launchParams.files.length > 0) {
            const fileHandle = launchParams.files[0];
            const blob = await fileHandle.getFile();

            const maxSize = 5 * 1024 * 1024;

            if (blob.size > maxSize) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('File size exceeds the 5MB limit')}`,
              });
              return; // Arrête le traitement du fichier
            }

            if (blob.type === "image/jpeg" || blob.type === "image/jpg" || blob.type === "image/png") {
              if (blob.type !== "image/png") {
                const canvas = document.createElement('canvas');
                const ctx = canvas.getContext('2d');
                const img = new Image();
                img.src = URL.createObjectURL(blob);
                img.onload = function () {
                  canvas.width = img.width;
                  canvas.height = img.height;
                  ctx.drawImage(img, 0, 0);
                  canvas.toBlob(function (pngBlob) {
                    // Copier le fichier PNG dans le presse-papiers
                    if ('clipboard' in navigator && 'write' in navigator.clipboard) {
                      const item = new ClipboardItem({ 'image/png': pngBlob });
                      navigator.clipboard.write([item]).then(() => {
                        IonToast.show({
                          template: blob.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('Image copied to clipboard')}`,
                        });
                      }).catch((err) => {
                        console.error('Erreur lors de la copie du fichier dans le presse-papiers :', err);
                        IonToast.show({
                          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Unable to copy to clipboard')}`,
                        });
                      });
                    } else {
                      console.error('Le presse-papiers n\'est pas disponible pour ecrire des fichiers');
                    }
                  }, 'image/png');
                };
              } else {
                // Copier directement le fichier PNG dans le presse-papiers
                if ('clipboard' in navigator && 'write' in navigator.clipboard) {
                  const item = new ClipboardItem({ 'image/png': blob });
                  navigator.clipboard.write([item]).then(() => {
                    IonToast.show({
                      template: blob.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('Image copied to clipboard')}`,
                    });
                  }).catch((err) => {
                    console.error('Erreur lors de la copie du fichier dans le presse-papiers :', err);
                    IonToast.show({
                      title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Unable to copy to clipboard')}`,
                    });
                  });
                } else {
                  console.error('Le presse-papiers n\'est pas disponible pour ecrire des fichiers');
                }
              }


            } else if (!blob.type || blob.type === "text/plain") {
              // Définit un FileReader pour déterminer le type de contenu
              const reader = new FileReader();
              reader.onload = async (event) => {
                try {
                  const content = event.target.result;

                  // Vérifie si le contenu semble être du texte
                  if (content.match(/[\x20-\x7E\n]/)) {
                    // Traite comme un fichier texte
                    const textContent = await blob.text();
                    if (navigator.clipboard && textContent) {
                      navigator.clipboard.writeText(textContent).then(() => {
                        IonToast.show({
                          template: blob.name, position: 'top', type: 'progress', showClose: false, title: `<i class="icon fa fa-upload"></i> ${i18n.__('Content copied to clipboard')}`,
                        });
                      }).catch((err) => {

                        IonPopup.confirm({
                          title: i18n.__('open File text'),
                          template: i18n.__('Do you want to open this file in a personal note'),
                          onOk() {
                            pageSession.set('docOpen', textContent);
                            pageSession.set('isFileOpen', true);
                            Router.go('meNoteAdd');
                          },
                          onCancel() {

                          },
                          cancelText: i18n.__('cancel'),
                          okText: i18n.__('yes'),
                        });
                      });
                    }

                  }
                } catch (error) {
                  console.error('Erreur lors de la détermination du type de fichier :', error);
                }
              };

              // Lit seulement les premiers 1024 octets pour vérifier le type ou le contenu
              reader.readAsText(blob.slice(0, 1024));
            }


          }
        } catch (error) {
          console.error('Erreur lors de la gestion du fichier :', error);
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Error processing file')}`,
          });
        }
      });
    }
  }

});