/* eslint-disable meteor/no-session */
/* global IonToast */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';
import { _ } from 'meteor/underscore';
import i18n from 'meteor/universe:i18n';

import { Organizations } from '../../../api/collection/organizations.js';

import { pageSession } from '../../../api/client/reactive.js';

import { scrollPagination } from '../../../api/helpers.js';

import './assignUserDashboard.html';

// window.Organizations = Organizations;

Template.assignUserDashboard.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });
  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  self.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', 'organizations');
  });

  self.autorun(function () {
    const paramPub = { scopeId: Router.current().params._id, limit: self.limit.get('limit') };
    if (pageSession.get('search')) {
      paramPub.search = pageSession.get('search');
    }
    const handle = Meteor.subscribe('listMembers', paramPub);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });

  });
});

Template.assignUserDashboard.onRendered(scrollPagination);

Template.assignUserDashboard.helpers({
  organizations() {
    return Organizations.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
  },
  listAssign(search) {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    let listMembers = [];
    const listMembersActions = orgaOne.listMembersDash(search);
    listMembers = [...listMembersActions.fetch()];

    const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);

    return listMembersUnique.sort((a, b) => {
      if (a.assignDash > b.assignDash) {
        return -1;
      }
      if (a.assignDash < b.assignDash) {
        return 1;
      }
      if (a.assignDash === b.assignDash && a.assignDash === 1) {
        return -1;
      }
      if (a.assignDash === b.assignDash && a.assignDash === 0) {
        return 1;
      }
      return 0;
    });
  },
  countMembers(search) {
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    let listMembers = [];
    const listMembersActions = orgaOne.listMembers(search);
    listMembers = [...listMembersActions.fetch()];
    const listMembersUnique = listMembers.filter((v, i, a) => a.findIndex((t) => (t._id === v._id)) === i);
    const countMembers = listMembersUnique && listMembersUnique.length ? listMembersUnique.length : 0;
    return countMembers;
  },
  search() {
    return pageSession.get('search');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

/*
admin : assigner un user à une action
admin : de-assigner un user à une action
*/

Template.assignUserDashboard.events({
  'click .assigndashmember-js'(event) {
    event.preventDefault();
    Meteor.call('assignDash', { orgId: Router.current().params._id, memberId: this._id.valueOf(), action: 'push' }, (error) => {
      if (error) {
        // instance.state.set('call', false);
        // IonPopup.alert({ template: i18n.__(error.reason) });
      }
    });
  },
  'click .unassigndashmember-js'(event) {
    event.preventDefault();
    const orgId = Router.current().params._id;
    Meteor.call('assignDash', {
      orgId, memberId: this._id.valueOf(), action: 'pull',
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      }
    });
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('search', null);
    }
  }, 500),
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});
