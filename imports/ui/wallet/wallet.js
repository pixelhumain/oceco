/* eslint-disable meteor/no-session */
/* global IonToast Session */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';

// collection
import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Actions } from '../../api/collection/actions.js';
import { Rooms } from '../../api/collection/rooms.js';

import { searchAction } from '../../api/client/reactive.js';

import './wallet.html';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;
// window.Actions = Actions;
// window.Rooms = Rooms;

Template.wallet.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.selectview = new ReactiveVar('aFaire');
  this.autorun(function () {
    if (this.selectview.get() === 'valides') {
      const handle = this.subscribe('user.actions.historiqueOpti', 'organizations', Session.get('orgaCibleId'));
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    } else {
      const handle = this.subscribe('user.actionsOpti', 'organizations', Session.get('orgaCibleId'), this.selectview.get());
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    }
  }.bind(this));
});

Template.buttonActionFinish.events({
  'click .finish-action-js'(event) {
    event.preventDefault();
    const actionId = this.action._id.valueOf();
    Meteor.call('finishAction', {
      id: actionId,
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
  'click .sortir-action-js'(event) {
    event.preventDefault();
    const actionId = this.action._id.valueOf();
    const orgId = Session.get('orgaCibleId');
    Meteor.call('exitAction', {
      id: actionId, orgId,
    }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error}`,
        });
      }
    });
  },
});

Template.wallet.events({
  'click .change-selectview-js'(event) {
    event.preventDefault();
    Template.instance().selectview.set(event.currentTarget.id);
  },

});

Template.wallet.helpers({
  scope() {
    return Organizations.findOne({
      _id: new Mongo.ObjectID(Session.get('orgaCibleId')),
    });
  },
  search() {
    return searchAction.get('search');
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  selectview() {
    return Template.instance().selectview.get();
  },
});

Template.buttonActionFinish.helpers({
  options() {
    const actionStates = [{
      finihed: 'Fini',
      unsubscribe: 'Annuler',
    }];
    return actionStates;
  },
});

Template.whalletInputAction.onCreated(function () {
  this.displayDesc = new ReactiveVar(false);
});

Template.whalletInputAction.events({
  'click .display-desc-js'(event) {
    event.preventDefault();
    if (!Template.instance().displayDesc.get()) {
      Template.instance().displayDesc.set(true);
    } else {
      Template.instance().displayDesc.set(false);
    }
  },
  'click .tracking-js'(event) {
    event.preventDefault();
    const tracking = this.action.tracking !== true;
    Meteor.call('trackingAction', { actionId: this.action._id.valueOf(), tracking }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Tracking action'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__(tracking ? 'tracking this action' : 'untracking this action')}`,
        });
      }
    });
  },
});
Template.whalletInputAction.helpers({
  displayDesc() {
    return Template.instance().displayDesc.get();
  },
});

Template.whalletInputInWaitingAction.helpers({
  badgeColorTest() {
    return this.action.credits > 0 ? this.badgeColor : this.badgeColorDepense;
  },
});
