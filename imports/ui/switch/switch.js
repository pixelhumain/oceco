import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import { Router } from 'meteor/iron:router';
import { _ } from 'meteor/underscore';

// collection
import { Organizations } from '../../api/collection/organizations.js';
import { Citoyens } from '../../api/collection/citoyens.js';

import './switch.html';
import { ActivityStream } from '/imports/api/collection/activitystream.js';

const SwitchFavoris = new Mongo.Collection('switchfavoris', { idGeneration: 'MONGO' });

Template.switch.helpers({
  citoyenScope() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
  },
});

Template.switchList.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.search = new ReactiveVar('');
  this.autorun(function () {
    const handle = this.subscribe('orga.switch');
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.switchList.helpers({
  OrganizationsOceco() {
    // Récupération de l'utilisateur actuel et de ses favoris
    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { 'oceco.favoris.organizations': 1 } });

    let favorisIds = [];

    if (citoyen && citoyen.oceco && citoyen.oceco.favoris && citoyen.oceco.favoris.organizations) {
      favorisIds = citoyen.oceco.favoris.organizations || [];
    }

    const findRequest = { oceco: { $exists: true }, parent: { $exists: false } };

    if (Template.instance().search.get().length > 0) {
      findRequest.name = { $regex: Template.instance().search.get(), $options: 'i' };
    }

    const allOrganizations = Organizations.find(findRequest).fetch();

    allOrganizations.forEach(org => {
      org.ocecoFavoris = favorisIds.includes(org._id.valueOf());
    });

    // Trier les organisations côté client
    allOrganizations.sort((a, b) => {
      const aIsFavori = favorisIds.includes(a._id.valueOf());
      const bIsFavori = favorisIds.includes(b._id.valueOf());

      if (aIsFavori && !bIsFavori) {
        return -1;
      } else if (!aIsFavori && bIsFavori) {
        return 1;
      }
      return 0;
    });

    return allOrganizations;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  search() {
    return Template.instance().search.get();
  },
});

Template.switchList.events({
  'keyup #search, change #search': _.debounce((event, instance) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);
      instance.search.set(event.currentTarget.value);
    } else {
      instance.search.set('');
    }
  }, 500),
});

Template.switchItem.events({
  'click .switchredirect-js'(event, instance) {
    event.preventDefault();
    if (this.orga.collection === 'organizations') {
      Router.go('switchRedirect', { _id: this.orga._id.valueOf() });
    }
    if (this.orga.collection === 'projects') {
      Router.go('detailListOrga', { orgaCibleId: this.orga.orgaId, scope: 'projects', _id: this.orga._id.valueOf() });
    }
    if (this.orga.collection === 'notes') {
      if (this.orga.parentType === 'organizations') {
        Router.go('notesEditorOrga', { orgaCibleId: this.orga.organizationId, scope: this.orga.parentType, _id: this.orga.parentId, docId: this.orga._id.valueOf() });
      }
      if (this.orga.parentType === 'projects') {
        Router.go('notesEditorOrga', { orgaCibleId: this.orga.organizationId, scope: this.orga.parentType, _id: this.orga.parentId, docId: this.orga._id.valueOf() });
      }
      if (this.orga.parentType === 'citoyens') {
        Router.go('notesEditor', { scope: this.orga.parentType, _id: this.orga.parentId, docId: this.orga._id.valueOf() });
      }
    }
  },
  'click .favorisorga-js'(event, instance) {
    event.stopPropagation();
    if (this.orga.collection === 'organizations') {
      Meteor.call('favorisScope', { scope: 'organizations', scopeId: this.orga._id.valueOf() }, function (err, res) {
        if (err) {
          // console.log(err);
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
          });
        } else {
          IonToast.show({
            template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('favorites')}`,
          });
        }
      });
    }
    if (this.orga.collection === 'projects') {
      Meteor.call('favorisScope', { scope: 'projects', scopeId: this.orga._id.valueOf(), options: { orgaId: this.orga.orgaId } }, function (err, res) {
        if (err) {
          // console.log(err);
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
          });
        } else {
          IonToast.show({
            template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('favorites')}`,
          });
        }
      });
    } else if (this.orga.collection === 'notes') {
      // const options = { parentId: this.orga.parentId, parentType: this.orga.parentType };
      // if (this.orga.organizationId) {
      //   options.organizationId = this.orga.organizationId;
      // }

      Meteor.call('trackingNote', { tracking: false, docId: this.orga._id.valueOf(), organizationId: this.orga.organizationId }, function (err, res) {
        if (err) {
          // console.log(err);
          IonToast.show({
            title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
          });
        } else {
          IonToast.show({
            template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('favorites')}`,
          });
        }
      });
    }
  }
});

Template.switchItemCount.helpers({
  countNotifaction() {
    if (this.orga.collection === 'organizations') {
      return ActivityStream.api.countUnseenContext(Meteor.userId(), this.orga._id.valueOf());
    }
    if (this.orga.collection === 'projects') {
      return ActivityStream.api.countUnseenContext(Meteor.userId(), this.orga.orgaId, this.orga._id.valueOf());
    }
  },
});

Template.switchListFavoris.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.currentFilter = new ReactiveVar('all');
  this.activeTabs = new ReactiveVar([]); // Stocke les types de favoris présents

  this.autorun(function () {
    const citoyen = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { 'oceco.favoris.organizations': 1, 'oceco.favoris.projects': 1, 'oceco.favoris.notes': 1 } });
    if (citoyen && citoyen.oceco && citoyen.oceco.favoris) {
      // Vérification des types de favoris présents
      const activeTabs = [];
      if (citoyen.oceco.favoris.organizations && citoyen.oceco.favoris.organizations.length > 0) {
        activeTabs.push('organizations');
      }
      if (citoyen.oceco.favoris.projects && citoyen.oceco.favoris.projects.length > 0) {
        activeTabs.push('projects');
      }
      if (citoyen.oceco.favoris.notes && citoyen.oceco.favoris.notes.length > 0) {
        activeTabs.push('notes');
      }

      this.activeTabs.set(activeTabs); // Mise à jour de la liste des types de favoris présents

      const handle = this.subscribe('orga.switchFavoris', citoyen.oceco.favoris);
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    }
  }.bind(this));
});

Template.switchListFavoris.helpers({
  OrganizationsOceco() {
    const filter = Template.instance().currentFilter.get();
    const query = {};

    if (filter !== 'all') {
      query.collection = filter;
    }

    const allOrganizations = SwitchFavoris.find(query, { sort: { collection: 1 } }).fetch();

    allOrganizations.forEach(org => {
      org.ocecoFavoris = true;
    });

    return allOrganizations;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  tabItemActive(tab) {
    return Template.instance().currentFilter.get() === tab ? 'tab-item-active' : '';
  },
  activeTabs() {
    return Template.instance().activeTabs.get(); // Récupère les types de favoris actifs
  },
  getTabIcon(tab) {
    switch (tab) {
      case 'organizations':
        return 'fa fa-users';
      case 'projects':
        return 'fa fa-lightbulb-o';
      case 'notes':
        return 'fas fa-sticky-note';
      default:
        return '';
    }
  },
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },
  showTabs() {
    return Template.instance().activeTabs.get().length > 1;
  }
});

Template.switchListFavoris.events({
  'click .tab-item'(event, instance) {
    event.preventDefault();
    const tab = event.currentTarget.id.replace('filterFavoris', '').toLowerCase();
    instance.currentFilter.set(tab);
  },
});
