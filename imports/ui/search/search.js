/* global IonActionSheet IonPopup IonToast */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { _ } from 'meteor/underscore';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';

import './search.html';
import { Citoyens } from '../../api/collection/citoyens.js';

const pageSession = new ReactiveDict('pageSearchGlobal');

Template.searchGlobal.onRendered(function () {
  this.autorun(function () {
    if (pageSession.get('filter')) {
      const query = pageSession.get('filter');
      const querySearch = {};
      if (query.charAt(0) === '#' && query.length > 1) {
        querySearch.name = query;
        querySearch.searchTag = [query.substr(1)];
      } else {
        querySearch.name = query;
      }
      // querySearch['searchTag'] = query;
      // querySearch['locality'] = ;
      querySearch.searchType = ['organizations'];
      querySearch.searchBy = 'ALL';
      Meteor.call('searchGlobalautocomplete', querySearch, function (error, result) {
        const data = (result && result.results) ? result.results : result;
        const array = _.map(data, (arraySearch) => ({
          _id: arraySearch._id.valueOf(),
          name: arraySearch.name,
          profilThumbImageUrl: arraySearch.profilThumbImageUrl,
          type: arraySearch.type,
          collection: arraySearch.collection,
          address: arraySearch.address,
        }));
        // console.log(array);
        if (data) {
          pageSession.set('searchGlobal', array);
        }
      });
    }
  });
});

Template.searchGlobal.helpers({
  searchGlobal() {
    return pageSession.get('searchGlobal');
  },
  citoyenScope() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { 'roles.superAdmin': 1 } });
  },
  countSearchGlobal() {
    return pageSession.get('searchGlobal') && pageSession.get('searchGlobal').length;
  },
  filter() {
    return pageSession.get('filter');
  },
  icone(icone) {
    if (icone === 'citoyens') {
      return { class: 'icon fa fa-user yellow' };
    } if (icone === 'projects') {
      return { class: 'icon fa fa-lightbulb-o purple' };
    } if (icone === 'organizations') {
      return { class: 'icon fa fa-users green' };
    } if (icone === 'city') {
      return { class: 'icon fa fa-university red' };
    }
    return undefined;
  },
  urlType() {
    if (this.collection === 'citoyens') {
      return { class: 'icon fa fa-user yellow' };
    } if (this.collection === 'projects') {
      return { class: 'icon fa fa-lightbulb-o purple' };
    } if (this.collection === 'organizations') {
      return { class: 'icon fa fa-users green' };
    } if (this.collection === 'city') {
      return { class: 'icon fa fa-university red' };
    }
    return undefined;
  },
});

Template.searchGlobal.events({
  'click .orga-config-oceco-js'(event) {
    event.preventDefault();
    // info,description,contact
    const self = this;
    if (self.oceco) {
      IonActionSheet.show({
        titleText: i18n.__('Config oceco organization'),
        buttons: [
          // { text: `${i18n.__('copier tout les admin de l\'organisation mére')}` },
          // { text: `${i18n.__('copier tout les membres de l\'organisation mére')}` },
          // { text: `${i18n.__('inviter des membres / admin de l\'organisation mére')}` },
        ],
        destructiveText: `${i18n.__('Delete the oceco config')} <i class="icon ion-trash-a"></i>`,
        destructiveButtonClicked() {
          // console.log('Edit!');
          IonPopup.confirm({
            title: i18n.__('delete'),
            template: i18n.__('Remove the oceco config from this organization'),
            onOk() {
              Meteor.call('deleteConfigOceco', { scopeId: self._id, scope: 'organizations' }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                  });
                } else {
                  IonToast.show({
                    template: i18n.__('deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Config oceco organization')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
          return true;
        },
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            // Router.go('invitations', { _id: self._id.valueOf(), scope: 'organizations' });
          }
          if (index === 1) {
            // console.log('Edit!');

          }
          if (index === 2) {
            // console.log('Edit!');

          }

          return true;
        },
      });
    } else {
      IonActionSheet.show({
        titleText: i18n.__('Config oceco organization'),
        buttons: [
          { text: `${i18n.__('copy the default oceco config')}` },
        ],
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            // console.log('Edit!');
            Meteor.call('copieConfigDefaultOceco', { scopeId: self._id, scope: 'organizations' }, function (error, result) {
              if (result) {
                const copie = pageSession.get('filter');
                pageSession.set('filter', null);
                pageSession.set('filter', copie);
                IonToast.show({
                  template: i18n.__('The default oceco config has been copied to the organization'), position: 'bottom', type: 'success', showClose: false, title: `<i class= "icon ion-checkmark" ></i> ${i18n.__('config oceco')}`,
                });
              }
            });
          }
          return true;
        },
      });
    }
  },
  'click .copie-config-oceco-js'(event) {
    event.preventDefault();
    Meteor.call('copieConfigDefaultOceco', { scopeId: this._id, scope: 'organizations' }, function (error, result) {
      if (result) {
        const copie = pageSession.get('filter');
        pageSession.set('filter', copie);
        IonToast.show({
          template: i18n.__('The default oceco config has been copied to the organization'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('config oceco')}`,
        });
      }
    });
  },
  'keyup #search, change #search': _.throttle((event) => {
    if (event.currentTarget.value.length > 2) {
      pageSession.set('filter', event.currentTarget.value);
    }
  }, 500),
});
