/* eslint-disable no-underscore-dangle */
/* eslint-disable meteor/no-session */
/* global Session IonModal IonToast cordova */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Router } from 'meteor/iron:router';
import { AutoForm } from 'meteor/aldeed:autoform';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { Accounts } from 'meteor/accounts-base';
import { HTTP } from 'meteor/jkuester:http';
import i18n from 'meteor/universe:i18n';

import { remove as removeAccent } from 'remove-accents';

import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import frLocale from '@fullcalendar/core/locales/fr';
import listPlugin from '@fullcalendar/list';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import { createPopper } from '@popperjs/core';

// collections
import { Citoyens } from '../../api/collection/citoyens.js';
import { Projects } from '../../api/collection/projects.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Events } from '../../api/collection/events.js';
import { BlockEventsRest } from '../../api/schema/events.js';

// submanager
import { listsSubs } from '../../api/client/subsmanager.js';

// import '../map/map.js';
import '../components/scope/item.js';

import '../../../node_modules/@fullcalendar/common/main.css';
import '../../../node_modules/@fullcalendar/list/main.css';
import '../../../node_modules/@fullcalendar/daygrid/main.css';
import '../../../node_modules/@fullcalendar/timegrid/main.css';
import './calendar.css';

import './list.html';

import { pageSession } from '../../api/client/reactive.js';
// eslint-disable-next-line import/named
import { searchQuery, queryGeoFilter, matchTags, nameToCollection, getBackgroundColor } from '../../api/helpers.js';

Template.listEvents.onCreated(function () {
  pageSession.set('startDateCal', null);
  pageSession.set('endDateCal', null);
  const self = this;
  const dataContext = Template.currentData();
  const template = Template.instance();
  self.eventsCalendar = new ReactiveDict();
  self.ready = new ReactiveVar();
  pageSession.set('dataContext', null);

  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope ?? 'organizations';
      template._id = Router.current().params._id ?? Session.get('orgaCibleId');
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
  });

  self.autorun(function () {
    const handle = self.subscribe('directoryProjectsListEvents', template.scope, template._id, pageSession.get('startDateCal'), pageSession.get('endDateCal'));

    Tracker.autorun(() => {
      if (handle.ready()) {
        const query = {};
        const inputDateStart = pageSession.get('startDateCal');
        const inputDateEnd = pageSession.get('endDateCal');
        const collection = nameToCollection(template.scope);
        const events = collection.findOne({ _id: new Mongo.ObjectID(template._id) }).listProjectsEventsCreator(query, inputDateStart, inputDateEnd);
        if (events) {
          const eventsParse = events.map((event) => {
            const project = event.organizerEvent();
            const backgroundColor = event.endDate && moment().isAfter(event.endDate) ? '#ccc' : '#E33551';
            const eventParse = {
              id: event._id.valueOf(),
              title: event.name,
              projectName: project[0].values[0].name,
              projectId: project[0].values[0]._id.valueOf(),
              backgroundColor,
              start: event.startDate,
              end: event.endDate,
              url: `/organizations/${Session.get('orgaCibleId')}/events/actions/${event._id.valueOf()}`,
              source: project[0].values[0]._id.valueOf(),
            };
            return eventParse;
          });
          self.eventsCalendar.set('eventsCalendar', eventsParse);
        }
        self.ready.set(handle.ready());
      }
    });

  });
});

Template.listEvents.onRendered(function () {
  const template = Template.instance();
  const self = this;
  const calendarEl = self.find('#calendar');
  const options = {
    plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin],
    // defaultView: 'timeGridWeek',
    // height: 'parent',
    aspectRatio: 1,
    height: '100%',
    locales: frLocale,
    locale: 'fr',
    timeZone: 'local',
    views: {
      dayGridMonth: {
        dayMaxEventRows: true,
      },
      timeGridWeek: {
        eventMaxStack: 3,
      },
      timeGridDay: {
        eventMaxStack: 3,
      },
    },
    viewDidMount(view) {
      pageSession.set('startDateCal', calendar.view.activeStart);
      pageSession.set('endDateCal', calendar.view.activeEnd);
    },
    eventClick(info) {
      info.jsEvent.preventDefault();
      if (info.event.url) {
        Router.go(info.event.url);
      }
    },
    eventDidMount(info) {
      if (info.event.extendedProps.projectName) {
        const eventTitle = info.el.getElementsByClassName('fc-list-event-title')[0];
        if (eventTitle) {
          const projectNode = document.createElement('strong');
          const txt = document.createTextNode(` - ${info.event.extendedProps.projectName.replace(' <br />', '')}`);
          projectNode.appendChild(txt);
          projectNode.setAttribute('style', `color:${getBackgroundColor(info.event.extendedProps.projectName)}`);
          eventTitle.appendChild(projectNode);
        }
      }
    },
    select(info) {
      if (moment(info.start).format() > moment().format()) {
        const parentDataContext = {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), startDate: info.startStr, endDate: info.endStr,
        };
        const isAdmin = !!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`);
        if (isAdmin) {
          IonModal.open('eventsAddModal', parentDataContext);
        }
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Please select valid dates')}`,
        });
        calendar.unselect();
      }
    },
    eventDrop(info) {
      if (moment(info.event.start).format() > moment().format()) {
        const isAdmin = (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        if (isAdmin) {
          const modifier = {};
          modifier.$set = {};
          modifier.$set.typeElement = 'events';
          modifier.$set.block = 'when';
          modifier.$set.startDate = info.event.start;
          modifier.$set.endDate = info.event.end;
          Meteor.call('updateBlock', { modifier, _id: info.event.id }, (error, result) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Error')}`,
              });
            } else {
              IonToast.show({
                title: i18n.__('update'), position: 'top', type: 'success', showClose: true, template: `<i class="icon fa fa-check-circle"></i> ${i18n.__('Dates have been changed')}`,
              });
            }
          });
        }
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Please select valid dates')}`,
        });
      }
    },
    eventResize(info) {
      if (moment(info.event.start).format() > moment().format()) {
        const isAdmin = (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        if (isAdmin) {
          const modifier = {};
          modifier.$set = {};
          modifier.$set.typeElement = 'events';
          modifier.$set.block = 'when';
          modifier.$set.startDate = info.event.start;
          modifier.$set.endDate = info.event.end;
          Meteor.call('updateBlock', { modifier, _id: info.event.id }, (error, result) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Error')}`,
              });
            } else {
              IonToast.show({
                title: i18n.__('update'), position: 'top', type: 'success', showClose: true, template: `<i class="icon fa fa-check-circle"></i> ${i18n.__('Dates have been changed')}`,
              });
            }
          });
        }
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Please select valid dates')}`,
        });
      }
    },
  };

  const calendar = new Calendar(calendarEl, options);

  self.autorun(function () {
    if (Meteor.isCordova) {
      calendar.setOption('selectable', false);
      calendar.setOption('headerToolbar', {
        left: 'title',
        center: 'ical,changeMonth,changeYear',
        right: 'prev,next',
      });
      calendar.setOption('initialView', 'listMonth');
      calendar.setOption('customButtons', {
        ical: {
          text: 'ical',
          click() {
            IonModal.open('ical');
          },
        },
        prev: {
          icon: 'chevron-left',
          click() {
            calendar.prev();
            pageSession.set('startDateCal', calendar.view.activeStart);
            pageSession.set('endDateCal', calendar.view.activeEnd);
          },
        },
        next: {
          icon: 'chevron-right',
          click() {
            calendar.next();
            pageSession.set('startDateCal', calendar.view.activeStart);
            pageSession.set('endDateCal', calendar.view.activeEnd);
          },
        },
        changeMonth: {
          text: 'mois',
          click() {
            calendar.changeView('listMonth');
            pageSession.set('startDateCal', calendar.view.activeStart);
            pageSession.set('endDateCal', calendar.view.activeEnd);
          },
        },
        changeYear: {
          text: 'année',
          click() {
            calendar.changeView('listYear');
            pageSession.set('startDateCal', calendar.view.activeStart);
            pageSession.set('endDateCal', calendar.view.activeEnd);
          },
        },
        changeActions: {
          text: 'actions',
          click() {
            // calendar.destroy();
            Router.go('listActionsAgendaScopeOrga', { orgaCibleId: Session.get('orgaCibleId'), scope: template.scope, _id: template._id });
          },
        },
      });

      if (pageSession.get('scope') === 'projects') {
        calendar.changeView('listMonth');
      } else {
        calendar.changeView('listMonth');
      }

    } else {
      if (pageSession.get('scope') === 'projects') {
        calendar.setOption('selectable', !!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        calendar.setOption('editable', !!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        calendar.setOption('headerToolbar', {
          left: 'title',
          center: 'changeGridMonth,changeGridWeek,changeGridDay,changeMonth,changeYear',
          right: 'today,prev,next',
        });
        calendar.setOption('initialView', 'dayGridMonth');
        calendar.setOption('customButtons', {
          ical: {
            text: 'ical',
            click() {
              IonModal.open('ical');
            },
          },
          prev: {
            icon: 'chevron-left',
            click() {
              calendar.prev();
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          next: {
            icon: 'chevron-right',
            click() {
              calendar.next();
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeMonth: {
            text: 'liste mois',
            click() {
              calendar.changeView('listMonth');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeYear: {
            text: 'liste année',
            click() {
              calendar.changeView('listYear');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeActions: {
            text: 'actions',
            click() {
              // calendar.destroy();
              Router.go('listActionsAgendaScopeOrga', { orgaCibleId: Session.get('orgaCibleId'), scope: template.scope, _id: template._id });
            },
          },
          changeGridMonth: {
            text: 'mois',
            click() {
              calendar.changeView('dayGridMonth');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeGridWeek: {
            text: 'semaine',
            click() {
              calendar.changeView('timeGridWeek');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeGridDay: {
            text: 'jour',
            click() {
              calendar.changeView('timeGridDay');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
        });

        calendar.changeView('dayGridMonth');

      } else {
        calendar.setOption('selectable', false);
        calendar.setOption('editable', false);
        calendar.setOption('headerToolbar', {
          left: 'title',
          center: 'ical,changeMonth,changeYear,changeActions',
          right: 'prev,next',
        });
        calendar.setOption('initialView', 'listMonth');
        calendar.setOption('customButtons', {
          ical: {
            text: 'ical',
            click() {
              IonModal.open('ical');
            },
          },
          prev: {
            icon: 'chevron-left',
            click() {
              calendar.prev();
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          next: {
            icon: 'chevron-right',
            click() {
              calendar.next();
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeMonth: {
            text: 'mois',
            click() {
              calendar.changeView('listMonth');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeYear: {
            text: 'année',
            click() {
              calendar.changeView('listYear');
              pageSession.set('startDateCal', calendar.view.activeStart);
              pageSession.set('endDateCal', calendar.view.activeEnd);
            },
          },
          changeActions: {
            text: 'actions',
            click() {
              // calendar.destroy();
              Router.go('listActionsAgendaScopeOrga', { orgaCibleId: Session.get('orgaCibleId'), scope: template.scope, _id: template._id });
            },
          },
        });

        calendar.changeView('listMonth');

      }
    }
  });


  self.autorun(function () {
    if (Template.instance().ready.get()) {
      const eventsCalendar = Template.instance().eventsCalendar.get('eventsCalendar') ?? [];
      const eventsCalendarDiff = eventsCalendar.filter((item) => !calendar.getEventById(item.id));
      calendar.addEventSource(eventsCalendarDiff);
      calendar.render();
    }
  });
});

Template.listEvents.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.listActionsAgenda.onCreated(function () {
  pageSession.setDefault('startDateCal', null);
  pageSession.setDefault('endDateCal', null);
  pageSession.setDefault('actionsTags', null);
  pageSession.setDefault('actionsNoTags', true);
  pageSession.setDefault('isJustMe', false);
  const self = this;
  const dataContext = Template.currentData();
  const template = Template.instance();
  self.actionsCalendar = new ReactiveDict();
  self.ready = new ReactiveVar();
  pageSession.set('dataContext', null);
  pageSession.setDefault('search', null);


  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
  });

  self.autorun(function () {
    if (pageSession.get('startDateCal') && pageSession.get('endDateCal')) {

      const handle = self.subscribe('all.actionsSCope', template.scope, template._id, pageSession.get('startDateCal'), pageSession.get('endDateCal'), pageSession.get('isJustMe'));
      Tracker.autorun(() => {
        if (handle.ready()) {
          const query = {};
          const inputDateStart = pageSession.get('startDateCal');
          const inputDateEnd = pageSession.get('endDateCal');
          const collection = nameToCollection(template.scope);
          const actions = collection.findOne({ _id: new Mongo.ObjectID(template._id) }).actionsAllAgenda(inputDateStart, inputDateEnd);
          if (actions) {
            // const ressources = [];
            const tags = [];
            const actionsParse = actions.map((action) => {
              const scopeParent = action.parentScope();
              // const backgroundColor = action.endDate && moment().isAfter(action.endDate) ? '#ccc' : getBackgroundColor(scopeParent.name);
              // const borderColor = action.endDate && moment().isAfter(action.endDate) ? '#ccc' : getBackgroundColor(scopeParent.name);
              // const textColor = action.endDate && moment().isAfter(action.endDate) ? getBackgroundColor(scopeParent.name) : '#FFFFFF';
              const backgroundColor = action.endDate && moment().isAfter(action.endDate) ? '#ccc' : (action.tags && action.tags.length > 0 ? getBackgroundColor(action.tags[0]) : '#E33551');
              const borderColor = action.endDate && moment().isAfter(action.endDate) ? '#ccc' : (action.tags && action.tags.length > 0 ? getBackgroundColor(action.tags[0]) : '#E33551');
              const textColor = action.endDate && moment().isAfter(action.endDate) ? '#000000' : '#FFFFFF';
              const isMe = action?.links?.contributors?.[Meteor.userId()] ? true : false;
              const eventParse = {
                id: action._id.valueOf(),
                title: action.name,
                scopeName: scopeParent.name,
                scopeId: action.parentId,
                scope: action.parentType,
                roomId: action.idParentRoom,
                status: action.status,
                isMe,
                backgroundColor,
                textColor,
                borderColor,
                start: action.status !== 'disabled' ? (action.endDate ? action.startDate : new Date()) : action.startDate,
                end: action.status !== 'disabled' ? (action.endDate ?? new Date()) : action.startDate,
                url: template.scope !== 'citoyens' ? `/organizations/${Session.get('orgaCibleId')}/${action.parentType}/rooms/${action.parentId}/room/${action.idParentRoom}/action/${action._id.valueOf()}` : `/${action.parentType}/rooms/${action.parentId}/room/${action.idParentRoom}/action/${action._id.valueOf()}`,
                source: scopeParent._id.valueOf(),
              };

              if (action.tags && action.tags.length > 0) {
                eventParse.tags = action.tags;
              }

              eventParse.allDay = (action.startDate < inputDateStart && action.endDate > inputDateEnd) ||
                (action.startDate.getHours() === 0 && action.startDate.getMinutes() === 0 &&
                  action.endDate.getHours() === 0 && action.endDate.getMinutes() === 0) ? true : false;

              return eventParse;
            });

            self.actionsCalendar.set('actionsCalendar', actionsParse);
          }

          self.ready.set(handle.ready());
        }
      });

    }
  });
});

Template.listActionsAgenda.onRendered(function () {
  const self = this;
  const calendarEl = self.find('#calendarActions');
  const tooltip = document.createElement('div');
  tooltip.classList.add('tooltip');
  document.body.appendChild(tooltip);
  const options = {
    plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin, listPlugin],
    aspectRatio: 1,
    height: 'calc(100vh - 155px)',
    customButtons: {
      prev: {
        icon: 'chevron-left',
        click() {
          calendar.prev();
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
      next: {
        icon: 'chevron-right',
        click() {
          calendar.next();
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
      changeMonth: {
        text: 'liste mois',
        click() {
          calendar.changeView('listMonth');
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
      changeYear: {
        text: 'liste année',
        click() {
          calendar.changeView('listYear');
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
      changeGridMonth: {
        text: 'mois',
        click() {
          calendar.changeView('dayGridMonth');
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
      changeGridWeek: {
        text: 'semaine',
        click() {
          calendar.changeView('timeGridWeek');
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
      changeGridDay: {
        text: 'jour',
        click() {
          calendar.changeView('timeGridDay');
          pageSession.set('startDateCal', calendar.view.activeStart);
          pageSession.set('endDateCal', calendar.view.activeEnd);
        },
      },
    },
    locales: frLocale,
    locale: 'fr',
    timeZone: 'local',
    views: {
      dayGridMonth: {
        dayMaxEventRows: true,
      },
      timeGridWeek: {
        eventMaxStack: 3,
      },
      timeGridDay: {
        eventMaxStack: 3,
      },
    },
    viewDidMount(view) {
      pageSession.set('startDateCal', calendar.view.activeStart);
      pageSession.set('endDateCal', calendar.view.activeEnd);
    },
    eventClick(info) {
      info.jsEvent.preventDefault();
      /* if (info.event.url) {
        Router.go(info.event.url);
      } */
      const parentDataContext = {
        _id: info.event.extendedProps.scopeId, scope: info.event.extendedProps.scope, roomId: info.event.extendedProps.roomId, actionId: info.event.id,
      };
      if (pageSession.get('scope') !== 'citoyens') {
        parentDataContext.orgaCibleId = Session.get('orgaCibleId');
        const isMember = Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
        if (isMember) {
          IonModal.open('detailActionsModal', parentDataContext);
        }
      } else {
        if (info.event.url) {
          Router.go(info.event.url);
        }
      }
    },
    eventDidMount(info) {
      if (info.view.type === 'timeGridWeek' || info.view.type === 'dayGridMonth') {
        // Ajouter les événements de survol

        info.el.addEventListener('mouseenter', () => {
          tooltip.textContent = info.event.title;
          tooltip.style.visibility = 'visible';
          const popperInstance = createPopper(info.el, tooltip, {
            placement: 'top', // Position du tooltip par rapport au bouton
            modifiers: [
              {
                name: 'offset',
                options: {
                  offset: [0, 10], // Décalage horizontal et vertical du tooltip
                },
              },
            ],
          });
          popperInstance.update();
        });

        info.el.addEventListener('mouseleave', () => {
          tooltip.style.visibility = 'hidden';
        });
      }

      if (info.event.extendedProps.scopeName) {
        const eventTitle = info.el.getElementsByClassName('fc-list-event-title')[0];
        if (eventTitle) {
          const projectNode = document.createElement('strong');
          const txt = document.createTextNode(` - ${info.event.extendedProps.scopeName.replace(' <br />', '')}`);
          projectNode.appendChild(txt);
          projectNode.setAttribute('style', `color:${getBackgroundColor(info.event.extendedProps.scopeName)}`);
          eventTitle.appendChild(projectNode);
        }
      }

      const search = pageSession.get('search');
      if (search) {
        if (search && search.charAt(0) === '#' && search.length > 1) {

          const tagsUniquesSearch = info.event.extendedProps.tags && info.event.extendedProps.tags.length > 0 ? info.event.extendedProps.tags.filter(element => removeAccent(element).toLowerCase().startsWith(removeAccent(search.substr(1).toLowerCase()))) : [];

          if (tagsUniquesSearch && tagsUniquesSearch.length > 0) {
            if (info.event.display !== 'auto') {
              info.event.setProp('display', '');
            }
          } else {
            if (info.event.display !== 'none') {
              info.event.setProp('display', 'none');
            }
          }
        } else {
          if (info.event.title.toLowerCase().includes(search.toLowerCase())) {
            if (info.event.display !== 'auto') {
              info.event.setProp('display', '');
            }
          } else {
            if (info.event.display !== 'none') {
              info.event.setProp('display', 'none');
            }
          }
        }

      } else {
        if (info.event.display !== 'auto') {
          info.event.setProp('display', '');
        }
      }

    },
    select(info) {
      if (moment(info.start).format() > moment().format()) {
        const parentDataContext = {
          orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: pageSession.get('scope'), startDate: info.startStr, endDate: info.endStr,
        };
        const isAdmin = (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        if (isAdmin) {
          IonModal.open('actionsAddModal', parentDataContext);
        }
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Please select valid dates')}`,
        });
        calendar.unselect();
      }
    },
    eventDrop(info) {
      if (moment(info.event.start).format() > moment().format()) {
        const isAdmin = (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        if (isAdmin) {
          const modifier = {};
          modifier.$set = {};
          modifier.$set.parentType = pageSession.get('scope');
          modifier.$set.parentId = pageSession.get('scopeId');
          modifier.$set.startDate = info.event.start;
          modifier.$set.endDate = info.event.end;
          Meteor.call('updateAction', { modifier, _id: info.event.id, noNotif: true }, (error, result) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Error')}`,
              });
            } else {
              IonToast.show({
                title: i18n.__('update'), position: 'top', type: 'success', showClose: true, template: `<i class="icon fa fa-check-circle"></i> ${i18n.__('Dates have been changed')}`,
              });
            }
          });
        }
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Please select valid dates')}`,
        });
      }
    },
    eventResize(info) {
      if (moment(info.event.start).format() > moment().format()) {
        const isAdmin = (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`));
        if (isAdmin) {
          const modifier = {};
          modifier.$set = {};
          modifier.$set.parentType = pageSession.get('scope');
          modifier.$set.parentId = pageSession.get('scopeId');
          modifier.$set.startDate = info.event.start;
          modifier.$set.endDate = info.event.end;
          Meteor.call('updateAction', { modifier, _id: info.event.id, noNotif: true }, (error, result) => {
            if (error) {
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Error')}`,
              });
            } else {
              IonToast.show({
                title: i18n.__('update'), position: 'top', type: 'success', showClose: true, template: `<i class="icon fa fa-check-circle"></i> ${i18n.__('Dates have been changed')}`,
              });
            }
          });
        }
      } else {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__('Please select valid dates')}`,
        });
      }
    },
  };

  /* options.refetchResourcesOnNavigate = true;
  options.resources = function (fetchInfo, successCallback, failureCallback) {
    console.log('ressources')
    successCallback(Template.instance().actionsCalendar.get('actionsRessources') ?? []);
  }; */

  const calendar = new Calendar(calendarEl, options);
  Template.instance().calendar = calendar;

  if (Meteor.isCordova) {
    calendar.setOption('selectable', false);
    calendar.setOption('headerToolbar', {
      left: 'title',
      center: 'changeMonth',//changeYear
      right: 'prev,next',
    });
    calendar.setOption('initialView', 'listMonth');
    calendar.changeView('listMonth');
  } else {
    calendar.setOption('editable', (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)));
    calendar.setOption('selectable', (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)));
    calendar.setOption('headerToolbar', {
      left: 'title',
      center: 'changeGridMonth,changeGridWeek,changeMonth', //changeYear
      right: 'today,prev,next',
    });
    calendar.setOption('initialView', 'dayGridMonth');
    calendar.changeView('dayGridMonth');
  }

  self.autorun(function () {
    if (Meteor.isCordova) {

    } else {
      calendar.setOption('editable', (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)));
      calendar.setOption('selectable', (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)));
    }
  });

  // self.autorun(function () {
  //   console.log('calendar set options')
  //   if (Meteor.isCordova) {
  //     calendar.setOption('selectable', false);
  //     calendar.setOption('headerToolbar', {
  //       left: 'title',
  //       center: 'changeMonth,changeYear',
  //       right: 'prev,next',
  //     });
  //     calendar.setOption('initialView', 'listMonth');
  //     calendar.changeView('listMonth');
  //   } else {
  //     calendar.setOption('editable', (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)));
  //     calendar.setOption('selectable', (pageSession.get('scopeId') === Meteor.userId() && pageSession.get('scope') === 'citoyens') ? true : (!!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`)));
  //     calendar.setOption('headerToolbar', {
  //       left: 'title',
  //       center: 'changeGridMonth,changeGridWeek,changeMonth,changeYear',
  //       right: 'today,prev,next',
  //     });
  //     calendar.setOption('initialView', 'dayGridMonth');
  //     console.log('calendar change view');
  //     calendar.changeView('dayGridMonth');
  //   }
  // });

  const mergeTags = (tags, tagsPrev) => {
    // Créer une copie de 'tags' pour ne pas modifier le tableau original
    const mergedTags = [...tags];

    if (tagsPrev) {
      tagsPrev.forEach(prevTag => {
        // Trouver l'index du tag existant dans 'tags'
        const index = mergedTags.findIndex(tag => tag.tag === prevTag.tag);

        if (index !== -1) {
          // Si le tag existe déjà dans 'tags', mettre à jour 'checked' seulement si 'checked' est false dans 'tagsPrev'
          if (!prevTag.checked) {
            mergedTags[index].checked = false;
          }
        } else {
          if (!prevTag.checked) {
            // Si le tag n'existe pas dans 'tags', l'ajouter avec 'count' à 0 et 'checked' à false
            mergedTags.push({ ...prevTag, count: 0, checked: false });
          }
        }
      });
    }

    return mergedTags;
  };

  self.autorun(function () {
    if (Template.instance().ready.get()) {
      // Récupération des actions du calendrier
      const actionsCalendar = Template.instance().actionsCalendar.get('actionsCalendar') ?? [];

      // Vérification de l'état "isJustMe" de manière réactive
      const isJustMe = pageSession.get('isJustMe');

      // Filtrer les actions en fonction de "isJustMe"
      const filteredActions = isJustMe
        ? actionsCalendar.filter(action => action.isMe) // Garder uniquement les actions associées à l'utilisateur actuel
        : actionsCalendar; // Prendre toutes les actions si "isJustMe" est désactivé

      // Calcul des tags à partir des actions filtrées
      const tags = filteredActions
        .flatMap(action => action.tags ?? [])
        .reduce((acc, tag) => {
          const existingTag = acc.find(item => item.tag === tag);
          if (existingTag) {
            existingTag.count++;
          } else {
            acc.push({ tagId: Random.id(), tag, count: 1, checked: true });
          }
          return acc;
        }, [])
        .sort((a, b) => b.count - a.count);

      // Récupérer les tags précédents sans déclencher de réactivité
      let tagsPrev;
      Tracker.nonreactive(() => {
        tagsPrev = pageSession.get('actionsTags');
      });

      // Mettre à jour les tags si nécessaire
      if (JSON.stringify(tags) !== JSON.stringify(tagsPrev)) {
        const mergedTags = mergeTags(tags, tagsPrev); // Fusionner les nouveaux tags avec les anciens
        pageSession.set('actionsTags', mergedTags); // Mettre à jour la session avec les tags fusionnés
      }

      // Différencier les actions pour les ajouter ou les mettre à jour dans le calendrier
      const actionsCalendarDiff = filteredActions.filter(item => {
        const event = calendar.getEventById(item.id);
        if (!event) {
          return true;
        }

        const hasChanged = ['allDay', 'title', 'backgroundColor', 'display'].some(
          prop => (event[prop] ?? false) !== (item[prop] ?? false)
        );
        if (hasChanged) {
          event.remove();
          return true;
        }
        return false;
      });

      // Supprimer un événement si un ID de suppression est présent
      if (pageSession.get('removeCalendarActionId')) {
        const removeId = pageSession.get('removeCalendarActionId');
        calendar.getEventById(removeId)?.remove();
        pageSession.delete('removeCalendarActionId');
        calendar.render();
      }

      // Ajouter les nouvelles actions ou mises à jour au calendrier
      if (actionsCalendarDiff && actionsCalendarDiff.length > 0) {
        calendar.addEventSource(actionsCalendarDiff);
      }
    }

    // Toujours rafraîchir le calendrier à la fin
    calendar.render();
  });


  // self.autorun(function () {
  //   if (pageSession.get('actionsTags')) {
  //     let noTagsPrev;
  //     Tracker.nonreactive(() => {
  //       noTagsPrev = pageSession.get('actionsNoTags');
  //     });
  //     const tagsUniques = pageSession.get('actionsTags').filter((tag) => !tag.checked).map((tag) => tag.tag);
  //     calendar.batchRendering(function () {
  //       calendar.getEvents().forEach(function (eventOne) {
  //         const tagsUniquesSearch = eventOne.extendedProps.tags && eventOne.extendedProps.tags.length > 0 ? eventOne.extendedProps.tags.filter(element => tagsUniques.includes(element)) : [];
  //         if (tagsUniquesSearch && tagsUniquesSearch.length > 0) {
  //           if (eventOne.display !== 'none') {
  //             eventOne.setProp('display', 'none');
  //           }
  //         } else {
  //           if (noTagsPrev === false) {
  //             if (!eventOne.extendedProps.tags || eventOne.extendedProps.tags.length === 0) {
  //               if (eventOne.display !== 'none') {
  //                 eventOne.setProp('display', 'none');
  //               }
  //             }
  //           } else {
  //             if (eventOne.display !== 'auto') {
  //               eventOne.setProp('display', '');
  //             }
  //           }
  //         }
  //       });
  //     });
  //   }
  // });

  // self.autorun(function () {
  //   if (pageSession.get('actionsNoTags') === false) {
  //     calendar.batchRendering(function () {
  //       calendar.getEvents().forEach(function (eventOne) {
  //         const tagsNoTagsSearch = !eventOne.extendedProps.tags ? true : false;
  //         if (tagsNoTagsSearch) {
  //           if (eventOne.display !== 'none') {
  //             eventOne.setProp('display', 'none');
  //           }
  //         }
  //       });
  //     });
  //   } else {
  //     calendar.batchRendering(function () {
  //       calendar.getEvents().forEach(function (eventOne) {
  //         const tagsNoTagsSearch = !eventOne.extendedProps.tags ? true : false;
  //         if (tagsNoTagsSearch) {
  //           if (eventOne.display !== 'auto') {
  //             eventOne.setProp('display', '');
  //           }
  //         }
  //       });
  //     });
  //   }
  // });

  // self.autorun(function () {
  //   if (pageSession.get('isJustMe') === true) {
  //     calendar.batchRendering(function () {
  //       calendar.getEvents().forEach(function (eventOne) {
  //         console.log('eventOne', eventOne);
  //         const isMe = !eventOne.extendedProps.isMe ? true : false;
  //         if (isMe) {
  //           if (eventOne.display !== 'none') {
  //             eventOne.setProp('display', 'none');
  //           }
  //         }
  //       });
  //     });
  //   } else {
  //     calendar.batchRendering(function () {
  //       calendar.getEvents().forEach(function (eventOne) {
  //         console.log('eventOne', eventOne);
  //         const isMe = !eventOne.extendedProps.isMe ? true : false;
  //         if (isMe) {
  //           if (eventOne.display !== 'auto') {
  //             eventOne.setProp('display', '');
  //           }
  //         }
  //       });
  //     });
  //   }
  // });

  self.autorun(() => {
    const isJustMe = pageSession.get('isJustMe');
    const noTags = pageSession.get('actionsNoTags');
    const tagsToHide = pageSession.get('actionsTags')?.filter(tag => !tag.checked).map(tag => tag.tag) || [];

    calendar.batchRendering(() => {
      calendar.getEvents().forEach(event => {
        const isHiddenByTags = tagsToHide.length > 0 && event.extendedProps.tags?.some(tag => tagsToHide.includes(tag));
        const isHiddenByNoTags = noTags === false && (!event.extendedProps.tags || event.extendedProps.tags.length === 0);
        const isHiddenByIsJustMe = isJustMe && !event.extendedProps.isMe;

        const shouldBeHidden = isHiddenByTags || isHiddenByNoTags || isHiddenByIsJustMe;
        event.setProp('display', shouldBeHidden ? 'none' : '');
      });
    });
  });

});


Template.listActionsAgenda.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
  isCitoyen() {
    return Template.instance().scope === 'citoyens' ? true : false;
  },
  search() {
    return pageSession.get('search');
  },
  isTagsUnchecked() {
    return pageSession.get('actionsTags') ? pageSession.get('actionsTags').filter((tag) => !tag.checked).length > 0 : false;
  },
  isJustMe() {
    return pageSession.get('isJustMe');
  }
});

Template.searchTags.helpers({
  tags() {
    return pageSession.get('actionsTags');
  },
  notag() {
    return pageSession.get('actionsNoTags');
  }
});

Template.searchTagsInput.events({
  'click .selecttag-js': function (event, instance) {
    event.preventDefault();
    const self = this;
    const tags = pageSession.get('actionsTags');
    const tagsChange = tags.map((tag) => {
      if (tag.tagId === self.tag.tagId) {
        tag.checked = !tag.checked;
      }
      return tag;
    });
    pageSession.set('actionsTags', tagsChange);
    pageSession.set('search', null);

  },
});

Template.searchNoTagsInput.events({
  'click .selectnotag-js': function (event, instance) {
    event.preventDefault();
    const self = this;
    pageSession.set('actionsNoTags', !self.notag);
    pageSession.set('search', null);
  },
});

Template.listActionsAgenda.events({
  'keyup #search, change #search': _.debounce((event, instance) => {
    if (event.currentTarget.value.length > 0) {
      pageSession.set('search', event.currentTarget.value);

      const tags = pageSession.get('actionsTags');
      const tagsChange = tags.map((tag) => {
        tag.checked = true;
        return tag;
      });
      pageSession.set('actionsTags', tagsChange);
      pageSession.set('actionsNoTags', true);


      if (event.currentTarget.value && event.currentTarget.value.charAt(0) === '#' && event.currentTarget.value.length > 0) {
        if (event.currentTarget.value.length > 1) {
          instance.calendar.batchRendering(function () {
            instance.calendar.getEvents().forEach(function (eventOne) {
              const tagsUniquesSearch = eventOne.extendedProps.tags && eventOne.extendedProps.tags.length > 0 ? eventOne.extendedProps.tags.filter(element => removeAccent(element).toLowerCase().startsWith(removeAccent(event.currentTarget.value.substr(1).toLowerCase()))) : [];
              if (tagsUniquesSearch && tagsUniquesSearch.length > 0) {
                if (eventOne.display !== 'auto') {
                  eventOne.setProp('display', '');
                }
              } else {
                if (eventOne.display !== 'none') {
                  eventOne.setProp('display', 'none');
                }
              }

            });
          });
        }
      } else {
        instance.calendar.batchRendering(function () {
          instance.calendar.getEvents().forEach(function (eventOne) {
            if (eventOne.title.toLowerCase().includes(event.currentTarget.value.toLowerCase())) {
              if (eventOne.display !== 'auto') {
                eventOne.setProp('display', '');
              }
            } else {
              if (eventOne.display !== 'none') {
                eventOne.setProp('display', 'none');
              }
            }
          });
        });
      }
    } else {
      pageSession.set('search', null);
      instance.calendar.batchRendering(function () {
        instance.calendar.getEvents().forEach(function (eventOne) {
          if (eventOne.display !== 'auto') {
            eventOne.setProp('display', '');
          }
        });
      });
    }
  }, 500),
  'click .isjusteme-js'(event, instance) {
    event.preventDefault();
    pageSession.set('isJustMe', !pageSession.get('isJustMe'));
  },
});

Template.ical.events({
  'focus input[name="icalurl"]'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="icalurl"]');
    element.select();
  },
  'click #copyical'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="icalurl"]');
    element.select();
    navigator.permissions.query({ name: 'clipboard-write' }).then((result) => {
      if (result.state === 'granted' || result.state === 'prompt') {
        navigator.clipboard.writeText(element.value).then(function () {
          /* clipboard successfully set */
        }, function () {
          /* clipboard write failed */
        });
      }
    });
  },
  'click .ical-admin-js'(event) {
    event.preventDefault();
    HTTP.get(Meteor.absoluteUrl(`ical/organizations/${Session.get('orgaCibleId')}/events/archives`), {
      headers: {
        'x-access-token': Accounts._storedLoginToken(),
        'x-user-id': Meteor.userId(),
      },
    }, function (error, result) {
      if (result && result.content) {
        let icalFile = null;
        const makeTextFile = function (resultIcal) {
          const data = new Blob([resultIcal.content], {
            type: 'text/calendar;charset=utf-8',
          });
          if (icalFile !== null) {
            window.URL.revokeObjectURL(icalFile);
          }
          icalFile = window.URL.createObjectURL(data);
          return icalFile;
        };
        const downloadLink = makeTextFile(result);
        if (Meteor.isCordova) {
          cordova.InAppBrowser.open(downloadLink, '_system');
        } else {
          window.open(downloadLink, '_blank');
        }
      }
    });
  },
});

Template.icalCitoyenAction.onCreated(function () {
  const template = Template.instance();
  template.icalurl = new ReactiveVar();
  this.state = new ReactiveDict();
  this.state.setDefault({
    call: false,
    error: false,
  });
});

Template.icalCitoyenAction.helpers({
  citoyen() {
    return Citoyens.findOne({ _id: Meteor.userId() });
  },
  icalurl() {
    return Template.instance().icalurl.get();
  },
  isCall() {
    return Template.instance().state.get('call');
  },
  error() {
    return Template.instance().state.get('error');
  },
  isTokenCreated() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()), 'loginTokens.name': 'restIcal', 'loginTokens.type': 'restAccessToken' }) ?? false;
  },
});

Template.icalCitoyenAction.events({
  'focus input[name="icalurl"]'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="icalurl"]');
    element.select();
  },
  'click #copyical'(event, instance) {
    event.preventDefault();
    const element = instance.find('input[name="icalurl"]');
    element.select();
    navigator.permissions.query({ name: 'clipboard-write' }).then((result) => {
      if (result.state === 'granted' || result.state === 'prompt') {
        navigator.clipboard.writeText(element.value).then(function () {
          /* clipboard successfully set */
        }, function () {
          /* clipboard write failed */
        });
      }
    });
  },
  'click .generateToken-js'(event, instance) {
    event.preventDefault();
    instance.state.set('error', false);
    instance.state.set('call', true);
    Meteor.call('generateTokenIcalCitoyen', {}, (error, result) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
        instance.state.set('error', error.reason.replace(': ', ''));
        instance.state.set('call', false);
      } else {
        if (result && result.ical) {
          IonToast.show({
            template: i18n.__('ical'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('genrate ical link')}`,
          });
          instance.icalurl.set(result.ical);
        }
        instance.state.set('call', false);
      }
    });
  },
});

Template.eventsAdd.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('postalCode', null);
  pageSession.set('country', null);
  pageSession.set('city', null);
  pageSession.set('cityName', null);
  pageSession.set('regionName', null);
  pageSession.set('depName', null);
  pageSession.set('localityId', null);
  pageSession.set('geoPosLatitude', null);
  pageSession.set('geoPosLongitude', null);
  pageSession.set('dataContext', null);

  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      if (dataContext.startDate) {
        template.startDate = dataContext.startDate;
      }
      if (dataContext.endDate) {
        template.endDate = dataContext.endDate;
      }
      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      pageSession.set('dataContext', true);
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('organizerType', template.scope);
    pageSession.set('organizerId', template._id);
  });

  this.autorun(function () {
    const handleList = listsSubs.subscribe('lists', 'eventTypes');
    Tracker.autorun(() => {
      template.ready.set(handleList.ready());
    });
  });
});

Template.eventsEdit.onCreated(function () {
  const template = Template.instance();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('organizerId', null);
  pageSession.set('postalCode', null);
  pageSession.set('country', null);
  pageSession.set('city', null);
  pageSession.set('cityName', null);
  pageSession.set('regionName', null);
  pageSession.set('depName', null);
  pageSession.set('localityId', null);
  pageSession.set('geoPosLatitude', null);
  pageSession.set('geoPosLongitude', null);

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', 'projects');
  });

  this.autorun(function () {
    const subs = [
      listsSubs.subscribe('lists', 'eventTypes'),
      Meteor.subscribe('scopeDetail', 'events', Router.current().params._id)
    ];
    Tracker.autorun(() => {
      const areAllSubsReady = subs.every(handle => handle.ready());
      template.ready.set(areAllSubsReady);
    });
  });
});

Template.eventsBlockEdit.onCreated(function () {
  const template = Template.instance();
  template.ready = new ReactiveVar();
  pageSession.set('error', false);
  pageSession.set('postalCode', null);
  pageSession.set('country', null);
  pageSession.set('city', null);
  pageSession.set('cityName', null);
  pageSession.set('regionName', null);
  pageSession.set('depName', null);
  pageSession.set('localityId', null);
  pageSession.set('geoPosLatitude', null);
  pageSession.set('geoPosLongitude', null);

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('block', Router.current().params.block);
  });

  this.autorun(function () {
    const subs = [
      listsSubs.subscribe('lists', 'eventTypes'),
      Meteor.subscribe('scopeDetail', 'events', Router.current().params._id)
    ];
    Tracker.autorun(() => {
      const areAllSubsReady = subs.every(handle => handle.ready());
      template.ready.set(areAllSubsReady);
    });
  });
});

Template.eventsAdd.helpers({
  event() {
    const eventEdit = {};
    const collection = nameToCollection(Template.instance().scope);
    const event = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    if (event && event.isAdmin()) {
      eventEdit.isAdmin = event.isAdmin();
    }
    if (Template.instance().startDate) {
      eventEdit.startDate = moment(Template.instance().startDate).toDate();
    }
    if (Template.instance().endDate) {
      eventEdit.endDate = moment(Template.instance().endDate).toDate();
    }
    return eventEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.eventsAddModal.inheritsHelpersFrom('eventsAdd');
Template.eventsAddModal.inheritsEventsFrom('eventsAdd');
Template.eventsAddModal.inheritsHooksFrom('eventsAdd');

Template.eventsEdit.helpers({
  event() {
    const event = Events.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    const eventEdit = {};
    eventEdit._id = event._id.valueOf();
    eventEdit.name = event.name;
    eventEdit.type = event.type;
    eventEdit.shortDescription = event.shortDescription;
    eventEdit.description = event.description;
    eventEdit.startDate = event.startDate;
    eventEdit.endDate = event.endDate;
    if (event && event.preferences) {
      eventEdit.preferences = {};
      if (event.preferences.isOpenData === 'true') {
        eventEdit.preferences.isOpenData = true;
      } else {
        eventEdit.preferences.isOpenData = false;
      }
      if (event.preferences.isOpenEdition === 'true') {
        eventEdit.preferences.isOpenEdition = true;
      } else {
        eventEdit.preferences.isOpenEdition = false;
      }
    }
    // eventEdit.allDay = event.allDay;
    eventEdit.country = event.address.addressCountry;
    eventEdit.postalCode = event.address.postalCode;
    eventEdit.city = event.address.codeInsee;
    eventEdit.cityName = event.address.addressLocality;
    if (event && event.address && event.address.streetAddress) {
      eventEdit.streetAddress = event.address.streetAddress;
    }
    if (event && event.address && event.address.regionName) {
      eventEdit.regionName = event.address.regionName;
    }
    if (event && event.address && event.address.depName) {
      eventEdit.depName = event.address.depName;
    }
    if (event && event.address && event.address.localityId) {
      eventEdit.localityId = event.address.localityId;
    }
    eventEdit.geoPosLatitude = event.geo.latitude;
    eventEdit.geoPosLongitude = event.geo.longitude;
    return eventEdit;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.eventsBlockEdit.helpers({
  event() {
    const event = Events.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    const eventEdit = {};
    eventEdit._id = event._id.valueOf();
    if (Router.current().params.block === 'descriptions') {
      eventEdit.description = event.description;
      eventEdit.shortDescription = event.shortDescription;
    } else if (Router.current().params.block === 'info') {
      eventEdit.name = event.name;
      eventEdit.type = event.type;
      if (event.tags) {
        eventEdit.tags = event.tags;
      }
      // eventEdit.email = event.email;
      eventEdit.url = event.url;
      /* if(event.telephone){
        if(event.telephone.fixe){
          eventEdit.fixe = event.telephone.fixe.join();
        }
        if(event.telephone.mobile){
          eventEdit.mobile = event.telephone.mobile.join();
        }
        if(event.telephone.fax){
          eventEdit.fax = event.telephone.fax.join();
        }
      } */
    } else if (Router.current().params.block === 'network') {
      if (event.socialNetwork) {
        if (event.socialNetwork.instagram) {
          eventEdit.instagram = event.socialNetwork.instagram;
        }
        if (event.socialNetwork.skype) {
          eventEdit.skype = event.socialNetwork.skype;
        }
        if (event.socialNetwork.github) {
          eventEdit.github = event.socialNetwork.github;
        }
        if (event.socialNetwork.twitter) {
          eventEdit.twitter = event.socialNetwork.twitter;
        }
        if (event.socialNetwork.facebook) {
          eventEdit.facebook = event.socialNetwork.facebook;
        }
      }
    } else if (Router.current().params.block === 'when') {
      // eventEdit.allDay = event.allDay;
      eventEdit.startDate = event.startDate;
      eventEdit.endDate = event.endDate;
    } else if (Router.current().params.block === 'locality') {
      if (event && event.address) {
        eventEdit.country = event.address.addressCountry;
        eventEdit.postalCode = event.address.postalCode;
        eventEdit.city = event.address.codeInsee;
        eventEdit.cityName = event.address.addressLocality;
        if (event && event.address && event.address.streetAddress) {
          eventEdit.streetAddress = event.address.streetAddress;
        }
        if (event && event.address && event.address.regionName) {
          eventEdit.regionName = event.address.regionName;
        }
        if (event && event.address && event.address.depName) {
          eventEdit.depName = event.address.depName;
        }
        if (event && event.address && event.address.localityId) {
          eventEdit.localityId = event.address.localityId;
        }
        eventEdit.geoPosLatitude = event.geo.latitude;
        eventEdit.geoPosLongitude = event.geo.longitude;
      }
    } else if (Router.current().params.block === 'preferences') {
      if (event && event.preferences) {
        eventEdit.preferences = {};
        if (event.preferences.isOpenData === true) {
          eventEdit.preferences.isOpenData = true;
        } else {
          eventEdit.preferences.isOpenData = false;
        }
        if (event.preferences.isOpenEdition === true) {
          eventEdit.preferences.isOpenEdition = true;
        } else {
          eventEdit.preferences.isOpenEdition = false;
        }
      }
    }
    return eventEdit;
  },
  blockSchema() {
    return BlockEventsRest[Router.current().params.block];
  },
  block() {
    return Router.current().params.block;
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.eventsFields.inheritsHelpersFrom('organizationsFields');
Template.eventsFields.inheritsEventsFrom('organizationsFields');
Template.eventsFields.inheritsHooksFrom('organizationsFields');

Template.eventsFields.helpers({
  organizerType() {
    return pageSession.get('organizerType');
  },
  organizerId() {
    return pageSession.get('organizerId');
  },
  optionsOrganizerId(organizerType) {
    let optionsOrganizer = false;
    if (Meteor.userId() && Citoyens && Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }) && organizerType) {
      // console.log(organizerType);
      if (organizerType === 'organizations') {
        optionsOrganizer = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).listOrganizationsCreator();
      } else if (organizerType === 'projects') {
        optionsOrganizer = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }).listProjectsCreator();
      } else if (organizerType === 'citoyens') {
        optionsOrganizer = Citoyens.find({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { _id: 1, name: 1 } });
      }
      if (optionsOrganizer) {
        // console.log(optionsOrganizer.fetch());
        return optionsOrganizer.map(function (c) {
          return { label: c.name, value: c._id.valueOf() };
        });
      }
    }
    return false;
  },
  parentId() {
    return pageSession.get('parentId');
  },
  optionsParentId(organizerType, organizerId) {
    let parent = false;
    if (Meteor.userId() && organizerType && organizerId) {
      if (organizerType === 'organizations') {
        if (Organizations && Organizations.findOne({ _id: new Mongo.ObjectID(organizerId) })) {
          parent = Organizations.findOne({ _id: new Mongo.ObjectID(organizerId) });
        }
      } else if (organizerType === 'projects') {
        if (Projects && Projects.findOne({ _id: new Mongo.ObjectID(organizerId) })) {
          parent = Projects.findOne({ _id: new Mongo.ObjectID(organizerId) });
        }
      } else if (organizerType === 'citoyens') {
        if (Citoyens && Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { name: 1, _id: 1 } })) {
          parent = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { name: 1, _id: 1 } });
        }
      }
      if (parent) {
        const optionsParentId = parent.listEventsCreator();
        if (optionsParentId) {
          // console.log(optionsParentId.fetch());
          // console.log(organizerType);
          // const arrayParent =  optionsParentId.fetch();
          if (optionsParentId.count() > 0) {
            const arrayParent = optionsParentId.map((c) => ({ label: c.name, value: c._id.valueOf() }));
            return arrayParent;
          }
          return false;
        }
      }
    }
    return false;
  },
  dataReadyOrganizer() {
    return Template.instance().readyOrganizer.get();
  },
  dataReadyParent() {
    return Template.instance().readyParent.get();
  },
  optsDatetimepicker() {
    return {
      formatValue: 'YYYY-MM-DDTHH:mm:ssZ',
      pikaday: {
        format: 'DD/MM/YYYY HH:mm',
        showTime: true,
        i18n: {
          previousMonth: 'Mois précédent',
          nextMonth: 'Mois prochain',
          months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
          weekdays: ['dimanche', ' lundi ', ' mardi ', ' mercredi ', ' jeudi ', ' vendredi ', ' samedi '],
          weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
          midnight: 'minuit',
          moon: 'midi',
        },
        showMinutes: true,
        showSeconds: false,
        use24hour: true,
        incrementHourBy: 1,
        incrementMinuteBy: 1,
        incrementSecondBy: 1,
        autoClose: true,
        timeLabel: null,
      },
    };
  },
});

Template.eventsFields.onCreated(function () {
  const self = this;
  const template = Template.instance();
  template.ready = new ReactiveVar();
  template.readyOrganizer = new ReactiveVar();
  template.readyParent = new ReactiveVar();

  self.autorun(function (c) {
    if (Router.current().params._id && Router.current().params.scope) {
      pageSession.set('scopeId', Router.current().params._id);
      pageSession.set('scope', 'projects');
      pageSession.set('organizerType', 'projects');
      pageSession.set('organizerId', Router.current().params._id);
      c.stop();
    }
  });
});

Template.eventsFields.onRendered(function () {
  const self = this;

  self.autorun(function () {
    const organizerType = pageSession.get('organizerType');
    // console.log(`autorun ${organizerType}`);
    if (organizerType && Meteor.userId()) {
      if (organizerType === 'organizations') {
        const handleOrganizer = self.subscribe('directoryListOrganizations', 'citoyens', Meteor.userId());
        Tracker.autorun(() => {
          self.readyOrganizer.set(handleOrganizer.ready());
        });
      } else if (organizerType === 'projects') {
        const handleOrganizer = self.subscribe('directoryListProjects', 'organizations', Session.get('orgaCibleId'));
        Tracker.autorun(() => {
          self.readyOrganizer.set(handleOrganizer.ready());
        });
      } else if (organizerType === 'citoyens') {
        const handleOrganizer = self.subscribe('citoyen');
        Tracker.autorun(() => {
          self.readyOrganizer.set(handleOrganizer.ready());
        });
      }
    }
  });

  self.autorun(function () {
    const organizerType = pageSession.get('organizerType');
    const organizerId = pageSession.get('organizerId');
    // console.log(`autorun ${organizerType} ${organizerId}`);
    if (organizerType && organizerId) {
      const handleParent = self.subscribe('directoryListEvents', organizerType, organizerId);
      Tracker.autorun(() => {
        self.readyParent.set(handleParent.ready());
      });
    }
  });
});

Template.eventsFields.events({
  'change select[name="organizerType"]'(event, instance) {
    event.preventDefault();
    // console.log(tmpl.$(e.currentTarget).val());
    pageSession.set('organizerType', instance.$(event.currentTarget).val());
    pageSession.set('organizerId', false);
  },
  'change select[name="organizerId"]'(event, instance) {
    event.preventDefault();
    // console.log(tmpl.$(e.currentTarget).val());
    pageSession.set('organizerId', instance.$(event.currentTarget).val());
  },
});

AutoForm.addHooks(['addEvent', 'editEvent'], {
  after: {
    method(error, result) {
      if (!error) {
        IonToast.show({
          template: i18n.__('added'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('event')}`,
        });
        if (pageSession.get('dataContext')) {
          IonModal.close();
        } else {
          Router.go('detailList', { _id: result.data.id, scope: 'events' }, { replaceState: true });
        }
      }
    },
    'method-update'(error, result) {
      if (!error) {
        Router.go('detailList', { _id: result.data.id, scope: 'events' }, { replaceState: true });
        IonToast.show({
          template: i18n.__('update'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('event')}`,
        });
      }
    },
  },
  before: {
    method(doc) {
      // console.log(doc);
      doc.organizerType = pageSession.get('organizerType');
      doc.organizerId = pageSession.get('organizerId');
      // eslint-disable-next-line no-param-reassign
      doc = matchTags(doc, pageSession.get('tags'));
      return doc;
    },
    'method-update'(modifier) {
      modifier.$set.organizerType = pageSession.get('organizerType');
      modifier.$set.organizerId = pageSession.get('organizerId');
      modifier.$set = matchTags(modifier.$set, pageSession.get('tags'));
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(': ', ''));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      }
    }
  },
});

AutoForm.addHooks(['editBlockEvent'], {
  after: {
    'method-update'(error) {
      if (!error) {
        if (pageSession.get('block') !== 'preferences') {
          Router.go('detailList', { _id: pageSession.get('scopeId'), scope: 'events' }, { replaceState: true });
        }
      }
    },
  },
  before: {
    'method-update'(modifier) {
      const scope = 'events';
      const block = pageSession.get('block');
      if (modifier && modifier.$set) {
        modifier.$set = matchTags(modifier.$set, pageSession.get('tags'));
      } else {
        modifier.$set = {};
      }
      modifier.$set.typeElement = scope;
      modifier.$set.block = block;
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(': ', ''));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${i18n.__(error.reason.replace(': ', ''))}`,
        });
      }
    }
  },
});
