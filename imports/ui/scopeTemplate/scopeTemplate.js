/* eslint-disable no-template-curly-in-string */
/* eslint-disable no-shadow */
/* eslint-disable consistent-return */
/* eslint-disable meteor/template-names */
/* eslint-disable no-underscore-dangle */
/* eslint-disable meteor/no-session */
/* global Session IonToast IonActionSheet Platform */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { $ } from 'meteor/jquery';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { MeteorCameraUI } from 'meteor/aboire:camera-ui';
import { AutoForm } from 'meteor/aldeed:autoform';
import i18n from 'meteor/universe:i18n';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import { IonPopup, IonModal, IonLoading } from 'meteor/meteoric:ionic';

// import '../qrcode/qrcode.js';

import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Rooms } from '../../api/collection/rooms.js';
import { Actions } from '../../api/collection/actions.js';
import { Forms } from '../../api/collection/forms.js';
import { Answers } from '../../api/collection/answers.js';

import { nameToCollection, compareValues, searchQuerySortActived, scrollPagination } from '../../api/helpers.js';

import { projectsCountOrga, eventsCountOrga } from '../../api/helpersOrga.js';

import { searchAction, searchNotes, pageSession as pageSessionAction } from '../../api/client/reactive.js';

import { lazy } from '../components/iolazyload.js';

import './scopeTemplate.html';

import '../components/directory/list.js';
import '../components/news/button-card.js';
import '../components/news/card.js';

// window.Events = Events;
// window.Organizations = Organizations;
// window.Projects = Projects;
// window.Citoyens = Citoyens;
// window.Rooms = Rooms;
// window.Forms = Forms;
// window.Answers = Answers;

const pageSession = new ReactiveDict('pageNews');

Template.scopeTemplate.onCreated(function () {
  this.readyScopeDetail = new ReactiveVar();
  const template = Template.instance();
  const dataContext = Template.currentData();

  pageSession.setDefault('limit', 5);

  this.autorun(function () {
    if (Router.current().route.getName() === 'notificationsList' || Router.current().route.getName() === 'notificationsListOrga') {
      pageSession.set('selectview', 'scopeNotificationsTemplate');
    } else if (Router.current().route.getName() === 'organizationsList') {
      pageSession.set('selectview', 'scopeOrganizationsTemplate');
    } else if (Router.current().route.getName() === 'projectsList') {
      pageSession.set('selectview', 'scopeProjectsTemplate');
    } else if (Router.current().route.getName() === 'eventsList' || Router.current().route.getName() === 'eventsListOrga') {
      if (Router.current().params.scope === 'organizations') {
        pageSession.set('selectview', 'scopeProjectsEventsTemplate');
      } else {
        pageSession.set('selectview', 'scopeEventsTemplate');
      }
    } else if (Router.current().route.getName() === 'formsList' || Router.current().route.getName() === 'formsListOrga') {
      pageSession.set('selectview', 'scopeFormsTemplate');
    } else if (Router.current().route.getName() === 'assessListOrga') {
      pageSession.set('selectview', 'scopeAssessTemplate');
    } else if (Router.current().route.getName() === 'answersList' || Router.current().route.getName() === 'answersListOrga') {
      pageSession.set('selectview', 'scopeAnswersTemplate');
    } else if (Router.current().route.getName() === 'actionsList' || Router.current().route.getName() === 'actionsListOrga') {
      pageSession.set('selectview', 'scopeActionsTemplate');
      pageSession.set('selectsubview', 'aFaire');
    } else if (Router.current().route.getName() === 'actionsListDepense' || Router.current().route.getName() === 'actionsListDepenseOrga') {
      pageSession.set('selectview', 'scopeActionsTemplate');
      pageSession.set('selectsubview', 'depenses');
    } else if (Router.current().route.getName() === 'contributorsList' || Router.current().route.getName() === 'contributorsListOrga') {
      pageSession.set('selectview', 'scopeProjectsContributorsTemplate');
    } else if (Router.current().route.getName() === 'projectsMilestonesList' || Router.current().route.getName() === 'projectsMilestonesListOrga') {
      pageSession.set('selectview', 'scopeProjectMilestonesTemplate');
    } else if (Router.current().route.getName() === 'roomsList') {
      pageSession.set('selectview', 'scopeRoomsTemplate');
    } else if (Router.current().route.getName() === 'notesList' || Router.current().route.getName() === 'notesListOrga') {
      pageSession.set('selectview', 'scopeNotesTemplate');
    } else {
      pageSession.set('selectview', 'scopeDetailTemplate');
    }
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {

    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;

      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;

      if (Router.current().params.orgaCibleId) {
        template.orgaCibleId = Router.current().params.orgaCibleId;
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        template.orgaCibleId = Router.current().params._id;
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
      template.dataContext = false;
    }

    if (Router.current().params.scope && Router.current().params._id) {
      const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
      Tracker.autorun(() => {
        if (handle.ready()) {
          if (!Router.current().params.orgaCibleId && Router.current().params.scope !== 'organizations') {
            const countOrga = Organizations.find({}).count();
            const orgaNoName = Organizations.find({
              name: { $exists: false },
            });

            if (countOrga > 1) {
              // console.log('news news.js 117 countOrga', countOrga, Session.get('orgaCibleId'));
              // 1 de plus quand faut changer

              if (Router.current().params.scope === 'projects') {
                const project = `links.projects.${Router.current().params._id}`;
                const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
              } else if (Router.current().params.scope === 'events') {
                const event = `links.events.${Router.current().params._id}`;
                const projectOne = Projects.find({ [event]: { $exists: 1 } });

                eventsCountOrga({
                  _id: Router.current().params._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                });
              } else if (Router.current().params.scope === 'actions') {
                const actionObjectId = new Mongo.ObjectID(Router.current().params._id);
                const actionOne = Actions.findOne({ _id: actionObjectId });
                const parentObjectId = new Mongo.ObjectID(actionOne.parentId);

                if (actionOne.parentType === 'events') {
                  // events
                  const projectOne = Projects.find({ _id: parentObjectId });
                  eventsCountOrga({
                    _id: Router.current().params._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                  });
                } else if (actionOne.parentType === 'projects') {
                  // projects
                  const project = `links.projects.${actionOne.parentId}`;
                  const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                  projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
                } else if (actionOne.parentType === 'organizations') {
                  Session.setPersistent('orgaCibleId', actionOne.parentId);
                } else if (actionOne.parentType === 'citoyens') {
                  // citoyens
                }
              }
            } else if (countOrga === 1) {
              // console.log('news news.js 144 countOrga === 1', Session.get('orgaCibleId'));
            }
          }
        }
        this.readyScopeDetail.set(handle.ready());
      });
    }
  }.bind(this));
});

Template.scopeTemplate.helpers({
  scope() {
    if (Router.current().params.scope) {
      const collection = nameToCollection(Router.current().params.scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
      if (scopeOne) {
        // scopeOne.dataContext = Template.instance().dataContext;
        scopeOne.orgaCibleId = Template.instance().orgaCibleId;
      }
      return scopeOne;
    }
    // return undefined;
  },
  scopeCardTemplate() {
    return `listCard${Router.current().params.scope}`;
  },
  isVote() {
    return this.type === 'vote';
  },
  dataReadyScopeDetail() {
    return Template.instance().readyScopeDetail.get();
  },
  selectview() {
    return pageSession.get('selectview');
  },
  scopeMe() {
    return pageSession.get('scope') === 'citoyens' && Router.current().params._id === Meteor.userId();
  },
  countMembersToBeValidated() {
    return Counter.get(`listMembersToBeValidated.${Router.current().params._id}.${Router.current().params.scope}`);
  },
  me() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) });
  },
});

Template.scopeDetailTemplate.helpers({
  scopeCardTemplate() {
    return `listCard${Router.current().params.scope}`;
  },
  dataReadyScopeDetail() {
    return Template.instance().readyScopeDetail.get();
  },
});

Template.scopeNotificationsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    if (Router.current().params.scope !== 'events') {
      const subs = [
        Meteor.subscribe('listMembersToBeValidated', Router.current().params.scope, Router.current().params._id),
        Meteor.subscribe('notificationsScope', Router.current().params.scope, Router.current().params._id),
        Meteor.subscribe('notificationsScopeCount', Router.current().params.scope, Router.current().params._id)
      ];

      Tracker.autorun(() => {
        // Vérifier si tous les abonnements sont prêts et mettre à jour isReady en conséquence
        const areAllSubsReady = subs.every(handle => handle.ready());
        this.ready.set(areAllSubsReady);
      });

    } else {
      const subs = [
        Meteor.subscribe('notificationsScope', Router.current().params.scope, Router.current().params._id),
        Meteor.subscribe('notificationsScopeCount', Router.current().params.scope, Router.current().params._id)
      ];

      Tracker.autorun(() => {
        // Vérifier si tous les abonnements sont prêts et mettre à jour isReady en conséquence
        const areAllSubsReady = subs.every(handle => handle.ready());
        this.ready.set(areAllSubsReady);
      });
    }
  }.bind(this));
});

Template.scopeNotificationsTemplate.helpers({
  scopeBoutonNotificationsTemplate() {
    return `boutonNotifications${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeNotificationsTemplate.events({
  'click .validateYes'(event, instance) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    // console.log(`${scopeId},${scope},${this._id.valueOf()},${this.scopeVar()}`);

    const linkOption = instance.data.isAdminPending(this._id.valueOf()) ? 'toBeValidated' : 'isAdminPending';
    Meteor.call('validateEntity', scopeId, scope, this._id.valueOf(), this.scopeVar(), linkOption, function (err) {
      if (err) {
        if (err.reason) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        }
      } else {
        // console.log('yes validate');
      }
    });
  },
  'click .validateNo'(event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('disconnectEntity', scopeId, scope, undefined, this._id.valueOf(), this.scopeVar(), function (err) {
      if (err) {
        if (err.reason) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        }
      } else {
        // console.log('no validate');
      }
    });
  },
});

Template.scopeProjectsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  const template = Template.instance();
  const dataContext = Template.currentData();

  pageSession.set('sortProjects', null);
  pageSession.set('searchProjects', null);

  this.autorun(function () {
    if (dataContext && dataContext.scope && dataContext._id) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;

      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;

      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
      template.dataContext = false;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
  });

  this.autorun(function () {
    const handle = this.subscribe('directoryListProjects', template.scope, template._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeProjectsTemplate.events({
  'keyup #searchProject, change #searchProject'(event) {
    if (event.currentTarget.value.length > 2) {
      // console.log('search');
      pageSession.set('searchProjects', event.currentTarget.value);
    } else {
      pageSession.set('searchProjects', null);
    }
  },
});

Template.scopeProjectsTemplate.helpers({
  scopeBoutonProjectsTemplate() {
    return `boutonProjects${Template.instance().scope}`;
  },
  searchProjects() {
    return pageSession.get('searchProjects');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

// Template.scopeProjectsTemplateModal.inheritsHelpersFrom('scopeProjectsTemplate');
Template.scopeProjectsTemplateModal.inheritsEventsFrom('scopeProjectsTemplate');
Template.scopeProjectsTemplateModal.inheritsHooksFrom('scopeProjectsTemplate');

Template.scopeProjectsTemplateModal.helpers({
  searchProjects() {
    return pageSession.get('searchProjects');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  listProjectsCreator(searchProjects) {
    if (Template.instance().scope) {
      const collection = nameToCollection(Template.instance().scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      if (scopeOne) {
        scopeOne.dataContext = Template.instance().dataContext;
        scopeOne.orgaCibleId = Template.instance().orgaCibleId;
      }
      return scopeOne.listProjectsCreator(searchProjects);
    }
    // return undefined;
  },
});

Template.scopeProjectsContributorsTemplate.onCreated(function () {
  const self = this;
  this.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });

  pageSession.set('search', null);

  this.autorun(function () {
    if (pageSession.get('search')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  self.autorun(function () {
    const paramPub = { scopeId: Router.current().params._id, limit: self.limit.get('limit') };
    if (pageSession.get('search')) {
      paramPub.search = pageSession.get('search');
    }

    const handle = Meteor.subscribe('listContributors', paramPub);
    Tracker.autorun(() => {
      self.ready.set(handle.ready());
    });
  });
});

Template.scopeProjectsContributorsTemplate.onRendered(scrollPagination);

Template.scopeProjectsContributorsTemplate.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
  search() {
    return pageSession.get('search');
  },
  isLimit(countMembers) {
    return countMembers > Template.instance().limit.get('limit') || (countMembers === Template.instance().limit.get('limit'));
  },
});

Template.scopeProjectsContributorsTemplate.events({
  'click .project-contributors-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('contributor')}: ${self.name}`,
      buttons: [{ text: i18n.__('Pass admin') }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this contributor'),
          onOk() {
            Meteor.call('deleteContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id.valueOf() }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('changeRoleContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id.valueOf(), role: 'admin' }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
  'click .project-contributors-admin-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('contributor')}: ${self.name}`,
      buttons: [{ text: i18n.__('Pass member') }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this contributor'),
          onOk() {
            Meteor.call('deleteContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id.valueOf() }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('changeRoleContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id.valueOf(), role: 'contributor' }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
  'keyup #search, change #search': _.debounce((event, instance) => {
    if (event.currentTarget.value.length > 0) {
      if (event.currentTarget.value === '/removeAll' && instance.data.isAdmin()) {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete all contributors'),
          onOk() {
            Meteor.call('deleteAllContributeur', { scopeId: pageSession.get('scopeId') }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
      }
      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('search', null);
    }
  }, 500),
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});

Template.scopeProjectMilestonesTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  pageSession.set('selectsubview', 'open');
  searchAction.set('searchMilestone', '');

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

});

Template.scopeProjectMilestonesTemplate.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  search() {
    if (searchAction.get('searchMilestone') && searchAction.get('searchMilestone').charAt(0) === '~' && searchAction.get('searchMilestone').length > 1) {
      return searchAction.get('searchMilestone').substr(1);
    }
    return searchAction.get('searchMilestone');
  },
  searchSort() {
    return searchAction.get('searchMilestoneSort');
  },
  boutonsAction() {
    return { event: 'button-positive action-project-milestone-js', icon: 'fa fa-cog' };
  },
});

Template.scopeProjectMilestonesTemplate.events({
  'click .change-selectsubview-js'(event) {
    event.preventDefault();
    pageSession.set('selectsubview', event.currentTarget.id);
  },
});

Template.milestoneAssignActionsModal.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.milestoneId = dataContext.milestoneId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.milestoneId = Router.current().params.milestoneId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('milestoneId', template.milestoneId);
    if (template.scope && template._id) {
      const paramPub = { scope: template.scope, scopeId: template._id, etat: 'todo', milestoneId: 'noMilestone' };
      if (searchAction.get('search')) {
        paramPub.search = searchAction.get('search');
      }
      if (searchAction.get('searchSort')) {
        paramPub.searchSort = searchAction.get('searchSort');
      }
      const handle = Meteor.subscribe('directoryListActions', paramPub);

      Tracker.autorun(() => {
        if (handle.ready()) {
          if (Organizations.find({}).count() === 2) {
            const orgaOne = Organizations.findOne({
              name: { $exists: false },
            });
            if (orgaOne && Session.get('orgaCibleId') !== orgaOne._id.valueOf()) {
              Session.setPersistent('orgaCibleId', orgaOne._id.valueOf());
            }
          }
          template.ready.set(handle.ready());
        }
      });
    }
  });
});

Template.milestoneAssignActionsModal.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id && Template.instance().ready.get()) {
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    }
    return undefined;
  },
  search() {
    return searchAction.get('search');
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  milestoneId() {
    return pageSession.get('milestoneId');
  },
  boutonsAction() {
    return { event: 'button-positive assign-jalon-action-js', icon: 'fa fa-check' };
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.milestoneMoveActionsModal.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.milestoneId = dataContext.milestoneId;
      template.milestoneName = dataContext.milestoneName;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.milestoneId = Router.current().params.milestoneId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('milestoneId', template.milestoneId);
    pageSession.set('milestoneName', template.milestoneName);
  });
});

Template.milestoneMoveActionsModal.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id) {
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    }
    return undefined;
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  milestoneName() {
    return pageSession.get('milestoneName');
  },
  milestoneId() {
    return pageSession.get('milestoneId');
  },
  search() {
    if (searchAction.get('searchMilestone') && searchAction.get('searchMilestone').charAt(0) === '~' && searchAction.get('searchMilestone').length > 1) {
      return searchAction.get('searchMilestone').substr(1);
    }
    return searchAction.get('searchMilestone');
  },
  searchSort() {
    return searchAction.get('searchMilestoneSort');
  },
  boutonsAction() {
    return { event: 'button-positive move-jalon-action-js', icon: 'fa fa-check' };
  },
});

Template.milestoneItem.helpers({
  formatMilestoneDate(date) {
    return date && moment(date).format('L');
  },
  isStartDate() {
    if (this.milestone.startDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment(start).isBefore(); // True
    }
    return false;
  },
  isNotStartDate() {
    if (this.milestone.startDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment().isBefore(start); // True
    }
    return false;
  },
  isEndDate() {
    if (this.milestone.endDate) {
      const end = moment(this.milestone.endDate).toDate();
      return moment(end).isBefore(); // True
    }
    return false;
  },
  isNotEndDate() {
    if (this.milestone.endDate) {
      const end = moment(this.milestone.endDate).toDate();
      return moment().isBefore(end); // True
    }
    return false;
  },
  isInProgress() {
    if (this.milestone.startDate && this.milestone.endDate) {
      const start = moment(this.milestone.startDate).toDate();
      const end = moment(this.milestone.endDate).toDate();
      return moment().isBefore(end) && moment(start).isBefore(); // True
    } if (this.milestone.startDate && !this.milestone.endDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment(start).isBefore(); // True
    }
    return false;
  },
  badgeStatus(status) {
    return status && status === 'open' ? 'balanced' : 'assertive';
  },
});

Template.milestoneItem.events({
  'click .action-project-milestone-js'(event) {
    event.preventDefault();
    const self = this;
    if (self.milestone.status === 'open') {
      IonActionSheet.show({
        titleText: i18n.__('Milestones'),
        buttons: [
          { text: `${i18n.__('Create an action on this milestone')}` },
          { text: `${i18n.__('Assign to all actions without milestone')}` },
          { text: `${i18n.__('Assign to actions without milestone')}` },
          { text: `${i18n.__('Move unfinished actions to another milestone')}` },
        ],
        destructiveText: `${i18n.__('Close milestone')} <i class="icon ion-close"></i>`,
        destructiveButtonClicked() {
          // console.log('Edit!');
          IonPopup.confirm({
            title: i18n.__('Close'),
            template: i18n.__('Close the milestone of this project'),
            onOk() {
              Meteor.call('milestoneStatus', { scopeId: self.scope._id.valueOf(), milestoneId: self.milestone.milestoneId, status: 'close' }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                  });
                } else {
                  IonToast.show({
                    template: i18n.__('Close milestone'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('this milestone has been closed')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
          return true;
        },
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            pageSessionAction.set('milestoneId', self.milestone.milestoneId);
            const parentDataContext = {
              orgaCibleId: Session.get('orgaCibleId'), _id: pageSession.get('scopeId'), scope: 'projects',
            };
            const isAdmin = !!Session.get(`isAdminOrga${Session.get('orgaCibleId')}`);
            if (isAdmin) {
              IonModal.open('actionsAddModal', parentDataContext);
            }

            // Router.go('actionsAdd', { _id: self.scope._id.valueOf(), scope: 'projects' });
          }
          if (index === 1) {
            Meteor.call('milestoneAssignAllActions', { scopeId: self.scope._id.valueOf(), milestoneId: self.milestone.milestoneId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('Milestone assignment'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('all actions without a milestone have been assigned to this milestone')}`,
                });
              }
            });
          }
          if (index === 2) {
            const parentDataContext = { _id: self.scope._id.valueOf(), scope: 'projects', milestoneId: self.milestone.milestoneId };
            IonModal.open('milestoneAssignActionsModal', parentDataContext);
          }
          if (index === 3) {
            const parentDataContext = {
              _id: self.scope._id.valueOf(), scope: 'projects', milestoneId: self.milestone.milestoneId, milestoneName: self.milestone.name,
            };
            IonModal.open('milestoneMoveActionsModal', parentDataContext);
          }
          return true;
        },
      });
    } else if (self.milestone.status === 'close') {
      IonActionSheet.show({
        titleText: i18n.__('Milestones'),
        buttons: [
          { text: `${i18n.__('Reopen the milestone')}` },
        ],
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            Meteor.call('milestoneStatus', { scopeId: self.scope._id.valueOf(), milestoneId: self.milestone.milestoneId, status: 'open' }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('Reopen the milestone'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('this milestone has been reopened')}`,
                });
              }
            });
          }
          return true;
        },
      });
    }
  },
  'click .move-jalon-action-js'(event) {
    event.preventDefault();
    const self = this;
    Meteor.call('milestoneMoveActions', { scopeId: self.scope._id.valueOf(), oldMilestoneId: self.oldMilestoneId, milestoneId: self.milestone.milestoneId }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Move unfinished actions to another milestone'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('the unfinished movement of actions has been made')}`,
        });
      }
    });
  },
});

Template.scopeActionsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });

  this.autorun(function () {
    if (searchAction.get('search') || searchAction.get('searchSort')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    if (Router.current().params.scope === 'citoyens') {
      const handle = this.subscribe('all.user.actions', Router.current().params.scope, Router.current().params._id);
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    } else {
      const paramPub = { scope: Router.current().params.scope, scopeId: Router.current().params._id, etat: 'todo', limit: this.limit.get('limit') };
      if (searchAction.get('search')) {
        paramPub.search = searchAction.get('search');
      }
      if (searchAction.get('searchSort')) {
        paramPub.searchSort = searchAction.get('searchSort');
      }
      const handle = this.subscribe('directoryListActions', paramPub);
      Tracker.autorun(() => {
        this.ready.set(handle.ready());
      });
    }
  }.bind(this));
});

Template.scopeActionsTemplate.onRendered(function () {
  const self = this;
  const showMoreVisible = () => {
    const $target = $('#showMoreResults');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 10;
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visible')) {
        $target.data('visible', true);
        self.limit.set('limit', self.limit.get('limit') + self.limit.get('incremente'));
      }
    } else if ($target.data('visible')) {
      $target.data('visible', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.scopeActionsTemplate.helpers({
  scopeBoutonActionsTemplate() {
    return `boutonActions${Router.current().params.scope}`;
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  search() {
    return searchAction.get('search');
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  searchMaintenance() {
    return searchAction.get('searchMaintenance') || searchAction.get('searchMaintenanceMe');
  },
  searchMaintenanceMe() {
    return searchAction.get('searchMaintenanceMe');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  isLimit(countActions) {
    return countActions === Template.instance().limit.get('limit');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeActionsTemplate.events({
  'click .change-selectsubview-js'(event) {
    event.preventDefault();
    pageSession.set('selectsubview', event.currentTarget.id);
  },
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
  'click .search-all-js'(event) {
    event.preventDefault();
    searchAction.set('search', `all:${searchAction.get('search').replace('all:', '')}`);
  },
});


Template.scopeNotesTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  const template = Template.instance();
  const dataContext = Template.currentData();
  this.limit = new ReactiveDict();
  this.limit.set({
    limit: 100,
    incremente: 100,
  });

  this.autorun(function () {
    if (searchNotes.get('search') || searchNotes.get('searchSort')) {
      this.limit.set({
        limit: 100,
        incremente: 100,
      });
    }
  }.bind(this));

  this.autorun(function () {
    if (dataContext && dataContext.scope && dataContext._id) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;

      if (dataContext.orgaCibleId) {
        template.orgaCibleId = dataContext.orgaCibleId;
        Session.setPersistent('orgaCibleId', dataContext.orgaCibleId);
      } else if (template.scope === 'organizations') {
        template.orgaCibleId = template._id;
        Session.setPersistent('orgaCibleId', template._id);
      }
      template.dataContext = true;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;

      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
      }
      template.dataContext = false;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
  });

  this.autorun(function () {
    const paramPub = { scope: pageSession.get('scope'), scopeId: pageSession.get('scopeId'), limit: this.limit.get('limit') };
    if (searchNotes.get('search')) {
      paramPub.search = searchNotes.get('search');
    }
    if (searchNotes.get('searchSort')) {
      paramPub.searchSort = searchNotes.get('searchSort');
    }
    const handle = this.subscribe('directoryListNotes', paramPub);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
    // }
  }.bind(this));
});

Template.scopeNotesTemplate.onRendered(function () {
  const self = this;
  const showMoreVisible = () => {
    const $target = $('#showMoreResults');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 10;
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visible')) {
        $target.data('visible', true);
        self.limit.set('limit', self.limit.get('limit') + self.limit.get('incremente'));
      }
    } else if ($target.data('visible')) {
      $target.data('visible', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.scopeNotesTemplate.helpers({
  scopeBoutonNotesTemplate() {
    return `boutonNotes${Router.current().params.scope}`;
  },
  search() {
    return searchNotes.get('search');
  },
  searchSort() {
    return searchNotes.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  isLimit(countActions) {
    return countActions === Template.instance().limit.get('limit');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeNotesTemplate.events({
  'click .give-me-more'(event, instance) {
    event.preventDefault();
    const newLimit = instance.limit.get('limit') + instance.limit.get('incremente');
    instance.limit.set('limit', newLimit);
  },
});

// Template.scopeNotesTemplateModal.inheritsHelpersFrom('scopeNotesTemplate');
Template.scopeNotesTemplateModal.inheritsEventsFrom('scopeNotesTemplate');
Template.scopeNotesTemplateModal.inheritsHooksFrom('scopeNotesTemplate');

Template.scopeNotesTemplateModal.helpers({
  search() {
    return searchNotes.get('search');
  },
  searchSort() {
    return searchNotes.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  isLimit(countActions) {
    return countActions === Template.instance().limit.get('limit');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
  listNotesCreator(search, searchSort) {
    if (Template.instance().scope) {
      const collection = nameToCollection(Template.instance().scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      if (scopeOne) {
        scopeOne.dataContext = Template.instance().dataContext;
        scopeOne.orgaCibleId = Template.instance().orgaCibleId;
      }
      return scopeOne.listNotesCreator(search, searchSort);
    }
    // return undefined;
  },
  listNotesCreatorParentInfo(search, searchSort) {
    if (Template.instance().scope) {
      const collection = nameToCollection(Template.instance().scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      if (scopeOne) {
        scopeOne.dataContext = Template.instance().dataContext;
        scopeOne.orgaCibleId = Template.instance().orgaCibleId;
      }
      return scopeOne.listNotesCreatorParentInfo(search, searchSort);
    }
  },
  countNotesCreator(search) {
    if (Template.instance().scope) {
      const collection = nameToCollection(Template.instance().scope);
      const scopeOne = collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
      if (scopeOne) {
        scopeOne.dataContext = Template.instance().dataContext;
        scopeOne.orgaCibleId = Template.instance().orgaCibleId;
      }
      return scopeOne.listNotesCreator(search) && scopeOne.listNotesCreator(search).count();
    }
  },
});

Template.searchNotes.onCreated(function () {

});

Template.searchNotes.onRendered(function () {
  // const wrap = $('.view .content.overflow-scroll');
  // const search = $('.view .content.overflow-scroll #search');
  // if (wrap.length > 0 && search.length > 0) {
  //   wrap.on('scroll', function () {
  //     if (!Platform.isIOS()) {
  //       if (this.scrollTop > 147) {
  //         wrap.addClass('fix-search');
  //       }
  //       if (this.scrollTop < 82) {
  //         wrap.removeClass('fix-search');
  //       }
  //     }
  //   });
  // } else {
  //   const wrapModal = $('.modal .content.overflow-scroll');
  //   wrapModal.scrollTop(0);
  //   const searchModal = $('.modal .content.overflow-scroll #search');
  //   if (wrapModal.length > 0 && searchModal.length > 0) {
  //     wrapModal.on('scroll', function () {
  //       if (!Platform.isIOS()) {
  //         if (this.scrollTop > 147) {
  //           wrapModal.addClass('fix-search');
  //         }
  //         if (this.scrollTop < 82) {
  //           wrapModal.removeClass('fix-search');
  //         }
  //       }
  //     });
  //   }
  // }
});

Template.searchNotes.helpers({
  search() {
    return searchNotes.get('search');
  },
  // searchTag() {
  //   return searchNotes.get('searchTag');
  // },
  searchHelp() {
    return searchNotes.get('searchHelp');
  },
  sortActived() {
    if (searchNotes.get('searchSort')) {
      return searchQuerySortActived(searchNotes.get('searchSort'));
    }
    return false;
  },
  // allTags() {
  //   const orgaOne = Organizations.findOne(new Mongo.ObjectID(Session.get('orgaCibleId')));
  //   if (!orgaOne) {
  //     return null;
  //   }
  //   const searchTag = searchNotes.get('searchTag');
  //   const arrayAll = orgaOne.actionsAll().fetch().flatMap((action) => action.tags || []);
  //   const arrayAllMerge = [...new Set(arrayAll)];

  //   if (searchTag?.length > 1) {
  //     return arrayAllMerge.filter((item) => item.includes(searchTag.substr(1)));
  //   } else {
  //     return arrayAllMerge.sort();
  //   }
  // },
});

Template.searchNotes.events({
  'click .searchfilter-js'(event) {
    event.preventDefault();
    const filter = $(event.currentTarget).data('action');
    if (filter) {
      searchNotes.set('search', filter);
      searchNotes.set('searchHelp', null);
    } else {
      searchNotes.set('search', null);
    }
  },
  'click .filter-help-js'(event) {
    event.preventDefault();
    if (searchNotes.get('search') !== '?') {
      searchNotes.setPersistent('search', '?');
      searchNotes.setPersistent('searchHelp', true);
    } else {
      searchNotes.setPersistent('search', null);
      searchNotes.setPersistent('searchHelp', null);
    }
  },
  // 'click .searchtag-js'(event) {
  //   event.preventDefault();
  //   if (this) {
  //     searchNotes.setPersistent('search', `#${this}`);
  //   } else {
  //     searchNotes.setPersistent('search', null);
  //   }
  // },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {

      // if (event.currentTarget.value.charAt(0) === '#') {
      //   searchNotes.setPersistent('searchTag', event.currentTarget.value);
      // } else {
      //   searchNotes.setPersistent('searchTag', null);
      // }

      if (event.currentTarget.value.length === 1 && event.currentTarget.value.charAt(0) === '?') {
        searchNotes.setPersistent('searchHelp', true);
      } else {
        searchNotes.setPersistent('searchHelp', null);
      }

      if (event.currentTarget.value.charAt(0) === '*') {
        searchNotes.setPersistent('searchTracking', event.currentTarget.value);
      } else {
        searchNotes.setPersistent('searchTracking', null);
      }

      searchNotes.setPersistent('search', event.currentTarget.value);
    } else {
      searchNotes.setPersistent('search', null);
      searchNotes.setPersistent('searchHelp', null);
      // searchNotes.setPersistent('searchTag', null);
    }
  }, 500),
});

Template.searchNotesSort.helpers({
  searchSort() {
    if (searchNotes.get('searchSort')) {
      if (searchNotes.get('searchSort').notes[2].field === 'tracking') {
        const sortDefaultChange = searchNotes.get('searchSort');
        sortDefaultChange.notes[2].field = `tracking.${Meteor.userId()}`;
        searchNotes.setPersistent('searchSort', sortDefaultChange);
      }
      return searchNotes.get('searchSort');
    }
    // Todo : translate label
    const sortDefault = {
      notes: [
        {
          label: i18n.__('sortModal.activity_date'), type: 'notes', field: 'updatedAt', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.creation_date'), type: 'notes', field: 'createdAt', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.tracking'), type: 'notes', field: `tracking.${Meteor.userId()}`, existField: true, checked: false, fieldDesc: false,
        },
      ],
    };
    searchNotes.setPersistent('searchSort', sortDefault);
    return searchNotes.get('searchSort');
  },
  orderType(type) {
    if (type) {
      const sort = searchNotes.get('searchSort');
      return sort && sort[type] ? [...sort[type]].filter((item) => item.checked === true).sort(compareValues('order')) : [];
    }
  },
});

Template.searchNotesSortItemToggle.events({
  'click .sort-checked-js'(event) {
    const self = this;
    if (this.type) {
      const sort = searchNotes.get('searchSort');
      const arrayOrder = sort[this.type].filter((item) => item.checked === true);
      const countOrder = arrayOrder && arrayOrder.length > 0 ? arrayOrder.length : 0;
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.checked = event.currentTarget.checked;
          if (event.currentTarget.checked) {
            item.order = countOrder + 1;
          } else {
            delete item.order;
          }
          return item;
        }
        return item;
      });
      searchNotes.setPersistent('searchSort', sort);
    }
  },
  'click .sort-desc-js'(event) {
    const self = this;
    if (this.type) {
      const sort = searchNotes.get('searchSort');
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.fieldDesc = event.currentTarget.checked;
          return item;
        }
        return item;
      });
      searchNotes.setPersistent('searchSort', sort);
    }
  },
});

Template.itemInputNote.events({
  'click .tracking-js'(event) {
    event.preventDefault();
    const tracking = this.note?.tracking?.[Meteor.userId()] !== true;

    const options = { docId: this.note._id.valueOf(), tracking };

    if (this.viewScope === 'citoyens') {
      if (this.note.organizationId) {
        options.organizationId = this.note.organizationId;
      }
    } else if (this.viewScope === 'projects') {
      options.organizationId = Session.get('orgaCibleId');
    } else if (this.viewScope === 'organizations') {
      options.organizationId = Session.get('orgaCibleId');
    }

    Meteor.call('trackingNote', options, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Tracking'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__(tracking ? 'tracking this note' : 'untracking this note')}`,
        });
      }
    });
  },
  'click .filter-parentId-js'(event) {
    event.preventDefault();
    searchNotes.set('search', `parentId:${this.note.parentId}`);
  }
});

Template.itemInputNote.helpers({
  isTracking() {
    return this.note?.tracking?.[Meteor.userId()] === true;
  },
});

Template.scopeAssessTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    const handle = this.subscribe('directoryListAnswers', Router.current().params.scope, Router.current().params._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeAssessTemplate.helpers({
  scopeBoutonAssessTemplate() {
    return `boutonAssess${Router.current().params.scope}`;
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeAnswersTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    const handle = this.subscribe('directoryListAnswers', Router.current().params.scope, Router.current().params._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeAnswersTemplate.helpers({
  scopeBoutonAnswersTemplate() {
    return `boutonAnswers${Router.current().params.scope}`;
  },
  search() {
    return pageSession.get('search');
  },
  searchSort() {
    return pageSession.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.searchAnswers.onRendered(function () {
  const wrap = $('.view .content.overflow-scroll');
  const search = $('.view .content.overflow-scroll #search');
  if (wrap.length > 0 && search.length > 0) {
    wrap.on('scroll', function () {
      if (!Platform.isIOS()) {
        if (this.scrollTop > 147) {
          wrap.addClass('fix-search');
        }
        if (this.scrollTop < 82) {
          wrap.removeClass('fix-search');
        }
      }
    });
  } else {
    const wrapModal = $('.modal .content.overflow-scroll');
    wrapModal.scrollTop(0);
    const searchModal = $('.modal .content.overflow-scroll #search');
    if (wrapModal.length > 0 && searchModal.length > 0) {
      wrapModal.on('scroll', function () {
        if (!Platform.isIOS()) {
          if (this.scrollTop > 147) {
            wrapModal.addClass('fix-search');
          }
          if (this.scrollTop < 82) {
            wrapModal.removeClass('fix-search');
          }
        }
      });
    }
  }
});

Template.searchAnswers.helpers({
  search() {
    return pageSession.get('search');
  },
  searchHelp() {
    return pageSession.get('searchHelp');
  },
  searchTag() {
    return pageSession.get('searchTag');
  },
  searchStatus() {
    return pageSession.get('searchStatus');
  },
  searchAcceptation() {
    return pageSession.get('searchAcceptation');
  },
  searchPriority() {
    return pageSession.get('searchPriority');
  },
  sortActived() {
    if (pageSession.get('searchSort')) {
      return searchQuerySortActived(pageSession.get('searchSort'));
    }
    return false;
  },
});

Template.scopeItemListAnswer.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});

Template.scopeItemListAnswer.events({
  'click .searchtag-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', `#${this}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchstatus-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', `!${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchacceptation-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', `>${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchpriority-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('search', `~${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
});

Template.searchAnswers.events({
  'click .searchstatus-js'(event) {
    event.preventDefault();
    pageSession.set('searchHelp', null);
    pageSession.set('searchTag', null);
    pageSession.set('searchStatus', null);
    pageSession.set('searchAcceptation', null);
    pageSession.set('searchPriority', null);
    if (this) {
      pageSession.set('search', `!${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchacceptation-js'(event) {
    event.preventDefault();
    pageSession.set('searchHelp', null);
    pageSession.set('searchTag', null);
    pageSession.set('searchStatus', null);
    pageSession.set('searchAcceptation', null);
    pageSession.set('searchPriority', null);
    if (this) {
      pageSession.set('search', `>${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchpriority-js'(event) {
    pageSession.set('searchHelp', null);
    pageSession.set('searchTag', null);
    pageSession.set('searchStatus', null);
    pageSession.set('searchAcceptation', null);
    pageSession.set('searchPriority', null);
    event.preventDefault();
    if (this) {
      pageSession.set('search', `~${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchfilter-js'(event) {
    event.preventDefault();
    const filter = $(event.currentTarget).data('action');
    if (filter) {
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', filter);
      pageSession.set('searchHelp', null);
    } else {
      pageSession.set('search', null);
    }
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);

      if (event.currentTarget.value.charAt(0) === '#') {
        pageSession.set('searchTag', event.currentTarget.value);
      } else {
        pageSession.set('searchTag', null);
      }

      if (event.currentTarget.value.charAt(0) === '!') {
        pageSession.set('searchStatus', event.currentTarget.value);
      } else {
        pageSession.set('searchStatus', null);
      }

      if (event.currentTarget.value.charAt(0) === '>') {
        pageSession.set('searchAcceptation', event.currentTarget.value);
      } else {
        pageSession.set('searchAcceptation', null);
      }

      if (event.currentTarget.value.charAt(0) === '~') {
        pageSession.set('searchPriority', event.currentTarget.value);
      } else {
        pageSession.set('searchPriority', null);
      }

      if (event.currentTarget.value.length === 1 && event.currentTarget.value.charAt(0) === '?') {
        pageSession.set('searchHelp', true);
      } else {
        pageSession.set('searchHelp', null);
      }

      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('searchHelp', null);
      pageSession.set('search', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
    }
  }, 500),
});

Template.searchAnswersSort.helpers({
  searchSort() {
    if (pageSession.get('searchSort')) {
      return pageSession.get('searchSort');
    }
    // Todo : translate label
    const sortDefault = {
      answers: [
        {
          label: i18n.__('sortModal.activity_date'), type: 'answers', field: 'updated', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.creation_date'), type: 'answers', field: 'created', existField: true, checked: false, fieldDesc: false,
        },
      ],
    };
    pageSession.set('searchSort', sortDefault);
    return pageSession.get('searchSort');
  },
  orderType(type) {
    if (type) {
      const sort = pageSession.get('searchSort');
      return sort && sort[type] ? [...sort[type]].filter((item) => item.checked === true).sort(compareValues('order')) : [];
    }
  },
});

Template.searchAnswersSortItemToggle.events({
  'click .sort-checked-js'(event) {
    const self = this;
    if (this.type) {
      const sort = pageSession.get('searchSort');
      const arrayOrder = sort[this.type].filter((item) => item.checked === true);
      const countOrder = arrayOrder && arrayOrder.length > 0 ? arrayOrder.length : 0;
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.checked = event.currentTarget.checked;
          if (event.currentTarget.checked) {
            item.order = countOrder + 1;
          } else {
            delete item.order;
          }
          return item;
        }
        return item;
      });
      pageSession.set('searchSort', sort);
    }
  },
  'click .sort-desc-js'(event) {
    const self = this;
    if (this.type) {
      const sort = pageSession.get('searchSort');
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.fieldDesc = event.currentTarget.checked;
          return item;
        }
        return item;
      });
      pageSession.set('searchSort', sort);
    }
  },
});

Template.scopeOrganizationsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = this.subscribe('directoryListOrganizations', Router.current().params.scope, Router.current().params._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeOrganizationsTemplate.helpers({
  scopeBoutonProjectsTemplate() {
    return `boutonOrganizations${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeOrganizationsTemplate.events({
  'click .copie-config-oceco-js'(event) {
    event.preventDefault();
    Meteor.call('copieConfigOceco', { scopeId: this._id.valueOf(), scope: 'organizations' }, function (error, result) {
      if (result) {
        IonToast.show({
          template: i18n.__('The oceco config of the parent organization has been copied to the sub-organization'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('config oceco')}`,
        });
      }
    });
  },
  'click .action-config-oceco-js'(event) {
    event.preventDefault();
    // info,description,contact
    const self = this;
    if (self.oceco) {
      IonActionSheet.show({
        titleText: i18n.__('Config oceco sub-organization'),
        buttons: [
          { text: `${i18n.__('invite members or admin of the parent organization')}` },
        ],
        destructiveText: `${i18n.__('Delete the oceco config')} <i class="icon ion-trash-a"></i>`,
        destructiveButtonClicked() {
          // console.log('Edit!');
          IonPopup.confirm({
            title: i18n.__('delete'),
            template: i18n.__('Delete the oceco config from this sub-organization'),
            onOk() {
              Meteor.call('deleteConfigOceco', { scopeId: self._id.valueOf(), scope: 'organizations' }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                  });
                } else {
                  IonToast.show({
                    template: i18n.__('deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Config oceco sub- organization')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
          return true;
        },
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            Router.go('invitations', { _id: self._id.valueOf(), scope: 'organizations' });
          }
          if (index === 1) {
            // console.log('Edit!');

          }
          if (index === 2) {
            // console.log('Edit!');

          }

          return true;
        },
      });
    } else {
      IonActionSheet.show({
        titleText: i18n.__('Config oceco sub-organization'),
        buttons: [
          { text: `${i18n.__('copy the mother oceco config')}` },
        ],
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            // console.log('Edit!');
            Meteor.call('copieConfigOceco', { scopeId: self._id.valueOf(), scope: 'organizations' }, function (error, result) {
              if (result) {
                IonToast.show({
                  template: i18n.__('The oceco config of the parent organization has been copied to the sub-organizationa'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Config oceco sub-organization')}`,
                });
              }
            });
          }
          return true;
        },
      });
    }
  },
});

Template.scopeEventsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('directoryListEvents', Router.current().params.scope, Router.current().params._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeEventsTemplate.helpers({
  scopeBoutonEventsTemplate() {
    return `boutonEvents${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeFormsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('directoryListForms', Router.current().params.scope, Router.current().params._id);
    // info : test si form = 1
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    Tracker.autorun(() => {
      if (handle.ready()) {
        if (orgaOne.countForms() === 1) {
          const listForms = orgaOne.listForms().fetch();
          Router.go('answersListOrga', { orgaCibleId: Router.current().params._id, _id: listForms[0]._id.valueOf(), scope: 'forms' }, { replaceState: true });
        } else {
          this.ready.set(handle.ready());
        }
      }
    });

  }.bind(this));
});

Template.scopeFormsTemplate.helpers({
  scopeBoutonFormsTemplate() {
    return `boutonForms${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeProjectsEventsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('directoryProjectsListEvents', Router.current().params.scope, Router.current().params._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeProjectsEventsTemplate.helpers({
  scopeBoutonEventsTemplate() {
    return `boutonEvents${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeRoomsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('directoryListRooms', Router.current().params.scope, Router.current().params._id);
    Tracker.autorun(() => {
      this.ready.set(handle.ready());
    });
  }.bind(this));
});

Template.scopeRoomsTemplate.helpers({
  scopeBoutonEventsTemplate() {
    return `boutonRooms${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.addMenuScopePopover.helpers({
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
});

Template.scopeTemplate.events({
  'click .selectview'(event) {
    event.preventDefault();
    pageSession.set('selectview', event.currentTarget.id);
  },
  'click .photo-link-scope'(event, instance) {
    event.preventDefault();
    // const self = this;
    // const scope = pageSession.get('scope');
    // if (Meteor.isCordova) {
    //   const options = {
    //     width: 640,
    //     height: 480,
    //     quality: 75,
    //   };
    //   MeteorCameraUI.getPicture(options, function (error, data) {
    //     if (!error) {
    //       const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
    //       Meteor.call('photoScope', scope, data, str, self._id.valueOf(), function (errorPhoto) {
    //         if (!errorPhoto) {
    //           // console.log(result);
    //         } else {
    //           // console.log('error', error);
    //         }
    //       });
    //     }
    //   });
    // } else {
    instance.$('#file-upload').trigger('click');
    // }
  },
  'change #file-upload'(event, instance) {
    event.preventDefault();
    const self = this;
    const scope = pageSession.get('scope');
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      _.each(instance.find('#file-upload').files, function (file) {
        if (file.size > 1) {
          const reader = new FileReader();
          reader.onload = function () {
            // console.log(file.name);
            // console.log(file.type);
            // console.log(reader.result);
            // let str = +new Date + Math.floor((Math.random() * 100) + 1) + ".jpg";
            const str = file.name;
            const dataURI = reader.result;
            Meteor.call('photoScope', scope, dataURI, str, self._id.valueOf(), function (error) {
              if (!error) {
                // console.log(result);
              } else {
                // console.log('error',error);
              }
            });
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
});

Template._inviteattendeesEvent.onCreated(function () {
  pageSession.set('error', false);
  pageSession.set('invitedUserEmail', false);
});

Template._inviteattendeesEvent.onRendered(function () {
  pageSession.set('error', false);
  pageSession.set('invitedUserEmail', false);
});

Template._inviteattendeesEvent.helpers({
  error() {
    return pageSession.get('error');
  },
});

AutoForm.addHooks(['inviteAttendeesEvent'], {
  before: {
    method(doc) {
      const scopeId = pageSession.get('scopeId');
      doc.eventId = scopeId;
      pageSession.set('invitedUserEmail', doc.invitedUserEmail);
      return doc;
    },
  },
  after: {
    method(error) {
      if (!error) {
        IonModal.close();
      }
    },
  },
  onError(formType, error) {
    // console.log(error);
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        if (error.reason === "Problème à l'insertion du nouvel utilisateur : une personne avec cet mail existe déjà sur la plateforme") {
          Meteor.call('saveattendeesEvent', pageSession.get('scopeId'), pageSession.get('invitedUserEmail'), function (errorSave) {
            if (errorSave) {
              pageSession.set('error', error.reason.replace(':', ' '));
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
              });
            } else {
              IonModal.close();
            }
          });
        }
      } else {
        pageSession.set('error', error.reason.replace(':', ' '));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
        });
      }
    }
  },
});

// eslint-disable-next-line meteor/template-names, no-underscore-dangle
Template._reportflag.onCreated(function () {
  pageSession.set('error', false);
  pageSession.set('report', Template.currentData());
});

// eslint-disable-next-line meteor/template-names, no-underscore-dangle
Template._reportflag.onRendered(function () {
  pageSession.set('error', false);
});

// eslint-disable-next-line no-underscore-dangle, meteor/template-names
Template._reportflag.helpers({
  error() {
    return pageSession.get('error');
  },
});

AutoForm.addHooks(['reportFlag'], {
  before: {
    method(doc) {
      // doc.scopeId = pageSession.get('report').scopeId;
      // doc.scope = pageSession.get('report').scope;
      doc.cibleScope = pageSession.get('report').cibleScope;
      doc.cibleId = pageSession.get('report').cibleId;
      return doc;
    },
  },
  after: {
    method(error) {
      if (!error) {
        IonModal.close();
        IonToast.show({
          template: i18n.__('report made'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon fa fa-flag"></i> ${i18n.__('Report abuse')}`,
        });
      }
    },
  },
  onError(formType, error) {
    // console.log(error);
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        // something somewhere went terribly wrong
        pageSession.set('error', error.reason.replace(':', ' '));
      } else {
        pageSession.set('error', error.reason.replace(':', ' '));
      }
    }
  },
});

AutoForm.addHooks(['deleteMyAccount'], {
  after: {
    method(error) {
      if (!error) {
        IonModal.close();
        Meteor.logout();
        Router.go('home');
        IonToast.show({
          template: i18n.__('Your account has been deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon fa fa-flag"></i> ${i18n.__('Your account has been deleted')}`,
        });
      }
    },
  },
  onError(formType, error) {
    // console.log(error);
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        // something somewhere went terribly wrong
        pageSession.set('error', error.reason.replace(':', ' '));
      } else {
        pageSession.set('error', error.reason.replace(':', ' '));
      }
    }
  },
});