import mermaid from 'mermaid';

export const mermaidMardown = (md) => {

  const defaultRenderer = md.renderer.rules.fence.bind(md.renderer.rules);

  const initMermaidValid = async function (code) {
    try {
      await mermaid.parse(code);
      mermaid.initialize({ startOnLoad: false, securityLevel: 'sandbox' });
      await mermaid.run()
    } catch (error) {
      throw new Error(`Error rendering mermaid diagram: ${error.message}`);
    }
  };

  md.renderer.rules.fence = (tokens, idx, options, env, slf) => {
    const token = tokens[idx];
    const code = token.content.trim();
    const info = token.info ? md.utils.unescapeAll(token.info).trim() : '';
    const langName = info ? info.split(/\s+/g)[0] : ''
    if (langName === 'mermaid') {
      const content = code.endsWith('```') ? code.slice(0, -3) : code;
      if (content === '') {
        return `<pre class="mermaid">\n</pre>\n`;
      }
      initMermaidValid(content);
      return `<pre class="mermaid">\n${content}\n</pre>\n`;
    }

    return defaultRenderer(tokens, idx, options, env, slf);
  }
}