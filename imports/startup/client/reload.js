/* eslint-disable meteor/no-session */
/* global Session IonPopup device */
import { Meteor } from 'meteor/meteor';
import { Reloader } from 'meteor/quave:reloader';

import { methodCall } from '../../infra/methodCall.js';
import { version } from '../../api/version.js';
import { pageSession } from '../../api/client/reactive.js';

Meteor.startup(async () => {
  Session.set('updateStore', false);
  if (Meteor.isCordova) {
    try {
      const appUpdateData = (await methodCall('getAppUpdateData', { clientVersion: version })) || {};
      pageSession.set('appUpdateData', appUpdateData);
      if (appUpdateData.version !== version && appUpdateData.forceUpdate && !appUpdateData.ignore) {
        Session.set('updateStore', true);
        if (window.plugins.updatePlugin && appUpdateData.updatePlugin) {
          window.plugins.updatePlugin.update(() => {
            // success callback
            console.info({
              message: 'success updatePlugin',
            });
          }, () => {
            // error callback
            console.info({
              message: 'error updatePlugin',
            });
          }, appUpdateData.updatePlugin);
        }
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.info({
        message: 'forcing app reload because getAppUpdateData is breaking',
      });
    }
  }
});

if (Meteor.isDevelopment) {
  // eslint-disable-next-line no-console
  console.log('reload dev');
  Reloader.initialize({
    beforeReload(ok, nok) {
      if (device.platform === 'Android') {
        IonPopup.alert({
          template: 'Fermez et réouvrez l\'application pour obtenir la nouvelle version!',
        });
      }
      nok();
    },
  });
} else if (Meteor.isCordova) {
  Reloader.initialize({
    beforeReload(ok, nok) {
      if (device.platform === 'Android') {
        IonPopup.alert({
          template: 'Fermez et réouvrez l\'application pour obtenir la nouvelle version!',
        });
      }
      nok();
    },
  });
}
