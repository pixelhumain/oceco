import { Parser } from 'htmlparser2';
import { DomHandler, isComment, isTag, hasChildren } from 'domhandler';
import { render } from 'dom-serializer';

const revealCommandSyntax = /^\s*\.(\w*):(.*)$/g
const dataAttributesSyntax = /\s*(data-[\w-]*|class)=(?:"((?:[^"\\]|\\"|\\)*)"|'([^']*)')/g

const processCommentNode = (node) => {
  const regexResult = node.data.split(revealCommandSyntax)
  if (regexResult.length === 1) {
    return
  }

  const parentNode = findTargetElement(node, regexResult[1])
  if (!parentNode) {
    return
  }

  const matches = [...regexResult[2].matchAll(dataAttributesSyntax)]
  for (const dataAttribute of matches) {
    const attributeName = dataAttribute[1]
    const attributeValue = dataAttribute[2] ?? dataAttribute[3]
    if (attributeValue) {
      parentNode.attribs[attributeName] = attributeValue
    }
  }
}

const findTargetElement = (node, selector) => {
  if (selector === 'slide') {
    return findNearestAncestorSection(node)
  } else if (selector === 'element') {
    return findParentElement(node)
  } else {
    return null
  }
}

const findParentElement = (node) => {
  return node.parentNode !== null && isTag(node.parentNode) ? node.parentNode : null
}

const findNearestAncestorSection = (node) => {
  let currentNode = node.parentNode
  while (currentNode != null) {
    if (isTag(currentNode) && currentNode.tagName === 'section') {
      break
    }
    currentNode = currentNode.parentNode
  }
  return currentNode
}


// La fonction qui traverse et modifie le DOM
function traverseAndProcessNodes(nodes) {
  nodes.forEach((node) => {
    if (node.type === 'comment') {
      processCommentNode(node);
    } else if (node.children) {
      traverseAndProcessNodes(node.children);
    }
  });
}

// Le plugin markdown-it
function revealCommentCommandNodePreprocessor(md) {
  const defaultRender = md.renderer.render.bind(md.renderer);

  md.renderer.render = (tokens, options, env) => {
    const html = defaultRender(tokens, options, env);

    // Parse le HTML en DOM
    const handler = new DomHandler((error, dom) => {
      if (!error) {
        traverseAndProcessNodes(dom);
      }
    });

    const parser = new Parser(handler);
    parser.write(html);
    parser.done();

    // Convertit le DOM modifié de retour en HTML
    return render(handler.dom);
  };
}

// Exportation du plugin pour une utilisation dans d'autres parties de votre application Meteor
export { revealCommentCommandNodePreprocessor };