import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';


if (!Meteor.isCordova) {

  const iOS = () => {
    const iDevices = ['iPad Simulator', 'iPhone Simulator', 'iPod Simulator', 'iPad', 'iPhone', 'iPod'];
    return !!navigator.platform && iDevices.includes(navigator.platform);
  };

  const isEnvironmentSupported = () => 'Notification' in window && 'serviceWorker' in navigator && !iOS();

  if (isEnvironmentSupported) {

    const urlB64ToUint8Array = (base64String) => {
      const padding = '='.repeat((4 - base64String.length % 4) % 4);
      const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
      const rawData = window.atob(base64);
      const outputArray = new Uint8Array(rawData.length);

      for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
      }
      return outputArray;
    }

    const subscribeUserToPush = () => {
      navigator.serviceWorker.ready.then(function (registration) {
        return registration.pushManager.getSubscription()
          .then(function (subscription) {
            if (subscription) {
              // console.log('Utilisateur déjà abonné.');
              // Optionnellement, vérifie si cet abonnement est déjà connu du serveur
              return subscription; // Retourne null pour indiquer aucun nouvel abonnement créé
            }
            const subscribeOptions = {
              userVisibleOnly: true,
              applicationServerKey: urlB64ToUint8Array(Meteor.settings.public.vapidPublicKey)
            };
            return registration.pushManager.subscribe(subscribeOptions);
          })
      })
        .then(function (pushSubscription) {
          if (pushSubscription) {
            // console.log(pushSubscription);
            // console.log('Nouvel abonnement ou abonnement non enregistré côté serveur.');
            // Envoyer le nouvel abonnement au serveur pour le stocker
            Meteor.call('webPushsaveSubscription', pushSubscription.toJSON());
          }
        });
    }

    const unsubscribeUserFromPush = () => {
      navigator.serviceWorker.ready.then(function (registration) {
        registration.pushManager.getSubscription().then(function (subscription) {
          if (subscription) {
            // console.log('Désabonnement');
            subscription.unsubscribe().then(function (successful) {
              // console.log('Désabonnement réussi');
              Meteor.call('webPushUnsubscribe', subscription.toJSON());
            }).catch(function (e) {
              // console.error('Échec du désabonnement', e);
            });
          } else {
            // console.log('Aucun abonnement trouvé');
          }
        });
      });
    }


    Tracker.autorun(() => {
      // Vérifier si un utilisateur est connecté
      const userId = Meteor.userId();

      if (!iOS()) {
        const permission = Notification.permission;
        if (userId) {
          if (Meteor.settings?.public?.vapidPublicKey) {
            if (permission === 'granted') {
              // L'utilisateur a déjà accordé la permission, donc on souscrit directement
              subscribeUserToPush();
            } else if (permission === 'denied') {
              // console.log('Permission refusée pour les notifications');
              // La permission a été révoquée, nettoyer l'abonnement côté serveur
              unsubscribeUserFromPush(); // Fonction pour désinscrire l'utilisateur
            } else {
              // Demander la permission seulement si elle n'a pas été refusée
              Notification.requestPermission().then(function (result) {
                if (result === 'granted') {
                  // console.log('Permission accordée pour les notifications');
                  subscribeUserToPush();
                }
              });
            }
          }
        }
      }
    });

  }
}