import yaml from 'js-yaml';
import { Meteor } from 'meteor/meteor';

export const extraireEtParserYAML = (markdownContent) => {
  // Supposer que le bloc YAML est au début du fichier, encadré par ---
  const yamlRegex = /^---\n([\s\S]*?)\n---/;
  const match = markdownContent.match(yamlRegex);

  if (match) {
    const yamlContent = match[1];
    try {
      const parsedYaml = yaml.load(yamlContent);
      // Renvoie les métadonnées YAML et le contenu Markdown sans le bloc YAML
      return {
        metadata: parsedYaml,
        content: markdownContent.replace(yamlRegex, '') // Enlever le bloc YAML du contenu
      };
    } catch (e) {
      console.error('Erreur lors du parsing du YAML:', e);
      throw new Meteor.Error('Erreur lors du parsing du YAML:', e);
    }
  } else {
    // console.log("Aucun bloc YAML trouvé.");
    return {
      content: markdownContent // Renvoie le contenu original si aucun bloc YAML n'est trouvé
    };
  }
}