import 'meteor/aldeed:autoform/static';
import '../../api/simpleschema-messages.js';
import '../../api/helper/client';
import './client.js';
import './permisions.js';
import './locale.js';
import './reload.js';
import './position.js';
import './routes.js';
import './push.js';
import './autolinker.js';
import './mapbox.js';
import './notification.js';

