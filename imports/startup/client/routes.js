/* eslint-disable no-lonely-if */
/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Router } from 'meteor/iron:router';
import { Tracker } from 'meteor/tracker';
import { Mongo } from 'meteor/mongo';

import { Organizations } from '../../api/collection/organizations.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Projects } from '../../api/collection/projects.js';
import { Pomodoro } from '../../api/client/podomoro.js';
import { StatusTimes } from '../../api/helpers.js';

const setSessionVariables = () => {
  const orgaCibleId = Session.get('orgaCibleId');
  // const user = Meteor.user();
  const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(orgaCibleId) });
  const userC = Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });

  if (orgaOne && orgaOne.oceco) {
    Session.setPersistent('settingOceco', orgaOne.oceco);
  }

  if (orgaOne && orgaOne.isMembers()) {
    Session.setPersistent(`isMembreOrga${orgaCibleId}`, true);
  } else {
    if (Session.get(`isMembreOrga${orgaCibleId}`)) {
      Session.clear(`isMembreOrga${orgaCibleId}`);
    }
  }

  if (orgaOne && orgaOne.isAdmin()) {
    Session.setPersistent(`isAdmin${orgaCibleId}`, true);
    Session.setPersistent(`isAdminOrga${orgaCibleId}`, true);
  } else {
    if (orgaOne?.links?.projects && userC?.links?.projects) {
      const arrayIds = Object.keys(orgaOne.links.projects)
        .filter((k) => userC.links.projects?.[k]?.isAdmin && !userC.links.projects?.[k]?.toBeValidated && !userC.links.projects?.[k]?.isAdminPending && !userC.links.projects?.[k]?.isInviting)
        .map((k) => new Mongo.ObjectID(k));
      const countProject = Projects.find({ _id: { $in: arrayIds } }).count();
      const isAdmin = !!(countProject > 0);
      Session.setPersistent(`isAdmin${orgaCibleId}`, isAdmin);
      Session.setPersistent(`isAdminOrga${orgaCibleId}`, false);
    } else {
      Session.clear(`isAdmin${orgaCibleId}`);
      Session.clear(`isAdminOrga${orgaCibleId}`);
    }
  }

  Session.set('loadMemberOrga', true);
};

Tracker.autorun(() => {
  const userId = Meteor.userId();
  const orgaCibleId = Session.get('orgaCibleId');
  // console.log('tracker autorun router', userId, orgaCibleId);
  if (userId && orgaCibleId) {
    // console.log('tracker autorun testConnectAdmin');
    Meteor.call('testConnectAdmin', { id: orgaCibleId });
  }
});

const allSubscriptionsReady = new ReactiveVar(false);

Tracker.autorun(() => {
  const userId = Meteor.userId();
  const orgaCibleId = Session.get('orgaCibleId');
  if (userId && orgaCibleId) {
    // const handleScopeDetail = Meteor.subscribe('scopeDetail', 'organizations', orgaCibleId);
    const handleDirectoryListProjects = Meteor.subscribe('directoryListProjects', 'organizations', orgaCibleId);
    const handleCitoyen = Meteor.subscribe('citoyen');
    Tracker.autorun(() => {
      if (handleDirectoryListProjects.ready() && handleCitoyen.ready()) {
        allSubscriptionsReady.set(true);
      } else {
        allSubscriptionsReady.set(false);
      }
    });
  } else {
    allSubscriptionsReady.set(false);
  }
});

Tracker.autorun(() => {
  if (allSubscriptionsReady.get()) {
    // console.log('tracker autorun allSubscriptionsReady');
    Pomodoro.podomoroSessionSetDefault('pomodoroTimeWork', StatusTimes.work);
    Pomodoro.podomoroSessionSetDefault('pomodoroTimeShortRest', StatusTimes.short_rest);
    Pomodoro.podomoroSessionSetDefault('pomodoroTimeLongRest', StatusTimes.long_rest);
    setSessionVariables();
  }
});

Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  progress: false,
});

// eslint-disable-next-line array-callback-return
Router.map(function () {
  this.route('camera-page');

  this.route('login', {
    path: '/login',
  });

  this.route('signin', {
    path: '/signin',
  });

  this.route('passwordLost', {
    path: '/password-lost',
  });

  this.route('signOut', {
    path: '/sign-out',
    layoutTemplate: 'layout',
    onBeforeAction() {
      // console.log('onBeforeAction signOut');
      if (Meteor.userId()) {
        Meteor.logout();
        this.redirect('home');
      }
      this.next();
      // Router.go('/');
    },
  });

  this.route('switch', {
    path: '/switch',
    template: 'switch',
    loadingTemplate: 'loading',
  });

  Router.route('/switch/:_id', function () {
    const id = this.params._id;
    // Meteor.settings.public.orgaCibleId = id;
    Session.setPersistent('orgaCibleId', id);
    // console.log('route switch id');
    // Session.get('orgaCibleId');
    this.redirect('home');
  }, {
    name: 'switchRedirect',
  });

  this.route('listEvents', {
    path: '/eventsAgenda',
    template: 'listEvents',
    loadingTemplate: 'loading',
  });

  this.route('listEventsOrga', {
    path: '/organizations/:orgaCibleId/eventsAgenda',
    template: 'listEvents',
    loadingTemplate: 'loading',
  });

  this.route('listEventsScopeOrga', {
    path: '/organizations/:orgaCibleId/eventsAgenda/:scope/:_id',
    template: 'listEvents',
    loadingTemplate: 'loading',
  });

  this.route('listActionsAgenda', {
    path: '/actionsAgenda',
    template: 'listActionsAgenda',
    loadingTemplate: 'loading',
  });

  this.route('listActionsAgendaOrga', {
    path: '/organizations/:orgaCibleId/actionsAgenda',
    template: 'listActionsAgenda',
    loadingTemplate: 'loading',
  });

  this.route('listActionsAgendaScope', {
    path: '/actionsAgenda/:scope/:_id',
    template: 'listActionsAgenda',
    loadingTemplate: 'loading',
  });

  this.route('listActionsAgendaScopeOrga', {
    path: '/organizations/:orgaCibleId/actionsAgenda/:scope/:_id',
    template: 'listActionsAgenda',
    loadingTemplate: 'loading',
  });

  this.route('listOrganizations', {
    path: '/organizations',
    template: 'listOrganizations',
    loadingTemplate: 'loading',
  });

  this.route('listProjects', {
    path: '/projects',
    template: 'listProjects',
    loadingTemplate: 'loading',
  });

  this.route('organizationsAdd', {
    template: 'organizationsAdd',
    path: '/organizations/add',
    loadingTemplate: 'loading',
  });

  this.route('sousOrganizationsAdd', {
    template: 'sousOrganizationsAdd',
    path: 'organizations/:orgaCibleId/organizations/add',
    loadingTemplate: 'loading',
  });

  this.route('citoyensBlockEdit', {
    template: 'citoyensBlockEdit',
    path: '/citoyens/:_id/edit/block/:block',
    loadingTemplate: 'loading',
  });

  this.route('eventsBlockEdit', {
    template: 'eventsBlockEdit',
    path: '/events/:_id/edit/block/:block',
    loadingTemplate: 'loading',
  });

  this.route('organizationsBlockEdit', {
    template: 'organizationsBlockEdit',
    path: '/organizations/:_id/edit/block/:block',
    loadingTemplate: 'loading',
  });

  this.route('projectsBlockEdit', {
    template: 'projectsBlockEdit',
    path: '/projects/:_id/edit/block/:block',
    loadingTemplate: 'loading',
  });

  this.route('ocecoEdit', {
    template: 'ocecoEdit',
    path: '/oceco/:orgaCibleId/edit',
    loadingTemplate: 'loading',
  });

  this.route('ocecoCitoyenEdit', {
    template: 'ocecoCitoyenEdit',
    path: '/oceco-citoyen/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('ocecoCitoyenChangePassword', {
    template: 'ocecoCitoyenChangePassword',
    path: '/oceco-citoyen/:_id/change-password',
    loadingTemplate: 'loading',
  });

  this.route('ocecoProjectEdit', {
    template: 'ocecoProjectEdit',
    path: '/oceco-project/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('ocecoProjectGitEdit', {
    template: 'ocecoProjectGitEdit',
    path: '/oceco-git-project/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('citoyensEdit', {
    template: 'citoyensEdit',
    path: '/citoyens/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('organizationsEdit', {
    template: 'organizationsEdit',
    path: '/organizations/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('projectsAdd', {
    template: 'projectsAdd',
    path: '/:scope/projects/:_id/add',
    loadingTemplate: 'loading',
  });

  this.route('projectsEdit', {
    template: 'projectsEdit',
    path: '/projects/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('eventsAdd', {
    template: 'eventsAdd',
    path: '/:scope/events/:_id/add',
    loadingTemplate: 'loading',
  });

  this.route('eventsEdit', {
    template: 'eventsEdit',
    path: '/events/:_id/edit',
    loadingTemplate: 'loading',
  });

  this.route('detailListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/detail/:_id',
    loadingTemplate: 'loading',
  });

  this.route('detailList', {
    template: 'scopeTemplate',
    path: '/:scope/detail/:_id',
    loadingTemplate: 'loading',
  });

  this.route('notificationsListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/notifications/:_id',
    loadingTemplate: 'loading',
  });

  this.route('notificationsList', {
    template: 'scopeTemplate',
    path: '/:scope/notifications/:_id',
    loadingTemplate: 'loading',
  });

  this.route('organizationsList', {
    template: 'scopeTemplate',
    path: '/:scope/organizations/:_id',
    loadingTemplate: 'loading',
  });

  this.route('projectsListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/projects/:_id',
    loadingTemplate: 'loading',
  });

  this.route('projectsList', {
    template: 'scopeTemplate',
    path: '/:scope/projects/:_id',
    loadingTemplate: 'loading',
  });

  this.route('contributorsListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/contributors/:_id',
    loadingTemplate: 'loading',
  });

  this.route('contributorsList', {
    template: 'scopeTemplate',
    path: '/:scope/contributors/:_id',
    loadingTemplate: 'loading',
  });

  this.route('projectsMilestonesListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/milestones/:_id',
    loadingTemplate: 'loading',
  });

  this.route('projectsMilestonesList', {
    template: 'scopeTemplate',
    path: '/:scope/milestones/:_id',
    loadingTemplate: 'loading',
  });

  this.route('projectsMilestonesListActionsOrga', {
    template: 'projectsMilestonesListActions',
    path: 'organizations/:orgaCibleId/:scope/milestones/:_id/milestoneId/:milestoneId',
    loadingTemplate: 'loading',
  });

  this.route('projectsMilestonesListActions', {
    template: 'projectsMilestonesListActions',
    path: '/:scope/milestones/:_id/milestoneId/:milestoneId',
    loadingTemplate: 'loading',
  });

  this.route('eventsListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/events/:_id',
    loadingTemplate: 'loading',
  });

  this.route('eventsList', {
    template: 'scopeTemplate',
    path: '/:scope/events/:_id',
    loadingTemplate: 'loading',
  });

  this.route('formsListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/forms/:_id',
    loadingTemplate: 'loading',
  });

  this.route('formsList', {
    template: 'scopeTemplate',
    path: '/:scope/forms/:_id',
    loadingTemplate: 'loading',
  });

  this.route('actionsListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/actions/:_id',
    loadingTemplate: 'loading',
  });

  this.route('actionsList', {
    template: 'scopeTemplate',
    path: '/:scope/actions/:_id',
    loadingTemplate: 'loading',
  });

  this.route('notesListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/notes/:_id',
    loadingTemplate: 'loading',
  });

  this.route('notesList', {
    template: 'scopeTemplate',
    path: '/:scope/notes/:_id',
    loadingTemplate: 'loading',
  });

  this.route('notesEditorOrga', {
    template: 'editor',
    path: 'organizations/:orgaCibleId/:scope/notes/:_id/editorDoc/:docId?',
  });

  this.route('notesEditor', {
    template: 'editor',
    path: '/:scope/notes/:_id/editorDoc/:docId?',
  });

  this.route('notesSlideOrga', {
    template: 'notesSlide',
    path: 'organizations/:orgaCibleId/:scope/notes/:_id/slideDoc/:docId',
  });

  this.route('notesSlide', {
    template: 'notesSlide',
    path: '/:scope/notes/:_id/slideDoc/:docId',
  });

  this.route('answersListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/answers/:_id',
    loadingTemplate: 'loading',
  });

  this.route('assessListOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/assess/:_id',
    loadingTemplate: 'loading',
  });

  this.route('answersList', {
    template: 'scopeTemplate',
    path: '/:scope/answers/:_id',
    loadingTemplate: 'loading',
  });

  this.route('actionsListDepenseOrga', {
    template: 'scopeTemplate',
    path: 'organizations/:orgaCibleId/:scope/actionsDepense/:_id',
    loadingTemplate: 'loading',
  });

  this.route('actionsListDepense', {
    template: 'scopeTemplate',
    path: '/:scope/actionsDepense/:_id',
    loadingTemplate: 'loading',
  });

  this.route('actionsDetailOrga', {
    template: 'detailActions',
    path: 'organizations/:orgaCibleId/:scope/rooms/:_id/room/:roomId/action/:actionId',
    loadingTemplate: 'loading',
  });

  this.route('actionsDetail', {
    template: 'detailActions',
    path: '/:scope/rooms/:_id/room/:roomId/action/:actionId',
    loadingTemplate: 'loading',
  });

  this.route('actionsAdd', {
    template: 'actionsAdd',
    path: '/:scope/rooms/:_id/room/add/actionadd',
    loadingTemplate: 'loading',
  });

  this.route('actionsEdit', {
    template: 'actionsEdit',
    path: '/:scope/rooms/:_id/room/:roomId/actionedit/:actionId',
    loadingTemplate: 'loading',
  });

  this.route('actionsDetailCommentsOrga', {
    template: 'actionsDetailComments',
    path: 'organizations/:orgaCibleId/:scope/rooms/:_id/room/:roomId/action/:actionId/comments',
  });

  this.route('answersDetailCommentsOrga', {
    template: 'answersDetailComments',
    path: 'organizations/:orgaCibleId/:scope/answers/:_id/answer/:answerId/comments',
  });

  this.route('actionsDetailComments', {
    template: 'actionsDetailComments',
    path: '/:scope/rooms/:_id/room/:roomId/action/:actionId/comments',
  });

  this.route('commentsActionsEdit', {
    template: 'commentsActionsEdit',
    path: '/:scope/rooms/:_id/room/:roomId/action/:actionId/comments/:commentId/edit',
    loadingTemplate: 'loading',
  });

  this.route('commentsAnswersEditOrga', {
    template: 'commentsAnswersEdit',
    path: 'organizations/:orgaCibleId/:scope/answers/:_id/answer/:answerId/comments/:commentId/edit',
    loadingTemplate: 'loading',
  });

  this.route('actionsAssign', {
    template: 'assignMembers',
    path: '/:scope/rooms/:_id/room/:roomId/action/:actionId/assign',
  });

  this.route('actionsAssignTask', {
    template: 'assignMembersTask',
    path: '/:scope/rooms/:_id/room/:roomId/action/:actionId/task/:taskId/assign',
  });

  this.route('answersDetailOrga', {
    template: 'detailAnswers',
    path: 'organizations/:orgaCibleId/:scope/answers/:_id/answer/:answerId',
    loadingTemplate: 'loading',
  });

  this.route('answersAdd', {
    template: 'answersAdd',
    path: 'organizations/:orgaCibleId/:scope/forms/:_id/add/answeradd',
    loadingTemplate: 'loading',
  });

  this.route('answersEdit', {
    template: 'answersEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answeredit/:answerId',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseEstimatesAdd', {
    template: 'answersDepenseEstimatesAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepenseestimates/:answerId/depense/:depensekey/add',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseEstimatesEdit', {
    template: 'answersDepenseEstimatesEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepenseestimates/:answerId/depense/:depensekey/edit/:key',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseFinanceAdd', {
    template: 'answersDepenseFinanceAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepensefinances/:answerId/finance/:depensekey/add',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseFinanceEdit', {
    template: 'answersDepenseFinanceEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepensefinances/:answerId/finance/:depensekey/edit/:key',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseWorkerAdd', {
    template: 'answersDepenseWorkerAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepenseworker/:answerId/worker/:depensekey/add',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseWorkerEdit', {
    template: 'answersDepenseWorkerEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepenseworker/:answerId/worker/:depensekey/edit',
    loadingTemplate: 'loading',
  });

  this.route('answersDepensePayementAdd', {
    template: 'answersDepensePayementAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepensepayement/:answerId/payement/:depensekey/add',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseTaskAdd', {
    template: 'answersDepenseTaskAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepensetask/:answerId/task/:depensekey/action/:actionkey/add',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseTaskAddAction', {
    template: 'answersDepenseTaskAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/answersdepensetask/:answerId/task/action/:actionkey/add/redirect/:redirect',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseTaskEdit', {
    template: 'answersDepenseTaskEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepensetask/:answerId/task/:depensekey/edit/action/:actionkey/edit/:key',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseAdd', {
    template: 'answersDepenseAdd',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepense/:answerId',
    loadingTemplate: 'loading',
  });

  this.route('answersDepenseEdit', {
    template: 'answersDepenseEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/answersdepense/:answerId/edit/:key',
    loadingTemplate: 'loading',
  });

  this.route('formsEdit', {
    template: 'formsEdit',
    path: '/organizations/:orgaCibleId/:scope/forms/:_id/budgetdepense/:answerId/editform',
    loadingTemplate: 'loading',
  });

  this.route('invitations', {
    template: 'pageInvitations',
    path: '/:scope/invitations/:_id',
    loadingTemplate: 'loading',
  });

  this.route('invitationsOrga', {
    template: 'pageInvitations',
    path: '/organizations/:orgaCibleId/:scope/invitations/:_id',
    loadingTemplate: 'loading',
  });

  this.route('messagesActions', {
    template: 'pageMessages',
    path: '/:scope/messagesaction/:_id/room/:roomId/action/:actionId',
    loadingTemplate: 'loading',
  });

  this.route('messages', {
    template: 'pageMessages',
    path: '/:scope/messages/:_id',
    loadingTemplate: 'loading',
  });

  this.route('listeventSous', {
    template: 'listeventSous',
    path: '/events/sous/:_id',
    loadingTemplate: 'loading',
  });

  this.route('listMembers', {
    template: 'listMembers',
    path: '/organizations/members/:orgaCibleId',
    loadingTemplate: 'loading',
  });

  this.route('listMembersDetailCitoyens', {
    template: 'listMembersDetailCitoyens',
    path: '/organizations/members/:orgaCibleId/citoyens/:citoyenId',
    loadingTemplate: 'loading',
  });

  this.route('listHistoricCreditDetailOrga', {
    template: 'listHistoricCreditDetailOrga',
    path: '/organizations/historic-credits/:orgaCibleId',
    loadingTemplate: 'loading',
  });

  this.route('creditHistoryStats', {
    template: 'creditHistoryStats',
    path: '/organizations/credit-history-stats/:orgaCibleId',
    loadingTemplate: 'loading',
  });

  // /organizations/members/555eba56c655675cdd65bf19/citoyens/55ee8d59e41d756612558516
  this.route('logUserActionsAdd', {
    template: 'logUserActionsAdd',
    path: '/organizations/members/:orgaCibleId/citoyens/:citoyenId/addlog',
    loadingTemplate: 'loading',
  });

  this.route('transfertOcecoTibillet', {
    template: 'transfertOcecoTibillet',
    path: '/organizations/transfert/:orgaCibleId/tibillet',
    loadingTemplate: 'loading',
  });

  this.route('usersView', {
    template: 'usersView',
    path: '/usersView/:_id',
    loadingTemplate: 'loading',
  });

  this.route('assignUserDashboard', {
    template: 'assignUserDashboard',
    path: '/assignUserDashboard/:_id',
  });

  this.route('listAttendees', {
    template: 'listAttendees',
    path: '/events/attendees/:_id',
    loadingTemplate: 'loading',
  });

  this.route('listFollows', {
    template: 'listFollows',
    path: '/citoyens/follows/:_id',
    loadingTemplate: 'loading',
  });

  this.route('rolesEdit', {
    template: 'rolesEdit',
    path: '/:scope/roles/:_id/type/:childType/cible/:childId/',
    loadingTemplate: 'loading',
  });

  this.route('notifications', {
    template: 'notifications',
    path: '/notifications',
    loadingTemplate: 'loading',
  });

  this.route('searchGlobal', {
    template: 'Page_search',
    path: '/search',
    loadingTemplate: 'loading',
  });
  this.route('polesView', {
    template: 'polesView',
    path: '/polesView/:pole',
    loadingTemplate: 'loading',

  });
  this.route('home', {
    template: 'homeView',
    path: '/',
    loadingTemplate: 'loading',

  });
  this.route('homeredir', {
    template: 'homeView',
    path: '/home',
    loadingTemplate: 'loading',

  });
  this.route('actionView', {
    template: 'actionView',
    path: '/actionView/:id',
    loadingTemplate: 'loading',

  });
  this.route('userPublicProfile', {
    template: 'userPublicProfile',
    path: '/userPublicProfile/:id',
    loadingTemplate: 'loading',

  });

  this.route('wallet', {
    template: 'wallet',
    path: '/wallet',
    loadingTemplate: 'loading',
  });

  this.route('adminDashboard', {
    template: 'adminDashboard',
    path: '/adminDashboard',
    loadingTemplate: 'loading',
  });

  this.route('walletOrga', {
    template: 'wallet',
    path: '/organizations/:orgaCibleId/wallet',
    loadingTemplate: 'loading',
  });

  this.route('adminDashboardOrga', {
    template: 'adminDashboard',
    path: '/organizations/:orgaCibleId/adminDashboard',
    loadingTemplate: 'loading',
  });

  // /me/profil = /citoyens/detail/55ed9107e41d75a41a558524
  // /me/notes = /citoyens/notes/55ed9107e41d75a41a558524
  // /me/notes/add = /citoyens/notes/55ed9107e41d75a41a558524/editorDoc
  // /me/organizations = /citoyens/organizations/55ed9107e41d75a41a558524
  // /me/actions = /citoyens/actions/55ed9107e41d75a41a558524
  // /me/actions/add = /citoyens/rooms/55ed9107e41d75a41a558524/room/add/actionadd
  // /me/actions/agenda = /actionsAgenda/citoyens/55ed9107e41d75a41a558524

  Router.route('/me/profil', function () {
    this.redirect('detailList', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meProfil',
  });

  Router.route('/me/notes', function () {
    this.redirect('notesList', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meNotes',
  });

  Router.route('/me/note/add', function () {
    this.redirect('notesEditor', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meNoteAdd',
  });


  Router.route('/me/note/:docId', function () {
    this.redirect('notesEditor', { scope: 'citoyens', _id: Meteor.userId(), docId: this.params.docId }, { replaceState: true });
  }, {
    name: 'meNote',
  });


  Router.route('/me/organizations', function () {
    this.redirect('organizationsList', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meOrganizations',
  });

  Router.route('/me/actions', function () {
    this.redirect('actionsList', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meActions',
  });

  Router.route('/me/actions/add', function () {
    this.redirect('actionsAdd', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meActionsAdd',
  });

  Router.route('/me/actions/agenda', function () {
    this.redirect('listActionsAgendaScope', { scope: 'citoyens', _id: Meteor.userId() }, { replaceState: true });
  }, {
    name: 'meActionsAgenda',
  });

  // public

  this.route('notePublic', {
    layoutTemplate: 'layoutPublic',
    template: 'notePublic',
    path: '/:scope/notes/:_id/public/:docId',
  });

  this.route('noteSlidePublic', {
    layoutTemplate: 'layoutPublic',
    template: 'noteSlidePublic',
    path: '/:scope/notes/:_id/slidePublic/:docId',
  });


  // Protocole handler pour les liens web+oceco://
  Router.route('/redirect', function () {
    console.log('redirectUri', this.params.query.url);
    const url = this.params.query.url;

    if (url.startsWith('web+oceco://')) {
      let url2 = url.replace('web+oceco://', '');
      if (!url2.startsWith('/')) {
        url2 = `/${url2}`;
      }
      Router.go(url2, {}, { replaceState: true });
    }

    if (url.startsWith('web+note://')) {
      const docId = url.replace('web+note://', '');
      this.redirect('meNote', { docId: docId }, { replaceState: true });
    }

    // erreur 404
    this.render('pageNotFound');
  }, {
    name: 'redirectUri',
  });


  this.route('pageNotFound', {
    template: 'pageNotFound',
    path: '/(.*)',
  });
});

const ensurePixelSwitch = function () {
  // console.log('ensurePixelSwitch', this.route.getName());
  if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.pixelhumain) {
    if (this.params && this.params.orgaCibleId) {
      if (Session.get('orgaCibleId') !== this.params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', this.params.orgaCibleId);
        Session.set('loadMemberOrga', false);
      }
    }
    if (Session.get('orgaCibleId') && this.params.scope !== 'citoyens' && this.route.getName() !== 'actionsDetail') {
      if (Session.get('loadMemberOrga') === true) {
        // verifier si membres de l'orga
        const isMember = this.params && this.params.orgaCibleId ? Session.get(`isMembreOrga${this.params.orgaCibleId}`) : Session.get(`isMembreOrga${Session.get('orgaCibleId')}`);
        if (['home'].includes(this.route.getName())) {
          this.next();
        } else if (this.route.getName() !== 'home' && isMember === true) {
          this.next();
        } else if (this.route.getName() !== 'home' && !isMember) {
          this.redirect('home');
        }
      }
    } else if (['detailList', 'organizationsList', 'actionsList', 'notesList', 'notesEditor', 'notesSlide', 'actionsAdd', 'actionsDetailComments', 'listActionsAgendaScope', 'commentsActionsEdit'].includes(this.route.getName()) && this.params.scope === 'citoyens') {
      // si citoyen et pas d'orga cible
      this.next();
    } else if (['actionsDetail'].includes(this.route.getName())) {
      // comment verifier si on est membre de l'orga ?
      // console.log(this.route.getName());
      // console.log(this.params);
      this.next();
    } else {
      // console.log('ensurePixelSwitch', 'rien');
      this.render('switch');
    }
  } else {
    if (this.route.getName() === 'signOut') {
      this.next();
    } else {
      this.render('switch');
    }
  }
};

const ensurePixelSignin = function () {
  // console.log('ensurePixelSignin', this.route.getName());
  if (Meteor.userId()) {
    // console.log('ensurePixelSignin', Meteor.userId());
    this.next();
  } else {
    // recup url passer en param

    if (!['home', 'login', 'signin', 'passwordLost', 'pageNotFound', 'redirectUri'].includes(this.route.getName())) {
      Session.set('urlRedirect', this.url);
    } else {
      Session.set('urlRedirect', null);
    }

    this.render('login');
  }
};

const ensurePixelIsAdmin = function () {
  // console.log('ensurePixelIsAdmin', this.route.getName());
  if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.pixelhumain) {
    const isAdmin = Session.get(`isAdmin${Session.get('orgaCibleId')}`);
    // console.log(RaffId);
    // console.log(isAdmin);
    if (isAdmin === true) {
      this.next();
    }
  } else {
    this.render('login');
  }
};

const ensureUpdateStore = function () {
  // console.log('ensureUpdateStore', this.route.getName());
  if (!Session.get('updateStore')) {
    this.next();
  } else {
    this.render('updateStore');
  }
};

const ensureRedirectPublic = function () {
  // tester si on est connecter si oui on laisse passer sinon on redirige, si la route est 'notesEditorOrga', 'notesEditor' alors on redirige vers la route notePublic, si la route est 'notesSlideOrga', 'notesSlide' alors on redirige vers la route noteSlidePublic
  // console.log('ensureRedirectPublic', this.route.getName());
  if (Meteor.userId()) {
    this.next();
  } else {
    if (this.route.getName() === 'notesEditorOrga' || this.route.getName() === 'notesEditor') {
      this.redirect('notePublic', { scope: this.params.scope, _id: this.params._id, docId: this.params.docId });
    } else if (this.route.getName() === 'notesSlideOrga' || this.route.getName() === 'notesSlide') {
      this.redirect('noteSlidePublic', { scope: this.params.scope, _id: this.params._id, docId: this.params.docId });
    } else {
      this.next();
    }
  }
};

Router.onBeforeAction(ensureUpdateStore);
Router.onBeforeAction(ensureRedirectPublic, { only: ['notesEditorOrga', 'notesEditor', 'notesSlideOrga', 'notesSlide'] });
Router.onBeforeAction(ensurePixelSignin, { except: ['login', 'signin', 'passwordLost', 'pageNotFound', 'redirectUri', 'notePublic', 'noteSlidePublic'] });
Router.onBeforeAction(ensurePixelIsAdmin, { only: ['adminDashboard', 'listMembers', 'assignUserDashboard', 'pageNotFound', 'redirectUri'] });
Router.onBeforeAction(ensurePixelSwitch, {
  except: ['login', 'signin', 'passwordLost', 'switch', 'switchRedirect', 'searchGlobal', 'notifications', 'pageNotFound', 'signOut', 'redirectUri', 'notePublic', 'noteSlidePublic']
});

Router.routes.login.options.progress = false;
Router.routes.signin.options.progress = false;
Router.routes.passwordLost.options.progress = false;
Router.routes.listEvents.options.progress = false;
Router.routes.home.options.progress = false;
