import Token from 'markdown-it/lib/token'

const addSectionClose = (currentTokenIndex, state, replaceCurrentToken) => {
  const sectionCloseToken = new Token('section_close', 'section', -1)
  state.tokens.splice(currentTokenIndex, replaceCurrentToken ? 1 : 0, sectionCloseToken)
}

const addSectionOpen = (insertIndex, state) => {
  const sectionOpenToken = new Token('section_open', 'section', 1)
  state.tokens.splice(insertIndex, 0, sectionOpenToken)
}

export const addSlideSectionsMarkdownItPlugin = (markdownIt) => {
  markdownIt.core.ruler.push('reveal.sections', (state) => {
    let sectionBeginIndex = 0
    let lastSectionWasBranch = false

    for (let currentTokenIndex = 0; currentTokenIndex < state.tokens.length; currentTokenIndex++) {
      const currentToken = state.tokens[currentTokenIndex]

      if (currentToken.type !== 'hr') {
        continue
      }

      addSectionOpen(sectionBeginIndex, state)
      currentTokenIndex += 1

      if (currentToken.markup === '---' && lastSectionWasBranch) {
        lastSectionWasBranch = false
        addSectionClose(currentTokenIndex, state, false)
        currentTokenIndex += 1
      } else if (currentToken.markup === '----' && !lastSectionWasBranch) {
        lastSectionWasBranch = true
        addSectionOpen(sectionBeginIndex, state)
        currentTokenIndex += 1
      }

      addSectionClose(currentTokenIndex, state, true)
      sectionBeginIndex = currentTokenIndex + 1
    }

    addSectionOpen(sectionBeginIndex, state)
    addSectionClose(state.tokens.length, state, false)
    if (lastSectionWasBranch) {
      addSectionClose(state.tokens.length, state, false)
    }
    return true
  })
}
