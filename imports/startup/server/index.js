import '../../../i18n/en.i18n.json';
import '../../../i18n/fr.i18n.json';
import './migrations.js';
import '../../api/simpleschema-messages.js';
import '../../api/helper';
import './startup.js';
import './logger.js';
import './observePush.js';
import '../../api/server/rest.js';
import '../../infra/rest.js';
import '../../infra/meta-tags.js';
import './activityStreamHook.js';
import './actionsHook.js';
import './answersHook.js';
