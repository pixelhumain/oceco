/* eslint-disable no-unused-vars */
/* eslint-disable key-spacing */
/* eslint-disable import/prefer-default-export */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import i18n from 'meteor/universe:i18n';

import { Citoyens } from '../../api/collection/citoyens.js';
import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { CountUserActions, CountActions } from '../../api/collection/countUserActions.js';
import { Projects } from '../../api/collection/projects.js';
import { Actions } from '../../api/collection/actions.js';
import { LogUserActions } from '../../api/collection/loguseractions.js';
import { ActivityStream, ActivityStreamReference } from '../../api/collection/activitystream.js';
import { Answers } from '../../api/collection/answers.js';

// import { apiCommunecter } from '../../api/server/api.js';


i18n.setOptions({
  open: '__',
  close: '__',
  defaultLocale: 'en',
  sameLocaleOnServerConnection: true,
  // translationsHeaders: {'Cache-Control':'no-cache'},
});

function parseBool(val) {
  if ((typeof val === 'string' && (val.toLowerCase() === 'true' || val.toLowerCase() === 'yes')) || val === 1) { return true; }
  if ((typeof val === 'string' && (val.toLowerCase() === 'false' || val.toLowerCase() === 'no')) || val === 0) { return false; }
  return null;
}

Meteor.startup(function () {

  // const orgaOceco = Organizations.find({ oceco: { $exists: true } });
  // orgaOceco.forEach(function (orga) {
  //   const scopeId = orga._id.valueOf();
  //   const scopeOne = orga;
  //   if (scopeOne) {
  //     const countAll = scopeOne.actionIndicatorCount('all').count();
  //     if (countAll > 0) {
  //       Organizations.update({ _id: orga._id }, { $unset: { 'actionsCount.all': "" } });
  //       scopeOne.actionIndicatorCount('all').forEach((action) => {
  //         Actions.update({ _id: action._id }, { $unset: { numberId: "" } });
  //       });
  //     }
  //   }

  //   // list projet de l'orga
  //   const listProjects = scopeOne.listProjects();
  //   if (listProjects && listProjects.count() > 0) {
  //     listProjects.forEach((project) => {
  //       // projects count
  //       if (project) {
  //         const countAll = project.actionIndicatorCount('all').count();
  //         if (countAll > 0) {
  //           Projects.update({ _id: project._id }, { $unset: { 'actionsCount.all': "" } });
  //           project.actionIndicatorCount('all').forEach((action) => {
  //             Actions.update({ _id: action._id }, { $unset: { numberId: "" } });
  //           });
  //         }
  //       }
  //     });
  //   }
  // });

  // const orgaOceco = Organizations.find({ oceco: { $exists: true } });
  // orgaOceco.forEach(function (orga) {
  //   const scopeId = orga._id.valueOf();
  //   const scopeOne = orga;
  //   if (scopeOne) {
  //     const countAll = scopeOne.actionIndicatorCount('all').count();
  //     if (countAll > 0) {
  //       Organizations.update({ _id: orga._id }, { $set: { 'actionsCount.all': countAll } });
  //       let inc = 0;
  //       scopeOne.actionIndicatorCount('all').forEach((action) => {
  //         // if (!action.numberId) {
  //         inc += 1;
  //         console.log('inc', inc);
  //         Actions.update({ _id: action._id }, { $set: { numberId: inc } });
  //         // }
  //       });
  //     }
  //   }

  //   // list projet de l'orga
  //   const listProjects = scopeOne.listProjects();
  //   if (listProjects && listProjects.count() > 0) {
  //     listProjects.forEach((project) => {
  //       // projects count
  //       if (project) {
  //         const countAll = project.actionIndicatorCount('all').count();
  //         if (countAll > 0) {
  //           Projects.update({ _id: project._id }, { $set: { 'actionsCount.all': countAll } });
  //           let inc = 0;
  //           console.log('project', countAll);
  //           project.actionIndicatorCount('all').forEach((action) => {
  //             inc += 1;
  //             console.log('inc', inc);
  //             // if (!action.numberId) {
  //             Actions.update({ _id: action._id }, { $set: { numberId: inc } });
  //             // }
  //           });
  //         }
  //       }
  //     });
  //   }
  // });

  /*
  const send = {};
  send.title = 'test api oceco';
  send.description = 'test api oceco';
  send.labels = 'oceco';
  apiCommunecter.callGitlabrest('https://gitlab.adullact.net/api/v4', 'xxxxx', 'POST', 'projects/609/issues', send);
  */

  /* const send = {};
  send.title = 'test api oceco';
  send.body = 'test api oceco';
  send.labels = ['oceco'];
  apiCommunecter.callGithubrest('https://api.github.com/repos/pixelhumain', 'xxxx', 'POST', 'oceco/issues', send);
  */

  /* Answers.find({ 'answers.aapStep1': { '$exists': true }, form: '618b7b4abc572b35b4505da3' }).forEach(function (doc) {
  
    if (doc.answers && doc.answers.aapStep1 && doc.answers.aapStep1.depense) {
      Object.keys(doc.answers.aapStep1.depense).forEach(function (key) {
        console.log(key);
        if (typeof (doc.answers.aapStep1.depense[key].date) === 'string') {
          doc.answers.aapStep1.depense[key].date = new Date(moment(doc.answers.aapStep1.depense[key].date, 'D/M/YYYY').toISOString());
        }
        if (typeof (doc.answers.aapStep1.depense[key].price) === 'string') {
          doc.answers.aapStep1.depense[key].price = parseFloat(doc.answers.aapStep1.depense[key].price);
        }
        if (doc.answers.aapStep1.depense[key].financer) {
          Object.keys(doc.answers.aapStep1.depense[key].financer).forEach(function (keyf) {
            if (typeof (doc.answers.aapStep1.depense[key].financer[keyf].date) === 'string') {
              doc.answers.aapStep1.depense[key].financer[keyf].date = new Date(moment(doc.answers.aapStep1.depense[key].financer[keyf].date, 'D/M/YYYY').toISOString());
            }
            if (typeof (doc.answers.aapStep1.depense[key].financer[keyf].amount) === 'string') {
              doc.answers.aapStep1.depense[key].financer[keyf].amount = parseFloat(doc.answers.aapStep1.depense[key].financer[keyf].amount);
            }
          });
        }
        if (doc.answers.aapStep1.depense[key].estimates) {
          Object.keys(doc.answers.aapStep1.depense[key].estimates).forEach(function (keye) {
            if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].date) === 'string') {
              doc.answers.aapStep1.depense[key].estimates[keye].date = new Date(moment(doc.answers.aapStep1.depense[key].estimates[keye].date, 'D/M/YYYY').toISOString());
            }
            if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].price) === 'string') {
              doc.answers.aapStep1.depense[key].estimates[keye].price = parseFloat(doc.answers.aapStep1.depense[key].estimates[keye].price);
            }
            if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].days) === 'string') {
              doc.answers.aapStep1.depense[key].estimates[keye].days = parseInt(doc.answers.aapStep1.depense[key].estimates[keye].days);
            }
            if (typeof (doc.answers.aapStep1.depense[key].estimates[keye].selected) === 'string') {
              doc.answers.aapStep1.depense[key].estimates[keye].selected = parseBool(doc.answers.aapStep1.depense[key].estimates[keye].selected);
            }
          });
        }
        if (doc.answers.aapStep1.depense[key].payement) {
          Object.keys(doc.answers.aapStep1.depense[key].payement).forEach(function (keyp) {
            if (typeof (doc.answers.aapStep1.depense[key].payement[keyp].date) === 'string') {
              doc.answers.aapStep1.depense[key].payement[keyp].date = new Date(moment(doc.answers.aapStep1.depense[key].payement[keyp].date, 'D/M/YYYY').toISOString());
            }
            if (typeof (doc.answers.aapStep1.depense[key].payement[keyp].amount) === 'string') {
              doc.answers.aapStep1.depense[key].payement[keyp].amount = parseFloat(doc.answers.aapStep1.depense[key].payement[keyp].amount);
            }
          });
        }
        console.log(EJSON.stringify(doc));
        Answers.update({ _id: doc._id }, { $set: { 'answers.aapStep1.depense': doc.answers.aapStep1.depense } });
      });
    }
    if (!Array.isArray(doc.status)) {
      delete doc.status;
      Answers.update({ _id: doc._id }, { $unset: { 'status': '' } });
    }
  }); */

  /* robmongo shell */
  /* db.getCollection('activityStream').find({ type: 'oceco' }).forEach(function (doc) {
      if (doc.notify.id && doc.notify.id) {
        const authorArray = doc.author ? Object.keys(doc.author) : null;
        const objectArray = doc.object ? Object.keys(doc.object) : null;
        const reference = {
          notificationId: doc._id.valueOf(),
          type: doc.type,
          updated: doc.updated,
          verb: doc.verb,
          targetId: doc.target.id,
          targetType: doc.target.type,
          notifyObjectType: doc.notify.objectType,
        };
        if (doc.targetEvent && doc.targetEvent.id) {
          reference.targetEvent = doc.targetEvent.id;
        }
        if (doc.targetRoom && doc.targetRoom.id) {
          reference.targetRoom = doc.targetRoom.id;
        }
        if (doc.targetProject && doc.targetProject.id) {
          reference.targetProject = doc.targetProject.id;
        }
  
        if (authorArray && authorArray.length > 0) {
          [reference.author] = authorArray;
        }
        if (objectArray && objectArray.length > 0) {
          [reference.objectId] = objectArray;
          reference.objectType = doc.object[reference.objectId].type;
        }
        Object.keys(doc.notify.id).forEach((key) => {
          reference.userId = key;
                reference.isUnread = doc.notify.id[key].isUnread === true ? true : false;
                reference.isUnseen = doc.notify.id[key].isUnseen === true ? true : false;
            if(!db.getCollection('activityStreamReference').findOne({notificationId:reference.notificationId,userId:reference.userId})){
            db.getCollection('activityStreamReference').insert(reference);
            }
        });
      }
    });
    */
  /* if (ActivityStreamReference.find({ type: 'oceco' }).count() === 0) {
    ActivityStream.find({ type: 'oceco' }).forEach(function (doc) {
      if (doc.notify.id && doc.notify.id) {
        const authorArray = doc.author ? Object.keys(doc.author) : null;
        const reference = {
          notificationId: doc._id.valueOf(),
          type: doc.type,
          updated: doc.updated,
          verb: doc.verb,
          targetId: doc.target.id,
          targetType: doc.target.type,
          targetParentId: doc.targetParentId ? doc.targetParentId : null,
          targetParentType: doc.targetParentType ? doc.targetParentType : null,
          notifyObjectType: doc.notify.ObjectType,
          objectId: doc.object.id,
          objectType: doc.object.type,
        };
        if (doc.targetEvent && doc.targetEvent.id) {
          reference.targetEvent = doc.targetEvent.id;
        }
        if (doc.targetRoom && doc.targetRoom.id) {
          reference.targetRoom = doc.targetRoom.id;
        }
        if (doc.targetProject && doc.targetProject.id) {
          reference.targetProject = doc.targetProject.id;
        }
  
        if (authorArray && authorArray.length > 0) {
          [reference.author] = authorArray;
        }
        Object.keys(doc.notify.id).forEach((key) => {
          reference.userId = key;
          reference.isUnread = doc.notify.id[key].isUnread;
          reference.isUnseen = doc.notify.id[key].isUnseen;
  
          ActivityStreamReference.insert(reference);
        });
      }
    });
  } */

  // liste les actions avec finishedBy exist true
  /* const actionsArray = Actions.find({ finishedBy: { $exists: true } });
  if (actionsArray.count() > 0 && LogUserActions.find().count() === 0) {
    actionsArray.forEach((action) => {
      Object.keys(action.finishedBy).forEach((key) => {
        if (action.finishedBy[key] === 'validated') {
          const parentObjectId = new Mongo.ObjectID(action.parentId);
          const orgOne = Organizations.findOne({ _id: parentObjectId });
          let orgId;
          if (orgOne) {
            orgId = orgOne._id.valueOf();
          } else {
            const eventId = Events.findOne({ _id: parentObjectId }) ? Events.findOne({ _id: parentObjectId })._id.valueOf() : null;
            const event = eventId ? `links.events.${eventId}` : null;
  
            const projectId = event ? Projects.findOne({ [event]: { $exists: 1 } })._id.valueOf() : null;
            const project = projectId ? `links.projects.${projectId}` : `links.projects.${Projects.findOne({ _id: parentObjectId })._id.valueOf()}`;
  
            orgId = Organizations.findOne({ [project]: { $exists: 1 } })._id.valueOf();
          }
  
          const logInsert = {};
          logInsert.userId = key;
          logInsert.organizationId = orgId;
          logInsert.actionId = action._id.valueOf();
          if (action.credits) {
            logInsert.createdAt = moment(action.endDate).format();
            logInsert.credits = action.credits;
            LogUserActions.insert(logInsert);
          }
          const userActions = `userWallet.${orgId}.userActions.${action._id.valueOf()}`;
          Citoyens.update({ _id: new Mongo.ObjectID(key) }, { $unset: { [userActions]: '' } });
        }
      });
    });
  } */

  /* const actionsArray = Actions.find({ credits: { $lt: 0 }, finishedBy: { $exists: true } });
  if (actionsArray.count() > 0) {
    actionsArray.forEach((action) => {
      // action.finishedBy
      Object.keys(action.finishedBy).forEach((key) => {
        if (action.finishedBy[key] === 'validated') {
          const parentObjectId = new Mongo.ObjectID(action.parentId);
          const orgOne = Organizations.findOne({ _id: parentObjectId });
          let orgId;
          if (orgOne) {
            orgId = orgOne._id.valueOf();
          } else {
            const eventId = Events.findOne({ _id: parentObjectId }) ? Events.findOne({ _id: parentObjectId })._id.valueOf() : null;
            const event = eventId ? `links.events.${eventId}` : null;
  
            const projectId = event ? Projects.findOne({ [event]: { $exists: 1 } })._id.valueOf() : null;
            const project = projectId ? `links.projects.${projectId}` : `links.projects.${Projects.findOne({ _id: parentObjectId })._id.valueOf()}`;
  
            orgId = Organizations.findOne({ [project]: { $exists: 1 } })._id.valueOf();
          }
  
          const logInsert = {};
          logInsert.userId = key;
          logInsert.organizationId = orgId;
          logInsert.actionId = action._id.valueOf();
          if (action.credits) {
            logInsert.createdAt = moment(action.endDate).format();
            logInsert.credits = action.credits;
            const logOne = LogUserActions.findOne(logInsert);
            if (logOne) {
              LogUserActions.update({ _id: logOne._id }, logInsert);
            } else {
              LogUserActions.insert(logInsert);
            }
          }
        }
      });
    });
  } */

  // correction de tools.chat.int vide
  /* const chatProject = Projects.find({ hasRC:{ $exists:true }, slug:{ $exists:true }, 'tools.chat.int':{ $exists:false } });
  if (chatProject.count() > 0) {
    chatProject.forEach((project) => {
      let path;
      if (project.preferences.private === true || project.preferences.private === 'true') {
        path = `/group/${project.slug}`;
      } else {
        path = `/channel/${project.slug}`;
      }
      Projects.update({ _id: project._id }, { $set: { hasRC: true }, $addToSet: { 'tools.chat.int': { name: project.slug, url: path } } });
    });
  }
  const chatOrga = Organizations.find({ hasRC: { $exists: true }, slug: { $exists: true }, 'tools.chat.int': { $exists: false } });
  if (chatOrga.count() > 0) {
    chatOrga.forEach((project) => {
      let path;
      if (project.preferences.private === true || project.preferences.private === 'true') {
        path = `/group/${project.slug}`;
      } else {
        path = `/channel/${project.slug}`;
      }
      Organizations.update({ _id: project._id }, { $set: { hasRC: true }, $addToSet: { 'tools.chat.int': { name: project.slug, url: path } } });
    });
  } */

  if (Meteor.isDevelopment) {
    const { protocol, username, password, host, port } = Meteor.settings.mailSetting?.dev || {};
    if (protocol && username && password && host && port) {
      process.env.MAIL_URL = `${protocol}://${username}:${password}@${host}:${port}/`;
    }
  } else if (Meteor.isProduction) {
    const { protocol, username, password, host, port } = Meteor.settings.mailSetting?.prod || {};
    if (protocol && username && password && host && port) {
      process.env.MAIL_URL = `${protocol}://${username}:${password}@${host}:${port}/`;
    }
  }
});
