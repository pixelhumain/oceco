/* eslint-disable */
const HTMLToCache = '/';
const version = 'MSW V0.5';

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(version).then(cache => {
      cache.add(HTMLToCache).then(self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(
    caches
      .keys()
      .then(cacheNames =>
        Promise.all(
          cacheNames.map(cacheName => {
            if (version !== cacheName) return caches.delete(cacheName);
          })
        )
      )
      .then(self.clients.claim())
  );
});

self.addEventListener('fetch', event => {
  if (!(event.request.url.indexOf('http') === 0)) return;
  if (event.request.cache === "reload") return;
  if (event.request.method === "POST") {
    event.respondWith(
      fetch(event.request, { cache: 'no-store' })
    );
  } else if (event.request.url.includes('/ical/')) {
    event.respondWith(
      fetch(event.request, { cache: 'no-store' })
    );
  } else {
    const requestToFetch = event.request.clone();
    event.respondWith(
      caches.match(event.request.clone()).then(cached => {
        // We don't return cached HTML (except if fetch failed)
        if (cached) {
          const resourceType = cached.headers.get('content-type');
          // We only return non css/js/html cached response e.g images
          if (!hasHash(event.request.url) && !/text\/html/.test(resourceType)) {
            return cached;
          }

          // If the CSS/JS didn't change since it's been cached, return the cached version
          if (
            hasHash(event.request.url) &&
            hasSameHash(event.request.url, cached.url)
          ) {
            return cached;
          }
        }
        return fetch(requestToFetch)
          .then(response => {
            const clonedResponse = response.clone();
            const contentType = clonedResponse.headers.get('content-type');

            if (
              !clonedResponse ||
              clonedResponse.status !== 200 ||
              clonedResponse.type !== 'basic' ||
              /\/sockjs\//.test(event.request.url)
            ) {
              return response;
            }

            if (/html/.test(contentType)) {
              caches
                .open(version)
                .then(cache => cache.put(HTMLToCache, clonedResponse));
            } else {
              // Delete old version of a file
              if (hasHash(event.request.url)) {
                caches.open(version).then(cache =>
                  cache.keys().then(keys =>
                    keys.forEach(asset => {
                      if (
                        new RegExp(removeHash(event.request.url)).test(
                          removeHash(asset.url)
                        )
                      ) {
                        cache.delete(asset);
                      }
                    })
                  )
                );
              }

              caches
                .open(version)
                .then(cache => cache.put(event.request, clonedResponse));
            }
            return response;
          })
          .catch(() => {
            if (hasHash(event.request.url))
              return caches.match(event.request.url);
            else if (!/\/sockjs\//.test(event.request.url))
              // If the request URL hasn't been served from cache and isn't sockjs we suppose it's HTML
              return caches.match(HTMLToCache);
            // Only for sockjs
            return new Response('No connection to the server', {
              status: 503,
              statusText: 'No connection to the server',
              headers: new Headers({ 'Content-Type': 'text/plain' }),
            });
          });
      })
    );
  }
});


self.addEventListener('push', function (event) {
  // console.log('[Service Worker] Push Received.', event);

  let pushData = {}; // Initialiser un objet pour les données de la notification
  try {
    pushData = event.data ? JSON.parse(event.data.text()) : {};
  } catch (error) {
    console.error('[Service Worker] Error parsing push data as JSON', error);
  }

  const title = pushData.title || 'Notification par défaut';
  const options = {
    icon: pushData.icon || '/oceco_192.png',
    vibrate: pushData.vibrate || [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: pushData.primaryKey || '1',
      url: pushData.url // Ajouter l'URL aux données de la notification pour utilisation ultérieure
    },
    actions: [
      { action: 'show', title: 'Afficher' },
      { action: 'close', title: 'Fermer' }
    ],
    body: pushData.body || 'Pas de corps de notification spécifié.',
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', event => {
  event.notification.close();
  const urlToOpen = new URL(event.notification.data.url);
  const originToMatch = urlToOpen.origin;

  event.waitUntil(
    clients.matchAll({
      type: 'window',
      includeUncontrolled: true
    })
      .then(windowClients => {
        // Trouver un client sous le même origine/domaine
        let matchingClient = windowClients.find(wc => new URL(wc.url).origin === originToMatch);

        if (matchingClient && 'navigate' in matchingClient) {
          // Envoie un message au client pour naviguer à la nouvelle URL
          matchingClient.postMessage({ type: 'navigate', url: urlToOpen.href });
          matchingClient.focus();
          return matchingClient.navigate(urlToOpen.href);
        } else {
          // Si aucun client correspondant n'est trouvé, ouvre une nouvelle fenêtre avec urlToOpen
          return clients.openWindow(urlToOpen.href);
        }
      })
  );
});

self.addEventListener('notificationclose', event => {
  const closedNotification = event.notification;
  const notificationData = closedNotification.data; // If you stored data when creating the notification

  // You can perform additional actions or logging here
  console.log('Notification was closed:', notificationData);
});

function removeHash(element) {
  if (typeof element === 'string') return element.split('?hash=')[0];
}

function hasHash(element) {
  if (typeof element === 'string') return /\?hash=.*/.test(element);
}

function hasSameHash(firstUrl, secondUrl) {
  if (typeof firstUrl === 'string' && typeof secondUrl === 'string') {
    return /\?hash=(.*)/.exec(firstUrl)[1] === /\?hash=(.*)/.exec(secondUrl)[1];
  }
}

// Service worker created by Ilan Schemoul alias NitroBAY as a specific Service Worker for Meteor
// Please see https://github.com/NitroBAY/meteor-service-worker for the official project source
