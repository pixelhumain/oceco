import '../imports/infra/serviceWorkerInit.js';

import '../imports/startup/client';

import '../imports/ui/global/global.js';

import '../imports/ui/pixel.js';

import '../imports/ui/loading/loading.js';

import '../imports/ui/login/login.js';

import '../imports/ui/images/images.js';

import '../imports/ui/switch/switch.js';

import '../imports/ui/home/home.js';

import '../imports/ui/polesView/polesView.js';

import '../imports/ui/wallet/wallet.js';

import '../imports/ui/scopeTemplate/scopeTemplate.js';

import '../imports/ui/admin/admin.js';

import '../imports/ui/organizations/members/members.js';
import '../imports/ui/organizations/list.js';

import '../imports/ui/events/attendees/attendees.js';
// import '../imports/ui/events/sousevent/sousevent.js';
import '../imports/ui/events/list.js';


// import '../imports/ui/citoyens/follows/follows.js';
import '../imports/ui/citoyens/list.js';

// import '../imports/ui/projects/contributors/contributors.js';
import '../imports/ui/projects/list.js';

import '../imports/ui/rooms/actions/comments/comments.js';
import '../imports/ui/rooms/actions/actions.js';
import '../imports/ui/rooms/actions/assign/assign.js';

import '../imports/ui/home/assignUserDashboard/assignUserDashboard.js';

import '../imports/ui/invitations/invitations.js';
import '../imports/ui/messages/messages.js';

import '../imports/ui/notifications/notifications.js';

import '../imports/ui/projects/milestones/milestones.js';

import '../imports/ui/search/search.js';

import '../imports/ui/forms/answers/answers.js';
import '../imports/ui/forms/answers/comments/comments.js';

import '../imports/ui/editor/editor.js';
